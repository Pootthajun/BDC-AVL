﻿
<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="SettingSupplier.aspx.vb" Inherits="SettingSupplier" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp"%>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link type="text/css" rel="stylesheet" href="assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
    <div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3>
							ผู้ขาย
						</h3>			
						<ul class="breadcrumb">
                            
                            <li><i class="icon-cogs"></i> <a href="javascript:;">ตั้งค่าระบบ</a><i class="icon-angle-right"></i></li>
                        	<li><i class="icon-user"></i> <a href="javascript:;">ผู้ขาย</a></li>
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->	
				     </div>				
			    </div>
                <asp:Panel ID="pnlData" runat="server" DefaultButton="btnSearch"> 



                <div class="row-fluid">
               <div class="span12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
						<div class="portlet">
							<div class="portlet-body">


                               <div class="table-toolbar">
									<div class="btn-group">
                                        <asp:LinkButton CssClass="btn green" id="btnAdd" runat="server">
										    เพิ่ม <i class="icon-plus"></i>
										    </asp:LinkButton>
                                    </div>
                                    <div class="btn-group  ">
                                        <asp:LinkButton CssClass="btn blue" id="btnSearch" runat="server">
										    ค้นหา <i class="icon-search"></i>
										    </asp:LinkButton>
									</div>
								</div>
                                 <%--แบ่งส่วนการค้นหา--%>
                                <fieldset>
                                    <legend><b>ค้นหา</b></legend>
                                </fieldset>
                                <div class="row-fluid form-horizontal">
									<div class="span4 ">
									<div class="control-group">
										<label class="control-label"> เลขผู้เสียภาษี</label>
										<div class="controls">
                                            <asp:TextBox ID="txt_Search_TaxNo" runat="server"  CssClass="m-wrap medium" placeholder="ค้นหาจากเลขผู้เสียภาษี"></asp:TextBox>
										</div>
									</div>
								</div>	
								<div class="span6 ">
									<div class="control-group">
										<label class="control-label"> ชื่อ</label>
										<div class="controls">
                                            <asp:TextBox ID="txt_Search_Name" runat="server" CssClass="m-wrap large" placeholder="ค้นหาจากชื่อหลัก/ชื่อย่อ/ชื่อผู้ติดต่อ"></asp:TextBox>
										</div>
									</div>
								</div>	
								</div>
                                <div class="row-fluid form-horizontal">
									<div class="span4 ">
										<div class="control-group">
											<label class="control-label"> สถานะ</label>
											<div class="controls">
												<asp:DropDownList ID="ddlStatus" runat="server"  CssClass="medium m-wrap">
				                                    <asp:ListItem Value="All" Selected="true">ทั้งหมด</asp:ListItem>
				                                    <asp:ListItem Value="1">ใช้งาน</asp:ListItem>
				                                    <asp:ListItem Value="0">ไม่ใช้งาน</asp:ListItem>
			                                    </asp:DropDownList>
											</div>
										</div>
									</div>	
									<div class="span6 ">
										<div class="control-group">
											
										</div>
									</div>	
								</div>






                                <div class="portlet-body no-more-tables">
<%--                                    <asp:Button ID="btnSearch" runat="server"  style="display:none;" />  --%>
                                    <span style="font-size:14px;font-weight:bold;"><asp:Label ID="lblCountList" runat ="server" ></asp:Label></span>                                          
								    <table class="table table-full-width table-advance dataTable no-more-tables table-hover">                                
									    <thead>
										    <tr>
											    <th style="text-align:center; vertical-align:middle;">เลขผู้เสียภาษี</th>
											    <th style="text-align:center; vertical-align:middle;">ชื่อหลัก</th>
											    <th style="text-align:center; vertical-align:middle;">ชื่อย่อ</th>
											    <th style="text-align:center; vertical-align:middle;">ผู้ติดต่อ</th>
											    <th style="text-align:center; vertical-align:middle;" width="50">ใช้</th>
											    <th style="text-align:center; vertical-align:middle;" width="130">ดำเนินการ</th>
										    </tr>
									    </thead>
									    <tbody>
                                            <asp:Repeater ID="rptData" runat="server">
										        <ItemTemplate>
										        <tr>
											        <td data-title="เลขผู้เสียภาษี">
                                                        <asp:Label ID="lbl_TaxNo" runat ="server"></asp:Label>
                                                        <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
                                                    </td>
											        <td data-title="ชื่อหลัก"><asp:Label ID="lbl_Name" runat ="server"></asp:Label></td>
											        <td data-title="ชื่อย่อ"><asp:Label ID="lbl_NameAlias" runat ="server"></asp:Label></td>
                                                    <td data-title="ผู้ติดต่อ"><asp:Label ID="lbl_ContactName" runat ="server"></asp:Label></td>
											        <td data-title="ใช้" style="text-align:center;">
                                                        <asp:Image ID="imgStatus" runat="server" ImageUrl="images/check.png"/>
                                                    </td>
											        <td data-title="ดำเนินการ" style="text-align:center; width :200px;" >
                                                        <asp:LinkButton ID="btnRptEdit" runat="server" CssClass="btn mini purple" CommandName="Edit"><i class="icon-edit"></i> รายละเอียด</asp:LinkButton>
                                                        <asp:LinkButton ID="btnRptDelete" runat="server" CssClass="btn mini black"  CommandName="Delete"><i class="icon-trash"></i> ลบ</asp:LinkButton>
                                                            <asp:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" Enabled="true" ConfirmText="คุณต้องการลบข้อมูล ใช่หรือไม่?" TargetControlID="btnRptDelete">
                                                            </asp:ConfirmButtonExtender>
                                                    </td>
										        </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
									    </tbody>
								    </table>
                                    <asp:PageNavigation ID="Pager" MaximunPageCount="5" PageSize="20" runat="server" />
                                </div> 
							</div>
						</div>
						<!-- END SAMPLE TABLE PORTLET-->
					</div>
				</div>
                </asp:Panel>

                <asp:Panel ID="pnlEdit" runat="server" DefaultButton="btnSave" Visible="false">
                <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN VALIDATION STATES-->
						<div class="portlet box blue">
							<div class="portlet-title">
								<div class="caption"><i class="icon-user"></i>เพิ่ม/แก้ไข ข้อมูลผู้ขาย</div>
								<div class="tools">
								</div>
							</div>
							<div class="portlet-body form form-horizontal">
                                <%--<asp:Panel ID="pnlEdit_txt" runat="server">--%>
                                
                                <form class="form-horizontal" id="form_sample_2"  novalidate="novalidate">
                                    <div class="control-group">
										<label class="control-label">เลขผู้เสียภาษี <font id="lblValidate_Supplier_TaxNo" runat="server" color="red">*</font>&nbsp;&nbsp;</label>
										<div class="controls">
											<asp:TextBox ID="txtSupplier_TaxNo" runat="server" AutoPostBack ="true"   
                                                class="span6 m-wrap" MaxLength ="13" ></asp:TextBox>
										        <asp:FilteredTextBoxExtender ID="txtTaxNo_FilteredTextBoxExtender" 
                                                    runat="server" Enabled="True" TargetControlID="txtSupplier_TaxNo" 
                                                FilterType="Numbers" InvalidChars="1234567890" >
                                                </asp:FilteredTextBoxExtender>
                                                <span class="help-inline">(ตัวเลข 13 หลัก)</span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">ชื่อหลัก <font id="lblValidate_Supplier_No" runat="server"  color="red">*</font>&nbsp;&nbsp;</label>
										<div class="controls">
                                            <asp:Label ID="lblSupplier_No" runat="server"  style="display:none;"  ></asp:Label>
											<asp:TextBox ID="txtSupplier_Name" runat="server" class="span6 m-wrap"></asp:TextBox>
										</div>
									</div>
                                    <div class="control-group">
										<label class="control-label">ชื่อย่อ&nbsp;&nbsp;</label>
										<div class="controls">
											<asp:TextBox ID="txtSupplier_NameAlias" runat="server" class="span6 m-wrap"></asp:TextBox>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">ที่อยู่ <font  id="lblValidate_Supplier_Address" runat="server"  color="red">*</font>&nbsp;&nbsp;</label>
                                    <div class="controls">
											<asp:TextBox ID="txtSupplier_Address" TextMode="MultiLine" runat="server" rows="3" class="span6 m-wrap" cols="20"></asp:TextBox>
										</div>
									</div>
                                    <div class="control-group">
										<label class="control-label">ชื่อผู้ติดต่อ <font id="lblValidate_Supplier_Contact_Name" runat="server"  color="red">*</font>&nbsp;&nbsp;</label>
										<div class="controls">
											<asp:TextBox ID="txtSupplier_Contact_Name" runat="server" class="span6 m-wrap"></asp:TextBox>
										</div>
									</div>
                                    <div class="control-group">
										<label class="control-label">เบอร์โทรศัพท์ <font id="lblValidate_Supplier_Phone" runat="server"  color="red">*</font>&nbsp;&nbsp;</label>
										<div class="controls">
											<asp:TextBox ID="txtSupplier_Phone" runat="server" class="span6 m-wrap"></asp:TextBox>
										</div>
									</div>
                                    <div class="control-group">
										<label class="control-label">โทรสาร&nbsp;&nbsp;</label>
										<div class="controls">
											<asp:TextBox ID="txtSupplier_Fax" runat="server" class="span6 m-wrap"></asp:TextBox>
										</div>
									</div>
                                    <%--<div class="control-group">
										<label class="control-label">อีเมล์&nbsp;&nbsp;</label>
										<div class="controls">
											<asp:TextBox ID="txtSupplier_Email" runat="server" class="span6 m-wrap"></asp:TextBox>
										</div>
									</div>  --%> 
                                    <div class="control-group">
										<label class="control-label">อีเมล์&nbsp;&nbsp;<font id="lblValidate_Supplier_Email" runat="server"  color="red">*</font></label>
										<div class="controls">
											<asp:TextBox ID="txtSupplier_Email" runat="server"  class="span6 m-wrap" 
                                                ></asp:TextBox>
                                                <asp:RegularExpressionValidator id="myRegValid" runat="server"
                                                    ErrorMessage="กรอกรูปแบบ อีเมล์ ให้ถูกต้อง" ControlToValidate="txtSupplier_Email" 
		                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>                                               
										</div>
									</div>                            								
                                    <div class="control-group">
										<label class="control-label" >ใช้งาน &nbsp;&nbsp;</label>
										<div class="controls">
                                           <asp:ImageButton ID="imgStatus" runat="server" ImageUrl="images/check.png" ToolTip="Click เพื่อเปลี่ยน" />
										</div>
									</div> 
									<%--<div class="form-actions">
                                        <asp:Button ID="btnSave" runat="server" CssClass="btn blue" Text="บันทึก" />
										<asp:Button ID="btnCancel" runat="server"  CssClass="btn" Text="ยกเลิก" />
									</div>--%>
								
                                <%--</asp:Panel>--%>
                                <%--<asp:Panel ID="pnlEdit_btn" runat="server" >--%>
                               
                                 <div class="form-actions">
                                        <asp:Button ID="btnSave" runat="server" CssClass="btn blue" Text="บันทึก" />
										<asp:Button ID="btnCancel" runat="server"  CssClass="btn" Text="ยกเลิก" />

									</div>                            
                                </form>
                               <%-- </asp:Panel>--%>
								<!-- END FORM-->
							</div>
						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>
                </asp:Panel>
                <asp:Panel ID="pnl_btn" runat="server" >
                <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN VALIDATION STATES-->
						<div class="portlet ">
							
							<div class="portlet-body form form-horizontal">
                    <div class="form">
                        <asp:LinkButton CssClass="btn" id="btnBack" Width="200px"  runat="server">
										   <i class="icon-angle-left"></i> กลับ 
										    </asp:LinkButton>

					</div> 
                            </div>
                        </div>
                    </div>
                </div>

                </asp:Panel>
</div>
    </ContentTemplate> 
    </asp:UpdatePanel>
    <script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>	
	<script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.js" type="text/javascript" ></script>
	<script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
    <script type="text/javascript" src="assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
</asp:Content>

