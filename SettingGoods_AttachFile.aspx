﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="SettingGoods_AttachFile.aspx.vb" Inherits="SettingGoods_AttachFile" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link type="text/css" rel="stylesheet" href="assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							อัพโหลดไฟล์ทะเบียนครุภัณฑ์จากระบบ CDG
						</h3>		
															
						<ul class="breadcrumb">
                            
                            <li><i class="icon-cogs"></i> <a href="javascript:;">ตั้งค่าระบบ</a><i class="icon-angle-right"></i></li>
                        	<li><i class="icon-stethoscope"></i> <a href="javascript:;">อัพโหลดไฟล์ทะเบียนครุภัณฑ์จากระบบ CDG</a></li>
                        	
                            <uc1:wuc_datereporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->	
				     </div>				
			    </div>
               <div class="row-fluid">
               <div class="span12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
						<div class="portlet ">

							<div class="portlet-body">
                            <form class="form-horizontal" id="form1" action="#" novalidate="novalidate">
                                <br />
                                
                                <div class="row-fluid form-horizontal">
										<div class="span4 ">
										<div class="control-group">
                                                    <label class="control-label"><span  style =" font-size :18px;"><b>เลือกไฟล์</b></span></label>
											        <%--<h4><b> เลือกไฟล์ </b></h4>--%>
											<div class="controls">
												<asp:FileUpload CssClass ="btn-file " ID="FileUpload12" runat="server" />			                                    
											</div>
										</div>
									</div>	
									<div class="span6 ">
										<div class="control-group">
											<label class="control-label"> </label>
											<div class="controls">
										<button runat ="server"  class="btn green" id="sample_editable_1_new"> Preview <i class="icon-focus"></i> </button>
                                            </div>
										</div>
									</div>	
									</div>
                                <div class="row-fluid form-horizontal">
										<div class="span8 ">
										<div class="control-group">
											<div class="controls">
												<h5 style="color:#2F7EB9; font-weight:bold;"> 
                                                อัพโหลดไฟล์ Excel 
                                                <asp:Image ID="Image34" runat="server"  ImageUrl="images/file_type/file_extension_xls.png" ToolTip="เอกสาร Excel"  Visible="True" /> 
                                                &nbsp;เท่านั้น  หลังจากนั้นคลิกปุ่ม Preview</h5>			                                    
											</div>
										</div>
									</div>	
									<div class="span4 ">
										<div class="control-group">
											<label class="control-label"> </label>
											<div class="controls">
                                            </div>
										</div>
									</div>	
									</div>
                                <div class="portlet-body no-more-tables">
                                <span id="ctl00_ContentPlaceHolder1_lblCountList" style="font-size:14px;font-weight:bold;">พบ 
                                    11 รายการ</span>                                            
								<table class="table table-bordered  table-full-width table-advance dataTable no-more-tables table-hover">                                
									<thead>
										<tr>
                                            <th style="text-align:center; vertical-align:middle;"><b>ลำดับ</b></th>
                                            <th style="text-align:center; vertical-align:middle;"><b>เลือก</b> <asp:CheckBox ID="CheckBox2" Checked  CssClass =" checkbox " runat="server" /></th>
											<th style="text-align:center; vertical-align:middle;"><b>รหัสประเภท</b></th>
											<th style="text-align:center; vertical-align:middle;"><b>ชื่อประเภท</b></th>
											<th style="text-align:center; vertical-align:middle;"><b>รหัสครุภัณฑ์</b></th>
											<th style="text-align:center; vertical-align:middle;"><b>ชื่อครุภัณฑ์</b></th>
											<th style="text-align:center; vertical-align:middle;">รูปภาพ</th>
										</tr>
									</thead>
									<tbody>
										<tr>
                                            <td data-title="เลือก"  style="text-align:center; ">1</td>
                                            <td data-title="เลือก"  style="text-align:center; "><asp:CheckBox ID="CheckBox3" Checked CssClass =" checkbox " runat="server" /></td>
											<td data-title="รหัสประเภท" style="text-align:center; ">90</td>
											<td data-title="ชื่อประเภท">เครื่องมือและวัสดุทางเวชกรรมและศัลยกรรม</td>
											<td data-title="รหัสครุภัณฑ์" style="text-align:center; ">10006</td>
											<td data-title="ชื่อครุภัณฑ์" >เครื่องให้ยาสลบ&nbsp;</td>
											<td data-title="ราคากลาง (บาท)" style="text-align:center; ">
                                                <asp:Image ID="Image35" runat="server" ImageUrl="images/G1.jpg" ToolTip="เอกสาร Excel"  Visible="True" /> 
											</td>
										</tr>
                                        <tr>
                                            <td data-title="เลือก"  style="text-align:center; ">2</td>
                                            <td data-title="เลือก"  style="text-align:center; "><asp:CheckBox ID="CheckBox4" Checked CssClass =" checkbox " runat="server" /></td>
											<td data-title="รหัสประเภท" style="text-align:center; ">90</td>
											<td data-title="ชื่อประเภท">เครื่องมือและวัสดุทางเวชกรรมและศัลยกรรม</td>
											<td data-title="รหัสครุภัณฑ์" style="text-align:center; ">10074</td>
											<td data-title="ชื่อครุภัณฑ์" >เครื่องให้โลหิต&nbsp;&nbsp;&nbsp;</td>
											<td data-title="ราคากลาง (บาท)" style="text-align:center; ">
												<asp:Image ID="Image1" runat="server" ImageUrl="images/G2.jpg" ToolTip="เอกสาร Excel"  Visible="True" /> 			                                    
											</td>
										</tr>
                                        <tr>
                                            <td data-title="เลือก"  style="text-align:center; ">3</td>
                                            <td data-title="เลือก"  style="text-align:center; "><asp:CheckBox ID="CheckBox5" Checked  CssClass =" checkbox " runat="server" /></td>
											<td data-title="รหัสประเภท" style="text-align:center; ">90</td>
											<td data-title="ชื่อประเภท">เครื่องมือและวัสดุทางเวชกรรมและศัลยกรรม</td>
											<td data-title="รหัสครุภัณฑ์" style="text-align:center; ">10087</td>
											<td data-title="ชื่อครุภัณฑ์" >เครื่องให้อ๊อกซิเจน&nbsp;</td>
											<td data-title="ราคากลาง (บาท)" style="text-align:center; ">
												<asp:Image ID="Image2" runat="server" ImageUrl="images/G3.jpg" ToolTip="เอกสาร Excel"  Visible="True" /> 		                                    
											</td>
										</tr>
                                        <tr>
                                            <td data-title="เลือก"  style="text-align:center; ">4</td>
                                            <td data-title="เลือก"  style="text-align:center; "><asp:CheckBox ID="CheckBox6" Checked  CssClass =" checkbox " runat="server" /></td>
											<td data-title="รหัสประเภท" style="text-align:center; ">90&nbsp;</td>
											<td data-title="ชื่อประเภท">เครื่องมือและวัสดุทางเวชกรรมและศัลยกรรม</td>
											<td data-title="รหัสครุภัณฑ์" style="text-align:center; ">15678</td>
											<td data-title="ชื่อครุภัณฑ์" >เครื่องตรวจสเปคตรัม</td>
											<td data-title="ราคากลาง (บาท)" style="text-align:center; ">
												<asp:FileUpload CssClass ="mini btn-file " ID="FileUpload16" runat="server" />			                                    
											</td>
										</tr>
                                        <tr>
                                            <td data-title="เลือก"  style="text-align:center; ">5</td>
                                            <td data-title="เลือก"  style="text-align:center; "><asp:CheckBox ID="CheckBox7"  Checked CssClass =" checkbox " runat="server" /></td>
											<td data-title="รหัสประเภท" style="text-align:center; ">90&nbsp;</td>
											<td data-title="ชื่อประเภท">เครื่องมือและวัสดุทางเวชกรรมและศัลยกรรม</td>
											<td data-title="รหัสครุภัณฑ์" style="text-align:center; ">10379</td>
											<td data-title="ชื่อครุภัณฑ์" >เครื่องนับเม็ดโลหิต (Microceu Counter)</td>
											<td data-title="ราคากลาง (บาท)" style="text-align:center; ">
												<asp:FileUpload CssClass ="mini btn-file " ID="FileUpload17" runat="server" />			                                    
											</td>
										</tr>
                                        <tr>
                                            <td data-title="เลือก"  style="text-align:center; ">6</td>
                                            <td data-title="เลือก"  style="text-align:center; "><asp:CheckBox ID="CheckBox8"  Checked  CssClass =" checkbox " runat="server" /></td>
											<td data-title="รหัสประเภท" style="text-align:center; ">90&nbsp;</td>
											<td data-title="ชื่อประเภท">เครื่องมือและวัสดุทางเวชกรรมและศัลยกรรม</td>
											<td data-title="รหัสครุภัณฑ์" style="text-align:center; ">10379</td>
											<td data-title="ชื่อครุภัณฑ์" >อุปกรณ์อ่านค่าปริมาณเม็ดเลือดแดง (Hematocrit)&nbsp;&nbsp;</td>
											<td data-title="ราคากลาง (บาท)" style="text-align:center; ">
												<asp:FileUpload CssClass ="mini btn-file " ID="FileUpload18" runat="server" />			                                    
											</td>
										</tr>
										<tr>
                                            <td data-title="เลือก"  style="text-align:center; ">7</td>
                                            <td data-title="เลือก"  style="text-align:center; "><asp:CheckBox ID="CheckBox9"  Checked  CssClass =" checkbox " runat="server" /></td>
											<td data-title="รหัสประเภท" style="text-align:center; ">17&nbsp;</td>
											<td data-title="ชื่อประเภท">เครื่องตกแต่ง อุปกรณ์ 
                                                เครื่องใช้และวัสดุที่ใช้ในโรงพยาบาล&nbsp;&nbsp;&nbsp;</td>
											<td data-title="รหัสครุภัณฑ์" style="text-align:center; ">10373</td>
											<td data-title="ชื่อครุภัณฑ์" >2.0 mm. Mini Fragment Plate 4 H.&nbsp;&nbsp;</td>
											<td data-title="ราคากลาง (บาท)" style="text-align:center; ">
												<asp:FileUpload CssClass ="mini btn-file " ID="FileUpload19" runat="server" />			                                    
											</td>
										</tr>
                                        <tr>
                                            <td data-title="เลือก"  style="text-align:center; ">8</td>
                                            <td data-title="เลือก"  style="text-align:center; "><asp:CheckBox ID="CheckBox10"  Checked CssClass =" checkbox " runat="server" /></td>
											<td data-title="รหัสประเภท" style="text-align:center; ">17&nbsp;</td>
											<td data-title="ชื่อประเภท">เครื่องตกแต่ง อุปกรณ์ 
                                                เครื่องใช้และวัสดุที่ใช้ในโรงพยาบาล&nbsp;&nbsp;&nbsp;</td>
											<td data-title="รหัสครุภัณฑ์" style="text-align:center; ">10374</td>
											<td data-title="ชื่อครุภัณฑ์" >ADVANTAG EMETER ที่ตรวจน้ำตาลในเลือด&nbsp;&nbsp;</td>
											<td data-title="ราคากลาง (บาท)" style="text-align:center; ">
												<asp:FileUpload CssClass ="mini btn-file " ID="FileUpload20" runat="server" />			                                    
											</td>
										</tr>
                                        <tr>
                                            <td data-title="เลือก"  style="text-align:center; ">9</td>
                                            <td data-title="เลือก"  style="text-align:center; "><asp:CheckBox ID="CheckBox12"  Checked CssClass =" checkbox " runat="server" /></td>
											<td data-title="รหัสประเภท" style="text-align:center; ">17&nbsp;</td>
											<td data-title="ชื่อประเภท">เครื่องตกแต่ง อุปกรณ์ 
                                                เครื่องใช้และวัสดุที่ใช้ในโรงพยาบาล&nbsp;&nbsp;&nbsp;</td>
											<td data-title="รหัสครุภัณฑ์" style="text-align:center; ">10375</td>
											<td data-title="ชื่อครุภัณฑ์" >ARTERY FORCEP ขนาด 5.5&quot; ปลายตรง&nbsp;&nbsp;&nbsp;</td>
											<td data-title="ราคากลาง (บาท)" style="text-align:center; ">
												<asp:FileUpload CssClass ="mini btn-file " ID="FileUpload21" runat="server" />			                                    
											</td>
										</tr>
                                        <tr>
                                            <td data-title="เลือก"  style="text-align:center; ">10</td>
                                            <td data-title="เลือก"  style="text-align:center; "><asp:CheckBox ID="CheckBox11" 
                                                    CssClass =" checkbox " runat="server" /></td>
											<td data-title="รหัสประเภท" style="text-align:center; ">17&nbsp;</td>
											<td data-title="ชื่อประเภท">เครื่องตกแต่ง อุปกรณ์ 
                                                เครื่องใช้และวัสดุที่ใช้ในโรงพยาบาล&nbsp;&nbsp;&nbsp;</td>
											<td data-title="รหัสครุภัณฑ์" style="text-align:center; ">10356</td>
											<td data-title="ชื่อครุภัณฑ์" >BLOOD BAG 350 ML 5 DPC TRIPLE CPD-A1&nbsp;</td>
											<td data-title="ราคากลาง (บาท)" style="text-align:center; ">
												<asp:FileUpload CssClass ="mini btn-file " ID="FileUpload22" runat="server" />			                                    
											</td>
										</tr>
										<tr>
                                            <td data-title="เลือก"  style="text-align:center; ">11</td>
                                            <td data-title="เลือก"  style="text-align:center; "><asp:CheckBox ID="CheckBox1" CssClass =" checkbox " runat="server" /></td>
											<td data-title="รหัสประเภท" style="text-align:center; ">17&nbsp;</td>
											<td data-title="ชื่อประเภท">เครื่องตกแต่ง อุปกรณ์ 
                                                เครื่องใช้และวัสดุที่ใช้ในโรงพยาบาล&nbsp;&nbsp;&nbsp;</td>
											<td data-title="รหัสครุภัณฑ์" style="text-align:center; ">1099</td>
											<td data-title="ชื่อครุภัณฑ์" >BLOOD BAG 450 ML TRIPLE CPD-A1</td>
											<td data-title="ราคากลาง (บาท)" style="text-align:center; ">
												<asp:FileUpload CssClass ="mini btn-file " ID="FileUpload23" runat="server" />			                                    
											</td>
										</tr>
									</tbody>
								</table>

                                <div class="form-actions">
						            <button class="btn blue" type="submit">นำเข้าข้อมูลทะเบียนครุภัณฑ์ CDG</button>
						            <button class="btn" type="button">ยกเลิก</button>
					            </div>

                            </div> 
                            </form>                                                          
                                <div class="row-fluid">
                                  
                                </div>                                   
							</div>
						</div>
						<!-- END SAMPLE TABLE PORTLET-->
					</div>
				</div>

</div>
    <script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>	
	<script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.js" type="text/javascript" ></script>
	<script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceHolder" Runat="Server">
</asp:Content>

