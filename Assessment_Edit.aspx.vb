﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Web.UI
'Imports System.Windows.Forms

Partial Class Assessment_Edit
    Inherits System.Web.UI.Page

    Dim BL As New AVLBL
    Dim GL As New GenericLib
    Dim CL As New textControlLib

    Public Property FromPage As AVLBL.AssessmentType
        Get
            Try
                Return ViewState("FromPage")
            Catch ex As Exception
                Return AVLBL.AssessmentType.Assessment
            End Try
        End Get
        Set(value As AVLBL.AssessmentType)
            ViewState("FromPage") = value
        End Set
    End Property

    Private Property Dept_ID As String
        Get
            Try
                Return ddl_Dept.Items(ddl_Dept.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As String)
            BL.BindDDlDEPT(ddl_Dept, User_ID, value)
        End Set
    End Property

    Private Property Sub_Dept_ID As String
        Get
            Try
                Return ddl_Sub_Dept.Items(ddl_Sub_Dept.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As String)
            BL.BindDDlSub_Dept(ddl_Sub_Dept, Dept_ID, User_ID, Current_Step, value)
        End Set
    End Property

    Public Property Cat_Group_ID As String
        Get
            Try
                Return lblCat_Group_ID.Text
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As String)
            lblCat_Group_ID.Text = value
        End Set
    End Property

    Private ReadOnly Property Search_CatGroup_ID As Integer
        Get
            Try
                Return ddl_CatGroup.Items(ddl_CatGroup.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
    End Property

    Private ReadOnly Property Search_Cat_ID As Integer
        Get
            Try
                Return ddl_Cat.Items(ddl_Cat.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
    End Property

    Private ReadOnly Property Search_Sub_Cat_ID As Integer
        Get
            Try
                Return ddl_Sub_Cat.Items(ddl_Sub_Cat.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
    End Property

    Private ReadOnly Property Search_Type_ID As Integer
        Get
            Try
                Return ddl_Type.Items(ddl_Type.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
    End Property

    Private Property AssessmentRole As AVLBL.AssessmentRole
        Get
            Try
                Return ViewState("AssRole")
            Catch ex As Exception
                Return AVLBL.AssessmentRole.None
            End Try
        End Get
        Set(value As AVLBL.AssessmentRole)
            ViewState("AssRole") = value
        End Set
    End Property

    Private ReadOnly Property PageName As String
        Get
            Return Me.AppRelativeVirtualPath.Substring(2)
        End Get
    End Property

    Private Property AccessMode As AVLBL.AccessRole
        Get
            Try
                Return ViewState("AccessMode")
            Catch ex As Exception
                Return AVLBL.AccessRole.None
            End Try
        End Get
        Set(value As AVLBL.AccessRole)
            ViewState("AccessMode") = value
        End Set
    End Property

    Private ReadOnly Property User_ID As Integer
        Get
            Try
                If IsNumeric(Session("User_ID")) Then
                    Return Session("User_ID")
                Else
                    Return -1
                End If
            Catch ex As Exception
                Return -1
            End Try
        End Get
    End Property

    Public Property Current_Step As AVLBL.AssessmentStep
        Get
            Try
                Return ViewState("Current_Step")
            Catch ex As Exception
                Return AVLBL.AssessmentStep.Auditor
            End Try
        End Get
        Set(value As AVLBL.AssessmentStep)
            ViewState("Current_Step") = value
        End Set
    End Property

    Private Property Ass_ID As Integer
        Get
            Try
                Return ViewState("Ass_ID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Integer)
            ViewState("Ass_ID") = value
        End Set
    End Property

    Private Property S_ID As Integer
        Get
            Try
                Return btnSupplierDialog.Attributes("S_ID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Integer)
            btnSupplierDialog.Attributes("S_ID") = value
        End Set
    End Property

    Private ReadOnly Property S_Type_ID As Integer
        Get
            Try
                Return ddl_S_Type.Items(ddl_S_Type.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
    End Property

    Private Property Cat_Name As String
        Get
            Try
                Return lbl_Cat.Text
            Catch ex As Exception
                Return ""
            End Try
        End Get
        Set(value As String)
            lbl_Cat.Text = value
        End Set
    End Property

    Private Property Sub_Cat_Name As String
        Get
            Try
                Return lbl_Sub_Cat.Text
            Catch ex As Exception
                Return ""
            End Try
        End Get
        Set(value As String)
            lbl_Sub_Cat.Text = value
        End Set
    End Property

    Private Property Type_Name As String
        Get
            Try
                Return lbl_Type.Text
            Catch ex As Exception
                Return ""
            End Try
        End Get
        Set(value As String)
            lbl_Type.Text = value
            pnl_Product.Visible = value <> ""
        End Set
    End Property

    Private Property Running_Name As String
        Get
            Try
                Return lbl_Running.Text
            Catch ex As Exception
                Return ""
            End Try
        End Get
        Set(value As String)
            lbl_Running.Text = value
            pnl_Product.Visible = value <> ""
        End Set
    End Property

    Private Property Type_ID As Integer
        Get
            Try
                Return lbl_Item_Type.Attributes("Type_ID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Integer)
            lbl_Item_Type.Attributes("Type_ID") = value
        End Set
    End Property

    Public Property Running_ID As Integer
        Get
            Try
                Return lbl_Item_Type.Attributes("Running_ID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Integer)
            lbl_Item_Type.Attributes("Running_ID") = value
        End Set
    End Property

    Private Property Title_Item_Group As String '------------ประเภทใบประเมิน สินค้า  หรือ บริการ--------------
        Get
            Try
                Return lblTitle_Item_Name.Text
            Catch ex As Exception
                Return ""
            End Try
        End Get
        Set(value As String)
            lblTitle_Item_Name.Text = value
        End Set
    End Property

    Private Property Item_Type_Name As String
        Get
            Return lbl_Item_Type.Text
        End Get
        Set(value As String)
            lbl_Item_Type.Text = value
        End Set
    End Property

    Public Property File_ID As Integer
        Get
            Try
                Return btnSaveFile.Attributes("File_ID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Integer)
            btnSaveFile.Attributes("File_ID") = value
        End Set
    End Property

    Public Property Role_1_Active As Boolean
        Get
            Try
                Return lblRole_Active.Attributes("Role_1_Active")
            Catch ex As Exception
                Return False
            End Try
        End Get
        Set(value As Boolean)
            lblRole_Active.Attributes("Role_1_Active") = value
        End Set
    End Property
    Public Property Role_2_Active As Boolean
        Get
            Try
                Return lblRole_Active.Attributes("Role_2_Active")
            Catch ex As Exception
                Return False
            End Try
        End Get
        Set(value As Boolean)
            lblRole_Active.Attributes("Role_2_Active") = value
        End Set
    End Property
    Public Property Role_3_Active As Boolean
        Get
            Try
                Return lblRole_Active.Attributes("Role_3_Active")
            Catch ex As Exception
                Return False
            End Try
        End Get
        Set(value As Boolean)
            lblRole_Active.Attributes("Role_3_Active") = value
        End Set
    End Property
    Public Property Role_4_Active As Boolean
        Get
            Try
                Return lblRole_Active.Attributes("Role_4_Active")
            Catch ex As Exception
                Return False
            End Try
        End Get
        Set(value As Boolean)
            lblRole_Active.Attributes("Role_4_Active") = value
        End Set
    End Property


    Public Property IsPass_Status As Boolean

        Get
            Return ViewState("IsPass_Status")
        End Get
        Set(value As Boolean)
            ViewState("IsPass_Status") = value
        End Set
    End Property

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        If IsNothing(Session("User_ID")) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "ShowAlert('warning','กรุณาล็อกอินเข้าสู่ระบบ','Login.aspx');", True)
            Response.Redirect("Login.aspx")
            Exit Sub
        End If

        If Not IsPostBack Then
            Try
                Select Case Request.QueryString("From")
                    Case "Ass"
                        FromPage = AVLBL.AssessmentType.Assessment
                    Case "ReAss"
                        FromPage = AVLBL.AssessmentType.ReAssessment
                End Select
            Catch ex As Exception
                FromPage = AVLBL.AssessmentType.Assessment
            End Try

            '----------- Check สิทธิ์ในเมนู --------------
            Dim MT As DataTable = BL.GetUserMenuRole(User_ID, PageName)
            If MT.Rows.Count = 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลนี้','Default.aspx');", True)
                Exit Sub
            End If
            AccessMode = MT.Rows(0).Item("MR_ID") '--------- สิทธิ์ในเมนูของผู้ใช้คนปัจจุบัน -----------
            If AccessMode = AVLBL.AccessRole.None Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลนี้','Default.aspx');", True)
                Exit Sub
            End If

            '--------------- ตรวจสอบคามพร้อมของแบบประเมิน --------------
            If Not IsNumeric(Request.QueryString("Ass_ID")) Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','ไม่พบแบบประเมินดังกล่าว','Assessment_List.aspx');", True)
                Exit Sub
            End If
            Ass_ID = Request.QueryString("Ass_ID")
            Dim DT As DataTable = BL.GetAssessmentHeader(Ass_ID)
            If DT.Rows.Count = 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','ไม่พบแบบประเมินดังกล่าว','Assessment_List.aspx');", True)
                Exit Sub
            End If
            Current_Step = DT.Rows(0).Item("Current_Step")

            Session("_Sessionddl_S_Type") = 0
            '---------------- Bind All Item--------------------
            BindDept()
            BindSubDept()

            BindWorkflow()
            BindButtonRef()
            BindProductType()
            BindProductDetail()
            BindFile()
            BindSupplier()
            BindAssessment()
            BindCommentUser()
            '---------------- Hide Dialog ---------------------
            dialogProductType.Visible = False
            dialogSupplier.Visible = False
            dialogUploadFile.Visible = False

            checkComplete()
        End If

    End Sub

    Private Sub BindButtonRef()

        Dim SQL As String = " "
        SQL &= " SELECT DISTINCT vw_Ass_Header.Ass_ID,Ref_Year,Ref_Month,Ref_Number,Ref_Code" & vbLf
        SQL &= " ,vw_Ass_Header.Item_Name,Dept_ID,Dept_Name" & vbLf
        SQL &= " ,dbo.udf_AVLCode(AVL_Y,AVL_M,AVL_Dept_ID,AVL_Sub_Dept_ID,AVL_No) AVL_Code" & vbLf
        SQL &= " ,AVL_Status,Get_Score,Pass_Score,Max_Score,Current_Step,Step_TH,Update_Time" & vbLf
        SQL &= " FROM vw_Ass_Header " & vbLf
        SQL &= " WHERE Ass_ID ='" & Ass_ID & "'" & vbLf

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        DA.Fill(DT)
        If DT.Rows.Count = 1 Then
            If Not IsDBNull(DT.Rows(0).Item("AVL_Code")) Then
                lblRef.Text = " เลขใบประเมิน : " & DT.Rows(0).Item("Ref_Code").ToString & " | " & " ได้เลข  " & DT.Rows(0).Item("AVL_Code").ToString & "  | "
            Else
                lblRef.Text = " เลขใบประเมิน : " & DT.Rows(0).Item("Ref_Code").ToString & " | " '& " ไม่ผ่านการประเมิน " & "  | "
            End If

            If Not IsDBNull(DT.Rows(0).Item("Max_Score")) And Not IsDBNull(DT.Rows(0).Item("Pass_Score")) Then
                If DT.Rows(0).Item("Get_Score") >= DT.Rows(0).Item("Pass_Score") Then
                    lblResult.Text = " ได้คะแนน <i class='icon-ok-sign'></i>  " & DT.Rows(0).Item("Get_Score") & " / " & DT.Rows(0).Item("Max_Score")
                    lblResult.ForeColor = Drawing.Color.White
                Else
                    lblResult.Text = " ได้คะแนน <i class='icon-remove-sign'></i>  " & DT.Rows(0).Item("Get_Score") & " / " & DT.Rows(0).Item("Max_Score")
                    lblResult.ForeColor = Drawing.Color.White
                End If
            End If

        End If


    End Sub

    Private PageScript As String = ""
    Protected Sub Page_LoadComplete(sender As Object, e As System.EventArgs) Handles Me.LoadComplete
        StoreJQuery()
    End Sub

    Private Sub checkComplete()
        '---------------ข้อ 1 -----------------
        If ddl_Dept.SelectedIndex <> 0 And ddl_Sub_Dept.SelectedIndex <> 0 Then
            box_timeLine_1.Attributes("class") = "timeline-lightblue"
            timeLine_1.Attributes("class") = "timeline-icon-green"
        Else
            box_timeLine_1.Attributes("class") = "timeline-pink"
            timeLine_1.Attributes("class") = "timeline-icon-red"
        End If


        '---------------ข้อ 2 -----------------
        If txt_Item_Name.Text <> "" Then
            box_timeLine_2.Attributes("class") = "timeline-lightblue"
            timeLine_2.Attributes("class") = "timeline-icon-green"
        Else
            box_timeLine_2.Attributes("class") = "timeline-pink"
            timeLine_2.Attributes("class") = "timeline-icon-red"
        End If

        '---------------ข้อ 3 -----------------
        If S_ID <> 0 And S_Type_ID >= 0 Then
            box_timeLine_3.Attributes("class") = "timeline-lightblue"
            timeLine_3.Attributes("class") = "timeline-icon-green"
        Else
            box_timeLine_3.Attributes("class") = "timeline-pink"
            timeLine_3.Attributes("class") = "timeline-icon-red"
        End If
        '---------------ข้อ 4 -----------------
       
        '----------------ประเมินยังไม่ครบ ไฮไลท์สีแดง----------------------------

        Dim AT As DataTable = AssessmentData()
        Dim ckAss As String = ""
        If AT.Rows.Count > 0 Then
            For i As Integer = 0 To AT.Rows.Count - 1
                If AT.Rows(i).Item("Get_Score") = 0 And AT.Rows(i).Item("ckConfirm") = False Then
                    box_timeLine_4.Attributes("class") = "timeline-pink"
                    timeLine_4.Attributes("class") = "timeline-icon-red"

                    Exit Sub
                End If
            Next

            box_timeLine_4.Attributes("class") = "timeline-lightblue"
            timeLine_4.Attributes("class") = "timeline-icon-green"

        End If



    End Sub

    Private Sub StoreJQuery()
        PageScript &= jQuerySlider() & vbLf
        If txt_Item_No.Enabled Then
            PageScript &= jQueryItemNo() & vbLf
        End If

        PageScript &= jQueryWorkflowButton() & vbLf
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "StoreJQuery", PageScript, True)
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As System.EventArgs) Handles btnBack1.Click, btnBack2.Click
        Response.Redirect("Assessment_List.aspx?Back=Y")
    End Sub

#Region "Dept"
    Private Sub BindDept()
        Dim DT As DataTable = BL.GetAssessmentHeader(Ass_ID)
        BL.BindDDlDEPT(ddl_Dept, User_ID, DT.Rows(0).Item("Dept_ID"))

        '------------- Set Enabling--------------
        If Current_Step = AVLBL.AssessmentStep.Completed Then
            ddl_Dept.Enabled = False
        Else
            ddl_Dept.Enabled = AssessmentRole = Current_Step
        End If

        lblDept.Text = ddl_Dept.SelectedItem.ToString()
    End Sub

    Private Sub BindSubDept()
        Dim DT As DataTable = BL.GetAssessmentHeader(Ass_ID)
        BL.BindDDlSub_Dept(ddl_Sub_Dept, Dept_ID, User_ID, Current_Step, DT.Rows(0).Item("Sub_Dept_ID").ToString())

        '------------- Set Enabling--------------
        If Current_Step = AVLBL.AssessmentStep.Completed Then
            ddl_Sub_Dept.Enabled = False
        Else
            ddl_Sub_Dept.Enabled = AssessmentRole = Current_Step
        End If
        lblSubDept.Text = ddl_Sub_Dept.SelectedItem.ToString()
    End Sub
    Private Sub SaveDept()
        Dim SQL As String = "SELECT * FROM tb_Ass_Header WHERE Ass_ID=" & Ass_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        DT.Rows(0).Item("Dept_ID") = Dept_ID
        DT.Rows(0).Item("Item_Type") = DBNull.Value
        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)
        checkComplete()
    End Sub

    Private Sub SaveSubDept()
        Dim SQL As String = "SELECT * FROM tb_Ass_Header WHERE Ass_ID=" & Ass_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        DT.Rows(0).Item("Sub_Dept_ID") = Sub_Dept_ID
        DT.Rows(0).Item("Item_Type") = DBNull.Value
        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)
        checkComplete()
    End Sub

    Protected Sub ddl_Dept_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddl_Dept.SelectedIndexChanged

        '-------------เชคผู้ขายรายนี้ถูกประเมินซ้ำ สินค้า  ผู้ขาย ฝ่าย หรือไม่----------------------
        Dim SRole As Boolean = BL.CanSelect_Sup_Type_SubDept_Assessment(Ass_ID, ddl_Dept.SelectedValue, Sub_Dept_ID, S_ID, Running_ID, "Dept_ID", Dept_ID)
        If SRole Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','มีการประเมินรายการชุดนี้แล้ว<br>ต้องประเมินทบทวนสินค้า/พัสดุครุภัณฑ์','');", True)
            BindDept()
            Exit Sub
        End If

        SaveDept() ' Manual Save
        BindDept()
        BindSubDept()
        BindProductType()
    End Sub

    Protected Sub ddl_Sub_Dept_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddl_Sub_Dept.SelectedIndexChanged

        '-------------เชคผู้ขายรายนี้ถูกประเมินซ้ำ สินค้า  ผู้ขาย ฝ่าย หรือไม่----------------------
        Dim SRole As Boolean = BL.CanSelect_Sup_Type_SubDept_Assessment(Ass_ID, Dept_ID, ddl_Sub_Dept.SelectedValue, S_ID, Running_ID, "Dept_ID", Sub_Dept_ID)
        If SRole Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','มีการประเมินรายการชุดนี้แล้ว<br>ต้องประเมินทบทวนสินค้า/พัสดุครุภัณฑ์','');", True)
            BindSubDept()
            Exit Sub
        End If
        SaveSubDept()  ' Manual Save
        BindSubDept()
    End Sub

#End Region

#Region "Product Type"
    Private Sub BindProductType()

        Dim DT As DataTable = BL.GetProductTypeByAssessment(Ass_ID)
        If DT.Rows.Count = 0 OrElse IsDBNull(DT.Rows(0).Item("Type_ID")) Then
            Cat_Group_ID = 0
            Cat_Name = ""
            Sub_Cat_Name = ""
            Type_Name = ""
            Type_ID = 0 '---------- Main Query -----------
            Running_ID = 0
            Running_Name = ""
            Item_Type_Name = ""
            txt_Item_Name.Text = ""
        Else
            'Start----เพิ่มปุ่มบันทึก----
            If (Running_ID = 0) Then   '---------If (Type_ID = 0 Or Type_ID = DT.Rows(0).Item("Type_ID")) Then 
                '-----------ดึงจากใบประเมิน กรณี Auto Save---------------
                Cat_Group_ID = DT.Rows(0).Item("CG_ID").ToString
                Cat_Name = DT.Rows(0).Item("Cat_Name").ToString
                Sub_Cat_Name = DT.Rows(0).Item("Sub_Cat_Name").ToString
                Type_Name = DT.Rows(0).Item("Type_Name").ToString

                Type_ID = DT.Rows(0).Item("Type_ID") '---------- Main Query -----------
                Item_Type_Name = DT.Rows(0).Item("Cat_No").ToString & DT.Rows(0).Item("Sub_Cat_No").ToString & DT.Rows(0).Item("Type_No").ToString & DT.DefaultView(0).Item("Running_No").ToString & " : " & DT.DefaultView(0).Item("Running_Name").ToString
                txt_Item_Name.Text = DT.Rows(0).Item("Item_Name").ToString
                Running_ID = DT.Rows(0).Item("Running_ID").ToString
                Running_Name = DT.Rows(0).Item("Running_Name").ToString

            Else
                '-----------ข้อมูลรอ save จากข้อมูลสินค้า
                Dim DT_Running As DataTable = Session("Assessment_Edit_ProductTypeList")
                DT_Running.DefaultView.RowFilter = "Running_ID='" & Running_ID & "'"
                Cat_Group_ID = DT_Running.DefaultView(0).Item("CG_ID")
                Cat_Name = DT_Running.DefaultView(0).Item("Cat_Name").ToString
                Sub_Cat_Name = DT_Running.DefaultView(0).Item("Sub_Cat_Name").ToString
                Type_Name = DT_Running.DefaultView(0).Item("Type_Name").ToString
                'txt_Item_Name.Text = DT_Running.DefaultView(0).Item("Running_Name").ToString
                Type_ID = DT_Running.DefaultView(0).Item("Type_ID") '---------- Main Query -----------
                Item_Type_Name = DT_Running.DefaultView(0).Item("Cat_No").ToString & DT_Running.DefaultView(0).Item("Sub_Cat_No").ToString & DT_Running.DefaultView(0).Item("Type_No").ToString & DT_Running.DefaultView(0).Item("Running_No").ToString & " : " & DT_Running.DefaultView(0).Item("Running_Name").ToString

                Running_ID = DT_Running.DefaultView(0).Item("Running_ID").ToString
                Running_Name = DT_Running.DefaultView(0).Item("Running_Name").ToString

            End If
            'End----เพิ่มปุ่มบันทึก----

        End If

        '--------------- Get Item Type Name -------------
        Dim CatGroup_Info As AVLBL.CatGroup_Info = BL.GetCatGroup_Info(Cat_Group_ID)
        lblTitle_Item_Name.Text = CatGroup_Info.CG_Name
        pnlItem_No.Visible = CatGroup_Info.Active_ALL
        Select Case Cat_Group_ID
            Case 0
                lblHeader_Item.Text = "สินค้า, พัสดุครุภัณฑ์ หรือ งานบริการจัดจ้าง"

            Case 2
                lblHeader_Item.Text = "งานบริการจัดจ้าง"

            Case Else
                lblHeader_Item.Text = "สินค้า, พัสดุครุภัณฑ์ที่ส่งมอบ"
        End Select
        Title_Item_Group = lblHeader_Item.Text

        'If DT.Rows.Count = 0 Then
        '    txt_Item_Name.Text = ""
        'Else
        '    txt_Item_Name.Text = DT.Rows(0).Item("Item_Name").ToString
        'End If

        '------------- Set Enabling--------------
        'If Current_Step = AVLBL.AssessmentStep.Completed Then
        '    btnProductDialog.Visible = False
        'Else
        '    btnProductDialog.Visible = AssessmentRole = Current_Step
        'End If


        If Current_Step = AVLBL.AssessmentStep.Creater Then
            btnProductDialog.Visible = AssessmentRole = Current_Step
        Else
            btnProductDialog.Visible = False
            lblValidate_Product.Visible = False
            lblValidate_Item.Visible = False
        End If


        txt_Item_Name.Enabled = btnProductDialog.Visible

    End Sub

    Private Sub SaveProductType()
        Dim SQL As String = "SELECT * FROM tb_Ass_Header WHERE Ass_ID=" & Ass_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        'DT.Rows(0).Item("Item_Type") = Type_ID
        DT.Rows(0).Item("Item_Type") = Running_ID
        DT.Rows(0).Item("Item_Name") = txt_Item_Name.Text

        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)
        checkComplete()
    End Sub

    Protected Sub txt_Item_Name_TextChanged(sender As Object, e As System.EventArgs) Handles txt_Item_Name.TextChanged
        'SaveProductType()
    End Sub

#End Region

#Region "Product Detail"
    Private Sub BindProductDetail()

        CL.ImplementJavaIntegerText(txt_Qty)
        CL.ImplementJavaMoneyText(txt_Price)



        Dim DT As DataTable = BL.GetAssessmentHeader(Ass_ID)
        If DT.Rows.Count = 0 Then
            txt_Item_No.Text = ""
            txt_Qty.Text = ""
            txt_UOM.Text = ""
            txt_Lot.Text = ""
            txt_Brand.Text = ""
            txt_Model.Text = ""
            txt_Color.Text = ""
            txt_Size.Text = ""
            txt_Price.Text = ""
            txt_Ref_Doc.Text = ""
            txt_RecievedDate.Text = ""
        Else
            '------------ Get List Of StockNo---------------------
            Dim IT As DataTable = BL.GetItemNoByAssessment(Ass_ID)
            txt_Item_No.Text = DT.Rows(0).Item("Item_No").ToString
            Dim ItemList As String = ""
            For i As Integer = 0 To IT.Rows.Count - 1
                Dim ItemNo As String = IT.Rows(i).Item("Item_No").ToString.Trim
                If ItemNo <> "" Then ItemList &= ItemNo & ","
            Next
            If ItemList <> "" Then txt_Item_No.Text = ItemList.Substring(0, ItemList.Length - 1)

            txt_Qty.Text = DT.Rows(0).Item("Qty").ToString
            txt_UOM.Text = DT.Rows(0).Item("UOM").ToString
            txt_Lot.Text = DT.Rows(0).Item("Lot").ToString
            txt_Brand.Text = DT.Rows(0).Item("Brand").ToString
            txt_Model.Text = DT.Rows(0).Item("Model").ToString
            txt_Color.Text = DT.Rows(0).Item("Color").ToString
            txt_Size.Text = DT.Rows(0).Item("Size").ToString
            If Not IsDBNull(DT.Rows(0).Item("Price")) Then
                txt_Price.Text = FormatNumber(DT.Rows(0).Item("Price"), 2)
            Else
                txt_Price.Text = ""
            End If
            txt_Ref_Doc.Text = DT.Rows(0).Item("Ref_Doc").ToString
            If Not IsDBNull(DT.Rows(0).Item("RecievedDate")) Then
                txt_RecievedDate.Text = GL.ReportProgrammingDate(DT.Rows(0).Item("RecievedDate"), txt_RecievedDate_CalendarExtender.Format)
            Else
                txt_RecievedDate.Text = ""
            End If
        End If

        '------------- Set Enabling--------------
        btnUpdateProductDetail.Visible = btnProductDialog.Visible
        txt_Qty.Enabled = btnProductDialog.Visible
        txt_UOM.Enabled = btnProductDialog.Visible
        txt_Lot.Enabled = btnProductDialog.Visible
        txt_Brand.Enabled = btnProductDialog.Visible
        txt_Model.Enabled = btnProductDialog.Visible
        txt_Color.Enabled = btnProductDialog.Visible
        txt_Size.Enabled = btnProductDialog.Visible
        txt_Price.Enabled = btnProductDialog.Visible
        txt_Ref_Doc.Enabled = btnProductDialog.Visible
        txt_RecievedDate.Enabled = btnProductDialog.Visible
        txt_RecievedDate_CalendarExtender.Enabled = btnProductDialog.Visible

        If Not btnProductDialog.Visible Then
            lblTagDesc.Visible = False
            txt_Item_No.Visible = False
            rptTag.DataSource = BL.GetItemNoByAssessment(Ass_ID)

            lblValidate_Product.Visible = False
            lblValidate_Item.Visible = False
        Else
            lblTagDesc.Visible = True
            txt_Item_No.Visible = True
            rptTag.DataSource = Nothing
        End If

        rptTag.DataBind()

    End Sub

    Private Sub SaveProductDetail()
        Dim Conn As New SqlConnection(BL.ConnectionString)
        Conn.Open()

        Dim SQL As String = "SELECT * FROM tb_Ass_Header WHERE Ass_ID=" & Ass_ID
        Dim DA As New SqlDataAdapter(SQL, Conn)
        Dim DT As New DataTable
        DA.Fill(DT)

        DT.Rows(0).Item("Item_No") = txt_Item_No.Text
        DT.Rows(0).Item("Qty") = txt_Qty.Text
        DT.Rows(0).Item("UOM") = txt_UOM.Text
        DT.Rows(0).Item("Lot") = txt_Lot.Text
        DT.Rows(0).Item("Brand") = txt_Brand.Text
        DT.Rows(0).Item("Model") = txt_Model.Text
        DT.Rows(0).Item("Color") = txt_Color.Text
        DT.Rows(0).Item("Size") = txt_Size.Text
        If IsNumeric(txt_Price.Text) Then
            DT.Rows(0).Item("Price") = CDbl(txt_Price.Text.Replace(",", ""))
        Else
            DT.Rows(0).Item("Price") = DBNull.Value
        End If

        DT.Rows(0).Item("Ref_Doc") = txt_Ref_Doc.Text

        If GL.IsProgrammingDate(txt_RecievedDate.Text, txt_RecievedDate_CalendarExtender.Format) Then
            Dim C As New Converter
            DT.Rows(0).Item("RecievedDate") = C.StringToDate(txt_RecievedDate.Text, txt_RecievedDate_CalendarExtender.Format)
        Else
            DT.Rows(0).Item("RecievedDate") = DBNull.Value
        End If

        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)

        '-----------Split Item_No --------------

        Dim Comm As New SqlCommand
        With Comm
            .CommandType = CommandType.Text
            .Connection = Conn
            .CommandText = "DELETE FROM tb_Item WHERE Ass_ID=" & Ass_ID
            .ExecuteNonQuery()
            .Dispose()
        End With

        Dim Item_No As String() = Split(txt_Item_No.Text, ",")
        SQL = "SELECT * FROM tb_Item WHERE 1=0"
        DT = New DataTable
        DA = New SqlDataAdapter(SQL, Conn)
        DA.Fill(DT)
        For i As Integer = 0 To Item_No.Length - 1
            If Trim(Item_No(i)) <> "" Then
                Dim DR As DataRow = DT.NewRow
                DR("Item_ID") = BL.GetNewPrimaryID("tb_Item", "Item_ID")
                DR("Ass_ID") = Ass_ID
                DR("Dept_Use_ID") = Dept_ID
                DR("Item_No") = Trim(Item_No(i))
                DR("Stock_No") = ""
                DR("Update_By") = User_ID
                DR("Update_Time") = Now
                DT.Rows.Add(DR)
                cmd = New SqlCommandBuilder(DA)
                DA.Update(DT)
                DT.AcceptChanges()
            End If
        Next
        'checkComplete()
    End Sub

    Protected Sub txt_ProductDetail_TextChanged(sender As Object, e As System.EventArgs) Handles txt_Qty.TextChanged, txt_UOM.TextChanged, txt_Lot.TextChanged _
                                                                                                , txt_Brand.TextChanged, txt_Model.TextChanged, txt_Color.TextChanged _
                                                                                                , txt_Size.TextChanged, txt_Price.TextChanged, txt_Ref_Doc.TextChanged, txt_RecievedDate.TextChanged _
                                                                                                , btnUpdateProductDetail.Click
        'SaveProductDetail()
        'BindProductDetail()
    End Sub

    Public Function jQueryItemNo() As String
        Dim Script As String = ""
        Script &= "$('#" & txt_Item_No.ClientID & "').tagsInput({" & vbLf
        Script &= "width:  'auto'," & vbLf
        Script &= "'onAddTag': function(){document.getElementById('" & btnUpdateProductDetail.ClientID & "').click();}," & vbLf
        Script &= "'onRemoveTag': function (){document.getElementById('" & btnUpdateProductDetail.ClientID & "').click();}" & vbLf
        Script &= "});" & vbLf
        Return Script
    End Function



#End Region

#Region "File"
    Private Sub BindFile()

        '------------- Set Enabling--------------
        If Current_Step = AVLBL.AssessmentStep.Completed Then
            btnAddFile.Visible = False
        Else
            btnAddFile.Visible = AssessmentRole = Current_Step
        End If

        Dim DT As DataTable = BL.GetFileByAssessment(Ass_ID)
        rpt_File.DataSource = DT
        rpt_File.DataBind()
    End Sub

    Protected Sub rpt_File_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rpt_File.ItemCommand
        '------------- Set Enabling--------------
        If Not btnAddFile.Visible Then Exit Sub

        Select Case e.CommandName
            Case "Default"
                Dim Comm As New SqlCommand
                Dim Conn As New SqlConnection(BL.ConnectionString)
                Conn.Open()
                With Comm
                    .Connection = Conn
                    .CommandType = CommandType.Text
                    .CommandText = "UPDATE tb_Ass_File SET IsDefault=CASE File_ID WHEN " & e.CommandArgument & " THEN 1 ELSE 0 END WHERE Ass_ID=" & Ass_ID
                    .ExecuteNonQuery()
                    .Dispose()
                End With
                Conn.Close()
                Conn.Dispose()

                'For Each Item As RepeaterItem In rpt_File.Items
                '    If Item.ItemType <> ListItemType.Item And Item.ItemType <> ListItemType.AlternatingItem Then Continue For
                '    Dim imgDefault As HtmlGenericControl = e.Item.FindControl("imgDefault")
                '    imgDefault.Visible = Equals(Item, e.Item)
                'Next
                BindFile()

            Case "Edit"

                dialogUploadFile.Visible = True
                dialogFileContainer.Style("width") = "600px"
                txtFileName.CssClass = "m-wrap span10"
                btnSaveFile.CssClass = "btn blue span2"
                pnlAddFile.Visible = False
                pnlEditFile.Visible = True

                Dim lbl_File_Name As Label = e.Item.FindControl("lbl_File_Name")

                Dim F As AVLBL.FileStructure = BL.GetFileStructure(Ass_ID, e.CommandArgument, Server.MapPath(""))
                Session("Assessment_Preview_File_Old") = F
                Session("Assessment_Preview_File_New") = Nothing

                fileOld.ImageUrl = "RenderFile.aspx?A=" & Ass_ID & "&F=" & e.CommandArgument & "&T=" & Now.ToOADate.ToString.Replace(".", "")
                fileNew.ImageUrl = "images/BlackDot.png"
                txtFileName.Text = F.FileName
                File_ID = e.CommandArgument

            Case "Delete"
                Dim Comm As New SqlCommand
                Dim Conn As New SqlConnection(BL.ConnectionString)
                Conn.Open()
                With Comm
                    .Connection = Conn
                    .CommandType = CommandType.Text
                    .CommandText = "Delete From tb_Ass_File WHERE Ass_ID=" & Ass_ID & " AND File_ID=" & e.CommandArgument
                    .ExecuteNonQuery()
                    .Dispose()
                End With
                Conn.Close()
                Conn.Dispose()

                BindFile()
        End Select

    End Sub

    Protected Sub rpt_File_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_File.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lnk_File_Dialog As HtmlAnchor = e.Item.FindControl("lnk_File_Dialog")
        Dim img_File As Image = e.Item.FindControl("img_File")
        Dim lbl_File_Name As Label = e.Item.FindControl("lbl_File_Name")
        Dim imgDefault As HtmlGenericControl = e.Item.FindControl("imgDefault")

        Dim pnlEditFile As HtmlGenericControl = e.Item.FindControl("pnlEditFile")
        Dim lnk_Default As LinkButton = e.Item.FindControl("lnk_Default")
        Dim lnk_Edit As LinkButton = e.Item.FindControl("lnk_Edit")
        Dim lnk_Delete As LinkButton = e.Item.FindControl("lnk_Delete")

        img_File.ImageUrl = "RenderFile.aspx?A=" & Ass_ID & "&F=" & e.Item.DataItem("File_ID") & "&T=" & Now.ToOADate.ToString.Replace(".", "")
        lbl_File_Name.Text = e.Item.DataItem("File_Name").ToString
        lnk_File_Dialog.HRef = img_File.ImageUrl
        imgDefault.Visible = Not IsDBNull(e.Item.DataItem("IsDefault")) AndAlso e.Item.DataItem("IsDefault")

        
        If GL.IsContentTypeImage(e.Item.DataItem("Content_Type").ToString) Then
            lnk_File_Dialog.Attributes("Title") = "Photo"
        Else
            img_File.ImageUrl = "images/file_type/View_pdf.png"

            lnk_File_Dialog.Attributes("Title") = "PDF"


        End If

        lnk_Default.CommandArgument = e.Item.DataItem("File_ID")
        lnk_Edit.CommandArgument = e.Item.DataItem("File_ID")
        lnk_Delete.CommandArgument = e.Item.DataItem("File_ID")

        '------- Set Enabled ---------
        Dim lnk_Space As LinkButton = e.Item.FindControl("lnk_Space")

        If btnAddFile.Visible Then
            lnk_Default.Visible = True
            lnk_Edit.Visible = True
            lnk_Delete.Visible = True
            lnk_Space.Visible = False
        Else
            lnk_Default.Visible = False
            lnk_Edit.Visible = False
            lnk_Delete.Visible = False
            lnk_Space.Visible = True
            pnlEditFile.Style("background-color") = "inherit !important"
        End If

    End Sub

    Protected Sub btnAddFile_Click(sender As Object, e As System.EventArgs) Handles btnAddFile.Click

        dialogUploadFile.Visible = True
        dialogFileContainer.Style("width") = "400px"
        txtFileName.CssClass = "m-wrap span9"
        btnSaveFile.CssClass = "btn blue span3"
        pnlAddFile.Visible = True
        pnlEditFile.Visible = False

        Session("Assessment_Preview_File_New") = Nothing
        Session("Assessment_Preview_File_Old") = Nothing

        fileAdd.ImageUrl = "images/BlackDot.png"
        txtFileName.Text = ""

        File_ID = 0
    End Sub

    Protected Sub lnkCloseUpload_Click(sender As Object, e As System.EventArgs) Handles lnkCloseUpload.Click
        dialogUploadFile.Visible = False
    End Sub

    Protected Sub btnUpdateFile_Click(sender As Object, e As System.EventArgs) Handles btnUpdateFile.Click
        Select Case True
            Case pnlAddFile.Visible
                Dim UploadedFile As AVLBL.FileStructure = Session("Assessment_Preview_File_New")
                fileAdd.ImageUrl = "RenderFile.aspx?T=" & Now.ToOADate.ToString.Replace(".", "") & "&S=Assessment_Preview_File_New&Mode=Thumbnail"
                txtFileName.Text = UploadedFile.FileName
            Case pnlEditFile.Visible
                Dim UploadedFile As AVLBL.FileStructure = Session("Assessment_Preview_File_New")
                fileNew.ImageUrl = "RenderFile.aspx?T=" & Now.ToOADate.ToString.Replace(".", "") & "&S=Assessment_Preview_File_New&Mode=Thumbnail"
                txtFileName.Text = UploadedFile.FileName
        End Select
        PageScript &= "$('#" & txtFileName.ClientID & "').select();" & vbLf
    End Sub

    Protected Sub btnSaveFile_Click(sender As Object, e As System.EventArgs) Handles btnSaveFile.Click

        Dim FU As AVLBL.FileStructure = Session("Assessment_Preview_File_New")

        If IsNothing(FU) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Upload", "ShowAlert('warning','เลือกไฟล์ภาพที่ Uplaod','');", True)
            Exit Sub
        End If

        If IsNothing(FU.FileContent) OrElse FU.FileContent.Length = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Upload", "ShowAlert('warning','เลือกไฟล์ภาพที่ Uplaod','');", True)
            Exit Sub
        End If

        '--------------- Create File Path --------------
        Dim Path As String = Server.MapPath("FileUpload/Ass/" & Ass_ID)
        If Not Directory.Exists(Path) Then
            Directory.CreateDirectory(Path)
        End If


        '--------------- Calculate To Save---------------
        Dim DT As New DataTable
        Dim SQL As String = "SELECT * FROM tb_Ass_File WHERE Ass_ID=" & Ass_ID & " AND File_ID=" & File_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        DA.Fill(DT)

        Dim UpdateOnlyOne As Boolean = False
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            DT.Rows.Add(DR)
            File_ID = GetNewFileID()
        Else
            DR = DT.Rows(0)
            UpdateOnlyOne = True
        End If

        '--------------- Save To File-------------------
        Path &= "/" & File_ID
        If File.Exists(Path) Then File.Delete(Path)
        Dim f As FileStream = File.Open(Path, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite)
        f.Write(FU.FileContent, 0, FU.FileContent.LongLength)
        f.Close()

        '--------------- Save To Database---------------
        DR("Ass_ID") = Ass_ID
        DR("File_ID") = File_ID
        DR("File_Name") = txtFileName.Text
        DR("Content_Type") = FU.ContentType
        DR("File_Size") = FU.FileContent.LongLength
        DR("Update_By") = User_ID
        DR("Update_Time") = Now
        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)

        '----------------- Hide Dialog -----------------
        dialogUploadFile.Visible = False

        'If UpdateOnlyOne Then
        '    For Each Item As RepeaterItem In rpt_File.Items
        '        If Item.ItemType <> ListItemType.Item And Item.ItemType <> ListItemType.AlternatingItem Then Continue For

        '        Dim lnk_Default As LinkButton = Item.FindControl("lnk_Default")
        '        If lnk_Default.CommandArgument <> File_ID Then Continue For
        '        Dim lnk_File_Dialog As HtmlAnchor = Item.FindControl("lnk_File_Dialog")
        '        Dim img_File As Image = Item.FindControl("img_File")
        '        Dim lbl_File_Name As Label = Item.FindControl("lbl_File_Name")

        '        Dim pnlDetail As HtmlGenericControl = Item.FindControl("pnlDetail")
        '        img_File.ImageUrl = "RenderFile.aspx?A=" & Ass_ID & "&F=" & File_ID & "&T=" & Now.ToOADate.ToString.Replace(".", "") & ""
        '        lbl_File_Name.Text = txtFileName.Text
        '        lnk_File_Dialog.HRef = img_File.ImageUrl

        '        If GL.IsContentTypeImage(FU.ContentType) Then
        '            lnk_File_Dialog.Attributes("Title") = "Photo"
        '        Else
        '            lnk_File_Dialog.Attributes("Title") = "PDF"
        '        End If
        '        Exit For
        '    Next
        'Else
        BindFile()
        'End If


    End Sub

    Private Function GetNewFileID() As Integer
        Dim SQL As String = "SELECT ISNULL(MAX(File_ID),0)+1 FROM tb_Ass_File WHERE Ass_ID=" & Ass_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0)(0)
    End Function

#End Region

#Region "Supplier"
    Private Sub BindSupplier()
        Dim DT As DataTable = BL.GetAssessmentHeader(Ass_ID)
        If DT.Rows.Count = 1 Then
            If (S_ID = 0) And Not IsDBNull(DT.Rows(0).Item("S_ID")) Then '-------เข้ามาครั้งแรก  
                S_ID = DT.Rows(0).Item("S_ID")
                lbl_S_Fullname.Text = DT.Rows(0).Item("S_Name").ToString
                BL.BindDDlSupplierType(ddl_S_Type, DT.Rows(0).Item("S_Type_ID"))
                lbl_S_Alias.Text = DT.Rows(0).Item("S_Alias").ToString
                lbl_S_Tax_No.Text = DT.Rows(0).Item("S_Tax_No").ToString
                lbl_S_Address.Text = DT.Rows(0).Item("S_Address").ToString
                txt_S_Contact_Name.Text = DT.Rows(0).Item("S_Contact_Name").ToString
                txt_S_Email.Text = DT.Rows(0).Item("S_Email").ToString
                lbl_S_Phone.Text = DT.Rows(0).Item("S_Phone").ToString
                lbl_S_Fax.Text = DT.Rows(0).Item("S_Fax").ToString
            ElseIf (S_ID <> 0) Then
                '-----------ข้อมูลรอ save จากข้อมูลสินค้า
                Dim DT_S As DataTable = Session("Assessment_Edit_SupplierList")
                DT_S.DefaultView.RowFilter = "S_ID='" & S_ID & "'"
                S_ID = DT_S.DefaultView(0).Item("S_ID")
                lbl_S_Fullname.Text = DT_S.DefaultView(0).Item("S_Name").ToString
                BL.BindDDlSupplierType(ddl_S_Type, Session("_Sessionddl_S_Type").ToString)
                lbl_S_Alias.Text = DT_S.DefaultView(0).Item("S_Alias").ToString
                lbl_S_Tax_No.Text = DT_S.DefaultView(0).Item("S_Tax_No").ToString
                lbl_S_Address.Text = DT_S.DefaultView(0).Item("S_Address").ToString
                txt_S_Contact_Name.Text = DT_S.DefaultView(0).Item("S_Contact_Name").ToString
                txt_S_Email.Text = DT_S.DefaultView(0).Item("S_Email").ToString
                lbl_S_Phone.Text = DT_S.DefaultView(0).Item("S_Phone").ToString
                lbl_S_Fax.Text = DT_S.DefaultView(0).Item("S_Fax").ToString
            Else
                S_ID = 0
                lbl_S_Fullname.Text = ""
                BL.BindDDlSupplierType(ddl_S_Type, Session("_Sessionddl_S_Type").ToString)
                lbl_S_Alias.Text = ""
                lbl_S_Tax_No.Text = ""
                lbl_S_Address.Text = ""
                txt_S_Contact_Name.Text = ""
                txt_S_Email.Text = ""
                lbl_S_Phone.Text = ""
                lbl_S_Fax.Text = ""
            End If


        End If


        'If DT.Rows.Count = 0 Or IsDBNull(DT.Rows(0).Item("S_ID")) Then
        '    S_ID = 0
        '    lbl_S_Fullname.Text = ""
        '    BL.BindDDlSupplierType(ddl_S_Type)
        '    lbl_S_Alias.Text = ""
        '    lbl_S_Tax_No.Text = ""
        '    lbl_S_Address.Text = ""
        '    txt_S_Contact_Name.Text = ""
        '    txt_S_Email.Text = ""
        '    lbl_S_Phone.Text = ""
        '    lbl_S_Fax.Text = ""
        'Else
        '    'Start----เพิ่มปุ่มบันทึก----
        '    If (S_ID = 0) Then

        '        lbl_S_Fullname.Text = DT.Rows(0).Item("S_Name").ToString
        '        BL.BindDDlSupplierType(ddl_S_Type, DT.Rows(0).Item("S_Type_ID"))
        '        lbl_S_Alias.Text = DT.Rows(0).Item("S_Alias").ToString
        '        lbl_S_Tax_No.Text = DT.Rows(0).Item("S_Tax_No").ToString
        '        lbl_S_Address.Text = DT.Rows(0).Item("S_Address").ToString
        '        txt_S_Contact_Name.Text = DT.Rows(0).Item("S_Contact_Name").ToString
        '        txt_S_Email.Text = DT.Rows(0).Item("S_Email").ToString
        '        lbl_S_Phone.Text = DT.Rows(0).Item("S_Phone").ToString
        '        lbl_S_Fax.Text = DT.Rows(0).Item("S_Fax").ToString

        '    Else
        '        '-----------ข้อมูลรอ save จากข้อมูลสินค้า
        '        Dim DT_S As DataTable = Session("Assessment_Edit_SupplierList")
        '        DT_S.DefaultView.RowFilter = "S_ID='" & S_ID & "'"
        '        S_ID = DT_S.DefaultView(0).Item("S_ID")
        '        lbl_S_Fullname.Text = DT_S.DefaultView(0).Item("S_Name").ToString
        '        BL.BindDDlSupplierType(ddl_S_Type)
        '        lbl_S_Alias.Text = DT_S.DefaultView(0).Item("S_Alias").ToString
        '        lbl_S_Tax_No.Text = DT_S.DefaultView(0).Item("S_Tax_No").ToString
        '        lbl_S_Address.Text = DT_S.DefaultView(0).Item("S_Address").ToString
        '        txt_S_Contact_Name.Text = DT_S.DefaultView(0).Item("S_Contact_Name").ToString
        '        txt_S_Email.Text = DT_S.DefaultView(0).Item("S_Email").ToString
        '        lbl_S_Phone.Text = DT_S.DefaultView(0).Item("S_Phone").ToString
        '        lbl_S_Fax.Text = DT_S.DefaultView(0).Item("S_Fax").ToString

        '    End If
        'End----เพิ่มปุ่มบันทึก----



        'S_ID = DT.Rows(0).Item("S_ID")
        'lbl_S_Fullname.Text = DT.Rows(0).Item("S_Name").ToString
        'BL.BindDDlSupplierType(ddl_S_Type, DT.Rows(0).Item("S_Type_ID"))
        'lbl_S_Alias.Text = DT.Rows(0).Item("S_Alias").ToString
        'lbl_S_Tax_No.Text = DT.Rows(0).Item("S_Tax_No").ToString
        'lbl_S_Address.Text = DT.Rows(0).Item("S_Address").ToString
        'txt_S_Contact_Name.Text = DT.Rows(0).Item("S_Contact_Name").ToString
        'txt_S_Email.Text = DT.Rows(0).Item("S_Email").ToString
        'lbl_S_Phone.Text = DT.Rows(0).Item("S_Phone").ToString
        'lbl_S_Fax.Text = DT.Rows(0).Item("S_Fax").ToString

        'End If

        '------------- Set Enabling--------------
        'If Current_Step = AVLBL.AssessmentStep.Completed Then
        '    btnSupplierDialog.Visible = False
        'Else
        '    btnSupplierDialog.Visible = AssessmentRole = Current_Step
        'End If

        If Current_Step = AVLBL.AssessmentStep.Creater Then
            btnSupplierDialog.Visible = AssessmentRole = Current_Step
            lblValidate_Supplier.Visible = AssessmentRole = Current_Step
            lblValidate_SupType.Visible = AssessmentRole = Current_Step
        Else
            btnSupplierDialog.Visible = False
            lblValidate_Supplier.Visible = False
            lblValidate_SupType.Visible = False
        End If

        lbl_S_Fullname.Enabled = btnSupplierDialog.Visible
        ddl_S_Type.Enabled = btnSupplierDialog.Visible
        lbl_S_Alias.Enabled = btnSupplierDialog.Visible
        lbl_S_Address.Enabled = btnSupplierDialog.Visible
        txt_S_Contact_Name.Enabled = btnSupplierDialog.Visible
        txt_S_Email.Enabled = btnSupplierDialog.Visible
        lbl_S_Phone.Enabled = btnSupplierDialog.Visible
        lbl_S_Fax.Enabled = btnSupplierDialog.Visible

    End Sub

    Private Sub SaveSupplier()
        Dim SQL As String = "SELECT * FROM tb_Ass_Header WHERE Ass_ID=" & Ass_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        DT.Rows(0).Item("S_ID") = S_ID
        DT.Rows(0).Item("S_Name") = lbl_S_Fullname.Text
        DT.Rows(0).Item("S_Alias") = lbl_S_Alias.Text
        DT.Rows(0).Item("S_Tax_No") = lbl_S_Tax_No.Text
        DT.Rows(0).Item("S_Address") = lbl_S_Address.Text
        DT.Rows(0).Item("S_Contact_Name") = txt_S_Contact_Name.Text
        DT.Rows(0).Item("S_Email") = txt_S_Email.Text
        DT.Rows(0).Item("S_Phone") = lbl_S_Phone.Text
        DT.Rows(0).Item("S_Fax") = lbl_S_Fax.Text
        DT.Rows(0).Item("S_Type_ID") = S_Type_ID
        If ddl_S_Type.SelectedIndex > 0 Then
            DT.Rows(0).Item("S_Type_Name") = ddl_S_Type.SelectedItem
        Else
            DT.Rows(0).Item("S_Type_Name") = DBNull.Value
        End If

        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)
        checkComplete()
    End Sub

    Protected Sub txt_Supplier_Changed(sender As Object, e As System.EventArgs) Handles txt_S_Contact_Name.TextChanged, txt_S_Email.TextChanged, ddl_S_Type.SelectedIndexChanged
        'SaveSupplier()
        'BindSupplier()
        Session("_Sessionddl_S_Type") = ddl_S_Type.SelectedValue
    End Sub

#End Region

#Region "Assessment"
    Private Sub BindAssessment()

        '-------------- Bind Questionaire------------------
        Dim QG As DataTable
        Dim Col() As String = {"Q_GP_ID", "Q_GP_Name", "Q_GP_Order"}
        If Current_Step = AVLBL.AssessmentStep.NotFound Then
            QG = BL.GetQuestionsByCurrentSetting(AVLBL.AssessmentType.Assessment)
        Else
            If AssessmentData.Rows.Count = 0 Then
                QG = BL.GetQuestionsByAssessment(Ass_ID)
            Else
                QG = AssessmentData()
            End If
        End If

        TMPQ = QG.Copy
        rpt_Ass_Q_Group.DataSource = QG.DefaultView.ToTable(True, Col).Copy
        rpt_Ass_Q_Group.DataBind()


        '-------------- Bind Result------------------
        lbl_Get_Score.Text = ""
        lbl_Max_Score.Text = ""
        lbl_Pass_Score.Text = ""
        knob.Text = ""


        '---START---------ปรับตรวจสอบแต่ละข้อ มีบางข้อไม่ผ่านเกณฑ์๔ือว่าไม่ผ่าน-------------
        'Dim IsPass_Status As Boolean = True
        IsPass_Status = True
        For i As Integer = 0 To TMPQ.Rows.Count - 1
            If (TMPQ.Rows(i).Item("Get_Score") < TMPQ.Rows(i).Item("Pass_Score")) Then
                IsPass_Status = False
                Exit For
            End If
        Next
        '---END---------ปรับตรวจสอบแต่ละข้อ มีบางข้อไม่ผ่านเกณฑ์๔ือว่าไม่ผ่าน-------------


        Dim Get_Score As Object = TMPQ.Compute("SUM(Get_Score)", "")
        Dim Pass_Score As Object = TMPQ.Compute("SUM(Pass_Score)", "")
        Dim Max_Score As Object = TMPQ.Compute("SUM(Max_Score)", "")
        knob.Attributes("data-fgcolor") = "silver"

        If Not IsDBNull(Get_Score) Then
            lbl_Get_Score.Text = Get_Score
            knob.Text = Get_Score
            If Not IsDBNull(Pass_Score) Then
                'If Get_Score >= Pass_Score Then  '------เดิม เปรียบเทียบคะแนนประเมินและเกณฑ์รวมเท่านั้น
                If IsPass_Status And Get_Score >= Pass_Score Then
                    span_Get_Score.Attributes("class") = "label label-success"
                    lbl_Result.Text = "ผ่านการประเมิน"
                    lbl_Result.ForeColor = Drawing.Color.Green
                    knob.Attributes("data-fgcolor") = "green"
                Else
                    span_Get_Score.Attributes("class") = "label label-important"
                    lbl_Result.Text = "ไม่ผ่านการประเมิน"
                    lbl_Result.ForeColor = Drawing.Color.Red
                    knob.Attributes("data-fgcolor") = "red"
                End If
            Else
                lbl_Result.Text = "ไม่สามารถประเมินผลได้"
                lbl_Result.ForeColor = Drawing.Color.Silver
                span_Get_Score.Attributes("class") = "label label-important"
            End If
        End If
        If Not IsDBNull(Pass_Score) Then
            lbl_Pass_Score.Text = Pass_Score
        End If
        If Not IsDBNull(Max_Score) Then
            lbl_Max_Score.Text = Max_Score
            knob.Attributes("data-max") = Max_Score
        End If

        '------------- Set Enabling--------------
        If Current_Step = AVLBL.AssessmentStep.Completed Then
            pnlMaskAssessment.Visible = True
            lblValidate_Ass.Visible = False
        Else
            pnlMaskAssessment.Visible = AssessmentRole <> Current_Step
            lblValidate_Ass.Visible = AssessmentRole = Current_Step
        End If

    End Sub

    Private Sub SaveAssessment()
        Dim AT As DataTable = AssessmentData()


        '----------------- Save Detail --------------
        Dim SQL As String = "SELECT * FROM tb_Ass_Detail WHERE Ass_ID=" & Ass_ID
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        DA.Fill(DT)
        For i As Integer = 0 To DT.Rows.Count - 1
            AT.DefaultView.RowFilter = "Q_ID=" & DT.Rows(i).Item("Q_ID")
            If AT.DefaultView.Count > 0 Then
                DT.Rows(i).Item("Get_Score") = AT.DefaultView(0).Item("Get_Score")
                If (AT.DefaultView(0).Item("Get_Score") = 0) Then
                    DT.Rows(i).Item("ckConfirm") = AT.DefaultView(0).Item("ckConfirm")
                Else
                    DT.Rows(i).Item("ckConfirm") = False
                End If
            End If
        Next
        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)
        '---------- Update Header Summary Score--------
        BL.UpdateHeaderScore_Assessment(Ass_ID)
        cmd.Dispose()
        DA.Dispose()
        DT.Dispose()

        checkComplete()
    End Sub

    Public Function AssessmentData() As DataTable

        Dim DT As New DataTable
        DT.Columns.Add("Ass_ID", GetType(Integer))
        DT.Columns.Add("Q_GP_ID", GetType(Integer))
        DT.Columns.Add("Q_ID", GetType(Integer))
        DT.Columns.Add("Q_GP_Order", GetType(Integer))
        DT.Columns.Add("Q_Order", GetType(Integer))
        DT.Columns.Add("Q_GP_Name")
        DT.Columns.Add("Q_Name")
        DT.Columns.Add("Max_Score", GetType(Integer))
        DT.Columns.Add("Pass_Score", GetType(Integer))
        DT.Columns.Add("Get_Score", GetType(Integer))
        DT.Columns.Add("Q_Comment")
        DT.Columns.Add("AR_ID", GetType(Integer))
        DT.Columns.Add("ckConfirm", GetType(Boolean))


        For Each GP As RepeaterItem In rpt_Ass_Q_Group.Items
            If GP.ItemType <> ListItemType.AlternatingItem And GP.ItemType <> ListItemType.Item Then Continue For
            Dim lbl_Q_GP_Name As Label = GP.FindControl("lbl_Q_GP_Name")
            Dim Q_GP_ID As Integer = lbl_Q_GP_Name.Attributes("Q_GP_ID")
            Dim Q_GP_Order As Integer = lbl_Q_GP_Name.Attributes("Q_GP_Order")
            Dim AR_ID As Integer = lbl_Q_GP_Name.Attributes("AR_ID")
            Dim rpt_Ass_Q As Repeater = GP.FindControl("rpt_Ass_Q")

            For Each Q As RepeaterItem In rpt_Ass_Q.Items
                If Q.ItemType <> ListItemType.AlternatingItem And Q.ItemType <> ListItemType.Item Then Continue For
                Dim lbl_Q_Name As Label = Q.FindControl("lbl_Q_Name")
                Dim lbl_Score As Label = Q.FindControl("lbl_Score")
                Dim txt_Score As TextBox = Q.FindControl("txt_Score")
                Dim ckConfirm As CheckBox = Q.FindControl("ckConfirm")

                Dim DR As DataRow = DT.NewRow
                DR("Ass_ID") = Ass_ID
                DR("Q_GP_ID") = Q_GP_ID
                DR("Q_ID") = lbl_Q_Name.Attributes("Q_ID")
                DR("Q_GP_Order") = Q_GP_Order
                DR("Q_Order") = lbl_Q_Name.Attributes("Q_Order")
                DR("Q_GP_Name") = lbl_Q_GP_Name.Text
                DR("Q_Name") = lbl_Q_Name.Text
                DR("Max_Score") = txt_Score.Attributes("Max_Score")
                DR("Pass_Score") = txt_Score.Attributes("Pass_Score")
                DR("Get_Score") = txt_Score.Text
                DR("Q_Comment") = ""
                DR("AR_ID") = lbl_Q_Name.Attributes("AR_ID")
                DR("ckConfirm") = ckConfirm.Checked

                DT.Rows.Add(DR)
            Next
            'For Each Q As RepeaterItem In 
        Next
        Return DT
    End Function

    Dim TMPQ As DataTable
    Protected Sub rpt_Ass_Q_Group_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_Ass_Q_Group.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lbl_Q_GP_No As Label = e.Item.FindControl("lbl_Q_GP_No")
        Dim lbl_Q_GP_Name As Label = e.Item.FindControl("lbl_Q_GP_Name")
        Dim rpt_Ass_Q As Repeater = e.Item.FindControl("rpt_Ass_Q")
        Dim hr As HtmlGenericControl = e.Item.FindControl("hr")

        hr.Visible = e.Item.ItemIndex > 0

        lbl_Q_GP_No.Text = (e.Item.ItemIndex + 1) & ". "
        lbl_Q_GP_Name.Text = e.Item.DataItem("Q_GP_Name").ToString
        lbl_Q_GP_Name.Attributes("Q_GP_ID") = e.Item.DataItem("Q_GP_ID")
        lbl_Q_GP_Name.Attributes("Q_GP_Order") = e.Item.DataItem("Q_GP_Order")

        TMPQ.DefaultView.RowFilter = "Q_GP_ID=" & e.Item.DataItem("Q_GP_ID")
        Ass_Count = TMPQ.DefaultView.Count
        AddHandler rpt_Ass_Q.ItemDataBound, AddressOf rpt_Ass_Q_ItemDataBound
        rpt_Ass_Q.DataSource = TMPQ.DefaultView.ToTable
        rpt_Ass_Q.DataBind()

    End Sub

    Dim Ass_Count As Integer = 0
    Protected Sub rpt_Ass_Q_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs)
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lbl_Q_Name As Label = e.Item.FindControl("lbl_Q_Name")
        Dim lbl_Q_Name_Order As Label = e.Item.FindControl("lbl_Q_Name_Order")
        Dim lbl_Score As Label = e.Item.FindControl("lbl_Score")
        Dim txt_Score As TextBox = e.Item.FindControl("txt_Score")
        Dim slider As HtmlGenericControl = e.Item.FindControl("slider")
        Dim lblReqField As Label = e.Item.FindControl("lblReqField")
        Dim ckConfirm As CheckBox = e.Item.FindControl("ckConfirm")
        Dim pnlSlider As Panel = e.Item.FindControl("pnlSlider")
        Dim pnlck As Panel = e.Item.FindControl("pnlck")
        Dim lbl_Pass_Score As Label = e.Item.FindControl("lbl_Pass_Score")
        If Current_Step <> AVLBL.AssessmentStep.Completed Then
            Select Case e.Item.DataItem("AR_ID").ToString '----------- สถานะใบประเมิน ----------
                Case AVLBL.AssessmentStep.Creater
                    lblReqField.Visible = AssessmentRole = AVLBL.AssessmentRole.Creater
                    pnlck.Visible = AssessmentRole = AVLBL.AssessmentRole.Creater And (e.Item.DataItem("Get_Score") = 0)
                Case (AVLBL.AssessmentStep.Auditor)
                    lblReqField.Visible = AssessmentRole = AVLBL.AssessmentRole.Auditor
                    pnlck.Visible = AssessmentRole = AVLBL.AssessmentRole.Auditor And (e.Item.DataItem("Get_Score") = 0)
                Case AVLBL.AssessmentStep.Supply_Officer
                    lblReqField.Visible = AssessmentRole = AVLBL.AssessmentRole.Supply_Officer
                    pnlck.Visible = AssessmentRole = AVLBL.AssessmentRole.Supply_Officer And (e.Item.DataItem("Get_Score") = 0)
                Case AVLBL.AssessmentStep.Director
                    lblReqField.Visible = AssessmentRole = AVLBL.AssessmentRole.Director
                    pnlck.Visible = AssessmentRole = AVLBL.AssessmentRole.Director And (e.Item.DataItem("Get_Score") = 0)
                Case AVLBL.AssessmentStep.Completed

            End Select
            If AssessmentRole = Current_Step Then
                If lblReqField.Visible Then
                    slider.Attributes("class") = "slider bg-green"
                Else
                    slider.Attributes("class") = "disabled"
                End If

            Else
                slider.Attributes("class") = "slider bg-green"
                lblReqField.Visible = False
            End If

        Else
            slider.Attributes("class") = "slider bg-green"
            lblReqField.Visible = False
            pnlck.Visible = False
        End If

        lbl_Pass_Score.Text = e.Item.DataItem("Pass_Score").ToString
        If Ass_Count > 1 Then
            lbl_Q_Name_Order.Text = e.Item.ItemIndex + 1 & ". "
            lbl_Q_Name.Text = e.Item.DataItem("Q_Name").ToString
            'lbl_Q_Name.Style("color") = "black"
        Else
            lbl_Q_Name.Text = e.Item.DataItem("Q_Name").ToString
        End If
        lbl_Q_Name.Attributes("Q_ID") = e.Item.DataItem("Q_ID")
        lbl_Q_Name.Attributes("Q_Order") = e.Item.DataItem("Q_Order")
        lbl_Q_Name.Attributes("AR_ID") = e.Item.DataItem("AR_ID")
        txt_Score.Attributes("Max_Score") = e.Item.DataItem("Max_Score")
        txt_Score.Attributes("Pass_Score") = e.Item.DataItem("Pass_Score")
        txt_Score.Text = e.Item.DataItem("Get_Score")

        If (e.Item.DataItem("ckConfirm") = True) Then
            ckConfirm.Checked = True
        Else
            ckConfirm.Checked = False
        End If

        ckConfirm.Attributes("onchange") = "document.getElementById('" & btnUpdateScore.ClientID & "').click();"

    End Sub

    Private Function jQuerySlider() As String
        Dim Script As String = "initKnob();" & vbLf
        For Each GP As RepeaterItem In rpt_Ass_Q_Group.Items
            If GP.ItemType <> ListItemType.AlternatingItem And GP.ItemType <> ListItemType.Item Then Continue For
            Dim rpt_Ass_Q As Repeater = GP.FindControl("rpt_Ass_Q")

            For Each Q As RepeaterItem In rpt_Ass_Q.Items
                If Q.ItemType <> ListItemType.AlternatingItem And Q.ItemType <> ListItemType.Item Then Continue For
                Dim lbl_Q_Name As Label = Q.FindControl("lbl_Q_Name")
                Dim lbl_Score As Label = Q.FindControl("lbl_Score")
                Dim txt_Score As TextBox = Q.FindControl("txt_Score")
                Dim slider As HtmlGenericControl = Q.FindControl("slider")
                Dim ckConfirm As CheckBox = Q.FindControl("ckConfirm")

                Script &= "initSlider('" & slider.ClientID & "'," & txt_Score.Text & ",0," & txt_Score.Attributes("Max_Score") & "," & txt_Score.Attributes("Pass_Score") & ",'" & lbl_Score.ClientID & "','" & txt_Score.ClientID & "','" & btnUpdateScore.ClientID & "','" & ckConfirm.ClientID & "');" & vbLf

            Next
        Next
        Return Script
    End Function

    Protected Sub btnUpdateScore_Click(sender As Object, e As System.EventArgs) Handles btnUpdateScore.Click
        'SaveAssessment()
        BindAssessment()
    End Sub

#End Region

#Region "Comment"
    Private Sub BindCommentUser()
        Dim DT As DataTable = BL.GetAssessmentHeader(Ass_ID)
        If DT.Rows.Count = 0 Then
            txt_Comment_Creater.Text = ""
            txt_Comment_Auditor.Text = ""
            txt_Comment_Officer.Text = ""
            txt_Comment_Director.Text = ""
            lbl_Creater_Name.Text = ""
            lbl_Auditor_Name.Text = ""
            lbl_Officer_Name.Text = ""
            lbl_Director_Name.Text = ""
        Else
            txt_Comment_Creater.Text = DT.Rows(0).Item("Role_1_Comment").ToString
            txt_Comment_Auditor.Text = DT.Rows(0).Item("Role_2_Comment").ToString
            txt_Comment_Officer.Text = DT.Rows(0).Item("Role_3_Comment").ToString
            txt_Comment_Director.Text = DT.Rows(0).Item("Role_4_Comment").ToString
            lbl_Creater_Name.Text = Trim(DT.Rows(0).Item("Role_1_Title").ToString & DT.Rows(0).Item("Role_1_Fisrt_Name").ToString & " " & DT.Rows(0).Item("Role_1_Last_Name").ToString)
            lbl_Auditor_Name.Text = Trim(DT.Rows(0).Item("Role_2_Title").ToString & DT.Rows(0).Item("Role_2_Fisrt_Name").ToString & " " & DT.Rows(0).Item("Role_2_Last_Name").ToString)
            lbl_Officer_Name.Text = Trim(DT.Rows(0).Item("Role_3_Title").ToString & DT.Rows(0).Item("Role_3_Fisrt_Name").ToString & " " & DT.Rows(0).Item("Role_3_Last_Name").ToString)
            lbl_Director_Name.Text = Trim(DT.Rows(0).Item("Role_4_Title").ToString & DT.Rows(0).Item("Role_4_Fisrt_Name").ToString & " " & DT.Rows(0).Item("Role_4_Last_Name").ToString)
        End If

        If lbl_Creater_Name.Text = "" Then lbl_Creater_Name.Text = "<br/>........."
        If lbl_Auditor_Name.Text = "" Then lbl_Auditor_Name.Text = "<br/>........."
        If lbl_Officer_Name.Text = "" Then lbl_Officer_Name.Text = "<br/>........."
        If lbl_Director_Name.Text = "" Then lbl_Director_Name.Text = "<br/>........."

        '------------- Set Enabling--------------
        txt_Comment_Creater.Enabled = False
        txt_Comment_Auditor.Enabled = False
        txt_Comment_Officer.Enabled = False
        txt_Comment_Director.Enabled = False
        Select Case Current_Step '----------- สถานะใบประเมิน ----------
            Case AVLBL.AssessmentStep.Creater
                txt_Comment_Creater.Enabled = AssessmentRole = AVLBL.AssessmentRole.Creater
            Case AVLBL.AssessmentStep.Auditor
                txt_Comment_Auditor.Enabled = AssessmentRole = AVLBL.AssessmentRole.Auditor
            Case AVLBL.AssessmentStep.Supply_Officer
                txt_Comment_Officer.Enabled = AssessmentRole = AVLBL.AssessmentRole.Supply_Officer
            Case AVLBL.AssessmentStep.Director
                txt_Comment_Director.Enabled = AssessmentRole = AVLBL.AssessmentRole.Director
            Case AVLBL.AssessmentStep.Completed

        End Select

    End Sub

    Private Sub SaveComment()
        Dim SQL As String = "SELECT * FROM tb_Ass_Header WHERE Ass_ID=" & Ass_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        If txt_Comment_Creater.Text = "" Then
            DT.Rows(0).Item("Role_1_Comment") = DBNull.Value
        Else
            DT.Rows(0).Item("Role_1_Comment") = txt_Comment_Creater.Text
        End If
        If txt_Comment_Auditor.Text = "" Then
            DT.Rows(0).Item("Role_2_Comment") = DBNull.Value
        Else
            DT.Rows(0).Item("Role_2_Comment") = txt_Comment_Auditor.Text
        End If
        If txt_Comment_Officer.Text = "" Then
            DT.Rows(0).Item("Role_3_Comment") = DBNull.Value
        Else
            DT.Rows(0).Item("Role_3_Comment") = txt_Comment_Officer.Text
        End If
        If txt_Comment_Director.Text = "" Then
            DT.Rows(0).Item("Role_4_Comment") = DBNull.Value
        Else
            DT.Rows(0).Item("Role_4_Comment") = txt_Comment_Director.Text
        End If

        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)
    End Sub

    Protected Sub txt_Comment_TextChanged(sender As Object, e As System.EventArgs) Handles txt_Comment_Creater.TextChanged, txt_Comment_Auditor.TextChanged, txt_Comment_Officer.TextChanged, txt_Comment_Director.TextChanged
        'SaveComment()
        'BindCommentUser()
    End Sub

#End Region

#Region "Command"
    Private Sub BindWorkflow()
        'Check AssessmentRole And Current_Step

        '----------- Check สิทธิ์ที่สามารถเข้าถึงอย่างน้อย 1 ฝ่าย --------------
        Dim RT As DataTable = BL.GetUserDeptRole(User_ID)
        If RT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลนี้','Default.aspx');", True)
            Exit Sub
        End If

        '------------แต่ละคนสามารถประเมินได้หลายขั้นตอน-----------------
        RT = BL.GetUserDeptRole(User_ID, Dept_ID, Sub_Dept_ID)
        If RT.Rows.Count > 0 Then
            RT.DefaultView.RowFilter = "AR_ID='" & Current_Step & "'"
            If RT.DefaultView.Count = 1 Then
                AssessmentRole = RT.DefaultView(0).Item("AR_ID") '--------- บทบาทการประเมินของผู้ใช้คนปัจจุบัน -----------
            End If
        End If

        If AssessmentRole = AVLBL.AssessmentRole.None Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลนี้ ','Default.aspx');", True)
            Exit Sub
        End If

        css_On_Creator.Visible = False
        css_On_Auditor.Visible = False
        css_On_Officer.Visible = False
        css_On_Director.Visible = False
        css_On_Complete.Visible = False
        css_On_Del.Visible = False  '-----ลบใบประเมินเฉพาะขั้นตอน สร้างและผู้มีสิทธิ์-----

        css_Can_Creator.Visible = False
        css_Can_Auditor.Visible = False
        css_Can_Officer.Visible = False
        css_Can_Director.Visible = False
        css_Can_Completed.Visible = False
        css_Can_Del.Visible = False  '-----ลบใบประเมินเฉพาะขั้นตอน สร้างและผู้มีสิทธิ์-----
        btn_Send_Del2.Visible = False
        btnSaveAll.CssClass = "btn green span6 margin-bottom-25 pull-left"
        btn_Send_Del2.CssClass = "btn yellow span6 margin-bottom-25 pull-left"

        pnl_Send_Delete.Visible = False     '-----ไม่ใช้ปุ่มนี้แล้วเลื่อนไปไว้ด้านบน

        lbl_Status_Helper.Text = ""

        '--------------- ดูว่า สถานะของการประเมินตรงกับผู้ใช้คนปัจจุบันหรือไม่ ---------------------
        Dim DA As New SqlDataAdapter("SELECT * FROM tb_Ass_Header WHERE Ass_ID=" & Ass_ID, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        '---------Endable ปุ่ม Delete--------
        Dim UserInfo As AVLBL.UserInfo = BL.GetUserInfo(User_ID)
        Dim Old_Role_1_ID As String = DT.Rows(0).Item("Role_1_ID").ToString()


        Select Case Current_Step '----------- สถานะใบประเมิน ----------
            Case AVLBL.AssessmentStep.Creater

                lbl_Status_Helper.Text = "ขณะนี้การประเมินอยู่ในขั้นตอนของ 'ผู้ประเมิน'"
                If AssessmentRole = AVLBL.AssessmentRole.Creater Then '----------- สิทธิ์ผู้ใช้ตรงกับใบประเมินหรือไม่ ----------
                    lbl_Status_Helper.Text &= " คุณสามารถคลิกเพื่อส่งต่อใบประเมินไปยัง 'ผู้ตรวจสอบ' ได้"
                    ''--------------- Save start-time if not saved yet -------------
                    'DT.Rows(0).Item("Role_1_ID") = Session("User_ID")
                    'DT.Rows(0).Item("Role_1_Title") = Session("Title")
                    'DT.Rows(0).Item("Role_1_Fisrt_Name") = Session("Fisrt_Name")
                    'DT.Rows(0).Item("Role_1_Last_Name") = Session("Last_Name")
                    'DT.Rows(0).Item("Role_1_Pos_Name") = Session("Pos_Name")
                    'If IsDBNull(DT.Rows(0).Item("Role_1_Start")) Then
                    '    DT.Rows(0).Item("Role_1_Start") = Now
                    'End If
                    'DT.Rows(0).Item("Role_1_Active") = True     '------------------เริ่มดำเนินการประเมิน ตามสิทธิ์ผู้ใช้----------
                    'DT.Rows(0).Item("Update_By") = User_ID
                    'DT.Rows(0).Item("Update_Time") = Now
                    'Dim cmd As New SqlCommandBuilder(DA)
                    'DA.Update(DT)
                End If
                css_On_Creator.Visible = True
                css_Can_Auditor.Visible = Current_Step = AssessmentRole
                btnSaveAll.Visible = Current_Step = AssessmentRole

                '------ปุ่ม Del--------
                Dim Iscan As Boolean = False
                If UserInfo.Cancel_Role Then    ' สามารถยกเลิกใบประเมินได้
                    btn_Send_Del2.Visible = UserInfo.Cancel_Role
                    Iscan = UserInfo.Cancel_Role

                Else
                    If IsDBNull(DT.Rows(0).Item("Role_2_Start")) Then
                        Iscan = (Current_Step = AssessmentRole) And (Session("User_ID") = Old_Role_1_ID)
                        btn_Send_Del2.Visible = Iscan
                    End If
                End If

                If ((Current_Step = AssessmentRole) And Iscan) Then
                    btnSaveAll.CssClass = "btn green span6 margin-bottom-25 pull-left"
                    btn_Send_Del2.CssClass = "btn yellow span6 margin-bottom-25 pull-left"



                ElseIf (((Current_Step = AssessmentRole)) And Not Iscan) Then
                    btnSaveAll.CssClass = "btn green span12 margin-bottom-25 pull-left"
                    btn_Send_Del2.Visible = Iscan



                ElseIf (Not ((Current_Step = AssessmentRole)) And Iscan) Then
                    btn_Send_Del2.CssClass = "btn yellow span12 margin-bottom-25 pull-left"
                    'btnSaveAll.Visible = UserInfo.Cancel_Role
                ElseIf (Not ((Current_Step = AssessmentRole)) And Not Iscan) Then
                    'btnSaveAll.Visible = UserInfo.Cancel_Role
                    btn_Send_Del2.Visible = Iscan
                End If

            Case AVLBL.AssessmentStep.Auditor

                lbl_Status_Helper.Text = "ขณะนี้การประเมินอยู่ในขั้นตอนของ 'ผู้ตรวจสอบ'"
                If AssessmentRole = AVLBL.AssessmentRole.Auditor Then '----------- สิทธิ์ผู้ใช้ตรงกับใบประเมินหรือไม่ ----------
                    lbl_Status_Helper.Text &= " คุณสามารถคลิกเพื่อส่งต่อใบประเมินไปยัง 'เจ้าหน้าที่พัสดุ' หรือส่งกลับไปยัง 'ผู้ประเมิน' ได้"
                    ''--------------- Save start-time if not saved yet -------------
                    'DT.Rows(0).Item("Role_2_ID") = Session("User_ID")
                    'DT.Rows(0).Item("Role_2_Title") = Session("Title")
                    'DT.Rows(0).Item("Role_2_Fisrt_Name") = Session("Fisrt_Name")
                    'DT.Rows(0).Item("Role_2_Last_Name") = Session("Last_Name")
                    'DT.Rows(0).Item("Role_2_Pos_Name") = Session("Pos_Name")
                    'If IsDBNull(DT.Rows(0).Item("Role_2_Start")) Then
                    '    DT.Rows(0).Item("Role_2_Start") = Now
                    'End If

                    'DT.Rows(0).Item("Role_2_Active") = True     '------------------เริ่มดำเนินการประเมิน ตามสิทธิ์ผู้ใช้----------
                    'DT.Rows(0).Item("Update_By") = User_ID
                    'DT.Rows(0).Item("Update_Time") = Now
                    'Dim cmd As New SqlCommandBuilder(DA)
                    'DA.Update(DT)
                End If
                css_On_Auditor.Visible = True
                If ((AssessmentRole + 1) = Current_Step) Then
                    css_Can_Creator.Visible = Not DT.Rows(0).Item("Role_2_Active")
                Else
                    css_Can_Creator.Visible = (Current_Step = AssessmentRole)
                End If
                css_Can_Officer.Visible = Current_Step = AssessmentRole
                btnSaveAll.Visible = Current_Step = AssessmentRole
                '------ปุ่ม Del--------
                'pnl_Send_Delete.Visible = UserInfo.Cancel_Role
                'css_Can_Del.Visible = UserInfo.Cancel_Role
                btn_Send_Del2.Visible = UserInfo.Cancel_Role

                If (Not UserInfo.Cancel_Role) Then
                    btnSaveAll.CssClass = "btn green span12 margin-bottom-25 pull-left"
                ElseIf (UserInfo.Cancel_Role And (btnSaveAll.Visible = False)) Then
                    btn_Send_Del2.CssClass = "btn yellow span12 margin-bottom-25 pull-left"
                End If


            Case AVLBL.AssessmentStep.Supply_Officer

                lbl_Status_Helper.Text = "ขณะนี้การประเมินอยู่ในขั้นตอนของ 'เจ้าหน้าที่พัสดุ'"
                If AssessmentRole = AVLBL.AssessmentRole.Supply_Officer Then '----------- สิทธิ์ผู้ใช้ตรงกับใบประเมินหรือไม่ ----------
                    lbl_Status_Helper.Text &= " คุณสามารถคลิกเพื่อส่งต่อใบประเมินไปยัง 'หัวหน้าฝ่ายบริหารทั่วไป' หรือส่งกลับไปยัง 'ผู้ตรวจสอบ' ได้"
                    ''--------------- Save start-time if not saved yet -------------
                    'DT.Rows(0).Item("Role_3_ID") = Session("User_ID")
                    'DT.Rows(0).Item("Role_3_Title") = Session("Title")
                    'DT.Rows(0).Item("Role_3_Fisrt_Name") = Session("Fisrt_Name")
                    'DT.Rows(0).Item("Role_3_Last_Name") = Session("Last_Name")
                    'DT.Rows(0).Item("Role_3_Pos_Name") = Session("Pos_Name")
                    'If IsDBNull(DT.Rows(0).Item("Role_3_Start")) Then
                    '    DT.Rows(0).Item("Role_3_Start") = Now
                    'End If
                    'DT.Rows(0).Item("Role_3_Active") = True     '------------------เริ่มดำเนินการประเมิน ตามสิทธิ์ผู้ใช้----------
                    'DT.Rows(0).Item("Update_By") = User_ID
                    'DT.Rows(0).Item("Update_Time") = Now
                    'Dim cmd As New SqlCommandBuilder(DA)
                    'DA.Update(DT)
                End If
                css_On_Officer.Visible = True
                If ((AssessmentRole + 1) = Current_Step) Then
                    css_Can_Auditor.Visible = Not DT.Rows(0).Item("Role_3_Active")
                Else
                    css_Can_Auditor.Visible = (Current_Step = AssessmentRole)
                End If
                css_Can_Director.Visible = Current_Step = AssessmentRole
                btnSaveAll.Visible = Current_Step = AssessmentRole
                '------ปุ่ม Del--------
                'pnl_Send_Delete.Visible = UserInfo.Cancel_Role
                'css_Can_Del.Visible = UserInfo.Cancel_Role
                btn_Send_Del2.Visible = UserInfo.Cancel_Role

                If (Not UserInfo.Cancel_Role) Then
                    btnSaveAll.CssClass = "btn green span12 margin-bottom-25 pull-left"
                ElseIf (UserInfo.Cancel_Role And (btnSaveAll.Visible = False)) Then
                    btn_Send_Del2.CssClass = "btn yellow span12 margin-bottom-25 pull-left"
                End If

            Case AVLBL.AssessmentStep.Director

                lbl_Status_Helper.Text = "ขณะนี้การประเมินอยู่ในขั้นตอนของ 'หัวหน้าฝ่ายบริหารทั่วไป'"
                If AssessmentRole = AVLBL.AssessmentRole.Director Then '----------- สิทธิ์ผู้ใช้ตรงกับใบประเมินหรือไม่ ----------
                    lbl_Status_Helper.Text &= " คุณสามารถคลิกเพื่อยืนยันจบการประเมิน หรือส่งกลับไปยัง 'เจ้าหน้าที่พัสดุ' ได้"
                    ''--------------- Save start-time if not saved yet -------------
                    'DT.Rows(0).Item("Role_4_ID") = Session("User_ID")
                    'DT.Rows(0).Item("Role_4_Title") = Session("Title")
                    'DT.Rows(0).Item("Role_4_Fisrt_Name") = Session("Fisrt_Name")
                    'DT.Rows(0).Item("Role_4_Last_Name") = Session("Last_Name")
                    'DT.Rows(0).Item("Role_4_Pos_Name") = Session("Pos_Name")
                    'If IsDBNull(DT.Rows(0).Item("Role_4_Start")) Then
                    '    DT.Rows(0).Item("Role_4_Start") = Now
                    'End If
                    'DT.Rows(0).Item("Role_4_Active") = True     '------------------เริ่มดำเนินการประเมิน ตามสิทธิ์ผู้ใช้----------
                    'DT.Rows(0).Item("Update_By") = User_ID
                    'DT.Rows(0).Item("Update_Time") = Now
                    'Dim cmd As New SqlCommandBuilder(DA)
                    'DA.Update(DT)
                End If
                css_On_Director.Visible = True
                If ((AssessmentRole + 1) = Current_Step) Then
                    css_Can_Officer.Visible = Not DT.Rows(0).Item("Role_4_Active")
                Else
                    css_Can_Officer.Visible = (Current_Step = AssessmentRole)
                End If
                css_Can_Completed.Visible = Current_Step = AssessmentRole
                btnSaveAll.Visible = Current_Step = AssessmentRole
                '------ปุ่ม Del--------
                btn_Send_Del2.Visible = UserInfo.Cancel_Role

                If (Not UserInfo.Cancel_Role) Then
                    btnSaveAll.CssClass = "btn green span12 margin-bottom-25 pull-left"
                ElseIf (UserInfo.Cancel_Role And (btnSaveAll.Visible = False)) Then
                    btn_Send_Del2.CssClass = "btn yellow span12 margin-bottom-25 pull-left"
                End If

            Case AVLBL.AssessmentStep.Completed
                css_On_Complete.Visible = True
                lbl_Status_Helper.Text = "ขณะนี้การประเมินเสร็จสมบูรณ์แล้ว"
                '--------- Save nothing-----------
                btnSaveAll.Visible = Current_Step = AssessmentRole
                '------ปุ่ม Del--------
                'pnl_Send_Delete.Visible = False
                'css_Can_Del.Visible = UserInfo.Cancel_Role
                btn_Send_Del2.Visible = UserInfo.Cancel_Role

                If (Not UserInfo.Cancel_Role) Then
                    btnSaveAll.CssClass = "btn green span12 margin-bottom-25 pull-left"
                ElseIf (UserInfo.Cancel_Role And (btnSaveAll.Visible = False)) Then
                    btn_Send_Del2.CssClass = "btn yellow span12 margin-bottom-25 pull-left"
                End If

        End Select

        '---------------Report user information history--------------

        lblCreaterInfo.Text = ReportSteppedUserInfo(DT.Rows(0).Item("Role_1_ID"), DT.Rows(0).Item("Role_1_Title"), DT.Rows(0).Item("Role_1_Fisrt_Name"), DT.Rows(0).Item("Role_1_Last_Name"), DT.Rows(0).Item("Role_1_Start"), DT.Rows(0).Item("Role_1_End"))
        lblAuditorInfo.Text = ReportSteppedUserInfo(DT.Rows(0).Item("Role_2_ID"), DT.Rows(0).Item("Role_2_Title"), DT.Rows(0).Item("Role_2_Fisrt_Name"), DT.Rows(0).Item("Role_2_Last_Name"), DT.Rows(0).Item("Role_2_Start"), DT.Rows(0).Item("Role_2_End"))
        lblSupplyOfficerInfo.Text = ReportSteppedUserInfo(DT.Rows(0).Item("Role_3_ID"), DT.Rows(0).Item("Role_3_Title"), DT.Rows(0).Item("Role_3_Fisrt_Name"), DT.Rows(0).Item("Role_3_Last_Name"), DT.Rows(0).Item("Role_3_Start"), DT.Rows(0).Item("Role_3_End"))
        lblDirectorInfo.Text = ReportSteppedUserInfo(DT.Rows(0).Item("Role_4_ID"), DT.Rows(0).Item("Role_4_Title"), DT.Rows(0).Item("Role_4_Fisrt_Name"), DT.Rows(0).Item("Role_4_Last_Name"), DT.Rows(0).Item("Role_4_Start"), DT.Rows(0).Item("Role_4_End"))

    End Sub

    Private Function ReportSteppedUserInfo(ByVal RoleID As Object, ByVal Title As Object, ByVal Fisrt_Name As Object, ByVal Last_Name As Object, ByVal Role_Start As Object, ByVal Role_End As Object) As String

        Dim Result As String = ""
        If IsDBNull(RoleID) Or IsDBNull(Title) Or IsDBNull(Fisrt_Name) Or IsDBNull(Last_Name) Then Return Result

        Result = " โดย " & Title & Fisrt_Name & " " & Last_Name

        'If Not IsDBNull(Role_Start) And Not IsDBNull(Role_Start) Then
        If (Not IsDBNull(Role_Start)) And (Not IsDBNull(Role_Start)) And (Not IsDBNull(Role_End)) Then
            Result &= " วันที่ " & GL.ReportThaiDate(Role_Start) & " ถึง " & GL.ReportThaiDate(Role_End)
            Result &= " (" & DateDiff(DateInterval.Day, Role_Start, Role_End) + 1 & " วัน)"
        ElseIf Not IsDBNull(Role_Start) Then
            Result &= " เริ่มประเมินวันที่ " & GL.ReportThaiDate(Role_Start)
            Select Case DateDiff(DateInterval.Day, Role_Start, Now)
                Case 0
                    Result &= " (วันนี้)"
                Case 1
                    Result &= " (เมื่อวาน)"
                Case Else
                    Result &= " (" & DateDiff(DateInterval.Day, Role_Start, Now) & " วันมาแล้ว)"
            End Select
        End If

        Return Result

    End Function

    Private Function jQueryWorkflowButton() As String
        Dim Script As String = ""
        'If css_Can_Creator.Visible Then Script &= "$('#pnl_Send_Creater').click(function () { if(confirm('ยืนยันส่งใบประเมินไปยัง ""ผู้ประเมิน"" ?'))$('#btn_Send_Creater').click(); });" & vbLf
        'If css_Can_Auditor.Visible Then Script &= "$('#pnl_Send_Auditor').click(function () { if(confirm('ยืนยันส่งใบประเมินไปยัง ""ผู้ตรวจสอบ"" ?'))$('#btn_Send_Auditor').click(); });" & vbLf
        'If css_Can_Officer.Visible Then Script &= "$('#pnl_Send_Officer').click(function () { if(confirm('ยืนยันส่งใบประเมินไปยัง ""เจ้าหน้าที่พัสดุ"" ?'))$('#btn_Send_Officer').click(); });" & vbLf
        'If css_Can_Director.Visible Then Script &= "$('#pnl_Send_Director').click(function () { if(confirm('ยืนยันส่งใบประเมินไปยัง ""หัวหน้าฝ่ายบริหาร"" ?'))$('#btn_Send_Director').click(); });" & vbLf
        'If css_Can_Completed.Visible Then Script &= "$('#pnl_Send_Complete').click(function () { if(confirm('ยืนยันผลการประเมินทั้งหมด ?'))$('#btn_Send_Complete').click(); });" & vbLf
        'If css_Can_Del.Visible Then Script &= "$('#pnl_Send_Delete').click(function () { if(confirm('ยืนยันลบใบประเมิน ?'))$('#btn_Send_Del').click(); });" & vbLf

        If css_Can_Creator.Visible Then pnl_Send_Creater.Attributes("onclick") = "if(confirm('ยืนยันส่งใบประเมินไปยัง ""ผู้ประเมิน"" ?'))document.getElementById('" & btn_Send_Creater.ClientID & "').click();" & vbLf
        If css_Can_Auditor.Visible Then pnl_Send_Auditor.Attributes("onclick") = "if(confirm('ยืนยันส่งใบประเมินไปยัง ""ผู้ตรวจสอบ"" ?'))document.getElementById('" & btn_Send_Auditor.ClientID & "').click();" & vbLf
        If css_Can_Officer.Visible Then pnl_Send_Officer.Attributes("onclick") = "if(confirm('ยืนยันส่งใบประเมินไปยัง ""เจ้าหน้าที่พัสดุ"" ?'))document.getElementById('" & btn_Send_Officer.ClientID & "').click();" & vbLf
        If css_Can_Director.Visible Then pnl_Send_Director.Attributes("onclick") = "if(confirm('ยืนยันส่งใบประเมินไปยัง ""หัวหน้าฝ่ายบริหาร"" ?'))document.getElementById('" & btn_Send_Director.ClientID & "').click();" & vbLf
        If css_Can_Completed.Visible Then pnl_Send_Complete.Attributes("onclick") = "if(confirm('ยืนยันผลการประเมินทั้งหมด ?'))document.getElementById('" & btn_Send_Complete.ClientID & "').click();" & vbLf
        If css_Can_Del.Visible Then pnl_Send_Delete.Attributes("onclick") = "if(confirm('ยืนยันลบใบประเมิน ?'))document.getElementById('" & btn_Send_Del.ClientID & "').click();" & vbLf

        Return Script

    End Function

    'btn_Send_Del2.Click
    Private Sub Delete_Ass(sender As Object, e As System.EventArgs) Handles btn_Send_Del.Click, btnOK_DEL.Click
        Dim btn As Button = sender
        If txt_DEL_Comment.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "validate", "ShowAlert('warning','กรอก หมายเหตุการยกเลิกใบประเมิน','');", True)
            txt_DEL_Comment.Focus()
            Exit Sub
        End If


        ' ตรวจสอบ มีใบประเมินทบทวนอยู่หรือไม่  ไม่มีถึงจะลบได้
        Dim SQL As String = "SELECT ReAss_ID,Ass_ID FROM tb_ReAss_Header WHERE Ass_ID=" & Ass_ID
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "validate", "ShowAlert('warning','ไม่สามารถลบใบประเมินได้<br>เนื่องจากมีประวัติการประเมินทบทวนนี้อยู่ในระบบ','');", True)
            Exit Sub
        End If

        ' เก็บ Description เข้าตาราง tb_Log_Delete
        Dim DTAss As DataTable = BL.GetAssessmentHeader(Ass_ID)

        SQL = "SELECT * FROM tb_Log_Delete WHERE 0=1"
        DT = New DataTable
        DA = New SqlDataAdapter(SQL, BL.ConnectionString)
        DA.Fill(DT)
        Dim DR As DataRow = DT.NewRow
        DR("LD_ID") = BL.GetNewPrimaryID("tb_Log_Delete", "LD_ID")
        DR("Ass_Type") = "Ass"
        DR("Ass_ID") = Ass_ID
        DR("ReAss_ID") = DBNull.Value
        DR("Ref_Year") = DTAss.Rows(0).Item("Ref_Year").ToString()
        DR("Ref_Month") = DTAss.Rows(0).Item("Ref_Month").ToString()
        DR("Dept_ID") = DTAss.Rows(0).Item("Dept_ID").ToString()
        DR("Sub_Dept_ID") = DTAss.Rows(0).Item("Sub_Dept_ID").ToString()
        DR("Dept_Name") = DTAss.Rows(0).Item("Dept_Name").ToString()
        DR("Sub_Dept_Name") = DTAss.Rows(0).Item("Sub_Dept_Name").ToString()
        DR("Ref_Number") = DTAss.Rows(0).Item("Ref_Number").ToString()

        DR("S_Name") = DTAss.Rows(0).Item("S_Name").ToString()

        Dim AssessmentNumber As String = "A" & DR("Ref_Year") & DR("Ref_Month") & DR("Dept_ID") & DR("Ref_Number")



        Dim Description As String = "ใบประเมินผู้ขายใหม่เลขที่ " + AssessmentNumber
        Description &= " โดย" & DTAss.Rows(0).Item("Dept_Name").ToString() & " ฝ่าย" & DTAss.Rows(0).Item("Sub_Dept_Name").ToString()

        If Not IsDBNull(DTAss.Rows(0).Item("Item_Name")) Or DTAss.Rows(0).Item("Item_Name").ToString() <> "" Then
            Select Case Cat_Group_ID
                Case 1
                    Description &= " สินค้า/พัสดุครุภัณฑ์ : " & DTAss.Rows(0).Item("Item_Name").ToString()
                Case 2
                    Description &= " จัดจ้าง/บริการ : " & DTAss.Rows(0).Item("Item_Name").ToString()
            End Select
            DR("Type_ID") = DTAss.Rows(0).Item("Type_ID").ToString()
            DR("Item_Name") = DTAss.Rows(0).Item("Item_Name").ToString()

        Else
            DR("Type_ID") = DBNull.Value
            DR("Item_Name") = DTAss.Rows(0).Item("Item_Name").ToString()

        End If

        If Not IsDBNull(DTAss.Rows(0).Item("S_ID")) Or DTAss.Rows(0).Item("S_ID").ToString() <> "" Then
            Description &= " ของ" & DTAss.Rows(0).Item("S_Name").ToString()
            DR("S_ID") = DTAss.Rows(0).Item("S_ID")

        Else
            DR("S_ID") = DBNull.Value
        End If

        DR("LD_Comment") = txt_DEL_Comment.Text
        DR("LD_Description") = Description
        DR("Delete_By") = User_ID
        DR("Delete_Time") = Now
        DT.Rows.Add(DR)
        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)

        ' ตรวจสอบเคยได้เลข AVL หรือไม่
        ' tb_AVL_List
        ' tb_Ass_File
        Dim Comm As New SqlCommand
        Dim Conn As New SqlConnection(BL.ConnectionString)
        Conn.Open()
        With Comm
            .Connection = Conn
            .CommandType = CommandType.Text
            .CommandText = "DELETE FROM  tb_Ass_File WHERE Ass_ID=" & Ass_ID
            .CommandText &= "DELETE FROM tb_AVL_List WHERE Ass_ID=" & Ass_ID
            .CommandText &= "DELETE FROM tb_Item WHERE Ass_ID=" & Ass_ID
            .ExecuteNonQuery()
            .Dispose()
        End With

        ' ลบรายละเอียด   tb_Ass_Detail
        With Comm
            .Connection = Conn
            .CommandType = CommandType.Text
            .CommandText = "DELETE FROM  tb_Ass_Detail WHERE Ass_ID=" & Ass_ID
            .ExecuteNonQuery()
            .Dispose()
        End With

        ' ลบใบประเมิน  tb_Ass_Header
        With Comm
            .Connection = Conn
            .CommandType = CommandType.Text
            .CommandText = "DELETE FROM  tb_Ass_Header WHERE Ass_ID=" & Ass_ID
            .ExecuteNonQuery()
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()
        '-----tb_Log_Delete----

        Response.Redirect("Assessment_List.aspx")
    End Sub


    Private Sub SaveWorkflow(sender As Object, e As System.EventArgs) Handles btn_Send_Creater.Click, btn_Send_Auditor.Click, btn_Send_Officer.Click, btn_Send_Director.Click, btn_Send_Complete.Click
        Dim btn As Button = sender

        Dim AT As DataTable = AssessmentData()
        Dim ToStep As Integer = 0
        Select Case btn.ID.ToString()
            Case "btn_Send_Creater"
                ToStep = AVLBL.AssessmentStep.Creater
            Case "btn_Send_Auditor"
                ToStep = AVLBL.AssessmentStep.Auditor
            Case "btn_Send_Officer"
                ToStep = AVLBL.AssessmentStep.Supply_Officer
            Case "btn_Send_Director"
                ToStep = AVLBL.AssessmentStep.Director
            Case "btn_Send_Complete"
                ToStep = AVLBL.AssessmentStep.Completed
        End Select

        If Current_Step <= ToStep Then
            '------- Validate First------------
            If ddl_Dept.SelectedIndex < 1 Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "validate", "ShowAlert('warning','เลือก สำนักงาน','');", True)
                Exit Sub
            End If
            If ddl_Sub_Dept.SelectedIndex < 1 Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "validate", "ShowAlert('warning','เลือก ฝ่าย/ภาค/งาน','');", True)
                Exit Sub
            End If
           

            If Type_ID = 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "validate", "ShowAlert('warning','เลือก " & Title_Item_Group & "','');", True)
                Exit Sub
            End If
            If Trim(Type_Name) = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "validate", "ShowAlert('warning','เลือก  ชื่อ" & Title_Item_Group & "','');", True)
                Exit Sub
            End If
            If Trim(txt_Item_Name.Text) = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "validate", "ShowAlert('warning','กรอก  ชื่อ" & Title_Item_Group & "','');", True)
                txt_Item_Name.Focus()
                Exit Sub
            End If
            If S_ID = 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "validate", "ShowAlert('warning','เลือก ผู้ขาย / ผู้ให้บริการ','');", True)
                btnSupplierDialog.Focus()
                Exit Sub
            End If

            If Trim(lbl_S_Fullname.Text) = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "validate", "ShowAlert('warning','กรอกชื่อผู้ขาย / ผู้ให้บริการ','');", True)
                lbl_S_Fullname.Focus()
                Exit Sub
            End If
            If S_Type_ID <= 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "validate", "ShowAlert('warning','เลือก ประเภทผู้ขาย / ผู้ให้บริการ','');", True)
                ddl_S_Type.Focus()
                Exit Sub
            End If
            'Dim AT As DataTable = AssessmentData()

            AT.DefaultView.RowFilter = "Get_Score IS NULL OR Get_Score=0 AND ckConfirm=0 AND AR_ID=" & Current_Step
            If AT.DefaultView.Count > 0 And Current_Step = AssessmentRole Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "validate", "ShowAlert('warning','check ยืนยันคะแนนประเมินเท่ากับ 0','');", True)
                pnlMaskAssessment.Focus()
                Exit Sub
            End If

        End If


        Save_Role_Action()      '-----บันทึกชื่อผู้ประเมินแต่ละขั้นประเมิน------

        'SaveAll
        SaveProductType()
        SaveProductDetail()
        SaveSupplier()
        SaveComment()
        checkComplete()
        SaveAssessment()


        Dim Get_Score As Object = AT.Compute("SUM(Get_Score)", "")
        Dim Pass_Score As Object = AT.Compute("SUM(Pass_Score)", "")


        '------เริ่ม update Status 0 ก่อน 
        If btn.ID.Replace("btn_Send_", "").ToUpper = "COMPLETE" Then
            BL.DisableAVLStatus(Ass_ID, User_ID, AVLBL.AssessmentType.Assessment) '---------ปรับสถานะ disable AVL เดิม
        End If

        '------- Save end-time and send to next step------------
        Dim SQL As String = "SELECT Ass_ID,Role_1_End,Role_1_Active,Role_2_End,Role_2_Active,Role_3_End,Role_3_Active,Role_4_End,Role_4_Active,Current_Step,Flag_Return,Pass_Status,Update_By,Update_Time" & vbLf
        SQL &= "FROM tb_Ass_Header WHERE Ass_ID=" & Ass_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim FromStep = CInt(AssessmentRole)
        Select Case AssessmentRole
            Case AVLBL.AssessmentRole.Creater
                DT.Rows(0).Item("Role_1_End") = Now
                DT.Rows(0).Item("Role_2_Active") = False
            Case AVLBL.AssessmentRole.Auditor
                DT.Rows(0).Item("Role_2_End") = Now
                DT.Rows(0).Item("Role_3_Active") = False
            Case AVLBL.AssessmentRole.Supply_Officer
                DT.Rows(0).Item("Role_3_End") = Now
                DT.Rows(0).Item("Role_4_Active") = False
            Case AVLBL.AssessmentRole.Director
                DT.Rows(0).Item("Role_4_End") = Now
        End Select

        Dim SuccessMessage As String = ""
        Dim ToRemoveAVL As Boolean = False
        Select Case btn.ID.Replace("btn_Send_", "").ToUpper
            Case "CREATER"
                DT.Rows(0).Item("Current_Step") = AVLBL.AssessmentStep.Creater
                SuccessMessage = "ระบบได้ทำการส่งใบประเมินไปยัง \""ผู้ประเมิน\"" แล้ว"
            Case "AUDITOR"
                DT.Rows(0).Item("Current_Step") = AVLBL.AssessmentStep.Auditor
                SuccessMessage = "ระบบได้ทำการส่งใบประเมินไปยัง \""ผู้ตรวจสอบ\"" แล้ว"
            Case "OFFICER"
                DT.Rows(0).Item("Current_Step") = AVLBL.AssessmentStep.Supply_Officer
                SuccessMessage = "ระบบได้ทำการส่งใบประเมินไปยัง \""เจ้าหน้าที่พัสดุ\"" แล้ว"
            Case "DIRECTOR"
                DT.Rows(0).Item("Current_Step") = AVLBL.AssessmentStep.Director
                SuccessMessage = "ระบบได้ทำการส่งใบประเมินไปยัง \""หัวหน้าฝ่ายบริหาร\"" แล้ว"
            Case "COMPLETE"
                DT.Rows(0).Item("Current_Step") = AVLBL.AssessmentStep.Completed
                SuccessMessage = "การประเมินเสร็จสมบูรณ์"


        End Select
        SuccessMessage &= "\n\nคุณสามารถตรวจสอบและสืบค้นข้อมูลการประเมินย้อนหลังได้จากเมนู \""รายงาน\"" และ\""การประเมินผู้ขายใหม่\"""

        'Dim ToStep = CInt(DT.Rows(0).Item("Current_Step"))
        '---------- ตีกลับไป Step ก่อนหน้า ----------
        'If ToStep < FromStep Then

        'End If

        If Current_Step <= ToStep Then
            DT.Rows(0).Item("Flag_Return") = False
        Else
            DT.Rows(0).Item("Flag_Return") = True
        End If

        DT.Rows(0).Item("Pass_Status") = IsPass_Status

        DT.Rows(0).Item("Update_By") = User_ID
        DT.Rows(0).Item("Update_Time") = Now
        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)

        '-----------------------START-----UPDATE AVL TO tb_Header--------------------
        If btn.ID.Replace("btn_Send_", "").ToUpper = "COMPLETE" Then
            If IsPass_Status And Get_Score >= Pass_Score Then '---------- Pass-------------
                Dim AVL As AVLBL.AVLCode = BL.GetAVLNoFromNewAssessment(Ass_ID, AVLBL.AssessmentType.Assessment)
                If AVL.AVL_Y = "" Or AVL.AVL_M = "" Or AVL.AVL_Dept_ID = "" Or AVL.AVL_Sub_Dept_ID = "" Or AVL.AVL_No = "" Then
                    '--------------ยังไม่เคยได้ AVL----สร้างใน TB_AVL_List
                    Dim Y As String = ""
                    If Now.Year < 2300 Then
                        Y = Now.Year
                    Else
                        Y = Now.Year
                    End If
                    Y = Y.Substring(2)
                    Dim M As String = Now.Month.ToString.PadLeft(2, "0")
                    AVL = BL.GetNewAVLNo(Y, M, Dept_ID, Sub_Dept_ID)
                    BL.SetAVLNoForNewAssessment(Ass_ID, AVL, User_ID, AVLBL.AssessmentType.Assessment) '------อัพเดท AVL ในtb_Header และ สร้าง AVL ใน tb_AVL_List

                Else
                    BL.UpdateAVLStatus(AVL, 1, User_ID)
                End If
                SuccessMessage &= "\nผู้ขายรายนี้ได้เลข AVL : " & AVL.AVL_Code
            End If
        End If
        '-----------------------END-----UPDATE AVL TO tb_Header--------------------

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "validate", "ShowAlert('success','" & SuccessMessage & "','Assessment_Edit.aspx?Ass_ID=" & Ass_ID & "');", True)
        BindWorkflow()
        BindCommentUser()
    End Sub

#End Region

#Region "Dialog ProductType"
    Protected Sub lnkCloseProduct_Click(sender As Object, e As System.EventArgs) Handles lnkCloseProduct.Click
        dialogProductType.Visible = False
    End Sub

    Protected Sub btnProductDialog_Click(sender As Object, e As System.EventArgs) Handles btnProductDialog.Click
        BindListProductType()
        BL.BindDDlCatGroup(ddl_CatGroup)
        txtSearchProduct.Text = ""
        BL.BindDDlCAT(ddl_Cat, User_ID, Dept_ID)
        ddl_CatGroup_SelectedIndexChanged(sender, e)
        dialogProductType.Visible = True
    End Sub

    Protected Sub ddl_CatGroup_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddl_CatGroup.SelectedIndexChanged
        'If Search_CatGroup_ID <> 0 Then
        BL.BindDDlCAT(ddl_Cat, User_ID, Dept_ID, 0, True, ddl_CatGroup.SelectedValue)
        ddl_Cat_SelectedIndexChanged(sender, e)

        'End If

    End Sub

    Protected Sub ddl_Cat_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddl_Cat.SelectedIndexChanged
        BL.BindDDlSub_Cat(ddl_Sub_Cat, User_ID, Search_Cat_ID)
        ddl_Sub_Cat_SelectedIndexChanged(sender, e)
        BindListProductType()
    End Sub

    Protected Sub ddl_Sub_Cat_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddl_Sub_Cat.SelectedIndexChanged
        BL.BindDDlType(ddl_Type, User_ID, Search_Sub_Cat_ID)
        ddl_Type_SelectedIndexChanged(sender, e)
        BindListProductType()
    End Sub

    Protected Sub ddl_Type_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddl_Type.SelectedIndexChanged
        BindListProductType()
    End Sub

    Protected Sub SearchProduct(sender As Object, e As System.EventArgs) Handles btnSearchProduct.Click, txtSearchProduct.TextChanged
        BindListProductType()
    End Sub

    Private Sub BindListProductType()
        Dim DT As DataTable = BL.GetProductTypeByDept(Dept_ID)

        Dim Filter As String = ""

        If Search_CatGroup_ID <> 0 Then
            Filter &= " CG_ID=" & Search_CatGroup_ID & " AND "
        Else
            Filter &= " (CG_ID IN (1,2) OR CG_ID IS NULL) AND "
        End If
        'If Search_Sub_Cat_ID <> 0 Then
        '    Filter &= " Sub_Cat_ID=" & Search_Sub_Cat_ID & " AND "
        'ElseIf Search_Cat_ID <> 0 Then
        '    Filter &= " Cat_ID=" & Search_Sub_Cat_ID & " AND "
        'End If


        If Search_Type_ID <> 0 Then
            Filter &= " Type_ID=" & Search_Type_ID & " AND "
        ElseIf Search_Sub_Cat_ID <> 0 Then
            Filter &= " Sub_Cat_ID=" & Search_Sub_Cat_ID & " AND "
        ElseIf Search_Cat_ID <> 0 Then
            Filter &= " Cat_ID=" & Search_Cat_ID & " AND "
        End If


        'If txtSearchProduct.Text <> "" Then
        '    Filter &= "( Cat_No+Sub_Cat_No+Type_No Like '%" & txtSearchProduct.Text.Replace("'", "''") & "%' OR " & vbLf
        '    Filter &= " Cat_Name Like '%" & txtSearchProduct.Text.Replace("'", "''") & "%' OR " & vbLf
        '    Filter &= " Sub_Cat_Name Like '%" & txtSearchProduct.Text.Replace("'", "''") & "%' OR " & vbLf
        '    Filter &= " Type_Name Like '%" & txtSearchProduct.Text.Replace("'", "''") & "%' " & vbLf
        '    Filter &= " ) AND "
        'End If

        If txtSearchProduct.Text <> "" Then
            Filter &= "( Cat_No+Sub_Cat_No+Type_No+Running_No Like '%" & txtSearchProduct.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Cat_No+Sub_Cat_No+Type_No Like '%" & txtSearchProduct.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Cat_Name Like '%" & txtSearchProduct.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Sub_Cat_Name Like '%" & txtSearchProduct.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Type_Name Like '%" & txtSearchProduct.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Running_Name Like '%" & txtSearchProduct.Text.Replace("'", "''") & "%' " & vbLf
            Filter &= " ) AND "
        End If

        If Filter <> "" Then DT.DefaultView.RowFilter = Filter.Substring(0, Filter.Length - 4)
        DT = DT.DefaultView.ToTable
        Session("Assessment_Edit_ProductTypeList") = DT
        PagerProduct.SesssionSourceName = "Assessment_Edit_ProductTypeList"
        PagerProduct.RenderLayout()

        If DT.Rows.Count = 0 Then
            lblTotalProduct.Text = "ไม่พบรายการดังกล่าว"
        Else
            lblTotalProduct.Text = "พบ " & FormatNumber(DT.Rows.Count, 0) & " รายการ"
        End If

    End Sub

    Protected Sub PagerProduct_PageChanging(Sender As PageNavigation) Handles PagerProduct.PageChanging
        PagerProduct.TheRepeater = rptProductTypeList
    End Sub

    Protected Sub rptProductTypeList_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptProductTypeList.ItemCommand
        Select Case e.CommandName
            Case "select"
                Running_ID = e.CommandArgument

                '-------------เชคผู้ขายรายนี้ถูกประเมินซ้ำ สินค้า  ผู้ขาย ฝ่าย หรือไม่----------------------
                Dim SRole As Boolean = BL.CanSelect_Sup_Type_SubDept_Assessment(Ass_ID, Dept_ID, Sub_Dept_ID, S_ID, Running_ID, "Item_Type", Type_ID)
                If Not SRole Then '---fase
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','มีการประเมินรายการชุดนี้แล้ว<br>ต้องประเมินทบทวนสินค้า/พัสดุครุภัณฑ์','');", True)
                    Exit Sub
                End If
                Dim TP As DataTable = BL.GetProductTypeByRunningNo(Running_ID)
                If TP.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','ไม่พบข้อมูลผู้ขายรายนี้','');", True)
                    Exit Sub
                End If
                '--------------- Update ข้อมูลเข้าไปในฟอร์ม ------------------------
                BindProductType()
                dialogProductType.Visible = False
        End Select
    End Sub

    Protected Sub rptProductTypeList_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptProductTypeList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lbl_Cat As Label = e.Item.FindControl("lbl_Cat")
        Dim lbl_SubCat As Label = e.Item.FindControl("lbl_SubCat")
        Dim lbl_Type As Label = e.Item.FindControl("lbl_Type")
        Dim lbl_Running As Label = e.Item.FindControl("lbl_Running")

        lbl_Cat.Text = e.Item.DataItem("Cat_No") & " : " & e.Item.DataItem("Cat_Name")
        lbl_SubCat.Text = e.Item.DataItem("Cat_No") & e.Item.DataItem("Sub_Cat_No") & " : " & e.Item.DataItem("Sub_Cat_Name")
        lbl_Type.Text = e.Item.DataItem("Cat_No") & e.Item.DataItem("Sub_Cat_No") & e.Item.DataItem("Type_No") & " : " & e.Item.DataItem("Type_Name")

        lbl_Running.Text = e.Item.DataItem("Cat_No") & e.Item.DataItem("Sub_Cat_No") & e.Item.DataItem("Type_No") & e.Item.DataItem("Running_No") & " : " & e.Item.DataItem("Running_Name")

        Dim btnSelect As Button = e.Item.FindControl("btnSelect")
        For i As Integer = 1 To 4
            Dim td As HtmlTableCell = e.Item.FindControl("td" & i)
            td.Attributes("onClick") = "document.getElementById('" & btnSelect.ClientID & "').click();"
        Next
        btnSelect.CommandArgument = e.Item.DataItem("Running_ID")
    End Sub

#End Region

#Region "Dialog Supplier"
    Protected Sub lnkCloseSupplier_Click(sender As Object, e As System.EventArgs) Handles lnkCloseSupplier.Click
        dialogSupplier.Visible = False
    End Sub

    Protected Sub btnSupplierDialog_Click(sender As Object, e As System.EventArgs) Handles btnSupplierDialog.Click
        txtSearchTaxNo.Text = ""
        txtSearchName.Text = ""
        txtSearchAlias.Text = ""
        txtSearchContact.Text = ""
        BindSupplierList()
        dialogSupplier.Visible = True
    End Sub

    Protected Sub SearchSupplier_Changed(sender As Object, e As System.EventArgs) Handles btnSearchSupplier.Click, txtSearchTaxNo.TextChanged, txtSearchName.TextChanged, txtSearchAlias.TextChanged, txtSearchContact.TextChanged
        BindSupplierList()
    End Sub

    Private Sub BindSupplierList()
        Dim DT As DataTable = BL.GetSupplierList(True)
        Dim Filter As String = ""
        If txtSearchTaxNo.Text <> "" Then
            Filter &= " S_Tax_No LIKE '%" & txtSearchTaxNo.Text.Replace("'", "''") & "%' AND "
        End If
        If txtSearchName.Text <> "" Then
            Filter &= " S_Name LIKE '%" & txtSearchName.Text.Replace("'", "''") & "%' AND "
        End If
        If txtSearchAlias.Text <> "" Then
            Filter &= " S_Alias LIKE '%" & txtSearchAlias.Text.Replace("'", "''") & "%' AND "
        End If
        If txtSearchContact.Text <> "" Then
            Filter &= " (S_Address LIKE '%" & txtSearchContact.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Phone LIKE '%" & txtSearchContact.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Fax LIKE '%" & txtSearchContact.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Email LIKE '%" & txtSearchContact.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Contact_Name LIKE '%" & txtSearchContact.Text.Replace("'", "''") & "%' " & vbLf
            Filter &= " ) AND "
        End If
        If Filter <> "" Then DT.DefaultView.RowFilter = Filter.Substring(0, Filter.Length - 4)
        DT = DT.DefaultView.ToTable
        Session("Assessment_Edit_SupplierList") = DT
        PagerSupplier.SesssionSourceName = "Assessment_Edit_SupplierList"
        PagerSupplier.RenderLayout()

        If DT.Rows.Count = 0 Then
            lblTotalSupplier.Text = "ไม่พบรายการดังกล่าว"
        Else
            lblTotalSupplier.Text = "พบ " & FormatNumber(DT.Rows.Count, 0) & " รายการ"
        End If

    End Sub

    Protected Sub PagerSupplier_PageChanging(Sender As PageNavigation) Handles PagerSupplier.PageChanging
        PagerSupplier.TheRepeater = rptSupplierList
    End Sub

    Protected Sub rptSupplierList_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptSupplierList.ItemCommand
        Select Case e.CommandName
            Case "select"
                Dim _S_ID As Integer = e.CommandArgument

                '-------------เชคผู้ขายรายนี้ถูกประเมินซ้ำ สินค้า  ผู้ขาย ฝ่าย หรือไม่----------------------
                Dim SRole As Boolean = BL.CanSelect_Sup_Type_SubDept_Assessment(Ass_ID, Dept_ID, Sub_Dept_ID, _S_ID, Running_ID, "S_ID", S_ID)

                If Not SRole Then '---fase
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','มีการประเมินรายการชุดนี้แล้ว<br>ต้องประเมินทบทวนสินค้า/พัสดุครุภัณฑ์ หรือประเมินใหม่','');", True)
                    Exit Sub
                End If


                Dim ST As DataTable = BL.GetSupplierDetail(_S_ID)
                If ST.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','ไม่พบข้อมูลผู้ขายรายนี้','');", True)
                    Exit Sub
                End If


                If (Session("_Sessionddl_S_Type") <> Nothing) Then
                    ddl_S_Type.SelectedValue = Session("_Sessionddl_S_Type")
                End If



                '--------------- Update ข้อมูลเข้าไปในฟอร์ม ------------------------
                S_ID = _S_ID

                lbl_S_Fullname.Text = ST.Rows(0).Item("S_Name").ToString
                lbl_S_Alias.Text = ST.Rows(0).Item("S_Alias").ToString
                lbl_S_Tax_No.Text = ST.Rows(0).Item("S_Tax_No").ToString
                lbl_S_Address.Text = ST.Rows(0).Item("S_Address").ToString
                txt_S_Contact_Name.Text = ST.Rows(0).Item("S_Contact_Name").ToString
                txt_S_Email.Text = ST.Rows(0).Item("S_Email").ToString
                lbl_S_Phone.Text = ST.Rows(0).Item("S_Phone").ToString
                lbl_S_Fax.Text = ST.Rows(0).Item("S_Fax").ToString
                'SaveSupplier()
                BindSupplier()

                dialogSupplier.Visible = False
        End Select
    End Sub

    Protected Sub rptSupplierList_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptSupplierList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lbl_Tax As Label = e.Item.FindControl("lbl_Tax")
        Dim lbl_Supplier As Label = e.Item.FindControl("lbl_Supplier")
        Dim lbl_Sales As Label = e.Item.FindControl("lbl_Sales")
        Dim lbl_Contact As Label = e.Item.FindControl("lbl_Contact")

        lbl_Tax.Text = e.Item.DataItem("S_Tax_No").ToString
        lbl_Supplier.Text = e.Item.DataItem("S_Name").ToString
        If Not IsDBNull(e.Item.DataItem("S_Alias")) AndAlso e.Item.DataItem("S_Alias") <> "" Then
            lbl_Supplier.Text &= "(" & e.Item.DataItem("S_Alias") & ")"
        End If
        lbl_Sales.Text = e.Item.DataItem("S_Contact_Name").ToString
        Dim Contact As String = ""
        If Not IsDBNull(e.Item.DataItem("S_Phone")) AndAlso e.Item.DataItem("S_Phone") <> "" Then
            Contact &= "<br/><i class='icon-briefcase'></i> " & e.Item.DataItem("S_Phone")
        End If
        If Not IsDBNull(e.Item.DataItem("S_Email")) AndAlso e.Item.DataItem("S_Email") <> "" Then
            Contact &= "<br/><i class='icon-envelope'></i> " & e.Item.DataItem("S_Email")
        End If
        If Not IsDBNull(e.Item.DataItem("S_Fax")) AndAlso e.Item.DataItem("S_Fax") <> "" Then
            Contact &= "<br/><i class='icon-print'></i> " & e.Item.DataItem("S_Fax")
        End If
        If Contact <> "" Then
            lbl_Contact.Text = Contact.Substring(5)
        End If

        Dim btnSelect As Button = e.Item.FindControl("btnSelect")
        For i As Integer = 1 To 4
            Dim td As HtmlTableCell = e.Item.FindControl("td" & i)
            td.Attributes("onClick") = "document.getElementById('" & btnSelect.ClientID & "').click();"
        Next
        btnSelect.CommandArgument = e.Item.DataItem("S_ID")
    End Sub

#End Region

#Region "PrintButton"

    Protected Sub btnPDF_Click(sender As Object, e As System.EventArgs) Handles btnPDF.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/RPT_Ass.aspx?Mode=PDF" & "&Ass_ID=" & Ass_ID & "');", True)
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/RPT_Ass.aspx?Mode=EXCEL" & "&Ass_ID=" & Ass_ID & "');", True)
    End Sub

#End Region


    Protected Sub btnSaveAll_Click(sender As Object, e As System.EventArgs) Handles btnSaveAll.Click
        'SaveDept()
        'SaveSubDept()
        SaveProductType()
        SaveProductDetail()
        SaveSupplier()
        SaveComment()
        checkComplete()
        SaveAssessment()
        SavePass_Status()
        Try
            BindWorkflow()
            BindCommentUser()

            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "validate", "ShowAlert('success','" & "บันทึกเรียบร้อย" & "','Assessment_Edit.aspx?Ass_ID=" & Ass_ID & "');", True)

            Exit Sub

        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "validate", "ShowAlert('warning','" & "ไม่สามารถบันทึกได้" & "','Assessment_Edit.aspx?Ass_ID=" & Ass_ID & "');", True)
            Exit Sub
        End Try

    End Sub

    '----ตรวจสอบ Result การประเมิน----------
    Private Sub SavePass_Status()

        Save_Role_Action()   '-----บันทึกชื่อผู้ประเมินแต่ละขั้นประเมิน------
        Dim SQL As String = "SELECT * FROM tb_Ass_Header WHERE Ass_ID=" & Ass_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        DT.Rows(0).Item("Pass_Status") = IsPass_Status

        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)
        '---------- Update Header Summary Score--------
        BL.UpdateHeaderScore_Assessment(Ass_ID)
        checkComplete()

    End Sub

    Private Sub Save_Role_Action()

        '--------update Herader--------
        '--------------- ดูว่า สถานะของการประเมินตรงกับผู้ใช้คนปัจจุบันหรือไม่ ---------------------
        Dim DA_H As New SqlDataAdapter("SELECT * FROM tb_Ass_Header WHERE Ass_ID=" & Ass_ID, BL.ConnectionString)
        Dim DT_H As New DataTable
        DA_H.Fill(DT_H)

        Select Case Current_Step '----------- สถานะใบประเมิน ----------
            Case AVLBL.AssessmentStep.Creater
                If AssessmentRole = AVLBL.AssessmentRole.Creater Then '----------- สิทธิ์ผู้ใช้ตรงกับใบประเมินหรือไม่ ----------
                    '--------------- Save start-time if not saved yet -------------
                    DT_H.Rows(0).Item("Role_1_ID") = Session("User_ID")
                    DT_H.Rows(0).Item("Role_1_Title") = Session("Title")
                    DT_H.Rows(0).Item("Role_1_Fisrt_Name") = Session("Fisrt_Name")
                    DT_H.Rows(0).Item("Role_1_Last_Name") = Session("Last_Name")
                    DT_H.Rows(0).Item("Role_1_Pos_Name") = Session("Pos_Name")
                    If IsDBNull(DT_H.Rows(0).Item("Role_1_Start")) Then
                        DT_H.Rows(0).Item("Role_1_Start") = Now
                    End If
                    DT_H.Rows(0).Item("Role_1_Active") = True     '------------------เริ่มดำเนินการประเมิน ตามสิทธิ์ผู้ใช้----------
                    DT_H.Rows(0).Item("Update_By") = User_ID
                    DT_H.Rows(0).Item("Update_Time") = Now
                    Dim cmd_H As New SqlCommandBuilder(DA_H)
                    DA_H.Update(DT_H)
                End If

            Case AVLBL.AssessmentStep.Auditor
                If AssessmentRole = AVLBL.AssessmentRole.Auditor Then '----------- สิทธิ์ผู้ใช้ตรงกับใบประเมินหรือไม่ ----------
                    '--------------- Save start-time if not saved yet -------------
                    DT_H.Rows(0).Item("Role_2_ID") = Session("User_ID")
                    DT_H.Rows(0).Item("Role_2_Title") = Session("Title")
                    DT_H.Rows(0).Item("Role_2_Fisrt_Name") = Session("Fisrt_Name")
                    DT_H.Rows(0).Item("Role_2_Last_Name") = Session("Last_Name")
                    DT_H.Rows(0).Item("Role_2_Pos_Name") = Session("Pos_Name")
                    If IsDBNull(DT_H.Rows(0).Item("Role_2_Start")) Then
                        DT_H.Rows(0).Item("Role_2_Start") = Now
                    End If

                    DT_H.Rows(0).Item("Role_2_Active") = True     '------------------เริ่มดำเนินการประเมิน ตามสิทธิ์ผู้ใช้----------
                    DT_H.Rows(0).Item("Update_By") = User_ID
                    DT_H.Rows(0).Item("Update_Time") = Now
                    Dim cmd_H As New SqlCommandBuilder(DA_H)
                    DA_H.Update(DT_H)
                End If

            Case AVLBL.AssessmentStep.Supply_Officer

                If AssessmentRole = AVLBL.AssessmentRole.Supply_Officer Then '----------- สิทธิ์ผู้ใช้ตรงกับใบประเมินหรือไม่ ----------
                    '--------------- Save start-time if not saved yet -------------
                    DT_H.Rows(0).Item("Role_3_ID") = Session("User_ID")
                    DT_H.Rows(0).Item("Role_3_Title") = Session("Title")
                    DT_H.Rows(0).Item("Role_3_Fisrt_Name") = Session("Fisrt_Name")
                    DT_H.Rows(0).Item("Role_3_Last_Name") = Session("Last_Name")
                    DT_H.Rows(0).Item("Role_3_Pos_Name") = Session("Pos_Name")
                    If IsDBNull(DT_H.Rows(0).Item("Role_3_Start")) Then
                        DT_H.Rows(0).Item("Role_3_Start") = Now
                    End If
                    DT_H.Rows(0).Item("Role_3_Active") = True     '------------------เริ่มดำเนินการประเมิน ตามสิทธิ์ผู้ใช้----------
                    DT_H.Rows(0).Item("Update_By") = User_ID
                    DT_H.Rows(0).Item("Update_Time") = Now
                    Dim cmd_H As New SqlCommandBuilder(DA_H)
                    DA_H.Update(DT_H)
                End If

            Case AVLBL.AssessmentStep.Director
                If AssessmentRole = AVLBL.AssessmentRole.Director Then '----------- สิทธิ์ผู้ใช้ตรงกับใบประเมินหรือไม่ ----------
                    '--------------- Save start-time if not saved yet -------------
                    DT_H.Rows(0).Item("Role_4_ID") = Session("User_ID")
                    DT_H.Rows(0).Item("Role_4_Title") = Session("Title")
                    DT_H.Rows(0).Item("Role_4_Fisrt_Name") = Session("Fisrt_Name")
                    DT_H.Rows(0).Item("Role_4_Last_Name") = Session("Last_Name")
                    DT_H.Rows(0).Item("Role_4_Pos_Name") = Session("Pos_Name")
                    If IsDBNull(DT_H.Rows(0).Item("Role_4_Start")) Then
                        DT_H.Rows(0).Item("Role_4_Start") = Now
                    End If
                    DT_H.Rows(0).Item("Role_4_Active") = True     '------------------เริ่มดำเนินการประเมิน ตามสิทธิ์ผู้ใช้----------
                    DT_H.Rows(0).Item("Update_By") = User_ID
                    DT_H.Rows(0).Item("Update_Time") = Now
                    Dim cmd_H As New SqlCommandBuilder(DA_H)
                    DA_H.Update(DT_H)
                End If
            Case AVLBL.AssessmentStep.Completed

        End Select

    End Sub


    Protected Sub btnClose_DEL_Click(sender As Object, e As System.EventArgs) Handles btnClose_DEL.Click
        ModalComment.Visible = False
    End Sub

    Protected Sub btn_Send_Del2_Click(sender As Object, e As System.EventArgs) Handles btn_Send_Del2.Click
        ModalComment.Visible = True
    End Sub
End Class
