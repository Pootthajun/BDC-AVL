﻿
Imports System.Data
Imports System.Data.SqlClient

Partial Class Compare_Supplier
    Inherits System.Web.UI.Page

    Dim BL As New AVLBL
    Dim GL As New GenericLib

    Private ReadOnly Property PageName As String
        Get
            Return "Compare_Supplier.aspx"
        End Get
    End Property

    Private ReadOnly Property User_ID As Integer
        Get
            Try
                If IsNumeric(Session("User_ID")) Then
                    Return Session("User_ID")
                Else
                    Return -1
                End If
            Catch ex As Exception
                Return -1
            End Try
        End Get
    End Property

    Private Property AccessMode As AVLBL.AccessRole
        Get
            Try
                Return ViewState("AccessMode")
            Catch ex As Exception
                Return AVLBL.AccessRole.None
            End Try
        End Get
        Set(value As AVLBL.AccessRole)
            ViewState("AccessMode") = value
        End Set
    End Property

    Private ReadOnly Property Dept_ID As String
        Get
            Try
                Return ddl_Dept.Items(ddl_Dept.SelectedIndex).Value
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Private ReadOnly Property Dept_Name As String
        Get
            Try
                Return ddl_Dept.Items(ddl_Dept.SelectedIndex).Text
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Public Property Running_ID As String
        Get
            Try
                Return lblRunning_ID_dialog.Text
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As String)
            lblRunning_ID_dialog.Text = value
        End Set
    End Property

    Private ReadOnly Property Search_CatGroup_ID As Integer
        Get
            Try
                Return ddl_CatGroup_dialog.Items(ddl_CatGroup_dialog.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
    End Property

    Private ReadOnly Property Search_Cat_ID As Integer
        Get
            Try
                Return ddl_Cat_dialog.Items(ddl_Cat_dialog.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
    End Property

    Private ReadOnly Property Search_Sub_Cat_ID As Integer
        Get
            Try
                Return ddl_Sub_Cat_dialog.Items(ddl_Sub_Cat_dialog.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
    End Property

    Private ReadOnly Property Search_Type_ID As Integer
        Get
            Try
                Return ddl_Type_dialog.Items(ddl_Type_dialog.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
    End Property

    'Private ReadOnly Property Cat_ID As Integer
    '    Get
    '        Try
    '            Return ddl_Cat.Items(ddl_Cat.SelectedIndex).Value
    '        Catch ex As Exception
    '            Return 0
    '        End Try
    '    End Get
    'End Property

    'Private ReadOnly Property Sub_Cat_ID As Integer
    '    Get
    '        Try
    '            Return ddl_Sub_Cat.Items(ddl_Sub_Cat.SelectedIndex).Value
    '        Catch ex As Exception
    '            Return 0
    '        End Try
    '    End Get
    'End Property

    'Private ReadOnly Property Type_ID As Integer
    '    Get
    '        Try
    '            Return ddl_Type.Items(ddl_Type.SelectedIndex).Value
    '        Catch ex As Exception
    '            Return 0
    '        End Try
    '    End Get
    'End Property

    '----------------Name-------------------
    Private Property Cat_Name As String
        Get
            Try
                Return lbl_Cat.Text
            Catch ex As Exception
                Return ""
            End Try
        End Get
        Set(value As String)
            lbl_Cat.Text = value
        End Set
    End Property

    Private Property Sub_Cat_Name As String
        Get
            Try
                Return lbl_Sub_Cat.Text
            Catch ex As Exception
                Return ""
            End Try
        End Get
        Set(value As String)
            lbl_Sub_Cat.Text = value
        End Set
    End Property

    Private Property Type_Name As String
        Get
            Try
                Return lbl_Type.Text
            Catch ex As Exception
                Return ""
            End Try
        End Get
        Set(value As String)
            lbl_Type.Text = value
        End Set
    End Property

    Private Property Running_Name As String
        Get
            Try
                Return lbl_Running.Text
            Catch ex As Exception
                Return ""
            End Try
        End Get
        Set(value As String)
            lbl_Running.Text = value
        End Set
    End Property

    Private Property Item_Code As String
        Get
            Try
                Return lbl_Running.Attributes("Item_Code")
            Catch ex As Exception
                Return ""
            End Try
        End Get
        Set(value As String)
            lbl_Running.Attributes("Item_Code") = value
        End Set
    End Property

    Private Property S_ID As Integer
        Get
            Try
                Return lnkAddSupplier.Attributes("S_ID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Integer)
            lnkAddSupplier.Attributes("S_ID") = value
        End Set
    End Property

    Private Property S_ID_Compare As String
        Get
            Try
                Return lnkAddSupplier.Attributes("S_ID_Compare")
            Catch ex As Exception
                Return ""
            End Try
        End Get
        Set(value As String)
            lnkAddSupplier.Attributes("S_ID_Compare") = value
        End Set
    End Property

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        If IsNothing(Session("User_ID")) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "ShowAlert('warning','กรุณาล็อกอินเข้าสู่ระบบ','Login.aspx');", True)
            Response.Redirect("Login.aspx")
            Exit Sub
        End If

        If Not IsPostBack Then
            SetUserRole()
            dialogSelectItem.Visible = False
            dialogSupplier.Visible = False
            PnlCompare.Visible = False
            pnl_Product.Visible = False
        End If


    End Sub

    Private Sub SetUserRole()

        '----------- Check สิทธิ์ในเมนู --------------
        Dim MT As DataTable = BL.GetUserMenuRole(User_ID, PageName)
        If MT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลนี้','Overview.aspx');", True)
            Exit Sub
        End If
        AccessMode = MT.Rows(0).Item("MR_ID") '--------- สิทธิ์ในเมนูของผู้ใช้คนปัจจุบัน -----------
        If AccessMode = AVLBL.AccessRole.None Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลนี้','Overview.aspx');", True)
            Exit Sub
        End If

    End Sub


#Region "DialogItem"

    Protected Sub lnkCloseDialogSelectItem_Click(sender As Object, e As System.EventArgs) Handles lnkCloseDialogSelectItem.Click
        dialogSelectItem.Visible = False
    End Sub

    Protected Sub lnkSelectItem_Click(sender As Object, e As System.EventArgs) Handles lnkSelectItem.Click
        BL.BindDDlDEPT(ddl_Dept, User_ID)
        ddl_Dept_SelectedIndexChanged(sender, e)
        dialogSelectItem.Visible = True

        pnlCreate.Visible = False
        pnlbtnProductDialog.Visible = False
        pnlHeaderDialog.Visible = False

    End Sub

    Protected Sub ddl_Dept_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddl_Dept.SelectedIndexChanged
        'BL.BindDDlCAT(ddl_Cat, User_ID, Dept_ID)
        'ddl_Cat_SelectedIndexChanged(sender, e)
        'pnlCat.Visible = Dept_ID <> ""

        pnlbtnProductDialog.Visible = True
        pnlCreate.Visible = False
        pnlbtnProductDialog.Visible = Dept_ID.ToString() <> "0"
        pnlHeaderDialog.Visible = False

    End Sub

    'Protected Sub ddl_Cat_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddl_Cat.SelectedIndexChanged
    '    BL.BindDDlSub_Cat(ddl_Sub_Cat, User_ID, Cat_ID)
    '    ddl_Sub_Cat_SelectedIndexChanged(sender, e)
    '    pnlSubCat.Visible = Cat_ID <> 0

    '    Cat_Name = ddl_Cat.SelectedItem.ToString()
    'End Sub

    'Protected Sub ddl_Sub_Cat_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddl_Sub_Cat.SelectedIndexChanged
    '    BL.BindDDlType(ddl_Type, User_ID, Sub_Cat_ID)
    '    ddl_Type_SelectedIndexChanged(sender, e)
    '    pnlType.Visible = Sub_Cat_ID <> 0
    '    Sub_Cat_Name = ddl_Sub_Cat.SelectedItem.ToString

    'End Sub

    'Protected Sub ddl_Type_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddl_Type.SelectedIndexChanged
    '    pnlCreate.Visible = Type_ID <> 0
    '    Type_Name = ddl_Type.SelectedItem.ToString
    'End Sub


#End Region

#Region "Dialog ProductType"
    Protected Sub lnkCloseProduct_Click(sender As Object, e As System.EventArgs) Handles lnkCloseProduct.Click
        dialogProductType.Visible = False
    End Sub

    Protected Sub btnProductDialog_Click(sender As Object, e As System.EventArgs) Handles btnProductDialog.Click
        BindListProductType()
        BL.BindDDlCatGroup(ddl_CatGroup_dialog)
        txtSearchProduct_dialog.Text = ""
        BL.BindDDlCAT(ddl_Cat_dialog, User_ID, Dept_ID)
        dialogProductType.Visible = True
    End Sub


    Protected Sub ddl_CatGroup_dialog_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddl_CatGroup_dialog.SelectedIndexChanged
        BL.BindDDlCAT(ddl_Cat_dialog, User_ID, Dept_ID, 0, True, ddl_CatGroup_dialog.SelectedValue)
        ddl_Cat_dialog_SelectedIndexChanged(sender, e)
        BindListProductType()
    End Sub

    Protected Sub ddl_Cat_dialog_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddl_Cat_dialog.SelectedIndexChanged
        BL.BindDDlSub_Cat(ddl_Sub_Cat_dialog, User_ID, Search_Cat_ID)
        ddl_Sub_Cat_dialog_SelectedIndexChanged(sender, e)
        BindListProductType()
    End Sub

    Protected Sub ddl_Sub_Cat_dialog_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddl_Sub_Cat_dialog.SelectedIndexChanged
        BL.BindDDlType(ddl_Type_dialog, User_ID, Search_Sub_Cat_ID)
        ddl_Type_dialog_SelectedIndexChanged(sender, e)
        BindListProductType()
    End Sub

    Protected Sub ddl_Type_dialog_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddl_Type_dialog.SelectedIndexChanged
        BindListProductType()
    End Sub

    Protected Sub btnSearchProduct_dialog_Click(sender As Object, e As System.EventArgs) Handles btnSearchProduct_dialog.Click
        BindListProductType()
    End Sub

    Private Sub BindListProductType()
        Dim DT As DataTable = BL.GetProductTypeByDept(Dept_ID)  '--------เฉพาะรายการที่มี RunningNo

        Dim Filter As String = ""

        If Search_CatGroup_ID <> 0 Then
            Filter &= " CG_ID=" & Search_CatGroup_ID & " AND "
        Else
            Filter &= " (CG_ID IN (1,2) OR CG_ID IS NULL) AND "
        End If

        If Search_Type_ID <> 0 Then
            Filter &= " Type_ID=" & Search_Type_ID & " AND "
        ElseIf Search_Sub_Cat_ID <> 0 Then
            Filter &= " Sub_Cat_ID=" & Search_Sub_Cat_ID & " AND "
        ElseIf Search_Cat_ID <> 0 Then
            Filter &= " Cat_ID=" & Search_Cat_ID & " AND "
        End If

        If txtSearchProduct_dialog.Text <> "" Then
            Filter &= "( Cat_No+Sub_Cat_No+Type_No+Running_No Like '%" & txtSearchProduct_dialog.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Cat_No+Sub_Cat_No+Type_No Like '%" & txtSearchProduct_dialog.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Cat_Name Like '%" & txtSearchProduct_dialog.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Sub_Cat_Name Like '%" & txtSearchProduct_dialog.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Type_Name Like '%" & txtSearchProduct_dialog.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Running_Name Like '%" & txtSearchProduct_dialog.Text.Replace("'", "''") & "%' " & vbLf
            Filter &= " ) AND "
        End If

        If Filter <> "" Then DT.DefaultView.RowFilter = Filter.Substring(0, Filter.Length - 4)
        DT = DT.DefaultView.ToTable
        Session("Assessment_List_ProductTypeList") = DT
        PagerProduct.SesssionSourceName = "Assessment_List_ProductTypeList"
        PagerProduct.RenderLayout()

        If DT.Rows.Count = 0 Then
            lblTotalProduct.Text = "ไม่พบรายการดังกล่าว"
        Else
            lblTotalProduct.Text = "พบ " & FormatNumber(DT.Rows.Count, 0) & " รายการ"
        End If

    End Sub

    Protected Sub PagerProduct_PageChanging(Sender As PageNavigation) Handles PagerProduct.PageChanging
        PagerProduct.TheRepeater = rptProductTypeList
    End Sub

    Protected Sub rptProductTypeList_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptProductTypeList.ItemCommand
        Select Case e.CommandName
            Case "select"
                Running_ID = e.CommandArgument
                Dim TP As DataTable = BL.GetProductTypeByRunningNo(Running_ID)

                If TP.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','ไม่พบข้อมูลผู้ขายรายนี้','');", True)
                    Exit Sub
                End If

                BindProductType()
                dialogProductType.Visible = False
                pnlCreate.Visible = Running_ID <> 0
        End Select
    End Sub

    Protected Sub rptProductTypeList_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptProductTypeList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lbl_Cat As Label = e.Item.FindControl("lbl_Cat")
        Dim lbl_SubCat As Label = e.Item.FindControl("lbl_SubCat")
        Dim lbl_Type As Label = e.Item.FindControl("lbl_Type")
        Dim lbl_Running As Label = e.Item.FindControl("lbl_Running")

        lbl_Cat.Text = e.Item.DataItem("Cat_No") & " : " & e.Item.DataItem("Cat_Name")
        lbl_SubCat.Text = e.Item.DataItem("Cat_No") & e.Item.DataItem("Sub_Cat_No") & " : " & e.Item.DataItem("Sub_Cat_Name")
        lbl_Type.Text = e.Item.DataItem("Cat_No") & e.Item.DataItem("Sub_Cat_No") & e.Item.DataItem("Type_No") & " : " & e.Item.DataItem("Type_Name")

        lbl_Running.Text = e.Item.DataItem("Cat_No") & e.Item.DataItem("Sub_Cat_No") & e.Item.DataItem("Type_No") & e.Item.DataItem("Running_No") & " : " & e.Item.DataItem("Running_Name")
        Dim btnSelect As Button = e.Item.FindControl("btnSelect")
        For i As Integer = 1 To 4
            Dim td As HtmlTableCell = e.Item.FindControl("td" & i)
            td.Attributes("onClick") = "document.getElementById('" & btnSelect.ClientID & "').click();"
        Next
        btnSelect.CommandArgument = e.Item.DataItem("Running_ID")

    End Sub

    Private Sub BindProductType()
        Dim DT As DataTable = BL.GetProductTypeByRunningNo(Running_ID)
        Dim Filter As String = ""
       
        Select Case DT.Rows(0).Item("CG_ID").ToString
            Case 0
                lblHeader_ItemDialog.Text = "สินค้า, พัสดุครุภัณฑ์ หรือ งานบริการจัดจ้าง"

            Case 2
                lblHeader_ItemDialog.Text = "จัดจ้างบริการ : "

            Case Else
                lblHeader_ItemDialog.Text = "สินค้า,พัสดุครุภัณฑ์ : "
        End Select
        lbl_Item_TypeDialog.Text = DT.Rows(0).Item("Cat_No").ToString & DT.Rows(0).Item("Sub_Cat_No").ToString
        lbl_Item_TypeDialog.Text += DT.Rows(0).Item("Type_No").ToString & DT.Rows(0).Item("Running_No").ToString & " : " & DT.Rows(0).Item("Running_Name").ToString

        lblHeader_ItemLeft.Text = lblHeader_ItemDialog.Text
        Cat_Name = DT.Rows(0).Item("Cat_Name").ToString
        Sub_Cat_Name = DT.Rows(0).Item("Sub_Cat_Name").ToString
        Type_Name = DT.Rows(0).Item("Type_Name").ToString
        Running_Name = DT.Rows(0).Item("Running_Name").ToString
        Item_Code = DT.Rows(0).Item("Item_Code").ToString
        pnlHeaderDialog.Visible = True

    End Sub

#End Region


#Region "buttonCreateCompare"

    Protected Sub btnCreateCompare_Click(sender As Object, e As System.EventArgs) Handles btnCreateCompare.Click

        Dim DT As DataTable = BL.GetSupplierList_Compare(True, Running_ID, "", Dept_ID)
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "ShowAlert('warning','ยังไม่มีการประเมินพัสดุ/ครุภัณฑ์ จัดจ้างและบริการ : " & Running_Name & " ในระบบ" & "','');", True)
            Exit Sub
        End If
        S_ID_Compare = ""
        rptCompare.DataSource = Nothing
        rptCompare.DataBind()
        PnlCompare.Visible = True
        lblHeader_Item.Text = ddl_Dept.SelectedItem.ToString & " > " & Cat_Name & " > " & Sub_Cat_Name & " > " & Type_Name & " > (" & Item_Code & ") " & Running_Name
        lbl_Cat.Text = Cat_Name
        lbl_Sub_Cat.Text = Sub_Cat_Name
        lbl_Type.Text = Type_Name
        lbl_Running.Text = Running_Name
        dialogSelectItem.Visible = False
    End Sub

#End Region

#Region "lnkAddSupplier"

    Private Sub BindSupplierList()

        '---------เปลี่ยนเป็น Query ผู้ขายที่ถูกประเมินในสินค้านั้นๆ

        Dim DT As DataTable = BL.GetSupplierList_Compare(True, Running_ID, S_ID_Compare, Dept_ID)
        Dim Filter As String = ""
        If txtSearchTaxNo.Text <> "" Then
            Filter &= " S_Tax_No LIKE '%" & txtSearchTaxNo.Text.Replace("'", "''") & "%' AND "
        End If
        If txtSearchName.Text <> "" Then
            Filter &= " S_Name LIKE '%" & txtSearchName.Text.Replace("'", "''") & "%' AND "
        End If
        If txtSearchAlias.Text <> "" Then
            Filter &= " S_Alias LIKE '%" & txtSearchAlias.Text.Replace("'", "''") & "%' AND "
        End If
        If txtSearchContact.Text <> "" Then
            Filter &= " (S_Address LIKE '%" & txtSearchContact.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Phone LIKE '%" & txtSearchContact.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Fax LIKE '%" & txtSearchContact.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Email LIKE '%" & txtSearchContact.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Contact_Name LIKE '%" & txtSearchContact.Text.Replace("'", "''") & "%' " & vbLf
            Filter &= " ) AND "
        End If


        If Filter <> "" Then DT.DefaultView.RowFilter = Filter.Substring(0, Filter.Length - 4)
        DT = DT.DefaultView.ToTable
        Session("Compare_Supplier") = DT
        PagerSupplier.SesssionSourceName = "Compare_Supplier"
        PagerSupplier.RenderLayout()

        If DT.Rows.Count = 0 Then
            lblTotalSupplier.Text = "ไม่พบรายการดังกล่าว"
        Else
            lblTotalSupplier.Text = "พบ " & FormatNumber(DT.Rows.Count, 0) & " รายการ"
        End If
    End Sub
    Protected Sub lnkAddSupplier_Click(sender As Object, e As System.EventArgs) Handles lnkAddSupplier.Click
        dialogSupplier.Visible = True
        BindSupplierList()

    End Sub
    Protected Sub lnkCloseSupplier_Click(sender As Object, e As System.EventArgs) Handles lnkCloseSupplier.Click
        dialogSupplier.Visible = False
    End Sub
    Protected Sub PagerSupplier_PageChanging(Sender As PageNavigation) Handles PagerSupplier.PageChanging
        PagerSupplier.TheRepeater = rptSupplierList
    End Sub

    Protected Sub rptSupplierList_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptSupplierList.ItemCommand
        Select Case e.CommandName
            Case "select"
                Dim _S_ID As Integer = e.CommandArgument

                Dim DT As New DataTable
                DT = CurrentS_ID_Compare()
                If DT.Rows.Count = 0 Then
                    S_ID_Compare &= "'" & _S_ID & "'"
                Else
                    S_ID_Compare = ""
                    For i As Integer = 0 To DT.Rows.Count - 1

                        S_ID_Compare &= "'" & DT.Rows(i).Item("S_ID") & "',"

                    Next
                    If S_ID_Compare <> "" Then
                        S_ID_Compare = "'" & _S_ID & "'," & S_ID_Compare.Substring(0, S_ID_Compare.Length - 1)
                    End If
                End If


                BindSupplierCompare()
                dialogSupplier.Visible = False
        End Select
    End Sub

    Protected Sub rptSupplierList_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptSupplierList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lbl_Tax As Label = e.Item.FindControl("lbl_Tax")
        Dim lbl_Supplier As Label = e.Item.FindControl("lbl_Supplier")
        Dim lbl_Sales As Label = e.Item.FindControl("lbl_Sales")
        Dim lbl_Contact As Label = e.Item.FindControl("lbl_Contact")

        lbl_Tax.Text = e.Item.DataItem("S_Tax_No").ToString
        lbl_Supplier.Text = e.Item.DataItem("S_Name").ToString
        If Not IsDBNull(e.Item.DataItem("S_Alias")) AndAlso e.Item.DataItem("S_Alias") <> "" Then
            lbl_Supplier.Text &= "(" & e.Item.DataItem("S_Alias") & ")"
        End If
        lbl_Sales.Text = e.Item.DataItem("S_Contact_Name").ToString
        Dim Contact As String = ""
        If Not IsDBNull(e.Item.DataItem("S_Phone")) AndAlso e.Item.DataItem("S_Phone") <> "" Then
            Contact &= "<br/><i class='icon-briefcase'></i> " & e.Item.DataItem("S_Phone")
        End If
        If Not IsDBNull(e.Item.DataItem("S_Email")) AndAlso e.Item.DataItem("S_Email") <> "" Then
            Contact &= "<br/><i class='icon-envelope'></i> " & e.Item.DataItem("S_Email")
        End If
        If Not IsDBNull(e.Item.DataItem("S_Fax")) AndAlso e.Item.DataItem("S_Fax") <> "" Then
            Contact &= "<br/><i class='icon-print'></i> " & e.Item.DataItem("S_Fax")
        End If
        If Contact <> "" Then
            lbl_Contact.Text = Contact.Substring(5)
        End If

        Dim btnSelect As Button = e.Item.FindControl("btnSelect")
        For i As Integer = 1 To 4
            Dim td_S As HtmlTableCell = e.Item.FindControl("td_S" & i)
            td_S.Attributes("onClick") = "document.getElementById('" & btnSelect.ClientID & "').click();"
        Next
        btnSelect.CommandArgument = e.Item.DataItem("S_ID")
    End Sub


#End Region

#Region "BindData"

    Private Sub BindSupplierCompare()
        'Dim SQL As String = " SELECT DISTINCT AVL_List .Ass_ID ,AVL_List .ReAss_ID , AVL_List.AVL_Y,AVL_List.AVL_M,AVL_List.AVL_No" & vbLf
        'SQL &= " ,dbo.udf_AVLCode(AVL_List.AVL_Y,AVL_List.AVL_M,AVL_List.AVL_Dept_ID,AVL_List.AVL_Sub_Dept_ID,AVL_List.AVL_No) AVL_Code" & vbLf
        'SQL &= " ,CASE WHEN AVL_List.ReAss_ID IS NULL THEN Ass.Ref_Code ELSE ReAss.Ref_Code END Ref_Code" & vbLf
        'SQL &= " ,AVL_List.Item_No,AVL_List.Item_Name,AVL_List.Dept_ID,AVL_List.Dept_Name,AVL_List .S_Name" & vbLf
        'SQL &= " ,AVL_List .S_ID ,AVL_List.Qty,AVL_List.UOM,AVL_List.Lot,AVL_List.Brand,AVL_List.Model,AVL_List.Color,AVL_List.Size,AVL_List.RecievedDate,AVL_List.Price" & vbLf
        'SQL &= " ,_File.*"
        'SQL &= " ,AVL_List.AVL_Status" & vbLf
        'SQL &= " FROM tb_AVL_List AVL_List" & vbLf
        'SQL &= " LEFT JOIN vw_Ass_Header Ass ON  Ass.Ass_ID=AVL_List.Ass_ID AND ReAss_ID IS NULL" & vbLf
        'SQL &= " LEFT JOIN vw_ReAss_Header ReAss  ON  AVL_List.Ass_ID IS NOT NULL AND ReAss.ReAss_ID = AVL_List.ReAss_ID " & vbLf
        'SQL &= " LEFT JOIN tb_Ass_File _File  ON _File.Ass_ID=Ass.Ass_ID OR _File.Ass_ID=ReAss.Ass_ID   AND _File.IsDefault =1" & vbLf
        'SQL &= " WHERE AVL_List.AVL_Status = 1 " & vbLf
        'SQL &= " AND ISNULL(Ass.Running_ID,ReAss.Running_ID) =" & Running_ID & ""

        'If S_ID_Compare <> "" Then
        '    SQL &= " AND AVL_List .S_ID  IN (" & S_ID_Compare & ")"
        'End If

        'Dim Filter As String = ""
        'Filter &= " AVL_List.Dept_ID IN (" & vbLf
        'Filter &= " SELECT D.Dept_ID FROM tb_Dept_Role R" & vbLf
        'Filter &= " INNER JOIN tb_User U ON R.User_ID=U.User_ID AND U.Active_Status=1" & vbLf
        'Filter &= " INNER JOIN tb_Dept D ON R.Dept_ID=D.Dept_ID AND D.Active_Status=1" & vbLf
        'Filter &= " INNER JOIN tb_Ass_Role AR ON R.AR_ID=AR.AR_ID" & vbLf
        'Filter &= " WHERE R.User_ID=" & User_ID & vbLf
        'Filter &= " ) AND "
        'If Filter <> "" Then
        '    SQL &= "AND " & Filter.Substring(0, Filter.Length - 4) & vbLf
        'End If

        'SQL &= " ORDER BY AVL_List.AVL_Y DESC,AVL_List.AVL_M DESC,AVL_List.AVL_No DESC" & vbLf


        Dim SQL As String = "" & vbLf
        SQL &= " SELECT  Ass.*, _File.* "
        SQL &= " FROM    vw_Ass_Header Ass  "
        SQL &= " INNER JOIN tb_Sup ON Ass.S_ID = tb_Sup.S_ID "
        SQL &= " LEFT JOIN tb_Ass_File _File  ON _File.Ass_ID=Ass.Ass_ID AND _File.IsDefault =1"

        If S_ID_Compare <> "" Then
            SQL &= " WHERE  Ass.S_ID  IN (" & S_ID_Compare & ")  AND Ass.Dept_ID = '" & Dept_ID & "'  AND  Ass.Running_ID='" & Running_ID & "'   AND Ass.Fail_Ass_ID  IS NULL "
        End If

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        DA.Fill(DT)

        If DT.Rows.Count > 0 Then
            lblCount_S.Text = "เปรียบเทียบผู้ขาย " & FormatNumber(DT.Rows.Count, 0) & " ราย"
        Else

        End If

        '------------- Binding To List ---------------
        rptCompare.DataSource = DT
        rptCompare.DataBind()

    End Sub

#End Region

    Protected Sub rptCompare_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptCompare.ItemCommand
        Select e.CommandName
            Case "View"
                BindSupplier(e.CommandArgument)
                dialogSupplierInfo.Visible = True

            Case "Remove"

                Dim DT As New DataTable
                DT = CurrentS_ID_Compare()
                Dim DTTemp As New DataTable
                DTTemp = DT.Copy()

                


                For i As Integer = 0 To DTTemp.Rows.Count - 1
                    If DTTemp.Rows(i).Item("S_ID") = e.CommandArgument Then
                        DT.Rows.RemoveAt(i)
                    End If
                Next

                If DT.Rows.Count = 0 Then
                    rptCompare.DataSource = Nothing
                    rptCompare.DataBind()
                    S_ID_Compare = ""

                    Exit Sub
                Else
                    S_ID_Compare = ""
                    For i As Integer = 0 To DT.Rows.Count - 1
                        S_ID_Compare &= "'" & DT.Rows(i).Item("S_ID") & "',"
                    Next
                    If S_ID_Compare <> "" Then
                        S_ID_Compare = S_ID_Compare.Substring(0, S_ID_Compare.Length - 1)
                    End If
                End If

                BindSupplierCompare()
        End Select

    End Sub

    Protected Sub rptCompare_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptCompare.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblS_Name As Label = e.Item.FindControl("lblS_Name")
        Dim lblS_ID As Label = e.Item.FindControl("lblS_ID")
        Dim btnViewDetail_S As LinkButton = e.Item.FindControl("btnViewDetail_S")
        Dim btnRemove_S As LinkButton = e.Item.FindControl("btnRemove_S")

        '------Detail------
        Dim lblDetail_Brand As Label = e.Item.FindControl("lblDetail_Brand")
        Dim lblDetail_Model As Label = e.Item.FindControl("lblDetail_Model")
        Dim lblDetail_Color As Label = e.Item.FindControl("lblDetail_Color")
        Dim lblDetail_Size As Label = e.Item.FindControl("lblDetail_Size")
        Dim lblDetail_RecievedDate As Label = e.Item.FindControl("lblDetail_RecievedDate")
        Dim lblPrice As Label = e.Item.FindControl("lblPrice")



        lblS_ID.Text = e.Item.DataItem("S_ID").ToString
        lblS_Name.Text = e.Item.DataItem("S_Name").ToString

        'Dim DT_Info As DataTable = Current_Info_Assessment(e.Item.DataItem("S_ID").ToString())
        'If (DT_Info.Rows.Count > 0) Then


        '    lblDetail_Brand.Text = DT_Info.Rows(0).Item("Brand").ToString()
        '    lblDetail_Model.Text = DT_Info.Rows(0).Item("Model").ToString()
        '    lblDetail_Color.Text = DT_Info.Rows(0).Item("Color").ToString()
        '    lblDetail_Size.Text = DT_Info.Rows(0).Item("Size").ToString()
        '    If Not IsDBNull(DT_Info.Rows(0).Item("RecievedDate")) Then
        '        lblDetail_RecievedDate.Text = GL.ReportThaiPassTime(DT_Info.Rows(0).Item("RecievedDate"))
        '    End If

        '    If Not IsDBNull(DT_Info.Rows(0).Item("Price")) Then
        '        lblPrice.Text = FormatNumber(DT_Info.Rows(0).Item("Price"), 2)
        '    Else
        '        lblPrice.Text = "-"
        '    End If
        'End If

        If e.Item.DataItem("Brand").ToString <> "" Then
            lblDetail_Brand.Text = e.Item.DataItem("Brand").ToString
        Else
            lblDetail_Brand.Text = "-"
        End If
        If e.Item.DataItem("Model").ToString <> "" Then
            lblDetail_Model.Text = e.Item.DataItem("Model").ToString
        Else
            lblDetail_Model.Text = "-"
        End If
        If e.Item.DataItem("Color").ToString <> "" Then
            lblDetail_Color.Text = e.Item.DataItem("Color").ToString
        Else
            lblDetail_Color.Text = "-"
        End If
        If e.Item.DataItem("Size").ToString <> "" Then
            lblDetail_Size.Text = e.Item.DataItem("Size").ToString
        Else
            lblDetail_Size.Text = "-"
        End If

        If Not IsDBNull(e.Item.DataItem("RecievedDate")) Then
            lblDetail_RecievedDate.Text = GL.ReportThaiPassTime(e.Item.DataItem("RecievedDate"))
        Else
            lblDetail_RecievedDate.Text = "-"
        End If

        If Not IsDBNull(e.Item.DataItem("Price")) Then
            lblPrice.Text = FormatNumber(e.Item.DataItem("Price"), 2)
        Else
            lblPrice.Text = "-"
        End If



        '----------------------------------
        Dim lnk_File_Dialog As HtmlAnchor = e.Item.FindControl("lnk_File_Dialog")
        Dim img_File As Image = e.Item.FindControl("img_File")
        Dim lbl_File_Name As Label = e.Item.FindControl("lbl_File_Name")
        Dim imgDefault As HtmlGenericControl = e.Item.FindControl("imgDefault")

        If IsDBNull(e.Item.DataItem("File_ID")) Then
            lnk_File_Dialog.Visible = False
        Else
            lnk_File_Dialog.Visible = True
            img_File.ImageUrl = "RenderFile.aspx?A=" & e.Item.DataItem("Ass_ID") & "&F=" & e.Item.DataItem("File_ID") & "&T=" & Now.ToOADate.ToString.Replace(".", "")
            lnk_File_Dialog.HRef = img_File.ImageUrl
            'imgDefault.Visible = Not IsDBNull(e.Item.DataItem("IsDefault")) AndAlso e.Item.DataItem("IsDefault")

            If GL.IsContentTypeImage(e.Item.DataItem("Content_Type").ToString) Then
                lnk_File_Dialog.Attributes("Title") = "Photo"
            Else
                lnk_File_Dialog.Attributes("Title") = "PDF"
                img_File.ImageUrl = "images/file_type/View_pdf.png"
            End If
        End If


        Dim rptHistory As Repeater = e.Item.FindControl("rptHistory")
        AddHandler rptHistory.ItemDataBound, AddressOf rptHistory_ItemDataBound
        Dim DT As New DataTable
        DT = GetHistory(e.Item.DataItem("S_ID"), Running_ID)


        rptHistory.DataSource = DT
        rptHistory.DataBind()
        btnViewDetail_S.CommandArgument = e.Item.DataItem("S_ID").ToString
        btnRemove_S.CommandArgument = e.Item.DataItem("S_ID").ToString


    End Sub

    'Public Function Current_Info_Assessment(ByRef S_ID As Integer) As DataTable

    '    Dim SQL As String = "SELECT * FROM vw_Ass_Header WHERE Dept_ID='" & Dept_ID & "' AND S_ID='" & S_ID & "' AND Running_ID='" & Running_ID & "' ORDER BY Ass_ID DESC"
    '    Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
    '    Dim DT As New DataTable
    '    DA.Fill(DT)


    '    'DT.Columns.Add("S_ID", GetType(Integer))

    '    'For Each Item As RepeaterItem In rptCompare.Items
    '    '    If Item.ItemType <> ListItemType.AlternatingItem And Item.ItemType <> ListItemType.Item Then Continue For
    '    '    Dim lblS_ID As Label = Item.FindControl("lblS_ID")
    '    '    Dim DR As DataRow = DT.NewRow
    '    '    DR("S_ID") = lblS_ID.Text
    '    '    DT.Rows.Add(DR)
    '    'Next
    '    Return DT
    'End Function

    Public Function GetHistory(ByRef S_ID As Integer, ByRef Running_ID As Integer) As DataTable

        Dim HT As DataTable = BL.GetAssessmentHeader_Compare(S_ID, Running_ID)
        If HT.Rows.Count = 0 Then

        End If
        '-------------------- Build History Table ----------------
        Dim DT As New DataTable
        DT.Columns.Add("Ass_Type")
        DT.Columns.Add("Ass_ID", GetType(Integer))
        DT.Columns.Add("ReAss_ID", GetType(Integer))
        DT.Columns.Add("Ref_Code")
        DT.Columns.Add("Dept_Name")
        DT.Columns.Add("Get_Score", GetType(Integer))
        DT.Columns.Add("Pass_Score", GetType(Integer))
        DT.Columns.Add("Max_Score", GetType(Integer))
        DT.Columns.Add("Pass_Status", GetType(Boolean))
        DT.Columns.Add("AVL_Y")
        DT.Columns.Add("AVL_M")
        DT.Columns.Add("AVL_Dept_ID")
        DT.Columns.Add("AVL_Sub_Dept_ID")
        DT.Columns.Add("AVL_No")
        DT.Columns.Add("AVL_Status")
        DT.Columns.Add("Current_Step", GetType(Integer))
        DT.Columns.Add("Step_TH")
        DT.Columns.Add("Update_Time", GetType(DateTime))

        '----------- Add First Record From HT-----------
        Dim HR As DataRow = DT.NewRow
        HR("Ass_Type") = "ประเมินผู้ขายใหม่"
        HR("Ass_ID") = HT.Rows(0).Item("Ass_ID")
        HR("ReAss_ID") = DBNull.Value
        HR("Ref_Code") = HT.Rows(0).Item("Ref_Code").ToString
        HR("Dept_Name") = HT.Rows(0).Item("Dept_Name").ToString
        HR("Get_Score") = HT.Rows(0).Item("Get_Score")
        HR("Pass_Score") = HT.Rows(0).Item("Pass_Score")
        HR("Max_Score") = HT.Rows(0).Item("Max_Score")
        HR("Pass_Status") = HT.Rows(0).Item("Pass_Status")

        HR("AVL_Y") = HT.Rows(0).Item("AVL_Y").ToString
        HR("AVL_M") = HT.Rows(0).Item("AVL_M").ToString
        HR("AVL_Dept_ID") = HT.Rows(0).Item("AVL_Dept_ID").ToString
        HR("AVL_Sub_Dept_ID") = HT.Rows(0).Item("AVL_Sub_Dept_ID").ToString
        HR("AVL_No") = HT.Rows(0).Item("AVL_No").ToString
        HR("AVL_Status") = HT.Rows(0).Item("AVL_Status")
        HR("Current_Step") = HT.Rows(0).Item("Current_Step")
        HR("Step_TH") = HT.Rows(0).Item("Step_TH").ToString
        HR("Update_Time") = HT.Rows(0).Item("Update_Time")
        DT.Rows.Add(HR)

        '------------- Get All ReAssessment -----------------
        Dim RT As DataTable = BL.GetReassessmentHeader_Compare(S_ID, Running_ID)
        RT.DefaultView.Sort = "Ref_Code"
        RT = RT.DefaultView.ToTable.Copy
        For i As Integer = 0 To RT.Rows.Count - 1
            Dim DR As DataRow = DT.NewRow
            DR("Ass_Type") = "ประเมินทบทวน"
            DR("Ass_ID") = RT.Rows(i).Item("Ass_ID")
            DR("ReAss_ID") = RT.Rows(i).Item("ReAss_ID")
            DR("Ref_Code") = RT.Rows(i).Item("Ref_Code").ToString
            DR("Dept_Name") = RT.Rows(i).Item("Dept_Name").ToString
            DR("Get_Score") = RT.Rows(i).Item("Get_Score")
            DR("Pass_Score") = RT.Rows(i).Item("Pass_Score")
            DR("Max_Score") = RT.Rows(i).Item("Max_Score")
            DR("Pass_Status") = RT.Rows(i).Item("Pass_Status")
            DR("AVL_Y") = RT.Rows(i).Item("AVL_Y").ToString
            DR("AVL_M") = RT.Rows(i).Item("AVL_M").ToString
            DR("AVL_Dept_ID") = RT.Rows(i).Item("AVL_Dept_ID").ToString
            DR("AVL_Sub_Dept_ID") = RT.Rows(i).Item("AVL_Sub_Dept_ID").ToString
            DR("AVL_No") = RT.Rows(i).Item("AVL_No").ToString
            DR("AVL_Status") = RT.Rows(i).Item("AVL_Status")
            DR("Current_Step") = RT.Rows(i).Item("Current_Step")
            DR("Step_TH") = RT.Rows(i).Item("Step_TH").ToString
            DR("Update_Time") = RT.Rows(i).Item("Update_Time")
            DT.Rows.Add(DR)
        Next
        Return DT
    End Function

    Protected Sub rptHistory_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs)
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        '------Detail------
        Dim lblRef_Code As Label = e.Item.FindControl("lblRef_Code")
        Dim lblUpdateDate As Label = e.Item.FindControl("lblUpdateDate")
        Dim lblScore As Label = e.Item.FindControl("lblScore")
        Dim lblAVL As Label = e.Item.FindControl("lblAVL")

        lblRef_Code.Text = e.Item.DataItem("Ref_Code").ToString

        If Not IsDBNull(e.Item.DataItem("Update_Time")) Then
            lblUpdateDate.Text = GL.ReportThaiDate(e.Item.DataItem("Update_Time"))
        Else
            lblUpdateDate.Text = ""
        End If
        BL.DisplayAssessmentGridResultLabel(e.Item.DataItem("Get_Score"), e.Item.DataItem("Pass_Score"), e.Item.DataItem("Max_Score"), e.Item.DataItem("Pass_Status"), lblScore)

        Dim Ref As String = e.Item.DataItem("Ref_Code").ToString.Substring(0, 1)
        Select Case Ref
            Case "A"
                lblRef_Code.Attributes("onClick") = "var win = window.open('Assessment_Edit.aspx?Ass_ID=" & e.Item.DataItem("Ass_ID").ToString & "','_blank'); win.focus();"
                lblRef_Code.Style("color") = "Teal"
            Case "R"
                lblRef_Code.Attributes("onClick") = "var win = window.open('Reassessment_Edit.aspx?Ass_ID=" & e.Item.DataItem("Ass_ID").ToString & "&ReAss_ID=" & e.Item.DataItem("ReAss_ID").ToString & "','_blank'); win.focus();"
                lblRef_Code.Style("color") = "Blue"
        End Select

        If (e.Item.DataItem("AVL_Y").ToString <> "") Then
            lblAVL.Text = "AVL" & e.Item.DataItem("AVL_Y").ToString & e.Item.DataItem("AVL_M").ToString & e.Item.DataItem("AVL_Dept_ID").ToString & e.Item.DataItem("AVL_Sub_Dept_ID").ToString & e.Item.DataItem("AVL_No").ToString
        Else
            lblAVL.Text = "-"
        End If


    End Sub

    Public Function CurrentS_ID_Compare() As DataTable

        Dim DT As New DataTable
        DT.Columns.Add("S_ID", GetType(Integer))

        For Each Item As RepeaterItem In rptCompare.Items
            If Item.ItemType <> ListItemType.AlternatingItem And Item.ItemType <> ListItemType.Item Then Continue For
            Dim lblS_ID As Label = Item.FindControl("lblS_ID")
            Dim DR As DataRow = DT.NewRow
            DR("S_ID") = lblS_ID.Text
            DT.Rows.Add(DR)
        Next
        Return DT
    End Function

    Private Sub BindSupplier(ByRef S_ID As Integer)
        Dim DT As DataTable = BL.GetSupplierDetail(S_ID)
        If DT.Rows.Count = 0 OrElse IsDBNull(DT.Rows(0).Item("S_ID")) Then
            lbl_S_Fullname.Text = "ไม่มีผู้ขายรายนี้ในระบบ"
            lbl_S_Fullname.Text = ""
            lbl_S_Alias.Text = ""
            lbl_S_Tax_No.Text = ""
            lbl_S_Address.Text = ""
            lbl_S_Contact_Name.Text = ""
            lbl_S_Email.Text = ""
            lbl_S_Phone.Text = ""
            lbl_S_Fax.Text = ""
        Else
            lbl_S_Fullname.Text = DT.Rows(0).Item("S_Name").ToString
            lbl_S_Alias.Text = DT.Rows(0).Item("S_Alias").ToString
            lbl_S_Tax_No.Text = DT.Rows(0).Item("S_Tax_No").ToString
            lbl_S_Address.Text = DT.Rows(0).Item("S_Address").ToString
            lbl_S_Contact_Name.Text = DT.Rows(0).Item("S_Contact_Name").ToString
            lbl_S_Email.Text = DT.Rows(0).Item("S_Email").ToString
            lbl_S_Phone.Text = DT.Rows(0).Item("S_Phone").ToString
            lbl_S_Fax.Text = DT.Rows(0).Item("S_Fax").ToString
        End If

    End Sub




    Protected Sub lnkCloseSupplierInfo_Click(sender As Object, e As System.EventArgs) Handles lnkCloseSupplierInfo.Click
        dialogSupplierInfo.Visible = False
    End Sub

    Private Declare Function GetKeyState Lib "user32" (ByVal nVirtKey As Long) As Integer

    Private Sub Form_KeyPress(KeyAscii As Integer)
        Dim CTRLKEY As Long
        Dim PKey As Long

        CTRLKEY = GetKeyState(17)
        PKey = GetKeyState(80)

        If (PKey = -127 Or PKey = -128) And (CTRLKEY = -127 Or CTRLKEY = -128) Then
            MsgBox("combination pressed")
        End If

    End Sub

    Protected Sub btnPrint_Click(sender As Object, e As System.EventArgs) Handles btnPrint.Click

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "ShowAlert('Success','กดปุ่ม Ctrl+P บนแป้นพิมพ์ เพื่อพิมพ์รายละเอียดเปรียบเทียบผู้ขาย','');", True)

    End Sub
End Class
