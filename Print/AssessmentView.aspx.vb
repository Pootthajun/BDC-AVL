﻿Imports System.Data.SqlClient
Imports CrystalDecisions.Web
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine

Partial Class Print_AssessmentView
    Inherits System.Web.UI.Page

    Dim C As New Converter
    Dim BL As New AVLBL
    Dim GL As New GenericLib

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Dim DT As DataTable = Session("Report_Assessment_View")
        Dim cc As New ReportDocument()
        cc.Load(Server.MapPath("../Report/AssessmentView.rpt"))
        cc.SetDataSource(DT)
        cc.SetParameterValue("Header", Session("Report_Assessment_View_Header"))
        cc.SetParameterValue("Title", Session("Report_Assessment_View_Title"))
        cc.SetParameterValue("CurDate", "จัดพิมพ์วันที่ " & GL.ReportThaiDate(Now))

        CrystalReportViewer1.ReportSource = cc
        Select Case Request.QueryString("Mode").ToUpper
            Case "PDF"
                Response.AddHeader("Content-Type", "application/pdf")
                Response.AppendHeader("Content-Disposition", "filename=การประเมิน_" & Now.Year & Now.Month.ToString.PadLeft(2, "0") & Now.Day.ToString.PadLeft(2, "0") & ".pdf")
                Dim B As Byte() = C.StreamToByte(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat))
                Response.BinaryWrite(B)
            Case "EXCEL"
                Response.AddHeader("Content-Type", "application/vnd.ms-excel")
                Response.AppendHeader("Content-Disposition", "filename=การประเมิน_" & Now.Year & Now.Month.ToString.PadLeft(2, "0") & Now.Day.ToString.PadLeft(2, "0") & ".xls")
                Dim B As Byte() = C.StreamToByte(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel))
                Response.BinaryWrite(B)
            Case Else

        End Select

    End Sub

End Class
