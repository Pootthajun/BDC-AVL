﻿Imports System.Data.SqlClient
Imports CrystalDecisions.Web
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine

Partial Class Print_RPT_Ass
    Inherits System.Web.UI.Page

    Dim C As New Converter
    Dim BL As New AVLBL


    Public ReadOnly Property Ass_ID() As Integer
        Get
            Return Request.QueryString("Ass_ID")
        End Get
    End Property

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
           If IsNothing(Session("User_ID")) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาล็อกอินเข้าสู่ระบบ');", True)
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If
        '----------------------------Header/Footer---------------------
        Dim SQL As String = ""
        SQL &= " SELECT * FROM vw_Ass_Header" & vbLf
        SQL &= " WHERE Ass_ID=" & Ass_ID & vbLf
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim SQL_Sub As String = ""
        SQL_Sub &= " 	SELECT * FROM tb_Ass_Detail" & vbLf
        SQL_Sub &= " 	WHERE Ass_ID=" & Ass_ID & vbLf
        SQL_Sub &= "    ORDER BY Q_GP_Order,Q_Order"
        Dim DA_Sub As New SqlDataAdapter(SQL_Sub, BL.ConnectionString)
        Dim DT_Sub As New DataTable
        DA_Sub.Fill(DT_Sub)

        Dim cc As New ReportDocument()
        cc.Load(Server.MapPath("../Report/RPT_Ass.rpt"))
        cc.Subreports("Ass").SetDataSource(DT_Sub)
        cc.SetDataSource(DT)

        ''--------------------------Paremeter-------------------------------------
        SQL = ""
        SQL &= " SELECT DISTINCT Ref_Code"
        SQL &= " ,dbo.udf_AVLCode(AVL_Y,AVL_M,AVL_Dept_ID,AVL_Sub_Dept_ID,AVL_No) AVL_Code,AVL_Status"
        SQL &= " FROM vw_Ass_Header "
        SQL &= " WHERE Ass_ID =" & Ass_ID & vbLf

        DA = New SqlDataAdapter(SQL, BL.ConnectionString)
        DT = New DataTable
        DA.Fill(DT)
        Dim AVL As String = ""
        If DT.Rows.Count = 1 Then
            If Not IsDBNull(DT.Rows(0).Item("AVL_Code")) And Not IsDBNull(DT.Rows(0).Item("AVL_Status")) Then
                AVL = DT.Rows(0).Item("AVL_Code").ToString
            Else
                AVL = "-"
            End If
        Else
            AVL = "-"
        End If

        cc.SetParameterValue("AVLRef", AVL.ToString)

        CrystalReportViewer1.ReportSource = cc
        Select Case Request.QueryString("Mode").ToUpper
            Case "PDF"
                Response.AddHeader("Content-Type", "application/pdf")
                Response.AppendHeader("Content-Disposition", "filename=Ass-" & DT.Rows(0).Item("Ref_Code").ToString & "_" & Now.Year & Now.Month.ToString.PadLeft(2, "0") & Now.Day.ToString.PadLeft(2, "0") & ".pdf")
                Dim B As Byte() = C.StreamToByte(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat))
                Response.BinaryWrite(B)
            Case "EXCEL"
                Response.AddHeader("Content-Type", "application/vnd.ms-excel")
                Response.AppendHeader("Content-Disposition", "filename=Ass-" & DT.Rows(0).Item("Ref_Code").ToString & "_" & Now.Year & Now.Month.ToString.PadLeft(2, "0") & Now.Day.ToString.PadLeft(2, "0") & ".xls")
                Dim B As Byte() = C.StreamToByte(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel))
                Response.BinaryWrite(B)
            Case Else
        End Select

    End Sub


End Class
