﻿Imports System.Data.SqlClient
Imports CrystalDecisions.Web
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine

Partial Class Print_BlackList_List
    Inherits System.Web.UI.Page

    Dim C As New Converter
    Dim BL As New AVLBL
    Dim GL As New GenericLib

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Dim DT As DataTable = Session("ReportBlackList")
        Dim cc As New ReportDocument()
        cc.Load(Server.MapPath("../Report/BlackList_List.rpt"))
        cc.SetDataSource(DT)
        cc.SetParameterValue("Title", Session("ReportBlackList_Title"))
        cc.SetParameterValue("CurDate", "วันที่จัดพิมพ์ " & GL.ReportThaiDate(Now))

        CrystalReportViewer1.ReportSource = cc
        Select Case Request.QueryString("Mode").ToUpper
            Case "PDF"
                Response.AddHeader("Content-Type", "application/pdf")
                Response.AppendHeader("Content-Disposition", "filename=รายงานผู้ขายที่ติด_BlackList_" & Now.Year & Now.Month.ToString.PadLeft(2, "0") & Now.Day.ToString.PadLeft(2, "0") & ".pdf")
                Dim B As Byte() = C.StreamToByte(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat))
                Response.BinaryWrite(B)
            Case "EXCEL"
                Response.AddHeader("Content-Type", "application/vnd.ms-excel")
                Response.AppendHeader("Content-Disposition", "filename=รายงานผู้ขายที่ติด_BlackList_" & Now.Year & Now.Month.ToString.PadLeft(2, "0") & Now.Day.ToString.PadLeft(2, "0") & ".xls")
                Dim B As Byte() = C.StreamToByte(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel))
                Response.BinaryWrite(B)
            Case Else

        End Select

    End Sub

End Class
