﻿Imports System.Data.SqlClient
Imports CrystalDecisions.Web
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine

Partial Class Print_RPT_ApprovedVenderList

    Inherits System.Web.UI.Page
    Dim C As New Converter
    Dim BL As New AVLBL
    Dim GL As New GenericLib

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("User_ID")) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาล็อกอินเข้าสู่ระบบ');", True)
            Response.Redirect("../Login.aspx")
            Exit Sub
        End If

        Dim DT As New DataTable
        Dim DR As DataRow
        DT = CType(Session("ApprovedVenderList"), DataTable).Copy

        DT.Columns.Add("Update_Time_TH", GetType(String))
        If DT.Rows.Count > 0 Then
            For i As Integer = 0 To DT.Rows.Count - 1
                DR = DT.Rows(i)
                DR("Update_Time_TH") = GL.ReportThaiPassTime(DT.Rows(i).Item("Update_Time"))
            Next
        End If

        Dim cc As New ReportDocument()
        cc.Load(Server.MapPath("../Report/RPT_ApprovedVenderList.rpt"))
        cc.SetDataSource(DT)
        cc.SetParameterValue("Title", Session("ApprovedVenderList_Title"))

        DT.Columns.Remove("Update_Time_TH")

        cc.SetParameterValue("lblCount", Session("RPT_ApprovedVenderList_Count"))

        cc.SetParameterValue("CreateName", Session("Title") & "" & Session("Fisrt_Name") & " " & Session("Last_Name"))
        'cc.SetParameterValue("CreateSubDept", Session("RPT_ApprovedVenderList_Count"))

        CrystalReportViewer1.ReportSource = cc
        Select Case Request.QueryString("Mode").ToUpper
            Case "PDF"
                Response.AddHeader("Content-Type", "application/pdf")
                Response.AppendHeader("Content-Disposition", "filename=ApprovedVenderList-" & "_" & Now.Year & Now.Month.ToString.PadLeft(2, "0") & Now.Day.ToString.PadLeft(2, "0") & ".pdf")
                Dim B As Byte() = C.StreamToByte(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat))
                Response.BinaryWrite(B)
            Case "EXCEL"
                Response.AddHeader("Content-Type", "application/vnd.ms-excel")
                Response.AppendHeader("Content-Disposition", "filename=ApprovedVenderList-" & "_" & Now.Year & Now.Month.ToString.PadLeft(2, "0") & Now.Day.ToString.PadLeft(2, "0") & ".xls")
                Dim B As Byte() = C.StreamToByte(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel))
                Response.BinaryWrite(B)
            Case Else
        End Select

    End Sub

End Class
