﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class SettingSupplierType
    Inherits System.Web.UI.Page

    Dim BL As New AVLBL

    Private ReadOnly Property PageName As String
        Get
            Return "SettingSupplierType.aspx"
        End Get
    End Property
    Private Property AccessMode As AVLBL.AccessRole
        Get
            Try
                Return ViewState("AccessMode")
            Catch ex As Exception
                Return AVLBL.AccessRole.None
            End Try
        End Get
        Set(value As AVLBL.AccessRole)
            ViewState("AccessMode") = value
        End Set
    End Property
    Private ReadOnly Property User_ID As Integer
        Get
            Try
                If IsNumeric(Session("User_ID")) Then
                    Return Session("User_ID")
                Else
                    Return -1
                End If
            Catch ex As Exception
                Return -1
            End Try
        End Get
    End Property


    Public Property IsActive As Boolean
        Get
            Return imgStatus.ImageUrl = "images/check.png"
        End Get
        Set(value As Boolean)
            If value Then
                imgStatus.ImageUrl = "images/check.png"
            Else
                imgStatus.ImageUrl = "images/none.png"
            End If
        End Set
    End Property

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("User_ID")) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "ShowAlert('warning','กรุณาล็อกอินเข้าสู่ระบบ','Login.aspx');", True)
            Response.Redirect("Login.aspx")
            Exit Sub
        End If

        If Not IsPostBack Then

            SetUserRole()
            ClearData()
        End If

    End Sub

    Private Sub SetUserRole()

        '----------- Check สิทธิ์ในเมนู --------------
        Dim MT As DataTable = BL.GetUserMenuRole(User_ID, PageName)
        If MT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลนี้','Overview.aspx');", True)
            Exit Sub
        End If
        AccessMode = MT.Rows(0).Item("MR_ID") '--------- สิทธิ์ในเมนูของผู้ใช้คนปัจจุบัน -----------
        If AccessMode = AVLBL.AccessRole.None Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลนี้','Overview.aspx');", True)
            Exit Sub
        End If

        btnAdd.Visible = AccessMode = AVLBL.AccessRole.Edit
        thEdit_Header.Visible = AccessMode = AVLBL.AccessRole.Edit
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As System.EventArgs) Handles btnAdd.Click
        pnlEdit.Visible = True
        pnlData.Visible = False
        txtName.Attributes.Add("ID", "")
    End Sub

    Sub ClearData()
        pnlEdit.Visible = False
        pnlData.Visible = True
        txtName.Text = ""
        IsActive = True
        BindData()
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
        ClearData()
    End Sub

    Sub BindData()
        Dim SQL As String = "SELECT * FROM tb_Sup_Type ORDER BY Active_Status DESC,S_Type_Name" & vbLf
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        rptData.DataSource = DT
        rptData.DataBind()
    End Sub

    Protected Sub rptData_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim lblId As Label = e.Item.FindControl("lblId")
        Dim lblName As Label = e.Item.FindControl("lblName")
        Dim imgStatus As Image = e.Item.FindControl("imgStatus")
        Dim tdEdit_List As HtmlTableCell = e.Item.FindControl("tdEdit_List")

        tdEdit_List.Visible = AccessMode = AVLBL.AccessRole.Edit
        lblId.Text = e.Item.DataItem("S_Type_ID").ToString
        lblName.Text = e.Item.DataItem("S_Type_Name").ToString

        If CBool(e.Item.DataItem("Active_Status")) = True Then
            imgStatus.ImageUrl = "images/check.png"
        Else
            imgStatus.ImageUrl = "images/none.png"
        End If
    End Sub

    Protected Sub rptData_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptData.ItemCommand

        Dim lblId As Label = e.Item.FindControl("lblId")
        Select Case e.CommandName
            Case "Edit"
                pnlEdit.Visible = True
                pnlData.Visible = False
                Dim lblName As Label = e.Item.FindControl("lblName")
                Dim imgStatus As Image = e.Item.FindControl("imgStatus")
                txtName.Attributes.Add("ID", lblId.Text)
                txtName.Text = lblName.Text
                IsActive = imgStatus.ImageUrl = "images/check.png"
               
            Case "Delete"
                Dim SQL As String = ""
                SQL = " SELECT * FROM tb_Ass_Header WHERE  S_Type_ID='" + lblId.Text + "'  "
                Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
                Dim DT As New DataTable
                DA.Fill(DT)
                If (DT.Rows.Count > 0) Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','ไม่สามารถลบข้อมูลได้ เนื่องจากมีการใช้งานข้อมูลนี้แล้วในระบบ','');", True)
                    Exit Sub
                End If


                SQL = "DELETE FROM tb_Sup_Type WHERE S_Type_ID = " & lblId.Text & vbCrLf
                Dim conn As New SqlConnection(BL.ConnectionString)
                conn.Open()
                Dim SqlCmd As SqlCommand
                SqlCmd = New SqlCommand(SQL, conn)
                SqlCmd.ExecuteNonQuery()
                conn.Close()
                ClearData()
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('success','ลบเรียบร้อย','');", True)
                Exit Sub

        End Select
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        If txtName.Text.Trim = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','กรุณากรอกข้อมูล ประเภทผู้ขาย','');", True)
            txtName.Focus()
            Exit Sub
        End If

        Dim SQL As String = ""
        Dim ID As String = ""
        If txtName.Attributes("ID").ToString <> "" Then
            ID = txtName.Attributes("ID").ToString
        End If

        Dim DR As DataRow
        'Add
        If ID = "" Then
            SQL &= "SELECT * FROM tb_Sup_Type WHERE S_Type_Name='" & txtName.Text.Replace("'", "''") & "'" & vbCrLf
            Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
            Dim DT As New DataTable
            DA.Fill(DT)
            If DT.Rows.Count > 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','ประเภทผู้ขาย ซ้ำกับข้อมูลในระบบ','');", True)
                txtName.Focus()
                Exit Sub
            End If

            DR = DT.NewRow
            DR("S_Type_ID") = BL.GetNewPrimaryID("tb_Sup_Type", "S_Type_ID")
            DR("S_Type_Name") = txtName.Text
            DR("Active_Status") = IsActive
            DR("Update_By") = Session("User_ID")
            DR("Update_Time") = Now
            DT.Rows.Add(DR)
            Dim cmd As New SqlCommandBuilder(DA)
            DA.Update(DT)
        Else
            'Edit
            SQL &= "SELECT * FROM tb_Sup_Type WHERE S_Type_Name='" & txtName.Text.Replace("'", "''") & "'" & vbCrLf
            SQL &= "AND S_Type_ID <> " & ID
            Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
            Dim DT As New DataTable
            DA.Fill(DT)
            If DT.Rows.Count > 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','ประเภทผู้ขาย ซ้ำกับข้อมูลในระบบ','');", True)
                txtName.Focus()
                Exit Sub
            End If

            SQL = "SELECT * FROM tb_Sup_Type WHERE S_Type_ID = " & ID
            DA = New SqlDataAdapter(SQL, BL.ConnectionString)
            DT = New DataTable
            DA.Fill(DT)

            DR = DT.Rows(0)
            DR("S_Type_Name") = txtName.Text
            DR("Active_Status") = IsActive
            DR("Update_By") = Session("User_ID")
            DR("Update_Time") = Now
            Dim cmd As New SqlCommandBuilder(DA)
            DA.Update(DT)
        End If
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('success','บันทึกเรียบร้อย','');", True)

        ClearData()
    End Sub

    Protected Sub imgStatus_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgStatus.Click
        IsActive = Not IsActive
    End Sub
End Class
