﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Reassessment_List
    Inherits System.Web.UI.Page

    Dim BL As New AVLBL
    Dim GL As New GenericLib

    Private ReadOnly Property PageName As String
        Get
            Return "ReAssessment_Edit.aspx"
        End Get
    End Property

    Private ReadOnly Property User_ID As Integer
        Get
            Try
                If IsNumeric(Session("User_ID")) Then
                    Return Session("User_ID")
                Else
                    Return -1
                End If
            Catch ex As Exception
                Return -1
            End Try
        End Get
    End Property

    Private Property HideRemark As Boolean
        Get
            Return ckHideRemark.Checked
        End Get
        Set(value As Boolean)
            ckHideRemark.Checked = value
        End Set
    End Property

    Private Property AccessMode As AVLBL.AccessRole
        Get
            Try
                Return ViewState("AccessMode")
            Catch ex As Exception
                Return AVLBL.AccessRole.None
            End Try
        End Get
        Set(value As AVLBL.AccessRole)
            ViewState("AccessMode") = value
        End Set
    End Property

    Public Property Last_Search_Dept As String
        Get
            Try
                Return Session("Assessment_List_Search_Dept")
            Catch ex As Exception
                Return ""
            End Try
        End Get
        Set(value As String)
            Session("Assessment_List_Search_Dept") = value
        End Set
    End Property

    Public Property Last_Search_Item As String
        Get
            Try
                Return Session("Assessment_List_Search_Item")
            Catch ex As Exception
                Return ""
            End Try
        End Get
        Set(value As String)
            Session("Assessment_List_Search_Item") = value
        End Set
    End Property

    Public Property Last_Search_Sup As String
        Get
            Try
                Return Session("Assessment_List_Search_Sup")
            Catch ex As Exception
                Return ""
            End Try
        End Get
        Set(value As String)
            Session("Assessment_List_Search_Sup") = value
        End Set
    End Property

    Public Property Last_Search_AVL As String
        Get
            Try
                Return Session("Assessment_List_Search_AVL")
            Catch ex As Exception
                Return ""
            End Try
        End Get
        Set(value As String)
            Session("Assessment_List_Search_AVL") = value
        End Set
    End Property

    Public Property Last_Search_Ref As String
        Get
            Try
                Return Session("Assessment_List_Search_Ref")
            Catch ex As Exception
                Return ""
            End Try
        End Get
        Set(value As String)
            Session("Assessment_List_Search_Ref") = value
        End Set
    End Property

    Public Property Last_Search_Step As Integer
        Get
            Try
                Return Session("Assessment_List_Search_Step")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Integer)
            Session("Assessment_List_Search_Step") = value
        End Set
    End Property
    Public Property Last_Search_Last As Integer
        Get
            Try
                Return Session("Assessment_List_Search_Last")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Integer)
            Session("Assessment_List_Search_Last") = value
        End Set
    End Property
    Public Property Last_Search_Page As Integer
        Get
            Try
                Return Session("Assessment_List_Search_Page")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Integer)
            Session("Assessment_List_Search_Page") = value
        End Set
    End Property

    Private Sub CollectLastCriteria()
        Last_Search_Dept = ddl_Search_Dept.SelectedValue
        Last_Search_Item = txt_Search_Item.Text
        Last_Search_Sup = txt_Search_Sup.Text
        Last_Search_AVL = txt_Search_AVL.Text
        Last_Search_Ref = txt_Search_Ref.Text
        Last_Search_Step = ddl_Search_Step.SelectedValue
        Last_Search_Last = ddlLastUpdate.SelectedIndex
        Last_Search_Page = Pager.CurrentPage
        'เพิ่มอีก 3 criteria
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("User_ID")) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "ShowAlert('warning','กรุณาล็อกอินเข้าสู่ระบบ','Login.aspx');", True)
            Response.Redirect("Login.aspx")
            Exit Sub
        End If

        '--------------- Check Role (Remain)-------------------

        If Not IsPostBack Then

            If Request.QueryString("Back") = Nothing Then
                Last_Search_Dept = ddl_Search_Dept.SelectedIndex = 0
                Last_Search_Item = ""
                Last_Search_Sup = ""
                Last_Search_AVL = ""
                Last_Search_Ref = ""
                Last_Search_Last = 4
                Last_Search_Step = 0
                Last_Search_Page = Pager.CurrentPage
            End If


            Dim MT As DataTable = BL.GetUserMenuRole(User_ID, PageName)
            If MT.Rows.Count = 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลนี้','Overview.aspx');", True)
                Exit Sub
            End If
            AccessMode = MT.Rows(0).Item("MR_ID") '--------- สิทธิ์ของผู้ใช้คนปัจจุบัน -----------
            If AccessMode = AVLBL.AccessRole.None Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลนี้','Overview.aspx');", True)
                Exit Sub
            End If


            '------------- Restore Last Search Criteria------------
            BL.BindDDlDEPT(ddl_Search_Dept, User_ID, Last_Search_Dept)
            txt_Search_Item.Text = Last_Search_Item
            txt_Search_Sup.Text = Last_Search_Sup
            txt_Search_AVL.Text = Last_Search_AVL
            txt_Search_Ref.Text = Last_Search_Ref
            ddl_Search_Step.SelectedValue = Last_Search_Step
            ddlLastUpdate.SelectedIndex = Last_Search_Last

            ClearSearchForm()
            BindList()

            '-----------------Check permission for creating new assessment----------------
            dialogAssessmentHistory.Visible = False

        End If
    End Sub

    Private Sub ClearSearchForm()
        ddlStart_M.Items.Clear()
        ddlEnd_M.Items.Clear()
        For i As Integer = 1 To 12
            ddlStart_M.Items.Add(New ListItem(GL.ReportMonthThaiShort(i), i))
            ddlEnd_M.Items.Add(New ListItem(GL.ReportMonthThaiShort(i), i))
        Next
        ddlEnd_M.SelectedIndex = 11

        ddlStart_Y.Items.Clear()
        ddlEnd_Y.Items.Clear()
        'Now.Year - 2
        For i As Integer = Now.Year - 2 To Now.Year + 8
            ddlStart_Y.Items.Add(New ListItem(i + 543, i))
            ddlEnd_Y.Items.Add(New ListItem(i + 543, i))
        Next
        ddlEnd_Y.SelectedIndex = ddlEnd_Y.Items.Count - 1
    End Sub
    Protected Sub ddlPeriod_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlEnd_M.SelectedIndexChanged, ddlEnd_Y.SelectedIndexChanged, ddlStart_M.SelectedIndexChanged, ddlStart_Y.SelectedIndexChanged

        Dim StartMonth As Integer = (ddlStart_Y.SelectedValue * 12) + ddlStart_M.SelectedValue
        Dim EndMonth As Integer = (ddlEnd_Y.SelectedValue * 12) + ddlEnd_M.SelectedValue

        If StartMonth > EndMonth Then
            Select Case True
                Case Equals(sender, ddlStart_M) Or Equals(sender, ddlStart_Y)
                    ddlEnd_M.SelectedIndex = ddlStart_M.SelectedIndex
                    ddlEnd_Y.SelectedIndex = ddlStart_Y.SelectedIndex
                Case Equals(sender, ddlEnd_M) Or Equals(sender, ddlEnd_Y)
                    ddlStart_M.SelectedIndex = ddlEnd_M.SelectedIndex
                    ddlStart_Y.SelectedIndex = ddlEnd_Y.SelectedIndex
            End Select
        End If

    End Sub

    Protected Sub Search_Changed(sender As Object, e As System.EventArgs) Handles btnSearch.Click ', ddl_Search_Dept.SelectedIndexChanged, txt_Search_Item.TextChanged, txt_Search_AVL.TextChanged, txt_Search_Sup.TextChanged, txt_Search_Ref.TextChanged, ddl_Search_Step.SelectedIndexChanged, ddlLastUpdate.SelectedIndexChanged, ddlAssResult.SelectedIndexChanged
        BindList()
    End Sub

    Private Sub BindList()
        Dim StartMonth As Integer = (ddlStart_Y.SelectedValue * 12) + ddlStart_M.SelectedValue
        Dim EndMonth As Integer = (ddlEnd_Y.SelectedValue * 12) + ddlEnd_M.SelectedValue
        Dim SQL As String = "DECLARE @S AS INT=" & StartMonth & vbLf
        SQL &= "DECLARE @E AS INT=" & EndMonth & vbLf & vbLf
        SQL &= " SELECT * FROM" & vbLf
        SQL &= " (SELECT " & vbLf
        SQL &= " RANK() OVER(PARTITION BY Header.Ass_ID ORDER BY dbo.udf_ReAssCode(ReAss.Ref_Year,ReAss.Ref_Month,Header.Dept_ID,ReAss.Ref_Number) DESC) LastAss," & vbLf
        SQL &= " Header.Ass_ID,dbo.udf_AssCode(Header.Ref_Year,Header.Ref_Month,Header.Dept_ID,Header.Ref_Number) Ass_Ref_Code," & vbLf
        SQL &= " ReAss.ReAss_ID,CASE WHEN ReAss.Ref_Year IS NULL THEN NULL ELSE dbo.udf_ReAssCode(ReAss.Ref_Year,ReAss.Ref_Month,Header.Dept_ID,ReAss.Ref_Number) END ReAss_Ref_Code," & vbLf
        SQL &= " tb_Cat.Cat_No + tb_Sub_Cat.Sub_Cat_No + tb_Type.Type_No+tb_Running.Running_No Item_Code ,tb_Running.Running_No,tb_Running.Running_Name,Header.Item_No,Header.Item_Name,Header.DEPT_ID,Header.Dept_Name,Header.Sub_Dept_ID,Header.Sub_Dept_Name ," & vbLf
        SQL &= " tb_Cat.Cat_Name ,tb_Sub_Cat.Sub_Cat_Name  ,tb_Type.Type_Name,tb_Cat.Cat_No  ,tb_Sub_Cat.Sub_Cat_No  ,tb_Type.Type_No, " & vbLf
        SQL &= " S_Tax_No,Header.S_Name,S_Alias,S_Address,S_Phone,S_Fax,S_Contact_Name,S_Email," & vbLf
        SQL &= " ISNULL(ReAss.Get_Score,Header.Get_Score) Get_Score,ISNULL(ReAss.Pass_Score,Header.Pass_Score) Pass_Score,ISNULL(ReAss.Max_Score,Header.Max_Score) Max_Score,ISNULL(ISNULL(ReAss.Pass_Status,Header.Pass_Status),1) Pass_Status," & vbLf

        SQL &= " ISNULL(ReAss.AVL_Status,Header.AVL_Status) AVL_Status,ISNULL(ReAss.Current_Step,Header.Current_Step) Current_Step," & vbLf

        SQL &= " dbo.udf_AVLCode(Header.AVL_Y,Header.AVL_M,Header.AVL_Dept_ID,Header.AVL_Sub_Dept_ID,Header.AVL_No) AssAVL_Code" & vbLf
        SQL &= " ,dbo.udf_AVLCode(ReAss.AVL_Y,ReAss.AVL_M,ReAss.AVL_Dept_ID,ReAss.AVL_Sub_Dept_ID,ReAss.AVL_No) ReAVL_Code" & vbLf
        SQL &= " ,CASE WHEN ReAss.ReAss_ID IS NULL THEN " & vbLf
        SQL &= "        dbo.udf_AVLCode(Header.AVL_Y, Header.AVL_M, Header.AVL_Dept_ID, Header.AVL_Sub_Dept_ID, Header.AVL_No)" & vbLf
        SQL &= " ELSE	" & vbLf
        SQL &= "        dbo.udf_AVLCode(ReAss.AVL_Y, ReAss.AVL_M, ReAss.AVL_Dept_ID, ReAss.AVL_Sub_Dept_ID, ReAss.AVL_No)" & vbLf
        SQL &= " END AVL_Code," & vbLf
        SQL &= " Step_TH,ISNULL(ReAss.Update_Time,Header.Update_Time) Update_Time" & vbLf
        SQL &= " ,ReAss.Role_1_Comment,ReAss.Role_2_Comment,ReAss.Role_3_Comment,ReAss.Role_4_Comment,ReAss.Flag_Return" & vbLf


        SQL &= " ,ReAss.Role_1_ID,ReAss.Role_1_Title,ReAss.Role_1_Fisrt_Name,ReAss.Role_1_Last_Name,ReAss.Role_1_Pos_Name" & vbLf
        SQL &= " ,ReAss.Role_2_ID,ReAss.Role_2_Title,ReAss.Role_2_Fisrt_Name,ReAss.Role_2_Last_Name,ReAss.Role_2_Pos_Name" & vbLf
        SQL &= " ,ReAss.Role_3_ID,ReAss.Role_3_Title,ReAss.Role_3_Fisrt_Name,ReAss.Role_3_Last_Name,ReAss.Role_3_Pos_Name" & vbLf
        SQL &= " ,ReAss.Role_4_ID,ReAss.Role_4_Title,ReAss.Role_4_Fisrt_Name,ReAss.Role_4_Last_Name,ReAss.Role_4_Pos_Name" & vbLf

        SQL &= " " & vbLf
        SQL &= " FROM tb_Ass_Header Header" & vbLf
        SQL &= " LEFT JOIN tb_ReAss_Header ReAss ON Header.Ass_ID=ReAss.Ass_ID " & vbLf
        SQL &= "  LEFT JOIN tb_Running ON Header.Item_Type=tb_Running.Running_ID --AND Header.Dept_ID=tb_Running.Dept_ID" & vbLf
        SQL &= " LEFT JOIN tb_Type ON tb_Running.Type_ID=tb_Type.Type_ID --AND Header.Dept_ID=tb_Type.Dept_ID" & vbLf
        SQL &= " LEFT JOIN tb_Sub_Cat ON tb_Type.Sub_Cat_ID=tb_Sub_Cat.Sub_Cat_ID --AND tb_Type.Dept_ID=tb_Sub_Cat.Dept_ID" & vbLf
        SQL &= " LEFT JOIN tb_Cat ON tb_Sub_Cat.Cat_ID=tb_Cat.Cat_ID --AND tb_Sub_Cat.Dept_ID=tb_Cat.Dept_ID" & vbLf
        SQL &= " LEFT JOIN tb_Step Step ON ISNULL(ReAss.Current_Step,Header.Current_Step)=Step.Step_ID" & vbLf
        SQL &= " WHERE Header.Current_Step=5 AND Header.Pass_Status =1 " & vbLf
        SQL &= " ) A WHERE LastAss=1  " & vbLf  '----------ประเมินทบทวนเฉพาะ กรณีประเมินผู้ขายใหม่ผ่านเท่านั้น 

        SQL &= " AND Ass_ID NOT IN ( "
        SQL &= " SELECT Fail_Ass_ID FROM tb_Ass_Header  WHERE Fail_Ass_ID IS NOT NULL "
        SQL &= " ) " & vbLf

        Dim Title As String = ""
        Dim Filter As String = ""
        If ddl_Search_Dept.SelectedIndex > 0 Then
            SQL &= " AND Dept_ID='" & ddl_Search_Dept.SelectedValue & "' " & vbLf
            Title &= " " & ddl_Search_Dept.Items(ddl_Search_Dept.SelectedIndex).Text
        End If
        If ddl_Search_SUB_DEPT.SelectedIndex > 0 Then
            SQL &= " AND Sub_Dept_ID='" & ddl_Search_SUB_DEPT.SelectedValue & "'  "
            Title &= " ของฝ่าย/ภาค/งาน " & ddl_Search_SUB_DEPT.Items(ddl_Search_SUB_DEPT.SelectedIndex).Text
        End If
        If txt_Search_Item.Text <> "" Then
            SQL &= " AND (" & vbLf
            SQL &= " Item_No LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR " & vbLf
            SQL &= " Item_Name LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR " & vbLf
            SQL &= " CAT_Name LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR " & vbLf
            SQL &= " Sub_Cat_Name LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR " & vbLf
            SQL &= " Type_Name LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR" & vbLf
            SQL &= " Running_Name LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR" & vbLf
            SQL &= " Cat_No + Sub_Cat_No+Type_No+Running_No LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' " & vbLf
            SQL &= ") " & vbLf
            Title &= " พัสดุ/ครุภัณฑ์ จัดจ้างและบริการ : " & txt_Search_Item.Text
        End If
        If txt_Search_Sup.Text <> "" Then
            SQL &= " AND (" & vbLf
            SQL &= " S_Tax_No LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            SQL &= " S_Name LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            SQL &= " S_Alias LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            SQL &= " S_Address LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            SQL &= " Replace(Replace(S_Phone,'-',''),',','') LIKE '%" & txt_Search_Sup.Text.Replace("'", "''").Replace("-", "") & "%' OR " & vbLf
            SQL &= " Replace(Replace(S_Fax,'-',''),',','') LIKE '%" & txt_Search_Sup.Text.Replace("'", "''").Replace("-", "") & "%' OR " & vbLf
            SQL &= " S_Contact_Name LIKE '%" & txt_Search_Sup.Text.Replace("'", "''").Replace("-", "") & "%' OR " & vbLf
            SQL &= " S_Email LIKE '%" & txt_Search_Sup.Text.Replace("'", "''").Replace("-", "") & "%' " & vbLf
            SQL &= ")" & vbLf
            Title &= " ผู้ขาย : " & txt_Search_Sup.Text
        End If
        If txt_Search_Ref.Text <> "" Then
            SQL &= " AND (Ass_Ref_Code LIKE '%" & txt_Search_Ref.Text.Replace("'", "''") & "%' OR ReAss_Ref_Code LIKE '%" & txt_Search_Ref.Text.Replace("'", "''") & "%' )" & vbLf
            Title &= " เลขใบประเมิน : " & txt_Search_Ref.Text
        End If
        Select Case ddlLastUpdate.SelectedIndex
            Case 1 'ภายใน 1 ปี
                SQL &= " AND DATEDIFF(MONTH,Update_Time,GetDate())<=12 " & vbLf
            Case 2 '1-2 ปี
                SQL &= " AND DATEDIFF(MONTH,Update_Time,GetDate())>=12 AND DATEDIFF(MONTH,Update_Time,GetDate())<=24 " & vbLf
            Case 3 'ไม่เกิน 2 ปี
                SQL &= " AND DATEDIFF(MONTH,Update_Time,GetDate())<=24 " & vbLf
            Case 4 '2 ปีขึ้นไป
                SQL &= " AND DATEDIFF(MONTH,Update_Time,GetDate())>=24 " & vbLf
        End Select
        'Select Case ddlLastUpdate.SelectedIndex
        '    Case 1 'ภายใน 1 ปี
        '        SQL &= " AND DATEDIFF(MONTH,Update_Time,GetDate())<=1 " & vbLf
        '    Case 2 '1-2 ปี
        '        SQL &= " AND DATEDIFF(MONTH,Update_Time,GetDate())>=1 AND DATEDIFF(MONTH,Update_Time,GetDate())<=2 " & vbLf
        '    Case 3 'ไม่เกิน 2 ปี
        '        SQL &= " AND DATEDIFF(MONTH,Update_Time,GetDate())<=2 " & vbLf
        '    Case 4 '2 ปีขึ้นไป
        '        SQL &= " AND DATEDIFF(MONTH,Update_Time,GetDate())>=2 " & vbLf
        'End Select
        If txt_Search_AVL.Text <> "" Then
            SQL &= " AND AVL_Code LIKE '%" & txt_Search_AVL.Text.Replace("'", "''") & "%' " & vbLf
            Title &= " เลข AVL : " & txt_Search_AVL.Text
        End If
        Select Case ddlLastUpdate.SelectedIndex
            Case 1 'ภายใน 1 ปี
                Title &= " ช่วงประเมินภายใน 1 ปี"
            Case 2 '1-2 ปี
                Title &= " ช่วงประเมินระหว่าง 1-2 ปี"
            Case 3 'ไม่เกิน 2 ปี
                Title &= " ช่วงประเมินไม่เกิน 2 ปี"
            Case 4 '2 ปีขึ้นไป
                Title &= " ประเมินมาแล้วมากกว่า 2 ปี"
        End Select
        
        If ddl_Search_Step.SelectedIndex > 0 Then
            If ddl_Search_Step.SelectedIndex = 1 Then
                SQL &= " AND ReAss_ID IS NULL" & vbLf
            ElseIf ddl_Search_Step.SelectedIndex = ddl_Search_Step.Items.Count - 1 Then
                SQL &= " AND Current_Step=" & ddl_Search_Step.SelectedValue & " AND ReAss_ID IS NOT NULL " & vbLf
            Else
                SQL &= " AND Current_Step=" & ddl_Search_Step.SelectedValue & vbLf
            End If
            If ddl_Search_Step.SelectedIndex = 1 Then
                Title &= " แสดงเฉพาะการประเมินผู้ขายใหม่ที่ยังไม่ได้ประเมินทบทวน"
            Else
                Title &= " ขั้นตอน" & ddl_Search_Step.Items(ddl_Search_Step.SelectedIndex).Text
            End If

        End If
        Select Case ddlAssResult.SelectedIndex
            Case 1
                SQL &= " AND Get_Score>=Pass_Score " & vbLf
                Title &= " เฉพาะที่ผ่านการประเมิน"
            Case 2
                SQL &= " AND ISNULL(Get_Score,0)<ISNULL(Pass_Score,0) " & vbLf
                Title &= " เฉพาะที่ไม่ผ่านการประเมิน"
        End Select

        SQL &= " AND dbo.UDF_IsRangeIntersected(@S,@E,Month(Update_Time)+(Year(Update_Time)*12),Month(Update_Time)+(Year(Update_Time)*12))=1  " & vbLf
        Title &= " รายการประเมินในช่วง " & ddlStart_M.Items(ddlStart_M.SelectedIndex).Text & " " & ddlStart_Y.Items(ddlStart_Y.SelectedIndex).Text
        Title &= " ถึง "
        Title &= ddlStart_M.Items(ddlEnd_M.SelectedIndex).Text & " " & ddlEnd_Y.Items(ddlEnd_Y.SelectedIndex).Text & " "


        SQL &= " ORDER BY  Dept_ID ,Update_Time" & vbLf

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        DA.Fill(DT)

        Session("Search_ReAssessment_List") = DT
        Session("Search_ReAssessment_List_Title") = Title

        lblTotalRecord.Text = Title
        If DT.Rows.Count = 0 Then
            lblTotalRecord.Text &= " ไม่พบรายการดังกล่าว"
        Else
            lblTotalRecord.Text &= " พบ " & FormatNumber(DT.Rows.Count, 0) & " รายการ"
        End If
        LastDept = ""
        '------------- Binding To List ---------------
        Pager.SesssionSourceName = "Search_ReAssessment_List"
        'Pager.CurrentPage = Last_Search_Page
        Pager.RenderLayout()

    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
        '----------- Collect Search Keyword To Session ----------
        CollectLastCriteria()
    End Sub

    Protected Sub rptList_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Select Case e.CommandName
            Case "View"
                Dim btnView As LinkButton = e.Item.FindControl("btnView")
                '----------- Collect Search Keyword To Session ----------
                CollectLastCriteria()
                '---------------- Check role for department -------------
                Dim lblDept As Label = e.Item.FindControl("lblDept")
                Dim _dept_id As String = lblDept.Attributes("Dept_ID")
                Dim RT As DataTable = BL.GetUserDeptRole(User_ID, _dept_id)
                If RT.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่สามารถเข้าถึงใบประเมินทบทวนดังกล่าวได้','');", True)
                    Exit Sub
                End If
                '---------------- Redirect---------------
                Dim Ass_ID As Integer = btnView.CommandArgument
                AssID = Ass_ID
                BindAssHistory()

        End Select
    End Sub

    Dim LastDept As String = ""
    Protected Sub rptList_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblAssNo As Label = e.Item.FindControl("lblAssNo")
        Dim lblRef As Label = e.Item.FindControl("lblRef")
        Dim lblItem As Label = e.Item.FindControl("lblItem")
        Dim lblSub_Dept_Name As Label = e.Item.FindControl("lblSub_Dept_Name")
        Dim lblSup As Label = e.Item.FindControl("lblSup")
        Dim lblAVL As Label = e.Item.FindControl("lblAVL")
        Dim lblResult As Label = e.Item.FindControl("lblResult")
        Dim lblResultText As Label = e.Item.FindControl("lblResultText")
        Dim lblUpdate As Label = e.Item.FindControl("lblUpdate")
        Dim lblStep As Label = e.Item.FindControl("lblStep")
        Dim lblReassResult As Label = e.Item.FindControl("lblReassResult")
        Dim imgAlert As Image = e.Item.FindControl("imgAlert")
        Dim lblAssessor As Label = e.Item.FindControl("lblAssessor")

        Dim trRemark As HtmlTableRow = e.Item.FindControl("trRemark")
        Dim lblRole_1_Comment As Label = e.Item.FindControl("lblRole_1_Comment")
        Dim lblRole_2_Comment As Label = e.Item.FindControl("lblRole_2_Comment")
        Dim lblRole_3_Comment As Label = e.Item.FindControl("lblRole_3_Comment")
        Dim lblRole_4_Comment As Label = e.Item.FindControl("lblRole_4_Comment")

        Dim btnView As LinkButton = e.Item.FindControl("btnView")
        Dim pnl_icon As Panel = e.Item.FindControl("pnl_icon")

        Dim trDept As HtmlTableRow = e.Item.FindControl("trDept")
        Dim lblDept As Label = e.Item.FindControl("lblDept")
        If LastDept <> e.Item.DataItem("Dept_Name").ToString Then
            LastDept = e.Item.DataItem("Dept_Name").ToString
            lblDept.Text = LastDept
            trDept.Visible = True
        Else
            trDept.Visible = False
        End If
        lblDept.Attributes("Dept_ID") = e.Item.DataItem("Dept_ID")
        lblSub_Dept_Name.Text = e.Item.DataItem("Sub_Dept_Name").ToString
        lblAssNo.Text = e.Item.DataItem("Ass_Ref_Code").ToString
        lblRef.Text = e.Item.DataItem("ReAss_Ref_Code").ToString
        lblItem.Text = "(" & e.Item.DataItem("Item_Code").ToString & ") " & e.Item.DataItem("Item_Name").ToString.Replace(vbLf, "<br>")
        'lblSup.Text = e.Item.DataItem("S_Alias").ToString
        lblSup.Text = e.Item.DataItem("S_Name").ToString
        lblAVL.Text = e.Item.DataItem("AVL_Code").ToString

        ' แสดง lbl สถานะของการประเมิน  เสร็จแล้ว , ยังไม่เสร็จ
        If (e.Item.DataItem("Current_Step").ToString() = 5) Then
            lblResultText.Text = "(เสร็จแล้ว)"
            lblResultText.Style("color") = "green"
        Else
            lblResultText.Text = "(ยังไม่เสร็จ)"
            lblResultText.Style("color") = "red"
        End If

        '------Remark--------
        lblRole_1_Comment.Text = "ผู้จัดทำ : " & e.Item.DataItem("Role_1_Comment").ToString
        lblRole_2_Comment.Text = "ผู้ตรวจสอบ : " & e.Item.DataItem("Role_2_Comment").ToString
        lblRole_3_Comment.Text = "เจ้าหน้าที่พัสดุ : " & e.Item.DataItem("Role_3_Comment").ToString
        lblRole_4_Comment.Text = "หัวหน้าฝ่ายบริหารทั่วไป : " & e.Item.DataItem("Role_4_Comment").ToString

        If HideRemark Or (e.Item.DataItem("Role_1_Comment").ToString = "" And e.Item.DataItem("Role_2_Comment").ToString = "" And e.Item.DataItem("Role_3_Comment").ToString = "" And e.Item.DataItem("Role_4_Comment").ToString = "") Then
            trRemark.Style.Item("display") = "none"
        End If


        If Not IsDBNull(e.Item.DataItem("Flag_Return")) Then
            pnl_icon.Visible = e.Item.DataItem("Flag_Return")
        End If

        '----ประเมินผู้ขายใหม่
        BL.DisplayAssessmentGridResultLabel(e.Item.DataItem("Get_Score"), e.Item.DataItem("Pass_Score"), e.Item.DataItem("Max_Score"), e.Item.DataItem("Pass_Status"), lblResult)
        lblUpdate.Text = GL.ReportThaiPassTime(e.Item.DataItem("Update_Time"))
        If DateDiff(DateInterval.Month, e.Item.DataItem("Update_Time"), Now) > 23 Then
            imgAlert.Visible = True
            lblUpdate.ForeColor = Drawing.Color.Red
        Else
            imgAlert.Visible = False
        End If

        If lblRef.Text = "" Then
            lblStep.Text = "ยังไม่ได้ทบทวน"
            lblStep.ForeColor = Drawing.Color.LightGray
        Else
            lblStep.Text = e.Item.DataItem("Step_TH").ToString
            If Not IsDBNull(e.Item.DataItem("Current_Step")) Then
                lblStep.ForeColor = BL.GetStepColor(e.Item.DataItem("Current_Step"))
            Else
                lblStep.ForeColor = BL.GetStepColor(e.Item.DataItem("Current_Step"))
            End If
        End If

        Dim Creater As String = Trim(e.Item.DataItem("Role_1_Fisrt_Name") & " " & e.Item.DataItem("Role_1_Last_Name"))
        Dim Auditor As String = Trim(e.Item.DataItem("Role_2_Fisrt_Name") & " " & e.Item.DataItem("Role_2_Last_Name"))
        Dim Supply_Officer As String = Trim(e.Item.DataItem("Role_3_Fisrt_Name") & " " & e.Item.DataItem("Role_3_Last_Name"))
        Dim Director As String = Trim(e.Item.DataItem("Role_4_Fisrt_Name") & " " & e.Item.DataItem("Role_4_Last_Name"))
        Select Case CType(e.Item.DataItem("Current_Step"), AVLBL.AssessmentStep)
            Case AVLBL.AssessmentStep.Creater
                lblAssessor.Text = Creater
            Case AVLBL.AssessmentStep.Auditor
                If Auditor <> "" Then
                    lblAssessor.Text = Auditor
                Else
                    lblAssessor.Text = Creater
                End If
            Case AVLBL.AssessmentStep.Supply_Officer
                If Supply_Officer <> "" Then
                    lblAssessor.Text = Supply_Officer
                ElseIf Auditor <> "" Then
                    lblAssessor.Text = Auditor
                Else
                    lblAssessor.Text = Creater
                End If
            Case AVLBL.AssessmentStep.Director, AVLBL.AssessmentStep.Completed

                If Director <> "" Then
                    lblAssessor.Text = Director
                ElseIf Supply_Officer <> "" Then
                    lblAssessor.Text = Supply_Officer
                ElseIf Auditor <> "" Then
                    lblAssessor.Text = Auditor
                Else
                    lblAssessor.Text = Creater
                End If
            Case Else
        End Select

        btnView.Attributes("ReAss_ID") = e.Item.DataItem("ReAss_ID").ToString
        btnView.CommandArgument = e.Item.DataItem("Ass_ID")

    End Sub

#Region "Dialog History"

    Public Property AssID As Integer
        Get
            Try
                Return lblAssNo.Attributes("AssID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Integer)
            lblAssNo.Attributes("AssID") = value
        End Set
    End Property

    Private Sub ClearDialogHistory()

        lblDept.Text = ""
        lblSupplier.Text = ""
        lblItem.Text = ""
        lblLastAss.Text = ""
        lblLastResult.Text = ""
        lblTotolHistory.Text = ""
        rptHistory.DataSource = Nothing
        rptHistory.DataBind()
        pnlCreate.Visible = False
    End Sub

    Private Sub BindAssHistory()

        Dim HT As DataTable = BL.GetAssessmentHeader(AssID)
        If HT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','ไม่พบแบบประเมินดังกล่าว','');", True)
            Exit Sub
        End If

        ClearDialogHistory()

        lblAssNo.Text = HT.Rows(0).Item("Ref_Code").ToString
        lblDept.Text = HT.Rows(0).Item("Dept_Name").ToString
        lblSupplier.Text = HT.Rows(0).Item("S_Name").ToString '& " (" & HT.Rows(0).Item("S_Tax_No").ToString & ")"
        lblItem.Text = HT.Rows(0).Item("Cat_Name").ToString & " > " & HT.Rows(0).Item("Sub_Cat_Name").ToString & " > " & HT.Rows(0).Item("Type_Name") & " > " & HT.Rows(0).Item("Running_Name") & " (" & HT.Rows(0).Item("Cat_No").ToString & HT.Rows(0).Item("Sub_Cat_No") & HT.Rows(0).Item("Type_No") & HT.Rows(0).Item("Running_No") & ") > <b>" & HT.Rows(0).Item("Item_Name") & "</b>"

        '-------------------- Build History Table ----------------
        Dim DT As New DataTable
        DT.Columns.Add("Ass_Type")
        DT.Columns.Add("Ass_ID", GetType(Integer))
        DT.Columns.Add("ReAss_ID", GetType(Integer))
        DT.Columns.Add("Ref_Code")
        DT.Columns.Add("Role_1_Title")
        DT.Columns.Add("Role_1_Fisrt_Name")
        DT.Columns.Add("Role_1_Last_Name")
        DT.Columns.Add("Role_2_Title")
        DT.Columns.Add("Role_2_Fisrt_Name")
        DT.Columns.Add("Role_2_Last_Name")
        DT.Columns.Add("Role_3_Title")
        DT.Columns.Add("Role_3_Fisrt_Name")
        DT.Columns.Add("Role_3_Last_Name")
        DT.Columns.Add("Role_4_Title")
        DT.Columns.Add("Role_4_Fisrt_Name")
        DT.Columns.Add("Role_4_Last_Name")
        DT.Columns.Add("Get_Score", GetType(Integer))
        DT.Columns.Add("Pass_Score", GetType(Integer))
        DT.Columns.Add("Max_Score", GetType(Integer))
        DT.Columns.Add("Pass_Status", GetType(Boolean))
        DT.Columns.Add("AVL_Y")
        DT.Columns.Add("AVL_M")
        DT.Columns.Add("AVL_Dept_ID")
        DT.Columns.Add("AVL_Sub_Dept_ID")
        DT.Columns.Add("AVL_No")
        DT.Columns.Add("AVL_Status")
        DT.Columns.Add("Current_Step", GetType(Integer))
        DT.Columns.Add("Step_TH")
        DT.Columns.Add("Flag_Return", GetType(Boolean))
        DT.Columns.Add("Update_Time", GetType(DateTime))

        '----------- Add First Record From HT-----------
        Dim HR As DataRow = DT.NewRow
        HR("Ass_Type") = "ประเมินผู้ขายใหม่"
        HR("Ass_ID") = HT.Rows(0).Item("Ass_ID")
        HR("ReAss_ID") = DBNull.Value
        HR("Ref_Code") = HT.Rows(0).Item("Ref_Code").ToString
        HR("Role_1_Title") = HT.Rows(0).Item("Role_1_Title").ToString
        HR("Role_1_Fisrt_Name") = HT.Rows(0).Item("Role_1_Fisrt_Name").ToString
        HR("Role_1_Last_Name") = HT.Rows(0).Item("Role_1_Last_Name").ToString
        HR("Role_2_Title") = HT.Rows(0).Item("Role_2_Title").ToString
        HR("Role_2_Fisrt_Name") = HT.Rows(0).Item("Role_2_Fisrt_Name").ToString
        HR("Role_2_Last_Name") = HT.Rows(0).Item("Role_2_Last_Name").ToString
        HR("Role_3_Title") = HT.Rows(0).Item("Role_3_Title").ToString
        HR("Role_3_Fisrt_Name") = HT.Rows(0).Item("Role_3_Fisrt_Name").ToString
        HR("Role_3_Last_Name") = HT.Rows(0).Item("Role_3_Last_Name").ToString
        HR("Role_4_Title") = HT.Rows(0).Item("Role_4_Title").ToString
        HR("Role_4_Fisrt_Name") = HT.Rows(0).Item("Role_4_Fisrt_Name").ToString
        HR("Role_4_Last_Name") = HT.Rows(0).Item("Role_4_Last_Name").ToString
        HR("Get_Score") = HT.Rows(0).Item("Get_Score")
        HR("Pass_Score") = HT.Rows(0).Item("Pass_Score")
        HR("Max_Score") = HT.Rows(0).Item("Max_Score")
        HR("Pass_Status") = HT.Rows(0).Item("Pass_Status")
        HR("AVL_Y") = HT.Rows(0).Item("AVL_Y").ToString
        HR("AVL_M") = HT.Rows(0).Item("AVL_M").ToString
        HR("AVL_Dept_ID") = HT.Rows(0).Item("AVL_Dept_ID").ToString
        HR("AVL_Sub_Dept_ID") = HT.Rows(0).Item("AVL_Sub_Dept_ID").ToString
        HR("AVL_No") = HT.Rows(0).Item("AVL_No").ToString
        HR("AVL_Status") = HT.Rows(0).Item("AVL_Status")
        HR("Current_Step") = HT.Rows(0).Item("Current_Step")
        HR("Step_TH") = HT.Rows(0).Item("Step_TH").ToString
        'HR("Flag_Return") = HT.Rows(0).Item("Flag_Return").ToString
        If HT.Rows(0).Item("Flag_Return").ToString <> "" Then HR("Flag_Return") = HT.Rows(0).Item("Flag_Return") Else HR("Flag_Return") = False
        HR("Update_Time") = HT.Rows(0).Item("Update_Time")

        DT.Rows.Add(HR)

        '------------- Get All ReAssessment -----------------
        Dim RT As DataTable = BL.GetReassessmentHeader(AssID)
        RT.DefaultView.Sort = "Ref_Code"
        RT = RT.DefaultView.ToTable.Copy
        For i As Integer = 0 To RT.Rows.Count - 1
            Dim DR As DataRow = DT.NewRow
            DR("Ass_Type") = "ประเมินทบทวน"
            DR("Ass_ID") = RT.Rows(i).Item("Ass_ID")
            DR("ReAss_ID") = RT.Rows(i).Item("ReAss_ID")
            DR("Ref_Code") = RT.Rows(i).Item("Ref_Code").ToString
            DR("Role_1_Title") = RT.Rows(i).Item("Role_1_Title").ToString
            DR("Role_1_Fisrt_Name") = RT.Rows(i).Item("Role_1_Fisrt_Name").ToString
            DR("Role_1_Last_Name") = RT.Rows(i).Item("Role_1_Last_Name").ToString
            DR("Role_2_Title") = RT.Rows(i).Item("Role_2_Title").ToString
            DR("Role_2_Fisrt_Name") = RT.Rows(i).Item("Role_2_Fisrt_Name").ToString
            DR("Role_2_Last_Name") = RT.Rows(i).Item("Role_2_Last_Name").ToString
            DR("Role_3_Title") = RT.Rows(i).Item("Role_3_Title").ToString
            DR("Role_3_Fisrt_Name") = RT.Rows(i).Item("Role_3_Fisrt_Name").ToString
            DR("Role_3_Last_Name") = RT.Rows(i).Item("Role_3_Last_Name").ToString
            DR("Role_4_Title") = RT.Rows(i).Item("Role_4_Title").ToString
            DR("Role_4_Fisrt_Name") = RT.Rows(i).Item("Role_4_Fisrt_Name").ToString
            DR("Role_4_Last_Name") = RT.Rows(i).Item("Role_4_Last_Name").ToString
            DR("Get_Score") = RT.Rows(i).Item("Get_Score")
            DR("Pass_Score") = RT.Rows(i).Item("Pass_Score")
            DR("Max_Score") = RT.Rows(i).Item("Max_Score")
            DR("Pass_Status") = RT.Rows(i).Item("Pass_Status")
            DR("AVL_Y") = RT.Rows(i).Item("AVL_Y").ToString
            DR("AVL_M") = RT.Rows(i).Item("AVL_M").ToString
            DR("AVL_Dept_ID") = RT.Rows(i).Item("AVL_Dept_ID").ToString
            DR("AVL_Sub_Dept_ID") = RT.Rows(i).Item("AVL_Sub_Dept_ID").ToString
            DR("AVL_No") = RT.Rows(i).Item("AVL_No").ToString
            DR("AVL_Status") = RT.Rows(i).Item("AVL_Status")
            DR("Current_Step") = RT.Rows(i).Item("Current_Step")
            DR("Step_TH") = RT.Rows(i).Item("Step_TH").ToString
            'DR("Flag_Return") = RT.Rows(i).Item("Flag_Return").ToString
            If RT.Rows(i).Item("Flag_Return").ToString <> "" Then DR("Flag_Return") = RT.Rows(i).Item("Flag_Return").ToString Else DR("Flag_Return") = False
            DR("Update_Time") = RT.Rows(i).Item("Update_Time")
            DT.Rows.Add(DR)
        Next

        rptHistory.DataSource = DT
        rptHistory.DataBind()

        Dim Update_Time As DateTime = DT.Compute("MAX(UPDATE_TIME)", "")
        lblLastAss.Text = GL.ReportThaiPassTime(Update_Time)
        BL.DisplayAssessmentGridResultLabel(DT.Rows(DT.Rows.Count - 1).Item("Get_Score"), DT.Rows(DT.Rows.Count - 1).Item("Pass_Score"), DT.Rows(DT.Rows.Count - 1).Item("Max_Score"), DT.Rows(DT.Rows.Count - 1).Item("Pass_Status"), lblLastResult)
        lblTotolHistory.Text = FormatNumber(DT.Rows.Count, 0) & " ครั้ง"

        Dim Creatable As Boolean = (DT.Compute("COUNT(Ass_Type)", "Current_Step<>5") = 0)
        '--------- สิทธิ์ในเมนูของผู้ใช้คนปัจจุบัน -----------
        Dim MT As DataTable = BL.GetUserMenuRole(User_ID, PageName)
        Dim CreateRole As Boolean = False
        If MT.Rows.Count > 0 AndAlso MT.Rows(0).Item("MR_ID") = AVLBL.AccessRole.Edit Then
            CreateRole = True
        End If

        Dim ART As DataTable = BL.GetUserDeptRole(User_ID)
        pnlCreate.Visible = Creatable And CreateRole And BL.CanCreateAssessment(ART, "AR_ID")

        dialogAssessmentHistory.Visible = True

    End Sub

    Protected Sub rptHistory_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptHistory.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblAssType As Label = e.Item.FindControl("lblAssType")
        Dim lblRefCode As Label = e.Item.FindControl("lblRefCode")
        Dim lblAVL As Label = e.Item.FindControl("lblAVL")
        Dim lblUpdate As Label = e.Item.FindControl("lblUpdate")
        Dim lblResult As Label = e.Item.FindControl("lblResult")
        Dim lblResultText As Label = e.Item.FindControl("lblResultText")
        Dim lblStep As Label = e.Item.FindControl("lblStep")
        Dim lblAssessor As Label = e.Item.FindControl("lblAssessor")
        Dim btnView As LinkButton = e.Item.FindControl("btnView")
        Dim pnl_icon As Panel = e.Item.FindControl("pnl_icon")
        lblAssType.Text = e.Item.DataItem("Ass_Type").ToString
        Select Case e.Item.DataItem("Ass_Type").ToString
            Case "ประเมินผู้ขายใหม่"
                lblAssType.ForeColor = Drawing.Color.Teal
            Case Else
                lblAssType.ForeColor = Drawing.Color.Blue
        End Select

        ' แสดง lbl สถานะของการประเมิน  เสร็จแล้ว , ยังไม่เสร็จ
        If (e.Item.DataItem("Current_Step").ToString() = 5) Then
            lblResultText.Text = "(เสร็จแล้ว)"
            lblResultText.Style("color") = "green"
        Else
            lblResultText.Text = "(ยังไม่เสร็จ)"
            lblResultText.Style("color") = "red"
        End If

        lblRefCode.Text = e.Item.DataItem("Ref_Code")

        If IsDBNull(e.Item.DataItem("AVL_Y")) Or e.Item.DataItem("AVL_Y").ToString = "" Then
            lblAVL.Text = ""
        Else
            lblAVL.Text = "AVL" & e.Item.DataItem("AVL_Y").ToString & e.Item.DataItem("AVL_M").ToString & e.Item.DataItem("AVL_Dept_ID").ToString & e.Item.DataItem("AVL_Sub_Dept_ID").ToString & e.Item.DataItem("AVL_No").ToString
        End If
        BL.DisplayAssessmentGridResultLabel(e.Item.DataItem("Get_Score"), e.Item.DataItem("Pass_Score"), e.Item.DataItem("Max_Score"), e.Item.DataItem("Pass_Status"), lblResult)
        Dim Creater As String = Trim(e.Item.DataItem("Role_1_Fisrt_Name") & " " & e.Item.DataItem("Role_1_Last_Name"))
        Dim Auditor As String = Trim(e.Item.DataItem("Role_2_Fisrt_Name") & " " & e.Item.DataItem("Role_2_Last_Name"))
        Dim Supply_Officer As String = Trim(e.Item.DataItem("Role_3_Fisrt_Name") & " " & e.Item.DataItem("Role_3_Last_Name"))
        Dim Director As String = Trim(e.Item.DataItem("Role_4_Fisrt_Name") & " " & e.Item.DataItem("Role_4_Last_Name"))
        Select Case CType(e.Item.DataItem("Current_Step"), AVLBL.AssessmentStep)
            Case AVLBL.AssessmentStep.Creater
                lblAssessor.Text = Creater
            Case AVLBL.AssessmentStep.Auditor
                If Auditor <> "" Then
                    lblAssessor.Text = Auditor
                Else
                    lblAssessor.Text = Creater
                End If
            Case AVLBL.AssessmentStep.Supply_Officer
                If Supply_Officer <> "" Then
                    lblAssessor.Text = Supply_Officer
                ElseIf Auditor <> "" Then
                    lblAssessor.Text = Auditor
                Else
                    lblAssessor.Text = Creater
                End If
            Case AVLBL.AssessmentStep.Director, AVLBL.AssessmentStep.Completed
                If Director <> "" Then
                    lblAssessor.Text = Director
                ElseIf Supply_Officer <> "" Then
                    lblAssessor.Text = Supply_Officer
                ElseIf Auditor <> "" Then
                    lblAssessor.Text = Auditor
                Else
                    lblAssessor.Text = Creater
                End If
            Case Else
        End Select
        lblUpdate.Text = GL.ReportThaiPassTime(e.Item.DataItem("Update_Time"))
        lblStep.Text = e.Item.DataItem("Step_TH").ToString
        lblStep.ForeColor = BL.GetStepColor(e.Item.DataItem("Current_Step"))

        Select Case e.Item.DataItem("Ass_Type")
            Case "ประเมินผู้ขายใหม่"
                btnView.PostBackUrl = "Assessment_Edit.aspx?Ass_ID=" & e.Item.DataItem("Ass_ID") & "&From=ReAss"
            Case "ประเมินทบทวน"
                btnView.PostBackUrl = "Reassessment_Edit.aspx?Ass_ID=" & e.Item.DataItem("Ass_ID") & "&ReAss_ID=" & e.Item.DataItem("ReAss_ID") & "&From=ReAss"
        End Select

        If Not IsDBNull(e.Item.DataItem("Flag_Return")) Then
            pnl_icon.Visible = e.Item.DataItem("Flag_Return")
        End If

    End Sub

    Protected Sub btnCreateOK_Click(sender As Object, e As System.EventArgs) Handles btnCreateOK.Click
        Response.Redirect("Reassessment_Edit.aspx?Ass_ID=" & AssID & "&From=ReAss")
    End Sub

    Protected Sub lnkCloseDialogHistory_Click(sender As Object, e As System.EventArgs) Handles lnkCloseDialogHistory.Click
        dialogAssessmentHistory.Visible = False
    End Sub

#End Region

#Region "PrintButton"

    Protected Sub btnPDF_Click(sender As Object, e As System.EventArgs) Handles btnPDF.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/ReAssessment_List.aspx?Mode=PDF');", True)
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/ReAssessment_List.aspx?Mode=EXCEL');", True)
    End Sub

#End Region


  

    Protected Sub ddl_Search_Dept_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddl_Search_Dept.SelectedIndexChanged
        'BL.BindDDlSub_Dept(ddl_Sub_Dept, Dept_ID, User_ID, AVLBL.AssessmentRole.Creater, "", True)
        'ddl_Sub_Dept_SelectedIndexChanged(sender, e)
        'pnlSubDept.Visible = Dept_ID.ToString() <> "0"
        'ddl_Sub_Dept.SelectedIndex = 0

        BL.BindDDlSubDept_Search(ddl_Search_SUB_DEPT, ddl_Search_Dept.SelectedValue)
        ddl_Search_SUB_DEPT.SelectedIndex = 0
    End Sub
End Class
