﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Assessment_Upload.aspx.vb" Inherits="Assessment_Upload" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>

    <style type="text/css">
        textarea 
        {
            resize: none; margin-bottom:15px; 
            }
        form, body
        {
            margin:0px 0px 0px 0px;
            }
    </style>

<script type="text/javascript" src="assets/plugins/jquery-1.10.1.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
                        <asp:Panel CssClass="row-fluid" id="pnlEditFile" runat="server">
                                <div class="span6">
                                    <h5><i class="icon-ok-sign"></i> ไฟล์ปัจจุบัน</h5>
                                    <asp:Image ID="fileOld" runat="server" CssClass="span12 iconFileOld" ImageUrl="~/images/G3.jpg" ToolTip="ไฟล์เดิม" />
                                </div>
                                <div class="span6">
                                    <h5 class="span6"><i class="icon-folder-open"></i> ไฟล์ใหม่</h5>
                                    <asp:Image ID="fileNew" runat="server" CssClass="span12 iconFileNew" style="cursor:pointer;" ImageUrl="~/images/G5.jpg" ToolTip="ไฟล์ที่คุณอัพโหดเข้าไปใหม่(Click เพื่ออัพโหลด)" />
                                </div>                              
                            </asp:Panel>

                            <asp:Panel CssClass="row-fluid" id="pnlAddFile" runat="server">
                                <div class="span11">
                                    <asp:Image ID="fileAdd" runat="server" CssClass="span12" style="cursor:pointer;" ImageUrl="~/images/G3.jpg" ToolTip="ไฟล์ที่คุณอัพโหดเข้าไปใหม่ (Click เพื่ออัพโหลด)" />
                                </div>
                            </asp:Panel>

                             <div class="form-actions" style="margin-top:0px; margin-bottom:0px; padding:10px 10px 10px 10px; text-align:right;">
                                 <asp:TextBox ID="txtFileName" runat="server" CssClass="m-wrap span10" BackColor="white" placeholder="กรอกรายละเอียด" ToolTip="กรอกรายละเอียด"></asp:TextBox>
								 <asp:Button ID="btnSaveFile" runat="server" CssClass="btn blue span2" Text="บันทึก" />
                                 <asp:FileUpload ID="ful" runat="server" style="display:none;" />
							 </div>
    </form>
</body>
</html>
