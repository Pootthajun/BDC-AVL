﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="Assessment_List.aspx.vb" Inherits="Assessment_List" %>

<%@ Register Src="WUC_DateReporter.ascx" TagName="WUC_DateReporter" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="PageNavigation.ascx" TagName="PageNavigation" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="assets/css/pages/timeline.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" rel="stylesheet"
        type="text/css" />
    <link href="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="assets/plugins/chosen-bootstrap/chosen/chosen.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .gridResult tr td
        {
            background-color: white;
            cursor: pointer;
        }
        
        .gridResult tr:hover td
        {
            background-color: #ccccff;
        }
    </style>
    <link href="style/style.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ToolkitScriptManager ID="toolkit1" runat="server">
    </asp:ToolkitScriptManager>
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    การประเมินผู้ขายใหม่
                </h3>
                <ul class="breadcrumb">
                    <li><i class="icon-ok-sign"></i><a href="javascript:;">การประเมิน</a><i class="icon-angle-right"></i>
                    </li>
                    <li><i class="icon-ok-sign"></i><a href="javascript:;">การประเมินผู้ขายใหม่</a></li>
                    <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet">
                    <div class="portlet-body">
                        <asp:UpdatePanel ID="udpSearch" runat="server">
                            <ContentTemplate>
                                <div class="table-toolbar">
                                    <div class="btn-group">
                                        <asp:LinkButton CssClass="btn green" ID="lnkAdd1" runat="server" ToolTip="ต้องการประเมินผู้ขายรายใหม่">ต้องการประเมินผู้ขายรายใหม่ Click  <i class="icon-plus"></i></asp:LinkButton>
                                    </div>

                                    <div class="btn-group pull-right">  
                                        <asp:LinkButton  data-toggle="dropdown" class="btn dropdown-toggle" id="Button2" runat="server">
										    พิมพ์ <i class="icon-angle-down"></i>
										    </asp:LinkButton>
                                            <ul class="dropdown-menu pull-right">
											    <li><asp:LinkButton ID="btnPDF" runat="server">รูปแบบ PDF</asp:LinkButton></li>
											    <li><asp:LinkButton ID="btnExcel" runat="server">รูปแบบ Excel</asp:LinkButton></li>											
										    </ul>    
									    </div>
                                         <div class="btn-group  ">
                                            <asp:LinkButton CssClass="btn blue" id="btnSearch" runat="server">
										        ค้นหา <i class="icon-search"></i>
										        </asp:LinkButton>    
									    </div>
                                </div>
                                <%--แบ่งส่วนการค้นหา--%>
                                <fieldset>
                                    <legend><b>ค้นหา</b></legend>
                                </fieldset>
                                <div class="row-fluid form-horizontal">
                                    <div class="span4 ">
                                        <div class="control-group">
                                            <label class="control-label">
                                                สำนักงาน</label>
                                            <div class="controls">
                                                <asp:DropDownList ID="ddl_Search_Dept" runat="server"  AutoPostBack ="true" class="medium m-wrap"  >
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="span8">
                                        <div class="control-group">
                                            <label class="control-label">
                                                ฝ่าย/ภาค/งาน</label>
                                            <div class="controls">
                                                <asp:DropDownList ID="ddl_Search_SUB_DEPT" runat="server" class="m-wrap large" >
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-fluid form-horizontal">
                                    <div class="span4 ">
                                        <div class="control-group">
                                            <label class="control-label">
                                                พัสดุ/ครุภัณฑ์<br />
                                                งานจ้าง/บริการ</label>
                                            <div class="controls">
                                                <asp:TextBox ID="txt_Search_Item" runat="server" class="m-wrap medium"
                                                    placeholder="ชื่อ พัสดุ/ครุภัณฑ์ / งานจ้างและบริการ"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="span8">
                                        <div class="control-group">
                                            <label class="control-label">
                                                เลข AVL</label>
                                            <div class="controls">
                                                <asp:TextBox ID="txt_Search_AVL" runat="server" class="m-wrap large"  placeholder="เลข AVL"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="row-fluid form-horizontal">
                                    <div class="span4 ">
                                        <div class="control-group">
                                            <label class="control-label">
                                                ผู้ขาย</label>
                                            <div class="controls">
                                                <asp:TextBox ID="txt_Search_Sup" runat="server" class="m-wrap medium" placeholder="ชื่อผู้ขาย/ผู้ติดต่อ/เลขประจำตัวผู้เสียภาษี"
                                                   ></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="span8 ">
                                        <div class="control-group">
                                            <label class="control-label">
                                                เลขใบประเมิน</label>
                                            <div class="controls">
                                                <asp:TextBox ID="txt_Search_Ref" runat="server" class="m-wrap large" placeholder="เลขใบประเมิน"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-fluid form-horizontal">
                                    <div class="span4 ">
                                        <div class="control-group">
                                            <label class="control-label">
                                                ขั้นตอน</label>
                                            <div class="controls">
                                                    <asp:DropDownList ID="ddl_Search_Step" runat="server" class="medium m-wrap" >
                                                    <asp:ListItem Value="0" Text="ทั้งหมด"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="เจ้าหน้าที่ประเมิน"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="หัวหน้าฝ่ายตรวจสอบ"></asp:ListItem>
                                                    <asp:ListItem Value="3" Text="เจ้าหน้าที่พัสดุตรวจสอบ"></asp:ListItem>
                                                    <asp:ListItem Value="4" Text="ฝ่ายบริหารตรวจสอบ"></asp:ListItem>
                                                    <asp:ListItem Value="5" Text="ประเมินเสร็จสมบูรณ์"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="span4 ">
                                        <div class="control-group">
                                            <label class="control-label">
                                                ซ่อนหมายเหตุ</label>
                                            <div class="controls">
                                                <asp:CheckBox ID="ckHideRemark" runat="server" CssClass="large m-wrap" 
                                                    ToolTip="แสดงความคิดเห็นทั้งหมดในตาราง" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-fluid form-horizontal">
									<div class="span12 ">
										<div class="control-group">
											<label class="control-label"> ช่วงที่ ประเมิน</label>
											<div class="controls">
												<asp:DropDownList ID="ddlStart_M" runat="server" AutoPostBack="true" CssClass="m-wrap" Width="70px">				                                    
			                                    </asp:DropDownList>
                                                <asp:DropDownList ID="ddlStart_Y" runat="server" AutoPostBack="true" CssClass="m-wrap" Width="100px">				                                    
			                                    </asp:DropDownList>
                                                <span style="font-size: 14px; padding-left:30px; padding-right:30px;">ถึง</span>
                                                <asp:DropDownList ID="ddlEnd_M" runat="server" AutoPostBack="true" CssClass="m-wrap"  Width="70px">				                                    
			                                    </asp:DropDownList>
                                                <asp:DropDownList ID="ddlEnd_Y" runat="server" AutoPostBack="true" CssClass="m-wrap" Width="100px">				                                    
			                                    </asp:DropDownList>

                                                
											</div>
                                            
										</div>
									</div>									
								</div>

 
                                <div role="grid" class="dataTables_wrapper form-inline">
                                    <div class="portlet-body no-more-tables">
                                        <span style="font-weight: bold; font-size: 14px;">การประเมินผู้ขายใหม่</span>
                                        <asp:Label ID="lblTotalRecord" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
                                        <table class="table table-full-width table-advance dataTable no-more-tables table-hover">
                                            <thead>
                                                <tr>
                                                    <th style="text-align: center; vertical-align: middle;">
                                                        เลขใบประเมิน
                                                    </th>
                                                    <th style="text-align: center; vertical-align: middle;">
                                                        เลข AVL
                                                    </th>
                                                    <th style="text-align: center; vertical-align: middle;">
                                                        ผู้ขาย
                                                    </th>
                                                    <th style="text-align: center; vertical-align: middle; display: none;">
                                                        เลขพัสดุ/ครุภัณฑ์
                                                    </th>
                                                    <th style="text-align: center; vertical-align: middle;">
                                                        ชื่อพัสดุ/ครุภัณฑ์<br />
                                                        งานจ้าง/บริการ
                                                    </th>
                                                    <th style="text-align: center; vertical-align: middle; width: 80px;">
                                                        ขั้นตอน
                                                    </th>
                                                    <th style="text-align: center; vertical-align: middle;">
                                                        ผู้ประเมิน
                                                    </th>
                                                    <th style="text-align: center; vertical-align: middle;">
                                                        ฝ่าย/ภาค/งาน
                                                    </th>
                                                    <th style="text-align: center; vertical-align: middle; width: 100px;">
                                                        ประเมินล่าสุดเมื่อ
                                                    </th>
                                                    <th style="text-align: center; vertical-align: middle; width: 80px;">
                                                        ผลการประเมิน
                                                    </th>
                                                    <th style="text-align: center; vertical-align: middle; width: 100px;">
                                                        ดำเนินการ
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="rptList" runat="server">
                                                    <ItemTemplate>
                                                        <tr id="trDept" runat="server">
                                                            <td data-title="ฝ่าย/ภาค/งาน" colspan="10" style="text-align: center">
                                                                <asp:Label ID="lblDept" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td data-title="เลขใบประเมิน" style="text-align: center;">
                                                                <asp:Label ID="lblRef" runat="server" Font-Bold="True"></asp:Label>
                                                            </td>
                                                            <td data-title="เลข AVL" style="text-align: center;">
                                                                <asp:Label ID="lblAVL" runat="server" Font-Bold="True"></asp:Label>
                                                            </td>
                                                            <td data-title="ผู้ขาย">
                                                                <asp:Label ID="lblSup" runat="server"></asp:Label>
                                                            </td>
                                                            <td data-title="เลขพัสดุ/ครุภัณฑ์" style="text-align: center; display: none;">
                                                                <asp:Label ID="lblItemCode" runat="server"></asp:Label>
                                                            </td>
                                                            <td data-title="ชื่อพัสดุ/ครุภัณฑ์<br>งานจ้างและบริการ">
                                                                <asp:Label ID="lblItemName" runat="server"></asp:Label>
                                                            </td>
                                                            <td data-title="ขั้นตอน" style="text-align: center;">
                                                                <asp:Panel CssClass="inline" ID="pnl_icon" runat="server" Visible="false">
                                                                    <i id="icon_undo" runat="server" class="icon-undo" style="color: Silver;"></i>
                                                                </asp:Panel>
                                                                <asp:Label ID="lblStep" runat="server"></asp:Label>
                                                            </td>
                                                            <td data-title="ผู้ประเมิน">
                                                                <asp:Label ID="lblAssessor" runat="server"></asp:Label>
                                                            </td>
                                                            <td data-title="ฝ่าย">
                                                                <asp:Label ID="lblSub_Dept_Name" runat="server"></asp:Label>
                                                            </td>
                                                            <td data-title="Updated" style="text-align: center ;">
                                                                <asp:Label ID="lblUpdate" runat="server"></asp:Label>
                                                            </td>
                                                            <td data-title="ผลการประเมิน" style="text-align: center;">
                                                                <asp:Label ID="lblResult" runat="server"></asp:Label><br /><asp:Label ID="lblResultText" runat="server"></asp:Label>
                                                            </td>
                                                            <td data-title="ดำเนินการ" style="text-align: center;">
                                                                <asp:LinkButton CssClass="btn mini purple" ID="btnView" runat="server" CommandName="View"><i class="icon-search"></i> การประเมิน</asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                        <tr id="trRemark" runat="server" >
                                                            <td data-title="เลขใบประเมิน" style="text-align: center; border-top: white;">
                                                            </td>
                                                            <td data-title="เลข AVL" style="text-align: center; border-top: white;">
                                                                <span style="color: Silver;">หมายเหตุ <i class="icon-comments"></i></span>
                                                            </td>
                                                            <td data-title="หมายเหตุ" colspan="8">
                                                                <span>
                                                                    <div class="span3">
                                                                        <asp:Label ID="lblRole_1_Comment" Style="color: Silver;" runat="server"></asp:Label></div>
                                                                    <div class="span3">
                                                                        <asp:Label ID="lblRole_2_Comment" Style="color: Silver;" runat="server"></asp:Label></div>
                                                                    <div class="span3">
                                                                        <asp:Label ID="lblRole_3_Comment" Style="color: Silver;" runat="server"></asp:Label></div>
                                                                    <div class="span3">
                                                                        <asp:Label ID="lblRole_4_Comment" Style="color: Silver;" runat="server"></asp:Label></div>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>
                                        </table>
                                        <uc2:PageNavigation ID="Pager" runat="server" />
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <div class="form-actions" style="margin-top: 0px; margin-bottom: 0px;" id="pnlBottomForm"
            runat="server">
            <%--<asp:UpdatePanel ID="udpAddBottom" runat="server">
                <ContentTemplate>--%>
                    <asp:LinkButton CssClass="btn green" ID="lnkAdd2" runat="server" ToolTip="ต้องการประเมินผู้ขายรายใหม่">ต้องการประเมินผู้ขายรายใหม่ Click  <i class="icon-plus"></i></asp:LinkButton>
                <%--</ContentTemplate>
            </asp:UpdatePanel>--%>
        </div>
        <asp:UpdatePanel ID="udpDialogNew" runat="server">
            <ContentTemplate>
                <asp:Panel CssClass="modal-scrollable" ID="dialogNewAssessment" runat="server">
                    <div class="modal fade in modal-overflow" style="display: block; margin-top: 30px;">
                        <div class="modal-header">
                            <h3>
                                ประเมินผู้ขายรายใหม่</h3>
                        </div>
                        <div class="modal-body">
                            <div id="ctl00_ContentPlaceHolder1_udpDialog">
                                <div class="row-fluid form-horizontal">
                                    <div class="control-group">
                                        <label class="control-label">
                                            สำนักงาน</label>
                                        <div class="controls">
                                            <asp:DropDownList ID="ddl_Dept" runat="server" Font-Bold="True" CssClass="span12"
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-fluid form-horizontal" id="pnlSubDept" runat="server">
                                    <div class="control-group">
                                        <label class="control-label">
                                            ฝ่าย/ภาค/งาน</label>
                                        <div class="controls">
                                            <%--เอาเฉพาะ ฝ่าย/ภาค/งาน ในสำนักงานที่เจ้าตัวมีสิทธิ์เท่านั้น--%>
                                            <asp:DropDownList ID="ddl_Sub_Dept" runat="server" Font-Bold="True" CssClass="span12"
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-fluid form-horizontal" id="pnlbtnProductDialog" runat="server" style="margin-bottom: 15px;">
                                    <div class="btn-group pull-right">
                                        <asp:LinkButton CssClass="btn blue" ID="btnProductDialog" runat="server"><i class="icon-barcode"></i> เลือกสินค้า / พัสดุ</asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row-fluid form-horizontal" id="pnlHeaderDialog" runat="server">
                                    <div class="control-group">
                                        <label class="control-label">
                                            <asp:Label ID="lblHeader_Item" runat="server"></asp:Label></label>
                                        <div class="controls">
                                            <asp:Label ID="lbl_Item_Type" runat="server" Font-Bold="True" CssClass="span12"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <%--                                        <div class="row-fluid form-horizontal" id="pnlCatGroup" runat="server">							
								                <div class="control-group">
									                <label class="control-label"> สินค้า/บริการ</label>
									                <div class="controls">
                                                      <asp:DropDownList ID="ddl_CatGroup" runat="server" Font-Bold="True" CssClass="span12" AutoPostBack="True"></asp:DropDownList>       
									                </div>
								                </div>                                           
                                        </div>
                                        
                                        <div class="row-fluid form-horizontal" id="pnlCat" runat="server">							
								                <div class="control-group">
									                <label class="control-label"> ประเภท</label>
									                <div class="controls">
                                                      <asp:DropDownList ID="ddl_Cat" runat="server" Font-Bold="True" CssClass="span12" AutoPostBack="True"></asp:DropDownList>       
									                </div>
								                </div>                                           
                                        </div>
                                        <div class="row-fluid form-horizontal" id="pnlSubCat" runat="server">							
								                <div class="control-group">
									                <label class="control-label"> ชนิด</label>
									                <div class="controls">
                                                      <asp:DropDownList ID="ddl_Sub_Cat" runat="server" Font-Bold="True" CssClass="span12" AutoPostBack="True"></asp:DropDownList>	       
									                </div>
								                </div>                                           
                                        </div>
                                        <div class="row-fluid form-horizontal" id="pnlType" runat="server">							
								                <div class="control-group">
									                <label class="control-label"> ชนิดย่อย</label>
									                <div class="controls">
                                                        <asp:DropDownList ID="ddl_Type" runat="server" Font-Bold="True" CssClass="span12" AutoPostBack="True"></asp:DropDownList>	       
									                </div>
								                </div>                                           
                                        </div>
                                --%>
                            </div>
                        </div>
                        <div class="modal-footer" id="pnlCreate" runat="server">
                            <asp:Button CssClass="btn red" ID="btnCreateOK" runat="server" Text="สร้างใบประเมิน">
                            </asp:Button>
                        </div>
                        <asp:LinkButton CssClass="fancybox-item fancybox-close" ToolTip="ปิด" ID="lnkCloseDialogNew"
                            runat="server"></asp:LinkButton>
                    </div>
                    <div class="modal-backdrop" style="z-index: 10049;">
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="udpSelectProduct" runat="server">
            <ContentTemplate>
                <asp:Panel CssClass="modal-scrollable" ID="dialogProductType" runat="server" Visible="false"
                    Style="margin-left: -450px;" DefaultButton="btnSearchProduct_dialog">
                    <div class="modal fade in modal-overflow" style="display: block; margin-top: 30px;
                        width: 1100px;">
                        <div class="modal-header">
                            <h3>
                                <i class="icon-barcode"></i>เลือกประเภทสินค้า / พัสดุครุภัณฑ์</h3>
                            <asp:Label ID="lblType_ID_dialog" runat="server" Style="display: none;"></asp:Label>
                            <asp:Label ID="lblRunning_ID_dialog" runat="server" Style="display: none;"></asp:Label>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="table-toolbar">
                                    <div class="btn-group" style="padding-left: 50px;">
                                        <asp:Button ID="btnSearchProduct_dialog" CssClass="btn green" runat="server" Text="ค้นหา" />
                                    </div>
                                </div>
                            </div>
                           
                            <div class="row-fluid form-horizontal">
                                <div class="control-group span6">
                                    <label class="control-label">
                                        กลุ่มสินค้า/บริการ</label>
                                    <div class="controls">
                                        <asp:DropDownList ID="ddl_CatGroup_dialog" runat="server" Font-Bold="True" CssClass="m-wrap"
                                            AutoPostBack="True">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="control-group span6">
                                    <label class="control-label">
                                        ประเภท</label>
                                    <div class="controls">
                                        <asp:DropDownList ID="ddl_Cat_dialog" runat="server" Font-Bold="True" CssClass="m-wrap"
                                            AutoPostBack="True">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row-fluid form-horizontal">
                                <div class="control-group span6">
                                    <label class="control-label">
                                        ชนิด</label>
                                    <div class="controls">
                                        <asp:DropDownList ID="ddl_Sub_Cat_dialog" runat="server" Font-Bold="True" CssClass="m-wrap"
                                            AutoPostBack="True">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="control-group span6">
                                    <label class="control-label">
                                        ชนิดย่อย</label>
                                    <div class="controls">
                                        <asp:DropDownList ID="ddl_Type_dialog" runat="server" Font-Bold="True" CssClass="m-wrap"
                                            AutoPostBack="True">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row-fluid form-horizontal">
                                <div class="control-group span6">
                                    <label class="control-label">
                                        ค้นจากชื่อ</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtSearchProduct_dialog" runat="server" CssClass="span11 m-wrap"
                                            AutoPostBack="True" Placeholder="ชื่อชนิด ชนิดย่อย ประเภท รหัสครุภัณฑ์ "></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group span6">
                                   
                                </div>
                            </div>
                            <div class="portlet-body no-more-tables">
                                <asp:Label ID="lblTotalProduct" runat="server" Width="100%" Font-Size="16px" Font-Bold="True"
                                    Style="text-align: center;"></asp:Label>
                                <div>
                                </div>
                                <table class="table table-bordered table-advance gridResult">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;">
                                                <i class="icon-list"></i>ประเภท
                                            </th>
                                            <th style="text-align: center;">
                                                <i class="icon-list"></i>ชนิด
                                            </th>
                                            <th style="text-align: center;">
                                                <i class="icon-list"></i>ชนิดย่อย
                                            </th>
                                            <th style="text-align: center;">
                                                <i class="icon-list"></i>Running
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="rptProductTypeList" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td id="td1" runat="server" data-title="ประเภท" style="border-right: 1px solid #eeeeee;">
                                                        <asp:Label ID="lbl_Cat" runat="server"></asp:Label>
                                                    </td>
                                                    <td id="td2" runat="server" data-title="ชนิด" style="border-right: 1px solid #eeeeee;">
                                                        <asp:Label ID="lbl_SubCat" runat="server"></asp:Label>
                                                    </td>
                                                    <td id="td3" runat="server" data-title="ชนิดย่อย" style="border-right: 1px solid #eeeeee;">
                                                        <asp:Label ID="lbl_Type" runat="server"></asp:Label>                                                        
                                                    </td>
                                                    <td id="td4" runat="server" data-title="Running" style="border-right: 1px solid #eeeeee;">
                                                        <asp:Button ID="btnSelect" runat="server" Style="display: none;" CommandName="select" />
                                                        <asp:Label ID="lbl_Running" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                                <uc2:PageNavigation ID="PagerProduct" runat="server" PageSize="8" />
                            </div>
                        </div>
                        <asp:LinkButton CssClass="fancybox-item fancybox-close" ToolTip="ปิด" ID="lnkCloseProduct"
                            runat="server"></asp:LinkButton>
                    </div>
                    <div class="modal-backdrop" style="z-index: 10049;">
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
