﻿
Partial Class Assessment_Upload
    Inherits System.Web.UI.Page

    Dim C As New Converter

    Public Property UploadedFile As AVLBL.FileStructure
        Get
            Try
                Return Session("Assessment_Preview_File_New")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As AVLBL.FileStructure)
            Session("Assessment_Preview_File_New") = value
        End Set
    End Property

    Public Property OldFile As AVLBL.FileStructure
        Get
            Try
                Return Session("Assessment_Preview_File_Old")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As AVLBL.FileStructure)
            Session("Assessment_Preview_File_Old") = value
        End Set
    End Property

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Request.Files.Count = 0 Then Exit Sub
        Dim fileContent As HttpPostedFile = Request.Files(0)
        Dim Result As New AVLBL.FileStructure
        Result.FileName = fileContent.FileName
        Result.FileContent = C.StreamToByte(fileContent.InputStream)
        Result.ContentType = fileContent.ContentType
        UploadedFile = Result
    End Sub
End Class
