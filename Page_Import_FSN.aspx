﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Page_Import_FSN.aspx.vb" Inherits="Page_Import_FSN" %>


<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc2" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link type="text/css" rel="stylesheet" href="assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							นำเข้าข้อมูล FSN
						    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                            </asp:ToolkitScriptManager>
						</h3>		
						<ul class="breadcrumb">
                            <li><font color="blue"></font></li>
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
				     </div>				
			    </div>
                <div class="row-fluid">
                           <div class="portlet-body form form-horizontal">
                            <div class="span12">
                                
                                
                            </div>
                            <div class="row-fluid">
													<!--/span-->
													<div class="span6 ">
                                                         <div class="control-group">
										                        <label class="control-label">เลือกไฟล์ .xlsx&nbsp;&nbsp;</label>
															    <div class="controls">
                                                                     <asp:FileUpload  ID="ful" runat="server"    style=" width :100%; "  />    
														        </div>
													    </div>
                                                        <div class="control-group">
										                        <label class="control-label">&nbsp;&nbsp;</label>
															    <div class="controls">
                                                                    <asp:Button ID="btnUpload" CssClass ="btn blue" runat="server" CommandName="Upload" Text ="แสดงข้อมูลในตาราง"   />
                                                                    <asp:Button ID="btnRemove" runat="server" CommandName="Remove" style="display:none"/>  	 											
															    </div>
														    </div>
													</div>
													<!--/span-->
													<div class="span6 ">
                                                        <div class="control-group">
										                        <label class="control-label"> ข้อมูลที่นำเข้า</label>
															    <div class="controls">
                                                                        <asp:DropDownList ID="ddlFile" runat="server" CssClass="medium m-wrap" AutoPostBack="true">
													                        <asp:ListItem Text="FSN"></asp:ListItem>
													                        <asp:ListItem Text="Supplier"></asp:ListItem>
													                    </asp:DropDownList>
                                                                </div>
														 </div>
													</div>
                                                    

												</div>

                <asp:Panel ID="pnlFSN" runat="server" Visible="false">
                
                <div class="portlet-body form form-horizontal">	
                        <div class="form-actions">
						    <asp:Button CssClass="btn blue "  Width ="500px"  id="btn_import" runat="server"  Text="นำเข้าข้อมูล"></asp:Button>  
                        </div> 
                </div>
              
                <div class="row-fluid">
                <div class="span12">
						<div class="portlet box red">
							<div class="portlet-title">
								<div class="caption"><i class="icon-envelope-alt"></i>นำเข้ารายชื่อครุภัณฑ์ FSN</div>
							</div>
							<div class="portlet-body">
 									<!-- BEGIN FORM-->  
                                <div class ="space12" style ="margin-bottom :2em;" >
                                    <h5 style="color:#35AA47; font-weight:bold;">
                                            พบ <span class="text"><asp:Label ID="lblCountRows" runat="server"></asp:Label></span> รายการ&nbsp;
                                    </h5>
                                </div>                             

                            <div class="portlet-body no-more-tables">
								<table class="table table-bordered table-striped">                                
									<thead>
										<tr>
                                            <th style="text-align:center; vertical-align:middle;">No.</th> 
                                            <th style="text-align:center; vertical-align:middle;">ประเภท</th> 
                                            <th style="text-align:center; vertical-align:middle;">ชนิด</th> 
                                            <th style="text-align:center; vertical-align:middle;">ชนิดย่อย</th> 
                                            <th style="text-align:center; vertical-align:middle;">รหัส (Running no)</th> 
                                            <th style="text-align:center; vertical-align:middle;">ชื่อประเภท</th> 
                                            <th style="text-align:center; vertical-align:middle;">ชื่อชนิด</th> 
                                            <th style="text-align:center; vertical-align:middle;">ชื่อชนิดย่อย</th> 
                                            <th style="text-align:center; vertical-align:middle;">ชื่อรหัส (Running no)</th>
										</tr>
									</thead>
									<tbody>
                                    <asp:Repeater ID="rptData" runat="server">
                                        <ItemTemplate>
										    <tr id="trView">

                                                <th style="text-align:center; vertical-align:middle;"><asp:Label ID="lblNumber" runat="server" ></asp:Label></th> 
                                                <th style="text-align:center; vertical-align:middle;"><asp:Label ID="lblCat_No" runat="server" ></asp:Label></th> 
                                                <th style="text-align:center; vertical-align:middle;"><asp:Label ID="lblSub_Cat_No" runat="server" ></asp:Label></th> 
                                                <th style="text-align:center; vertical-align:middle;"><asp:Label ID="lblType_No" runat="server" ></asp:Label></th> 
                                                <th style="text-align:center; vertical-align:middle;"><asp:Label ID="lblRunning_No" runat="server" ></asp:Label></th> 
                                                <th style="text-align:center; vertical-align:middle;"><asp:Label ID="lblCat_Name" runat="server" ></asp:Label></th> 
                                                <th style="text-align:center; vertical-align:middle;"><asp:Label ID="lblSub_Cat_Name" runat="server" ></asp:Label></th> 
                                                <th style="text-align:center; vertical-align:middle;"><asp:Label ID="lblType_Name" runat="server" ></asp:Label></th> 
                                                <th style="text-align:center; vertical-align:middle;"><asp:Label ID="lblRunning_Name" runat="server" ></asp:Label></th>

											    
										    </tr>
										</ItemTemplate> 
                                        </asp:Repeater>
									</tbody>
								</table>
                            </div> 
                        
                            <div class="portlet-body form form-horizontal">	
                                    <div class="form-actions">
						                <asp:Button CssClass="btn blue "  Width ="500px"  id="btnSave" runat="server"  CommandName="Update_FSN" Text="นำเข้าข้อมูล"></asp:Button>  
                                    </div> 
                            </div>  
                                   
							</div>
						</div>
					</div>
				</div>
                </asp:Panel>                               
             


                <asp:Panel ID="pnlSupplier" runat="server" Visible="false">
                
                <div class="portlet-body form form-horizontal">	
                        <div class="form-actions">
						    <asp:Button CssClass="btn blue "  Width ="500px"  id="btn_import_Supplier" runat="server"  Text="นำเข้าข้อมูลผู้ขาย"></asp:Button>  
                        </div> 
                </div>
              
                <div class="row-fluid">
                <div class="span12">
						<div class="portlet box red">
							<div class="portlet-title">
								<div class="caption"><i class="icon-envelope-alt"></i>นำเข้ารายชื่อผู้ประกอบการ</div>
							</div>
							<div class="portlet-body">
 									<!-- BEGIN FORM-->  
                                <div class ="space12" style ="margin-bottom :2em;" >
                                    <h5 style="color:#35AA47; font-weight:bold;">
                                            พบ <span class="text"><asp:Label ID="lblCound_Supplier" runat="server"></asp:Label></span> รายการ&nbsp;
                                    </h5>
                                </div>                             

                            <div class="portlet-body no-more-tables">
								<table class="table table-bordered table-striped">                                
									<thead>
										<tr>
                                            <th style="text-align:center; vertical-align:middle;">No.</th> 
                                            <th style="text-align:center; vertical-align:middle;">TAX_NO</th> 
                                            <th style="text-align:center; vertical-align:middle;">SUPPLIER_NAME</th> 
                                            <th style="text-align:center; vertical-align:middle;">ADDRESS</th>
                                            <th style="text-align:center; vertical-align:middle;">PHONE</th> 
                                            <th style="text-align:center; vertical-align:middle;">FAX</th> 
                                            <th style="text-align:center; vertical-align:middle;">EMAIL</th>

										</tr>
									</thead>
									<tbody>
                                    <asp:Repeater ID="rptSupplier" runat="server">
                                        <ItemTemplate>
										    <tr id="trView">

                                                <th style="text-align:center; vertical-align:middle;"><asp:Label ID="lblNo" runat="server" ></asp:Label></th> 
                                                <th style="text-align:center; vertical-align:middle;"><asp:Label ID="lblTAX_NO" runat="server" ></asp:Label></th> 
                                                <th style="text-align:center; vertical-align:middle;"><asp:Label ID="lblSUPPLIER_NAME" runat="server" ></asp:Label></th> 
                                                <th style="text-align:center; vertical-align:middle;"><asp:Label ID="lblADDRESS" runat="server" ></asp:Label></th>
                                                <th style="text-align:center; vertical-align:middle;"><asp:Label ID="lblPHONE" runat="server" ></asp:Label></th> 
                                                <th style="text-align:center; vertical-align:middle;"><asp:Label ID="lblFAX" runat="server" ></asp:Label></th> 
                                                <th style="text-align:center; vertical-align:middle;"><asp:Label ID="lblEMAIL" runat="server" ></asp:Label></th> 											    
										    </tr>
										</ItemTemplate> 
                                        </asp:Repeater>
									</tbody>
								</table>
                            </div> 
                        
                            <div class="portlet-body form form-horizontal">	
                                    <div class="form-actions">
						                <asp:Button CssClass="btn blue "  Width ="500px"  id="btn_import_Supplier2" runat="server"  Text="นำเข้าข้อมูลผู้ขาย"></asp:Button>  
                                    </div> 
                            </div>  
                                   
							</div>
						</div>
					</div>
				</div>
                </asp:Panel>                               


                </div>
</div> 
                
    <script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>	
	<script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.js" type="text/javascript" ></script>
	<script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>

    </div>
</asp:Content>



