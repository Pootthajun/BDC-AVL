﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Setting_ReAss_Question.aspx.vb" Inherits="Setting_ReAss_Question" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link type="text/css" rel="stylesheet" href="assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>

    <div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							กำหนดชุดคำถามทบทวน
						</h3>		
						
									
						<ul class="breadcrumb">
                            
                            <li><i class="icon-cogs"></i> <a href="javascript:;">ตั้งค่าระบบ</a><i class="icon-angle-right"></i></li>
                            <li><i class="icon-th-list"></i> <a href="javascript:;">ใบทบทวน</a><i class="icon-angle-right"></i></li>
                        	<li><i class="icon-folder-open"></i> <a href="javascript:;">กำหนดชุดคำถามทบทวน</a></li>
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->	
				     </div>				
			    </div>
               <asp:Panel ID="pnlList" runat="server">
               <div class="row-fluid">
               <div class="span12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
						<div class="portlet">
							<div class="portlet-body">
                                <div class="table-toolbar">
									<div class="btn-group">
                                        <asp:Button ID="btnAdd_Group_Question" runat="server" CssClass="btn green " Text="เพิ่มหมวดคำถามทบทวน" />
									</div>
								</div>

                                <div class="portlet-body no-more-tables">                                            
								<table class="table table-bordered table-advance">                                
									<thead>
										<tr>
											<th id="thEdit_Header" runat="server"  style="text-align:center; vertical-align:middle;" ><b>กำหนด</b></th>
											<th style="text-align:center; vertical-align:middle;"><b>หมวดคำถามทบทวน</b></th>
											<th style="text-align:center; vertical-align:middle;"><b>หัวข้อการประเมิน</b></th>
                                            <th style="text-align:center; vertical-align:middle;" ><b>สิทธิผู้ประเมิน</b></th>
                                            <th style="text-align:center; vertical-align:middle;" ><b>เกณฑ์คะแนนผ่าน</b></th>
											<th style="text-align:center; vertical-align:middle;" ><b>คะแนนเต็ม</b></th>
											<th style="text-align:center; vertical-align:middle;" ><b>ใช้ในระบบ</b></th>
										</tr>
									</thead>
									<tbody>
                                    <asp:Repeater ID="rptList" runat="server">
										<ItemTemplate>
										<tr id="tr_rptList" runat ="server" >
											<td id="td1"  style="border-right:1px solid #eeeeee; text-align:center;" runat="server" data-title="ดำเนินการ" >
                                                <asp:Button ID="btnEdit" runat="server" CssClass="btn mini purple" CommandName="Edit" Text="กำหนด" />
                                            </td>
											<td id="td2" style="border-right:1px solid #eeeeee; " runat="server" data-title="หมวดคำถามทบทวน"><b><asp:Label ID="lblQ_GP_Name" runat="server"></asp:Label></b></td>
											<td id="td3" style="border-right:1px solid #eeeeee; " runat="server" data-title="หัวข้อการประเมิน"> <asp:Label ID="lblQ_Name" runat="server"></asp:Label></td>
                                            <td data-title="สิทธิผู้ประเมิน" style="border-right:1px solid #eeeeee; text-align :center ;"> <asp:Label ID="lblName_Role" runat="server"></asp:Label></td>
											<td id="td4" style="border-right:1px solid #eeeeee;  text-align :center ;" runat="server" data-title="เกณฑ์คะแนนผ่าน" > <asp:Label ID="lblPass_Score" runat="server"></asp:Label></td>
                                            <td id="td5" style="border-right:1px solid #eeeeee; text-align :center ;border-bottom :none;" runat="server" data-title="คะแนนเต็ม" ><asp:Label ID="lblMax_Score" runat ="server"  ></asp:Label></td>
											<td id="td6" style="border-right:1px solid #eeeeee;  text-align :center ;" runat="server" data-title="ใช้ในระบบ"  ><a ID="img_Status" runat="server" target="_blank"></a></td>

										</tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
									</tbody>
                                    <tfoot>
                                        <tr id="trFooter" runat ="server" >

											<td data-title="ดำเนินการ" style="text-align:center; "></td>
											<td data-title="หมวดคำถามทบทวน" style =" text-align :center ;">&nbsp;</td>
											<td data-title="หัวข้อการประเมิน" style =" text-align :center ;"><b>
                                                <span style="color :black; font-size :16px;">รวมคะแนน</span>
                                                <span style="color :Blue; font-size :16px;">(เฉพาะข้อที่ใช้งานในระบบ)</span></b></td>
                                            <td data-title="สิทธิผู้ประเมิน" style="text-align:center; "></td>
                                            <td data-title="เกณฑ์คะแนนผ่าน"  style =" text-align :center ; "><b><U><asp:Label ID="lblSUM_Pass_Score" runat ="server" Font-Size ="16px" ></asp:Label></U></b></td>
											<td data-title="คะแนนเต็ม" style =" text-align :center ;"><b><U><asp:Label ID="lblSUM_Max_Score" runat ="server" Font-Size ="16px" ></asp:Label></U></b></td>
											<td data-title="ใช้ในระบบ"  style =" text-align :center ;"></td>
										</tr>
                                    </tfoot>
								</table>
                            </div> 
                                                                                      
                                <div class="row-fluid">
                                </div>                                   

							</div>
						</div>
						<!-- END SAMPLE TABLE PORTLET-->
					</div>
				</div>
               </asp:Panel>
                <asp:Panel ID="pnlEdit" runat="server" DefaultButton="btnOK">
                <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN VALIDATION STATES-->
						<div class="portlet box blue">
							<div class="portlet-title">
								<div class="caption"><i class="icon-folder-open"></i>เพิ่ม/แก้ไข หมวดคำถามทบทวน</div>
								<div class="tools">
								</div>
							</div>
							<div class="portlet-body form form-horizontal">
                                <form class="form-horizontal" id="form_sample_2" action="#" novalidate="novalidate">
									
									<div class="control-group">
										<label class="control-label" style =" font-size :16px;"><b>หมวดคำถาม </b> <font color="red">*</font>&nbsp;&nbsp;</label>
										<div class="controls">
                                            <asp:Label ID="lblQ_GP_ID" runat="server"  style="display:none;"  ></asp:Label>
											<asp:TextBox ID="txtQ_GP_Name" TextMode="MultiLine" Font-Size ="18px" Width ="100%"  runat="server"  AutoPostBack ="true"  rows="1" class="span6 m-wrap" cols="20"></asp:TextBox>
										</div>
									</div>
                                    
									<div class="control-group">
										<label class="control-label">&nbsp;&nbsp;</label>
										<div class="controls">

                                            <asp:Panel ID="pnlList_Question" runat="server">
								            <table class="table table-full-width table-advance dataTable no-more-tables table-hover">                                
									            <thead>
										            <tr>
											            <th style="text-align:center; vertical-align:middle;"><b>หัวข้อการประเมิน</b></th>
                                                        <th style="vertical-align:middle; text-align :center ; "><b>สิทธิผู้ประเมิน</b></th>
                                                        <th style="text-align:center; vertical-align:middle;" ><b>เกณฑ์คะแนนผ่าน</b></th>
											            <th style="text-align:center; vertical-align:middle;" ><b>คะแนนเต็ม</b></th>
											            <th style="text-align:center; vertical-align:middle;" ><b>ใช้ในระบบ</b></th>
											            <th style="text-align:center; vertical-align:middle; width :130px;" ><b>ดำเนินการ</b></th>
										            </tr>
									            </thead>
									            <tbody>
                                                <asp:Repeater ID="rptQuestion" runat="server">
										            <ItemTemplate>
										            <tr>
											            <td data-title="หัวข้อการประเมิน"><asp:Label ID="lbl_Question_Q_Name" runat ="server"  ></asp:Label></td>
                                                        <td data-title="สิทธิผู้ประเมิน"  style="text-align :center ;"><asp:Label ID="lbl_AR_Name" runat ="server"  ></asp:Label></td>
                                                        <td data-title="เกณฑ์คะแนนผ่าน" style =" text-align :center ;"><asp:Label ID="lbl_Question_Pass_Score" runat ="server"  ></asp:Label></td>
											            <td data-title="คะแนนเต็ม" style =" text-align :center ;"><asp:Label ID="lbl_Question_Max_Score" runat ="server"  ></asp:Label></td>
											            <td data-title="ใช้ในระบบ"  style =" text-align :center ;"><a ID="img_Status_Question" runat="server" target="_blank"></a></td>
											            <td data-title="ดำเนินการ" style="text-align:center; ">
                                                            <asp:Button ID="btnEdit_Question" runat="server" CssClass="btn mini purple" CommandName="Edit" Text="แก้ไข" />
                                                            <asp:Button ID="btnDel_Question" runat="server" CssClass="btn mini red" CommandName="Delete" Text="ลบ" />
                                                            <asp:ConfirmButtonExtender ID="btnDel_Confirm_SubQuestion" runat="server" ConfirmText="ลบหัวข้อการประเมิน ?" TargetControlID="btnDel_Question"></asp:ConfirmButtonExtender>
                                                        </td>
										            </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
									            </tbody>
                                                <tfoot>
                                                    <tr>
											            <td data-title="หัวข้อการประเมิน" style =" text-align :center ;"><b><span style ="color :black; font-size :16px;">รวมคะแนน</span> <span style ="color :Blue; font-size :16px;">(เฉพาะข้อที่ใช้งานในระบบ)</span></b></td>
											            <td data-title="สิทธิผู้ประเมิน"  style="text-align :center ;"></td>
                                                        <td data-title="เกณฑ์คะแนนผ่าน"  style =" text-align :center ;"><b><U><asp:Label ID="lbl_Question_SUM_Pass_Score" runat ="server" Font-Size ="16px" ></asp:Label></U></b></td>
                                                        <td data-title="คะแนนเต็ม" style =" text-align :center ;"><b><U><asp:Label ID="lbl_Question_SUM_Max_Score" runat ="server" Font-Size ="16px" ></asp:Label></U></b></td>
											            <td data-title="ใช้ในระบบ"  style =" text-align :center ;"></td>
											            <td data-title="ดำเนินการ" style="text-align:center; "></td>
										            </tr>
                                                </tfoot>
								            </table>
                                            </asp:Panel> 
                                            <asp:Button ID="btnAdd_Question" runat="server" Width ="100%" CssClass="btn green" Text="Click เพิ่มหัวข้อการประเมิน" />
                                        </div>
									</div>
                                    <%--<div class="control-group" >
										<label class="control-label" style =" font-size :16px;"><b>ลำดับข้อ&nbsp;&nbsp;</b> </label>
										<div class="controls">
                                                <asp:TextBox ID="txtQ_GP_Order" runat="server"  class=" m-wrap" style=" margin:0px; width :50px;"></asp:TextBox>
										</div>
									</div> --%>
                                    <div class="control-group">
									    <label class="control-label">ลำดับข้อ <font color="red"></font>&nbsp;&nbsp;</label>
									    <div class="controls">
										    <asp:TextBox ID="txtQ_GP_Order" runat="server"  class=" m-wrap" style=" margin:0px; width :50px; text-align:right; display :none;  "></asp:TextBox>
                                            <asp:DropDownList ID="ddlQ_GP_Order" runat="server" CssClass="m-wrap" style=" margin:0px; width :70px; text-align:center ;" ></asp:DropDownList> 
                                        </div>
								    </div>

                                    <div class="control-group">
										<label class="control-label" style =" font-size :16px;"><b>ใช้ในระบบ&nbsp;&nbsp;</b> </label>
										<div class="controls">
                                           <asp:ImageButton ID="imgGroupStatus" runat="server" ImageUrl="images/check.png" ToolTip="Click เพื่อเปลี่ยน" />
										</div>
									</div> 

									<div class="form-actions">
                                        <asp:Button ID="btnOK" runat="server" CssClass="btn blue" Text="บันทึกหมวดคำถาม"  Visible ="true" />
                                        <asp:Button ID="btnCancel" runat="server" CssClass="btn" Text="ย้อนกลับ" />
                                        <asp:Button ID="btnDelete" runat="server" CssClass="btn red" Text="ลบคำถามทบทวนทั้งหมด" />
                                        <asp:ConfirmButtonExtender ID="btnDelete_Confirm" runat="server" ConfirmText="ลบหัวข้อการประเมินนี้ทั้งหมด?" TargetControlID="btnDelete"></asp:ConfirmButtonExtender>
										
									</div>
								</form>
								<!-- END FORM-->
							</div>
						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>
                </asp:Panel> 

                                   
   
                <asp:Panel CssClass="modal" style="top:10%; " id="Modal_Add" runat="server" Visible="False" >
					<div class="modal-header">										
						<h3><asp:Label ID="Label2" runat="server"></asp:Label>หัวข้อการประเมิน</h3>
					</div>
					<div class="modal-body">
						
              
						<div class="portlet-body form form-horizontal">
								<div class="control-group">
									<label class="control-label">หัวข้อย่อย <font color="red">*</font>&nbsp;&nbsp;</label>
									<div class="controls">
                                        <asp:Label ID="lblDialog_Q_ID" runat="server"  style="display:none;"  ></asp:Label>
										<asp:TextBox ID="txtDialog_Q_Name" TextMode="MultiLine"  runat="server" rows="4" class="span4 m-wrap" cols="20"></asp:TextBox>
									</div>
								</div>
                                <div class="control-group">
									<label class="control-label">สิทธิการประเมิน <font color="red">*</font>&nbsp;&nbsp;</label>
									<div class="controls">
										<asp:DropDownList ID="ddlAss_Role" runat="server" CssClass="medium m-wrap" AutoPostBack="true">
										    <asp:ListItem Text=""></asp:ListItem>
										    <asp:ListItem Text="เจ้าหน้าที่" ></asp:ListItem>
										    <asp:ListItem Text="ผู้ตรวจสอบ"></asp:ListItem>
                                            <asp:ListItem Text="เจ้าหน้าที่พัสดุ" ></asp:ListItem>
										    <asp:ListItem Text="ฝ่ายบริหาร"></asp:ListItem>
										</asp:DropDownList>
                                    </div>
								</div>
                                <div class="control-group">
									<label class="control-label">เกณฑ์คะแนนผ่าน <font color="red">*</font>&nbsp;&nbsp;</label>
									<div class="controls">
										<asp:TextBox ID="txtDialog_Pass_Score" runat="server"  class=" m-wrap" style=" margin:0px; width :50px; text-align:right;"></asp:TextBox>
                                    </div>
								</div>

                                <div class="control-group">
									<label class="control-label">คะแนนเต็ม <font color="red">*</font>&nbsp;&nbsp;</label>
									<div class="controls">
										<asp:TextBox ID="txtDialog_Max_Score" runat="server"  class=" m-wrap" style=" margin:0px; width :50px; text-align:right;"></asp:TextBox>
                                    </div>
								</div>
									
                                <div class="control-group">
									<label class="control-label">ลำดับข้อย่อย <font color="red"></font>&nbsp;&nbsp;</label>
									<div class="controls">
										<asp:TextBox ID="txtDialog_Q_Order" runat="server"  class=" m-wrap" style=" margin:0px; width :50px; text-align:right; display :none ; "></asp:TextBox>
                                        <asp:DropDownList ID="ddl_Q_Order" runat="server" CssClass="m-wrap" style=" margin:0px; width :70px; text-align:center ;" AutoPostBack="True"></asp:DropDownList> 
                                    </div>
								</div>

                                <div class="control-group">
									<label class="control-label" >ใช้ในระบบ&nbsp;&nbsp;</label>
									<div class="controls">
                                         <asp:ImageButton ID="imgQuestionStatus" runat="server" ImageUrl="images/check.png" ToolTip="Click เพื่อเปลี่ยน" />
									</div>
								</div> 
                                
    
                        </div> 
					</div>
					<div class="modal-footer">
                        <asp:Button ID="btnOK_Sub" runat="server" CssClass="btn green " Text="บันทึกหัวข้อการประเมิน"  />	
					    <asp:Button ID="btnClose" runat="server" CssClass="btn" Text="ยกเลิก" />
															
					</div>
				</asp:Panel>



            </div>



        </ContentTemplate>
    </asp:UpdatePanel>
    <script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>	
	<script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.js" type="text/javascript" ></script>
	<script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>

</asp:Content>





