﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO


Partial Class SettingSupplier
    Inherits System.Web.UI.Page

    Dim BL As New AVLBL
    Dim CL As New textControlLib


    Public Property IsActive As Boolean
        Get
            Return imgStatus.ImageUrl = "images/check.png"
        End Get
        Set(value As Boolean)
            If value Then
                imgStatus.ImageUrl = "images/check.png"
            Else
                imgStatus.ImageUrl = "images/none.png"
            End If
        End Set
    End Property

    Private ReadOnly Property PageName As String
        Get
            Return "SettingSupplier.aspx"
        End Get
    End Property
    Private Property AccessMode As AVLBL.AccessRole
        Get
            Try
                Return ViewState("AccessMode")
            Catch ex As Exception
                Return AVLBL.AccessRole.None
            End Try
        End Get
        Set(value As AVLBL.AccessRole)
            ViewState("AccessMode") = value
        End Set
    End Property
    Private ReadOnly Property User_ID As Integer
        Get
            Try
                If IsNumeric(Session("User_ID")) Then
                    Return Session("User_ID")
                Else
                    Return -1
                End If
            Catch ex As Exception
                Return -1
            End Try
        End Get
    End Property


    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
         If IsNothing(Session("User_ID")) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "ShowAlert('warning','กรุณาล็อกอินเข้าสู่ระบบ','Login.aspx');", True)
            Response.Redirect("Login.aspx")
            Exit Sub
        End If

        If Not IsPostBack Then
            '----------- Check สิทธิ์ในเมนู --------------
            SetUserRole()

            ClearData()
        End If

    End Sub

    Private Sub SetUserRole()

        '----------- Check สิทธิ์ในเมนู --------------
        Dim MT As DataTable = BL.GetUserMenuRole(User_ID, PageName)
        If MT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลนี้','Overview.aspx');", True)
            Exit Sub
        End If
        AccessMode = MT.Rows(0).Item("MR_ID") '--------- สิทธิ์ในเมนูของผู้ใช้คนปัจจุบัน -----------
        If AccessMode = AVLBL.AccessRole.None Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลนี้','Overview.aspx');", True)
            Exit Sub
        End If

        btnAdd.Visible = AccessMode = AVLBL.AccessRole.Edit

    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As System.EventArgs) Handles btnAdd.Click
        pnlData.Visible = False
        pnlEdit.Visible = True
        'CL.ImplementJavaIntegerText(txtSupplier_TaxNo)

        txtSupplier_TaxNo.Attributes.Add("ID", "")
        txtSupplier_TaxNo.Focus()
    End Sub

    Private Sub ClearData()
        pnlEdit.Visible = False
        pnlData.Visible = True
        pnl_btn.Visible = False
        CL.ImplementJavaOnlyNumberText(txtSupplier_TaxNo)
        txtSupplier_TaxNo.Text = ""
        txtSupplier_Name.Text = ""
        txtSupplier_NameAlias.Text = ""
        txtSupplier_Address.Text = ""
        txtSupplier_Phone.Text = ""
        txtSupplier_Fax.Text = ""
        txtSupplier_Contact_Name.Text = ""
        txtSupplier_Email.Text = ""
        IsActive = True
        BindSupplierList()

    End Sub

    Private Sub BindSupplierList()
        Dim SQL As String = ""
        SQL &= "  SELECT Supplier.* " & vbLf
        SQL &= "  FROM tb_Sup Supplier" & vbLf
        SQL &= "  WHERE Supplier.S_Tax_No IS NOT NULL " & vbLf
        If txt_Search_TaxNo.Text <> "" Then
            SQL &= " AND (Supplier.S_Tax_No LIKE '%" & txt_Search_TaxNo.Text.Replace("'", "''") & "%' ) " & vbLf
        End If
        If txt_Search_Name.Text <> "" Then
            SQL &= " AND (Supplier.S_Name LIKE '%" & txt_Search_Name.Text.Replace("'", "''") & "%' OR " & vbLf
            SQL &= " Supplier.S_Alias LIKE '%" & txt_Search_Name.Text.Replace("'", "''") & "%' OR " & vbLf
            SQL &= " Supplier.S_Contact_Name LIKE '%" & txt_Search_Name.Text.Replace("'", "''") & "%' )" & vbLf
        End If
        If ddlStatus.Text <> "All" Then
            SQL &= " AND (Supplier.Active_Status=" & ddlStatus.Items(ddlStatus.SelectedIndex).Value & " ) " & vbLf
        End If
        SQL &= "  ORDER BY Active_Status desc,Supplier.S_Name" & vbLf
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        Session("SettingSupplier") = DT
        Pager.SesssionSourceName = "SettingSupplier"
        Pager.RenderLayout()

        If DT.Rows.Count = 0 Then
            lblCountList.Text = "ไม่พบรายการดังกล่าว"
        Else
            lblCountList.Text = "พบ " & FormatNumber(DT.Rows.Count, 0) & " รายการ"
        End If

    End Sub

    Protected Sub Pager_PageChanging(ByVal Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptData
    End Sub
    Protected Sub btnSearch_Click(sender As Object, e As System.EventArgs) Handles btnSearch.Click, txt_Search_TaxNo.TextChanged, txt_Search_Name.TextChanged, ddlStatus.SelectedIndexChanged
        BindSupplierList()
    End Sub


    Protected Sub rptData_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptData.ItemCommand
        Dim lblId As Label = e.Item.FindControl("lblId")
        Dim btnRptDelete As LinkButton = e.Item.FindControl("btnRptDelete")
        Select Case e.CommandName
            Case "Edit"
                pnlEdit.Visible = True
                pnlData.Visible = False

                txtSupplier_TaxNo.Attributes.Add("ID", lblId.Text)
                Dim sql As String = ""
                sql = "select * from tb_Sup WHERE S_ID = " & lblId.Text
                Dim DA As New SqlDataAdapter(sql, BL.ConnectionString)
                Dim DT As New DataTable
                DA.Fill(DT)

                txtSupplier_TaxNo.Text = DT.Rows(0).Item("S_Tax_No").ToString
                txtSupplier_Name.Text = DT.Rows(0).Item("S_Name").ToString
                txtSupplier_NameAlias.Text = DT.Rows(0).Item("S_Alias").ToString
                txtSupplier_Address.Text = DT.Rows(0).Item("S_Address").ToString
                txtSupplier_Phone.Text = DT.Rows(0).Item("S_Phone").ToString
                txtSupplier_Fax.Text = DT.Rows(0).Item("S_Fax").ToString
                txtSupplier_Contact_Name.Text = DT.Rows(0).Item("S_Contact_Name").ToString
                txtSupplier_Email.Text = DT.Rows(0).Item("S_Email").ToString

                IsActive = CBool(DT.Rows(0).Item("Active_Status"))

                '---สิทธิ์ในเมนู----
                pnlEdit.Enabled = AccessMode = AVLBL.AccessRole.Edit
                btnSave.Visible = AccessMode = AVLBL.AccessRole.Edit
                btnCancel.Visible = AccessMode = AVLBL.AccessRole.Edit
                pnl_btn.Visible = AccessMode = AVLBL.AccessRole.View

                ' ดอกจัน Validate
                lblValidate_Supplier_TaxNo.Visible = AccessMode = AVLBL.AccessRole.Edit
                lblValidate_Supplier_No.Visible = AccessMode = AVLBL.AccessRole.Edit
                lblValidate_Supplier_Address.Visible = AccessMode = AVLBL.AccessRole.Edit
                lblValidate_Supplier_Contact_Name.Visible = AccessMode = AVLBL.AccessRole.Edit
                lblValidate_Supplier_Phone.Visible = AccessMode = AVLBL.AccessRole.Edit
                lblValidate_Supplier_Email.Visible = AccessMode = AVLBL.AccessRole.Edit

            Case "Delete"
                Dim SQL As String = ""
                SQL &= "DELETE FROM tb_Sup WHERE S_ID = " & lblId.Text
                Dim conn As New SqlConnection(BL.ConnectionString)
                conn.Open()
                Dim SqlCmd As SqlCommand
                SqlCmd = New SqlCommand(SQL, conn)
                SqlCmd.ExecuteNonQuery()
                conn.Close()
                ClearData()
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('success','ลบเรียบร้อย','');", True)
                Exit Sub
        End Select

    End Sub

    Protected Sub rptData_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim lblId As Label = e.Item.FindControl("lblId")
        Dim lbl_TaxNo As Label = e.Item.FindControl("lbl_TaxNo")
        Dim lbl_Name As Label = e.Item.FindControl("lbl_Name")
        Dim lbl_NameAlias As Label = e.Item.FindControl("lbl_NameAlias")
        Dim lbl_ContactName As Label = e.Item.FindControl("lbl_ContactName")
        Dim lbl_TypeName As Label = e.Item.FindControl("lbl_TypeName")
        Dim imgStatus As Image = e.Item.FindControl("imgStatus")
        Dim btnRptDelete As LinkButton = e.Item.FindControl("btnRptDelete")

        lblId.Text = e.Item.DataItem("S_ID").ToString
        lbl_TaxNo.Text = "<b>" & e.Item.DataItem("S_Tax_No").ToString & "</b>"
        lbl_Name.Text = "<b>" & e.Item.DataItem("S_Name").ToString & "</b>"
        lbl_NameAlias.Text = e.Item.DataItem("S_Alias").ToString
        lbl_ContactName.Text = e.Item.DataItem("S_Contact_Name").ToString
        If e.Item.DataItem("S_Phone").ToString <> "" Then
            lbl_ContactName.Text = lbl_ContactName.Text & "  ( " & e.Item.DataItem("S_Phone").ToString & " )"
        End If
        If e.Item.DataItem("S_Email").ToString <> "" Then
            lbl_ContactName.Text = lbl_ContactName.Text & "  :  " & e.Item.DataItem("S_Email").ToString
        End If

        If CBool(e.Item.DataItem("ACTIVE_STATUS")) = True Then
            imgStatus.ImageUrl = "images/check.png"
        Else
            imgStatus.ImageUrl = "images/none.png"
        End If

        '---สิทธิ์ในเมนู----
        btnRptDelete.Visible = AccessMode = AVLBL.AccessRole.Edit
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click, btnBack.Click
        pnlData.Visible = True
        pnlEdit.Visible = False
        ClearData()
    End Sub


    Protected Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        If txtSupplier_TaxNo.Text.Trim = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','กรุณากรอกข้อมูล เลขผู้เสียภาษี','');", True)
            txtSupplier_TaxNo.Focus()
            Exit Sub
        End If

        If txtSupplier_TaxNo.Text.Length <> 13 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','กรุณากรอกข้อมูล เลขผู้เสียภาษีเป็นตัวเลข 13 หลัก','');", True)
            txtSupplier_TaxNo.Focus()
            Exit Sub
        End If

        If txtSupplier_Name.Text.Trim = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','กรุณากรอกข้อมูล ชื่อหลัก','');", True)
            txtSupplier_Name.Focus()
            Exit Sub
        End If

        If txtSupplier_Address.Text.Trim = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','กรุณากรอกข้อมูล ที่อยู่','');", True)
            txtSupplier_Address.Focus()
            Exit Sub
        End If

        If txtSupplier_Contact_Name.Text.Trim = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','กรุณากรอกข้อมูล ชื่อผู้ติดต่อ','');", True)
            txtSupplier_Contact_Name.Focus()
            Exit Sub
        End If

        If txtSupplier_Phone.Text.Trim = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','กรุณากรอกข้อมูล เบอร์โทรศัพท์','');", True)
            txtSupplier_Phone.Focus()
            Exit Sub
        End If

        If txtSupplier_Email.Text.Trim = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','กรุณากรอกข้อมูล อีเมล์','');", True)
            txtSupplier_Email.Focus()
            Exit Sub
        End If

        Dim SQL As String = ""
        Dim ID As String = ""
        If txtSupplier_TaxNo.Attributes("ID").ToString <> "" Then
            ID = txtSupplier_TaxNo.Attributes("ID").ToString
        End If

        Dim DR As DataRow
        'Add
        If ID = "" Then
            SQL &= "SELECT * FROM tb_Sup WHERE S_Tax_No='" & txtSupplier_TaxNo.Text.Replace("'", "''") & "'" & vbCrLf
            Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
            Dim DT As New DataTable
            DA.Fill(DT)
            If DT.Rows.Count > 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','เลขผู้เสียภาษี ซ้ำกับข้อมูลในระบบ','');", True)
                txtSupplier_TaxNo.Focus()
                Exit Sub
            End If

            DR = DT.NewRow
            DR("S_ID") = BL.GetNewPrimaryID("tb_Sup", "S_ID")
            DR("S_Tax_No") = txtSupplier_TaxNo.Text
            DR("S_Name") = txtSupplier_Name.Text
            DR("S_Alias") = txtSupplier_NameAlias.Text
            DR("S_Address") = txtSupplier_Address.Text
            DR("S_Phone") = txtSupplier_Phone.Text
            DR("S_Fax") = txtSupplier_Fax.Text
            DR("S_Contact_Name") = txtSupplier_Contact_Name.Text
            DR("S_Email") = txtSupplier_Email.Text
            DR("ACTIVE_STATUS") = IsActive
            DR("Update_By") = Session("User_ID")
            DR("Update_Time") = Now
            DT.Rows.Add(DR)
            Dim cmd As New SqlCommandBuilder(DA)
            DA.Update(DT)
        Else
            'Edit
            SQL &= "SELECT * FROM tb_Sup WHERE S_Tax_No='" & txtSupplier_TaxNo.Text.Replace("'", "''") & "'" & vbCrLf
            SQL &= "AND S_ID <> " & ID
            Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
            Dim DT As New DataTable
            DA.Fill(DT)
            If DT.Rows.Count > 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','เลขผู้เสียภาษี ซ้ำกับข้อมูลในระบบ','');", True)
                txtSupplier_TaxNo.Focus()
                Exit Sub
            End If

            SQL = "SELECT * FROM tb_Sup WHERE S_ID = " & ID
            DA = New SqlDataAdapter(SQL, BL.ConnectionString)
            DT = New DataTable
            DA.Fill(DT)

            DR = DT.Rows(0)
            DR("S_Tax_No") = txtSupplier_TaxNo.Text
            DR("S_Name") = txtSupplier_Name.Text
            DR("S_Alias") = txtSupplier_NameAlias.Text
            DR("S_Address") = txtSupplier_Address.Text
            DR("S_Phone") = txtSupplier_Phone.Text
            DR("S_Fax") = txtSupplier_Fax.Text
            DR("S_Contact_Name") = txtSupplier_Contact_Name.Text
            DR("S_Email") = txtSupplier_Email.Text
            DR("ACTIVE_STATUS") = IsActive
            DR("Update_By") = Session("User_ID")
            DR("Update_Time") = Now
            Dim cmd As New SqlCommandBuilder(DA)
            DA.Update(DT)

        End If
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('success','บันทึกเรียบร้อย','');", True)

        Exit Sub


        'ClearData()
    End Sub

    Protected Sub imgStatus_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgStatus.Click
        IsActive = Not IsActive
    End Sub

  
  
End Class
