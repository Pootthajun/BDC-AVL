﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class AssessmentView
    Inherits System.Web.UI.Page

    Dim BL As New AVLBL
    Dim GL As New GenericLib

    Private ReadOnly Property PageName As String
        Get
            Return "AssessmentView.aspx"
        End Get
    End Property

    Private ReadOnly Property User_ID As Integer
        Get
            Try
                If IsNumeric(Session("User_ID")) Then
                    Return Session("User_ID")
                Else
                    Return -1
                End If
            Catch ex As Exception
                Return -1
            End Try
        End Get
    End Property

    Private ReadOnly Property Dept_ID As String
        Get
            Try
                Return ddl_Search_Dept.Items(ddl_Search_Dept.SelectedIndex).Value
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Private Property AccessMode As AVLBL.AccessRole
        Get
            Try
                Return ViewState("AccessMode")
            Catch ex As Exception
                Return AVLBL.AccessRole.None
            End Try
        End Get
        Set(value As AVLBL.AccessRole)
            ViewState("AccessMode") = value
        End Set
    End Property


    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("User_ID")) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "ShowAlert('warning','กรุณาล็อกอินเข้าสู่ระบบ','Login.aspx');", True)
            Response.Redirect("Login.aspx")
            Exit Sub
        End If

        '--------------- Check Role (Remain)-------------------
        If Not IsPostBack Then

            Dim MT As DataTable = BL.GetUserMenuRole(User_ID, PageName)
            If MT.Rows.Count = 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลนี้','Overview.aspx');", True)
                Exit Sub
            End If
            AccessMode = MT.Rows(0).Item("MR_ID") '--------- สิทธิ์ของผู้ใช้คนปัจจุบัน -----------
            If AccessMode = AVLBL.AccessRole.None Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลนี้','Overview.aspx');", True)
                Exit Sub
            End If

            '------------- Restore Last Search Criteria------------
            BL.BindDDlDEPT(ddl_Search_Dept, User_ID)
            ClearSearchForm()
            BindList()
        End If
    End Sub

    Private Sub ClearSearchForm()
        ddlStart_M.Items.Clear()
        ddlEnd_M.Items.Clear()
        For i As Integer = 1 To 12
            ddlStart_M.Items.Add(New ListItem(GL.ReportMonthThaiShort(i), i))
            ddlEnd_M.Items.Add(New ListItem(GL.ReportMonthThaiShort(i), i))
        Next
        ddlEnd_M.SelectedIndex = 11

        ddlStart_Y.Items.Clear()
        ddlEnd_Y.Items.Clear()
        For i As Integer = Now.Year - 2 To Now.Year + 8
            ddlStart_Y.Items.Add(New ListItem(i + 543, i))
            ddlEnd_Y.Items.Add(New ListItem(i + 543, i))
        Next
        ddlEnd_Y.SelectedIndex = ddlEnd_Y.Items.Count - 1
    End Sub
    Protected Sub ddlPeriod_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlEnd_M.SelectedIndexChanged, ddlEnd_Y.SelectedIndexChanged, ddlStart_M.SelectedIndexChanged, ddlStart_Y.SelectedIndexChanged

        Dim StartMonth As Integer = (ddlStart_Y.SelectedValue * 12) + ddlStart_M.SelectedValue
        Dim EndMonth As Integer = (ddlEnd_Y.SelectedValue * 12) + ddlEnd_M.SelectedValue

        If StartMonth > EndMonth Then
            Select Case True
                Case Equals(sender, ddlStart_M) Or Equals(sender, ddlStart_Y)
                    ddlEnd_M.SelectedIndex = ddlStart_M.SelectedIndex
                    ddlEnd_Y.SelectedIndex = ddlStart_Y.SelectedIndex
                Case Equals(sender, ddlEnd_M) Or Equals(sender, ddlEnd_Y)
                    ddlStart_M.SelectedIndex = ddlEnd_M.SelectedIndex
                    ddlStart_Y.SelectedIndex = ddlEnd_Y.SelectedIndex
            End Select
        End If

    End Sub

    Protected Sub Search_Changed(sender As Object, e As System.EventArgs) Handles btnSearch.Click ', ddl_Search_Dept.SelectedIndexChanged, txt_Search_Item.TextChanged, txt_Search_Sup.TextChanged, txt_Search_Sup.TextChanged, txt_Search_Code.TextChanged, ddl_Search_Type.SelectedIndexChanged, ddl_Search_Step.SelectedIndexChanged
        BindList()
    End Sub

    Private Function GetSQL_Ass() As String
        Dim SQL As String = "SELECT DISTINCT Ass_ID,Ref_Code Ass_Ref_Code,CAST(Null AS INT) ReAss_ID,CAST(Null AS VARCHAR) ReAss_Ref_Code" & vbLf
        SQL &= " ,Item_No,Item_Name,Cat_No + Sub_Cat_No+Type_No+Running_No Item_Code" & vbLf
        SQL &= " ,Dept_ID,Dept_Name,S_ID,S_Name,S_Tax_No,S_Alias" & vbLf
        SQL &= " ,dbo.udf_AVLCode(AVL_Y,AVL_M,AVL_Dept_ID,AVL_Sub_Dept_ID,AVL_No) AVL_Code" & vbLf
        SQL &= " ,AVL_Status,Get_Score,Pass_Score,Max_Score,ISNULL(Pass_Status,1) Pass_Status,Current_Step,Step_TH,Update_Time,vw_Ass_Header.Sub_Dept_Name,vw_Ass_Header.Sub_Dept_ID  " & vbLf
        SQL &= " FROM vw_Ass_Header" & vbLf

        Dim Filter As String = ""
        If ddl_Search_Dept.SelectedIndex > 0 Then
            Filter &= " Dept_ID='" & ddl_Search_Dept.SelectedValue & "' AND "
        Else
            Filter &= " Dept_ID IN (" & vbLf
            Filter &= " SELECT D.Dept_ID FROM tb_Dept_Role R" & vbLf
            Filter &= " INNER JOIN tb_User U ON R.User_ID=U.User_ID AND U.Active_Status=1" & vbLf
            Filter &= " INNER JOIN tb_Dept D ON R.Dept_ID=D.Dept_ID AND D.Active_Status=1" & vbLf
            Filter &= " INNER JOIN tb_Ass_Role AR ON R.AR_ID=AR.AR_ID" & vbLf
            Filter &= " WHERE R.User_ID=" & User_ID & vbLf
            Filter &= " ) AND "
        End If
        If ddl_Search_SUB_DEPT.SelectedIndex > 0 Then
            Filter &= " vw_Ass_Header.Sub_Dept_ID ='" & ddl_Search_SUB_DEPT.SelectedValue & "' AND "
        End If
        If txt_Search_Item.Text <> "" Then
            Filter &= " (" & vbLf
            Filter &= " Item_No LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Item_Name LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " CAT_Name LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Sub_Cat_Name LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Type_Name LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR" & vbLf
            Filter &= " Running_Name LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR" & vbLf
            Filter &= " Cat_No + Sub_Cat_No+Type_No+Running_No LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' " & vbLf
            Filter &= ") AND "
        End If
        If txt_Search_Sup.Text <> "" Then
            Filter &= " (" & vbLf
            Filter &= " S_Tax_No LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Name LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Alias LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Address LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Replace(Replace(S_Phone,'-',''),',','') LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Replace(Replace(S_Fax,'-',''),',','') LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Contact_Name LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Email LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' " & vbLf
            Filter &= ") AND "
        End If
        If txt_Search_Code.Text <> "" Then
            Filter &= " (dbo.udf_AVLCode(AVL_Y,AVL_M,AVL_Dept_ID,AVL_Sub_Dept_ID,AVL_No) LIKE '%" & txt_Search_Code.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Ref_Code LIKE '%" & txt_Search_Code.Text.Replace("'", "''") & "%') AND " & vbLf
        End If
        If ddl_Search_Step.SelectedIndex > 0 Then
            Filter &= "Current_Step=" & ddl_Search_Step.SelectedValue & " AND "
        End If
        If Filter <> "" Then
            SQL &= "WHERE " & Filter.Substring(0, Filter.Length - 5) & vbLf
        End If

        Return SQL
    End Function

    Private Function GetSQL_ReAss() As String
        Dim SQL As String = "SELECT DISTINCT Ass_ID,dbo.udf_AssCode(Ass_Ref_Year,Ass_Ref_Month,Dept_ID,Ass_Ref_Number) Ass_Ref_Code,ReAss_ID,Ref_Code ReAss_Ref_Code" & vbLf
        SQL &= " ,Item_No,Item_Name,Cat_No + Sub_Cat_No+Type_No+Running_No Item_Code" & vbLf
        SQL &= " ,Dept_ID,Dept_Name,S_ID,S_Name,S_Tax_No,S_Alias" & vbLf
        SQL &= " ,dbo.udf_AVLCode(AVL_Y,AVL_M,AVL_Dept_ID,AVL_Sub_Dept_ID,AVL_No) AVL_Code" & vbLf
        SQL &= " ,AVL_Status,Get_Score,Pass_Score,Max_Score,ISNULL(Pass_Status,1) Pass_Status,Current_Step,Step_TH,Update_Time,vw_ReAss_Header.Sub_Dept_Name,vw_ReAss_Header.Sub_Dept_ID  " & vbLf
        SQL &= " FROM vw_ReAss_Header" & vbLf

        Dim Filter As String = ""
        If ddl_Search_Dept.SelectedIndex > 0 Then
            Filter &= " Dept_ID='" & ddl_Search_Dept.SelectedValue & "' AND "
        Else
            Filter &= " Dept_ID IN (" & vbLf
            Filter &= " SELECT D.Dept_ID FROM tb_Dept_Role R" & vbLf
            Filter &= " INNER JOIN tb_User U ON R.User_ID=U.User_ID AND U.Active_Status=1" & vbLf
            Filter &= " INNER JOIN tb_Dept D ON R.Dept_ID=D.Dept_ID AND D.Active_Status=1" & vbLf
            Filter &= " INNER JOIN tb_Ass_Role AR ON R.AR_ID=AR.AR_ID" & vbLf
            Filter &= " WHERE R.User_ID=" & User_ID & vbLf
            Filter &= " ) AND "
        End If
        If ddl_Search_SUB_DEPT.SelectedIndex > 0 Then
            Filter &= " vw_ReAss_Header.Sub_Dept_ID ='" & ddl_Search_SUB_DEPT.SelectedValue & "' AND "
        End If
        If txt_Search_Item.Text <> "" Then
            Filter &= " (" & vbLf
            Filter &= " Item_No LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Item_Name LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " CAT_Name LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Sub_Cat_Name LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Type_Name LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR" & vbLf
            Filter &= " Running_Name LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR" & vbLf
            Filter &= " Cat_No + Sub_Cat_No+Type_No+Running_No LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' " & vbLf
            Filter &= ") AND "
        End If
        If txt_Search_Sup.Text <> "" Then
            Filter &= " (" & vbLf
            Filter &= " S_Tax_No LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Name LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Alias LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Address LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Replace(Replace(S_Phone,'-',''),',','') LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Replace(Replace(S_Fax,'-',''),',','') LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Contact_Name LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Email LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' " & vbLf
            Filter &= ") AND "
        End If
        If txt_Search_Code.Text <> "" Then
            Filter &= " (dbo.udf_AVLCode(AVL_Y,AVL_M,AVL_Dept_ID,AVL_Sub_Dept_ID,AVL_No) LIKE '%" & txt_Search_Code.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Ref_Code LIKE '%" & txt_Search_Code.Text.Replace("'", "''") & "%') AND " & vbLf
        End If
        If ddl_Search_Step.SelectedIndex > 0 Then
            Filter &= "Current_Step=" & ddl_Search_Step.SelectedValue & " AND "
        End If
        If Filter <> "" Then
            SQL &= "WHERE " & Filter.Substring(0, Filter.Length - 5) & vbLf
        End If

        Return SQL
    End Function

    Private Function GetSQL_Last() As String
        Dim SQL As String = ""
        SQL &= " SELECT Ass_ID,Ass_Ref_Code,ReAss_ID,ReAss_Ref_Code,Item_No,Item_Name,Item_Code,Dept_ID,Dept_Name" & vbLf
        SQL &= " ,S_ID,S_Name,S_Tax_No,S_Alias,AVL_Status,Get_Score,Pass_Score,Max_Score,Current_Step,Step_TH,Update_Time,Sub_Dept_ID,Sub_Dept_Name,Pass_Status" & vbLf

        SQL &= " ,Reass_AVL_Status"
        SQL &= " ,CASE WHEN ReAss_ID IS NOT NULL AND Reass_AVL_Status=0 THEN '' ELSE AVL_Code END AVL_Code"
        SQL &= " FROM" & vbLf
        SQL &= " (SELECT " & vbLf
        SQL &= " RANK() OVER(PARTITION BY Header.Ass_ID ORDER BY dbo.udf_ReAssCode(ReAss.Ref_Year,ReAss.Ref_Month,Header.Dept_ID,ReAss.Ref_Number) DESC) LastAss," & vbLf
        SQL &= " Header.Ass_ID,dbo.udf_AssCode(Header.Ref_Year,Header.Ref_Month,Header.Dept_ID,Header.Ref_Number) Ass_Ref_Code," & vbLf
        SQL &= " ReAss.ReAss_ID,CASE WHEN ReAss.Ref_Year IS NULL THEN NULL ELSE dbo.udf_ReAssCode(ReAss.Ref_Year,ReAss.Ref_Month,Header.Dept_ID,ReAss.Ref_Number) END ReAss_Ref_Code," & vbLf
        SQL &= " tb_Cat.Cat_No + tb_Sub_Cat.Sub_Cat_No + tb_Type.Type_No+tb_Running.Running_No Item_Code,Header.Item_No,Header.Item_Name,Header.DEPT_ID,Header.Dept_Name," & vbLf
        SQL &= " S_ID,S_Tax_No,Header.S_Name,S_Alias,S_Address,S_Phone,S_Fax,S_Contact_Name,S_Email," & vbLf
        SQL &= " ISNULL(ReAss.Get_Score,Header.Get_Score) Get_Score,ISNULL(ReAss.Pass_Score,Header.Pass_Score) Pass_Score,ISNULL(ReAss.Max_Score,Header.Max_Score) Max_Score,ISNULL(ISNULL(ReAss.Pass_Status,Header.Pass_Status),1) Pass_Status," & vbLf
        SQL &= " dbo.udf_AVLCode(Header.AVL_Y,Header.AVL_M,Header.AVL_Dept_ID,Header.AVL_Sub_Dept_ID,Header.AVL_No) AVL_Code,ISNULL(ReAss.AVL_Status,Header.AVL_Status) AVL_Status,ISNULL(ReAss.Current_Step,Header.Current_Step) Current_Step," & vbLf

        SQL &= " ISNULL(ReAss.AVL_Status,0) Reass_AVL_Status"

        SQL &= " ,Step_TH,ISNULL(ReAss.Update_Time,Header.Update_Time) Update_Time,Header.Sub_Dept_ID,Header.Sub_Dept_Name " & vbLf
        SQL &= " " & vbLf
        SQL &= " FROM tb_Ass_Header Header" & vbLf
        SQL &= " LEFT JOIN tb_ReAss_Header ReAss ON Header.Ass_ID=ReAss.Ass_ID " & vbLf
        SQL &= " LEFT JOIN tb_Running ON Header.Item_Type=tb_Running.Running_ID AND Header.Dept_ID=tb_Running.Dept_ID" & vbLf

        SQL &= " LEFT JOIN tb_Type ON tb_Running.Type_ID=tb_Type.Type_ID AND Header.Dept_ID=tb_Type.Dept_ID" & vbLf
        SQL &= " LEFT JOIN tb_Sub_Cat ON tb_Type.Sub_Cat_ID=tb_Sub_Cat.Sub_Cat_ID AND tb_Type.Dept_ID=tb_Sub_Cat.Dept_ID" & vbLf
        SQL &= " LEFT JOIN tb_Cat ON tb_Sub_Cat.Cat_ID=tb_Cat.Cat_ID AND tb_Sub_Cat.Dept_ID=tb_Cat.Dept_ID" & vbLf
        SQL &= " LEFT JOIN tb_Step Step ON ISNULL(ReAss.Current_Step,Header.Current_Step)=Step.Step_ID" & vbLf
        SQL &= " WHERE Header.Current_Step=5" & vbLf
        SQL &= " ) A WHERE LastAss=1" & vbLf

        If ddl_Search_Dept.SelectedIndex > 0 Then
            SQL &= " AND Dept_ID='" & ddl_Search_Dept.SelectedValue & "' " & vbLf
        Else
            SQL &= " AND Dept_ID IN (" & vbLf
            SQL &= " SELECT D.Dept_ID FROM tb_Dept_Role R" & vbLf
            SQL &= " INNER JOIN tb_User U ON R.User_ID=U.User_ID AND U.Active_Status=1" & vbLf
            SQL &= " INNER JOIN tb_Dept D ON R.Dept_ID=D.Dept_ID AND D.Active_Status=1" & vbLf
            SQL &= " INNER JOIN tb_Ass_Role AR ON R.AR_ID=AR.AR_ID" & vbLf
            SQL &= " WHERE R.User_ID=" & User_ID & vbLf
            SQL &= " ) " & vbLf
        End If
        If ddl_Search_SUB_DEPT.SelectedIndex > 0 Then
            SQL &= " AND Sub_Dept_ID ='" & ddl_Search_SUB_DEPT.SelectedValue & "'    "
        End If
        If txt_Search_Item.Text <> "" Then
            SQL &= " AND (" & vbLf
            SQL &= " Item_Code LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR " & vbLf
            SQL &= " Item_No LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR " & vbLf
            SQL &= " Item_Name LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR " & vbLf
            SQL &= " CAT_Name LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR " & vbLf
            SQL &= " Sub_Cat_Name LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR " & vbLf
            SQL &= " Type_Name LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR " & vbLf
            SQL &= " Running_Name LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' " & vbLf
            SQL &= ") " & vbLf
        End If
        If txt_Search_Code.Text <> "" Then
            SQL &= " AND (AVL_Code LIKE '%" & txt_Search_Code.Text.Replace("'", "''") & "%' OR " & vbLf
            SQL &= " Ass_Ref_Code LIKE '%" & txt_Search_Code.Text.Replace("'", "''") & "%' OR " & vbLf
            SQL &= " ReAss_Ref_Code LIKE '%" & txt_Search_Code.Text.Replace("'", "''") & "%' " & vbLf
            SQL &= " ) " & vbLf
        End If
        If txt_Search_Sup.Text <> "" Then
            SQL &= " AND (" & vbLf
            SQL &= " S_Tax_No LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            SQL &= " S_Name LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            SQL &= " S_Alias LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            SQL &= " S_Address LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            SQL &= " Replace(Replace(S_Phone,'-',''),',','') LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            SQL &= " Replace(Replace(S_Fax,'-',''),',','') LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            SQL &= " S_Contact_Name LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            SQL &= " S_Email LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' " & vbLf
            SQL &= ") " & vbLf
        End If
        If ddl_Search_Step.SelectedIndex > 0 Then
            SQL &= " AND Current_Step=" & ddl_Search_Step.SelectedValue & " "
        End If
        'SQL &= " ORDER BY Ass_Ref_Code" & vbLf

        Return SQL
    End Function

    Private Sub BindList()
        Dim StartMonth As Integer = (ddlStart_Y.SelectedValue * 12) + ddlStart_M.SelectedValue
        Dim EndMonth As Integer = (ddlEnd_Y.SelectedValue * 12) + ddlEnd_M.SelectedValue
        Dim SQL As String = "DECLARE @S AS INT=" & StartMonth & vbLf
        SQL &= "DECLARE @E AS INT=" & EndMonth & vbLf & vbLf
        Select Case ddl_Search_Type.SelectedIndex
            Case 0 'ทั้งหมด
                SQL &= "SELECT * FROM " & vbLf
                SQL &= "(" & vbLf
                SQL &= GetSQL_Ass() & vbLf
                SQL &= "UNION ALL" & vbLf
                SQL &= GetSQL_ReAss() & vbLf
                SQL &= ") A " & vbLf
                'SQL &= "ORDER BY Ass_Ref_Code"
            Case 1 'เฉพาะการประเมินผู้ขายใหม่
                SQL &= "SELECT * FROM " & vbLf
                SQL &= "(" & vbLf
                SQL &= GetSQL_Ass() & vbLf
                SQL &= ") A " & vbLf
                'SQL &= "ORDER BY Ass_Ref_Code"
            Case 2 'เฉพาะการประเมินทบทวน
                SQL &= "SELECT * FROM " & vbLf
                SQL &= "(" & vbLf
                SQL &= GetSQL_ReAss() & vbLf
                SQL &= ") A " & vbLf
                'SQL &= "ORDER BY Ass_Ref_Code"
            Case 3 'เฉพาะการประเมินล่าสุด
                SQL &= "SELECT * FROM " & vbLf
                SQL &= "(" & vbLf
                SQL &= GetSQL_Last() & vbLf
                SQL &= ") A " & vbLf
                'SQL &= "ORDER BY Ass_Ref_Code"
        End Select
        Dim Header As String = ""
        Dim Title As String = ""
        Select Case ddl_Search_Type.SelectedIndex
            Case 0
                Header &= "ทั้งหมด"
            Case 1
                Header &= "ผู้ขายใหม่"
            Case 2
                Header &= "ทบทวน"
            Case 3
                Header &= "ล่าสุด"
        End Select
        If ddl_Search_Dept.SelectedIndex > 0 Then
            Title &= " " & ddl_Search_Dept.Items(ddl_Search_Dept.SelectedIndex).Text
        End If
        If ddl_Search_SUB_DEPT.SelectedIndex > 0 Then
            Title &= " ของฝ่าย/ภาค/งาน " & ddl_Search_SUB_DEPT.Items(ddl_Search_SUB_DEPT.SelectedIndex).Text
        End If
        If txt_Search_Item.Text <> "" Then
            Title &= " พัสดุ/ครุภัณฑ์ : " & txt_Search_Item.Text
        End If
        If txt_Search_Sup.Text <> "" Then
            Title &= " ผู้ขาย : " & txt_Search_Sup.Text
        End If
        If txt_Search_Code.Text <> "" Then
            Title &= " เลขเอกสาร : " & ddl_Search_Step.Items(ddl_Search_Step.SelectedIndex).Text
        End If
        If ddl_Search_Step.SelectedIndex > 0 Then
            Title &= " ขั้นตอน" & ddl_Search_Step.Items(ddl_Search_Step.SelectedIndex).Text
        End If
        Title &= " "

        Dim Filter As String = " dbo.UDF_IsRangeIntersected(@S,@E,Month(Update_Time)+(Year(Update_Time)*12),Month(Update_Time)+(Year(Update_Time)*12))=1  AND" & vbLf
        Title &= " รายการประเมินในช่วง " & ddlStart_M.Items(ddlStart_M.SelectedIndex).Text & " " & ddlStart_Y.Items(ddlStart_Y.SelectedIndex).Text
        Title &= " ถึง "
        Title &= ddlStart_M.Items(ddlEnd_M.SelectedIndex).Text & " " & ddlEnd_Y.Items(ddlEnd_Y.SelectedIndex).Text & " "


        If Filter <> "" Then
            SQL &= "WHERE " & Filter.Substring(0, Filter.Length - 4) & vbLf
        End If

        SQL &= " ORDER BY Dept_ID,Sub_Dept_ID,Ass_Ref_Code"


        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        DA.Fill(DT)

        Session("Report_Assessment_View") = DT
        Session("Report_Assessment_View_Header") = Header
        Session("Report_Assessment_View_Title") = Title

        lblTotalRecord.Text = Title
        If DT.Rows.Count = 0 Then
            lblTotalRecord.Text &= " ไม่พบรายการดังกล่าว"
        Else
            lblTotalRecord.Text &= " พบ " & FormatNumber(DT.Rows.Count, 0) & " ใบประเมิน"
        End If

        LastDept = ""
        '------------- Binding To List ---------------
        Pager.SesssionSourceName = "Report_Assessment_View"
        Pager.RenderLayout()

    End Sub

    Dim LastDept As String = ""
    Protected Sub rptList_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim trDept As HtmlTableRow = e.Item.FindControl("trDept")
        Dim lblDept As Label = e.Item.FindControl("lblDept")
        Dim lblDocType As Label = e.Item.FindControl("lblDocType")
        Dim lnkRef As HtmlAnchor = e.Item.FindControl("lnkRef")
        Dim lblSup As Label = e.Item.FindControl("lblSup")
        'Dim lblTax As Label = e.Item.FindControl("lblTax")
        Dim lblItem As Label = e.Item.FindControl("lblItem")
        Dim lblStep As Label = e.Item.FindControl("lblStep")
        Dim lblSub_Dept_Name As Label = e.Item.FindControl("lblSub_Dept_Name")

        Dim lblUpdate As Label = e.Item.FindControl("lblUpdate")
        Dim lblResult As Label = e.Item.FindControl("lblResult")
        Dim lblResultText As Label = e.Item.FindControl("lblResultText")
        Dim lblAVL As Label = e.Item.FindControl("lblAVL")

        If LastDept <> e.Item.DataItem("Dept_Name").ToString Then
            LastDept = e.Item.DataItem("Dept_Name").ToString
            lblDept.Text = LastDept
            trDept.Visible = True
        Else
            trDept.Visible = False
        End If
        lblDept.Attributes("Dept_ID") = e.Item.DataItem("Dept_ID")


        ' แสดง lbl สถานะของการประเมิน  เสร็จแล้ว , ยังไม่เสร็จ
        If (e.Item.DataItem("Current_Step").ToString() = 5) Then
            lblResultText.Text = "(เสร็จแล้ว)"
            lblResultText.Style("color") = "green"
        Else
            lblResultText.Text = "(ยังไม่เสร็จ)"
            lblResultText.Style("color") = "red"
        End If

        If IsDBNull(e.Item.DataItem("ReAss_ID")) Then
            lblDocType.Text = "ประเมินผู้ขายใหม่"
            lblDocType.ForeColor = Drawing.Color.Teal
            lnkRef.InnerHtml = e.Item.DataItem("Ass_Ref_Code").ToString
            lnkRef.Style("color") = "Teal"
            lnkRef.HRef = "Assessment_Edit.aspx?Ass_ID=" & e.Item.DataItem("Ass_ID") & "&From=Ass"
        Else
            lblDocType.Text = "ประเมินทบทวน"
            lblDocType.ForeColor = Drawing.Color.Blue
            lnkRef.InnerHtml = e.Item.DataItem("ReAss_Ref_Code").ToString
            lnkRef.Style("color") = "Blue"
            lnkRef.HRef = "Reassessment_Edit.aspx?Ass_ID=" & e.Item.DataItem("Ass_ID") & "&ReAss_ID=" & e.Item.DataItem("ReAss_ID") & "&From=ReAss"
        End If
        lblSup.Text = e.Item.DataItem("S_Name").ToString
        'lblTax.Text = e.Item.DataItem("S_Tax_No").ToString
        lblItem.Text = "(" & e.Item.DataItem("Item_Code").ToString & ") " & e.Item.DataItem("Item_Name").ToString
        lblStep.Text = e.Item.DataItem("Step_TH").ToString
        lblStep.ForeColor = BL.GetStepColor(e.Item.DataItem("Current_Step"))
        lblSub_Dept_Name.Text = e.Item.DataItem("Sub_Dept_Name").ToString()

        lblUpdate.Text = GL.ReportThaiDate(e.Item.DataItem("Update_Time"))
        BL.DisplayAssessmentGridResultLabel(e.Item.DataItem("Get_Score"), e.Item.DataItem("Pass_Score"), e.Item.DataItem("Max_Score"), e.Item.DataItem("Pass_Status"), lblResult)
        lblAVL.Text = e.Item.DataItem("AVL_Code").ToString
    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

#Region "PrintButton"

    Protected Sub btnPDF_Click(sender As Object, e As System.EventArgs) Handles btnPDF.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/AssessmentView.aspx?Mode=PDF');", True)
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/AssessmentView.aspx?Mode=EXCEL');", True)
    End Sub

#End Region
    Protected Sub ddl_Search_Dept_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddl_Search_Dept.SelectedIndexChanged

        BL.BindDDlSubDept_Search(ddl_Search_SUB_DEPT, ddl_Search_Dept.SelectedValue)
        ddl_Search_SUB_DEPT.SelectedIndex = 0
    End Sub

End Class
