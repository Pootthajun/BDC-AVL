﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Reassessment_List.aspx.vb" Inherits="Reassessment_List" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<link href="assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>

<style type="text/css">
    .modal
    {
        width:70%;
        left:35%;
        }
     
     .lblDialog
     {
        margin-top:6px;
        font-size:14px;
         }
     .captionDialog
     {
         color:#888;
         }
</style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>
    <div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							การประเมินทบทวน
						</h3>		
						<ul class="breadcrumb">                            
                            <li>
                            	<i class="icon-ok-sign"></i><a href="javascript:;">การประเมิน</a><i class="icon-angle-right"></i>
                            </li>
                        	<li><i class="icon-ok-sign"></i> <a href="javascript:;">การประเมินทบทวน</a></li>
                        	
                            <uc1:wuc_datereporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->	
				     </div>				
			    </div>

                <asp:UpdatePanel ID="udpSearch" runat="server">
                <ContentTemplate>    

                <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet  ">							
							<div class="portlet-body">
                            <div class="table-toolbar">

  									    <div class="btn-group pull-right">  
                                        <asp:LinkButton  data-toggle="dropdown" class="btn dropdown-toggle" id="Button2" runat="server">
										    พิมพ์ <i class="icon-angle-down"></i>
										    </asp:LinkButton>
                                            <ul class="dropdown-menu pull-right">
											    <li><asp:LinkButton ID="btnPDF" runat="server">รูปแบบ PDF</asp:LinkButton></li>
											    <li><asp:LinkButton ID="btnExcel" runat="server">รูปแบบ Excel</asp:LinkButton></li>											
										    </ul>    
									    </div>
                                         <div class="btn-group">
                                            <asp:LinkButton CssClass="btn blue" id="btnSearch" runat="server">
										        ค้นหา <i class="icon-search"></i>
										        </asp:LinkButton>    
									    </div>
							</div>

                         
                            </div>
                                        <%--แบ่งส่วนการค้นหา--%>
                                        <fieldset>
                                            <legend><b>ค้นหา</b></legend>
                                        </fieldset>
                                    <div class="row-fluid form-horizontal">
									    <div class="span4 ">
									        <div class="control-group">
										        <label class="control-label"> สำนักงาน</label>
										        <div class="controls">
                                                        <asp:DropDownList ID="ddl_Search_Dept"  AutoPostBack="true" runat="server" class="medium m-wrap" ></asp:DropDownList>											       
										        </div>
									        </div>
                                        </div>
                                        <div class="span6 ">
									        <div class="control-group">
										        <label class="control-label"> ฝ่าย/ภาค/งาน</label>
										        <div class="controls">
                                                        <asp:DropDownList ID="ddl_Search_SUB_DEPT" runat="server" class="large m-wrap" ></asp:DropDownList>											       
										        </div>
									        </div>
                                        </div>
                                    </div>
                                    <div class="row-fluid form-horizontal">
									    <div class="span4 ">
									        <div class="control-group">
											    <label class="control-label"> พัสดุ/ครุภัณฑ์<br />งานจ้าง/บริการ</label>
											    <div class="controls">
												    <asp:TextBox ID="txt_Search_Item" runat="server" class="m-wrap medium"
                                                    placeholder="ชื่อ พัสดุ/ครุภัณฑ์ / งานจ้างและบริการ"></asp:TextBox>
                                   		        </div>
										    </div>
                                        </div>
                                        <div class="span6">
									        <div class="control-group">
										        <label class="control-label">  เลข AVL</label>
										        <div class="controls">
											           <asp:TextBox ID="txt_Search_AVL" runat="server" class="m-wrap large" placeholder="เลข AVL"></asp:TextBox>												
										        </div>
									        </div>
                                        </div>
                                    </div>
                                    <div class="row-fluid form-horizontal">
									    <div class="span4 ">
									        <div class="control-group">
										        <label class="control-label">  ผู้ขาย</label>
										        <div class="controls">
												    <asp:TextBox ID="txt_Search_Sup" runat="server" class="m-wrap medium" placeholder="ชื่อผู้ขาย/ผู้ติดต่อ/เลขประจำตัวผู้เสียภาษี" ></asp:TextBox>
											    </div>
									        </div>
                                        </div>
                                        <div class="span8 ">
									        <div class="control-group">
											    <label class="control-label"> เลขใบประเมิน</label>
											    <div class="controls">
											           <asp:TextBox ID="txt_Search_Ref" runat="server" class="m-wrap large" placeholder="เลขใบประเมิน"></asp:TextBox>												
										        </div>
										    </div>
                                        </div>
                                    </div>
                                    <div class="row-fluid form-horizontal">
									    <div class="span4 ">
									        <div class="control-group">
										        <label class="control-label">  ขั้นตอน</label>
										        <div class="controls">											          
                                                    <asp:DropDownList ID="ddl_Search_Step" runat="server" class="m-wrap medium" >
                                                        <asp:ListItem Value="0" Text="ทั้งหมด"></asp:ListItem>
                                                        <asp:ListItem Value="-1" Text="ยังไม่ได้ทบทวน"></asp:ListItem>
                                                        <asp:ListItem Value="1" Text="เจ้าหน้าที่ประเมิน"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="หัวหน้าฝ่ายตรวจสอบ"></asp:ListItem>
                                                        <asp:ListItem Value="3" Text="เจ้าหน้าที่พัสดุตรวจสอบ"></asp:ListItem>
                                                        <asp:ListItem Value="4" Text="ฝ่ายบริหารตรวจสอบ"></asp:ListItem>
                                                        <asp:ListItem Value="5" Text="ประเมินเสร็จสมบูรณ์"></asp:ListItem>
                                                    </asp:DropDownList>	                                                
                                                </div>
									        </div>
                                        </div>
                                        <div class="span8 ">
									        <div class="control-group">
											    <label class="control-label"> ประเมินล่าสุดเมื่อ</label>
											    <div class="controls">
												    <asp:DropDownList ID="ddlLastUpdate" runat="server" class="large m-wrap" >
                                                        <asp:ListItem Text="ทั้งหมด"></asp:ListItem>
                                                        <asp:ListItem Text="ภายใน 1 ปี"></asp:ListItem>
                                                        <asp:ListItem Text="1-2 ปี"></asp:ListItem>
                                                        <asp:ListItem Text="ไม่เกิน 2 ปี"></asp:ListItem>
                                                        <asp:ListItem Selected="True"  Text="2 ปีขึ้นไป"></asp:ListItem>
                                                    </asp:DropDownList>											        
											    </div>
										    </div>
                                        </div>
                                    </div>
                                    <div class="row-fluid form-horizontal">
                                    <div class="span4 ">
                                        <div class="control-group">
                                            <label class="control-label">
                                                ผลการประเมิน</label>
                                            <div class="controls">
												    <asp:DropDownList ID="ddlAssResult" runat="server" class="medium m-wrap" >
                                                        <asp:ListItem Value="0" Text="ทั้งหมด"></asp:ListItem>
                                                        <asp:ListItem Value="1" Text="ผ่าน"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="ไม่ผ่าน"></asp:ListItem>
                                                    </asp:DropDownList>											        
											    </div>
                                        </div>
                                    </div>
                                    <div class="span8 ">
									        <div class="control-group">
											    <label class="control-label"> 
                                                    ซ่อนหมายเหตุ</label>
											    <div class="controls">
                                                <asp:CheckBox ID="ckHideRemark" runat="server" CssClass="large m-wrap" 
                                                    ToolTip="แสดงความคิดเห็นทั้งหมดในตาราง" />
                                            </div>
										    </div>
                                        </div>
                                </div>
                                    <div class="row-fluid form-horizontal">
									<div class="span12 ">
										<div class="control-group">
											<label class="control-label"> ช่วงที่ ประเมิน</label>
											<div class="controls">
												<asp:DropDownList ID="ddlStart_M" runat="server" AutoPostBack="true" CssClass="m-wrap" Width="70px">				                                    
			                                    </asp:DropDownList>
                                                <asp:DropDownList ID="ddlStart_Y" runat="server" AutoPostBack="true" CssClass="m-wrap" Width="100px">				                                    
			                                    </asp:DropDownList>
                                                <span style="font-size: 14px; padding-left:30px; padding-right:30px;">ถึง</span>
                                                <asp:DropDownList ID="ddlEnd_M" runat="server" AutoPostBack="true" CssClass="m-wrap"  Width="70px">				                                    
			                                    </asp:DropDownList>
                                                <asp:DropDownList ID="ddlEnd_Y" runat="server" AutoPostBack="true" CssClass="m-wrap" Width="100px">				                                    
			                                    </asp:DropDownList>
											</div>
                                            
										</div>
									</div>									
								</div>
						            
                                    <div role="grid" class="dataTables_wrapper form-inline">
                                         <div class="portlet-body no-more-tables">                                        
                                            <span style="font-weight:bold; font-size:14px;">การประเมินทบทวน</span> <asp:Label ID="lblTotalRecord" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								                <table class="table table-full-width table-advance dataTable no-more-tables table-hover">                                
									                <thead>
										                <tr>											            
											                <th style="text-align:center; vertical-align:middle;">ใบประเมินหลัก</th>
                                                            <th style="text-align:center; vertical-align:middle;">ใบประเมินทบทวน</th>
                                                            <th style="text-align:center; vertical-align:middle;">เลข AVL</th>
                                                            <th style="text-align:center; vertical-align:middle;">ผู้ขาย</th>
											                <th style="text-align:center; vertical-align:middle;">ชื่อพัสดุ/ครุภัณฑ์ <br />งานจ้าง/บริการ</th>
                                                            <th style="text-align:center; vertical-align:middle; width: 80px;">ขั้นตอน</th>
                                                            <th style="text-align: center; vertical-align: middle;">ผู้ประเมิน</th>
                                                            <th style="text-align:center; vertical-align:middle;">ฝ่าย/ภาค/งาน</th>
											                <th style="text-align:center; vertical-align:middle; width:100px;">ประเมินล่าสุดเมื่อ</th>
                                                            <th style="text-align:center; vertical-align:middle; width: 80px;">ผลการประเมิน</th>

                                                            <th style="text-align:center; vertical-align:middle; width:100px;">ดำเนินการ</th>
										                </tr>
									                </thead>
									                <tbody>
                                                        <asp:Repeater ID="rptList" runat="server">
                                                        <ItemTemplate>
                                                            <tr id="trDept" runat="server">
                                                                <td data-title="ฝ่าย/ภาค/งาน" colspan="11" style="text-align:center"><asp:Label ID="lblDept"  runat="server" Font-Size="14px" Font-Bold="true"></asp:Label></td>
                                                            </tr>
                                                            <tr>
											                    <td data-title="เลขใบประเมินหลัก" style="text-align:center;"><asp:Label ID="lblAssNo" runat="server"></asp:Label></td>
                                                                <td data-title="ใบประเมินทบทวน" style="text-align:center;"><asp:Label ID="lblRef" runat="server" Font-Bold="True"></asp:Label></td>
                                                                <td data-title="เลข AVL" style="text-align:center;"><asp:Label ID="lblAVL" runat="server" Font-Bold="True"></asp:Label></td>
                                                                <td data-title="ผู้ขาย"><asp:Label ID="lblSup" runat="server"></asp:Label></td>
											                    <td data-title="พัสดุ/ครุภัณฑ์" style="text-align:left; "><asp:Label ID="lblItem" runat="server"></asp:Label></td>
                                                                <td data-title="ขั้นตอน" style="text-align:center;"><asp:Panel CssClass="inline" ID ="pnl_icon" runat ="server" Visible ="false" ><i id="icon_undo" runat="server" class="icon-undo" style =" color : Silver   ;"></i></asp:Panel> <asp:Label ID="lblStep" runat="server"></asp:Label></td>
                                                                <td data-title="ผู้ประเมิน">
                                                                    <asp:Label ID="lblAssessor" runat="server"></asp:Label>
                                                                </td>
                                                                <td data-title="ฝ่าย/ภาค/งาน"><asp:Label ID="lblSub_Dept_Name" runat="server"></asp:Label></td>
                                                                <td data-title="ประเมินล่าสุดเมื่อ" style="text-align:center;"><asp:Label ID="lblUpdate" runat="server"></asp:Label><asp:Image ImageUrl="images/alert.gif" ID="imgAlert" runat="server"/></td>
                                                                <td data-title="ผลการประเมิน" style="text-align:center;"><asp:Label ID="lblResult" runat="server"></asp:Label><br /><asp:Label ID="lblResultText" runat="server"></asp:Label></td>
											                    <td data-title="ดำเนินการ" style="text-align:center;"><asp:LinkButton CssClass="btn mini purple" ID="btnView" runat="server" CommandName="View"><i class="icon-search"></i> การประเมิน</asp:LinkButton></td>											
										                    </tr>
                                                            <tr id="trRemark" runat="server">
											                    <td data-title="เลขใบประเมิน" style="text-align:center;border-top:white;"></td>
                                                                <td data-title="เลข AVL" style="text-align:center;border-top:white;"><span  style =" color : Silver   ;" >หมายเหตุ <i class="icon-comments"></i></span></td>
											                    <td data-title="หมายเหตุ" colspan ="8">
                                                                    <span >
                                                                        <div class="span3"><asp:Label ID="lblRole_1_Comment" style =" color : Silver   ;" runat="server"></asp:Label></div>
                                                                        <div class="span3"><asp:Label ID="lblRole_2_Comment" style =" color : Silver   ;" runat="server"></asp:Label></div>
                                                                        <div class="span3"><asp:Label ID="lblRole_3_Comment" style =" color : Silver   ;" runat="server"></asp:Label></div>
                                                                        <div class="span3"><asp:Label ID="lblRole_4_Comment" style =" color : Silver   ;" runat="server"></asp:Label></div>
                                                                    </span>
                                                                </td>										
										                    </tr>


                                                        </ItemTemplate>
                                                        </asp:Repeater>
										            
									                </tbody>
								                </table>

                                            <uc2:PageNavigation ID="Pager" runat="server" />
                                        </div> 
                                    </div>
							    
                            </div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>    
               </ContentTemplate>
               </asp:UpdatePanel>

               <asp:UpdatePanel ID="udpDialogNew" runat="server">
                    <ContentTemplate>
                         <asp:Panel CssClass="modal-scrollable" id="dialogAssessmentHistory" runat="server">
                            <div class="modal fade in modal-overflow" style="display: block; margin-top: 30px; ">
				                <div class="modal-header">
					                <h3>การประเมินจากใบประเมินเลขที่ <asp:Label ID="lblAssNo" runat="server"></asp:Label></h3>
				                </div>
				                <div class="modal-body">
                                        <div class="row-fluid form-horizontal">
												<div class="control-group span5">
									                <label class="control-label captionDialog"> สำนักงาน : </label>
									                <div class="controls lblDialog">
                                                       <asp:Label ID="lblDept" runat="server" Font-Bold="True">xxxxxxxxxxxxxxxxxxxx</asp:Label>	       
									                </div>
								                </div>
                                                <div class="control-group span7">
									                <label class="control-label captionDialog"> ผู้ขาย : </label>
									                <div class="controls lblDialog">
                                                       <asp:Label ID="lblSupplier" runat="server" Font-Bold="True">xxxxxxxxxxxxxxxxxxxx</asp:Label>	       
									                </div>
								                </div>                              
                                        </div>
                                        <div class="row-fluid form-horizontal">							
								                <div class="control-group span12">
									                <label class="control-label captionDialog"> พัสดุ/ครุภัณฑ์ : </label>
									                <div class="controls lblDialog">
                                                       <asp:Label ID="lblItem" runat="server">xxxxxxxxxxxxxxxxxxxx</asp:Label>	       
									                </div>
								                </div>                                                                  
                                        </div>
                                        <div class="row-fluid form-horizontal">							
								                <div class="control-group span5">
									                <label class="control-label captionDialog"> วันที่ประเมินล่าสุด : </label>
									                <div class="controls lblDialog">
                                                       <asp:Label ID="lblLastAss" runat="server" Font-Bold="True">xxxxxxxxxxxxxxxxxxxx</asp:Label>	       
									                </div>
								                </div>
                                                <div class="control-group span7">
									                <label class="control-label captionDialog"> ผลการประเมิน : </label>
									                <div class="controls lblDialog">
                                                       <asp:Label ID="lblLastResult" runat="server" Font-Bold="True">xxxxxxxxxxxxxxxxxxxx</asp:Label>	       
									                </div>
								                </div>
                                        </div>
				                <h4>ประวัติการประเมิน <asp:Label ID="lblTotolHistory" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label></h4>
                                    <table class="table table-full-width table-advance dataTable no-more-tables table-hover">                                
									    <thead>
										    <tr>											            
											    <th style="text-align:center; vertical-align:middle;">การประเมิน</th>
                                                <th style="text-align:center; vertical-align:middle;">เลขที่</th>
                                                <th style="text-align:center; vertical-align:middle;">เลข AVL</th>                                                                                                            
                                                <th style="text-align:center; vertical-align:middle; width: 100px;">ประเมินเมื่อ</th>
                                                <th style="text-align:center; vertical-align:middle; width: 80px;">ผลการประเมิน</th>
                                                <th style="text-align:center; vertical-align:middle; width: 80px;">ขั้นตอน</th>
                                                <th style="text-align:center; vertical-align:middle;">ผู้ประเมิน</th>
											    <th style="text-align:center; vertical-align:middle; width:100px;">ดำเนินการ</th>
										    </tr>
									    </thead>
									    <tbody>
                                            <asp:Repeater ID="rptHistory" runat="server">
                                            <ItemTemplate>
                                                <tr>
											        <td data-title="การประเมิน" style="text-align:center;"><asp:Label ID="lblAssType" runat="server"></asp:Label></td>
                                                    <td data-title="เลขที่" style="text-align:center;"><asp:Label ID="lblRefCode" runat="server" Font-Bold="True"></asp:Label></td>
                                                    <td data-title="เลข AVL" style="text-align:center;"><asp:Label ID="lblAVL" runat="server" Font-Bold="True"></asp:Label></td>
                                                    <td data-title="ประเมินเมื่อ" style="text-align:center;"><asp:Label ID="lblUpdate" runat="server"></asp:Label></td>
                                                    <td data-title="ผลการประเมิน" style="text-align:center;"><asp:Label ID="lblResult" runat="server"></asp:Label><br /><asp:Label ID="lblResultText" runat="server"></asp:Label></td>
											        <td data-title="ขั้นตอน" style="text-align:center;"><asp:Panel CssClass="inline" ID ="pnl_icon" runat ="server" Visible ="false" ><i id="icon_undo" runat="server" class="icon-undo" style =" color : Silver   ;"></i></asp:Panel><asp:Label ID="lblStep" runat="server"></asp:Label></td>                                                    
											        <td data-title="ผู้ประเมิน"><asp:Label ID="lblAssessor" runat="server"></asp:Label></td>											                    
                                                    <td data-title="ดำเนินการ" style="text-align:center;"><asp:LinkButton CssClass="btn mini purple" ID="btnView" runat="server" CommandName="View"><i class="icon-search"></i> การประเมิน</asp:LinkButton></td>											
										        </tr>
                                            </ItemTemplate>
                                            </asp:Repeater>										            
									    </tbody>
								    </table>                                
                                </div>
                                                              
				                <div class="modal-footer" id="pnlCreate" runat="server">                        
                                        <asp:Button CssClass="btn red" ID="btnCreateOK" runat="server" Text="สร้างใบประเมินทบทวน"></asp:Button>
                                        <%--<asp:ConfirmButtonExtender ID="cfm_btnCreateOK" runat="server" ConfirmText="ยืนยันสร้างใบประเมินทบทวนใหม่" BehaviorID="btnCreateOK"></asp:ConfirmButtonExtender> --%>                            
				                </div>
                                <asp:LinkButton CssClass="fancybox-item fancybox-close" ToolTip="ปิด" id="lnkCloseDialogHistory" runat="server"></asp:LinkButton>
			                </div>
                            <div class="modal-backdrop" style="z-index: 10049;"></div>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>

 </div>
</asp:Content>


