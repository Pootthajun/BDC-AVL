﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class Reassessment_Edit
    Inherits System.Web.UI.Page

    Dim BL As New AVLBL
    Dim GL As New GenericLib

    Public Property FromPage As AVLBL.AssessmentType
        Get
            Try
                Return ViewState("FromPage")
            Catch ex As Exception
                Return AVLBL.AssessmentType.Assessment
            End Try
        End Get
        Set(value As AVLBL.AssessmentType)
            ViewState("FromPage") = value
        End Set
    End Property

    Private ReadOnly Property PageName As String
        Get
            Return Me.AppRelativeVirtualPath.Substring(2)
        End Get
    End Property

    Private Property AccessMode As AVLBL.AccessRole
        Get
            Try
                Return ViewState("AccessMode")
            Catch ex As Exception
                Return AVLBL.AccessRole.None
            End Try
        End Get
        Set(value As AVLBL.AccessRole)
            ViewState("AccessMode") = value
        End Set
    End Property

    Private Property Ass_ID As Integer
        Get
            Try
                Return ViewState("Ass_ID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Integer)
            ViewState("Ass_ID") = value
        End Set
    End Property

    Private Property ReAss_ID As Integer
        Get
            Try
                Return ViewState("ReAss_ID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Integer)
            ViewState("ReAss_ID") = value
        End Set
    End Property

    Public Property Current_Step As AVLBL.ReassessmentStep
        Get
            Try
                Return ViewState("Current_Step")
            Catch ex As Exception
                Return AVLBL.ReassessmentStep.Auditor
            End Try
        End Get
        Set(value As AVLBL.ReassessmentStep)
            ViewState("Current_Step") = value
        End Set
    End Property

    Private ReadOnly Property User_ID As Integer
        Get
            Try
                If IsNumeric(Session("User_ID")) Then
                    Return Session("User_ID")
                Else
                    Return -1
                End If
            Catch ex As Exception
                Return -1
            End Try
        End Get
    End Property

    Private Property Cat_Name As String
        Get
            Try
                Return lbl_Cat.Text
            Catch ex As Exception
                Return ""
            End Try
        End Get
        Set(value As String)
            lbl_Cat.Text = value
        End Set
    End Property

    Private Property Sub_Cat_Name As String
        Get
            Try
                Return lbl_Sub_Cat.Text
            Catch ex As Exception
                Return ""
            End Try
        End Get
        Set(value As String)
            lbl_Sub_Cat.Text = value
        End Set
    End Property

    Private Property Type_Name As String
        Get
            Try
                Return lbl_Type.Text
            Catch ex As Exception
                Return ""
            End Try
        End Get
        Set(value As String)
            lbl_Type.Text = value
            pnl_Product.Visible = value <> ""
        End Set
    End Property

    Private Property Type_ID As Integer
        Get
            Try
                Return lbl_Item_Type.Attributes("Type_ID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Integer)
            lbl_Item_Type.Attributes("Type_ID") = value
        End Set
    End Property

    Private Property Item_Type_Name As String
        Get
            Return lbl_Item_Type.Text
        End Get
        Set(value As String)
            lbl_Item_Type.Text = value
        End Set
    End Property

    Private Property Running_Name As String
        Get
            Try
                Return lbl_Running.Text
            Catch ex As Exception
                Return ""
            End Try
        End Get
        Set(value As String)
            lbl_Running.Text = value
            pnl_Product.Visible = value <> ""
        End Set
    End Property

    Public Property Running_ID As Integer
        Get
            Try
                Return lbl_Item_Type.Attributes("Running_ID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Integer)
            lbl_Item_Type.Attributes("Running_ID") = value
        End Set
    End Property

    Public Property File_ID As Integer
        Get
            Try
                Return btnSaveFile.Attributes("File_ID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Integer)
            btnSaveFile.Attributes("File_ID") = value
        End Set
    End Property


    Private ReadOnly Property Dept_ID As String
        Get
            Try
                Return ddl_Dept.Items(ddl_Dept.SelectedIndex).Value
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Private ReadOnly Property Dept_Name As String
        Get
            Try
                Return ddl_Dept.Items(ddl_Dept.SelectedIndex).Text
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property


    Private ReadOnly Property Sub_Dept_ID As String
        Get
            Try
                Return ddl_Sub_Dept.Items(ddl_Sub_Dept.SelectedIndex).Value
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Private ReadOnly Property Sub_Dept_Name As String
        Get
            Try
                Return ddl_Sub_Dept.Items(ddl_Sub_Dept.SelectedIndex).Text
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Private Property Title_Item_Group As String '------------ประเภทใบประเมิน สินค้า  หรือ บริการ--------------
        Get
            Try
                Return lblTitle_Item_Name.Text
            Catch ex As Exception
                Return ""
            End Try
        End Get
        Set(value As String)
            lblTitle_Item_Name.Text = value
        End Set
    End Property

    Public Property Cat_Group_ID As String
        Get
            Try
                Return lblCat_Group_ID.Text
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As String)
            lblCat_Group_ID.Text = value
        End Set
    End Property



    Private Property S_ID As Integer
        Get
            Try
                Return ddl_S_Type.Attributes("S_ID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Integer)
            ddl_S_Type.Attributes("S_ID") = value
        End Set
    End Property

    Private ReadOnly Property S_Type_ID As Integer
        Get
            Try
                Return ddl_S_Type.Items(ddl_S_Type.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
    End Property

    Private Property ReassessmentRole As AVLBL.ReassessmentRole
        Get
            Try
                Return ViewState("ReassRole")
            Catch ex As Exception
                Return AVLBL.ReassessmentRole.None
            End Try
        End Get
        Set(value As AVLBL.ReassessmentRole)
            ViewState("ReassRole") = value
        End Set
    End Property

    Public Property IsPass_Status As Boolean

        Get
            Return ViewState("IsPass_Status")
        End Get
        Set(value As Boolean)
            ViewState("IsPass_Status") = value
        End Set
    End Property

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        If IsNothing(Session("User_ID")) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "ShowAlert('warning','กรุณาล็อกอินเข้าสู่ระบบ','Login.aspx');", True)
            Response.Redirect("Login.aspx")
            Exit Sub
        End If

        If Not IsPostBack Then

            Try
                Select Case Request.QueryString("From")
                    Case "Ass"
                        FromPage = AVLBL.AssessmentType.Assessment
                    Case "ReAss"
                        FromPage = AVLBL.AssessmentType.ReAssessment
                End Select
            Catch ex As Exception
                FromPage = AVLBL.AssessmentType.Assessment
            End Try

            '----------- Check สิทธิ์ในเมนู --------------
            Dim MT As DataTable = BL.GetUserMenuRole(User_ID, PageName)
            If MT.Rows.Count = 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลนี้','Default.aspx');", True)
                Exit Sub
            End If
            AccessMode = MT.Rows(0).Item("MR_ID") '--------- สิทธิ์ในเมนูของผู้ใช้คนปัจจุบัน -----------
            If AccessMode = AVLBL.AccessRole.None Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลนี้','Default.aspx');", True)
                Exit Sub
            End If

            Ass_ID = Request.QueryString("Ass_ID")
            ReAss_ID = Request.QueryString("ReAss_ID")
            Dim DT_Dept As DataTable = BL.GetAssessmentHeader(Ass_ID)

            BindDept()
            BindSubDept()

            '--------สร้างแบบประเมินทบทวน กรณีเข้ามาครั้งแรก-------
            If ReAss_ID = 0 Then
                Dim RT As DataTable = BL.GetUserDeptRole(User_ID)
                If Not BL.CanCreateAssessment(RT, "AR_ID") Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่ได้รับสิทธิ์ให้สร้างแบบประเมิน','Reassessment_List.aspx');", True)
                    Exit Sub
                End If
                '--------- หาสิทธิ์ในการสร้างแบบประเมิน ---------
                SaveHeaderReass()
            End If
            Dim DT As DataTable = BL.GetReassessmentHeader(Ass_ID, ReAss_ID)

            If DT.Rows.Count = 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','ไม่พบแบบประเมินดังกล่าว','Reassessment_List.aspx');", True)
                Exit Sub
            End If
            Current_Step = DT.Rows(0).Item("Current_Step")


            '---------------- Bind All Item--------------------
            BindWorkflow()
            '------ดึงข้อมูลอ้างอิงจากใบประเมินผู้ขายใหม่มาแสดงไม่สามารถแก้ไขได้ ยกเว้นไฟล์แนบ---------
            'BindDept()
            'BindSubDept()

            BindProductType()
            BindProductDetail()
            BindFile()
            BindSupplier()
            BindReassessment()
            BindCommentUser()
            BindButtonRef()
            checkComplete()
            '---------------- Hide Dialog ---------------------
            dialogUploadFile.Visible = False
        End If

    End Sub

    Private Sub BindButtonRef()

        Dim SQL As String = " "
        SQL &= " SELECT DISTINCT Ref_Code" & vbLf
        SQL &= " ,Item_Name,Dept_ID,Dept_Name" & vbLf
        SQL &= " ,dbo.udf_AVLCode(AVL_Y,AVL_M,AVL_Dept_ID,AVL_Sub_Dept_ID,AVL_No) AVL_Code" & vbLf
        SQL &= " ,AVL_Status,Get_Score,Pass_Score,Max_Score,Current_Step,Step_TH,Update_Time" & vbLf
        SQL &= " FROM vw_ReAss_Header " & vbLf
        SQL &= " WHERE ReAss_ID =" & ReAss_ID & vbLf

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        DA.Fill(DT)
        If DT.Rows.Count = 1 Then
            If Not IsDBNull(DT.Rows(0).Item("AVL_Code")) Then
                lblRef.Text = " เลขใบประเมิน : " & DT.Rows(0).Item("Ref_Code").ToString & " | " & " ได้เลข  " & DT.Rows(0).Item("AVL_Code").ToString & "  | "
            Else
                lblRef.Text = " เลขใบประเมิน : " & DT.Rows(0).Item("Ref_Code").ToString & " | " '& " ไม่ผ่านการประเมิน " & "  | "
            End If

            If Not IsDBNull(DT.Rows(0).Item("Max_Score")) And Not IsDBNull(DT.Rows(0).Item("Pass_Score")) Then
                If DT.Rows(0).Item("Get_Score") >= DT.Rows(0).Item("Pass_Score") Then
                    lblResult.Text = " ได้คะแนน <i class='icon-ok-sign'></i>  " & DT.Rows(0).Item("Get_Score") & " / " & DT.Rows(0).Item("Max_Score")
                    lblResult.ForeColor = Drawing.Color.White
                Else
                    lblResult.Text = " ได้คะแนน <i class='icon-remove-sign'></i>  " & DT.Rows(0).Item("Get_Score") & " / " & DT.Rows(0).Item("Max_Score")
                    lblResult.ForeColor = Drawing.Color.White
                End If
            End If

        End If


    End Sub

    Private Sub checkComplete()
        '---------------ข้อ 1 -----------------
        timeLine_1.Attributes("class") = "timeline-icon-green"
        timeLine_2.Attributes("class") = "timeline-icon-green"
        timeLine_3.Attributes("class") = "timeline-icon-green"

        '---------------ข้อ 4 -----------------
        If knob.Text <> 0 Then
            Dim AT As DataTable = ReassessmentData()
            Dim ckAss As String = ""
            If AT.Rows.Count > 0 Then
                For i As Integer = 0 To AT.Rows.Count - 1
                    'If AT.Rows(i).Item("Get_Score") < AT.Rows(i).Item("Pass_Score") Then
                    If AT.Rows(i).Item("Get_Score") <= 1 Then
                        timeLine_4.Attributes("class") = "timeline-icon-red"
                        ckAss = "red"
                        Exit For
                    End If
                Next
                If ckAss = "" Then
                    timeLine_4.Attributes("class") = "timeline-icon-green"
                End If
            End If
        Else
            timeLine_4.Attributes("class") = "timeline-icon-red"
        End If

    End Sub


    Private PageScript As String = ""
    Protected Sub Page_LoadComplete(sender As Object, e As System.EventArgs) Handles Me.LoadComplete
        StoreJQuery()
    End Sub

    Private Sub StoreJQuery()
        PageScript &= jQuerySlider() & vbLf

        PageScript &= jQueryWorkflowButton() & vbLf
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "StoreJQuery", PageScript, True)
    End Sub

#Region "Dept"
    Private Sub BindDept()
        Dim DT As DataTable = BL.GetAssessmentHeader(Ass_ID)
        BL.BindDDlDEPT(ddl_Dept, User_ID, DT.Rows(0).Item("Dept_ID"))
        ddl_Dept.Enabled = False
        lblDept.Text = ddl_Dept.SelectedItem.ToString()

    End Sub
#End Region

#Region "SubDept"
    Private Sub BindSubDept()
        Dim DT As DataTable = BL.GetAssessmentHeader(Ass_ID)
        BL.BindDDlSub_Dept(ddl_Sub_Dept, Dept_ID, User_ID, Current_Step, DT.Rows(0).Item("Sub_Dept_ID").ToString())

        '------------- Set Enabling--------------
        ddl_Sub_Dept.Enabled = False
        lblSubDept.Text = ddl_Sub_Dept.SelectedItem.ToString()

    End Sub
#End Region



#Region "Product Type"
    Private Sub BindProductType()
        Dim DT As DataTable = BL.GetProductTypeByAssessment(Ass_ID)
        If DT.Rows.Count = 0 OrElse IsDBNull(DT.Rows(0).Item("Type_ID")) Then
            Cat_Group_ID = 0
            Cat_Name = ""
            Sub_Cat_Name = ""
            Type_Name = ""
            Type_ID = 0 '---------- Main Query -----------
            Item_Type_Name = ""
            Running_ID = 0
            Running_Name = ""
        Else
            Cat_Group_ID = DT.Rows(0).Item("CG_ID").ToString
            Cat_Name = DT.Rows(0).Item("Cat_Name").ToString
            Sub_Cat_Name = DT.Rows(0).Item("Sub_Cat_Name").ToString
            Type_Name = DT.Rows(0).Item("Type_Name").ToString
            Type_ID = DT.Rows(0).Item("Type_ID") '---------- Main Query -----------
            Item_Type_Name = DT.Rows(0).Item("Cat_No").ToString & DT.Rows(0).Item("Sub_Cat_No").ToString & DT.Rows(0).Item("Type_No").ToString & DT.DefaultView(0).Item("Running_No").ToString & " : " & DT.DefaultView(0).Item("Running_Name").ToString
            txt_Item_Name.Text = DT.Rows(0).Item("Item_Name").ToString

            Running_ID = DT.Rows(0).Item("Running_ID").ToString
            Running_Name = DT.Rows(0).Item("Running_Name").ToString
        End If

        '--------------- Get Item Type Name -------------
        DT = BL.GetAssessmentHeader(Ass_ID)

        Dim CatGroup_Info As AVLBL.CatGroup_Info = BL.GetCatGroup_Info(Cat_Group_ID)
        lblTitle_Item_Name.Text = CatGroup_Info.CG_Name
        pnlItem_No.Visible = CatGroup_Info.Active_ALL
        Select Case Cat_Group_ID
            Case 0
                lblHeader_Item.Text = "สินค้า, พัสดุครุภัณฑ์ หรือ งานบริการจัดจ้าง"

            Case 2
                lblHeader_Item.Text = "งานบริการจัดจ้าง"

            Case Else
                lblHeader_Item.Text = "สินค้า, พัสดุครุภัณฑ์ที่ส่งมอบ"
        End Select
        Title_Item_Group = lblHeader_Item.Text

        If DT.Rows.Count = 0 Then
            txt_Item_Name.Text = ""
        Else
            txt_Item_Name.Text = DT.Rows(0).Item("Item_Name").ToString
        End If

        '------------- Set Enabling--------------        
        txt_Item_Name.Enabled = False


    End Sub
#End Region

#Region "Product Detail"
    Private Sub BindProductDetail()
        Dim DT As DataTable = BL.GetAssessmentHeader(Ass_ID)
        If DT.Rows.Count = 0 Then
            txt_Item_No.Text = ""
            txt_Qty.Text = ""
            txt_UOM.Text = ""
            txt_Lot.Text = ""
            txt_Brand.Text = ""
            txt_Model.Text = ""
            txt_Color.Text = ""
            txt_Size.Text = ""
            txt_Price.Text = ""
            txt_Ref_Doc.Text = ""
            txt_RecievedDate.Text = ""
        Else
            '------------ Get List Of StockNo---------------------
            Dim IT As DataTable = BL.GetItemNoByAssessment(Ass_ID)
            txt_Item_No.Text = DT.Rows(0).Item("Item_No").ToString
            Dim ItemList As String = ""
            For i As Integer = 0 To IT.Rows.Count - 1
                Dim ItemNo As String = IT.Rows(i).Item("Item_No").ToString.Trim
                If ItemNo <> "" Then ItemList &= ItemNo & ","
            Next
            If ItemList <> "" Then txt_Item_No.Text = ItemList.Substring(0, ItemList.Length - 1)

            txt_Qty.Text = DT.Rows(0).Item("Qty").ToString
            txt_UOM.Text = DT.Rows(0).Item("UOM").ToString
            txt_Lot.Text = DT.Rows(0).Item("Lot").ToString
            txt_Brand.Text = DT.Rows(0).Item("Brand").ToString
            txt_Model.Text = DT.Rows(0).Item("Model").ToString
            txt_Color.Text = DT.Rows(0).Item("Color").ToString
            txt_Size.Text = DT.Rows(0).Item("Size").ToString
            If Not IsDBNull(DT.Rows(0).Item("Price")) Then
                txt_Price.Text = FormatNumber(DT.Rows(0).Item("Price"), 2)
            Else
                txt_Price.Text = ""
            End If
            txt_Ref_Doc.Text = DT.Rows(0).Item("Ref_Doc").ToString
            If Not IsDBNull(DT.Rows(0).Item("RecievedDate")) Then
                txt_RecievedDate.Text = GL.ReportProgrammingDate(DT.Rows(0).Item("RecievedDate"), txt_RecievedDate_CalendarExtender.Format)
            Else
                txt_RecievedDate.Text = ""
            End If
        End If

        '------------- Set Enabling--------------
        txt_Qty.Enabled = False
        txt_UOM.Enabled = False
        txt_Lot.Enabled = False
        txt_Brand.Enabled = False
        txt_Model.Enabled = False
        txt_Color.Enabled = False
        txt_Size.Enabled = False
        txt_Price.Enabled = False
        txt_Ref_Doc.Enabled = False
        txt_RecievedDate.Enabled = False
        txt_RecievedDate_CalendarExtender.Enabled = False

        lblTagDesc.Visible = False
        txt_Item_No.Visible = False
        rptTag.DataSource = BL.GetItemNoByAssessment(Ass_ID)


        rptTag.DataBind()

    End Sub
#End Region

#Region "File"
    Private Sub BindFile()

        '---รอเชค Step----

        '------------- Set Enabling--------------
        'If Current_Step = AVLBL.ReassessmentStep.Completed Then
        '    btnAddFile.Visible = False
        'Else
        '    btnAddFile.Visible = AssessmentRole = Current_Step
        'End If

        Dim DT As DataTable = BL.GetFileByAssessment(Ass_ID)
        rpt_File.DataSource = DT
        rpt_File.DataBind()
    End Sub

    Protected Sub rpt_File_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rpt_File.ItemCommand
        '------------- Set Enabling--------------
        If Not btnAddFile.Visible Then Exit Sub

        Select Case e.CommandName
            Case "Default"
                Dim Comm As New SqlCommand
                Dim Conn As New SqlConnection(BL.ConnectionString)
                Conn.Open()
                With Comm
                    .Connection = Conn
                    .CommandType = CommandType.Text
                    .CommandText = "UPDATE tb_Ass_File SET IsDefault=CASE File_ID WHEN " & e.CommandArgument & " THEN 1 ELSE 0 END WHERE Ass_ID=" & Ass_ID
                    .ExecuteNonQuery()
                    .Dispose()
                End With
                Conn.Close()
                Conn.Dispose()
                BindFile()

            Case "Edit"

                dialogUploadFile.Visible = True
                dialogFileContainer.Style("width") = "600px"
                txtFileName.CssClass = "m-wrap span10"
                btnSaveFile.CssClass = "btn blue span2"
                pnlAddFile.Visible = False
                pnlEditFile.Visible = True

                Dim lbl_File_Name As Label = e.Item.FindControl("lbl_File_Name")

                Dim F As AVLBL.FileStructure = BL.GetFileStructure(Ass_ID, e.CommandArgument, Server.MapPath(""))
                Session("Assessment_Preview_File_Old") = F
                Session("Assessment_Preview_File_New") = Nothing

                fileOld.ImageUrl = "RenderFile.aspx?A=" & Ass_ID & "&F=" & e.CommandArgument & "&T=" & Now.ToOADate.ToString.Replace(".", "")
                fileNew.ImageUrl = "images/BlackDot.png"
                txtFileName.Text = F.FileName
                File_ID = e.CommandArgument

            Case "Delete"
                Dim Comm As New SqlCommand
                Dim Conn As New SqlConnection(BL.ConnectionString)
                Conn.Open()
                With Comm
                    .Connection = Conn
                    .CommandType = CommandType.Text
                    .CommandText = "Delete From tb_Ass_File WHERE Ass_ID=" & Ass_ID & " AND File_ID=" & e.CommandArgument
                    .ExecuteNonQuery()
                    .Dispose()
                End With
                Conn.Close()
                Conn.Dispose()

                BindFile()
        End Select

    End Sub

    Protected Sub rpt_File_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_File.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lnk_File_Dialog As HtmlAnchor = e.Item.FindControl("lnk_File_Dialog")
        Dim img_File As Image = e.Item.FindControl("img_File")
        Dim lbl_File_Name As Label = e.Item.FindControl("lbl_File_Name")
        Dim imgDefault As HtmlGenericControl = e.Item.FindControl("imgDefault")

        Dim pnlEditFile As HtmlGenericControl = e.Item.FindControl("pnlEditFile")
        Dim lnk_Default As LinkButton = e.Item.FindControl("lnk_Default")
        Dim lnk_Edit As LinkButton = e.Item.FindControl("lnk_Edit")
        Dim lnk_Delete As LinkButton = e.Item.FindControl("lnk_Delete")

        img_File.ImageUrl = "RenderFile.aspx?A=" & Ass_ID & "&F=" & e.Item.DataItem("File_ID") & "&T=" & Now.ToOADate.ToString.Replace(".", "")
        lbl_File_Name.Text = e.Item.DataItem("File_Name").ToString
        lnk_File_Dialog.HRef = img_File.ImageUrl
        imgDefault.Visible = Not IsDBNull(e.Item.DataItem("IsDefault")) AndAlso e.Item.DataItem("IsDefault")

        If GL.IsContentTypeImage(e.Item.DataItem("Content_Type").ToString) Then
            lnk_File_Dialog.Attributes("Title") = "Photo"
        Else
            img_File.ImageUrl = "images/file_type/View_pdf.png"

            lnk_File_Dialog.Attributes("Title") = "PDF"
        End If

        lnk_Default.CommandArgument = e.Item.DataItem("File_ID")
        lnk_Edit.CommandArgument = e.Item.DataItem("File_ID")
        lnk_Delete.CommandArgument = e.Item.DataItem("File_ID")

        '------- Set Enabled ---------
        Dim lnk_Space As LinkButton = e.Item.FindControl("lnk_Space")

        If btnAddFile.Visible Then
            lnk_Default.Visible = True
            lnk_Edit.Visible = True
            lnk_Delete.Visible = True
            lnk_Space.Visible = False
        Else
            lnk_Default.Visible = False
            lnk_Edit.Visible = False
            lnk_Delete.Visible = False
            lnk_Space.Visible = True
            pnlEditFile.Style("background-color") = "inherit !important"
        End If

    End Sub

    Protected Sub btnAddFile_Click(sender As Object, e As System.EventArgs) Handles btnAddFile.Click

        dialogUploadFile.Visible = True
        dialogFileContainer.Style("width") = "400px"
        txtFileName.CssClass = "m-wrap span9"
        btnSaveFile.CssClass = "btn blue span3"
        pnlAddFile.Visible = True
        pnlEditFile.Visible = False

        Session("Assessment_Preview_File_New") = Nothing
        Session("Assessment_Preview_File_Old") = Nothing

        fileAdd.ImageUrl = "images/BlackDot.png"
        txtFileName.Text = ""

        File_ID = 0
    End Sub

    Protected Sub lnkCloseUpload_Click(sender As Object, e As System.EventArgs) Handles lnkCloseUpload.Click
        dialogUploadFile.Visible = False
    End Sub

    Protected Sub btnUpdateFile_Click(sender As Object, e As System.EventArgs) Handles btnUpdateFile.Click
        Select Case True
            Case pnlAddFile.Visible
                Dim UploadedFile As AVLBL.FileStructure = Session("Assessment_Preview_File_New")
                fileAdd.ImageUrl = "RenderFile.aspx?T=" & Now.ToOADate.ToString.Replace(".", "") & "&S=Assessment_Preview_File_New&Mode=Thumbnail"
                txtFileName.Text = UploadedFile.FileName
            Case pnlEditFile.Visible
                Dim UploadedFile As AVLBL.FileStructure = Session("Assessment_Preview_File_New")
                fileNew.ImageUrl = "RenderFile.aspx?T=" & Now.ToOADate.ToString.Replace(".", "") & "&S=Assessment_Preview_File_New&Mode=Thumbnail"
                txtFileName.Text = UploadedFile.FileName
        End Select
        PageScript &= "$('#" & txtFileName.ClientID & "').select();" & vbLf
    End Sub

    Protected Sub btnSaveFile_Click(sender As Object, e As System.EventArgs) Handles btnSaveFile.Click

        Dim FU As AVLBL.FileStructure = Session("Assessment_Preview_File_New")

        If IsNothing(FU) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Upload", "ShowAlert('warning','เลือกไฟล์ภาพที่ Uplaod','');", True)
            Exit Sub
        End If

        If IsNothing(FU.FileContent) OrElse FU.FileContent.Length = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Upload", "ShowAlert('warning','เลือกไฟล์ภาพที่ Uplaod','');", True)
            Exit Sub
        End If

        '--------------- Create File Path --------------
        Dim Path As String = Server.MapPath("FileUpload/Ass/" & Ass_ID)
        If Not Directory.Exists(Path) Then
            Directory.CreateDirectory(Path)
        End If


        '--------------- Calculate To Save---------------
        Dim DT As New DataTable
        Dim SQL As String = "SELECT * FROM tb_Ass_File WHERE Ass_ID=" & Ass_ID & " AND File_ID=" & File_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        DA.Fill(DT)

        Dim UpdateOnlyOne As Boolean = False
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            DT.Rows.Add(DR)
            File_ID = GetNewFileID()
        Else
            DR = DT.Rows(0)
            UpdateOnlyOne = True
        End If

        '--------------- Save To File-------------------
        Path &= "/" & File_ID
        If File.Exists(Path) Then File.Delete(Path)
        Dim f As FileStream = File.Open(Path, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite)
        f.Write(FU.FileContent, 0, FU.FileContent.LongLength)
        f.Close()

        '--------------- Save To Database---------------
        DR("Ass_ID") = Ass_ID
        DR("File_ID") = File_ID
        DR("File_Name") = txtFileName.Text
        DR("Content_Type") = FU.ContentType
        DR("File_Size") = FU.FileContent.LongLength
        DR("Update_By") = User_ID
        DR("Update_Time") = Now
        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)

        '----------------- Hide Dialog -----------------
        dialogUploadFile.Visible = False

        BindFile()

    End Sub

    Private Function GetNewFileID() As Integer
        Dim SQL As String = "SELECT ISNULL(MAX(File_ID),0)+1 FROM tb_Ass_File WHERE Ass_ID=" & Ass_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0)(0)
    End Function

#End Region

#Region "Supplier"
    Private Sub BindSupplier()
        Dim DT As DataTable = BL.GetAssessmentHeader(Ass_ID)
        If DT.Rows.Count = 0 OrElse IsDBNull(DT.Rows(0).Item("S_ID")) Then
            S_ID = 0
            txt_S_Fullname.Text = ""
            BL.BindDDlSupplierType(ddl_S_Type)
            txt_S_Alias.Text = ""
            lbl_S_Tax_No.Text = ""
            txt_S_Address.Text = ""
            txt_S_Contact_Name.Text = ""
            txt_S_Email.Text = ""
            txt_S_Phone.Text = ""
            txt_S_Fax.Text = ""
        Else
            S_ID = DT.Rows(0).Item("S_ID")
            txt_S_Fullname.Text = DT.Rows(0).Item("S_Name").ToString
            'BL.BindDDlSupplierType(ddl_S_Type, S_Type_ID)
            BL.BindDDlSupplierType(ddl_S_Type, DT.Rows(0).Item("S_Type_ID"))
            txt_S_Alias.Text = DT.Rows(0).Item("S_Alias").ToString
            lbl_S_Tax_No.Text = DT.Rows(0).Item("S_Tax_No").ToString
            txt_S_Address.Text = DT.Rows(0).Item("S_Address").ToString
            txt_S_Contact_Name.Text = DT.Rows(0).Item("S_Contact_Name").ToString
            txt_S_Email.Text = DT.Rows(0).Item("S_Email").ToString
            txt_S_Phone.Text = DT.Rows(0).Item("S_Phone").ToString
            txt_S_Fax.Text = DT.Rows(0).Item("S_Fax").ToString
        End If

        '------------- Set Enabling--------------

        txt_S_Fullname.Enabled = False
        ddl_S_Type.Enabled = False
        txt_S_Alias.Enabled = False
        txt_S_Address.Enabled = False
        txt_S_Contact_Name.Enabled = False
        txt_S_Email.Enabled = False
        txt_S_Phone.Enabled = False
        txt_S_Fax.Enabled = False

    End Sub
#End Region

#Region "Reassessment"
    Private Sub BindReassessment()
        '-------------- Bind Questionaire------------------
        Dim QG As DataTable
        Dim Col() As String = {"Q_GP_ID", "Q_GP_Name", "Q_GP_Order"}
        If Current_Step = AVLBL.ReassessmentStep.NotFound Then
            QG = BL.GetQuestionsByCurrentSetting(AVLBL.AssessmentType.ReAssessment)
        Else
            If ReassessmentData.Rows.Count = 0 Then
                QG = BL.GetQuestionsByReassessment(ReAss_ID)
            Else
                QG = ReassessmentData()
            End If

        End If

        TMPQ = QG.Copy
        rpt_ReAss_Q_Group.DataSource = QG.DefaultView.ToTable(True, Col).Copy
        rpt_ReAss_Q_Group.DataBind()
        '-------------- Bind Result------------------
        lbl_Get_Score.Text = ""
        lbl_Max_Score.Text = ""
        lbl_Pass_Score.Text = ""
        knob.Text = ""

        '---START---------ปรับตรวจสอบแต่ละข้อ มีบางข้อไม่ผ่านเกณฑ์๔ือว่าไม่ผ่าน-------------
        IsPass_Status = True
        For i As Integer = 0 To TMPQ.Rows.Count - 1
            If (TMPQ.Rows(i).Item("Get_Score") < TMPQ.Rows(i).Item("Pass_Score")) Then
                IsPass_Status = False
                Exit For
            End If
        Next
        '---END---------ปรับตรวจสอบแต่ละข้อ มีบางข้อไม่ผ่านเกณฑ์๔ือว่าไม่ผ่าน-------------


        Dim Get_Score As Object = TMPQ.Compute("SUM(Get_Score)", "")
        Dim Pass_Score As Object = TMPQ.Compute("SUM(Pass_Score)", "")
        Dim Max_Score As Object = TMPQ.Compute("SUM(Max_Score)", "")
        knob.Attributes("data-fgcolor") = "silver"

        
        lblCriterion.Text = "คะแนนประเมินรวมทุกข้อ ต้องไม่น้อยกว่า " & Pass_Score & " คะแนนถือว่าผ่านการประเมิน"


        If Not IsDBNull(Get_Score) Then
            lbl_Get_Score.Text = Get_Score
            knob.Text = Get_Score
            If Not IsDBNull(Pass_Score) Then
                '-------------ผ่าน/ไม่ผ่าน-------------
                If IsPass_Status And Get_Score >= Pass_Score Then
                    span_Get_Score.Attributes("class") = "label label-success"
                    lbl_Result.Text = "ผ่านการประเมิน"
                    lbl_Result.ForeColor = Drawing.Color.Green
                    knob.Attributes("data-fgcolor") = "green"
                Else
                    span_Get_Score.Attributes("class") = "label label-important"
                    lbl_Result.Text = "ไม่ผ่านการประเมิน"
                    lbl_Result.ForeColor = Drawing.Color.Red
                    knob.Attributes("data-fgcolor") = "red"
                End If


            Else
                lbl_Result.Text = "ไม่สามารถประเมินผลได้"
                lbl_Result.ForeColor = Drawing.Color.Silver
                span_Get_Score.Attributes("class") = "label label-important"
            End If
        End If
        If Not IsDBNull(Pass_Score) Then
            lbl_Pass_Score.Text = Pass_Score

        End If
        If Not IsDBNull(Max_Score) Then
            lbl_Max_Score.Text = Max_Score
            knob.Attributes("data-max") = Max_Score
        End If

        '------------- Set Enabling--------------
        If Current_Step = AVLBL.ReassessmentStep.Completed Then
            pnlMaskAssessment.Visible = True
            lblValidate_Ass.Visible = False
        Else
            pnlMaskAssessment.Visible = ReassessmentRole <> Current_Step
            lblValidate_Ass.Visible = ReassessmentRole = Current_Step
        End If

    End Sub

    Private Sub SaveReassessment()
        Dim AT As DataTable = ReassessmentData()

        '----------------- Save Detail --------------
        Dim SQL As String = "SELECT * FROM tb_ReAss_Detail WHERE ReAss_ID=" & ReAss_ID
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        DA.Fill(DT)
        For i As Integer = 0 To DT.Rows.Count - 1
            AT.DefaultView.RowFilter = "Q_ID=" & DT.Rows(i).Item("Q_ID")
            If AT.DefaultView.Count > 0 Then
                DT.Rows(i).Item("Get_Score") = AT.DefaultView(0).Item("Get_Score")
                If (AT.DefaultView(0).Item("Get_Score") = 0) Then
                    DT.Rows(i).Item("ckConfirm") = AT.DefaultView(0).Item("ckConfirm")
                Else
                    DT.Rows(i).Item("ckConfirm") = False
                End If
            End If
        Next
        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)
        '---------- Update Header Summary Score--------
        BL.UpdateHeaderScore_ReAssessment(ReAss_ID)
        cmd.Dispose()
        DA.Dispose()
        DT.Dispose()

        checkComplete()
    End Sub

    Public Function ReassessmentData() As DataTable

        Dim DT As New DataTable
        DT.Columns.Add("ReAss_ID", GetType(Integer))
        DT.Columns.Add("Q_GP_ID", GetType(Integer))
        DT.Columns.Add("Q_ID", GetType(Integer))
        DT.Columns.Add("Q_GP_Order", GetType(Integer))
        DT.Columns.Add("Q_Order", GetType(Integer))
        DT.Columns.Add("Q_GP_Name")
        DT.Columns.Add("Q_Name")
        DT.Columns.Add("Max_Score", GetType(Integer))
        DT.Columns.Add("Pass_Score", GetType(Integer))
        DT.Columns.Add("Get_Score", GetType(Integer))
        DT.Columns.Add("Q_Comment")
        DT.Columns.Add("AR_ID", GetType(Integer))
        DT.Columns.Add("ckConfirm", GetType(Boolean))


        For Each GP As RepeaterItem In rpt_ReAss_Q_Group.Items
            If GP.ItemType <> ListItemType.AlternatingItem And GP.ItemType <> ListItemType.Item Then Continue For
            Dim lbl_Q_GP_Name As Label = GP.FindControl("lbl_Q_GP_Name")
            Dim Q_GP_ID As Integer = lbl_Q_GP_Name.Attributes("Q_GP_ID")
            Dim Q_GP_Order As Integer = lbl_Q_GP_Name.Attributes("Q_GP_Order")
            Dim AR_ID As Integer = lbl_Q_GP_Name.Attributes("AR_ID")
            Dim rpt_ReAss_Q As Repeater = GP.FindControl("rpt_ReAss_Q")

            For Each Q As RepeaterItem In rpt_ReAss_Q.Items
                If Q.ItemType <> ListItemType.AlternatingItem And Q.ItemType <> ListItemType.Item Then Continue For
                Dim lbl_Q_Name As Label = Q.FindControl("lbl_Q_Name")
                Dim lbl_Score As Label = Q.FindControl("lbl_Score")
                Dim txt_Score As TextBox = Q.FindControl("txt_Score")
                Dim ckConfirm As CheckBox = Q.FindControl("ckConfirm")

                Dim DR As DataRow = DT.NewRow
                DR("ReAss_ID") = ReAss_ID
                DR("Q_GP_ID") = Q_GP_ID
                DR("Q_ID") = lbl_Q_Name.Attributes("Q_ID")
                DR("Q_GP_Order") = Q_GP_Order
                DR("Q_Order") = lbl_Q_Name.Attributes("Q_Order")
                DR("Q_GP_Name") = lbl_Q_GP_Name.Text
                DR("Q_Name") = lbl_Q_Name.Text
                DR("Max_Score") = txt_Score.Attributes("Max_Score")
                DR("Pass_Score") = txt_Score.Attributes("Pass_Score")
                DR("Get_Score") = txt_Score.Text
                DR("Q_Comment") = ""
                DR("AR_ID") = lbl_Q_Name.Attributes("AR_ID")
                DR("ckConfirm") = ckConfirm.Checked
                DT.Rows.Add(DR)
            Next
            'For Each Q As RepeaterItem In 
        Next
        Return DT

    End Function

    Dim TMPQ As DataTable
    Protected Sub rpt_ReAss_Q_Group_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpt_ReAss_Q_Group.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lbl_Q_GP_No As Label = e.Item.FindControl("lbl_Q_GP_No")
        Dim lbl_Q_GP_Name As Label = e.Item.FindControl("lbl_Q_GP_Name")
        Dim rpt_ReAss_Q As Repeater = e.Item.FindControl("rpt_ReAss_Q")
        Dim hr As HtmlGenericControl = e.Item.FindControl("hr")

        hr.Visible = e.Item.ItemIndex > 0

        lbl_Q_GP_No.Text = (e.Item.ItemIndex + 1) & ". "
        lbl_Q_GP_Name.Text = e.Item.DataItem("Q_GP_Name").ToString
        lbl_Q_GP_Name.Attributes("Q_GP_ID") = e.Item.DataItem("Q_GP_ID")
        lbl_Q_GP_Name.Attributes("Q_GP_Order") = e.Item.DataItem("Q_GP_Order")

        TMPQ.DefaultView.RowFilter = "Q_GP_ID=" & e.Item.DataItem("Q_GP_ID")
        Ass_Count = TMPQ.DefaultView.Count
        AddHandler rpt_ReAss_Q.ItemDataBound, AddressOf rpt_ReAss_Q_ItemDataBound
        rpt_ReAss_Q.DataSource = TMPQ.DefaultView.ToTable
        rpt_ReAss_Q.DataBind()

    End Sub

    Dim Ass_Count As Integer = 0
    Protected Sub rpt_ReAss_Q_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs)
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lbl_Q_Name As Label = e.Item.FindControl("lbl_Q_Name")
        Dim lbl_Q_Name_Order As Label = e.Item.FindControl("lbl_Q_Name_Order")
        Dim lbl_Score As Label = e.Item.FindControl("lbl_Score")
        Dim txt_Score As TextBox = e.Item.FindControl("txt_Score")
        Dim slider As HtmlGenericControl = e.Item.FindControl("slider")
        Dim lblReqField As Label = e.Item.FindControl("lblReqField")
        Dim ckConfirm As CheckBox = e.Item.FindControl("ckConfirm")
        Dim pnlSlider As Panel = e.Item.FindControl("pnlSlider")
        Dim pnlck As Panel = e.Item.FindControl("pnlck")
        Dim lbl_Pass_Score As Label = e.Item.FindControl("lbl_Pass_Score")
        If Current_Step <> AVLBL.ReassessmentStep.Completed Then
            Select Case e.Item.DataItem("AR_ID").ToString '----------- สถานะใบประเมิน ----------
                Case AVLBL.ReassessmentStep.Creater
                    lblReqField.Visible = ReassessmentRole = AVLBL.ReassessmentRole.Creater
                    pnlck.Visible = ReassessmentRole = AVLBL.ReassessmentRole.Creater And (e.Item.DataItem("Get_Score") = 0)
                Case (AVLBL.ReassessmentStep.Auditor)
                    lblReqField.Visible = ReassessmentRole = AVLBL.ReassessmentRole.Auditor
                    pnlck.Visible = ReassessmentRole = AVLBL.ReassessmentRole.Auditor And (e.Item.DataItem("Get_Score") = 0)
                Case AVLBL.ReassessmentStep.Supply_Officer
                    lblReqField.Visible = ReassessmentRole = AVLBL.ReassessmentRole.Supply_Officer
                    pnlck.Visible = ReassessmentRole = AVLBL.ReassessmentRole.Supply_Officer And (e.Item.DataItem("Get_Score") = 0)
                Case AVLBL.ReassessmentStep.Director
                    lblReqField.Visible = ReassessmentRole = AVLBL.ReassessmentRole.Director
                    pnlck.Visible = ReassessmentRole = AVLBL.ReassessmentRole.Director And (e.Item.DataItem("Get_Score") = 0)
                Case AVLBL.ReassessmentStep.Completed

            End Select
            If ReassessmentRole = Current_Step Then
                If lblReqField.Visible Then
                    slider.Attributes("class") = "slider bg-green"
                Else
                    slider.Attributes("class") = "disabled"
                End If

            Else
                slider.Attributes("class") = "slider bg-green"
                lblReqField.Visible = False
            End If

        Else
            slider.Attributes("class") = "slider bg-green"
            lblReqField.Visible = False
            pnlck.Visible = False
        End If

        lbl_Pass_Score.Text = e.Item.DataItem("Pass_Score").ToString
        If Ass_Count > 1 Then
            lbl_Q_Name_Order.Text = e.Item.ItemIndex + 1 & ". "
            lbl_Q_Name.Text = e.Item.DataItem("Q_Name").ToString
        Else
            lbl_Q_Name.Text = e.Item.DataItem("Q_Name").ToString
        End If
        lbl_Q_Name.Attributes("Q_ID") = e.Item.DataItem("Q_ID")
        lbl_Q_Name.Attributes("Q_Order") = e.Item.DataItem("Q_Order")
        lbl_Q_Name.Attributes("AR_ID") = e.Item.DataItem("AR_ID")
        txt_Score.Attributes("Max_Score") = e.Item.DataItem("Max_Score")
        txt_Score.Attributes("Pass_Score") = e.Item.DataItem("Pass_Score")
        txt_Score.Text = e.Item.DataItem("Get_Score")

        If (e.Item.DataItem("ckConfirm") = True) Then
            ckConfirm.Checked = True
        Else
            ckConfirm.Checked = False
        End If

        ckConfirm.Attributes("onchange") = "document.getElementById('" & btnUpdateScore.ClientID & "').click();"

    End Sub

    Private Function jQuerySlider() As String
        Dim Script As String = "initKnob();" & vbLf
        For Each GP As RepeaterItem In rpt_ReAss_Q_Group.Items
            If GP.ItemType <> ListItemType.AlternatingItem And GP.ItemType <> ListItemType.Item Then Continue For
            Dim rpt_ReAss_Q As Repeater = GP.FindControl("rpt_ReAss_Q")

            For Each Q As RepeaterItem In rpt_ReAss_Q.Items
                If Q.ItemType <> ListItemType.AlternatingItem And Q.ItemType <> ListItemType.Item Then Continue For
                Dim lbl_Q_Name As Label = Q.FindControl("lbl_Q_Name")
                Dim lbl_Score As Label = Q.FindControl("lbl_Score")
                Dim txt_Score As TextBox = Q.FindControl("txt_Score")
                Dim slider As HtmlGenericControl = Q.FindControl("slider")
                Script &= "initSlider('" & slider.ClientID & "'," & txt_Score.Text & ",0," & txt_Score.Attributes("Max_Score") & "," & txt_Score.Attributes("Pass_Score") & ",'" & lbl_Score.ClientID & "','" & txt_Score.ClientID & "','" & btnUpdateScore.ClientID & "');" & vbLf
            Next
        Next
        Return Script
    End Function

    Protected Sub btnUpdateScore_Click(sender As Object, e As System.EventArgs) Handles btnUpdateScore.Click
        'SaveReassessment()
        BindReassessment()
    End Sub

#End Region

#Region "Comment"
    Private Sub BindCommentUser()
        Dim DT As DataTable = BL.GetReassessmentHeader(Ass_ID, ReAss_ID)
        If DT.Rows.Count = 0 Then
            txt_Comment_Creater.Text = ""
            txt_Comment_Auditor.Text = ""
            txt_Comment_Officer.Text = ""
            txt_Comment_Director.Text = ""
            lbl_Creater_Name.Text = ""
            lbl_Auditor_Name.Text = ""
            lbl_Officer_Name.Text = ""
            lbl_Director_Name.Text = ""
        Else
            txt_Comment_Creater.Text = DT.Rows(0).Item("Role_1_Comment").ToString
            txt_Comment_Auditor.Text = DT.Rows(0).Item("Role_2_Comment").ToString
            txt_Comment_Officer.Text = DT.Rows(0).Item("Role_3_Comment").ToString
            txt_Comment_Director.Text = DT.Rows(0).Item("Role_4_Comment").ToString
            lbl_Creater_Name.Text = Trim(DT.Rows(0).Item("Role_1_Title").ToString & DT.Rows(0).Item("Role_1_Fisrt_Name").ToString & " " & DT.Rows(0).Item("Role_1_Last_Name").ToString)
            lbl_Auditor_Name.Text = Trim(DT.Rows(0).Item("Role_2_Title").ToString & DT.Rows(0).Item("Role_2_Fisrt_Name").ToString & " " & DT.Rows(0).Item("Role_2_Last_Name").ToString)
            lbl_Officer_Name.Text = Trim(DT.Rows(0).Item("Role_3_Title").ToString & DT.Rows(0).Item("Role_3_Fisrt_Name").ToString & " " & DT.Rows(0).Item("Role_3_Last_Name").ToString)
            lbl_Director_Name.Text = Trim(DT.Rows(0).Item("Role_4_Title").ToString & DT.Rows(0).Item("Role_4_Fisrt_Name").ToString & " " & DT.Rows(0).Item("Role_4_Last_Name").ToString)
        End If

        If lbl_Creater_Name.Text = "" Then lbl_Creater_Name.Text = "<br/>........."
        If lbl_Auditor_Name.Text = "" Then lbl_Auditor_Name.Text = "<br/>........."
        If lbl_Officer_Name.Text = "" Then lbl_Officer_Name.Text = "<br/>........."
        If lbl_Director_Name.Text = "" Then lbl_Director_Name.Text = "<br/>........."

        '------------- Set Enabling--------------
        txt_Comment_Creater.Enabled = False
        txt_Comment_Auditor.Enabled = False
        txt_Comment_Officer.Enabled = False
        txt_Comment_Director.Enabled = False
        Select Case Current_Step '----------- สถานะใบประเมิน ----------
            Case AVLBL.ReassessmentStep.Creater
                txt_Comment_Creater.Enabled = ReassessmentRole = AVLBL.ReassessmentRole.Creater
            Case AVLBL.ReassessmentStep.Auditor
                txt_Comment_Auditor.Enabled = ReassessmentRole = AVLBL.ReassessmentRole.Auditor
            Case AVLBL.ReassessmentStep.Supply_Officer
                txt_Comment_Officer.Enabled = ReassessmentRole = AVLBL.ReassessmentRole.Supply_Officer
            Case AVLBL.ReassessmentStep.Director
                txt_Comment_Director.Enabled = ReassessmentRole = AVLBL.ReassessmentRole.Director
            Case AVLBL.ReassessmentStep.Completed

        End Select

    End Sub
    Private Sub SaveComment()
        Dim SQL As String = "SELECT * FROM tb_ReAss_Header WHERE Ass_ID=" & Ass_ID & " AND ReAss_ID=" & ReAss_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        DT.Rows(0).Item("Role_1_Comment") = txt_Comment_Creater.Text
        DT.Rows(0).Item("Role_2_Comment") = txt_Comment_Auditor.Text
        DT.Rows(0).Item("Role_3_Comment") = txt_Comment_Officer.Text
        DT.Rows(0).Item("Role_4_Comment") = txt_Comment_Director.Text

        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)
    End Sub

    Protected Sub txt_Comment_TextChanged(sender As Object, e As System.EventArgs) Handles txt_Comment_Creater.TextChanged, txt_Comment_Auditor.TextChanged, txt_Comment_Officer.TextChanged, txt_Comment_Director.TextChanged
        'SaveComment()
        'BindCommentUser()
    End Sub

#End Region


#Region "Command"
    Private Sub BindWorkflow()

        '----------- Check สิทธิ์ที่สามารถเข้าถึงอย่างน้อย 1 ฝ่าย --------------
        Dim RT As DataTable = BL.GetUserDeptRole(User_ID)
        If RT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลนี้','Default.aspx');", True)
            Exit Sub
        End If

        '------------แต่ละคนสามารถประเมินได้หลายขั้นตอน-----------------
        RT = BL.GetUserDeptRole(User_ID, Dept_ID, Sub_Dept_ID)
        If RT.Rows.Count > 0 Then
            RT.DefaultView.RowFilter = "AR_ID='" & Current_Step & "'"
            If RT.DefaultView.Count = 1 Then
                ReassessmentRole = RT.DefaultView(0).Item("AR_ID") '--------- บทบาทการประเมินของผู้ใช้คนปัจจุบัน -----------
            End If
        End If


        If ReassessmentRole = AVLBL.ReassessmentRole.None Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลนี้','Default.aspx');", True)
            Exit Sub
        End If

        css_On_Creator.Visible = False
        css_On_Auditor.Visible = False
        css_On_Officer.Visible = False
        css_On_Director.Visible = False
        css_On_Complete.Visible = False
        css_On_Del.Visible = False  '-----ลบใบประเมินเฉพาะขั้นตอน สร้างและผู้มีสิทธิ์-----

        css_Can_Creator.Visible = False
        css_Can_Auditor.Visible = False
        css_Can_Officer.Visible = False
        css_Can_Director.Visible = False
        css_Can_Completed.Visible = False
        css_Can_Del.Visible = False  '-----ลบใบประเมินเฉพาะขั้นตอน สร้างและผู้มีสิทธิ์-----
        btnSaveAll.CssClass = "btn green span6 margin-bottom-25 pull-left"
        btn_Send_Del2.CssClass = "btn yellow span6 margin-bottom-25 pull-left"
        btn_Send_Del2.Visible = False
        pnl_Send_Delete.Visible = False     '-----ไม่ใช้ปุ่มนี้แล้วเลื่อนไปไว้ด้านบน

        lbl_Status_Helper.Text = ""

        '--------------- ดูว่า สถานะของการประเมินตรงกับผู้ใช้คนปัจจุบันหรือไม่ ---------------------
        Dim DA As New SqlDataAdapter("SELECT * FROM tb_ReAss_Header WHERE Ass_ID=" & Ass_ID & " AND ReAss_ID=" & ReAss_ID, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        '---------Endable ปุ่ม Delete--------
        Dim UserInfo As AVLBL.UserInfo = BL.GetUserInfo(User_ID)
        Dim Old_Role_1_ID As String = DT.Rows(0).Item("Role_1_ID").ToString()

        Select Case Current_Step '----------- สถานะใบประเมิน ----------
            Case AVLBL.ReassessmentStep.Creater

                lbl_Status_Helper.Text = "ขณะนี้การประเมินอยู่ในขั้นตอนของ 'ผู้ประเมิน'"
                If ReassessmentRole = AVLBL.ReassessmentRole.Creater Then '----------- สิทธิ์ผู้ใช้ตรงกับใบประเมินหรือไม่ ----------
                    lbl_Status_Helper.Text &= " คุณสามารถคลิกเพื่อส่งต่อใบประเมินไปยัง 'หัวหน้าฝ่าย/ภาค/งาน' ได้"
                    ''--------------- Save start-time if not saved yet -------------
                    'DT.Rows(0).Item("Role_1_ID") = User_ID
                    'DT.Rows(0).Item("Role_1_Title") = Session("Title")
                    'DT.Rows(0).Item("Role_1_Fisrt_Name") = Session("Fisrt_Name")
                    'DT.Rows(0).Item("Role_1_Last_Name") = Session("Last_Name")
                    'DT.Rows(0).Item("Role_1_Pos_Name") = Session("Pos_Name")
                    'If IsDBNull(DT.Rows(0).Item("Role_1_Start")) Then
                    '    DT.Rows(0).Item("Role_1_Start") = Now
                    'End If
                    'DT.Rows(0).Item("Role_1_Active") = True
                    'DT.Rows(0).Item("Pass_Status") = False
                    'DT.Rows(0).Item("Update_By") = User_ID
                    'DT.Rows(0).Item("Update_Time") = Now
                    'Dim cmd As New SqlCommandBuilder(DA)
                    'DA.Update(DT)
                End If
                css_On_Creator.Visible = True
                css_Can_Auditor.Visible = Current_Step = ReassessmentRole
                btnSaveAll.Visible = Current_Step = ReassessmentRole

                '------ปุ่ม Del--------
                Dim Iscan As Boolean = False
                If UserInfo.Cancel_Role Then    ' สามารถยกเลิกใบประเมินได้
                    btn_Send_Del2.Visible = UserInfo.Cancel_Role
                    Iscan = UserInfo.Cancel_Role

                Else
                    If IsDBNull(DT.Rows(0).Item("Role_2_Start")) Then
                        Iscan = (Current_Step = ReassessmentRole) And (Session("User_ID") = Old_Role_1_ID)
                        btn_Send_Del2.Visible = Iscan
                    End If
                End If

                If ((Current_Step = ReassessmentRole) And Iscan) Then
                    btnSaveAll.CssClass = "btn green span6 margin-bottom-25 pull-left"
                    btn_Send_Del2.CssClass = "btn yellow span6 margin-bottom-25 pull-left"



                ElseIf (((Current_Step = ReassessmentRole)) And Not Iscan) Then
                    btnSaveAll.CssClass = "btn green span12 margin-bottom-25 pull-left"
                    btn_Send_Del2.Visible = Iscan



                ElseIf (Not ((Current_Step = ReassessmentRole)) And Iscan) Then
                    btn_Send_Del2.CssClass = "btn yellow span12 margin-bottom-25 pull-left"
                    'btnSaveAll.Visible = UserInfo.Cancel_Role
                ElseIf (Not ((Current_Step = ReassessmentRole)) And Not Iscan) Then
                    'btnSaveAll.Visible = UserInfo.Cancel_Role
                    btn_Send_Del2.Visible = Iscan
                End If


                ''------ปุ่ม Del--------
                'Dim Iscan As Boolean = False
                'If UserInfo.Cancel_Role Then
                '    btn_Send_Del2.Visible = UserInfo.Cancel_Role
                '    Iscan = UserInfo.Cancel_Role
                'Else
                '    If IsDBNull(DT.Rows(0).Item("Role_2_Start")) Then
                '        Iscan = (Current_Step = ReassessmentRole) And (Session("User_ID") = Old_Role_1_ID)
                '        btn_Send_Del2.Visible = Iscan
                '    End If
                'End If

                'If ((Current_Step = ReassessmentRole) And Iscan) Then
                '    btnSaveAll.CssClass = "btn green span6 margin-bottom-25 pull-left"
                '    btn_Send_Del2.CssClass = "btn yellow span6 margin-bottom-25 pull-left"
                'ElseIf (((Current_Step = ReassessmentRole)) And Not Iscan) Then
                '    btnSaveAll.CssClass = "btn green span12 margin-bottom-25 pull-left"
                '    btn_Send_Del2.Visible = Iscan
                'ElseIf (Not ((Current_Step = ReassessmentRole)) And Iscan) Then
                '    btn_Send_Del2.CssClass = "btn yellow span12 margin-bottom-25 pull-left"
                '    btnSaveAll.Visible = UserInfo.Cancel_Role
                'ElseIf (Not ((Current_Step = ReassessmentRole)) And Not Iscan) Then
                '    btnSaveAll.Visible = UserInfo.Cancel_Role
                '    btn_Send_Del2.Visible = Iscan
                'End If

                ''If (Not UserInfo.Cancel_Role) Then
                ''    btnSaveAll.CssClass = "btn green span12 margin-bottom-25 pull-left"
                ''ElseIf (UserInfo.Cancel_Role And (btnSaveAll.Visible = False)) Then
                ''    btn_Send_Del2.CssClass = "btn yellow span12 margin-bottom-25 pull-left"
                ''End If

            Case AVLBL.ReassessmentStep.Auditor

                lbl_Status_Helper.Text = "ขณะนี้การประเมินอยู่ในขั้นตอนของ 'หัวหน้าฝ่าย/ภาค/งาน'"
                If ReassessmentRole = AVLBL.ReassessmentRole.Auditor Then '----------- สิทธิ์ผู้ใช้ตรงกับใบประเมินหรือไม่ ----------
                    lbl_Status_Helper.Text &= " คุณสามารถคลิกเพื่อส่งต่อใบประเมินไปยัง 'เจ้าหน้าที่พัสดุ' หรือส่งกลับไปยัง 'ผู้ประเมิน' ได้"
                    ''--------------- Save start-time if not saved yet -------------
                    'DT.Rows(0).Item("Role_2_ID") = User_ID
                    'DT.Rows(0).Item("Role_2_Title") = Session("Title")
                    'DT.Rows(0).Item("Role_2_Fisrt_Name") = Session("Fisrt_Name")
                    'DT.Rows(0).Item("Role_2_Last_Name") = Session("Last_Name")
                    'DT.Rows(0).Item("Role_2_Pos_Name") = Session("Pos_Name")
                    'If IsDBNull(DT.Rows(0).Item("Role_2_Start")) Then
                    '    DT.Rows(0).Item("Role_2_Start") = Now
                    'End If
                    'DT.Rows(0).Item("Role_2_Active") = True
                    'DT.Rows(0).Item("Update_By") = User_ID
                    'DT.Rows(0).Item("Update_Time") = Now
                    'Dim cmd As New SqlCommandBuilder(DA)
                    'DA.Update(DT)
                End If
                css_On_Auditor.Visible = True
                If ((ReassessmentRole + 1) = Current_Step) Then
                    css_Can_Creator.Visible = Not DT.Rows(0).Item("Role_2_Active")
                Else
                    css_Can_Creator.Visible = (Current_Step = ReassessmentRole)
                End If
                css_Can_Officer.Visible = Current_Step = ReassessmentRole
                btnSaveAll.Visible = Current_Step = ReassessmentRole
                '------ปุ่ม Del--------
                btn_Send_Del2.Visible = UserInfo.Cancel_Role

                If (Not UserInfo.Cancel_Role) Then
                    btnSaveAll.CssClass = "btn green span12 margin-bottom-25 pull-left"
                ElseIf (UserInfo.Cancel_Role And (btnSaveAll.Visible = False)) Then
                    btn_Send_Del2.CssClass = "btn yellow span12 margin-bottom-25 pull-left"
                End If

            Case AVLBL.ReassessmentStep.Supply_Officer

                lbl_Status_Helper.Text = "ขณะนี้การประเมินอยู่ในขั้นตอนของ 'เจ้าหน้าที่พัสดุ'"
                If ReassessmentRole = AVLBL.ReassessmentRole.Supply_Officer Then '----------- สิทธิ์ผู้ใช้ตรงกับใบประเมินหรือไม่ ----------
                    lbl_Status_Helper.Text &= " คุณสามารถคลิกเพื่อส่งต่อใบประเมินไปยัง 'หัวหน้าฝ่ายบริหารทั่วไป' หรือส่งกลับไปยัง 'หัวหน้าฝ่าย/ภาค/งาน' ได้"
                    ''--------------- Save start-time if not saved yet -------------
                    'DT.Rows(0).Item("Role_3_ID") = User_ID
                    'DT.Rows(0).Item("Role_3_Title") = Session("Title")
                    'DT.Rows(0).Item("Role_3_Fisrt_Name") = Session("Fisrt_Name")
                    'DT.Rows(0).Item("Role_3_Last_Name") = Session("Last_Name")
                    'DT.Rows(0).Item("Role_3_Pos_Name") = Session("Pos_Name")
                    'If IsDBNull(DT.Rows(0).Item("Role_3_Start")) Then
                    '    DT.Rows(0).Item("Role_3_Start") = Now
                    'End If
                    'DT.Rows(0).Item("Role_3_Active") = True
                    'DT.Rows(0).Item("Update_By") = User_ID
                    'DT.Rows(0).Item("Update_Time") = Now
                    'Dim cmd As New SqlCommandBuilder(DA)
                    'DA.Update(DT)
                End If
                css_On_Officer.Visible = True
                If ((ReassessmentRole + 1) = Current_Step) Then
                    css_Can_Auditor.Visible = Not DT.Rows(0).Item("Role_3_Active")
                Else
                    css_Can_Auditor.Visible = (Current_Step = ReassessmentRole)
                End If
                css_Can_Director.Visible = Current_Step = ReassessmentRole
                btnSaveAll.Visible = Current_Step = ReassessmentRole
                '------ปุ่ม Del--------
                btn_Send_Del2.Visible = UserInfo.Cancel_Role

                If (Not UserInfo.Cancel_Role) Then
                    btnSaveAll.CssClass = "btn green span12 margin-bottom-25 pull-left"
                ElseIf (UserInfo.Cancel_Role And (btnSaveAll.Visible = False)) Then
                    btn_Send_Del2.CssClass = "btn yellow span12 margin-bottom-25 pull-left"
                End If

            Case AVLBL.ReassessmentStep.Director

                lbl_Status_Helper.Text = "ขณะนี้การประเมินอยู่ในขั้นตอนของ 'หัวหน้าฝ่ายบริหารทั่วไป'"
                If ReassessmentRole = AVLBL.ReassessmentRole.Director Then '----------- สิทธิ์ผู้ใช้ตรงกับใบประเมินหรือไม่ ----------
                    lbl_Status_Helper.Text &= " คุณสามารถคลิกเพื่อยืนยันจบการประเมิน หรือส่งกลับไปยัง 'เจ้าหน้าที่พัสดุ' ได้"
                    '--------------- Save start-time if not saved yet -------------
                    DT.Rows(0).Item("Role_4_ID") = User_ID
                    DT.Rows(0).Item("Role_4_Title") = Session("Title")
                    DT.Rows(0).Item("Role_4_Fisrt_Name") = Session("Fisrt_Name")
                    DT.Rows(0).Item("Role_4_Last_Name") = Session("Last_Name")
                    DT.Rows(0).Item("Role_4_Pos_Name") = Session("Pos_Name")
                    If IsDBNull(DT.Rows(0).Item("Role_4_Start")) Then
                        DT.Rows(0).Item("Role_4_Start") = Now
                    End If
                    DT.Rows(0).Item("Role_4_Active") = True
                    DT.Rows(0).Item("Update_By") = User_ID
                    DT.Rows(0).Item("Update_Time") = Now
                    Dim cmd As New SqlCommandBuilder(DA)
                    DA.Update(DT)
                End If
                css_On_Director.Visible = True
                If ((ReassessmentRole + 1) = Current_Step) Then
                    css_Can_Officer.Visible = Not DT.Rows(0).Item("Role_4_Active")
                Else
                    css_Can_Officer.Visible = (Current_Step = ReassessmentRole)
                End If
                css_Can_Completed.Visible = Current_Step = ReassessmentRole
                btnSaveAll.Visible = Current_Step = ReassessmentRole
                '------ปุ่ม Del--------
                btn_Send_Del2.Visible = UserInfo.Cancel_Role

                If (Not UserInfo.Cancel_Role) Then
                    btnSaveAll.CssClass = "btn green span12 margin-bottom-25 pull-left"
                ElseIf (UserInfo.Cancel_Role And (btnSaveAll.Visible = False)) Then
                    btn_Send_Del2.CssClass = "btn yellow span12 margin-bottom-25 pull-left"
                End If

            Case AVLBL.ReassessmentStep.Completed
                css_On_Complete.Visible = True
                lbl_Status_Helper.Text = "ขณะนี้การประเมินเสร็จสมบูรณ์แล้ว"
                '--------- Save nothing-----------
                btnSaveAll.Visible = False
                '------ปุ่ม Del--------
                btn_Send_Del2.Visible = UserInfo.Cancel_Role

                If (Not UserInfo.Cancel_Role) Then
                    btnSaveAll.CssClass = "btn green span12 margin-bottom-25 pull-left"
                ElseIf (UserInfo.Cancel_Role And (btnSaveAll.Visible = False)) Then
                    btn_Send_Del2.CssClass = "btn yellow span12 margin-bottom-25 pull-left"
                End If

        End Select

        '---------------Report user information history--------------
        lblCreaterInfo.Text = ReportSteppedUserInfo(DT.Rows(0).Item("Role_1_ID"), DT.Rows(0).Item("Role_1_Title"), DT.Rows(0).Item("Role_1_Fisrt_Name"), DT.Rows(0).Item("Role_1_Last_Name"), DT.Rows(0).Item("Role_1_Start"), DT.Rows(0).Item("Role_1_End"))
        lblAuditorInfo.Text = ReportSteppedUserInfo(DT.Rows(0).Item("Role_2_ID"), DT.Rows(0).Item("Role_2_Title"), DT.Rows(0).Item("Role_2_Fisrt_Name"), DT.Rows(0).Item("Role_2_Last_Name"), DT.Rows(0).Item("Role_2_Start"), DT.Rows(0).Item("Role_2_End"))
        lblSupplyOfficerInfo.Text = ReportSteppedUserInfo(DT.Rows(0).Item("Role_3_ID"), DT.Rows(0).Item("Role_3_Title"), DT.Rows(0).Item("Role_3_Fisrt_Name"), DT.Rows(0).Item("Role_3_Last_Name"), DT.Rows(0).Item("Role_3_Start"), DT.Rows(0).Item("Role_3_End"))
        lblDirectorInfo.Text = ReportSteppedUserInfo(DT.Rows(0).Item("Role_4_ID"), DT.Rows(0).Item("Role_4_Title"), DT.Rows(0).Item("Role_4_Fisrt_Name"), DT.Rows(0).Item("Role_4_Last_Name"), DT.Rows(0).Item("Role_4_Start"), DT.Rows(0).Item("Role_4_End"))

    End Sub

    Private Function ReportSteppedUserInfo(ByVal RoleID As Object, ByVal Title As Object, ByVal Fisrt_Name As Object, ByVal Last_Name As Object, ByVal Role_Start As Object, ByVal Role_End As Object) As String

        Dim Result As String = ""
        If IsDBNull(RoleID) Or IsDBNull(Title) Or IsDBNull(Fisrt_Name) Or IsDBNull(Last_Name) Then Return Result

        Result = " โดย " & Title & Fisrt_Name & " " & Last_Name
        If (Not IsDBNull(Role_Start)) And (Not IsDBNull(Role_Start)) And (Not IsDBNull(Role_End)) Then
            Result &= " วันที่ " & GL.ReportThaiDate(Role_Start) & " ถึง " & GL.ReportThaiDate(Role_End)
            Result &= " (" & DateDiff(DateInterval.Day, Role_Start, Role_End) + 1 & " วัน)"
        ElseIf Not IsDBNull(Role_Start) Then
            Result &= " เริ่มประเมินวันที่ " & GL.ReportThaiDate(Role_Start)
            Select Case DateDiff(DateInterval.Day, Role_Start, Now)
                Case 0
                    Result &= " (วันนี้)"
                Case 1
                    Result &= " (เมื่อวาน)"
                Case Else
                    Result &= " (" & DateDiff(DateInterval.Day, Role_Start, Now) & " วันมาแล้ว)"
            End Select
        End If

        Return Result

    End Function


    Private Function jQueryWorkflowButton() As String
        Dim Script As String = ""
        'If css_Can_Creator.Visible Then Script &= "$('#pnl_Send_Creater').click(function () { if(confirm('ยืนยันส่งใบประเมินไปยัง ""ผู้ประเมิน"" ?'))$('#btn_Send_Creater').click(); });" & vbLf
        'If css_Can_Auditor.Visible Then Script &= "$('#pnl_Send_Auditor').click(function () { if(confirm('ยืนยันส่งใบประเมินไปยัง ""หัวหน้าฝ่าย/ภาค/งาน"" ?'))$('#btn_Send_Auditor').click(); });" & vbLf
        'If css_Can_Officer.Visible Then Script &= "$('#pnl_Send_Officer').click(function () { if(confirm('ยืนยันส่งใบประเมินไปยัง ""เจ้าหน้าที่พัสดุ"" ?'))$('#btn_Send_Officer').click(); });" & vbLf
        'If css_Can_Director.Visible Then Script &= "$('#pnl_Send_Director').click(function () { if(confirm('ยืนยันส่งใบประเมินไปยัง ""หัวหน้าฝ่ายบริหาร"" ?'))$('#btn_Send_Director').click(); });" & vbLf
        'If css_Can_Completed.Visible Then Script &= "$('#pnl_Send_Complete').click(function () { if(confirm('ยืนยันผลการประเมินทั้งหมด ?'))$('#btn_Send_Complete').click(); });" & vbLf
        'If css_Can_Del.Visible Then Script &= "$('#pnl_Send_Delete').click(function () { if(confirm('ยืนยันลบใบประเมินทบทวน ?'))$('#btn_Send_Del').click(); });" & vbLf

        If css_Can_Creator.Visible Then pnl_Send_Creater.Attributes("onclick") = "if(confirm('ยืนยันส่งใบประเมินไปยัง ""ผู้ประเมิน"" ?'))document.getElementById('" & btn_Send_Creater.ClientID & "').click();" & vbLf
        If css_Can_Auditor.Visible Then pnl_Send_Auditor.Attributes("onclick") = "if(confirm('ยืนยันส่งใบประเมินไปยัง ""หัวหน้าฝ่าย/ภาค/งาน"" ?'))document.getElementById('" & btn_Send_Auditor.ClientID & "').click();" & vbLf
        If css_Can_Officer.Visible Then pnl_Send_Officer.Attributes("onclick") = "if(confirm('ยืนยันส่งใบประเมินไปยัง ""เจ้าหน้าที่พัสดุ"" ?'))document.getElementById('" & btn_Send_Officer.ClientID & "').click();" & vbLf
        If css_Can_Director.Visible Then pnl_Send_Director.Attributes("onclick") = "if(confirm('ยืนยันส่งใบประเมินไปยัง ""หัวหน้าฝ่ายบริหาร"" ?'))document.getElementById('" & btn_Send_Director.ClientID & "').click();" & vbLf
        If css_Can_Completed.Visible Then pnl_Send_Complete.Attributes("onclick") = "if(confirm('ยืนยันผลการประเมินทั้งหมด ?'))document.getElementById('" & btn_Send_Complete.ClientID & "').click();" & vbLf
        If css_Can_Del.Visible Then pnl_Send_Delete.Attributes("onclick") = "if(confirm('ยืนยันลบใบประเมินทบทวน ?'))document.getElementById('" & btn_Send_Del.ClientID & "').click();" & vbLf

        Return Script
    End Function

    '----ลบใบประเมิน
    Private Sub Delete_ReAss(sender As Object, e As System.EventArgs) Handles btn_Send_Del.Click, btnOK_DEL.Click
        Dim btn As Button = sender
        If txt_DEL_Comment.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "validate", "ShowAlert('warning','กรอก หมายเหตุการยกเลิกใบประเมินทบทวน','');", True)
            txt_DEL_Comment.Focus()
            Exit Sub
        End If

        ' ตรวจสอบ มีใบประเมินทบทวนล่าสุดหรือไม่ ล่าสุดถึงจะลบได้
        Dim SQL As String = ""
        SQL &= " SELECT TOP 1 ReAss_ID,Ass_ID      "
        SQL &= " FROM tb_ReAss_Header "
        SQL &= " WHERE Ass_ID = " & Ass_ID
        SQL &= " ORDER BY  ReAss_ID  Desc"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            If DT.Rows(0).Item("ReAss_ID").ToString() <> ReAss_ID Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "validate", "ShowAlert('warning','ไม่สามารถลบใบประเมินทบทวนนี้ได้  เนื่องจากมีประวัติการประเมินทบทวนใบนี้อยู่ในระบบ','');", True)
                Exit Sub
            End If
        End If

        ' เก็บ Description เข้าตาราง tb_Log_Delete
        Dim DTReAss As DataTable = BL.GetReassessmentHeader(Ass_ID, ReAss_ID)

        SQL = "SELECT * FROM tb_Log_Delete WHERE 0=1"
        DT = New DataTable
        DA = New SqlDataAdapter(SQL, BL.ConnectionString)
        DA.Fill(DT)
        Dim DR As DataRow = DT.NewRow
        DR("LD_ID") = BL.GetNewPrimaryID("tb_Log_Delete", "LD_ID")
        DR("Ass_Type") = "ReAss"
        DR("Ass_ID") = Ass_ID
        DR("ReAss_ID") = ReAss_ID
        DR("Ref_Year") = DTReAss.Rows(0).Item("Reass_Ref_Year").ToString()
        DR("Ref_Month") = DTReAss.Rows(0).Item("Reass_Ref_Month").ToString()
        DR("Dept_ID") = DTReAss.Rows(0).Item("Dept_ID").ToString()
        DR("Sub_Dept_ID") = DTReAss.Rows(0).Item("Sub_Dept_ID").ToString()
        DR("Dept_Name") = DTReAss.Rows(0).Item("Dept_Name").ToString()
        DR("Sub_Dept_Name") = DTReAss.Rows(0).Item("Sub_Dept_Name").ToString()
        DR("Ref_Number") = DTReAss.Rows(0).Item("Reass_Ref_Number").ToString()

        DR("S_Name") = DTReAss.Rows(0).Item("S_Name").ToString()

        Dim ReassessmentNumber As String = "R" & DR("Ref_Year") & DR("Ref_Month") & DR("Dept_ID") & DR("Ref_Number")
        Dim Description As String = "ยกเลิกใบประเมินผู้ขายทบทวนเลขที่ " + ReassessmentNumber
        Description &= " โดย" & DTReAss.Rows(0).Item("Dept_Name").ToString() & " ฝ่าย" & DTReAss.Rows(0).Item("Sub_Dept_Name").ToString()

        If Not IsDBNull(DTReAss.Rows(0).Item("Item_Name")) Or DTReAss.Rows(0).Item("Item_Name").ToString() <> "" Then
            Select Case Cat_Group_ID
                Case 1
                    Description &= " สินค้า/พัสดุครุภัณฑ์ : " & DTReAss.Rows(0).Item("Item_Name").ToString()
                Case 2
                    Description &= " จัดจ้าง/บริการ : " & DTReAss.Rows(0).Item("Item_Name").ToString()
            End Select
            DR("Type_ID") = DTReAss.Rows(0).Item("Type_ID").ToString()
            DR("Item_Name") = DTReAss.Rows(0).Item("Item_Name").ToString()

        Else
            DR("Type_ID") = DBNull.Value
            DR("Item_Name") = DTReAss.Rows(0).Item("Item_Name").ToString()

        End If

        If Not IsDBNull(DTReAss.Rows(0).Item("S_ID")) Or DTReAss.Rows(0).Item("S_ID").ToString() <> "" Then
            Description &= " ของ" & DTReAss.Rows(0).Item("S_Name").ToString()
            DR("S_ID") = DTReAss.Rows(0).Item("S_ID")

        Else
            DR("S_ID") = DBNull.Value
        End If

        DR("LD_Comment") = txt_DEL_Comment.Text
        DR("LD_Description") = Description
        DR("Delete_By") = User_ID
        DR("Delete_Time") = Now
        DT.Rows.Add(DR)
        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)

        ' ตรวจสอบเคยได้เลข AVL หรือไม่ tb_AVL_List
        Dim Comm As New SqlCommand
        Dim Conn As New SqlConnection(BL.ConnectionString)
        Conn.Open()
        With Comm
            .Connection = Conn
            .CommandType = CommandType.Text
            .CommandText = "DELETE FROM tb_AVL_List WHERE Ass_ID=" & Ass_ID & " AND ReAss_ID=" & ReAss_ID
            .ExecuteNonQuery()
            .Dispose()
        End With

        ' ลบรายละเอียด   tb_ReAss_Detail
        With Comm
            .Connection = Conn
            .CommandType = CommandType.Text
            .CommandText = "DELETE FROM  tb_ReAss_Detail WHERE  ReAss_ID=" & ReAss_ID
            .ExecuteNonQuery()
            .Dispose()
        End With

        ' ลบใบประเมิน  tb_ReAss_Header
        With Comm
            .Connection = Conn
            .CommandType = CommandType.Text
            .CommandText = "DELETE FROM  tb_ReAss_Header WHERE  ReAss_ID=" & ReAss_ID
            .ExecuteNonQuery()
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()
        '-----tb_Log_Delete----

        Response.Redirect("Reassessment_List.aspx")
    End Sub

    Private Sub SaveWorkflow(sender As Object, e As System.EventArgs) Handles btn_Send_Creater.Click, btn_Send_Auditor.Click, btn_Send_Officer.Click, btn_Send_Director.Click, btn_Send_Complete.Click
        Dim btn As Button = sender

        Dim AT As DataTable = ReassessmentData()
        Dim ToStep As Integer = 0
        Select Case btn.ID.ToString()
            Case "btn_Send_Creater"
                ToStep = AVLBL.AssessmentStep.Creater
            Case "btn_Send_Auditor"
                ToStep = AVLBL.AssessmentStep.Auditor
            Case "btn_Send_Officer"
                ToStep = AVLBL.AssessmentStep.Supply_Officer
            Case "btn_Send_Director"
                ToStep = AVLBL.AssessmentStep.Director
            Case "btn_Send_Complete"
                ToStep = AVLBL.AssessmentStep.Completed
        End Select

        If Current_Step <= ToStep Then

            '------- Validate First------------
            If ddl_Dept.SelectedIndex < 1 Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "validate", "ShowAlert('warning','เลือก ฝ่าย/ภาค/งาน','');", True)
                Exit Sub
            End If
            If Type_ID = 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "validate", "ShowAlert('warning','เลือก " & Title_Item_Group & "','');", True)
                Exit Sub
            End If
            If Trim(Type_Name) = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "validate", "ShowAlert('warning','เลือก  ชื่อ" & Title_Item_Group & "','');", True)
                Exit Sub
            End If
            If Trim(txt_Item_Name.Text) = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "validate", "ShowAlert('warning','กรอก  ชื่อ" & Title_Item_Group & "','');", True)
                Exit Sub
            End If
            If S_ID = 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "validate", "ShowAlert('warning','เลือก ผู้ขาย / ผู้ให้บริการ','');", True)
                Exit Sub
            End If
            If Trim(txt_S_Fullname.Text) = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "validate", "ShowAlert('warning','กรอกชื่อผู้ขาย / ผู้ให้บริการ','');", True)
                Exit Sub
            End If
            If S_Type_ID = 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "validate", "ShowAlert('warning','เลือก ประเภทผู้ขาย / ผู้ให้บริการ','');", True)
                Exit Sub
            End If

            AT.DefaultView.RowFilter = "Get_Score IS NULL OR Get_Score=0 AND ckConfirm=0 AND AR_ID=" & Current_Step

            If AT.DefaultView.Count > 0 And Current_Step = ReassessmentRole Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "validate", "ShowAlert('warning','check ยืนยันคะแนนประเมินเท่ากับ 0','');", True)
                Exit Sub
            End If

        End If

        Save_Role_Action()
        SaveReassessment()

        Dim Get_Score As Object = AT.Compute("SUM(Get_Score)", "")
        Dim Pass_Score As Object = AT.Compute("SUM(Pass_Score)", "")


        '------เริ่ม update Status 0 ก่อน 
        If btn.ID.Replace("btn_Send_", "").ToUpper = "COMPLETE" Then
            BL.DisableAVLStatus(Ass_ID, User_ID, AVLBL.AssessmentType.ReAssessment, ReAss_ID) '---------ปรับสถานะ disable AVL เดิม
        End If

        '------- Save end-time and send to next step------------
        Dim SQL As String = "SELECT ReAss_ID,Ass_ID,Role_1_End,Role_1_Active,Role_2_End,Role_2_Active,Role_3_End,Role_3_Active,Role_4_End,Role_4_Active,Current_Step,Pass_Status,Flag_Return,Update_By,Update_Time" & vbLf
        SQL &= "FROM tb_ReAss_Header WHERE Ass_ID=" & Ass_ID & " AND ReAss_ID=" & ReAss_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        DT.Rows(0).Item("ReAss_ID") = ReAss_ID
        Dim FromStep = CInt(ReassessmentRole)
        Select Case ReassessmentRole
            Case AVLBL.ReassessmentRole.Creater
                DT.Rows(0).Item("Role_1_End") = Now
                DT.Rows(0).Item("Role_2_Active") = False
            Case AVLBL.ReassessmentRole.Auditor
                DT.Rows(0).Item("Role_2_End") = Now
                DT.Rows(0).Item("Role_3_Active") = False
            Case AVLBL.ReassessmentRole.Supply_Officer
                DT.Rows(0).Item("Role_3_End") = Now
                DT.Rows(0).Item("Role_4_Active") = False
            Case AVLBL.ReassessmentRole.Director
                DT.Rows(0).Item("Role_4_End") = Now
        End Select

        Dim SuccessMessage As String = ""
        'Dim ToRemoveAVL As Boolean = False
        Select Case btn.ID.Replace("btn_Send_", "").ToUpper
            Case "CREATER"
                DT.Rows(0).Item("Current_Step") = AVLBL.ReassessmentStep.Creater
                SuccessMessage = "ระบบได้ทำการส่งใบประเมินทบทวนไปยัง \""ผู้ประเมิน\"" แล้ว"
            Case "AUDITOR"
                DT.Rows(0).Item("Current_Step") = AVLBL.ReassessmentStep.Auditor
                SuccessMessage = "ระบบได้ทำการส่งใบประเมินทบทวนไปยัง \""หัวหน้าฝ่าย/ภาค/งาน\"" แล้ว"
            Case "OFFICER"
                DT.Rows(0).Item("Current_Step") = AVLBL.ReassessmentStep.Supply_Officer
                SuccessMessage = "ระบบได้ทำการส่งใบประเมินทบทวนไปยัง \""เจ้าหน้าที่พัสดุ\"" แล้ว"
            Case "DIRECTOR"
                DT.Rows(0).Item("Current_Step") = AVLBL.ReassessmentStep.Director
                SuccessMessage = "ระบบได้ทำการส่งใบประเมินทบทวนไปยัง \""หัวหน้าฝ่ายบริหาร\"" แล้ว"
            Case "COMPLETE"
                DT.Rows(0).Item("Current_Step") = AVLBL.ReassessmentStep.Completed
                SuccessMessage = "การประเมินทบทวนเสร็จสมบูรณ์"

        End Select
        SuccessMessage &= "\n\nคุณสามารถตรวจสอบและสืบค้นข้อมูลการประเมินย้อนหลังได้จากเมนู \""รายงาน\"" และ\""การประเมินผู้ขายใหม่\"""
        If Current_Step <= ToStep Then
            DT.Rows(0).Item("Flag_Return") = False
        Else
            DT.Rows(0).Item("Flag_Return") = True
        End If
        DT.Rows(0).Item("Pass_Status") = IsPass_Status

        DT.Rows(0).Item("Update_By") = User_ID
        DT.Rows(0).Item("Update_Time") = Now
        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)

        '-----------------------START-----UPDATE AVL TO tb_Header--------------------
        If btn.ID.Replace("btn_Send_", "").ToUpper = "COMPLETE" Then
            If IsPass_Status And Get_Score >= Pass_Score Then '---------- Pass-------------
                Dim AVL As AVLBL.AVLCode = BL.GetAVLNoFromNewAssessment(Ass_ID, AVLBL.AssessmentType.ReAssessment, ReAss_ID)
                If AVL.AVL_Y = "" Or AVL.AVL_M = "" Or AVL.AVL_Dept_ID = "" Or AVL.AVL_Sub_Dept_ID = "" Or AVL.AVL_No = "" Then
                    '--------------ยังไม่เคยได้ AVL----สร้างใน TB_AVL_List
                    Dim Y As String = ""
                    If Now.Year < 2300 Then
                        Y = Now.Year
                    Else
                        Y = Now.Year
                    End If
                    Y = Y.Substring(2)
                    Dim M As String = Now.Month.ToString.PadLeft(2, "0")
                    AVL = BL.GetNewAVLNo(Y, M, Dept_ID, Sub_Dept_ID)
                    BL.SetAVLNoForNewAssessment(Ass_ID, AVL, User_ID, AVLBL.AssessmentType.ReAssessment, ReAss_ID) '------อัพเดท AVL ในtb_Header และ สร้าง AVL ใน tb_AVL_List

                Else
                    BL.UpdateAVLStatus(AVL, 1, User_ID)
                End If
                SuccessMessage &= "\nผู้ขายรายนี้ได้เลข AVL : " & AVL.AVL_Code
            End If
        End If

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "validate", "ShowAlert('success','" & SuccessMessage & "','Reassessment_Edit.aspx?Ass_ID=" & Ass_ID & "&ReAss_ID=" & ReAss_ID & "&From=ReAss" & "');", True)

        BindWorkflow()
        BindCommentUser()
    End Sub

#End Region

    Private Sub SaveHeaderReass()

        ReAss_ID = BL.GetNewPrimaryID("tb_ReAss_Header", "ReAss_ID")
        '--------------- Save Header--------------------
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter("SELECT * FROM tb_ReAss_Header WHERE 0=1 ", BL.ConnectionString)
        DA.Fill(DT)

        Dim DR As DataRow = DT.NewRow
        'DR = DT.Rows(0)
        DR("Ass_ID") = Ass_ID
        DR("ReAss_ID") = ReAss_ID
        DR("Ref_Year") = (Now.Year + 543).ToString.Substring(2)
        DR("Ref_Month") = Now.Month.ToString.PadLeft(2, "0")
        DR("Dept_ID") = Dept_ID
        DR("Sub_Dept_ID") = Sub_Dept_ID
        DR("Ref_Number") = GetNewRefNumber(DR("Ref_Year"), DR("Ref_Month"))
        DR("Dept_Name") = Dept_Name
        DR("Sub_Dept_Name") = Sub_Dept_Name
        DR("Role_1_ID") = User_ID
        DR("Role_1_Title") = Session("Title")
        DR("Role_1_Fisrt_Name") = Session("Fisrt_Name")
        DR("Role_1_Last_Name") = Session("Last_Name")
        DR("Role_1_Pos_Name") = Session("Pos_Name")
        DR("Role_1_Start") = Now

        DR("Role_1_Active") = True
        DR("Role_2_Active") = False
        DR("Role_3_Active") = False
        DR("Role_4_Active") = False

        DR("Current_Step") = AVLBL.ReassessmentStep.Creater
        DR("Flag_Return") = False
        DR("Pass_Status") = False
        DR("Update_By") = User_ID
        DR("Update_Time") = Now

        DT.Rows.Add(DR)
        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)
        Dim ReassessmentNumber As String = "R" & DR("Ref_Year") & DR("Ref_Month") & DR("Ref_Number")

        '--------------- Save Questionaire--------------------
        Dim QT As DataTable = BL.GetQuestionsByCurrentSetting(AVLBL.AssessmentType.ReAssessment)
        Dim Sql As String = "SELECT * FROM tb_ReAss_Detail WHERE 1=0"
        DT = New DataTable
        DA = New SqlDataAdapter(Sql, BL.ConnectionString)
        DA.Fill(DT)

        For i As Integer = 0 To QT.Rows.Count - 1
            Dim Q As DataRow = DT.NewRow
            Q("ReAss_ID") = ReAss_ID
            Q("Q_GP_ID") = QT.Rows(i).Item("Q_GP_ID")
            Q("Q_ID") = QT.Rows(i).Item("Q_ID")
            'Q("AR_ID") = AVLBL.AssessmentRole.Creater
            Q("Q_GP_Order") = QT.Rows(i).Item("Q_GP_Order")
            Q("Q_Order") = QT.Rows(i).Item("Q_Order")
            Q("Q_GP_Name") = QT.Rows(i).Item("Q_GP_Name")
            Q("Q_Name") = QT.Rows(i).Item("Q_Name")
            Q("Max_Score") = QT.Rows(i).Item("Max_Score")
            Q("Pass_Score") = QT.Rows(i).Item("Pass_Score")
            Q("Get_Score") = 0
            '--------สิทธิ์การประเมินแต่ละข้อ--------
            Q("AR_ID") = QT.Rows(i).Item("AR_ID")
            Q("ckConfirm") = 0

            Q("Q_Comment") = ""
            DT.Rows.Add(Q)
        Next
        cmd = New SqlCommandBuilder(DA)
        DA.Update(DT)
        '---------- Update Header Summary Score--------
        BL.UpdateHeaderScore_ReAssessment(ReAss_ID)
        cmd.Dispose()
        DA.Dispose()
        DT.Dispose()

        Response.Redirect("Reassessment_Edit.aspx?Ass_ID=" & Ass_ID & "&ReAss_ID=" & ReAss_ID)

    End Sub

    Private Function GetNewRefNumber(ByVal YY As String, ByVal MM As String) As String
        Dim SQL As String = "SELECT ISNULL(MAX(CAST(Ref_Number AS INT)),0)+1 "
        SQL &= " FROM  tb_ReAss_Header "
        SQL &= " WHERE Ref_Year='" & YY.Replace("'", "''") & "' AND Ref_Month='" & MM.Replace("'", "''") & "'"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0).ToString.PadLeft(5, "0")
    End Function


    Protected Sub btnBack_Click(sender As Object, e As System.EventArgs) Handles btnBack1.Click, btnBack2.Click
        Response.Redirect("Reassessment_List.aspx?Back=Y")
    End Sub


    'Protected Sub btnRef_Click(sender As Object, e As System.EventArgs) Handles btnRef.Click

    'End Sub

#Region "PrintButton"

    Protected Sub btnPDF_Click(sender As Object, e As System.EventArgs) Handles btnPDF.Click
        Session("Reassessment_Edit_Conditions") = lblCriterion.Text
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/RPT_Reass.aspx?Mode=PDF" & "&Ass_ID=" & Ass_ID & "&ReAss_ID=" & ReAss_ID & "');", True)

    End Sub
    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        Session("Reassessment_Edit_Conditions") = lblCriterion.Text
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/RPT_Reass.aspx?Mode=EXCEL" & "&Ass_ID=" & Ass_ID & "&ReAss_ID=" & ReAss_ID & "');", True)

    End Sub
#End Region

    Protected Sub btnSaveAll_Click(sender As Object, e As System.EventArgs) Handles btnSaveAll.Click
        Page.MaintainScrollPositionOnPostBack = True
        SaveComment()
        checkComplete()
        SaveReassessment()
        SavePass_Status()
        Try
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "validate", "ShowAlert('success','" & "บันทึกเรียบร้อย" & "','Reassessment_Edit.aspx?Ass_ID=" & Ass_ID & "&ReAss_ID=" & ReAss_ID & "&From=ReAss" & "');", True)
            BindWorkflow()
            BindCommentUser()

            Exit Sub
        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "validate", "ShowAlert('warning','" & "ไม่สามารถบันทึกได้" & "','Reassessment_Edit.aspx?Ass_ID=" & Ass_ID & "&ReAss_ID=" & ReAss_ID & "&From=ReAss" & "');", True)

            Exit Sub
        End Try
    End Sub

    '----ตรวจสอบ Result การประเมิน----------
    Private Sub SavePass_Status()
        Save_Role_Action()
        Dim SQL As String = "SELECT * FROM tb_ReAss_Header WHERE ReAss_ID=" & ReAss_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        DT.Rows(0).Item("Pass_Status") = IsPass_Status

        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)
        '---------- Update Header Summary Score--------
        BL.UpdateHeaderScore_Assessment(Ass_ID)
        checkComplete()

    End Sub

    Private Sub Save_Role_Action()

        '--------update Herader--------
        '--------------- ดูว่า สถานะของการประเมินตรงกับผู้ใช้คนปัจจุบันหรือไม่ ---------------------
        Dim DA_H As New SqlDataAdapter("SELECT * FROM tb_ReAss_Header WHERE Ass_ID=" & Ass_ID & " AND ReAss_ID=" & ReAss_ID, BL.ConnectionString)
        Dim DT_H As New DataTable
        DA_H.Fill(DT_H)

        Select Case Current_Step '----------- สถานะใบประเมิน ----------
            Case AVLBL.ReassessmentStep.Creater
                If ReassessmentRole = AVLBL.ReassessmentRole.Creater Then '----------- สิทธิ์ผู้ใช้ตรงกับใบประเมินหรือไม่ ----------
                    '--------------- Save start-time if not saved yet -------------
                    DT_H.Rows(0).Item("Role_1_ID") = Session("User_ID")
                    DT_H.Rows(0).Item("Role_1_Title") = Session("Title")
                    DT_H.Rows(0).Item("Role_1_Fisrt_Name") = Session("Fisrt_Name")
                    DT_H.Rows(0).Item("Role_1_Last_Name") = Session("Last_Name")
                    DT_H.Rows(0).Item("Role_1_Pos_Name") = Session("Pos_Name")
                    If IsDBNull(DT_H.Rows(0).Item("Role_1_Start")) Then
                        DT_H.Rows(0).Item("Role_1_Start") = Now
                    End If
                    DT_H.Rows(0).Item("Role_1_Active") = True     '------------------เริ่มดำเนินการประเมิน ตามสิทธิ์ผู้ใช้----------
                    DT_H.Rows(0).Item("Update_By") = User_ID
                    DT_H.Rows(0).Item("Update_Time") = Now
                    Dim cmd_H As New SqlCommandBuilder(DA_H)
                    DA_H.Update(DT_H)
                End If

            Case AVLBL.ReassessmentStep.Auditor
                If ReassessmentRole = AVLBL.ReassessmentRole.Auditor Then '----------- สิทธิ์ผู้ใช้ตรงกับใบประเมินหรือไม่ ----------
                    '--------------- Save start-time if not saved yet -------------
                    DT_H.Rows(0).Item("Role_2_ID") = Session("User_ID")
                    DT_H.Rows(0).Item("Role_2_Title") = Session("Title")
                    DT_H.Rows(0).Item("Role_2_Fisrt_Name") = Session("Fisrt_Name")
                    DT_H.Rows(0).Item("Role_2_Last_Name") = Session("Last_Name")
                    DT_H.Rows(0).Item("Role_2_Pos_Name") = Session("Pos_Name")
                    If IsDBNull(DT_H.Rows(0).Item("Role_2_Start")) Then
                        DT_H.Rows(0).Item("Role_2_Start") = Now
                    End If

                    DT_H.Rows(0).Item("Role_2_Active") = True     '------------------เริ่มดำเนินการประเมิน ตามสิทธิ์ผู้ใช้----------
                    DT_H.Rows(0).Item("Update_By") = User_ID
                    DT_H.Rows(0).Item("Update_Time") = Now
                    Dim cmd_H As New SqlCommandBuilder(DA_H)
                    DA_H.Update(DT_H)
                End If

            Case AVLBL.ReassessmentStep.Supply_Officer

                If ReassessmentRole = AVLBL.ReassessmentRole.Supply_Officer Then '----------- สิทธิ์ผู้ใช้ตรงกับใบประเมินหรือไม่ ----------
                    '--------------- Save start-time if not saved yet -------------
                    DT_H.Rows(0).Item("Role_3_ID") = Session("User_ID")
                    DT_H.Rows(0).Item("Role_3_Title") = Session("Title")
                    DT_H.Rows(0).Item("Role_3_Fisrt_Name") = Session("Fisrt_Name")
                    DT_H.Rows(0).Item("Role_3_Last_Name") = Session("Last_Name")
                    DT_H.Rows(0).Item("Role_3_Pos_Name") = Session("Pos_Name")
                    If IsDBNull(DT_H.Rows(0).Item("Role_3_Start")) Then
                        DT_H.Rows(0).Item("Role_3_Start") = Now
                    End If
                    DT_H.Rows(0).Item("Role_3_Active") = True     '------------------เริ่มดำเนินการประเมิน ตามสิทธิ์ผู้ใช้----------
                    DT_H.Rows(0).Item("Update_By") = User_ID
                    DT_H.Rows(0).Item("Update_Time") = Now
                    Dim cmd_H As New SqlCommandBuilder(DA_H)
                    DA_H.Update(DT_H)
                End If

            Case AVLBL.ReassessmentStep.Director
                If ReassessmentRole = AVLBL.ReassessmentRole.Director Then '----------- สิทธิ์ผู้ใช้ตรงกับใบประเมินหรือไม่ ----------
                    '--------------- Save start-time if not saved yet -------------
                    DT_H.Rows(0).Item("Role_4_ID") = Session("User_ID")
                    DT_H.Rows(0).Item("Role_4_Title") = Session("Title")
                    DT_H.Rows(0).Item("Role_4_Fisrt_Name") = Session("Fisrt_Name")
                    DT_H.Rows(0).Item("Role_4_Last_Name") = Session("Last_Name")
                    DT_H.Rows(0).Item("Role_4_Pos_Name") = Session("Pos_Name")
                    If IsDBNull(DT_H.Rows(0).Item("Role_4_Start")) Then
                        DT_H.Rows(0).Item("Role_4_Start") = Now
                    End If
                    DT_H.Rows(0).Item("Role_4_Active") = True     '------------------เริ่มดำเนินการประเมิน ตามสิทธิ์ผู้ใช้----------
                    DT_H.Rows(0).Item("Update_By") = User_ID
                    DT_H.Rows(0).Item("Update_Time") = Now
                    Dim cmd_H As New SqlCommandBuilder(DA_H)
                    DA_H.Update(DT_H)
                End If
            Case AVLBL.ReassessmentStep.Completed

        End Select

    End Sub

    Protected Sub btnClose_DEL_Click(sender As Object, e As System.EventArgs) Handles btnClose_DEL.Click
        ModalComment.Visible = False
    End Sub

    Protected Sub btn_Send_Del2_Click(sender As Object, e As System.EventArgs) Handles btn_Send_Del2.Click
        ModalComment.Visible = True
    End Sub
End Class
