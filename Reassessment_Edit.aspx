﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Reassessment_Edit.aspx.vb" Inherits="Reassessment_Edit" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">


    <link href="assets/css/pages/timeline.css" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css" rel="stylesheet"/>
    <link href="assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/chosen-bootstrap/chosen/chosen.css" rel="stylesheet" type="text/css"/>
    
    <style type="text/css">
           .media-body ,.media-indent
           {
               padding-left:30px;
               }
           .ui-widget-header
           {
               background:#ececec;
               cursor:pointer;
               }
               
           .ui-slider .ui-slider-handle
           {
               height:0.6em; cursor:pointer;
               }
           .ui-slider-horizontal .ui-slider-handle
           {
               top:auto;
               }
           .disabled,slider {
	            opacity: 0.5;
                pointer-events:none;
            }    
           .media-heading,slider
           {
               margin-top:-3px;
               }
           .score
           {
               font-size:larger;
               /*margin-top:-10px;
               margin-left:10px;*/
               position:relative;
               left:10px; top:-5px; 
               text-align:right;      
               }
            
            .gridResult tr td
            {
                    background-color:white;
                    cursor:pointer;
                }
            
            .gridResult tr:hover td
            {
                    background-color:#ccccff;     
                }
             .img_Default
             {
                 color:Green;
                 font-size:20px;
                 position:absolute;
                 width:25px;
                 height:20px;
                 background-color:White;
                 padding-right:-3px;
                 padding-bottom:2px;
                 padding-top:2px;
                 }
            .ckConfirm
           {
               position:relative;
               top:-5px; 
               text-align:right;      
               }
    </style>
    <link href="style/style.css" rel="stylesheet" type="text/css"/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ToolkitScriptManager ID="aJaxToolkit" runat="server"></asp:ToolkitScriptManager>
    <div class="container-fluid">
       <!-- BEGIN PAGE HEADER-->
       <div class="row-fluid">
			<div class="span12"> 
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					ประเมินทบทวนผู้ขาย
                    <asp:LinkButton CssClass="btn blue pull-right" ID="btnBack1" runat="server" style="margin-right:30px"><i class="icon-circle-arrow-left"></i> กลับไปดูการประเมินทั้งหมด</asp:LinkButton>
                </h3>			
				<ul class="breadcrumb">                           
                    <li>
                        <i class="icon-ok-sign"></i><a href="javascript:;">การประเมิน</a><i class="icon-angle-right"></i>
                    </li>
                    <li><i class="icon-ok-sign"></i> <a href="javascript:;">ประเมินทบทวนผู้ขาย</a></li>
                        	
                    <asp:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                    
                </ul>
				<!-- END PAGE TITLE & BREADCRUMB-->	
			</div>				
		</div>
   <div class="row-fluid">
        <div class="span10">
             <asp:LinkButton Enabled ="false" CssClass="btn blue span11 margin-bottom-25 pull-right" ID="btnRef" runat="server"> <asp:Label ID="lblRef" runat="server" Font-Size ="18px"></asp:Label><asp:Label ID="lblResult" runat="server" Font-Size ="18px"></asp:Label></asp:LinkButton>
                   
        </div>
        <div class="span2">
                                    <div class="table-toolbar">                                    
                                         <div class="btn-group">     
                                         </div>
									    <div class="btn-group pull-right">                                    
										    <button data-toggle="dropdown" class="btn dropdown-toggle" id="Button1">พิมพ์ <i class="icon-angle-down"></i></button>										
										    <ul class="dropdown-menu pull-right">
											    <li><asp:LinkButton ID="btnPDF" runat="server">รูปแบบ PDF</asp:LinkButton></li>
											    <li><asp:LinkButton ID="btnExcel" runat="server">รูปแบบ Excel</asp:LinkButton></li>											
										    </ul>
									    </div>
								    </div>
    </div>
</div>

    <div class="row-fluid">
        <div class="span12">

            <asp:UpdatePanel ID="udpAlmost" runat="server">
            <ContentTemplate>
                <ul class="timeline">

                    <li class="timeline-pink">
					    <div id="timeLine_1" runat="server"  class="timeline-icon-red" ><h3 style =" margin-top :3px; font-weight:bold;">1</h3></div>
						<div class="timeline-body">
							<h3 id="h3_Dept" runat="server" visible ="false">สำนักงาน <b><span style =" color :Red ;"> * </span></b><asp:DropDownList ID="ddl_Dept" runat="server" Font-Size="21px" Font-Bold="True" CssClass="m-wrap" style="border:none; width:auto; height:25px; background-color:transparent;" AutoPostBack="True"></asp:DropDownList></h3>
							<b><h3>สำนักงาน :<span style =""> <asp:Label ID="lblDept" runat="server"></asp:Label> </span></h3></b>

						<br />
							<h4 id="h4_SubDept" runat="server" visible ="false">ฝ่าย/ภาค/งาน <b><span style =" color :Red ;"> * </span></b><asp:DropDownList ID="ddl_Sub_Dept" runat="server" Font-Size="21px" Font-Bold="True" CssClass="m-wrap" style="border:none; width:auto; height:25px; background-color:transparent;" AutoPostBack="True"></asp:DropDownList></h4>
							<b><h3>ฝ่าย/ภาค/งาน : <span style =""> <asp:Label ID="lblSubDept" runat="server"></asp:Label> </span></h3></b>

						</div>
					</li>
                    <li class="timeline-lightblue">
                        <div id="timeLine_2" runat="server"  class="timeline-icon-green"><h3 style =" margin-top :3px; font-weight:bold; ">2</h3></div>
							<div class="timeline-body ">
								<%--สินค้า, พัสดุครุภัณฑ์ที่ส่งมอบ--%>
                                <h2><asp:Label id="lblHeader_Item" runat ="server" ></asp:Label>
                                    <asp:Label ID="lbl_Item_Type" runat="server"></asp:Label>
                                    <div class="btn-group pull-right">
                                        <%--<asp:LinkButton CssClass="btn blue" ID="btnProductDialog" runat="server"><i class="icon-barcode"></i> เลือกประเภท</asp:LinkButton> --%>                                   
                                    </div>
                                </h2>
								<div class="timeline-content" >

                                    <asp:Panel ID="pnl_Product" runat="server" CssClass="span12"> 
                                        <ul class="breadcrumb">
							                <li>
								                <i class="icon-barcode"></i>
                                                <asp:Label ID="lbl_Cat" runat="server"></asp:Label>
								                <span class="icon-angle-right"></span>
							                </li>
							                <li>
								                <asp:Label ID="lbl_Sub_Cat" runat="server"></asp:Label>
								                <span class="icon-angle-right"></span>
							                </li>
							                <li>
                                                <asp:Label ID="lbl_Type" runat="server"></asp:Label>                                               
                                                <span class="icon-angle-right"></span></li>
                                            <li>
                                                <asp:Label ID="lbl_Running" runat="server"></asp:Label>
                                            </li>
						                </ul>
                                    </asp:Panel>
                                    
                                    <%--ชื่อพัสดุ/ครุภัณฑ์--%>

 
                                         <asp:Panel ID="pnlItem_NAME" runat ="server" >
                                        <%--ชื่อสินค้าพัสดุ และบริการ--%>
                                        <div class="span12">
                                            <div class="row-fluid form-horizontal">
                                                <div class="control-group">
                                                    <label class="control-label">
                                                    <asp:Label id="lblTitle_Item_Name" runat ="server" ></asp:Label> <b><span style =" color :Red ;">  </span></b></label>
                                                    <div class="controls">
                                                        <asp:TextBox ID="txt_Item_Name" runat="server" AutoPostBack="True" 
                                                            CssClass="m-wrap span11"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlItem_No" runat ="server" >
                                            <div class="span12">
                                                <div class="row-fluid form-horizontal">
                                                    <div class="control-group">
                                                        <label class="control-label">
                                                        <asp:Label id="Label2" runat ="server" Text ="เลขทะเบียนคลัง" ></asp:Label><br /><i ID="lblTagDesc" runat="server">(แต่ละเลขคั่นด้วย , )</i></label>
                                                        <div class="controls">
                                                        
                                                            <asp:TextBox ID="txt_Item_No" runat="server" CssClass="m-wrap  tags span11" 
                                                                style="display: none; height:50px;"></asp:TextBox>
                                                            <asp:Repeater ID="rptTag" runat="server">
                                                                <HeaderTemplate>
                                                                    <ul class="tag_list_readonly">
                                                        
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <li><%# Eval("Item_No") %> </li>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    </ul>
                                                                </FooterTemplate>
                                                            </asp:Repeater>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                                                   
                                </div>

                                <h2>รายละเอียดเกี่ยวกับสินค้า/บริการ (ถ้ามี)</h2>
                                <div class="alert">										
									รายละเอียดเพิ่มเติมเกี่ยวกับสินค้าพัสดุ/ครุภัณฑ์ ช่วยในการสืบค้นประวัติย้อนหลัง
								</div>
                                
                                <div class="row-fluid form-horizontal">
                                    <div class="span6 ">
									    <div class="control-group">
										    <label class="control-label">จำนวน </label>
										    <div class="controls">
										        <asp:TextBox ID="txt_Qty" runat="server" CssClass="span11 m-wrap" AutoPostBack="True"></asp:TextBox>
										    </div>
									    </div>
								    </div>
                                    <div class="span6 ">
									    <div class="control-group">
										    <label class="control-label">หน่วยนับ </label>
										    <div class="controls">
										        <asp:TextBox ID="txt_UOM" runat="server" CssClass="span11 m-wrap" AutoPostBack="True"></asp:TextBox>
										    </div>
									    </div>
								    </div>	
                                </div>
                                <div class="row-fluid form-horizontal">
                                    <div class="span6 ">
									    <div class="control-group">
										    <label class="control-label">Lot </label>
										    <div class="controls">
										        <asp:TextBox ID="txt_Lot" runat="server" CssClass="span11 m-wrap" AutoPostBack="True"></asp:TextBox>
										    </div>
									    </div>
								    </div>
                                    <div class="span6 ">
									    <div class="control-group">
										    <label class="control-label">ยี่ห้อ </label>
										    <div class="controls">
										        <asp:TextBox ID="txt_Brand" runat="server" CssClass="span11 m-wrap" AutoPostBack="True"></asp:TextBox>
										    </div>
									    </div>
								    </div>
                                </div>
                                <div class="row-fluid form-horizontal">
                                    <div class="span6 ">
									    <div class="control-group">
										    <label class="control-label">รุ่น/Model</label>
										    <div class="controls">
										        <asp:TextBox ID="txt_Model" runat="server" CssClass="span11 m-wrap" AutoPostBack="True"></asp:TextBox>
										    </div>
									    </div>
								    </div>
                                    <div class="span6 ">
									    <div class="control-group">
										    <label class="control-label">สี</label>
										    <div class="controls">
										        <asp:TextBox ID="txt_Color" runat="server" CssClass="span11 m-wrap" AutoPostBack="True"></asp:TextBox>
										    </div>
									    </div>
								    </div>
                                 </div>
                                 <div class="row-fluid form-horizontal">
                                    <div class="span6 ">
									    <div class="control-group">
										    <label class="control-label">ขนาด</label>
										    <div class="controls">
										        <asp:TextBox ID="txt_Size" runat="server" CssClass="span11 m-wrap" AutoPostBack="True"></asp:TextBox>
										    </div>
									    </div>
								    </div>
                                    <div class="span6 ">
									    <div class="control-group">
										    <label class="control-label">ราคา</label>
										    <div class="controls">
										        <asp:TextBox ID="txt_Price" runat="server" CssClass="span11 m-wrap" AutoPostBack="True"></asp:TextBox>
										    </div>
									    </div>
								    </div>
                                </div>
                                <div class="row-fluid form-horizontal">
                                    <div class="span6 ">
									    <div class="control-group">
										    <label class="control-label">เอกสารอ้างอิง</label>
										    <div class="controls">
										        <asp:TextBox ID="txt_Ref_Doc" runat="server" CssClass="span11 m-wrap" AutoPostBack="True"></asp:TextBox>
										    </div>
									    </div>
								    </div>
                                    <div class="span6 ">
									    <div class="control-group">
										    <label class="control-label">วันที่ได้มา (dd.MM.yyyy)</label>
										    <div class="controls">
										        <asp:TextBox ID="txt_RecievedDate" runat="server" CssClass="span11 m-wrap" AutoPostBack="True"></asp:TextBox>
										        <asp:CalendarExtender ID="txt_RecievedDate_CalendarExtender" runat="server" Format="dd-MMM-yyyy" BehaviorID="txt_RecievedDate" TargetControlID="txt_RecievedDate">
                                                </asp:CalendarExtender>
										    </div>
									    </div>
								    </div>
                                </div>

                                <h2>ภาพและไฟล์ที่เกียวข้อง(ถ้ามี)
                                    <div class="btn-group pull-right">
                                            <asp:LinkButton CssClass="btn blue" ID="btnAddFile" runat="server"><i class="icon-plus-sign"></i> เพิ่มภาพหรือไฟล์ที่เกี่ยวข้อง</asp:LinkButton>                                    
                                    </div>
                                </h2>
                                
                                <div class="alert">										
									ไฟล์ที่เกียวข้องไม่ว่าจะเป็นสภาพสินค้าที่ส่งมอบ บรรจุภัณฑ์ หรืออื่นๆ เพื่อช่วยประกอบการตัดสินใจในการอนุมัติผลการประเมิน <br>รวมไปถึงการเปรียบเทียบคุณภาพของผู้ขายแต่ละรายในการจัดซื้อครั้งต่อไป
								</div>
                                <div class="row-fluid form-horizontal">
                                    <asp:Repeater ID="rpt_File" runat="server">
                                        <ItemTemplate>
                                             <div class="span3">
										        <div class="item">
											        
                                                    <a ID="lnk_File_Dialog" runat="server" class="fancybox-button" data-rel="fancybox-button" target="_blank" title="Photo">
                                                        
                                                        <div class="zoom">
                                                            <div class="img_Default" id="imgDefault" runat="server">
                                                              <i class="icon-ok-sign"></i>
                                                            </div>
                                                            <asp:Image ID="img_File" runat="server" AlternateText="" />              
													        <div class="zoom-icon"></div>                                                            
												        </div>
                                                    </a>
                                                    <div class="details" id="pnlEditFile" runat="server">
                                                        <asp:LinkButton CssClass="icon" ID="lnk_Space" runat="server" style="visibility:hidden;"><i class="icon-pk"></i></asp:LinkButton>
												        <asp:LinkButton CssClass="icon" ID="lnk_Default" CommandName="Default" runat="server"><i class="icon-ok"></i></asp:LinkButton>
												        <asp:LinkButton CssClass="icon" ID="lnk_Edit" CommandName="Edit" runat="server"><i class="icon-pencil"></i></asp:LinkButton>
												        <asp:LinkButton CssClass="icon" ID="lnk_Delete" CommandName="Delete" runat="server"><i class="icon-remove"></i></asp:LinkButton>
                                                        <asp:ConfirmButtonExtender TargetControlID="lnk_Delete" ID="lnk_Delete_Confirm" runat="server" ConfirmText="ยืนยันลบภาพ?"></asp:ConfirmButtonExtender>   
											        </div>
                                                    <div class="filename">
                                                       <asp:Label ID="lbl_File_Name" runat="server"></asp:Label>
                                                    </div>                                                    
										        </div>
									        </div>   
                                            
                                        </ItemTemplate>
                                    </asp:Repeater>                                                                                                       
                                </div>                                

							</div>
                            
                    </li>
         
                    <li class="timeline-lightblue">
						<div id="timeLine_3" runat="server"  class="timeline-icon-green"><h3 style =" margin-top :3px; font-weight:bold; ">3</h3></div>
                        <div class="timeline-body">
						    <h2>ผู้ขาย / ผู้ให้บริการ
                                <%--<div class="btn-group pull-right">
                                    <asp:LinkButton CssClass="btn blue" ID="btnSupplierDialog" runat="server"><i class="icon-user"></i> เลือกผู้ขาย / ผู้ให้บริการ</asp:LinkButton>                                    
                                </div>--%>
                            </h2> 
							<div class="timeline-content">     
                                    <div class="row-fluid form-horizontal">
										<div class="span6 ">
											<div class="control-group">
												<label class="control-label">ชื่อ</label>
												<div class="controls">																
													<asp:TextBox ID="txt_S_Fullname" runat="server" AutoPostBack="True" 
                                                        CssClass="span11 m-wrap"></asp:TextBox>
                                                </div>
											</div>
										</div>
										<!--/span-->                                       
										<div class="span6 ">
											<div class="control-group">
												<label class="control-label">ประเภท</label>
												<div class="controls">
                                                        <asp:DropDownList ID="ddl_S_Type" runat="server" CssClass="medium m-wrap" AutoPostBack="True"></asp:DropDownList>                                               
												</div>
											</div>
										</div>     
									</div>
                                    <div class="row-fluid form-horizontal">                                        
                                        <div class="span6 ">
											<div class="control-group">
												<label class="control-label">ชื่อย่อ</label>
												<div class="controls">																
													<asp:TextBox ID="txt_S_Alias" runat="server" AutoPostBack="True" 
                                                        CssClass="span11 m-wrap"></asp:TextBox>
                                                </div>
											</div>
										</div>
                                        <div class="span6 ">
											<div class="control-group">
												<label class="control-label">เลขผู้เสียภาษี</label>
												<div class="controls">																
													<asp:Label ID="lbl_S_Tax_No" runat="server" Font-Bold="true" Font-Size="Large" style="top:7px; position:relative;" AutoPostBack="True"></asp:Label>
												</div>
											</div>
										</div>
                                    </div>
                                    <div class="row-fluid form-horizontal">
										<div class="span12 ">
											<div class="control-group">
												<label class="control-label">ที่อยู่</label>
												<div class="controls">												
													<asp:TextBox ID="txt_S_Address" runat="server" CssClass="span11 m-wrap" AutoPostBack="True"></asp:TextBox>
												</div>
											</div>
										</div>
                                    </div>
                                    <div class="row-fluid form-horizontal">
										<!--/span-->
										<div class="span6 ">
											<div class="control-group">
												<label class="control-label">ผู้ติดต่อ </label>
												<div class="controls">
													<asp:TextBox ID="txt_S_Contact_Name" runat="server" CssClass="medium m-wrap" AutoPostBack="True"></asp:TextBox>
												</div>
											</div>
										</div>
										<!--/span-->
                                        <div class="span6 ">
											<div class="control-group">
												<label class="control-label">Email </label>
												<div class="controls">
													<asp:TextBox ID="txt_S_Email" runat="server" CssClass="medium m-wrap" AutoPostBack="True"></asp:TextBox>
												</div>
											</div>
										</div>
									</div>
                                    <div class="row-fluid form-horizontal">
										<div class="span6 ">
											<div class="control-group">
												<label class="control-label">โทรศัพท์</label>
												<div class="controls">																
													<asp:TextBox ID="txt_S_Phone" runat="server" CssClass="medium m-wrap" AutoPostBack="True"></asp:TextBox>
												</div>
											</div>
										</div>
										<!--/span-->
										<div class="span6 ">
											<div class="control-group">
												<label class="control-label">โทรสาร </label>
												<div class="controls">
													<asp:TextBox ID="txt_S_Fax" runat="server" CssClass="medium m-wrap" AutoPostBack="True"></asp:TextBox>
												</div>
											</div>
										</div>
										<!--/span-->
									</div>                                                           
                                </div>
								
						 </div>
					</li>

                    <li class="timeline-pink">
								<div id="timeLine_4" runat="server"  class="timeline-icon-red"><h3 style =" margin-top :3px; font-weight:bold; ">4</h3></div>
								<div class="timeline-body article-block">
                                <asp:Panel ID="pnlMaskAssessment" Visible="false" runat="server" style="width:100%; height:100%; background-color:white; position:absolute; opacity:0; filter: alpha(opacity=0); z-index:10;"></asp:Panel>
									<%--<h2>การประเมินทบทวน</h2>--%>
									<h2>การประเมินทบทวน <b><span style =" color :Red ;">  </span></b> <span  id="lblValidate_Ass" runat="server"  style =" font-size:16px;"> ( ประเมินเฉพาะข้อที่มีเครื่องหมาย <b><span style =" color :red ;">*</span></b> เท่านั้น )</span></h2>
                                    <div class="row-fluid">
					                    <div class="span12 blog-page">
						                    <div class="row-fluid">
                                                <div class="article-block">								
								                  <asp:Repeater ID="rpt_ReAss_Q_Group" runat="server">
                                                    <ItemTemplate>
                                                        <div class="media-body">
                                                             <hr id="hr" runat="server"/>
									                         <h4><asp:Label ID="lbl_Q_GP_No" runat="server"></asp:Label><asp:Label ID="lbl_Q_GP_Name" runat="server"></asp:Label></h4>	                                                       							                    
<%--									                            <asp:Repeater ID="rpt_Ass_Q" runat="server">
                                                                    <ItemTemplate>
                                                                        <div class="media-indent span11">
										                                    <h5 class="media-heading pull-left Span7"><asp:Label ID="lbl_Q_Name" runat="server"></asp:Label></h5>
                                                                            <div class="score pull-right span1">
													                                <asp:Label ID="lbl_Score" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_Score" runat="server" style="display:none;"></asp:TextBox>
												                            </div>
                                                                            <div class="span3 pull-right">
                                                                                <div id="slider" class="slider bg-green" runat="server"></div>												                
                                                                            </div>
										                                </div>  
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
--%>
									                            <asp:Repeater ID="rpt_ReAss_Q" runat="server" >
                                                                    <ItemTemplate>
                                                                        <div class="media-indent span11">
                                                                            <h5 class="media-heading pull-left Span6" style=" width :350px;">
										                                    <asp:Label ID="lbl_Q_Name_Order" runat="server"></asp:Label><asp:Label ID="lbl_Q_Name" runat="server"></asp:Label><b><asp:Label ID="lblReqField" style=" color :Red ; font-style:oblique ; margin-left :2px;" runat="server"> * </asp:Label></b></h5>
                                                                              
                                                                             <div class="ckConfirm pull-right span1"  style=" width :60px;" >
                                                                                <asp:Panel  ID="pnlck" runat ="server"  ><asp:CheckBox id="ckConfirm" runat ="server" ToolTip ="Check ยืนยันประเมิน 0 คะแนน"  /> <asp:Label  ID="lblck" runat ="server" Text ="ยืนยัน" style=" font-size :10px;"></asp:Label>			</asp:Panel> 								                
                                                                            </div>                                                           
                                                                                           
                                                                            <div class="score pull-right span2">
													                                <asp:Label ID="lbl_Score" runat="server"></asp:Label><span  style=" display :inline ; font-size :10px; color :Green;"> เกณฑ์ <asp:Label ID="lbl_Pass_Score" runat="server"></asp:Label></span> 
                                                                                    <asp:TextBox ID="txt_Score" runat="server" style="display:none;"></asp:TextBox>
												                            </div>
                                                                            <div class="span3 pull-right">
                                                                                <asp:Panel  ID="pnlSlider" runat ="server"  ><div id="slider" class="slider bg-green disabled" runat="server"></div></asp:Panel>												                
                                                                            </div>

										                                </div>  
                                                                    </ItemTemplate>
                                                                </asp:Repeater>


								                            </div>
                                                    </ItemTemplate>
                                                  </asp:Repeater>			
							                    </div>							                  
						                    </div>
					                    </div>
                                        <asp:Button ID="btnUpdateScore" runat="Server" style="display:none;" />
									</div>
                                    <hr />
                                     <div class="row-fluid">
                                        <div class="span8" style="text-align:right;">
                                            <h3><asp:Label ID="lbl_Result" runat="server" ForeColor="red"></asp:Label></h3>
                                                                                       
                                        <div class="details">												    
												    <span id="span_Get_Score" runat="server" class="label label-important"><i class="icon-user"></i>ได้ <asp:Label ID="lbl_Get_Score" runat="server"></asp:Label></span>
                                                    <span class="label label-info"><i class="icon-bookmark-empty"></i>เต็ม <asp:Label ID="lbl_Max_Score" runat="server"></asp:Label></span>												    
												    <span class="label label-success"><i class="icon-ok"></i>เกณฑ์ <asp:Label ID="lbl_Pass_Score" runat="server"></asp:Label></span>
											    </div>
                                        </div>
                                        
                                        <div class="span4 responsive" data-tablet="span12 fix-margin" data-desktop="span4">
										    <div class="block">
											    <div class="visual">
												   <asp:TextBox ID="knob" CssClass="knob" runat="server" Text="12" data-width="200" data-fgcolor="#ee0000" data-max="100" data-min="0"></asp:TextBox>
                                               
                                                </div>											    
										    </div>
									    </div>

                                      </div>   
                                      <div class="alert" style =" font-size :11pt;">เงื่อนไขผ่านการประเมิน : ต้องผ่านการประเมินทุกข้อ และผลรวมการประเมินต้องผ่านเกณฑ์</div>                 
                                     
                                      <div id="lblCri" runat="server" visible ="false" class="alert">เงื่อนไขผ่านการประเมิน : <asp:Label ID="lblCriterion" runat ="server" Text ="" ></asp:Label></div>                 
								</div>
							</li>
                           
							<li class="timeline-pink">
								<div class="timeline-icon-blue"><i class="icon-comments"></i></div>
								<div class="timeline-body">
									<h2>หมายเหตุ/ข้อเสนอแนะ (ถ้ามี)</h2>
									<div class="timeline-content">
                                                <div class="row-fluid">
													<div class="span12 ">
														<div class="control-group">
															<label class="control-label"><b>หมายเหตุ ผู้ประเมิน</b> <asp:Label ID="lblCreaterInfo" runat="server"></asp:Label></label>
															<div class="controls">
                                                                <asp:TextBox ID="txt_Comment_Creater" runat="server" TextMode="MultiLine" CssClass="span10" Height="70px" rows="2"  placeholder="หมายเหตุผู้ประเมิน" AutoPostBack="True"></asp:TextBox>
															</div>
														</div>
													</div>
												</div>
                                                <div class="row-fluid">
													<div class="span12 ">
														<div class="control-group">
															<label class="control-label"><b>หมายเหตุ หัวหน้าฝ่าย/ภาค/งาน</b> <asp:Label ID="lblAuditorInfo" runat="server"></asp:Label></label>
															<div class="controls">
                                                                <asp:TextBox ID="txt_Comment_Auditor" runat="server" TextMode="MultiLine" CssClass="span10" Height="70px" rows="2"  placeholder="หมายเหตุหัวหน้าฝ่าย/ภาค/งาน" AutoPostBack="True"></asp:TextBox>
															</div>
														</div>
													</div>
												</div>
                                                <div class="row-fluid">
													<div class="span12 ">
														<div class="control-group">
															<label class="control-label"><b>หมายเหตุ งานจัดซื้อและพัสดุ</b> <asp:Label ID="lblSupplyOfficerInfo" runat="server"></asp:Label></label>
															<div class="controls">
                                                                <asp:TextBox ID="txt_Comment_Officer" runat="server" TextMode="MultiLine" CssClass="span10" Height="70px" rows="2"  placeholder="หมายเหตุงานจัดซื้อและพัสดุ" AutoPostBack="True"></asp:TextBox>
															</div>
														</div>
													</div>
												</div>
                                                <div class="row-fluid">
													<div class="span12 ">
														<div class="control-group">
															<label class="control-label"><b>หมายเหตุ หัวหน้าฝ่ายบริหารทั่วไป</b> <asp:Label ID="lblDirectorInfo" runat="server"></asp:Label></label>
															<div class="controls">
                                                                <asp:TextBox ID="txt_Comment_Director" runat="server" TextMode="MultiLine" CssClass="span10" Height="70px" rows="2"  placeholder="หมายเหตุหัวหน้าฝ่ายบริหารทั่วไป" AutoPostBack="True"></asp:TextBox>
															</div>
														</div>
													</div>
												</div>
                                   
                                    </div>
									<div class="timeline-footer">
										 <div class="alert">
                                         <asp:Label ID="lbl_Status_Helper" runat="server" Text='ขณะนี้การประเมินอยู่ในขั้นตอนของ "ผู้ประเมิน" คุณสามารถคลิกเพื่อส่งใบประเมินไปยัง "หัวหน้าแผนก" ได้'></asp:Label>
                                         </div>
                                   </div>
								</div>                               
							</li>
                </ul>

                <!-- BEGIN STEPS -->
<%--                <asp:LinkButton CssClass="btn green span11 margin-bottom-25 pull-right" ID="btnSaveAll" runat="server"><i class="icon-save"></i> บันทึกใบประเมินทบทวน</asp:LinkButton>
                <asp:ConfirmButtonExtender ID="btn_SentReport" runat="server" ConfirmText="ยืนยันการบันทึกข้อมูลใบประเมินทบทวน ?" TargetControlID="btnSaveAll"></asp:ConfirmButtonExtender>
--%>

                    <div class="row-fluid span11 pull-right" >
                        
                      
                             <asp:LinkButton CssClass="btn green span6 margin-bottom-25 pull-left" ID="btnSaveAll"  runat="server"><i class="icon-save"></i> บันทึกใบประเมิน</asp:LinkButton>
                             <asp:ConfirmButtonExtender ID="btn_SentReport" runat="server" ConfirmText="ยืนยันการบันทึกข้อมูลใบประเมินทบทวน ?" TargetControlID="btnSaveAll"></asp:ConfirmButtonExtender>
                                            
                             <asp:LinkButton  CssClass="btn yellow span6 margin-bottom-25 pull-right" ID="btn_Send_Del2" runat="server"><i class="icon-remove"></i> ยกเลิกใบประเมินทบทวน</asp:LinkButton>
 
                    </div>



                <div class="row-fluid span11 margin-bottom-25 pull-right" >
                    <asp:Panel ID="pnl_Send_Creater" ClientIDMode="Static" runat="server" style="height :160px;"  CssClass="span2 front-steps front-step-one">
                        <h2>ผู้ประเมิน</h2>
                        <p><asp:Label ID="lbl_Creater_Name" runat="server"></asp:Label></p>
                    </asp:Panel>
                    <asp:Panel ID="pnl_Send_Auditor" ClientIDMode="Static" runat="server" style="height :160px;" CssClass="span2 front-steps front-step-two" >
                        <h2>หัวหน้าฝ่าย/ภาค/งาน</h2>
                        <p><asp:Label ID="lbl_Auditor_Name" ClientIDMode="Static" runat="server"></asp:Label></p>
                    </asp:Panel>
                    <asp:Panel ID="pnl_Send_Officer" ClientIDMode="Static" runat="server" style="height :160px;"  CssClass="span2 front-steps front-step-three">
                        <h2>เจ้าหน้าที่พัสดุ</h2>
                        <p><asp:Label ID="lbl_Officer_Name" ClientIDMode="Static" runat="server"></asp:Label></p>
                    </asp:Panel>
                    <asp:Panel ID="pnl_Send_Director" ClientIDMode="Static" runat="server" style="height :160px;"  CssClass="span2 front-steps front-step-four">
                        <h2>หัวหน้าฝ่ายบริหาร</h2>
                        <p><asp:Label ID="lbl_Director_Name" ClientIDMode="Static" runat="server"></asp:Label></p>
                    </asp:Panel>
                    <asp:Panel ID="pnl_Send_Complete" ClientIDMode="Static" runat="server" style="height :160px;"  CssClass="span2 front-steps front-step-five">
                        <h2>เสร็จสมบูรณ์</h2>
                    </asp:Panel>
                    <asp:Panel ID="pnl_Send_Delete" ClientIDMode="Static" runat="server" style="height :160px;"  CssClass="span2 front-steps front-step-del">
                        <h2>ยกเลิก</h2>
                    </asp:Panel>
                    <asp:Button ID="btn_Send_Creater" ClientIDMode="Static" runat="server" style="display:none;" />
                    <asp:Button ID="btn_Send_Auditor" ClientIDMode="Static" runat="server"  style="display:none;" />
                    <asp:Button ID="btn_Send_Officer" ClientIDMode="Static" runat="server" style="display:none;" />
                    <asp:Button ID="btn_Send_Director" ClientIDMode="Static" runat="server" style="display:none;" />
                    <asp:Button ID="btn_Send_Complete" ClientIDMode="Static" runat="server" style="display:none;" />
                    <asp:Button ID="btn_Send_Del" ClientIDMode="Static" runat="server" style="display:none;" />
                    <asp:Label ID="lblRole_Active" runat="server" style="display:none;"></asp:Label>
                    <asp:Label ID="lblCat_Group_ID" runat="server" style="display:none;"></asp:Label>

                </div>
                <!-- END STEPS --> 
                
<%--                <asp:LinkButton CssClass="btn green span6 margin-bottom-25 pull-right" ID="btnSaveAll" runat="server"><i class="icon-save"></i> บันทึกใบประเมินทบทวน</asp:LinkButton>
                <asp:ConfirmButtonExtender ID="btn_SentReport" runat="server" ConfirmText="ยืนยันการบันทึกข้อมูลใบประเมินทบทวน ?" TargetControlID="btnSaveAll"></asp:ConfirmButtonExtender>
--%>

                <asp:LinkButton CssClass="btn blue span11 margin-bottom-25 pull-right" ID="btnBack2" runat="server"><i class="icon-circle-arrow-left"></i> กลับไปดูการประเมินทั้งหมด</asp:LinkButton>
            </ContentTemplate>
            </asp:UpdatePanel>
        </div>

            

             <asp:UpdatePanel ID="udpUploadFile" runat="server">
                <ContentTemplate>
                    <asp:Panel CssClass="modal-scrollable" id="dialogUploadFile" runat="server" DefaultButton="btnSaveFile" Visible="false">
                        <div class="modal fade in modal-overflow" style="display: block; margin-top: 20px; width:600px;" id="dialogFileContainer" runat="server">
				        <div class="modal-header">
					        <h3><i class="icon-file"></i> เลือกไฟล์เพื่ออัพโหลด</h3>
				        </div>
				        <div class="modal-body">      
                                                                                    
                            <asp:Panel CssClass="row-fluid" id="pnlEditFile" runat="server">
                                <div class="span6">
                                    <h5><i class="icon-ok-sign"></i> ไฟล์ปัจจุบัน</h5>
                                    <asp:Image ID="fileOld" runat="server" CssClass="span12"  ToolTip="ไฟล์เดิม" />
                                </div>
                                <div class="span6">
                                    <h5><i class="icon-folder-open"></i> ไฟล์ใหม่</h5>
                                    <asp:Image ID="fileNew" runat="server" CssClass="span12" onClick="browseFile();"  style="cursor:pointer;" ToolTip="ไฟล์ที่คุณอัพโหดเข้าไปใหม่(Click เพื่ออัพโหลด)" />
                                </div>                                
                            </asp:Panel>

                            <asp:Panel CssClass="row-fluid" id="pnlAddFile" runat="server">
                                <div class="span12">
                                    <asp:Image ID="fileAdd" runat="server" onClick="browseFile();" CssClass="span12" style="cursor:pointer;" ToolTip="ไฟล์ที่คุณอัพโหดเข้าไปใหม่ (Click เพื่ออัพโหลด)" />
                                </div>
                            </asp:Panel>
                             <div class="alert">ระบบรองรับไฟล์ png, jpg, jpeg, gif และ pdf เท่านั้น</div>
                             <div class="span12">
                                    <asp:TextBox ID="txtFileName" runat="server" CssClass="m-wrap span10" BackColor="white" placeholder="กรอกรายละเอียด" ToolTip="กรอกรายละเอียด"></asp:TextBox>
								    <asp:Button ID="btnSaveFile" runat="server" CssClass="btn blue span2" Text="บันทึก" />
                                    <asp:FileUpload ID="ful" runat="server" style="display:none;" onchange="asyncUpload();" />
                                    <asp:Button ID="btnUpdateFile" runat="server" style="display:none;" />
							 </div>

				        </div>				                
                        <asp:LinkButton CssClass="fancybox-item fancybox-close" ToolTip="ปิด" id="lnkCloseUpload" runat="server"></asp:LinkButton>
                            
			            </div>
                    <div class="modal-backdrop" style="z-index: 10049;"></div>
                    </asp:Panel>
                </ContentTemplate>
             </asp:UpdatePanel>


             <asp:UpdatePanel ID="udpUploadComment" runat="server">
                <ContentTemplate>
                    <asp:Panel CssClass="modal" style="top:20%; width:400px; position:fixed;" id="ModalComment" runat="server" Visible="false" >
									<div class="modal-header">										
										<h3>เพิ่มหมายเหตุการยกเลิกใบประเมินทบทวน</h3>
									</div>
									<div class="modal-body">
										<div style="height:120px">
											<div class="row-fluid">
											        <asp:TextBox TextMode="MultiLine" ID="txt_DEL_Comment" runat="server"
											         CssClass="m-wrap" Width="95%" Height="110px"></asp:TextBox>														       
											</div>
										</div>
									</div>
									<div class="modal-footer">
									    <asp:Button ID="btnOK_DEL" runat="server" CssClass="btn blue" Text="บันทึก"  />  
                                        <asp:ConfirmButtonExtender ID="Confirm_btnOK_DEL" runat="server" ConfirmText="ยืนยันลบใบประเมิน ?" TargetControlID="btnOK_DEL"></asp:ConfirmButtonExtender>


									    <asp:Button ID="btnClose_DEL"  runat="server" CssClass="btn" Text="ยกเลิก" />																		
									</div>
			  </asp:Panel>
                </ContentTemplate>
             </asp:UpdatePanel>


    </div>          
      
    <style type="text/css" id="css_On_Creator" runat="server" visible="false">
        .front-steps.front-step-one:after
        {
            border-left-color:#36b3e7;
            }
            .front-steps.front-step-one
        {
            background-color:#0da3e2;
            }            
    </style>

        
    <style type="text/css" id="css_On_Auditor" runat="server" visible="false">
        .front-steps.front-step-two:after
        {
            border-left-color:#36b3e7;
            }
            .front-steps.front-step-two
        {
            background-color:#0da3e2;
            }            
    </style>
        
    <style type="text/css" id="css_On_Officer" runat="server" visible="false">
        .front-steps.front-step-three:after
        {
            border-left-color:#36b3e7;
            }
            .front-steps.front-step-three
        {
            background-color:#0da3e2;
            }            
    </style>
        
    <style type="text/css" id="css_On_Director" runat="server" visible="false">
            .front-steps.front-step-four
        {
            background-color:#0da3e2;
            }            
    </style>
        
    <style type="text/css" id="css_On_Complete" runat="server" visible="false">
            .front-steps.front-step-five
        {
            background-color:#0da3e2;
            }            
    </style>
    <style type="text/css" id="css_On_Del" runat="server" visible="false">
            .front-steps.front-step-del
        {
            background-color:#0da3e2;
            }            
    </style>     
    
    <style type="text/css" id="css_Can_Creator" runat="server" visible="false">
        .front-steps.front-step-one:hover:after
        {
            cursor:pointer;
            border-left-color:#0099cc;
            }
            .front-steps.front-step-one:hover
        {
            cursor:pointer;
            background-color:#0099cc;
            }            
    </style>
    
    <style type="text/css" id="css_Can_Auditor" runat="server" visible="false">
            .front-steps.front-step-two:hover:after
            {
                cursor:pointer;
                border-left-color:#0099cc;
                }
                .front-steps.front-step-two:hover
            {
                cursor:pointer;
                background-color:#0099cc;
                }
        </style>      
        
    <style type="text/css" id="css_Can_Officer" runat="server" visible="false">
            .front-steps.front-step-three:hover:after
            {
                cursor:pointer;
                border-left-color:#0099cc;
                }
                .front-steps.front-step-three:hover
            {
                cursor:pointer;
                background-color:#0099cc;
                }            
        </style>               
       
    <style type="text/css" id="css_Can_Director" runat="server" visible="false">
            .front-steps.front-step-four:hover:after
            {
                cursor:pointer;
                border-left-color:#0099cc;
                }
                .front-steps.front-step-four:hover
            {
                cursor:pointer;
                background-color:#0099cc;
                }         
    </style>
       
    <style type="text/css" id="css_Can_Completed" runat="server" visible="false">
            .front-steps.front-step-five:hover
        {
            cursor:pointer;
            background-color:#0099cc;
            }
    </style>
    <style type="text/css" id="css_Can_Del" runat="server" visible="false">
            .front-steps.front-step-del:hover
        {
            cursor:pointer;
            background-color:#E99F46;
            }
    </style>

</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceHolder" Runat="Server">

	<script src="assets/plugins/jquery-knob/js/jquery.knob.js" type="text/javascript"></script>
    <script src="assets/plugins/jquery-tags-input/jquery.tagsinput.min.js" type="text/javascript"></script>
    <script src="assets/scripts/Assessment_Upload.js" type="text/javascript"></script>
    <script src="assets/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript" ></script>   
	<script src="assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js" type="text/javascript" ></script>
    
	<script src="assets/scripts/app.js" type="text/javascript"></script>
    <script src="assets/scripts/gallery.js" type="text/javascript"></script>  
	
<script type="text/javascript">
    jQuery(document).ready(function () {
        // initiate layout and plugins
        Gallery.init();
    });


    function initSlider(elementID, value, min, max, pass, spanScore, txtScore, btnUpdate) {
        $("#" + elementID).slider({
            range: "max",
            min: min,
            max: max,
            value: value,
            slide: function (event, ui) {
                if (ui.value < pass) setSliderColor(elementID, 'red');
                else setSliderColor(elementID, 'green');

                $("#" + spanScore).text(ui.value + '/' + max); // Display
                $("#" + txtScore).val(ui.value); // Value               
            },
            stop: function (event, ui) {
                $("#" + btnUpdate).click();
            }
        });
        if ($("#" + elementID).slider("value") < pass) setSliderColor(elementID, 'red'); else setSliderColor(elementID, 'green');
        $("#" + txtScore).val(value); // Value  
        $("#" + spanScore).text($("#" + elementID).slider("value") + '/' + max);
    }

    function setSliderColor(elementID, _class) {
        $("#" + elementID).removeClass("bg-green");
        $("#" + elementID).removeClass("bg-red");
        $("#" + elementID).addClass("bg-" + _class);
    }

    function initKnob() {
        $(".knob").knob({
            readOnly: true,
            skin: "tron",
            'width': 100,
            'height': 100,
            'dynamicDraw': true,
            'thickness': 0.2,
            'tickColorizeValues': true,
            'skin': 'tron'
        });
    }

</script>



     

</asp:Content>

