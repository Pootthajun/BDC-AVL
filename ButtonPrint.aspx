﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ButtonPrint.aspx.vb" Inherits="ButtonPrint" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link type="text/css" rel="stylesheet" href="assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>

    <div class="container-fluid">
    				<div class="row-fluid">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							สินค้า/พัสดุครุภัณฑ์
						</h3>		
						
									
						<ul class="breadcrumb">
                            
                            <li><i class="icon-cogs"></i> <a href="javascript:;">ตั้งค่าระบบ</a><i class="icon-angle-right"></i></li>
                            <li><i class="icon-th-list"></i> <a href="javascript:;">Report</a><i class="icon-angle-right"></i></li>
                        	<li><i class="icon-folder-open"></i> <a href="javascript:;">ปุ่ม</a></li>
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->	
				     </div>				
			    </div>

				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
                        <div class="btn-group">                                    
							<button data-toggle="dropdown" class="btn dropdown-toggle" id="Button1">พิมพ์  ** Print Out ใบประเมินผู้ขายใหม่ <i class="icon-angle-down"></i></button>										
							<ul class="dropdown-menu pull-right">
								<li><asp:LinkButton ID="btnPDF" runat="server">รูปแบบ PDF</asp:LinkButton></li>
								<li><asp:LinkButton ID="btnExcel" runat="server">รูปแบบ Excel</asp:LinkButton></li>											
							</ul>
						</div>                    
                    </div>
                </div>
    </div>
</ContentTemplate>
</asp:UpdatePanel>





</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceHolder" Runat="Server">
</asp:Content>

