﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Cancel_List
    Inherits System.Web.UI.Page

    Dim BL As New AVLBL
    Dim GL As New GenericLib
    Dim Sub_Dept_ID As String


    Private ReadOnly Property PageName As String
        Get
            Return "Cancel_List.aspx"
        End Get
    End Property

    Private ReadOnly Property User_ID As Integer
        Get
            Try
                If IsNumeric(Session("User_ID")) Then
                    Return Session("User_ID")
                Else
                    Return -1
                End If
            Catch ex As Exception
                Return -1
            End Try
        End Get
    End Property

    Private ReadOnly Property Dept_ID As String
        Get
            Try
                Return ddl_Search_Dept.Items(ddl_Search_Dept.SelectedIndex).Value
                Return ddl_Search_SUB_DEPT.Items(ddl_Search_SUB_DEPT.SelectedIndex).Value
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Private Property AccessMode As AVLBL.AccessRole
        Get
            Try
                Return ViewState("AccessMode")
            Catch ex As Exception
                Return AVLBL.AccessRole.None
            End Try
        End Get
        Set(value As AVLBL.AccessRole)
            ViewState("AccessMode") = value
        End Set
    End Property

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("User_ID")) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "ShowAlert('warning','กรุณาล็อกอินเข้าสู่ระบบ','Login.aspx');", True)
            Response.Redirect("Login.aspx")
            Exit Sub
        End If

        If Not IsPostBack Then
            '--------สิทธิ์---------------
            SetUserRole()


            '------------- Restore Last Search Criteria------------
            BL.BindDDlDEPT(ddl_Search_Dept, User_ID)
            ClearSearchForm()
            BindList()
        End If



    End Sub

    Private Sub SetUserRole()

        '----------- Check สิทธิ์ในเมนู --------------
        Dim MT As DataTable = BL.GetUserMenuRole(User_ID, PageName)
        If MT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลนี้','Overview.aspx');", True)
            Exit Sub
        End If
        AccessMode = MT.Rows(0).Item("MR_ID") '--------- สิทธิ์ในเมนูของผู้ใช้คนปัจจุบัน -----------
        If AccessMode = AVLBL.AccessRole.None Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลนี้','Overview.aspx');", True)
            Exit Sub
        End If

    End Sub

    Private Sub ClearSearchForm()
        ddlStart_M.Items.Clear()
        ddlEnd_M.Items.Clear()
        For i As Integer = 1 To 12
            ddlStart_M.Items.Add(New ListItem(GL.ReportMonthThaiShort(i), i))
            ddlEnd_M.Items.Add(New ListItem(GL.ReportMonthThaiShort(i), i))
        Next
        ddlEnd_M.SelectedIndex = 11

        ddlStart_Y.Items.Clear()
        ddlEnd_Y.Items.Clear()
        For i As Integer = Now.Year - 2 To Now.Year + 8
            ddlStart_Y.Items.Add(New ListItem(i + 543, i))
            ddlEnd_Y.Items.Add(New ListItem(i + 543, i))
        Next
        ddlEnd_Y.SelectedIndex = ddlEnd_Y.Items.Count - 1
    End Sub
    Protected Sub ddlPeriod_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlEnd_M.SelectedIndexChanged, ddlEnd_Y.SelectedIndexChanged, ddlStart_M.SelectedIndexChanged, ddlStart_Y.SelectedIndexChanged

        Dim StartMonth As Integer = (ddlStart_Y.SelectedValue * 12) + ddlStart_M.SelectedValue
        Dim EndMonth As Integer = (ddlEnd_Y.SelectedValue * 12) + ddlEnd_M.SelectedValue

        If StartMonth > EndMonth Then
            Select Case True
                Case Equals(sender, ddlStart_M) Or Equals(sender, ddlStart_Y)
                    ddlEnd_M.SelectedIndex = ddlStart_M.SelectedIndex
                    ddlEnd_Y.SelectedIndex = ddlStart_Y.SelectedIndex
                Case Equals(sender, ddlEnd_M) Or Equals(sender, ddlEnd_Y)
                    ddlStart_M.SelectedIndex = ddlEnd_M.SelectedIndex
                    ddlStart_Y.SelectedIndex = ddlEnd_Y.SelectedIndex
            End Select
        End If

    End Sub


    Private Sub BindList()
        Dim StartMonth As Integer = (ddlStart_Y.SelectedValue * 12) + ddlStart_M.SelectedValue
        Dim EndMonth As Integer = (ddlEnd_Y.SelectedValue * 12) + ddlEnd_M.SelectedValue
        Dim SQL As String = "DECLARE @S AS INT=" & StartMonth & vbLf
        SQL &= "DECLARE @E AS INT=" & EndMonth & vbLf & vbLf
        SQL &= "SELECT LD_ID,Ass_Type,Ass_ID,ReAss_ID,Ref_Year,Ref_Month " & vbLf
        SQL &= " ,tb_Log_Delete.Dept_ID,tb_Log_Delete.Sub_Dept_ID,Ref_Number"
        SQL &= " ,Case WHEN ReAss_ID IS NULL THEN dbo.udf_AssCode(Ref_Year,Ref_Month,tb_Log_Delete.Dept_ID,Ref_Number) ELSE dbo.udf_ReAssCode(Ref_Year,Ref_Month,tb_User.Dept_ID,Ref_Number) END Ref_Code"
        SQL &= " ,Dept_Name,Sub_Dept_Name,S_ID,S_Name  " & vbLf
        SQL &= " ,Type_ID,Item_Name,LD_Description,LD_Comment,Delete_By,Delete_Time " & vbLf
        SQL &= " ,ISNULL(tb_Prefix.Prefix_ABBR,'') + tb_User.Fisrt_Name + ' ' + tb_User.Last_Name FullName" & vbLf
        SQL &= " FROM tb_Log_Delete " & vbLf
        SQL &= " LEFT JOIN tb_User ON tb_User.User_ID= tb_Log_Delete.Delete_By" & vbLf
        SQL &= " INNER JOIN tb_Prefix ON tb_Prefix.Prefix_ID = tb_User.Prefix_ID " & vbLf

        Select Case ddl_Search_Type.SelectedIndex
            Case 0 'ทั้งหมด
                SQL &= " " & vbLf
            Case 1 'เฉพาะการประเมินผู้ขายใหม่
                SQL &= " WHERE  Ass_Type ='Ass'" & vbLf
            Case 2 'เฉพาะการประเมินทบทวน
                SQL &= " WHERE  Ass_Type ='ReAss'" & vbLf
        End Select
        Dim Header As String = ""
        Dim Header_Sub As String = ""
        Dim Title As String = ""
        Select Case ddl_Search_Type.SelectedIndex
            Case 0
                Header &= "ทั้งหมด"
            Case 1
                Header &= "ผู้ขายใหม่"
            Case 2
                Header &= "ทบทวน"
        End Select


        If ddl_Search_Dept.SelectedIndex > 0 Then
            SQL &= " AND tb_Log_Delete.Dept_ID ='" & ddl_Search_Dept.SelectedValue & "' " & vbLf
            Header_Sub &= " " & ddl_Search_Dept.Items(ddl_Search_Dept.SelectedIndex).Text

        End If
        If ddl_Search_SUB_DEPT.SelectedIndex > 0 Then
            SQL &= " AND tb_Log_Delete.Sub_Dept_ID ='" & ddl_Search_SUB_DEPT.SelectedValue & "'  "
            Header_Sub &= " ของฝ่าย/ภาค/งาน " & ddl_Search_SUB_DEPT.Items(ddl_Search_SUB_DEPT.SelectedIndex).Text
        End If
        


        Dim Filter As String = " dbo.UDF_IsRangeIntersected(@S,@E,Month(Delete_Time)+(Year(Delete_Time)*12),Month(Delete_Time)+(Year(Delete_Time)*12))=1  AND" & vbLf
        Title &= " รายการประเมินในช่วง " & ddlStart_M.Items(ddlStart_M.SelectedIndex).Text & " " & ddlStart_Y.Items(ddlStart_Y.SelectedIndex).Text
        Title &= " ถึง "
        Title &= ddlStart_M.Items(ddlEnd_M.SelectedIndex).Text & " " & ddlEnd_Y.Items(ddlEnd_Y.SelectedIndex).Text & " "




        If txt_Search_Item.Text <> "" Then
            Filter &= " (" & vbLf
            Filter &= " LD_Description LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%'  " & vbLf
            Filter &= ") AND "
            Title &= " พัสดุ/ครุภัณฑ์ : " & txt_Search_Item.Text
        End If

        If txt_Search_Code.Text <> "" Then
            Filter &= " (dbo.udf_AssCode(Ref_Year,Ref_Month,tb_Log_Delete.Dept_ID,Ref_Number) LIKE '%" & txt_Search_Code.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " dbo.udf_ReAssCode(Ref_Year,Ref_Month,tb_User.Dept_ID,Ref_Number) LIKE '%" & txt_Search_Code.Text.Replace("'", "''") & "%')    AND" & vbLf
            Title &= " เลขเอกสาร : " & txt_Search_Code.Text
        End If
        If txt_Search_Sup.Text <> "" Then
            Filter &= " (" & vbLf
            Filter &= " S_Name LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%'  " & vbLf
            Filter &= ") AND "
            Title &= " ผู้ขาย : " & txt_Search_Sup.Text
        End If
        If Filter <> "" Then
            SQL &= "AND " & Filter.Substring(0, Filter.Length - 4) & vbLf
        End If

        SQL &= " Order by tb_Log_Delete.Dept_ID,tb_Log_Delete.Sub_Dept_ID, Delete_Time DESC " & vbLf


        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        DA.Fill(DT)

        Session("Report_Cancel_List") = DT
        Session("Report_Cancel_List_Header") = Header
        Session("Report_Cancel_List_Header_Sub") = Header_Sub
        Session("Report_Cancel_List_Title") = Title

        lblTotalRecord.Text = Title
        If DT.Rows.Count = 0 Then
            lblTotalRecord.Text &= " ไม่พบรายการดังกล่าว"
        Else
            lblTotalRecord.Text &= " พบ " & FormatNumber(DT.Rows.Count, 0) & " ใบประเมิน"
        End If

        LastDept = ""
        '------------- Binding To List ---------------
        Pager.SesssionSourceName = "Report_Cancel_List"
        Pager.RenderLayout()

    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

    Protected Sub Search_Changed(sender As Object, e As System.EventArgs) Handles btnSearch.Click
        'ddl_Search_Dept.SelectedIndexChanged, txt_Search_Item.TextChanged, txt_Search_Code.TextChanged, txt_Search_Sup.TextChanged, ddl_Search_Type.SelectedIndexChanged
        BindList()
    End Sub

    Dim LastDept As String = ""
    Protected Sub rptList_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptList.ItemDataBound
        Dim trDept As HtmlTableRow = e.Item.FindControl("trDept")
        Dim lblDept As Label = e.Item.FindControl("lblDept")
        Dim lblDocType As Label = e.Item.FindControl("lblDocType")
        Dim lblRef As Label = e.Item.FindControl("lblRef")
        Dim lblLD_Description As Label = e.Item.FindControl("lblLD_Description")
        Dim lblLD_Comment As Label = e.Item.FindControl("lblLD_Comment")
        Dim lblSub_Dept_Name As Label = e.Item.FindControl("lblSub_Dept_Name")
        Dim lblBy As Label = e.Item.FindControl("lblBy")
        Dim lblDelTime As Label = e.Item.FindControl("lblDelTime")


        If LastDept <> e.Item.DataItem("Dept_Name").ToString Then
            LastDept = e.Item.DataItem("Dept_Name").ToString
            lblDept.Text = LastDept
            trDept.Visible = True
        Else
            trDept.Visible = False
        End If
        lblDept.Attributes("Dept_ID") = e.Item.DataItem("Dept_ID")
        If IsDBNull(e.Item.DataItem("ReAss_ID")) Then
            lblDocType.Text = "ประเมินผู้ขายใหม่"
            lblDocType.ForeColor = Drawing.Color.Teal
            lblRef.Text = e.Item.DataItem("Ref_Code").ToString
            lblRef.Style("color") = "Teal"
        Else
            lblDocType.Text = "ประเมินทบทวน"
            lblDocType.ForeColor = Drawing.Color.Blue
            lblRef.Text = e.Item.DataItem("Ref_Code").ToString
            lblRef.Style("color") = "Blue"
        End If

        lblLD_Description.Text = e.Item.DataItem("LD_Description").ToString
        lblLD_Comment.Text = e.Item.DataItem("LD_Comment").ToString
        lblSub_Dept_Name.Text = e.Item.DataItem("Sub_Dept_Name").ToString
        lblBy.Text = e.Item.DataItem("FullName").ToString
        lblDelTime.Text = GL.ReportThaiDate(e.Item.DataItem("Delete_Time"))


    End Sub


#Region "PrintButton"

    Protected Sub btnPDF_Click(sender As Object, e As System.EventArgs) Handles btnPDF.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/Cancel_List.aspx?Mode=PDF');", True)
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/Cancel_List.aspx?Mode=EXCEL');", True)
    End Sub

#End Region

    Protected Sub ddl_Search_Dept_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_Search_Dept.SelectedIndexChanged
        'BL.BindDDlSub_Dept(ddl_Sub_Dept, Dept_ID, User_ID, AVLBL.AssessmentRole.Creater, "", True)

        BL.BindDDlSubDept_Search(ddl_Search_SUB_DEPT, ddl_Search_Dept.SelectedValue)
        ddl_Search_SUB_DEPT.SelectedIndex = 0
    End Sub

End Class
