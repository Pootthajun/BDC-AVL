﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Login.aspx.vb" Inherits="Login" %>

<%--<%@ Register src="UpdateProgress.ascx" tagname="UpdateProgress" tagprefix="uc1" %>
--%>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>ระบบประเมินผู้ขาย AVL</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	
	<%--<link rel="shortcut icon" href="images/Logo.png" type="image/x-icon"/>--%>
    <link rel="icon" href="images/Logo.ico" type="image/ico"/>  
    <link href="images/Logo.ico" rel="shortcut icon" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link href="assets/css/pages/login.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css"/>
    <link href="assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
	<!-- END PAGE LEVEL STYLES -->
	
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">

	<!-- BEGIN LOGO -->
	<div class="logo" style="text-align:left;">
        <img src="images/Logo.png" style="height:60px; margin-right:20px;" align="left"/>
        
            <span style="color:#fff; font-size:34px;">ระบบประเมินผู้ขาย<br></span>
		    <span style="color:#fff; font-size:24px; position:relative; top:10px;">
                <font style="font-size:32px; color:Red;">A</font>pproved 
                <font style="font-size:32px; color:Red;">V</font>ender 
                <font style="font-size:32px; color:Red;">L</font>ist
             </span>
       
	</div>
	<!-- END LOGO -->
	<!-- BEGIN LOGIN -->
	<div class="content">
		<!-- BEGIN LOGIN FORM -->
		<form class="form-vertical login-form" runat="server" method="post" defaultbutton="btnLogin">
        <asp:ScriptManager ID="scm" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="udp" runat="server">
		    <ContentTemplate>	  

			<h3 class="form-title">ล็อกอินเข้าใช้งาน</h3>
			<div class="alert alert-error hide">
				<button class="close" data-dismiss="alert"></button>
				<span>กรอกชื่อผู้ใช้และรหัสผ่าน</span>
			</div>
			<div class="control-group">
				<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
				<label class="control-label visible-ie8 visible-ie9">ชื่อผู้ใช้ระบบ</label>
				<div class="controls">
					<div class="input-icon left">
						<i class="icon-user"></i>
						<asp:TextBox CssClass="m-wrap placeholder-no-fix" autocomplete="off" placeholder="ชื่อผู้ใช้ระบบ" ID="txtUserName" runat="server"/>
					</div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label visible-ie8 visible-ie9">รหัสผ่าน</label>
				<div class="controls">
					<div class="input-icon left">
						<i class="icon-lock"></i>
						<asp:TextBox CssClass="m-wrap placeholder-no-fix" autocomplete="off" TextMode="Password" placeholder="รหัสผ่าน" ID="txtPassword"  name="txtPassword" runat="server" />
					</div>
				</div>
			</div>
			<div class="form-actions">
				<label class="checkbox" style="margin-left:20px;">
				<input type="checkbox" name="remember" value="1"/> จำฉันไว้ในระบบ
				</label>
				<asp:LinkButton CssClass="btn green pull-right" runat="server" id="btnLogin">
				ตกลง <i class="m-icon-swapright m-icon-white" ></i>
				</asp:LinkButton>
						
			</div>
			
			<div class="forget-password">
			    <h4>ลืมรหัสผ่าน ?</h4>
			    
			    <p>
					กรุณาติดต่อ...................</p>
				
			</div>
			

        <!--BEGIN Show Modal-->
            <a id="btnShowModal" class="btn blue" data-toggle="modal" style="display:none;" href="#modalAlert">View</a>    
            <div id="modalAlert" class="modal hide fade" tabindex="-1" data-width="760">
		        <div class="modal-header">
			        <h3 id="modalTitleWarning" style="color:red; display:none;"><i class="icon-exclamation-sign"></i> Warning</h3>
                    <h3 id="modalTitleSuccess" style="color:green;"><i class="icon-ok-sign"></i> Success</h3>
		        </div>
		        <div class="modal-body">
			        <div  data-always-visible="1" data-rail-visible1="1">
				        <div class="row-fluid" style="min-height:50px;">
					        <div class="row-fluid">
						        <div class="span12">
							        <h5 id="modalMessage"></h5>												
						        </div>
												
					        </div>
				        </div>
			        </div>
		
                  </div>
		        <div class="modal-footer">
			        <button id="modalButton" type="button" data-dismiss="modal" class="btn red">OK</button>
		        </div>
	        </div>
            <!--END Show Modal-->
             </ContentTemplate>
             </asp:UpdatePanel>

			
		</form>
		<!-- END LOGIN FORM -->   
	</div>
	<!-- END LOGIN -->
	<!-- BEGIN COPYRIGHT -->
	<div class="copyright" style="width:300px;">
		
		<%--2013 &copy; tapioca.dft.go.th. All Right Reserved.--%>
	</div>
	<!-- END COPYRIGHT -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->   
	<script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<%--<script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>--%>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<%--<script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>--%>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>  
	<![endif]  --> 
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<%--<script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>--%>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="assets/plugins/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>	
	<script src="assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript" ></script>
	<script src="assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript" ></script>
    <%--<script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>    --%> 
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="assets/scripts/app.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript" src="assets/scripts/ui-modals.js"></script>
	<script src="assets/scripts/login.js" type="text/javascript"></script> 
	<!-- END PAGE LEVEL SCRIPTS --> 
	<script type="text/javascript" language="javascript">
	    jQuery(document).ready(function () {
	        App.init();
	        UIModals.init();
	    });

	    function ShowAlert(alertType, message, redirectToUrl) {

	        document.getElementById('modalTitleWarning').style.display = 'none';
	        document.getElementById('modalTitleSuccess').style.display = 'none';
	        if (alertType.toUpperCase() == 'SUCCESS') {
	            document.getElementById('modalTitleSuccess').style.display = '';
	            document.getElementById('modalButton').className = 'btn green';

	        } else {
	            document.getElementById('modalTitleWarning').style.display = '';
	            document.getElementById('modalButton').className = 'btn red';
	        }
	        if (redirectToUrl != '') {
	            document.getElementById("modalButton").addEventListener("click", function () { window.location.href = redirectToUrl; });
	        }

	        document.getElementById('modalMessage').innerHTML = message;
	        document.getElementById('btnShowModal').click();
	    }
       
	</script>
	<!-- END JAVASCRIPTS -->
	

</body>
<!-- END BODY -->
</html>