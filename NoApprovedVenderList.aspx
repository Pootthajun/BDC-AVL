﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="NoApprovedVenderList.aspx.vb" Inherits="NoApprovedVenderList" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>
    <div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
			        <div class="span12"> 
				        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
				        <h3 class="page-title">
					        ผู้ขายที่ไม่ผ่านการประเมิน
				        </h3>		
				        <ul class="breadcrumb">                            
                            <li>
                                <i class="icon-time"></i><a href="javascript:;">ประวัติ/รายงานสรุป</a><i class="icon-angle-right"></i>
                            </li>
                            <li><i class="icon-remove"></i> <a href="javascript:;">ผู้ขายที่ไม่ผ่านการประเมิน</a></li>
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
				        <!-- END PAGE TITLE & BREADCRUMB-->	
				    </div>				
		        </div>
                <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet">							
							<div class="portlet-body">
                               <asp:UpdatePanel ID="udpSearch" runat="server">
                               <ContentTemplate>                                    
									<div class="table-toolbar"> 
									<div class="btn-group pull-right">
                                    <asp:LinkButton data-toggle="dropdown" class="btn dropdown-toggle" ID="Button2" runat="server">
										    พิมพ์ <i class="icon-angle-down"></i>
                                    </asp:LinkButton>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <asp:LinkButton ID="btnPDF" runat="server">รูปแบบ PDF</asp:LinkButton></li>
                                        <li>
                                            <asp:LinkButton ID="btnExcel" runat="server">รูปแบบ Excel</asp:LinkButton></li>
                                    </ul>
                                </div>
                                <div class="btn-group">
                                    <asp:LinkButton CssClass="btn blue" ID="btnSearch" runat="server">
										        ค้นหา <i class="icon-search"></i>
                                    </asp:LinkButton>
                                </div>
                                </div>

                                <%--แบ่งส่วนการค้นหา--%>
                                <fieldset>
                                    <legend><b>ค้นหา</b></legend>
                                </fieldset>					   
                                    <div class="row-fluid form-horizontal">
									    <div class="span6 ">
									        <div class="control-group">
										        <label class="control-label"> สำนักงาน</label>
										        <div class="controls">
                                                        <asp:DropDownList ID="ddl_Search_Dept" runat="server" class="m-wrap large " AutoPostBack="true"></asp:DropDownList>											       
										        </div>
									        </div>
                                        </div>
                                        <div class="span6 ">
									        <div class="control-group">
										        <label class="control-label"> ฝ่าย/ภาค/งาน</label>
										        <div class="controls">
                                                    <asp:DropDownList ID="ddl_Search_SUB_DEPT" runat="server" class="m-wrap medium" >
                                                    </asp:DropDownList>
                                                </div>
									        </div>
                                        </div>
                                    </div>
                                    <div class="row-fluid form-horizontal">
                                        <div class="span6">
									        <div class="control-group">
											    <label class="control-label"> พัสดุ/ครุภัณฑ์<br />งานจ้าง/บริการ</label>
											    <div class="controls">
												    <asp:TextBox ID="txt_Search_Item" runat="server" class="m-wrap large" AutoPostBack="false" 
                                                    placeholder="ชื่อ พัสดุ/ครุภัณฑ์ / งานจ้างและบริการ"></asp:TextBox>
                                   		        </div>
										    </div>
                                        </div>									    
                                        <div class="span6 " id="divAVL" runat="server" visible ="true">
									        <div class="control-group">
										        <label class="control-label">  เลขใบประเมิน</label>
										        <div class="controls">
											           <asp:TextBox ID="txt_Search_Ref_Code" runat="server" class="m-wrap medium" placeholder="เลขใบประเมิน" AutoPostBack="false"></asp:TextBox>												
										        </div>
									        </div>
                                        </div>    
                                                                            
                                    </div>
                                <div class="row-fluid form-horizontal">
                                <div class="span4 ">
									        <div class="control-group">
											    <label class="control-label"> ผู้ขาย</label>
											    <div class="controls">
												    <asp:TextBox ID="txt_Search_Sup" runat="server" class="m-wrap large" placeholder="ชื่อผู้ขาย/ผู้ติดต่อ/เลขประจำตัวผู้เสียภาษี" AutoPostBack="false"></asp:TextBox>
											    </div>
										    </div>
                                        </div>
                                </div>
                                <div class="row-fluid form-horizontal">
									<div class="span12 ">
										<div class="control-group">
											<label class="control-label"> ช่วงที่ ประเมิน</label>
											<div class="controls">
												<asp:DropDownList ID="ddlStart_M" runat="server" AutoPostBack="true" CssClass="m-wrap" Width="70px">				                                    
			                                    </asp:DropDownList>
                                                <asp:DropDownList ID="ddlStart_Y" runat="server" AutoPostBack="true" CssClass="m-wrap" Width="100px">				                                    
			                                    </asp:DropDownList>
                                                <span style="font-size: 14px; padding-left:30px; padding-right:30px;">ถึง</span>
                                                <asp:DropDownList ID="ddlEnd_M" runat="server" AutoPostBack="true" CssClass="m-wrap"  Width="70px">				                                    
			                                    </asp:DropDownList>
                                                <asp:DropDownList ID="ddlEnd_Y" runat="server" AutoPostBack="true" CssClass="m-wrap" Width="100px">				                                    
			                                    </asp:DropDownList>
											</div>
                                            
										</div>
									</div>									
								</div>

                                    <div role="grid" class="dataTables_wrapper form-inline">
                                         <div class="portlet-body no-more-tables">                                        
                                            <span style="font-weight:bold; font-size:14px;">รายงานผู้ขายที่ไม่ผ่านการประเมิน</span> <asp:Label ID="lblTotalRecord" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
								                <table class="table table-full-width table-advance dataTable no-more-tables table-hover">                                
									                <thead>
										                <tr>											            
											                <th style="text-align:center; vertical-align:middle;" id="thAVL" runat="server" visible ="false">เลข AVL</th>
                                                            <th style="text-align:center; vertical-align:middle;">ผู้ขาย</th>
                                                            <th style="text-align:center; vertical-align:middle;">เลขประจำตัวผู้เสียภาษี</th>
											                <th style="text-align:center; vertical-align:middle;">ชื่อพัสดุ/ครุภัณฑ์ <br />งานจ้าง/บริการ</th>
                                                            <th style="text-align:center; vertical-align:middle;">ฝ่าย/ภาค/งาน</th>
											                <th style="text-align:center; vertical-align:middle;">เลขใบประเมิน</th>                                                          
                                                            <th style="text-align:center; vertical-align:middle; width :120px;">วันที่ประเมิน</th>
										                </tr>
									                </thead>
									                <tbody>
                                                        <asp:Repeater ID="rptList" runat="server">
                                                        <ItemTemplate>
                                                            <tr id="trDept" runat="server">
                                                                <td data-title="ฝ่าย/ภาค/งาน" colspan="8" style="text-align:center"><asp:Label ID="lblDept" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td data-title="เลข AVL" style="text-align:center;" id="trAVL" runat="server" visible ="false"><asp:Label ID="lblAVL" runat="server" Font-Bold="True"></asp:Label></td>
                                                                <td data-title="ผู้ขาย"><asp:Label ID="lblSup" runat="server"></asp:Label></td>
                                                                <td data-title="เลขประจำตัวผู้เสียภาษี"  style="text-align:center;"><asp:Label ID="lblTax" runat="server"></asp:Label></td>
                                                                <td data-title="ชื่อพัสดุ/ครุภัณฑ์ งานจ้าง/บริการ"><asp:Label ID="lblItem" runat="server"></asp:Label></td>
											                    <td data-title="ฝ่าย/ภาค/งาน" style="text-align: center;">
                                                                    <asp:Label ID="lblSub_Dept_Name" runat="server"></asp:Label>
                                                                </td>									                    
                                                                <td data-title="เลขใบประเมิน" style="text-align:center;"><b><a href="" id="lnkRef" runat="server" target ="_blank" ></a></b></td>
											                    <td data-title="วันที่" style="text-align:center;"><asp:Label ID="lblUpdate" runat="server" Font-Bold="True"></asp:Label></td>										
										                    </tr>
                                                        </ItemTemplate>
                                                        </asp:Repeater>
										            
									            
									                </tbody>
								                </table>

                                                <uc2:PageNavigation ID="Pager" runat="server" />
                                            </div> 
                                    </div>
							    </ContentTemplate>
                               </asp:UpdatePanel>
                            </div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>    
               
 </div>


</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceHolder" Runat="Server">
</asp:Content>


