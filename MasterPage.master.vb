﻿
Partial Class MasterPage
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If IsNothing(Session("User_ID")) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "showAlert('กรุณาลงชื่อเข้าใช้ระบบ');", True)
            Response.Redirect("Login.aspx")
            Exit Sub
        End If

        SetThemeColor()
        SetSideBarToggler()
        If Not IsPostBack Then
            SetMenuActive()
            SetUserRole()
            lblUserName.Text = Session("Title") & "" & Session("Fisrt_Name") & " " & Session("Last_Name")
        End If

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "jQuery", "App.initUniform();", True)

    End Sub

    Private Sub SetThemeColor()
        If Not IsNothing(Request.Cookies("style_color")) Then
            Select Case Request.Cookies("style_color").Value.ToString
                Case "blue"
                    Theme_blue.Attributes("class") &= " current"
                    style_color.Href = "assets/css/themes/blue.css"
                Case "brown"
                    Theme_brown.Attributes("class") &= " current"
                    style_color.Href = "assets/css/themes/brown.css"
                Case "purple"
                    Theme_purple.Attributes("class") &= " current"
                    style_color.Href = "assets/css/themes/purple.css"
                Case "grey"
                    Theme_grey.Attributes("class") &= " current"
                    style_color.Href = "assets/css/themes/grey.css"
                Case "light"
                    Theme_light.Attributes("class") &= " current"
                    style_color.Href = "assets/css/themes/light.css"
                Case Else
                    Theme_default.Attributes("class") &= " current"
                    style_color.Href = "assets/css/themes/default.css"
            End Select
        Else
            Theme_default.Attributes("class") &= " current"
            style_color.Href = "assets/css/themes/default.css"
        End If
    End Sub

    Private Sub SetSideBarToggler()
        If Not IsNothing(Request.Cookies("sidebar_width")) Then
            Select Case Request.Cookies("sidebar_width").Value.ToString
                Case "35"
                    Body.Attributes("class") = "page-header-fixed page-sidebar-closed"
                Case "225"
                    Body.Attributes("class") = "page-header-fixed"
            End Select
        End If
    End Sub
    'page-sidebar-closed

    Private Sub SetUserRole()

    End Sub

    Private Sub SetMenuActive()
        Select Case Page.TemplateControl.ToString.ToUpper
            Case "ASP." & "Assessment_Edit.aspx".Replace(".", "_").ToUpper, _
                 "ASP." & "Assessment_List.aspx".Replace(".", "_").ToUpper, _
                 "ASP." & "Reassessment_Edit.aspx".Replace(".", "_").ToUpper, _
                 "ASP." & "Reassessment_List.aspx".Replace(".", "_").ToUpper
                muAssessment.Attributes("class") &= " active"

            Case "ASP." & "SettingSupplier.aspx".Replace(".", "_").ToUpper, _
                            "ASP." & "Setting_AllItem.aspx".Replace(".", "_").ToUpper, _
                            "ASP." & "Setting_User.aspx".Replace(".", "_").ToUpper, _
                            "ASP." & "Setting_Sub_Dept.aspx".Replace(".", "_").ToUpper, _
                            "ASP." & "SettingSupplierType.aspx".Replace(".", "_").ToUpper, _
                            "ASP." & "SettingCertificate.aspx".Replace(".", "_").ToUpper
                muSetting.Attributes("class") &= " active"

            Case "ASP." & "ApprovedVenderList.aspx".Replace(".", "_").ToUpper, _
                "ASP." & "NoApprovedVenderList.aspx".Replace(".", "_").ToUpper, _
                "ASP." & "BlackList_List.aspx".Replace(".", "_").ToUpper, _
                "ASP." & "AssessmentView.aspx".Replace(".", "_").ToUpper, _
                "ASP." & "Compare_Supplier.aspx".Replace(".", "_").ToUpper, _
                "ASP." & "Compare_Supplier.aspx".Replace(".", "_").ToUpper
                muHistory.Attributes("class") &= " active"



            Case "ASP." & "Setting_Ass_Question.aspx".Replace(".", "_").ToUpper, _
                  "ASP." & "Setting_ReAss_Question.aspx".Replace(".", "_").ToUpper
                SubmuQuestion.Attributes("class") &= " active"
                muSetting.Attributes("class") &= " active"

            Case "ASP." & "Setting_Sub_Dept.aspx".Replace(".", "_").ToUpper
                SubmuSub_Dept.Attributes("class") &= " active"
                muSetting.Attributes("class") &= " active"

            Case "ASP." & "Submu_Dept.aspx".Replace(".", "_").ToUpper
                SubmuSub_Dept.Attributes("class") &= " active"
                muSetting.Attributes("class") &= " active"

            Case "ASP." & "HistoryReportAssessment.aspx".Replace(".", "_").ToUpper
                muHistory.Attributes("class") &= " active"


        End Select
    End Sub

    Protected Sub btnCloseModal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseModal.Click

    End Sub

    Protected Sub btnLogout_Click(sender As Object, e As System.EventArgs) Handles btnLogout.Click
        Session.Abandon()
        Response.Redirect("Login.aspx")
    End Sub

End Class

