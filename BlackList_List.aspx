﻿<%@ Page Title="" Language="VB" UICulture="th" Culture="th-TH" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="BlackList_List.aspx.vb" Inherits="BlackList_List" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="ajax" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<link href="assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
    .title_Detail
    {
        width:100%; 
        font-size:16px; 
        background-color:#e02222; 
        color:White;
        font-weight:bold; 
        text-align:center; 
        padding-top:10px; 
        padding-bottom:10px; 
        margin-bottom:20px;
        }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <ajax:ToolkitScriptManager ID="toolkit1" runat="server" EnableScriptGlobalization="true"></ajax:ToolkitScriptManager>
 <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
		<div class="row-fluid">
			<div class="span12"> 
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Black List ผู้ขาย
				</h3>		
				<ul class="breadcrumb">                            
                    <li>
                        <i class="icon-time"></i><a href="javascript:;">ประวัติ/รายงานสรุป</a><i class="icon-angle-right"></i>
                    </li>
                    <li><i class="icon-remove-sign"></i> <a href="javascript:;">Black List ผู้ขาย</a></li>
                        	
                    <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                </ul>
				<!-- END PAGE TITLE & BREADCRUMB-->	
				</div>				
		</div>
            <asp:UpdatePanel ID="udpData" runat="server">
            <ContentTemplate>
            <asp:Panel ID="pnlData" runat="server" DefaultButton="btnSearch"> 
            
                <div class="row-fluid">
               <div class="span12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
						<div class="portlet ">
							<div class="portlet-body">
                                <div class="table-toolbar">
									    <div class="btn-group">     
                                           <asp:LinkButton CssClass="btn red" id="lnkAdd1" runat="server" ToolTip="Black List ผู้ขาย">Black List ผู้ขายเพิ่มเติม Click  <i class="icon-plus"></i></asp:LinkButton>
                                         </div>
                                        <%--<div class="btn-group pull-right">                                    
										    <button data-toggle="dropdown" class="btn dropdown-toggle" id="Button1">พิมพ์ <i class="icon-angle-down"></i></button>										
										    <ul class="dropdown-menu pull-right">
											    <li><asp:LinkButton ID="btnPDF" runat="server">รูปแบบ PDF</asp:LinkButton></li>
											    <li><asp:LinkButton ID="btnExcel" runat="server">รูปแบบ Excel</asp:LinkButton></li>											
										    </ul>
									    </div>
								    </div>--%>
                                    <div class="btn-group pull-right">
                                    <asp:LinkButton data-toggle="dropdown" class="btn dropdown-toggle" ID="Button2" runat="server">
										    พิมพ์ <i class="icon-angle-down"></i>
                                    </asp:LinkButton>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <asp:LinkButton ID="btnPDF" runat="server">รูปแบบ PDF</asp:LinkButton></li>
                                        <li>
                                            <asp:LinkButton ID="btnExcel" runat="server">รูปแบบ Excel</asp:LinkButton></li>
                                    </ul>
                                </div>
                                <div class="btn-group ">
                                    <asp:LinkButton CssClass="btn blue" ID="btnSearch" runat="server">
										        ค้นหา <i class="icon-search"></i>
                                    </asp:LinkButton>
                                </div>
                                </div>
                                <%--แบ่งส่วนการค้นหา--%>
                                <fieldset>
                                    <legend><b>ค้นหา</b></legend>
                                </fieldset>
                                <div class="row-fluid form-horizontal">
									<div class="span6 ">
									    <div class="control-group">
										    <label class="control-label"> ชื่อ</label>
										    <div class="controls">
                                                <asp:TextBox ID="txt_Search_Name" runat="server" AutoPostBack="true" CssClass="m-wrap large" placeholder="ค้นหาจากชื่อหลัก/ชื่อย่อ/ชื่อผู้ติดต่อ"></asp:TextBox>
										    </div>
									    </div>
								    </div>	
                                    <div class="span6 ">
									    <div class="control-group">
										    <label class="control-label"> เลขผู้เสียภาษี</label>
										    <div class="controls">
                                                <asp:TextBox ID="txt_Search_TaxNo" runat="server" AutoPostBack="true" CssClass="m-wrap medium" placeholder="ค้นหาจากเลขผู้เสียภาษี"></asp:TextBox>
										    </div>
									    </div>
								    </div>								    
								</div>
                                <div class="row-fluid form-horizontal">
									<div class="span12 ">
										<div class="control-group">
											<label class="control-label"> ช่วงที่ BLack List</label>
											<div class="controls">
												<asp:DropDownList ID="ddlStart_M" runat="server" AutoPostBack="true" CssClass="m-wrap" Width="70px">				                                    
			                                    </asp:DropDownList>
                                                <asp:DropDownList ID="ddlStart_Y" runat="server" AutoPostBack="true" CssClass="m-wrap" Width="100px">				                                    
			                                    </asp:DropDownList>
                                                <span style="font-size: 14px; padding-left:30px; padding-right:30px;">ถึง</span>
                                                <asp:DropDownList ID="ddlEnd_M" runat="server" AutoPostBack="true" CssClass="m-wrap"  Width="70px">				                                    
			                                    </asp:DropDownList>
                                                <asp:DropDownList ID="ddlEnd_Y" runat="server" AutoPostBack="true" CssClass="m-wrap" Width="100px">				                                    
			                                    </asp:DropDownList>
											</div>
                                            
										</div>
									</div>									
								</div>
                                <div class="portlet-body no-more-tables">
                                    <span style="font-size:14px;font-weight:bold;">รายงานผู้ขายที่ถูก Black List <asp:Label ID="lblCountList" runat ="server" ></asp:Label></span>                                          
								    <table class="table table-full-width table-advance dataTable no-more-tables table-hover">                                
									    <thead>
										    <tr>
											    <th style="text-align:center; vertical-align:middle;">ชื่อถูก Black List</th>
                                                <th style="text-align:center; vertical-align:middle;">เลขผู้เสียภาษี</th>											    
											    <th style="text-align:center; vertical-align:middle;">ชื่อย่อ</th>
											    <th style="text-align:center; vertical-align:middle;">ผู้ติดต่อ</th>
											    <th style="text-align:center; vertical-align:middle;">ช่วงที่ Black List</th>
											    <th style="text-align:center; vertical-align:middle;">รายละเอียด</th>
											    <th style="text-align:center; vertical-align:middle; width :220px;">กำหนด</th>
										    </tr>
									    </thead>
									    <tbody>
                                            <asp:Repeater ID="rptData" runat="server">
										        <ItemTemplate>
										        <tr>
											        <td data-title="ชื่อผู้ขาย"><asp:Label ID="lbl_Name" runat ="server" ForeColor="Red"></asp:Label></td>
                                                    <td data-title="เลขผู้เสียภาษี">
                                                        <asp:Label ID="lbl_TaxNo" runat ="server" ForeColor="Red"></asp:Label>
                                                        <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
                                                    </td>											        
											        <td data-title="ชื่อย่อ"><asp:Label ID="lbl_NameAlias" runat ="server"></asp:Label></td>
                                                    <td data-title="ผู้ติดต่อ"><asp:Label ID="lbl_ContactName" runat ="server"></asp:Label></td>
                                                    <td data-title="ช่วงที่ Black List" style="text-align:center;"><asp:Label ID="lblPeriod" runat ="server"></asp:Label></td>
                                                    <td data-title="รายละเอียด" style="text-align:center;"><asp:Label ID="lblB_Comment" runat ="server"></asp:Label></td>                                                    
                                                    <td data-title="ดำเนินการ" style="text-align:center;">
                                                        <asp:LinkButton ID="btnRptEdit" runat="server" CssClass="btn mini purple" CommandName="Edit"><i class="icon-edit"></i> กำหนด</asp:LinkButton>
                                                        <asp:LinkButton ID="btnRptDelete" runat="server" CssClass="btn mini black"  CommandName="Delete"><i class="icon-trash"></i> ยกเลิก Black List</asp:LinkButton>
                                                            <Ajax:ConfirmButtonExtender ID="cfm_Delete" runat="server" Enabled="true" ConfirmText="ยืนยันยกเลิก Black List?" TargetControlID="btnRptDelete">
                                                            </Ajax:ConfirmButtonExtender>
                                                    </td>
										        </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
									    </tbody>
								    </table>
                                    <asp:PageNavigation ID="Pager" MaximunPageCount="5" PageSize="20" runat="server" />
                                </div> 
							</div>
						</div>
						<!-- END SAMPLE TABLE PORTLET-->
					</div>
				</div>
                <div class="form-actions" style="margin-top:0px; margin-bottom:0px;" id="pnlBottomForm" runat="server">                
                        <asp:LinkButton CssClass="btn red" id="lnkAdd2" runat="server" ToolTip="Black List ผู้ขายเพิ่มเติม">Black List ผู้ขายเพิ่มเติม Click  <i class="icon-plus"></i></asp:LinkButton>
       			</div>            
            </asp:Panel>
            </ContentTemplate>
            </asp:UpdatePanel>

            <asp:UpdatePanel ID="udpEdit" runat="server">
            <ContentTemplate>
            <asp:Panel ID="pnlEdit" runat="server" DefaultButton="btnSave" >            
                <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN VALIDATION STATES-->
						<div class="portlet box red">
							<div class="portlet-title">
								<div class="caption"><i class="icon-user"></i>กำหนด Black List ผู้ขาย</div>								
                                <div class="btn-group pull-right">
                                    <asp:LinkButton CssClass="btn black" ID="btnSupplierDialog" runat="server"><i class="icon-user"></i> เลือกผู้ขาย / ผู้ให้บริการ</asp:LinkButton>                                    
                                </div>
							</div>                          
							<div class="portlet-body form form-horizontal">
                                <div class="row-fluid form-horizontal">
									<div class="span12 ">
										<div class="control-group">
											<label class="control-label">ชื่อ</label>
											<div class="controls">																
												<asp:TextBox ID="txt_S_Fullname" runat="server" CssClass="span11 m-wrap" ReadOnly="True"></asp:TextBox>
                                       		</div>
										</div>
									</div>									
								</div>
                                <div class="row-fluid form-horizontal">                                        
                                    <div class="span6 ">
										<div class="control-group">
											<label class="control-label">ชื่อย่อ</label>
											<div class="controls">																
												<asp:TextBox ID="txt_S_Alias" runat="server" CssClass="medium m-wrap" ReadOnly="True"></asp:TextBox>
											</div>
										</div>
									</div>
                                    <div class="span6 ">
										<div class="control-group">
											<label class="control-label">เลขผู้เสียภาษี</label>
											<div class="controls">																
												<asp:TextBox ID="txt_S_Tax_No" runat="server" CssClass="medium m-wrap" ReadOnly="True"></asp:TextBox>
											</div>
										</div>
									</div>
                                </div>
                                <div class="row-fluid form-horizontal">
									<div class="span12 ">
										<div class="control-group">
											<label class="control-label">ที่อยู่</label>
											<div class="controls">												
												<asp:TextBox ID="txt_S_Address" runat="server" CssClass="span11 m-wrap" ReadOnly="True"></asp:TextBox>
											</div>
										</div>
									</div>
                                </div>
                                <div class="row-fluid form-horizontal">
									<!--/span-->
									<div class="span6 ">
										<div class="control-group">
											<label class="control-label">ผู้ติดต่อ </label>
											<div class="controls">
												<asp:TextBox ID="txt_S_Contact_Name" runat="server" CssClass="medium m-wrap" ReadOnly="True"></asp:TextBox>
											</div>
										</div>
									</div>
									<!--/span-->
                                    <div class="span6 ">
										<div class="control-group">
											<label class="control-label">Email </label>
											<div class="controls">
												<asp:TextBox ID="txt_S_Email" runat="server" CssClass="medium m-wrap" ReadOnly="True"></asp:TextBox>
											</div>
										</div>
									</div>
								</div>
                                <div class="row-fluid form-horizontal">
									<div class="span6 ">
										<div class="control-group">
											<label class="control-label">โทรศัพท์</label>
											<div class="controls">																
												<asp:TextBox ID="txt_S_Phone" runat="server" CssClass="medium m-wrap" ReadOnly="True"></asp:TextBox>
											</div>
										</div>
									</div>
									<!--/span-->
									<div class="span6 ">
										<div class="control-group">
											<label class="control-label">โทรสาร </label>
											<div class="controls">
												<asp:TextBox ID="txt_S_Fax" runat="server" CssClass="medium m-wrap" ReadOnly="True"></asp:TextBox>
											</div>
										</div>
									</div>
									<!--/span-->
								</div>   
                                 <div class="title_Detail">
                                    <i class="icon-info-sign"></i> รายละเอียดเพิ่มเติม
                                 </div>
								 <div class="row-fluid form-horizontal">
									<div class="span6 ">
										<div class="control-group">
											<label class="control-label">Black List ตั้งแต่</label>
											<div class="controls">																
												<asp:TextBox ID="txtEdit_Start" runat="server" CssClass="medium m-wrap" Style="text-align:center;"></asp:TextBox>
                                                <ajax:CalendarExtender ID="cal_txtStart" runat="server" TargetControlID="txtEdit_Start" Format="dd MMM yyyy"></ajax:CalendarExtender>
											</div>
										</div>
									</div>
									<!--/span-->
									<div class="span6 ">
										<div class="control-group">
											<label class="control-label">ถึง </label>
											<div class="controls">
												<asp:TextBox ID="txtEdit_End" runat="server" CssClass="medium m-wrap" Style="text-align:center;"></asp:TextBox>
                                                <ajax:CalendarExtender ID="cal_txtEnd" runat="server" TargetControlID="txtEdit_End" Format="dd MMM yyyy"></ajax:CalendarExtender>
											</div>
										</div>
									</div>
									<!--/span-->
								</div>
                                <div class="row-fluid form-horizontal">
                                    <div class="span12 ">
										<div class="control-group">
											<label class="control-label">รายละเอียดเพิ่มเติม</label>
											<div class="controls">																
												<asp:TextBox ID="txtComment" MaxLength="1000" runat="server" CssClass="span11 m-wrap" TextMode="MultiLine" Rows="10"></asp:TextBox>
											</div>
										</div>
									</div>
                                </div>

								<div class="form-actions">
                                    <asp:Button ID="btnCancel" runat="server"  CssClass="btn" Text="ย้อนกลับ" />
                                    <asp:Button ID="btnDelete" runat="server" CssClass="btn green" Text="ยกเลิกการ Black List" />
                                    <asp:Button ID="btnSave" runat="server" CssClass="btn red" Text="ยืนยัน Black List" />
                                     <Ajax:ConfirmButtonExtender ID="cfm_Delete" runat="server" Enabled="true" ConfirmText="ยืนยันยกเลิก Black List?" TargetControlID="btnDelete">
                                     </Ajax:ConfirmButtonExtender>
								</div>
								<!-- END FORM-->
							</div>
						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>            
            </asp:Panel>
            </ContentTemplate>
            </asp:UpdatePanel>


</div>

            <asp:UpdatePanel ID="udpSelectSupplier" runat="server">
            <ContentTemplate>
                    <asp:Panel CssClass="modal-scrollable" id="dialogSupplier" runat="server" Visible="false" style="margin-left:-450px;" DefaultButton="btnSearchSupplier">
                        <div class="modal fade in modal-overflow" style="display: block; margin-top: 30px; width:900px;">
				        <div class="modal-header">
					        <h3><i class="icon-user"></i> เลือกผู้ขาย / ผู้ให้บริการ</h3>
				        </div>
				        <div class="modal-body">                                                              
                                <div class="row-fluid form-horizontal">							
								        <div class="control-group span6">
									        <label class="control-label"> เลขผู้เสียภาษี</label>
									        <div class="controls">
                                                <asp:TextBox ID="txtSearchTaxNo" runat="server" CssClass="m-wrap" AutoPostBack="True" Placeholder="เลขประจำตัวผู้เสียภาษี"></asp:TextBox>       
									        </div>
								        </div> 
                                        <div class="control-group span6">
									        <label class="control-label"> ค้นจากชื่อ</label>
									        <div class="controls">
                                                <asp:TextBox ID="txtSearchName" runat="server" CssClass="m-wrap" AutoPostBack="True" Placeholder="ชื่อผู้ขาย บริษัท/หจก. ชื่อย่อ"></asp:TextBox>       
									        </div>
								        </div>                                                
                                </div>
                                    <div class="row-fluid form-horizontal">
                                        <div class="control-group span6">
									            <label class="control-label"> ผู้ติดต่อ</label>
									            <div class="controls">
                                                    <asp:TextBox ID="txtSearchAlias" runat="server" CssClass="m-wrap" AutoPostBack="True" Placeholder="ชื่อย่อ"></asp:TextBox>                                                       
									            </div>
								        </div>
                                        <div class="control-group span6">
									            <label class="control-label"> ข้อมูลติดต่อ</label>
									            <div class="controls">
                                                    <asp:TextBox ID="txtSearchContact" runat="server" CssClass="m-wrap" AutoPostBack="True" Placeholder="ชื่อผู้ติดต่อ เบอร์โทร Email Fax"></asp:TextBox>
                                                    <asp:Button ID="btnSearchSupplier" runat="server" style="display:none;" />  
									            </div>
								        </div>
                                    </div>
                                    <div class="portlet-body no-more-tables">
                                    <asp:Label ID="lblTotalSupplier" runat="server" Width="100%" Font-Size="16px" Font-Bold="True" style="text-align:center;"></asp:Label>
                                    <table class="table table-bordered table-advance gridResult">                                                                                                                                                                                                 
									    <thead>
										    <tr>											
                                                <th style="text-align:center;"><i class="icon-bookmark"></i> เลขผู้เสียภาษี</th>
											    <th style="text-align:center;"><i class="icon-user"></i> ชื่อผู้ขาย บริษัท/หจก.</th>
											    <th style="text-align:center;"><i class="icon-user"></i> ผู้ติดต่อ</th>    
                                                <th style="text-align:center;"><i class="icon-comments"></i> ข้อมูลการติดต่อ</th>                                         
										    </tr>
									    </thead>
									    <tbody>
                                            <asp:Repeater ID="rptSupplierList" runat="server">								   
                                            <ItemTemplate>                                                 
										        <tr>	
                                                    <td id="td1" runat="server" data-title="เลขผู้เสียภาษี" style="border-right:1px solid #eeeeee; "><asp:Label ID="lbl_Tax" runat="server"></asp:Label></td>
											        <td id="td2" runat="server" data-title="ชื่อผู้ขาย บริษัท/หจก." style="border-right:1px solid #eeeeee; "><asp:Label ID="lbl_Supplier" runat="server"></asp:Label></td>
											        <td id="td3" runat="server" data-title="ผู้ติดต่อ" style="border-right:1px solid #eeeeee; "><asp:Label ID="lbl_Sales" runat="server"></asp:Label></td>
                                                    <td id="td4" runat="server" data-title="ข้อมูลการติดต่อ" style="border-right:1px solid #eeeeee; "><asp:Label ID="lbl_Contact" runat="server"></asp:Label>
                                                    <asp:Button ID="btnSelect" runat="server" style="display:none;" CommandName="select" />
                                                    </td>                                                    
										        </tr>                                                       
                                                </ItemTemplate>
                                            </asp:Repeater>
								    </tbody>
							        </table>
                                    <asp:PageNavigation ID="PagerSupplier" runat="server"  PageSize="10" />
                                </div>
				        </div>				                
                        <asp:LinkButton CssClass="fancybox-item fancybox-close" ToolTip="ปิด" id="lnkCloseSupplier" runat="server"></asp:LinkButton>
			        </div>
                    <div class="modal-backdrop" style="z-index: 10049;"></div>
                    </asp:Panel>
             </ContentTemplate>
             </asp:UpdatePanel>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceHolder" Runat="Server">
</asp:Content>

