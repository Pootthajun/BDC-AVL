﻿
Imports System.Data
Imports System.Data.SqlClient

Partial Class Setting_AllItem
    Inherits System.Web.UI.Page
    Dim CL As New textControlLib
    Dim BL As New AVLBL
    Dim GL As New GenericLib

    Private ReadOnly Property PageName As String
        Get
            Return "Setting_AllItem.aspx"
        End Get
    End Property
    Private Property AccessMode As AVLBL.AccessRole
        Get
            Try
                Return ViewState("AccessMode")
            Catch ex As Exception
                Return AVLBL.AccessRole.None
            End Try
        End Get
        Set(value As AVLBL.AccessRole)
            ViewState("AccessMode") = value
        End Set
    End Property
    Private ReadOnly Property User_ID As Integer
        Get
            Try
                If IsNumeric(Session("User_ID")) Then
                    Return Session("User_ID")
                Else
                    Return -1
                End If
            Catch ex As Exception
                Return -1
            End Try
        End Get
    End Property


#Region "Search_Property"
    Public ReadOnly Property Search_DEPT_ID As String
        Get
            Return ddlDept_Search.Items(ddlDept_Search.SelectedIndex).Value
        End Get
    End Property

    Public ReadOnly Property Search_Keyword As String
        Get
            Return txtKeyword_Search.Text
        End Get
    End Property

    Public ReadOnly Property Search_ItemCode As String
        Get
            Return txtItemCode_Search.Text
        End Get
    End Property

    Public ReadOnly Property Search_Status As Integer
        Get
            Return ddlStatus_Search.SelectedIndex
        End Get
    End Property

    Public Property All_Data As DataTable
        Get
            Try
                Return Session("Setting_AllItem_All_Data")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As DataTable)
            Session("Setting_AllItem_All_Data") = value
        End Set
    End Property

#End Region

#Region "Old_Edit_Property"

    Public Property EditMode As String
        Get
            Return txt_EditMode.Text
        End Get
        Set(value As String)
            txt_EditMode.Text = value
            lblEditMode.Text = value
        End Set
    End Property

    Public Property Old_Cat_ID As Integer
        Get
            Return Val(txt_Cat_ID.Text)
        End Get
        Set(value As Integer)
            txt_Cat_ID.Text = value
        End Set
    End Property

    Public Property Old_CatGroup_ID As Integer
        Get
            Return Val(txt_CatGroup_ID.Text)
        End Get
        Set(value As Integer)
            txt_CatGroup_ID.Text = value
        End Set
    End Property

    Public Property Old_SubCat_ID As Integer
        Get
            Return Val(txt_SubCat_ID.Text)
        End Get
        Set(value As Integer)
            txt_SubCat_ID.Text = value
        End Set
    End Property
 
    Public Property Old_Type_ID As Integer
        Get
            Return txt_Type_ID.Text
        End Get
        Set(value As Integer)
            txt_Type_ID.Text = value
        End Set
    End Property

    Public Property Old_RunningNo_ID As Integer
        Get
            Return txt_RunningNo_ID.Text
        End Get
        Set(value As Integer)
            txt_RunningNo_ID.Text = value
        End Set
    End Property
#End Region

#Region "New_Edit_Property"

    'Public ReadOnly Property DEPT_ID As String
    '    Get
    '        Return ddlDeptEdit.SelectedValue
    '    End Get
    '    Set(value As String)
    '        ddlDeptEdit.SelectedValue = "01"
    '    End Set
    'End Property

    Public Property DEPT_ID As String
        Get
            Return ddlDeptEdit.SelectedValue
        End Get
        Set(value As String)
            ddlDeptEdit.SelectedValue = "1"
        End Set
    End Property

    Public ReadOnly Property New_CatGroup_ID As String
        Get
            Return ddlCatGroupEdit.SelectedValue
        End Get
    End Property

    Public ReadOnly Property New_Cat_ID As Integer
        Get
            Return ddlCatEdit.SelectedValue
        End Get
    End Property

    Public ReadOnly Property New_SubCat_ID As Integer
        Get
            Return ddlSubCatEdit.SelectedValue
        End Get
    End Property

    Public ReadOnly Property New_Type_ID As Integer
        Get
            Return ddlTypeEdit.SelectedValue
        End Get
    End Property

    Public ReadOnly Property New_RunningNo_ID As Integer
        Get
            Return Val(txtRunningNoEdit.Text)
        End Get
    End Property
#End Region

    Private ReadOnly Property Search_Cat_Group_ID As Integer
        Get
            Try
                Return ddlGroupCat.Items(ddlGroupCat.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
    End Property
    Protected Sub btnCloseDialog_Click(sender As Object, e As System.EventArgs) Handles btnCloseDialog.Click, btnCancel.Click
        pnlEdit.Style("display") = "none"
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("User_ID")) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "ShowAlert('warning','กรุณาล็อกอิน','Login.aspx');", True)
            Exit Sub
        End If

        '--------------- Check Role (Remain)-------------------

        If Not IsPostBack Then
            '----------- Check สิทธิ์ในเมนู --------------
            SetUserRole()

            pnlEdit.Style("display") = "none"
            BL.BindDDlDEPT(ddlDept_Search, Session("User_ID"))
            BindList()
        End If

    End Sub

    Private Sub SetUserRole()

        '----------- Check สิทธิ์ในเมนู --------------
        Dim MT As DataTable = BL.GetUserMenuRole(User_ID, PageName)
        If MT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลนี้','Overview.aspx');", True)
            Exit Sub
        End If
        AccessMode = MT.Rows(0).Item("MR_ID") '--------- สิทธิ์ในเมนูของผู้ใช้คนปัจจุบัน -----------
        If AccessMode = AVLBL.AccessRole.None Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลนี้','Overview.aspx');", True)
            Exit Sub
        End If

        btnAddCat1.Visible = AccessMode = AVLBL.AccessRole.Edit
        btnAddSubCat1.Visible = AccessMode = AVLBL.AccessRole.Edit
        btnAddType1.Visible = AccessMode = AVLBL.AccessRole.Edit
        btnAddRunning1.Visible = AccessMode = AVLBL.AccessRole.Edit

        btnAddCat2.Visible = AccessMode = AVLBL.AccessRole.Edit
        btnAddSubCat2.Visible = AccessMode = AVLBL.AccessRole.Edit
        btnAddType2.Visible = AccessMode = AVLBL.AccessRole.Edit
        btnAddRunning2.Visible = AccessMode = AVLBL.AccessRole.Edit

    End Sub
    Protected Sub Search_Changed(sender As Object, e As System.EventArgs) Handles btnSearch.Click, ddlDept_Search.SelectedIndexChanged, ddlStatus_Search.SelectedIndexChanged, txtItemCode_Search.TextChanged, txtKeyword_Search.TextChanged
        BindList()
    End Sub

    Private Sub BindList()

        Dim SQL As String = ""
        SQL &= " SELECT DEPT.Dept_ID,DEPT.Dept_Name," & vbLf
        'SQL &= " CAT.Cat_No+SUB_CAT.Sub_Cat_No+_TYPE.Type_No TYPE_CODE," & vbLf
        SQL &= " CAT.Cat_ID,CAT.Cat_No,CAT.Cat_Name," & vbLf
        SQL &= " SUB_CAT.Sub_Cat_ID,SUB_CAT.Sub_Cat_No,SUB_CAT.Sub_Cat_Name," & vbLf
        SQL &= " _TYPE.Type_ID,_TYPE.Type_No,_TYPE.Type_Name" & vbLf

        SQL &= " ,R.Running_ID, R.Running_No, R.Running_Name" & vbLf
        SQL &= " ,CAT.Active_Status CAT_Status,SUB_CAT.Active_Status SUB_CAT_Status,_TYPE.Active_Status Type_Status ,R.Active_Status Running_Status " & vbLf
        SQL &= " ,ISNULL(CAT.CG_ID,1) CG_ID ,cg.CG_Name " & vbLf
        SQL &= " FROM tb_Dept DEPT" & vbLf
        SQL &= " INNER JOIN tb_Cat CAT ON DEPT.Dept_ID=CAT.Dept_ID" & vbLf
        SQL &= " LEFT JOIN tb_Sub_Cat SUB_CAT ON CAT.Cat_ID=SUB_CAT.Cat_ID " & vbLf
        SQL &= " LEFT JOIN tb_Type _TYPE ON SUB_CAT.Sub_Cat_ID=_TYPE.Sub_Cat_ID " & vbLf
        SQL &= " LEFT JOIN tb_Running R  ON R.Type_ID =  _TYPE.Type_ID " & vbLf
        SQL &= " LEFT JOIN tb_Cat_Group CG ON CG.CG_ID = CAT.CG_ID " & vbLf
        SQL &= " WHERE DEPT.Active_Status=1" & vbLf

        ',R.Running_ID, R.Running_No, R.Running_Name
        'LEFT JOIN tb_Running R ON  R.Type_ID = T.Type_ID AND R.Active_Status=1

        Dim Filter As String = ""
        If Search_DEPT_ID <> 0 Then
            Filter &= vbLf & " DEPT.Dept_ID='" & Search_DEPT_ID.Replace("'", "''") & "' AND "
        End If

        If Search_Cat_Group_ID <> 0 Then
            Filter &= " CAT.CG_ID=" & Search_Cat_Group_ID & " AND "
        Else
            Filter &= " (CAT.CG_ID IN (1,2) OR CAT.CG_ID IS NULL)  AND "
        End If

        Select Case Search_Status
            Case 0 '----------- ทั้งหมด ------------

            Case 1 '---------- เปิดใช้งาน-------------
                Filter &= vbLf & "(" & vbLf
                Filter &= vbLf & " CAT.Active_Status=1 AND "
                Filter &= vbLf & " ISNULL(SUB_CAT.Active_Status,-1)<>0 AND "
                Filter &= vbLf & " ISNULL(_TYPE.Active_Status,-1)<>0 "
                Filter &= vbLf & " ) AND "
            Case 2 '----------ไม่ได้เปิดใช้งาน----------
                Filter &= vbLf & " (CAT.Active_Status=0 OR SUB_CAT.Active_Status=0 OR _TYPE.Active_Status=0) AND "
        End Select

        If Search_Keyword <> "" Then
            Filter &= " (" & vbLf
            Filter &= " (CAT.Cat_No+SUB_CAT.Sub_Cat_No+_TYPE.Type_No+R.Running_No) LIKE '%" & Search_Keyword.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Dept_Name LIKE '%" & Search_Keyword.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Cat_Name LIKE '%" & Search_Keyword.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Sub_Cat_Name LIKE '%" & Search_Keyword.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Type_Name LIKE '%" & Search_Keyword.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Running_Name LIKE '%" & Search_Keyword.Replace("'", "''") & "%' " & vbLf

            Filter &= " ) AND "
        End If

        If Search_ItemCode <> "" Then
            Filter &= " (CAT.Cat_No+SUB_CAT.Sub_Cat_No+_TYPE.Type_No+R.Running_No) LIKE '%" & Search_ItemCode.Replace("'", "''") & "%' AND "
        End If
        SQL &= " " & vbLf

        If Filter <> "" Then
            SQL &= " AND " & Filter.Substring(0, Filter.Length - 4) & vbLf
        End If

        SQL &= " ORDER BY CAT.CG_ID DESC , Dept_ID,Cat_No,Sub_Cat_No,_TYPE.Type_No,R.Running_No" & vbLf

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        DA.Fill(DT)
        LastDept = ""
        LastCat = ""
        LastSubCat = ""
        LastType = ""

        All_Data = DT.Copy

        '------------- Calculate Summary -------------
        'Dim Col() As String = {"Dept_ID"}
        'DT.DefaultView.RowFilter = "Dept_ID IS NOT NULL"
        'Dim SumDEPT As Integer = DT.DefaultView.ToTable(True, Col).Rows.Count
        'GL.PushArray_String(Col, "Cat_ID")
        'DT.DefaultView.RowFilter = "Dept_ID IS NOT NULL AND Cat_ID IS NOT NULL"
        'Dim SumCAT As Integer = DT.DefaultView.ToTable(True, Col).Rows.Count
        'GL.PushArray_String(Col, "Sub_Cat_ID")
        'DT.DefaultView.RowFilter = "Dept_ID IS NOT NULL AND Cat_ID IS NOT NULL AND Sub_Cat_ID IS NOT NULL"
        'Dim SumSubCat As Integer = DT.DefaultView.ToTable(True, Col).Rows.Count
        'GL.PushArray_String(Col, "Type_ID")
        'DT.DefaultView.RowFilter = "Dept_ID IS NOT NULL AND Cat_ID IS NOT NULL AND Sub_Cat_ID IS NOT NULL AND Type_ID IS NOT NULL"
        'Dim SumType As Integer = DT.Compute("COUNT(Type_ID)", "Type_ID IS NOT NULL")

        'If SumDEPT = 0 Then
        '    lblTotalList.Text = "ไม่พบข้อมูล"
        'ElseIf SumType > 0 Then
        '    lblTotalList.Text = "พบข้อมูล " & FormatNumber(SumType, 0) & " ชนิดย่อย," & FormatNumber(SumSubCat, 0) & " ชนิด " & FormatNumber(SumSubCat, 0) & " ประเภทใน " & SumDEPT & " ฝ่าย"
        'ElseIf SumSubCat > 0 Then
        '    lblTotalList.Text = "พบข้อมูล " & FormatNumber(SumSubCat, 0) & " ชนิด " & FormatNumber(SumSubCat, 0) & " ประเภทใน " & SumDEPT & " ฝ่าย"
        'ElseIf SumCAT > 0 Then
        '    lblTotalList.Text = "พบข้อมูล " & FormatNumber(SumCAT, 0) & " ประเภทใน " & SumDEPT & " ฝ่าย"
        'End If
        lblTotalList.Text = "พบ " & FormatNumber(All_Data.Rows.Count, 0) & " เรคอร์ด"

        '------------- Binding To List ---------------
        Pager.SesssionSourceName = "Setting_AllItem_All_Data"
        Pager.RenderLayout()

        pnlBottomForm.Visible = rptList.Items.Count > 10 '--------- แล้วแต่ความเหมาะสม ------------
    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub

    Protected Sub rptList_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptList.ItemCommand
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lbl_Dept As Label = e.Item.FindControl("lbl_Dept")
        Dim lnk_Cat_Edit As LinkButton = e.Item.FindControl("lnk_Cat_Edit")
        Dim lnk_SubCat_Edit As LinkButton = e.Item.FindControl("lnk_SubCat_Edit")
        Dim lnk_Type_Edit As LinkButton = e.Item.FindControl("lnk_Type_Edit")
        Dim lnk_RunningNo_Edit As LinkButton = e.Item.FindControl("lnk_RunningNo_Edit")

        Select Case e.CommandName
            Case "EditCat"

                ClearPanelEdit()
                '---------------- Cat -------------------
                div_Cat_txt.Visible = True
                '---------------- Cat_Group ----------------
                div_Cat_Group_ddl.Visible = True

                pnlEdit.Style("display") = ""
                EditMode = "แก้ไขประเภท"
                lblEditName.Text = "ชื่อประเภท"

                '------- Set Default Dept & Cat ---------
                Dim _cat_id As Integer = lnk_Cat_Edit.CommandArgument
                Dim SQL As String = "SELECT tb_DEPT.Dept_ID,tb_Cat.Cat_No,tb_Cat.Cat_Name " & vbLf
                SQL &= " ,ISNULL(tb_Cat.CG_ID,1) CG_ID " & vbLf
                SQL &= " FROM tb_DEPT INNER JOIN tb_Cat ON tb_DEPT.Dept_ID=tb_Cat.Dept_ID" & vbLf
                SQL &= " WHERE tb_Cat.Cat_ID=" & _cat_id
                Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','ไม่พบรหัสประเภทดังกล่าว','');", True)
                    BindList()
                    Exit Sub
                End If
                Dim _dept_id As String = DT.Rows(0).Item("DEPT_ID")
                BL.BindDDlDEPT(ddlDeptEdit, Session("User_ID"), _dept_id)
                Old_Cat_ID = _cat_id

                '-----------ประเภทสินค้า/บริการ--------------
                Dim _catGroup_id As String = DT.Rows(0).Item("CG_ID")
                BL.BindDDlCatGroup(ddlCatGroupEdit, _catGroup_id)
                'Old_CatGroup_ID = lbl_Dept.Attributes("CG_ID")
                ddlCatGroupEdit_SelectedIndexChanged(ddlCatGroupEdit, Nothing)

                '---------- Set Enabled -----------------
                ddlDeptEdit.Enabled = False
                ddlCatGroupEdit.Enabled = False
                lblEditItem.Text = DT.Rows(0).Item("Cat_No") & " : " & DT.Rows(0).Item("Cat_Name")
                txtCatEdit.Text = DT.Rows(0).Item("Cat_No")
                txtCatEdit.Enabled = False
                txtEditName.Text = DT.Rows(0).Item("Cat_Name")

                '---------- Set Focus Control ------------------
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "$('#" & txtEditName.ClientID & "').focus();", True)

            Case "AddSubCat"

                ClearPanelEdit()
                '---------------- Cat -----------
                div_Cat_ddl.Visible = True
                '---------------- Cat_Group ----------------
                div_Cat_Group_ddl.Visible = True
                '-------------Sub Cat------------
                div_SubCat_txt.Visible = True

                '------- Set Default Dept & Cat ---------
                Dim _cat_id As Integer = lnk_Cat_Edit.CommandArgument
                Dim SQL As String = "SELECT tb_DEPT.Dept_ID " & vbLf
                SQL &= " ,ISNULL(tb_Cat.CG_ID,1) CG_ID " & vbLf
                SQL &= " FROM tb_DEPT INNER JOIN tb_Cat ON tb_DEPT.Dept_ID=tb_Cat.Dept_ID" & vbLf
                SQL &= " WHERE tb_Cat.Cat_ID=" & _cat_id
                Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','ไม่พบรหัสประเภทดังกล่าว','');", True)
                    BindList()
                    Exit Sub
                End If
                Dim _dept_id As String = DT.Rows(0).Item("DEPT_ID")
                BL.BindDDlDEPT(ddlDeptEdit, Session("User_ID"), _dept_id)
                ddlDeptEdit_SelectedIndexChanged(ddlDeptEdit, Nothing)
                '------------สินค้าหรือบริการ---------
                Dim _catGroup_id As String = DT.Rows(0).Item("CG_ID")
                BL.BindDDlCatGroup(ddlCatGroupEdit, _catGroup_id)
                ddlCatGroupEdit_SelectedIndexChanged(ddlCatGroupEdit, Nothing)

                BL.BindDDlCAT(ddlCatEdit, Session("User_ID"), _dept_id, _cat_id)

                pnlEdit.Style("display") = ""
                EditMode = "เพิ่มชนิด"
                lblEditName.Text = "ชื่อชนิด"

                '---------- Set Enabled -----------------
                ddlDeptEdit.Enabled = False
                ddlCatEdit.Enabled = False
                ddlCatGroupEdit.Enabled = False

                '---------- Set Focus Control ------------------
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "$('#" & txtSubCatEdit.ClientID & "').focus();", True)

            Case "ToggleCat"

                Dim _cat_id As Integer = lnk_Cat_Edit.CommandArgument
                Dim Conn As New SqlConnection(BL.ConnectionString)
                Conn.Open()
                Dim Comm As New SqlCommand
                With Comm
                    .Connection = Conn
                    .CommandType = CommandType.Text
                    .CommandText = "UPDATE tb_Cat SET Active_Status=CASE Active_Status WHEN 1 THEN 0 ELSE 1 END WHERE Cat_ID=" & _cat_id
                    .ExecuteNonQuery()
                    .Dispose()
                End With
                Conn.Close()
                Conn.Dispose()
                BindList()

            Case "DeleteCat"

                Dim _cat_id As Integer = lnk_Cat_Edit.CommandArgument
                Dim Conn As New SqlConnection(BL.ConnectionString)
                Conn.Open()
                Dim Comm As New SqlCommand
                With Comm
                    .Connection = Conn
                    .CommandType = CommandType.Text
                    .CommandText = "DELETE FROM tb_Running WHERE Type_ID IN (SELECT Type_ID FROM tb_Type WHERE Type_ID IN (SELECT Type_ID FROM tb_Type WHERE Sub_Cat_ID IN (SELECT Sub_Cat_ID FROM tb_Sub_Cat WHERE CAT_ID=" & _cat_id & " )) )"
                    .ExecuteNonQuery()
                    .CommandText = "DELETE FROM tb_Type WHERE Sub_Cat_ID IN (SELECT Sub_Cat_ID FROM tb_Sub_Cat WHERE CAT_ID=" & _cat_id & ")"
                    .ExecuteNonQuery()
                    .CommandText = "DELETE FROM tb_Sub_Cat WHERE Cat_ID=" & _cat_id
                    .ExecuteNonQuery()
                    .CommandText = "DELETE FROM tb_Cat WHERE Cat_ID=" & _cat_id
                    .ExecuteNonQuery()
                    .Dispose()
                End With
                Conn.Close()
                Conn.Dispose()
                BindList()

            Case "EditSubCat"

                ClearPanelEdit()
                '---------------- Cat -----------
                div_Cat_ddl.Visible = True
                '---------------- Cat_Group ----------------
                div_Cat_Group_ddl.Visible = True
                '-------------Sub Cat------------
                div_SubCat_txt.Visible = True

                pnlEdit.Style("display") = ""
                EditMode = "แก้ไขชนิด"
                lblEditName.Text = "ชื่อชนิด"

                '------- Set Default Dept & Cat & SubCat---------
                Dim _subcat_id As Integer = lnk_SubCat_Edit.CommandArgument
                Dim SQL As String = "SELECT tb_DEPT.Dept_ID,tb_Cat.Cat_ID,tb_Cat.Cat_No,tb_Sub_Cat.Sub_Cat_No,tb_Sub_Cat.Sub_Cat_Name,ISNULL(tb_Cat.CG_ID,1) CG_ID " & vbLf
                SQL &= " FROM tb_Sub_Cat INNER JOIN tb_DEPT ON tb_DEPT.Dept_ID=tb_Sub_Cat.Dept_ID " & vbLf
                SQL &= " INNER JOIN tb_Cat ON tb_Cat.Cat_ID=tb_Sub_Cat.Cat_ID" & vbLf
                SQL &= " WHERE tb_Sub_Cat.Sub_Cat_ID =" & _subcat_id & vbLf
                Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','ไม่พบรหัสประเภทดังกล่าว','');", True)
                    BindList()
                    Exit Sub
                End If
                Dim _dept_id As String = DT.Rows(0).Item("DEPT_ID")
                Dim _cat_id As String = DT.Rows(0).Item("Cat_ID")

                BL.BindDDlDEPT(ddlDeptEdit, Session("User_ID"), _dept_id)
                ddlDeptEdit_SelectedIndexChanged(ddlDeptEdit, Nothing)
                BL.BindDDlCAT(ddlCatEdit, Session("User_ID"), _dept_id, _cat_id)
                Old_SubCat_ID = _subcat_id
                '-----------ประเภทสินค้า/บริการ--------------
                Dim _catGroup_id As String = DT.Rows(0).Item("CG_ID")
                BL.BindDDlCatGroup(ddlCatGroupEdit, _catGroup_id)
                'Old_CatGroup_ID = lbl_Dept.Attributes("CG_ID")
                ddlCatGroupEdit_SelectedIndexChanged(ddlCatGroupEdit, Nothing)

                '---------- Set Enabled -----------------
                ddlDeptEdit.Enabled = False
                ddlCatGroupEdit.Enabled = False
                ddlCatEdit.Enabled = False
                lblEditItem.Text = DT.Rows(0).Item("Cat_No") & DT.Rows(0).Item("Sub_Cat_No") & " : " & DT.Rows(0).Item("Sub_Cat_Name")
                txtSubCatEdit.Text = DT.Rows(0).Item("Sub_Cat_No")
                txtSubCatEdit.Enabled = False
                txtEditName.Text = DT.Rows(0).Item("Sub_Cat_Name")

                '---------- Set Focus Control ------------------
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "$('#" & txtEditName.ClientID & "').focus();", True)

            Case "AddType"

                ClearPanelEdit()
                '---------------- Cat -----------
                div_Cat_ddl.Visible = True
                '---------------- Cat_Group ----------------
                div_Cat_Group_ddl.Visible = True
                '-------------Sub Cat-------------
                div_SubCat_ddl.Visible = True
                '--------------Type---------------
                div_Type_txt.Visible = True

                '------- Set Default Dept & Cat & SubCat ---------
                Dim _subcat_id As Integer = lnk_SubCat_Edit.CommandArgument
                Dim SQL As String = "SELECT tb_DEPT.Dept_ID,tb_Cat.Cat_ID" & vbLf
                SQL &= " ,ISNULL(tb_Cat.CG_ID,1) CG_ID " & vbLf
                SQL &= " FROM tb_Sub_Cat INNER JOIN tb_DEPT ON tb_DEPT.Dept_ID=tb_Sub_Cat.Dept_ID " & vbLf
                SQL &= " INNER JOIN tb_Cat ON tb_Cat.Cat_ID=tb_Sub_Cat.Cat_ID" & vbLf
                SQL &= " WHERE tb_Sub_Cat.Sub_Cat_ID =" & _subcat_id & vbLf
                Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','ไม่พบรหัสชนิดดังกล่าว','');", True)
                    BindList()
                    Exit Sub
                End If
                Dim _dept_id As String = DT.Rows(0).Item("DEPT_ID")
                Dim _cat_id As Integer = DT.Rows(0).Item("Cat_ID")
                BL.BindDDlDEPT(ddlDeptEdit, Session("User_ID"), _dept_id)
                ddlDeptEdit_SelectedIndexChanged(ddlDeptEdit, Nothing)

                '------------สินค้าหรือบริการ---------
                Dim _catGroup_id As String = DT.Rows(0).Item("CG_ID")
                BL.BindDDlCatGroup(ddlCatGroupEdit, _catGroup_id)
                ddlCatGroupEdit_SelectedIndexChanged(ddlCatGroupEdit, Nothing)


                BL.BindDDlCAT(ddlCatEdit, Session("User_ID"), _dept_id, _cat_id)
                ddlCatEdit_SelectedIndexChanged(ddlCatEdit, Nothing)
                BL.BindDDlSub_Cat(ddlSubCatEdit, Session("User_ID"), _cat_id, _subcat_id)

                pnlEdit.Style("display") = ""
                EditMode = "เพิ่มชนิดย่อย"
                lblEditName.Text = "ชื่อชนิดย่อย"

                '---------- Set Enabled -----------------
                ddlDeptEdit.Enabled = False
                ddlCatGroupEdit.Enabled = False
                ddlCatEdit.Enabled = False
                ddlSubCatEdit.Enabled = False
                '---------- Set Focus Control ------------------
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "$('#" & txtTypeEdit.ClientID & "').focus();", True)

            Case "ToggleSubCat"

                Dim _subcat_id As Integer = lnk_SubCat_Edit.CommandArgument
                Dim Conn As New SqlConnection(BL.ConnectionString)
                Conn.Open()
                Dim Comm As New SqlCommand
                With Comm
                    .Connection = Conn
                    .CommandType = CommandType.Text
                    .CommandText = "UPDATE tb_Sub_Cat SET Active_Status=CASE Active_Status WHEN 1 THEN 0 ELSE 1 END WHERE Sub_Cat_ID=" & _subcat_id
                    .ExecuteNonQuery()
                    .Dispose()
                End With
                Conn.Close()
                Conn.Dispose()
                BindList()

            Case "DeleteSubCat"

                Dim _subcat_id As Integer = lnk_SubCat_Edit.CommandArgument
                Dim Conn As New SqlConnection(BL.ConnectionString)
                Conn.Open()
                Dim Comm As New SqlCommand
                With Comm
                    .Connection = Conn
                    .CommandType = CommandType.Text
                    .CommandText = "DELETE FROM tb_Running WHERE Type_ID IN ( SELECT Type_ID FROM tb_Type WHERE Sub_Cat_ID =" & _subcat_id & " )"
                    .ExecuteNonQuery()
                    .CommandText = "DELETE FROM tb_Type WHERE Sub_Cat_ID=" & _subcat_id
                    .ExecuteNonQuery()
                    .CommandText = "DELETE FROM tb_Sub_Cat WHERE Sub_Cat_ID=" & _subcat_id
                    .ExecuteNonQuery()
                    .Dispose()
                End With
                Conn.Close()
                Conn.Dispose()
                BindList()

            Case "EditType"

                ClearPanelEdit()
                '---------------- Cat -----------
                div_Cat_ddl.Visible = True
                '---------------- Cat_Group ----------------
                div_Cat_Group_ddl.Visible = True
                '-------------Sub Cat-------------
                div_SubCat_ddl.Visible = True
                '--------------Type---------------
                div_Type_txt.Visible = True

                pnlEdit.Style("display") = ""
                EditMode = "แก้ไขชนิดย่อย"
                lblEditName.Text = "ชื่อชนิดย่อย"

                '------- Set Default Dept & Cat ---------
                Dim _type_id As Integer = lnk_Type_Edit.CommandArgument
                Dim SQL As String = "SELECT tb_DEPT.Dept_ID,tb_Cat.Cat_ID,tb_Cat.Cat_No,tb_Sub_Cat.Sub_Cat_ID,tb_Sub_Cat.Sub_Cat_No,tb_Type.Type_No,tb_Type.Type_Name" & vbLf
                SQL &= " ,ISNULL(tb_Cat.CG_ID,1) CG_ID " & vbLf
                SQL &= " FROM tb_Type " & vbLf
                SQL &= " INNER JOIN tb_Sub_Cat ON tb_Type.Sub_Cat_ID=tb_Sub_Cat.Sub_Cat_ID"
                SQL &= " INNER JOIN tb_DEPT ON tb_DEPT.Dept_ID=tb_Sub_Cat.Dept_ID " & vbLf
                SQL &= " INNER JOIN tb_Cat ON tb_Cat.Cat_ID=tb_Sub_Cat.Cat_ID" & vbLf
                SQL &= " WHERE tb_Type.Type_ID =" & _type_id & vbLf

                Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','ไม่พบชนิดย่อยดังกล่าว','');", True)
                    BindList()
                    Exit Sub
                End If
                Dim _dept_id As String = DT.Rows(0).Item("DEPT_ID")
                Dim _cat_id As Integer = DT.Rows(0).Item("Cat_ID")
                Dim _subcat_id As Integer = DT.Rows(0).Item("Sub_Cat_ID")

                BL.BindDDlDEPT(ddlDeptEdit, Session("User_ID"), _dept_id)
                ddlDeptEdit_SelectedIndexChanged(ddlDeptEdit, Nothing)

                '------------สินค้าหรือบริการ---------
                Dim _catGroup_id As String = DT.Rows(0).Item("CG_ID")
                BL.BindDDlCatGroup(ddlCatGroupEdit, _catGroup_id)
                ddlCatGroupEdit_SelectedIndexChanged(ddlCatGroupEdit, Nothing)

                BL.BindDDlCAT(ddlCatEdit, Session("User_ID"), _dept_id, _cat_id)
                ddlCatEdit_SelectedIndexChanged(ddlCatEdit, Nothing)
                BL.BindDDlSub_Cat(ddlSubCatEdit, Session("User_ID"), _cat_id, _subcat_id)
                Old_Type_ID = _type_id

                '---------- Set Enabled -----------------
                ddlDeptEdit.Enabled = False
                ddlCatGroupEdit.Enabled = False
                ddlCatEdit.Enabled = False
                ddlSubCatEdit.Enabled = False

                lblEditItem.Text = DT.Rows(0).Item("Cat_No") & DT.Rows(0).Item("Sub_Cat_No") & DT.Rows(0).Item("Type_No") & " : " & DT.Rows(0).Item("Type_Name")
                txtTypeEdit.Text = DT.Rows(0).Item("Type_No")
                txtTypeEdit.Enabled = False
                txtEditName.Text = DT.Rows(0).Item("Type_Name")

                '---------- Set Focus Control ------------------
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "$('#" & txtEditName.ClientID & "').focus();", True)

                'เพิ่ม AddRunningNo

            Case "AddRunningNo"

                ClearPanelEdit()
                '---------------- Cat -----------
                div_Cat_ddl.Visible = True
                '---------------- Cat_Group ----------------
                div_Cat_Group_ddl.Visible = True
                '-------------Sub Cat-------------
                div_SubCat_ddl.Visible = True
                '--------------Type---------------
                div_Type_ddl.Visible = True
                '--------------RunningNo---------------
                div_RunningNo_txt.Visible = True

                '------- Set Default Dept & Cat & SubCat & Type---------
                Dim _Type_id As Integer = lnk_Type_Edit.CommandArgument
                Dim SQL As String = "SELECT tb_DEPT.Dept_ID,tb_Cat.Cat_ID" & vbLf
                SQL &= " ,ISNULL(tb_Cat.CG_ID,1) CG_ID,tb_Type.Sub_Cat_ID, tb_Type.Type_ID " & vbLf
                SQL &= " FROM tb_Sub_Cat INNER JOIN tb_DEPT ON tb_DEPT.Dept_ID=tb_Sub_Cat.Dept_ID " & vbLf
                SQL &= " INNER JOIN tb_Cat ON tb_Cat.Cat_ID=tb_Sub_Cat.Cat_ID" & vbLf
                SQL &= " INNER JOIN tb_Type ON tb_Sub_Cat.Sub_Cat_ID=tb_Type.Sub_Cat_ID" & vbLf

                SQL &= " WHERE tb_Type.Type_ID =" & _Type_id & vbLf
                Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','ไม่พบรหัสชนิดย่อยดังกล่าว','');", True)
                    BindList()
                    Exit Sub
                End If
                Dim _dept_id As String = DT.Rows(0).Item("DEPT_ID")
                Dim _cat_id As Integer = DT.Rows(0).Item("Cat_ID")
                Dim _subcat_id As Integer = DT.Rows(0).Item("Sub_Cat_ID")

                BL.BindDDlDEPT(ddlDeptEdit, Session("User_ID"), _dept_id)
                ddlDeptEdit_SelectedIndexChanged(ddlDeptEdit, Nothing)

                '------------สินค้าหรือบริการ---------
                Dim _catGroup_id As String = DT.Rows(0).Item("CG_ID")
                BL.BindDDlCatGroup(ddlCatGroupEdit, _catGroup_id)
                ddlCatGroupEdit_SelectedIndexChanged(ddlCatGroupEdit, Nothing)


                BL.BindDDlCAT(ddlCatEdit, Session("User_ID"), _dept_id, _cat_id)
                ddlCatEdit_SelectedIndexChanged(ddlCatEdit, Nothing)

                BL.BindDDlSub_Cat(ddlSubCatEdit, Session("User_ID"), _cat_id, _subcat_id)
                ddlSubCatEdit_SelectedIndexChanged(ddlCatEdit, Nothing)

                BL.BindDDlType(ddlTypeEdit, Session("User_ID"), _subcat_id, _Type_id)

                pnlEdit.Style("display") = ""
                EditMode = "เพิ่ม RunningNo"
                lblEditName.Text = "ชื่อ RunningNo"

                '---------- Set Enabled -----------------
                ddlDeptEdit.Enabled = False
                ddlCatGroupEdit.Enabled = False
                ddlCatEdit.Enabled = False
                ddlSubCatEdit.Enabled = False
                ddlTypeEdit.Enabled = False
                '---------- Set Focus Control ------------------
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "$('#" & txtRunningNoEdit.ClientID & "').focus();", True)



            Case "ToggleType"

                Dim _type_id As Integer = lnk_Type_Edit.CommandArgument
                Dim Conn As New SqlConnection(BL.ConnectionString)
                Conn.Open()
                Dim Comm As New SqlCommand
                With Comm
                    .Connection = Conn
                    .CommandType = CommandType.Text
                    .CommandText = "UPDATE tb_Type SET Active_Status=CASE Active_Status WHEN 1 THEN 0 ELSE 1 END WHERE Type_ID=" & _type_id
                    .ExecuteNonQuery()
                    .Dispose()
                End With
                Conn.Close()
                Conn.Dispose()
                BindList()

            Case "DeleteType"

                Dim _type_id As Integer = lnk_Type_Edit.CommandArgument
                Dim Conn As New SqlConnection(BL.ConnectionString)
                Conn.Open()
                Dim Comm As New SqlCommand
                With Comm
                    .Connection = Conn
                    .CommandType = CommandType.Text
                    .CommandText = "DELETE FROM tb_Running WHERE Type_ID=" & _type_id
                    .ExecuteNonQuery()
                    .CommandText = "DELETE FROM tb_Type WHERE Type_ID=" & _type_id
                    .ExecuteNonQuery()
                    .Dispose()
                End With

                Conn.Close()
                Conn.Dispose()
                BindList()



            Case "EditRunningNo"

                ClearPanelEdit()
                '---------------- Cat -----------
                div_Cat_ddl.Visible = True
                '---------------- Cat_Group ----------------
                div_Cat_Group_ddl.Visible = True
                '-------------Sub Cat-------------
                div_SubCat_ddl.Visible = True
                '--------------Type---------------
                div_Type_ddl.Visible = True
                '--------------RunningNo---------------
                div_RunningNo_txt.Visible = True


                pnlEdit.Style("display") = ""
                EditMode = "แก้ไข RunningNo"
                lblEditName.Text = "ชื่อ RunningNo"

                '------- Set Default Dept & Cat ---------
                Dim _runningNo_id As Integer = lnk_RunningNo_Edit.CommandArgument
                Dim SQL As String = "SELECT tb_DEPT.Dept_ID,tb_Cat.Cat_ID,tb_Cat.Cat_No,tb_Sub_Cat.Sub_Cat_ID,tb_Sub_Cat.Sub_Cat_No,tb_Type.Type_ID,tb_Type.Type_No,tb_Type.Type_Name,tb_Running.Running_ID,tb_Running.Running_No,tb_Running.Running_Name" & vbLf
                SQL &= " ,ISNULL(tb_Cat.CG_ID,1) CG_ID " & vbLf
                SQL &= " FROM tb_Running " & vbLf

                SQL &= " INNER JOIN tb_Type ON tb_Type.Type_ID=tb_Running.Type_ID"
                SQL &= " INNER JOIN tb_Sub_Cat ON tb_Type.Sub_Cat_ID=tb_Sub_Cat.Sub_Cat_ID"
                SQL &= " INNER JOIN tb_DEPT ON tb_DEPT.Dept_ID=tb_Sub_Cat.Dept_ID " & vbLf
                SQL &= " INNER JOIN tb_Cat ON tb_Cat.Cat_ID=tb_Sub_Cat.Cat_ID" & vbLf
                SQL &= " WHERE tb_Running.Running_ID =" & _runningNo_id & vbLf

                Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','ไม่พบ RunningNo ดังกล่าว','');", True)
                    BindList()
                    Exit Sub
                End If
                Dim _dept_id As String = DT.Rows(0).Item("DEPT_ID")
                Dim _cat_id As Integer = DT.Rows(0).Item("Cat_ID")
                Dim _subcat_id As Integer = DT.Rows(0).Item("Sub_Cat_ID")
                Dim _Type_id As Integer = DT.Rows(0).Item("Type_ID")

                BL.BindDDlDEPT(ddlDeptEdit, Session("User_ID"), _dept_id)
                ddlDeptEdit_SelectedIndexChanged(ddlDeptEdit, Nothing)

                '------------สินค้าหรือบริการ---------
                Dim _catGroup_id As String = DT.Rows(0).Item("CG_ID")
                BL.BindDDlCatGroup(ddlCatGroupEdit, _catGroup_id)
                ddlCatGroupEdit_SelectedIndexChanged(ddlCatGroupEdit, Nothing)

                BL.BindDDlCAT(ddlCatEdit, Session("User_ID"), _dept_id, _cat_id)
                ddlCatEdit_SelectedIndexChanged(ddlCatEdit, Nothing)
                BL.BindDDlSub_Cat(ddlSubCatEdit, Session("User_ID"), _cat_id, _subcat_id)
                ddlSubCatEdit_SelectedIndexChanged(ddlCatEdit, Nothing)

                BL.BindDDlType(ddlTypeEdit, Session("User_ID"), _subcat_id, _Type_id)

                Old_RunningNo_ID = _runningNo_id
                '---------- Set Enabled -----------------
                ddlDeptEdit.Enabled = False
                ddlCatGroupEdit.Enabled = False
                ddlCatEdit.Enabled = False
                ddlSubCatEdit.Enabled = False
                ddlTypeEdit.Enabled = False


                lblEditItem.Text = DT.Rows(0).Item("Cat_No") & DT.Rows(0).Item("Sub_Cat_No") & DT.Rows(0).Item("Type_No") & DT.Rows(0).Item("Running_No") & " : " & DT.Rows(0).Item("Running_Name")

                txtRunningNoEdit.Text = DT.Rows(0).Item("Running_No")
                txtRunningNoEdit.Enabled = False
                txtEditName.Text = DT.Rows(0).Item("Running_Name")

                '---------- Set Focus Control ------------------
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "$('#" & txtEditName.ClientID & "').focus();", True)



            Case "ToggleRunningNo"

                Dim _runningNo_id As Integer = lnk_RunningNo_Edit.CommandArgument
                Dim Conn As New SqlConnection(BL.ConnectionString)
                Conn.Open()
                Dim Comm As New SqlCommand
                With Comm
                    .Connection = Conn
                    .CommandType = CommandType.Text
                    .CommandText = "UPDATE tb_Running SET Active_Status=CASE Active_Status WHEN 1 THEN 0 ELSE 1 END WHERE Running_ID=" & _runningNo_id
                    .ExecuteNonQuery()
                    .Dispose()
                End With
                Conn.Close()
                Conn.Dispose()
                BindList()

            Case "DeleteRunningNo"

                Dim _runningNo_id As Integer = lnk_RunningNo_Edit.CommandArgument
                Dim Conn As New SqlConnection(BL.ConnectionString)
                Conn.Open()
                Dim Comm As New SqlCommand
                With Comm
                    .Connection = Conn
                    .CommandType = CommandType.Text
                    .CommandText = "DELETE FROM tb_Running WHERE Running_ID=" & _runningNo_id
                    .ExecuteNonQuery()
                    .Dispose()
                End With



                Conn.Close()
                Conn.Dispose()
                BindList()



        End Select
    End Sub


    Dim LastGroup As String = ""


    Dim LastDept As String = ""
    Dim LastCat As String = ""
    Dim LastSubCat As String = ""
    Dim LastType As String = ""
    Protected Sub rptList_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblDept As Label = e.Item.FindControl("lblDept")
        Dim lnk_Cat As LinkButton = e.Item.FindControl("lnk_Cat")
        Dim lnk_SubCat As LinkButton = e.Item.FindControl("lnk_SubCat")
        Dim lnk_Type As LinkButton = e.Item.FindControl("lnk_Type")
        Dim lnk_RunningNo As LinkButton = e.Item.FindControl("lnk_RunningNo")

        '---------------- Keep ID -------------
        Dim lnk_Cat_Edit As LinkButton = e.Item.FindControl("lnk_Cat_Edit")
        Dim lnk_Cat_Toggle As LinkButton = e.Item.FindControl("lnk_Cat_Toggle")
        Dim lnk_SubCat_Edit As LinkButton = e.Item.FindControl("lnk_SubCat_Edit")
        Dim lnk_SubCat_Toggle As LinkButton = e.Item.FindControl("lnk_SubCat_Toggle")
        Dim lnk_Type_Edit As LinkButton = e.Item.FindControl("lnk_Type_Edit")
        Dim lnk_Type_Toggle As LinkButton = e.Item.FindControl("lnk_Type_Toggle")
        Dim lnk_RunningNo_Edit As LinkButton = e.Item.FindControl("lnk_RunningNo_Edit")
        Dim lnk_RunningNo_Toggle As LinkButton = e.Item.FindControl("lnk_RunningNo_Toggle")

        '-------- Set Confirm Extender -----------
        Dim cfmDeleteCat As AjaxControlToolkit.ConfirmButtonExtender = e.Item.FindControl("cfmDeleteCat")
        Dim cfmDeleteSubCat As AjaxControlToolkit.ConfirmButtonExtender = e.Item.FindControl("cfmDeleteSubCat")
        Dim cfmDeleteType As AjaxControlToolkit.ConfirmButtonExtender = e.Item.FindControl("cfmDeleteType")
        Dim cfmDeleteRunningNo As AjaxControlToolkit.ConfirmButtonExtender = e.Item.FindControl("cfmDeleteRunningNo")

        '-------- Partition Layout ---------------
        Dim tdDept As HtmlTableCell = e.Item.FindControl("tdDept")
        Dim tdCat As HtmlTableCell = e.Item.FindControl("tdCat")
        Dim tdSubCat As HtmlTableCell = e.Item.FindControl("tdSubCat")
        Dim tdType As HtmlTableCell = e.Item.FindControl("tdType")
        Dim tdRunningNo As HtmlTableCell = e.Item.FindControl("tdRunningNo")

        '--------สิทธิ์ในเมนู--------------------------
        Dim trDept As HtmlTableRow = e.Item.FindControl("trDept")

        Dim tr_Edit As HtmlTableRow = e.Item.FindControl("tr_Edit")
        Dim tr_View As HtmlTableRow = e.Item.FindControl("tr_View")
        '-------- Partition Layout ---------------
        Dim tdCat_View As HtmlTableCell = e.Item.FindControl("tdCat_View")
        Dim tdSubCat_View As HtmlTableCell = e.Item.FindControl("tdSubCat_View")
        Dim tdType_View As HtmlTableCell = e.Item.FindControl("tdType_View")
        Dim tdRunningNo_View As HtmlTableCell = e.Item.FindControl("tdRunningNo_View")

        Dim lbl_Cat_View As Label = e.Item.FindControl("lbl_Cat_View")
        Dim lbl_SubCat_View As Label = e.Item.FindControl("lbl_SubCat_View")
        Dim lbl_Type_View As Label = e.Item.FindControl("lbl_Type_View")
        Dim lbl_RunningNo_View As Label = e.Item.FindControl("lbl_RunningNo_View")


        tr_Edit.Visible = AccessMode = AVLBL.AccessRole.Edit
        tr_View.Visible = AccessMode = AVLBL.AccessRole.View


        'If LastDept <> e.Item.DataItem("Dept_Name").ToString Then
        '    LastDept = e.Item.DataItem("Dept_Name").ToString
        '    lblDept.Text = LastDept
        '    trDept.Visible = True
        'Else
        '    trDept.Visible = False
        'End If


        If LastGroup <> e.Item.DataItem("CG_Name").ToString Then
            LastGroup = e.Item.DataItem("CG_Name").ToString
            lblDept.Text = LastGroup

            trDept.Visible = True
        Else
            trDept.Visible = False
        End If

        Dim DuplicatedStyleTop As String = "border-top:none;"
      
        If Not IsDBNull(e.Item.DataItem("Cat_No")) Then

            Dim Cat_Text As String = e.Item.DataItem("Cat_No").ToString & " : " & e.Item.DataItem("Cat_Name").ToString
            If Cat_Text <> LastCat Then
                LastCat = Cat_Text
                lnk_Cat.Text = Cat_Text
                lbl_Cat_View.Text = Cat_Text
                lnk_Cat_Edit.CommandArgument = e.Item.DataItem("Cat_ID")
                cfmDeleteCat.ConfirmText &= Cat_Text
                If Not e.Item.DataItem("CAT_Status") Then
                    lnk_Cat.ForeColor = Drawing.Color.Silver
                    lnk_Cat_Toggle.Text = "<i class='icon-ok'></i> นำกลับมาใช้"
                End If
                '----Reset Sub ------
                LastSubCat = ""
                LastType = ""
            Else
                'lnk_Cat.Text = ""
                lnk_Cat.Visible = False
                'lbl_Cat_View.Visible = False
                tdCat.Attributes("style") &= DuplicatedStyleTop
                tdCat_View.Attributes("style") &= DuplicatedStyleTop
            End If

        Else

        End If
        If Not IsDBNull(e.Item.DataItem("Sub_Cat_No")) Then

            Dim SubCat_Text As String = e.Item.DataItem("Cat_No").ToString & " " & e.Item.DataItem("Sub_Cat_No").ToString & " : " & e.Item.DataItem("Sub_Cat_Name").ToString

            If SubCat_Text <> LastSubCat Then
                LastSubCat = SubCat_Text
                lnk_SubCat.Text = SubCat_Text
                lbl_SubCat_View.Text = SubCat_Text
                lnk_SubCat_Edit.CommandArgument = e.Item.DataItem("Sub_Cat_ID")
                cfmDeleteSubCat.ConfirmText &= lnk_SubCat.Text
                If Not e.Item.DataItem("SUB_CAT_Status") Then
                    lnk_SubCat.ForeColor = Drawing.Color.Silver
                    'lbl_SubCat_View.ForeColor = Drawing.Color.Silver
                    lnk_SubCat_Toggle.Text = "<i class='icon-ok'></i> นำกลับมาใช้"
                End If
                ''----Reset Sub ------
                LastType = ""
            Else
                'lnk_SubCat.Text = ""
                lnk_SubCat.Visible = False
                lbl_SubCat_View.Visible = False
                tdSubCat.Attributes("style") &= DuplicatedStyleTop
                tdSubCat_View.Attributes("style") &= DuplicatedStyleTop
            End If

        End If

        If Not IsDBNull(e.Item.DataItem("Type_No")) Then

            Dim Type_Text As String = e.Item.DataItem("Cat_No").ToString & e.Item.DataItem("Sub_Cat_No").ToString & " " & e.Item.DataItem("Type_No").ToString & " : " & e.Item.DataItem("Type_Name").ToString

            If Type_Text <> LastType Then
                LastType = Type_Text
                lbl_Type_View.Text = Type_Text
                lnk_Type.Text = Type_Text

                
                lnk_Type_Edit.CommandArgument = e.Item.DataItem("Type_ID")
                cfmDeleteType.ConfirmText &= lnk_Type.Text
                If Not e.Item.DataItem("Type_Status") Then
                    lnk_Type.ForeColor = Drawing.Color.Silver
                    'lbl_SubCat_View.ForeColor = Drawing.Color.Silver
                    lnk_Type_Toggle.Text = "<i class='icon-ok'></i> นำกลับมาใช้"
                End If
                ''----Reset Sub ------
                'LastType = ""
            Else
                'lnk_SubCat.Text = ""
                lnk_Type.Visible = False
                lbl_Type_View.Visible = False
                tdType.Attributes("style") &= DuplicatedStyleTop
                tdType_View.Attributes("style") &= DuplicatedStyleTop
            End If

        End If


        'If Not IsDBNull(e.Item.DataItem("Type_No")) Then
        '    lnk_Type.Text = e.Item.DataItem("Cat_No").ToString & e.Item.DataItem("Sub_Cat_No").ToString & " " & e.Item.DataItem("Type_No").ToString & " : " & e.Item.DataItem("Type_Name").ToString
        '    lbl_Type_View.Text = e.Item.DataItem("Cat_No").ToString & e.Item.DataItem("Sub_Cat_No").ToString & " " & e.Item.DataItem("Type_No").ToString & " : " & e.Item.DataItem("Type_Name").ToString

        '    lnk_Type_Edit.CommandArgument = e.Item.DataItem("Type_ID")
        '    cfmDeleteType.ConfirmText &= lnk_Type.Text
        '    If Not e.Item.DataItem("Type_Status") Then
        '        lnk_Type.ForeColor = Drawing.Color.Silver
        '        'lbl_Type_View.ForeColor = Drawing.Color.Silver
        '        lnk_Type_Toggle.Text = "<i class='icon-ok'></i> นำกลับมาใช้"
        '    End If
        'Else
        '    lnk_Type.Visible = False
        '    'lbl_Type_View.Visible = False
        'End If

        If Not IsDBNull(e.Item.DataItem("Running_No")) Then
            lnk_RunningNo.Text = e.Item.DataItem("Cat_No").ToString & e.Item.DataItem("Sub_Cat_No").ToString & e.Item.DataItem("Type_No").ToString & " " & e.Item.DataItem("Running_No").ToString & " : " & e.Item.DataItem("Running_Name").ToString
            lbl_RunningNo_View.Text = e.Item.DataItem("Cat_No").ToString & e.Item.DataItem("Sub_Cat_No").ToString & e.Item.DataItem("Type_No").ToString & " " & e.Item.DataItem("Running_No").ToString & " : " & e.Item.DataItem("Running_Name").ToString

            lnk_RunningNo_Edit.CommandArgument = e.Item.DataItem("Running_ID")
            cfmDeleteRunningNo.ConfirmText &= lnk_RunningNo.Text
            If Not e.Item.DataItem("Running_Status") Then
                lnk_RunningNo.ForeColor = Drawing.Color.Silver
                'lbl_RunningNo_View.ForeColor = Drawing.Color.Silver
                lnk_RunningNo_Toggle.Text = "<i class='icon-ok'></i> นำกลับมาใช้"
            End If
        Else
            lnk_RunningNo.Visible = False
            'lbl_RunningNo_View.Visible = False
        End If
    End Sub


    Protected Sub btnAddCat_Click(sender As Object, e As System.EventArgs) Handles btnAddCat1.Click, btnAddCat2.Click

        ClearPanelEdit()
        '---------------- Cat -------------------
        div_Cat_txt.Visible = True
        div_Cat_Group_ddl.Visible = True
        pnlEdit.Style("display") = ""
        EditMode = "เพิ่มประเภท"
        lblEditName.Text = "ชื่อประเภท"

    End Sub

    Protected Sub btnAddSubCat_Click(sender As Object, e As System.EventArgs) Handles btnAddSubCat1.Click, btnAddSubCat2.Click

        ClearPanelEdit()
        '---------------- Cat -----------
        div_Cat_ddl.Visible = True
        '---------------- Cat_Group ----------------
        div_Cat_Group_ddl.Visible = True
        '-------------Sub Cat------------
        div_SubCat_txt.Visible = True

        pnlEdit.Style("display") = ""
        EditMode = "เพิ่มชนิด"
        lblEditName.Text = "ชื่อชนิด"

    End Sub

    Protected Sub btnAddType_Click(sender As Object, e As System.EventArgs) Handles btnAddType1.Click, btnAddType2.Click
        ClearPanelEdit()
        '---------------- Cat -----------
        div_Cat_ddl.Visible = True
        '---------------- Cat_Group ----------------
        div_Cat_Group_ddl.Visible = True
        '-------------Sub Cat-------------
        div_SubCat_ddl.Visible = True
        '--------------Type---------------
        div_Type_txt.Visible = True

        pnlEdit.Style("display") = ""
        EditMode = "เพิ่มชนิดย่อย"
        lblEditName.Text = "ชื่อชนิดย่อย"
    End Sub

    Protected Sub btnAddRunning_Click(sender As Object, e As System.EventArgs) Handles btnAddRunning1.Click, btnAddRunning2.Click
        ClearPanelEdit()
        '---------------- Cat -----------
        div_Cat_ddl.Visible = True
        '---------------- Cat_Group ----------------
        div_Cat_Group_ddl.Visible = True
        '-------------Sub Cat-------------
        div_SubCat_ddl.Visible = True
        '--------------Type---------------
        div_Type_ddl.Visible = True
        '--------------Running---------------
        div_RunningNo_txt.Visible = True

        pnlEdit.Style("display") = ""
        EditMode = "เพิ่ม RunningNo"
        lblEditName.Text = "ชื่อ RunningNo"
    End Sub

    Private Sub ClearPanelEdit()

        ddlDeptEdit.Enabled = True
        ddlCatGroupEdit.Enabled = True
        ddlCatEdit.Enabled = True
        ddlSubCatEdit.Enabled = True
        ddlTypeEdit.Enabled = True

        BL.BindDDlDEPT(ddlDeptEdit, Session("User_ID"))

        ddlDeptEdit.SelectedValue = "01"
        BL.BindDDlCatGroup(ddlCatGroupEdit)

        '--------------- Header -----------------
        lblEditItem.Text = ""

        '---------------- Cat -------------------
        div_Cat_ddl.Visible = False
        BL.BindDDlCAT(ddlCatEdit, Session("User_ID"), DEPT_ID, 0, True, New_CatGroup_ID)
        div_Cat_txt.Visible = False
        txtCatEdit.Text = ""
        CL.ImplementJavaOnlyNumberText(txtCatEdit)
        Old_Cat_ID = 0

        '---------------- Cat_Group ----------------
        div_Cat_Group_ddl.Visible = False
        BL.BindDDlCatGroup(ddlCatGroupEdit, New_CatGroup_ID)
        div_SubCat_txt.Visible = False
        txtSubCatEdit.Text = ""
        Old_SubCat_ID = 0

        '---------------- SubCat ----------------
        div_SubCat_ddl.Visible = False
        BL.BindDDlSub_Cat(ddlSubCatEdit, Session("User_ID"), New_Cat_ID)
        div_SubCat_txt.Visible = False
        txtSubCatEdit.Text = ""
        CL.ImplementJavaOnlyNumberText(txtSubCatEdit)
        Old_SubCat_ID = 0

        '---------------- Type -----------------

        div_Type_ddl.Visible = False
        BL.BindDDlType(ddlTypeEdit, Session("User_ID"), New_SubCat_ID)
        div_Type_txt.Visible = False
        txtTypeEdit.Text = ""
        CL.ImplementJavaOnlyNumberText(txtTypeEdit)
        Old_Type_ID = 0

        '---------------- RunningNo -----------------
        div_RunningNo_txt.Visible = False
        txtRunningNoEdit.Text = ""
        CL.ImplementJavaOnlyNumberText(txtRunningNoEdit)
        Old_RunningNo_ID = 0

        '---------------- Item Level Name ------
        txtEditName.Text = ""

    End Sub

    Protected Sub ddlDeptEdit_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlDeptEdit.SelectedIndexChanged


        BL.BindDDlCAT(ddlCatEdit, Session("User_ID"), DEPT_ID)
    End Sub

    Protected Sub ddlCatGroupEdit_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlCatGroupEdit.SelectedIndexChanged

        BL.BindDDlCatGroup(ddlCatGroupEdit, New_CatGroup_ID)

        BL.BindDDlCAT(ddlCatEdit, Session("User_ID"), DEPT_ID, New_Cat_ID, True, New_CatGroup_ID)
    End Sub

    Protected Sub ddlCatEdit_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlCatEdit.SelectedIndexChanged
        BL.BindDDlSub_Cat(ddlSubCatEdit, Session("User_ID"), New_Cat_ID)

    End Sub


    'Protected Sub ddlSubCatEdit_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlSubCatEdit.SelectedIndexChanged
    '    BL.BindDDlType(ddlTypeEdit, Session("User_ID"), New_SubCat_ID)

    'End Sub

 

    Protected Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Select Case EditMode
            Case "เพิ่มประเภท", "แก้ไขประเภท"
                SaveCat()
            Case "เพิ่มชนิด", "แก้ไขชนิด"
                SaveSubCat()
            Case "เพิ่มชนิดย่อย", "แก้ไขชนิดย่อย"
                SaveType()
            Case "เพิ่ม RunningNo", "แก้ไข RunningNo"
                SaveRunningNo()
        End Select
    End Sub

    Private Sub SaveCat()
        If DEPT_ID = 0 Or DEPT_ID = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','เลือกแผนก','');", True)
            Exit Sub
        End If

        If New_CatGroup_ID = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','เลือกกลุ่มสินค้าหรือบริการ','');", True)
            Exit Sub
        End If

        If Not IsNumeric(txtCatEdit.Text) Or txtCatEdit.Text.Length < 4 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','กรอกรหัสประเภท เป็นตัวเลข 4 หลัก','');", True)
            Exit Sub
        End If

        If txtEditName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','กรอกชื่อประเภท','');", True)
            Exit Sub
        End If

        Dim SQL As String = "SELECT * FROM tb_Cat WHERE Dept_ID='" & DEPT_ID & "' AND CAT_No='" & txtCatEdit.Text & "' AND CAT_ID<>" & Old_Cat_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','รหัสประเภทซ้ำ','');", True)
            Exit Sub
        End If

        SQL = "SELECT * FROM tb_Cat WHERE Dept_ID='" & DEPT_ID & "' AND CAT_Name='" & txtEditName.Text.Replace("'", "''") & "' AND CAT_ID<>" & Old_Cat_ID
        DA = New SqlDataAdapter(SQL, BL.ConnectionString)
        DT = New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','ชื่อประเภทซ้ำ','');", True)
            Exit Sub
        End If

        SQL = "SELECT * FROM tb_Cat WHERE CAT_ID=" & Old_Cat_ID
        DA = New SqlDataAdapter(SQL, BL.ConnectionString)
        DT = New DataTable
        DA.Fill(DT)
        Dim DR As DataRow
        If Old_Cat_ID = 0 Then
            DR = DT.NewRow
            DR("Cat_ID") = BL.GetNewPrimaryID("tb_Cat", "Cat_ID")
            DT.Rows.Add(DR)
        ElseIf DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','ไม่พบข้อมูลประเภทดังกล่าว<br>อาจถูกลบไปก่อนหน้านี้','');", True)
            Exit Sub
        Else
            DR = DT.Rows(0)
        End If

        DR("Dept_ID") = DEPT_ID
        DR("Cat_No") = txtCatEdit.Text
        DR("Cat_Name") = txtEditName.Text
        DR("CG_ID") = New_CatGroup_ID
        DR("Active_Status") = True
        DR("Update_By") = Session("User_ID")
        DR("Update_Time") = Now

        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('success','บันทึกเรียบร้อย','');", True)


        BindList()
        pnlEdit.Style("display") = "none"



    End Sub

    Private Sub SaveSubCat()
        If DEPT_ID = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','เลือกแผนก','');", True)
            Exit Sub
        End If

        If New_CatGroup_ID = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','เลือกกลุ่มสินค้าหรือบริการ','');", True)
            Exit Sub
        End If

        If New_Cat_ID = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','เลือกประเภท','');", True)
            Exit Sub
        End If

        If Not IsNumeric(txtSubCatEdit.Text) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','กรอกรหัสชนิด เป็นตัวเลข 3 หลัก','');", True)
            Exit Sub
        End If

        If txtSubCatEdit.Text.Length < 3 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','กรอกรหัสชนิด ให้ครบ 3 หลัก','');", True)
            Exit Sub
        End If



        If txtEditName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','กรอกชื่อชนิด','');", True)
            Exit Sub
        End If

        Dim SQL As String = "SELECT * FROM tb_Sub_Cat WHERE CAT_ID=" & New_Cat_ID & " AND Sub_Cat_No='" & txtSubCatEdit.Text & "' AND Sub_Cat_ID<>" & Old_SubCat_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','รหัสชนิดซ้ำ','');", True)
            Exit Sub
        End If

        SQL = "SELECT * FROM tb_Sub_Cat WHERE CAT_ID=" & New_Cat_ID & " AND Sub_Cat_Name='" & txtEditName.Text.Replace("'", "''") & "' AND Sub_Cat_ID<>" & Old_SubCat_ID
        DA = New SqlDataAdapter(SQL, BL.ConnectionString)
        DT = New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','ชื่อชนิดซ้ำ','');", True)
            Exit Sub
        End If

        SQL = "SELECT * FROM tb_Sub_Cat WHERE Sub_Cat_ID=" & Old_SubCat_ID
        DA = New SqlDataAdapter(SQL, BL.ConnectionString)
        DT = New DataTable
        DA.Fill(DT)
        Dim DR As DataRow
        If Old_SubCat_ID = 0 Then
            DR = DT.NewRow
            DR("Sub_Cat_ID") = BL.GetNewPrimaryID("tb_Sub_Cat", "Sub_Cat_ID")
            DT.Rows.Add(DR)
        ElseIf DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','ไม่พบข้อมูลชนิดดังกล่าว\nอาจถูกลบไปก่อนหน้านี้','');", True)
            Exit Sub
        Else
            DR = DT.Rows(0)
        End If

        DR("Dept_ID") = DEPT_ID
        DR("Cat_ID") = New_Cat_ID
        DR("Sub_Cat_No") = txtSubCatEdit.Text
        DR("Sub_Cat_Name") = txtEditName.Text
        DR("Active_Status") = True
        DR("Update_By") = Session("User_ID")
        DR("Update_Time") = Now

        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('success','บันทึกเรียบร้อย','');", True)

        BindList()
        pnlEdit.Style("display") = "none"
    End Sub

    Private Sub SaveType()
        If DEPT_ID = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','เลือกแผนก','');", True)
            Exit Sub
        End If

        If New_CatGroup_ID = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','เลือกกลุ่มสินค้าหรือบริการ','');", True)
            Exit Sub
        End If

        If New_Cat_ID = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','เลือกประเภท','');", True)
            Exit Sub
        End If

        If New_SubCat_ID = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','เลือกชนิด','');", True)
            Exit Sub
        End If

        If Not IsNumeric(txtTypeEdit.Text) Or txtTypeEdit.Text.Length < 2 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','กรอกรหัสชนิดย่อย เป็นตัวเลข 2 หลัก','');", True)
            Exit Sub
        End If

        If txtEditName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','กรอกชื่อชนิดย่อย','');", True)
            Exit Sub
        End If

        Dim SQL As String = "SELECT * FROM tb_Type WHERE Sub_Cat_ID=" & New_SubCat_ID & " AND Type_No='" & txtTypeEdit.Text & "' AND Type_ID<>" & Old_Type_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','รหัสชนิดย่อยซ้ำ','');", True)
            Exit Sub
        End If

        SQL = "SELECT * FROM tb_Type WHERE Sub_Cat_ID=" & New_SubCat_ID & " AND Type_Name='" & txtEditName.Text.Replace("'", "''") & "' AND Type_ID<>" & Old_Type_ID
        DA = New SqlDataAdapter(SQL, BL.ConnectionString)
        DT = New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','ชื่อชนิดย่อยซ้ำ','');", True)
            Exit Sub
        End If

        SQL = "SELECT * FROM tb_Type WHERE Type_ID=" & Old_Type_ID
        DA = New SqlDataAdapter(SQL, BL.ConnectionString)
        DT = New DataTable
        DA.Fill(DT)

        Dim DR As DataRow
        If Old_Type_ID = 0 Then
            DR = DT.NewRow
            DR("Type_ID") = BL.GetNewPrimaryID("tb_Type", "Type_ID")
            DT.Rows.Add(DR)
        ElseIf DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','ไม่พบข้อมูลชนิดย่อยดังกล่าว<br>อาจถูกลบไปก่อนหน้านี้','');", True)
            Exit Sub
        Else
            DR = DT.Rows(0)
        End If

        DR("Dept_ID") = DEPT_ID
        DR("Sub_Cat_ID") = New_SubCat_ID
        DR("Type_No") = txtTypeEdit.Text
        DR("Type_Name") = txtEditName.Text
        DR("Active_Status") = True
        DR("Update_By") = Session("User_ID")
        DR("Update_Time") = Now

        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('success','บันทึกเรียบร้อย','');", True)

        BindList()
        pnlEdit.Style("display") = "none"

    End Sub

    Private Sub SaveRunningNo()
        If DEPT_ID = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','เลือกแผนก','');", True)
            Exit Sub
        End If

        If New_CatGroup_ID = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','เลือกกลุ่มสินค้าหรือบริการ','');", True)
            Exit Sub
        End If

        If New_Cat_ID = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','เลือกประเภท','');", True)
            Exit Sub
        End If

        If New_SubCat_ID = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','เลือกชนิด','');", True)
            Exit Sub
        End If

        If New_Type_ID = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','เลือกชนิดย่อย','');", True)
            Exit Sub
        End If

        If Not IsNumeric(txtRunningNoEdit.Text) Or txtRunningNoEdit.Text.Length < 3 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','กรอกรหัส Running No. เป็นตัวเลข 3 หลัก','');", True)
            Exit Sub
        End If

        If txtEditName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','กรอกชื่อ Running','');", True)
            Exit Sub
        End If

        Dim SQL As String = "SELECT * FROM tb_Running WHERE Type_ID=" & New_Type_ID & " AND Running_No='" & txtRunningNoEdit.Text & "' AND Running_ID<>" & Old_RunningNo_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','รหัส Running ซ้ำ','');", True)
            Exit Sub
        End If

        SQL = "SELECT * FROM tb_Running WHERE Type_ID=" & New_Type_ID & " AND Running_Name='" & txtEditName.Text.Replace("'", "''") & "' AND Running_ID<>" & Old_RunningNo_ID
        DA = New SqlDataAdapter(SQL, BL.ConnectionString)
        DT = New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','ชื่อ Running ซ้ำ','');", True)
            Exit Sub
        End If

        SQL = "SELECT * FROM tb_Running WHERE Running_ID=" & Old_RunningNo_ID
        DA = New SqlDataAdapter(SQL, BL.ConnectionString)
        DT = New DataTable
        DA.Fill(DT)

        Dim DR As DataRow
        If Old_RunningNo_ID = 0 Then
            DR = DT.NewRow
            DR("Running_ID") = BL.GetNewPrimaryID("tb_Running", "Running_ID")
            DT.Rows.Add(DR)
        ElseIf DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validate", "ShowAlert('warning','ไม่พบข้อมูล Running ดังกล่าว\nอาจถูกลบไปก่อนหน้านี้','');", True)
            Exit Sub
        Else
            DR = DT.Rows(0)
        End If

        DR("Dept_ID") = DEPT_ID
        DR("Type_ID") = New_Type_ID
        DR("Running_No") = txtRunningNoEdit.Text
        DR("Running_Name") = txtEditName.Text
        DR("Active_Status") = True
        DR("Update_By") = Session("User_ID")
        DR("Update_Time") = Now

        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('success','บันทึกเรียบร้อย','');", True)

        BindList()
        pnlEdit.Style("display") = "none"

    End Sub

    Protected Sub ddlTypeEdit_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlTypeEdit.SelectedIndexChanged

    End Sub

    Protected Sub ddlSubCatEdit_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlSubCatEdit.SelectedIndexChanged
        BL.BindDDlType(ddlTypeEdit, User_ID, New_SubCat_ID)
    End Sub
End Class
