﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports System
Imports System.IO
Imports AjaxControlToolkit

Imports System.Data.OleDb

Partial Class Page_Import_FSN
    Inherits System.Web.UI.Page

    Dim BL As New AVLBL
    Dim CL As New textControlLib
    Dim GL As New GenericLib
    Dim CV As New Converter

    Private ReadOnly Property Type_File As String
        Get
            Try
                Return ddlFile.Items(ddlFile.SelectedIndex).Value
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            pnlFSN.Visible = True
            pnlSupplier.Visible = False
        End If

         
    End Sub

    Private Structure FilterStructure
        Public ID As String
        Public Name As String
    End Structure

    Protected Sub btnUpload_Click(sender As Object, e As System.EventArgs) Handles btnUpload.Click
        If Not ful.HasFile Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','กรุณาเลือกไฟล์ ประเภท .xlsx','');", True)
            Exit Sub
        End If
        Select Case ful.PostedFile.ContentType
            Case "application/vnd.ms-excel"
            Case "application/msexcel"
            Case "application/x-msexcel"
            Case "application/x-ms-excel"
            Case "application/x-excel"
            Case "application/x-dos_ms_excel"
            Case "application/xls"
            Case "application/x-xls"
            Case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            Case Else
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','เลือกประเภทไฟล์ที่กำหนดดังนี้  .xlsx','');", True)
                Exit Sub
        End Select

        '-------------------------------------------------------------
        Dim F As New FileStructure
        F.Content = CV.StreamToByte(ful.PostedFile.InputStream)
        F.ContentType = ful.PostedFile.ContentType.ToLower
        F.FileName = Path.GetFileName(ful.PostedFile.FileName)
        Session("FILE_IMPORT_") = F

        '-------------------------------------------------------------
        '--------- Save To Path------------
        F = Session("FILE_IMPORT_")
        If Not IsNothing(F) AndAlso Not IsNothing(F.Content) Then
            Dim FS As FileStream = File.OpenWrite(Server.MapPath("FileImport/Import"))
            FS.Write(F.Content, 0, F.Content.Length)
            FS.Close()
        End If
        '-------------------------------------------------------------


        Dim ExcelPath As String = Server.MapPath("FileImport/Import")
        Dim Conn As New System.Data.OleDb.OleDbConnection("provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & ExcelPath & "; Extended Properties=Excel 12.0;")
        Conn.Open()
        Dim SCH As DataTable = Conn.GetSchema("Tables")
        Dim SCH_ As DataTable = Conn.GetSchema("Columns")
        Dim DT As New DataTable

        Dim a As String = SCH.TableName


        Select Case Type_File
            Case "FSN"  '---FSN
                Try
                    Dim DA As New OleDbDataAdapter("SELECT * FROM [Sheet1$]", Conn)
                    DA.Fill(DT)

                    ''----1----ตาราง tb_Cat-------------
                    'DT.DefaultView.RowFilter = " Cat_No IS NOT NULL AND Sub_Cat_No IS NULL AND  Type_No  IS NULL AND  Running_No  IS NULL "



                    ' ''-----2---ตาราง tb_Sub_Cat
                    'DT.DefaultView.RowFilter = " Cat_No IS NOT NULL AND Sub_Cat_No IS NOT NULL AND  Type_No  IS NULL AND  Running_No  IS NULL "


                    '' ''-----3---ตาราง tb_Type
                    'DT.DefaultView.RowFilter = " Cat_No IS NOT NULL AND Sub_Cat_No IS NOT NULL AND  Type_No IS NOT NULL AND  Running_No  IS NULL "

                    ' ''-----4---ตาราง tb_Running
                    DT.DefaultView.RowFilter = " Cat_No IS NOT NULL AND Sub_Cat_No IS NOT NULL AND  Type_No IS NOT NULL AND  Running_No  IS NOT NULL "



                    lblCountRows.Text = DT.DefaultView.Count
                    Session("DT_ImportFSN") = DT.DefaultView.ToTable
                    rptData.DataSource = DT.DefaultView.ToTable
                    rptData.DataBind()
                Catch ex As Exception
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','ตั้งชื่อ Sheet ที่ต้องการนำเข้าเป็น  Sheet1','');", True)
                    ' Exit Sub
                End Try

            Case "Supplier" '---Supplier

                Try
                    Dim DA As New OleDbDataAdapter("SELECT * FROM [Sheet1$]", Conn)
                    DA.Fill(DT)

                    ''----1----ตาราง -------------
                    DT.DefaultView.RowFilter = " SUPPLIER_NAME IS NOT NULL "

                    Dim DT_Supplier As New DataTable
                    DT_Supplier.Columns.Add("S_Tax_No")
                    DT_Supplier.Columns.Add("S_Name")
                    DT_Supplier.Columns.Add("S_Address")
                    DT_Supplier.Columns.Add("S_Phone")
                    DT_Supplier.Columns.Add("S_Fax")
                    DT_Supplier.Columns.Add("S_Email")
                    Dim DR As DataRow
                    For i As Integer = 0 To DT.DefaultView.Count - 1
                        DR = DT_Supplier.NewRow
                        DR("S_Tax_No") = (DT.DefaultView(i).Item("TAX_NO").ToString.Replace(" ", "")).Replace("-", "").ToString
                        DR("S_Name") = DT.DefaultView(i).Item("SUPPLIER_NAME").ToString
                        DR("S_Address") = DT.DefaultView(i).Item("ADDRESS").ToString & " " & DT.DefaultView(i).Item("PROVINCE_NAME").ToString & " " & DT.DefaultView(i).Item("POSTAL_CODE").ToString
                        DR("S_Phone") = DT.DefaultView(i).Item("PHONE").ToString.Replace(" ", "")
                        DR("S_Fax") = DT.DefaultView(i).Item("FAX").ToString.Replace(" ", "")
                        DR("S_Email") = DT.DefaultView(i).Item("EMAIL").ToString.Replace(" ", "")
                        DT_Supplier.Rows.Add(DR)
                    Next

                    lblCound_Supplier.Text = DT_Supplier.Rows.Count
                    Session("DT_ImportSupplier") = DT_Supplier
                    filter_Supplier()

                    'rptSupplier.DataSource = DT_Supplier
                    'rptSupplier.DataBind()
                Catch ex As Exception
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','ตั้งชื่อ Sheet ที่ต้องการนำเข้าเป็น  Sheet1 " & Session("i") & "','');", True)
                    ' Exit Sub
                End Try

            

        End Select


        ful = Nothing
        Conn.Close()
        Try
            If File.Exists(Server.MapPath("FileImport/Import")) Then
                File.Delete(Server.MapPath("FileImport/Import"))
            End If
        Catch : End Try

    End Sub


    Protected Sub rptData_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim lblNumber As Label = e.Item.FindControl("lblNumber")
        Dim lblCat_No As Label = e.Item.FindControl("lblCat_No")
        Dim lblSub_Cat_No As Label = e.Item.FindControl("lblSub_Cat_No")
        Dim lblType_No As Label = e.Item.FindControl("lblType_No")
        Dim lblRunning_No As Label = e.Item.FindControl("lblRunning_No")
        Dim lblCat_Name As Label = e.Item.FindControl("lblCat_Name")
        Dim lblSub_Cat_Name As Label = e.Item.FindControl("lblSub_Cat_Name")
        Dim lblType_Name As Label = e.Item.FindControl("lblType_Name")
        Dim lblRunning_Name As Label = e.Item.FindControl("lblRunning_Name")


        lblNumber.Text = e.Item.ItemIndex + 1
        lblCat_No.Text = e.Item.DataItem("Cat_No").ToString.Trim
        lblSub_Cat_No.Text = e.Item.DataItem("Sub_Cat_No").ToString.Trim
        lblType_No.Text = e.Item.DataItem("Type_No").ToString.Trim
        lblRunning_No.Text = e.Item.DataItem("Running_No").ToString.Trim

        lblCat_Name.Text = e.Item.DataItem("Cat_Name").ToString
        lblSub_Cat_Name.Text = e.Item.DataItem("Sub_Cat_Name").ToString
        lblType_Name.Text = e.Item.DataItem("Type_Name").ToString
        lblRunning_Name.Text = e.Item.DataItem("Running_Name").ToString



    End Sub

    Protected Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click, btn_import.Click
        filter_tb_Cat()
    End Sub


    Private Sub filter_tb_Cat()

        Dim DT_Temp As New DataTable
        DT_Temp = Session("DT_ImportFSN")

        Dim DT As New DataTable
        Dim DR As DataRow
        Dim cmd As New SqlCommandBuilder()

        For i As Integer = 0 To DT_Temp.Rows.Count - 1


            ' ''+++++++++++++  Start----ตาราง tb_Cat  +++++++++++++++++++++
            'Dim SQL As String = "SELECT * FROM tb_Cat WHERE Cat_No='" & DT_Temp.Rows(i).Item("Cat_No").ToString & "' AND Dept_ID='01'"
            'Dim DA = New SqlDataAdapter(SQL, BL.ConnectionString)
            'DT = New DataTable
            'DA.Fill(DT)
            'If DT.Rows.Count = 0 Then
            '    DR = DT.NewRow
            '    DR("Cat_ID") = BL.GetNewPrimaryID("tb_Cat", "Cat_ID")
            'Else
            '    DR = DT.Rows(0)
            'End If
            'DR("Dept_ID") = "01"
            'DR("Cat_No") = DT_Temp.Rows(i).Item("Cat_No").ToString
            'DR("Cat_Name") = DT_Temp.Rows(i).Item("Cat_Name").ToString
            'DR("CG_ID") = 1
            'DR("Active_Status") = True
            'DR("Update_By") = Session("User_ID")
            'DR("Update_Time") = Now
            'If DT.Rows.Count = 0 Then DT.Rows.Add(DR)
            'cmd = New SqlCommandBuilder(DA)
            'DA.Update(DT)  '-----เข้าตาราง tb_Cat
            ' ''++++++++++++++++  END----ตาราง tb_Cat  +++++++++++++++



            ' ''++++++++++++++++ Start----ตาราง tb_Sub_Cat ++++++++++++++++
            ''--------หา Cat_ID เพื่อเข้าตาราง tb_Sub_Cat  จาก Cat_No,Dept_ID=01,CG_ID=1
            'Dim SQL As String = "SELECT Cat_ID FROM tb_Cat WHERE Cat_No='" & DT_Temp.Rows(i).Item("Cat_No").ToString & "' AND Dept_ID='01' AND CG_ID=1"
            'Dim DA = New SqlDataAdapter(SQL, BL.ConnectionString)
            'Dim DT_Cat_ID = New DataTable
            'DA.Fill(DT_Cat_ID)

            ''--------ตาราง tb_Sub_Cat  
            'SQL = "SELECT * FROM tb_Sub_Cat WHERE  Dept_ID='01' AND Sub_Cat_No='" & DT_Temp.Rows(i).Item("Sub_Cat_No").ToString & "' AND Cat_ID='" & DT_Cat_ID.Rows(0).Item("Cat_ID").ToString & "'"
            'DA = New SqlDataAdapter(SQL, BL.ConnectionString)
            'DT = New DataTable
            'DA.Fill(DT)

            'If DT.Rows.Count = 0 Then
            '    DR = DT.NewRow
            '    DR("Sub_Cat_ID") = BL.GetNewPrimaryID("tb_Sub_Cat", "Sub_Cat_ID")
            'Else
            '    DR = DT.Rows(0)
            'End If
            'DR("Dept_ID") = "01"
            'DR("Cat_ID") = DT_Cat_ID.Rows(0).Item("Cat_ID").ToString
            'DR("Sub_Cat_No") = DT_Temp.Rows(i).Item("Sub_Cat_No").ToString
            'DR("Sub_Cat_Name") = DT_Temp.Rows(i).Item("Sub_Cat_Name").ToString
            'DR("Active_Status") = True
            'DR("Update_By") = Session("User_ID")
            'DR("Update_Time") = Now
            'If DT.Rows.Count = 0 Then DT.Rows.Add(DR)
            'cmd = New SqlCommandBuilder(DA)
            'DA.Update(DT)  '-----เข้าตาราง tb_Cat
            ' ''++++++++++++++++  End----ตาราง tb_Sub_Cat  ++++++++++++++++



            ' ''++++++++++++++++ Start----ตาราง tb_Type ++++++++++++++++
            ''--------หา Sub_Cat_ID เพื่อเข้าตาราง tb_Type  จาก Cat_No,Dept_ID=01,CG_ID=1
            'Dim SQL As String = ""
            'SQL &= " SELECT DEPT.Dept_ID,DEPT.Dept_Name,"
            'SQL &= " CAT.Cat_ID,CAT.Cat_No,CAT.Cat_Name,"
            'SQL &= " SUB_CAT.Sub_Cat_ID,SUB_CAT.Sub_Cat_No,SUB_CAT.Sub_Cat_Name,"
            'SQL &= " ISNULL(CAT.CG_ID,1) CG_ID "
            'SQL &= " FROM tb_Dept DEPT"
            'SQL &= " INNER JOIN tb_Cat CAT ON DEPT.Dept_ID=CAT.Dept_ID"
            'SQL &= " LEFT JOIN tb_Sub_Cat SUB_CAT ON CAT.Cat_ID=SUB_CAT.Cat_ID "
            'SQL &= " WHERE  DEPT.Dept_ID='01' AND CAT.Cat_No='" & DT_Temp.Rows(i).Item("Cat_No").ToString & "' AND Sub_Cat_No='" & DT_Temp.Rows(i).Item("Sub_Cat_No").ToString & "' "
            'Dim DA = New SqlDataAdapter(SQL, BL.ConnectionString)
            'Dim DT_Sub_Cat_ID = New DataTable
            'DA.Fill(DT_Sub_Cat_ID)

            ''--------ตาราง tb_Sub_Cat  
            'SQL = "SELECT * FROM tb_Type WHERE  Dept_ID='01' AND Type_No='" & DT_Temp.Rows(i).Item("Type_No").ToString & "' AND Sub_Cat_ID='" & DT_Sub_Cat_ID.Rows(0).Item("Sub_Cat_ID").ToString & "'"
            'DA = New SqlDataAdapter(SQL, BL.ConnectionString)
            'DT = New DataTable
            'DA.Fill(DT)


            'If DT.Rows.Count = 0 Then
            '    DR = DT.NewRow
            '    DR("Type_ID") = BL.GetNewPrimaryID("tb_Type", "Type_ID")
            'Else
            '    DR = DT.Rows(0)
            'End If
            'DR("Dept_ID") = "01"
            'DR("Sub_Cat_ID") = DT_Sub_Cat_ID.Rows(0).Item("Sub_Cat_ID").ToString
            'DR("Type_No") = DT_Temp.Rows(i).Item("Type_No").ToString
            'DR("Type_Name") = DT_Temp.Rows(i).Item("Type_Name").ToString
            'DR("Active_Status") = True
            'DR("Update_By") = Session("User_ID")
            'DR("Update_Time") = Now
            'If DT.Rows.Count = 0 Then DT.Rows.Add(DR)
            'cmd = New SqlCommandBuilder(DA)
            'DA.Update(DT)
            ' ''++++++++++++++++  End----ตาราง tb_Type  ++++++++++++++++



            ''++++++++++++++++ Start----ตาราง tb_Running ++++++++++++++++
            '--------หา TYPE_ID เพื่อเข้าตาราง tb_Running  จาก Cat_No,Dept_ID=01,CG_ID=1
            Dim SQL As String = ""
            SQL &= " SELECT DEPT.Dept_ID,DEPT.Dept_Name," & vbLf
            SQL &= " CAT.Cat_ID,CAT.Cat_No,CAT.Cat_Name," & vbLf
            SQL &= " SUB_CAT.Sub_Cat_ID,SUB_CAT.Sub_Cat_No,SUB_CAT.Sub_Cat_Name," & vbLf
            SQL &= " T.Type_ID ,T.Type_No ,T.Type_Name, " & vbLf
            SQL &= " ISNULL(CAT.CG_ID,1) CG_ID " & vbLf
            SQL &= " FROM tb_Dept DEPT" & vbLf
            SQL &= " INNER JOIN tb_Cat CAT ON DEPT.Dept_ID=CAT.Dept_ID" & vbLf
            SQL &= " LEFT JOIN tb_Sub_Cat SUB_CAT ON CAT.Cat_ID=SUB_CAT.Cat_ID " & vbLf
            SQL &= " LEFT JOIN tb_Type T ON T.Sub_Cat_ID  = SUB_CAT.Sub_Cat_ID " & vbLf
            SQL &= " WHERE  DEPT.Dept_ID='01' AND CAT.Cat_No='" & DT_Temp.Rows(i).Item("Cat_No").ToString & "' AND Sub_Cat_No='" & DT_Temp.Rows(i).Item("Sub_Cat_No").ToString & "'  AND Type_No='" & DT_Temp.Rows(i).Item("Type_No").ToString & "'   "
            Dim DA = New SqlDataAdapter(SQL, BL.ConnectionString)
            Dim DT_Type_ID = New DataTable
            DA.Fill(DT_Type_ID)

            If DT_Type_ID.Rows.Count > 0 Then


                '--------ตาราง tb_Running  
                SQL = "SELECT * FROM tb_Running WHERE  Dept_ID='01' AND Running_No='" & DT_Temp.Rows(i).Item("Running_No").ToString & "' AND Type_ID='" & DT_Type_ID.Rows(0).Item("Type_ID").ToString & "'"
                DA = New SqlDataAdapter(SQL, BL.ConnectionString)
                DT = New DataTable
                DA.Fill(DT)


                If DT.Rows.Count = 0 Then
                    DR = DT.NewRow
                    DR("Running_ID") = BL.GetNewPrimaryID("tb_Running", "Running_ID")
                Else
                    DR = DT.Rows(0)
                End If
                DR("Dept_ID") = "01"
                DR("Type_ID") = DT_Type_ID.Rows(0).Item("Type_ID").ToString
                DR("Running_No") = DT_Temp.Rows(i).Item("Running_No").ToString
                DR("Running_Name") = DT_Temp.Rows(i).Item("Running_Name").ToString
                DR("Active_Status") = True
                DR("Update_By") = Session("User_ID")
                DR("Update_Time") = Now
                If DT.Rows.Count = 0 Then DT.Rows.Add(DR)
                cmd = New SqlCommandBuilder(DA)
                DA.Update(DT)
                ''++++++++++++++++  End----ตาราง tb_Type  ++++++++++++++++
            End If




        Next





    End Sub



    Protected Sub rptSupplier_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptSupplier.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblTAX_NO As Label = e.Item.FindControl("lblTAX_NO")
        Dim lblSUPPLIER_NAME As Label = e.Item.FindControl("lblSUPPLIER_NAME")
        Dim lblADDRESS As Label = e.Item.FindControl("lblADDRESS")
        Dim lblPHONE As Label = e.Item.FindControl("lblPHONE")
        Dim lblFAX As Label = e.Item.FindControl("lblFAX")
        Dim lblEMAIL As Label = e.Item.FindControl("lblEMAIL")

        lblNo.Text = e.Item.ItemIndex + 1
        lblTAX_NO.Text = e.Item.DataItem("S_Tax_No").ToString.Trim
        lblSUPPLIER_NAME.Text = e.Item.DataItem("S_Name").ToString.Trim
        lblADDRESS.Text = e.Item.DataItem("S_Address").ToString.Trim
        lblPHONE.Text = e.Item.DataItem("S_Phone").ToString.Trim

        lblFAX.Text = e.Item.DataItem("S_Fax").ToString
        lblEMAIL.Text = e.Item.DataItem("S_Email").ToString

    End Sub

    Private Sub filter_Supplier()

        Dim DT_Temp As New DataTable
        DT_Temp = Session("DT_ImportSupplier")

        Dim DT As New DataTable
        Dim DR As DataRow
        Dim cmd As New SqlCommandBuilder()
        Dim SQL As String = ""
        For i As Integer = 0 To DT_Temp.Rows.Count - 1
            Session("i") = i
            If DT_Temp.Rows(i).Item("S_Tax_No").ToString <> "" Then
                SQL = "SELECT * FROM tb_Sup_Import WHERE S_Tax_No='" & DT_Temp.Rows(i).Item("S_Tax_No").ToString & "' OR S_Name ='" & DT_Temp.Rows(i).Item("S_Name").ToString & "'  "
            Else
                SQL = "SELECT * FROM tb_Sup_Import "
            End If

            Dim DA = New SqlDataAdapter(Sql, BL.ConnectionString)
            DT = New DataTable
            DA.Fill(DT)

            If DT.Rows.Count = 0 Then
                DR = DT.NewRow
                DR("S_ID") = BL.GetNewPrimaryID("tb_Sup_Import", "S_ID")

                'DR("S_ID") = i + 1

                DR("S_Tax_No") = DT_Temp.Rows(i).Item("S_Tax_No").ToString
                DR("S_Name") = DT_Temp.Rows(i).Item("S_Name").ToString
                DR("S_Address") = DT_Temp.Rows(i).Item("S_Address").ToString
                DR("S_Contact_Name") = DBNull.Value
                DR("S_Phone") = DT_Temp.Rows(i).Item("S_Phone").ToString
                DR("S_Fax") = DT_Temp.Rows(i).Item("S_Fax").ToString
                DR("S_Email") = DT_Temp.Rows(i).Item("S_Email").ToString
                DR("Active_Status") = True
                DR("Update_By") = Session("User_ID")
                DR("Update_Time") = Now
                If DT.Rows.Count = 0 Then DT.Rows.Add(DR)
                cmd = New SqlCommandBuilder(DA)
                DA.Update(DT)
            Else

            End If


        Next

    End Sub


    Protected Sub btn_import_Supplier_Click(sender As Object, e As System.EventArgs) Handles btn_import_Supplier.Click, btn_import_Supplier2.Click
        filter_Supplier()
    End Sub

    Protected Sub ddlFile_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlFile.SelectedIndexChanged
        Select Case Type_File
            Case "FSN"
                pnlFSN.Visible = True
                pnlSupplier.Visible = False

            Case "Supplier"
                pnlFSN.Visible = False
                pnlSupplier.Visible = True
        End Select
    End Sub
End Class
