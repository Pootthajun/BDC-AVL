﻿<%@ Page Language="VB" EnableEventValidation="false" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Setting_Dept.aspx.vb" Inherits="Setting_Dept" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link type="text/css" rel="stylesheet" href="assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
    <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>--%>
    <div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3>
							สำนักงาน
						    
						</h3>		
						
									
						<ul class="breadcrumb">
                            
                            <li><i class="icon-cogs"></i> <a href="javascript:;">ตั้งค่าระบบ</a><i class="icon-angle-right"></i></li>
                        	<li><i class="icon-hospital"></i> <a href="javascript:;">สำนักงาน</a></li>
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->	
				     </div>				
			    </div>
               <div class="row-fluid">
               <div class="span12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
						<div class="portlet">
							<asp:Panel ID="pnlData" runat="server">
							    <div class="portlet-body">
                                    <div class="table-toolbar">
									    <div class="btn-group">
										    <asp:LinkButton CssClass="btn green" id="btnAdd" runat="server">
										    เพิ่ม <i class="icon-plus"></i>
										    </asp:LinkButton>
									    </div>
								    </div>

                                    <div class="portlet-body no-more-tables"  style="width: 100%;text-align: center;">                                            
								    <table class="table table-advance dataTable no-more-tables table-hover">                     
									    <thead>
										    <tr>
											    <th style="text-align:Center; vertical-align:middle;">สำนักงาน</th>
											    <th style="text-align:center; vertical-align:middle;" width="150">ใช้</th>
											    <th id="thEdit_Header" runat="server" style="text-align:center; vertical-align:middle;" width="130">ดำเนินการ</th>
										    </tr>
									    </thead>
									    <tbody>
                                            <asp:Repeater ID="rptData" runat="server">
                                                <ItemTemplate>
                                                    <tr style="border-bottom:solid 1px #efefef; vertical-align:middle;">
                                                        <td>
                                                            <asp:Label ID="lblId" runat="server" Visible="True"></asp:Label> : 
                                                            <asp:Label ID="lblName" runat="server"></asp:Label>
                                                        </td>
                                                        <td style="text-align:center;">
                                                            <asp:Image ID="imgStatus" runat="server" ToolTip="Edit Job" ImageUrl="images/check.png"/>
                                                        </td>
                                                        <td  id="tdEdit_List" runat="server" style="text-align:center;">
                                                            <asp:LinkButton ID="btnRptEdit" runat="server" CssClass="btn mini purple"  CommandName="Edit"><i class="icon-edit"></i> แก้ไข</asp:LinkButton>
                                                            <asp:LinkButton ID="btnRptDelete" runat="server" CssClass="btn mini black"  CommandName="Delete"><i class="icon-trash"></i> ลบ</asp:LinkButton>
                                                           
                                                            <asp:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" Enabled="true" ConfirmText="คุณต้องการลบข้อมูล ใช่หรือไม่?" TargetControlID="btnRptDelete">
                                                            </asp:ConfirmButtonExtender>
                                                        </td>                                   
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
									    </tbody>
								    </table>
                                </div>                                                    
                                    <div class="row-fluid">
                                    </div>                                   
							    </div>
                            </asp:Panel>
                            <asp:Panel ID="pnlEdit" runat="server" Visible="false">
                                <div class="row-fluid">
					                <div class="span12">
						                <!-- BEGIN VALIDATION STATES-->
						                <div class="portlet box blue">
							                <div class="portlet-title">
								                <div class="caption"><i class="icon-user"></i>เพิ่ม/แก้ไข ข้อมูลสำนักงาน</div>
								                <div class="tools">
								                </div>
							                </div>
							                <div class="portlet-body form form-horizontal">
                                
                                                <form class="form-horizontal" id="form_sample_2" action="#" novalidate="novalidate">
                                                    <div class="control-group">
										                <label class="control-label">รหัสสำนักงาน <font id="lblValidate_Dept_ID" runat="server"  color="red">*</font>&nbsp;&nbsp;</label>
										                <div class="controls">
											                <asp:TextBox ID="txtDept_ID"  runat="server" class="span6 m-wrap"  placeholder="กรอกรหัส 2 หลัก"  MaxLength="2" ></asp:TextBox> 
										                    <asp:Label  ID="lblDept_ID" runat ="server" style=" display :none;"></asp:Label>
                                                            <asp:FilteredTextBoxExtender ID="txtDept_ID_FilteredTextBoxExtender" 
                                                    runat="server" Enabled="True" TargetControlID="txtDept_ID" 
                                                FilterType="Numbers" InvalidChars="1234567890" >
                                                </asp:FilteredTextBoxExtender>
                                                        </div>
									                </div>	
									                <div class="control-group">
										                <label class="control-label">สำนักงาน <font  id="lblValidate_txtName" runat="server"  color="red">*</font>&nbsp;&nbsp;</label>
										                <div class="controls">
											                <asp:TextBox ID="txtName" runat="server" class="span6 m-wrap"></asp:TextBox>
										                </div>
									                </div>							
                                                    <div class="control-group">
										                <label class="control-label" >ใช้งาน &nbsp;&nbsp;</label>
										                <div class="controls">
                                                            <asp:ImageButton ID="imgStatus" runat="server" ImageUrl="images/check.png" ToolTip="Click เพื่อเปลี่ยน" />
										                </div>
									                </div> 
									                <div class="form-actions">
                                                        <asp:Button ID="btnSave" runat="server" CssClass="btn blue" Text="บันทึก" />
                                                        <asp:Button ID="btnCancel" runat="server"  CssClass="btn" Text="ยกเลิก" />
									                </div>
								                </form>

								                <!-- END FORM-->
							                </div>
						                </div>
						                <!-- END VALIDATION STATES-->
					                </div>
				                </div>
                                </asp:Panel>
						</div>
						<!-- END SAMPLE TABLE PORTLET-->
					</div>
				</div>

                
</div>
    <%--</ContentTemplate>
    </asp:UpdatePanel>--%>
    <script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>	
	<script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.js" type="text/javascript" ></script>
	<script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>

</asp:Content>

