﻿

Imports System.Data
Imports System.Data.SqlClient

Partial Class NoApprovedVenderList
    Inherits System.Web.UI.Page

    Dim BL As New AVLBL
    Dim GL As New GenericLib

    Private ReadOnly Property PageName As String
        Get
            Return "NoApprovedVenderList.aspx"
        End Get
    End Property

    Private ReadOnly Property User_ID As Integer
        Get
            Try
                If IsNumeric(Session("User_ID")) Then
                    Return Session("User_ID")
                Else
                    Return -1
                End If
            Catch ex As Exception
                Return -1
            End Try
        End Get
    End Property

    Private Property AccessMode As AVLBL.AccessRole
        Get
            Try
                Return ViewState("AccessMode")
            Catch ex As Exception
                Return AVLBL.AccessRole.None
            End Try
        End Get
        Set(value As AVLBL.AccessRole)
            ViewState("AccessMode") = value
        End Set
    End Property

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("User_ID")) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "ShowAlert('warning','กรุณาล็อกอินเข้าสู่ระบบ','Login.aspx');", True)
            Response.Redirect("Login.aspx")
            Exit Sub
        End If

        If Not IsPostBack Then
            SetUserRole()
            BL.BindDDlDEPT(ddl_Search_Dept, User_ID)
            ClearSearchForm()
            BindList()

        End If


    End Sub

    Private Sub SetUserRole()

        '----------- Check สิทธิ์ในเมนู --------------
        Dim MT As DataTable = BL.GetUserMenuRole(User_ID, PageName)
        If MT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลนี้','Overview.aspx');", True)
            Exit Sub
        End If
        AccessMode = MT.Rows(0).Item("MR_ID") '--------- สิทธิ์ในเมนูของผู้ใช้คนปัจจุบัน -----------
        If AccessMode = AVLBL.AccessRole.None Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลนี้','Overview.aspx');", True)
            Exit Sub
        End If

    End Sub

    Private Sub ClearSearchForm()
        ddlStart_M.Items.Clear()
        ddlEnd_M.Items.Clear()
        For i As Integer = 1 To 12
            ddlStart_M.Items.Add(New ListItem(GL.ReportMonthThaiShort(i), i))
            ddlEnd_M.Items.Add(New ListItem(GL.ReportMonthThaiShort(i), i))
        Next
        ddlEnd_M.SelectedIndex = 11

        ddlStart_Y.Items.Clear()
        ddlEnd_Y.Items.Clear()
        For i As Integer = Now.Year - 2 To Now.Year + 8
            ddlStart_Y.Items.Add(New ListItem(i + 543, i))
            ddlEnd_Y.Items.Add(New ListItem(i + 543, i))
        Next
        ddlEnd_Y.SelectedIndex = ddlEnd_Y.Items.Count - 1
    End Sub

    Protected Sub ddlPeriod_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlEnd_M.SelectedIndexChanged, ddlEnd_Y.SelectedIndexChanged, ddlStart_M.SelectedIndexChanged, ddlStart_Y.SelectedIndexChanged

        Dim StartMonth As Integer = (ddlStart_Y.SelectedValue * 12) + ddlStart_M.SelectedValue
        Dim EndMonth As Integer = (ddlEnd_Y.SelectedValue * 12) + ddlEnd_M.SelectedValue

        If StartMonth > EndMonth Then
            Select Case True
                Case Equals(sender, ddlStart_M) Or Equals(sender, ddlStart_Y)
                    ddlEnd_M.SelectedIndex = ddlStart_M.SelectedIndex
                    ddlEnd_Y.SelectedIndex = ddlStart_Y.SelectedIndex
                Case Equals(sender, ddlEnd_M) Or Equals(sender, ddlEnd_Y)
                    ddlStart_M.SelectedIndex = ddlEnd_M.SelectedIndex
                    ddlStart_Y.SelectedIndex = ddlEnd_Y.SelectedIndex
            End Select
        End If

    End Sub


    Protected Sub Search_Changed(sender As Object, e As System.EventArgs) Handles btnSearch.Click ', ddl_Search_Dept.SelectedIndexChanged, txt_Search_Item.TextChanged, txt_Search_Sup.TextChanged, txt_Search_Ref_Code.TextChanged
        BindList()
    End Sub
#Region "BindData"

    Private Function GetSQL_Ass() As String
        Dim SQL As String = "SELECT DISTINCT Ass_ID,Ref_Code Ass_Ref_Code,CAST(Null AS INT) ReAss_ID,CAST(Null AS VARCHAR) ReAss_Ref_Code" & vbLf
        SQL &= " ,Item_No,Item_Name,Cat_No + Sub_Cat_No + Type_No+ Running_No Item_Code" & vbLf
        SQL &= " ,Dept_ID,Dept_Name,S_ID,S_Name,S_Tax_No,S_Alias,S_Contact_Name" & vbLf
        SQL &= " ,dbo.udf_AVLCode(AVL_Y,AVL_M,AVL_Dept_ID,AVL_Sub_Dept_ID,AVL_No) AVL_Code" & vbLf
        SQL &= " ,AVL_Status,Get_Score,Pass_Score,Max_Score,Current_Step,Step_TH,Update_Time,vw_Ass_Header.Sub_Dept_ID,vw_Ass_Header.Sub_Dept_Name " & vbLf
        SQL &= " FROM vw_Ass_Header" & vbLf

        Dim Filter As String = ""
        If ddl_Search_Dept.SelectedIndex > 0 Then
            Filter &= " Dept_ID='" & ddl_Search_Dept.SelectedValue & "' AND "
        Else
            Filter &= " Dept_ID IN (" & vbLf
            Filter &= " SELECT D.Dept_ID FROM tb_Dept_Role R" & vbLf
            Filter &= " INNER JOIN tb_User U ON R.User_ID=U.User_ID AND U.Active_Status=1" & vbLf
            Filter &= " INNER JOIN tb_Dept D ON R.Dept_ID=D.Dept_ID AND D.Active_Status=1" & vbLf
            Filter &= " INNER JOIN tb_Ass_Role AR ON R.AR_ID=AR.AR_ID" & vbLf
            Filter &= " WHERE R.User_ID=" & User_ID & vbLf
            Filter &= " ) AND "
        End If
        If ddl_Search_SUB_DEPT.SelectedIndex > 0 Then
            Filter &= " vw_Ass_Header.Sub_Dept_ID ='" & ddl_Search_SUB_DEPT.SelectedValue & "' AND "
        End If
        If txt_Search_Item.Text <> "" Then
            Filter &= " (" & vbLf
            Filter &= " Item_No LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Item_Name LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " CAT_Name LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Sub_Cat_Name LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Type_Name LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR" & vbLf
            Filter &= " Running_Name LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR" & vbLf
            Filter &= " Cat_No + Sub_Cat_No+Type_No+Running_No LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' " & vbLf
            Filter &= ") AND "
        End If
        If txt_Search_Sup.Text <> "" Then
            Filter &= " (" & vbLf
            Filter &= " S_Tax_No LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Name LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Alias LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Contact_Name LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Address LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Replace(Replace(S_Phone,'-',''),',','') LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Replace(Replace(S_Fax,'-',''),',','') LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Contact_Name LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Email LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' " & vbLf
            Filter &= ") AND "
        End If
        If txt_Search_Ref_Code.Text <> "" Then
            Filter &= " (Ref_Code LIKE '%" & txt_Search_Ref_Code.Text.Replace("'", "''") & "%')  AND" & vbLf
        End If
        If Filter <> "" Then
            SQL &= "WHERE " & Filter.Substring(0, Filter.Length - 4) & vbLf
        End If

        Return SQL
    End Function

    Private Function GetSQL_ReAss() As String
        Dim SQL As String = "SELECT DISTINCT Ass_ID,dbo.udf_AssCode(Ass_Ref_Year,Ass_Ref_Month,Dept_ID,Ass_Ref_Number) Ass_Ref_Code,ReAss_ID,Ref_Code ReAss_Ref_Code" & vbLf
        SQL &= " ,Item_No,Item_Name,Cat_No + Sub_Cat_No + Type_No+ Running_No Item_Code" & vbLf
        SQL &= " ,Dept_ID,Dept_Name,S_ID,S_Name,S_Tax_No,S_Alias,S_Contact_Name" & vbLf
        SQL &= " ,dbo.udf_AVLCode(AVL_Y,AVL_M,AVL_Dept_ID,AVL_Sub_Dept_ID,AVL_No) AVL_Code" & vbLf
        SQL &= " ,AVL_Status,Get_Score,Pass_Score,Max_Score,Current_Step,Step_TH,Update_Time,vw_ReAss_Header.Sub_Dept_ID,vw_ReAss_Header.Sub_Dept_Name " & vbLf
        SQL &= " FROM vw_ReAss_Header" & vbLf

        Dim Filter As String = ""
        If ddl_Search_Dept.SelectedIndex > 0 Then
            Filter &= " Dept_ID='" & ddl_Search_Dept.SelectedValue & "' AND "
        Else
            Filter &= " Dept_ID IN (" & vbLf
            Filter &= " SELECT D.Dept_ID FROM tb_Dept_Role R" & vbLf
            Filter &= " INNER JOIN tb_User U ON R.User_ID=U.User_ID AND U.Active_Status=1" & vbLf
            Filter &= " INNER JOIN tb_Dept D ON R.Dept_ID=D.Dept_ID AND D.Active_Status=1" & vbLf
            Filter &= " INNER JOIN tb_Ass_Role AR ON R.AR_ID=AR.AR_ID" & vbLf
            Filter &= " WHERE R.User_ID=" & User_ID & vbLf
            Filter &= " ) AND "
        End If
        If ddl_Search_SUB_DEPT.SelectedIndex > 0 Then
            Filter &= " vw_ReAss_Header.Sub_Dept_ID ='" & ddl_Search_SUB_DEPT.SelectedValue & "' AND "
        End If
        If txt_Search_Item.Text <> "" Then
            Filter &= " (" & vbLf
            Filter &= " Item_No LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Item_Name LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " CAT_Name LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Sub_Cat_Name LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Type_Name LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR" & vbLf
            Filter &= " Running_Name LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR" & vbLf
            Filter &= " Cat_No + Sub_Cat_No+Type_No+Running_No LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' " & vbLf
            Filter &= ") AND "
        End If
        If txt_Search_Sup.Text <> "" Then
            Filter &= " (" & vbLf
            Filter &= " S_Tax_No LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Name LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Alias LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Contact_Name LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Address LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Replace(Replace(S_Phone,'-',''),',','') LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Replace(Replace(S_Fax,'-',''),',','') LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Contact_Name LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Email LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' " & vbLf
            Filter &= ") AND "
        End If
        If txt_Search_Ref_Code.Text <> "" Then
            Filter &= " (Ref_Code LIKE '%" & txt_Search_Ref_Code.Text.Replace("'", "''") & "%')  AND" & vbLf
        End If

       
        If Filter <> "" Then
            SQL &= " WHERE " & Filter.Substring(0, Filter.Length - 4) & vbLf
        End If

        Return SQL
    End Function


    Private Sub BindList()

        Dim StartMonth As Integer = (ddlStart_Y.SelectedValue * 12) + ddlStart_M.SelectedValue
        Dim EndMonth As Integer = (ddlEnd_Y.SelectedValue * 12) + ddlEnd_M.SelectedValue
        Dim SQL As String = "DECLARE @S AS INT=" & StartMonth & vbLf
        SQL &= "DECLARE @E AS INT=" & EndMonth & vbLf & vbLf

        SQL &= " SELECT * FROM ( "
        SQL &= " SELECT "
        SQL &= " RANK() OVER(PARTITION BY Ass_ID,Current_Step ORDER BY ReAss_Ref_Code DESC) LastAss, "


        SQL &= " *,CASE WHEN A.ReAss_ID IS NULL THEN A.Ass_Ref_Code ELSE A.ReAss_Ref_Code END Ref_Code FROM " & vbLf
        SQL &= "(" & vbLf
        SQL &= GetSQL_Ass() & vbLf
        SQL &= "UNION ALL" & vbLf
        SQL &= GetSQL_ReAss() & vbLf
        SQL &= ") A WHERE AVL_Code IS NULL AND Current_Step=5" & vbLf
        Dim Filter As String = " dbo.UDF_IsRangeIntersected(@S,@E,Month(Update_Time)+(Year(Update_Time)*12),Month(Update_Time)+(Year(Update_Time)*12))=1  AND" & vbLf
        If Filter <> "" Then
            SQL &= " AND " & Filter.Substring(0, Filter.Length - 4) & vbLf
        End If
        SQL &= " ) B WHERE LastAss =1  AND Ass_ID NOT IN ( SELECT Fail_Ass_ID FROM tb_Ass_Header  WHERE Fail_Ass_ID IS NOT NULL ) "
        SQL &= "ORDER BY Dept_ID, Ass_Ref_Code"

        Dim Title As String = ""

        If ddl_Search_Dept.SelectedIndex > 0 Then
            Title &= " " & ddl_Search_Dept.Items(ddl_Search_Dept.SelectedIndex).Text
        End If
        If ddl_Search_SUB_DEPT.SelectedIndex > 0 Then
            Title &= " ของฝ่าย/ภาค/งาน " & ddl_Search_SUB_DEPT.Items(ddl_Search_SUB_DEPT.SelectedIndex).Text
        End If
        If txt_Search_Item.Text <> "" Then
            Title &= " พัสดุ/ครุภัณฑ์ : " & txt_Search_Item.Text
        End If
        If txt_Search_Sup.Text <> "" Then
            Title &= " ผู้ขาย : " & txt_Search_Sup.Text
        End If
        If txt_Search_Ref_Code.Text <> "" Then
            Title &= " เลขใบประเมิน : " & txt_Search_Ref_Code.Text
        End If

        Title &= " ระหว่างเดือน " & ddlStart_M.Items(ddlStart_M.SelectedIndex).Text & " " & ddlStart_Y.Items(ddlStart_Y.SelectedIndex).Text
        Title &= " ถึง "
        Title &= ddlStart_M.Items(ddlEnd_M.SelectedIndex).Text & " " & ddlEnd_Y.Items(ddlEnd_Y.SelectedIndex).Text & " "

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        DA.Fill(DT)

        Session("NoApprovedVenderList") = DT
        Session("NoApprovedVenderList_Title") = Title

        lblTotalRecord.Text = Title
        If DT.Rows.Count = 0 Then
            lblTotalRecord.Text &= " ไม่พบรายการดังกล่าว"
        Else
            lblTotalRecord.Text &= " พบ " & FormatNumber(DT.Rows.Count, 0) & " รายการ"
        End If
        LastDept = ""
        '------------- Binding To List ---------------
        Pager.SesssionSourceName = "NoApprovedVenderList"
        Pager.RenderLayout()

    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
    End Sub


#End Region

    Dim LastDept As String = ""
    Protected Sub rptList_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblAVL As Label = e.Item.FindControl("lblAVL")
        Dim lblSup As Label = e.Item.FindControl("lblSup")
        Dim lblTax As Label = e.Item.FindControl("lblTax")
        Dim lblItem As Label = e.Item.FindControl("lblItem")
        Dim lblSub_Dept_Name As Label = e.Item.FindControl("lblSub_Dept_Name")
        Dim lblUpdate As Label = e.Item.FindControl("lblUpdate")
        Dim lnkRef As HtmlAnchor = e.Item.FindControl("lnkRef")

        Dim trDept As HtmlTableRow = e.Item.FindControl("trDept")
        Dim lblDept As Label = e.Item.FindControl("lblDept")
        If LastDept <> e.Item.DataItem("Dept_Name").ToString Then
            LastDept = e.Item.DataItem("Dept_Name").ToString
            lblDept.Text = LastDept
            trDept.Visible = True
        Else
            trDept.Visible = False
        End If
        lblDept.Attributes("Dept_ID") = e.Item.DataItem("Dept_ID").ToString()

        lblAVL.Text = e.Item.DataItem("AVL_Code").ToString()
        lblSup.Text = e.Item.DataItem("S_Name").ToString()
        lblTax.Text = e.Item.DataItem("S_Tax_No").ToString()
        lblItem.Text = "(" & e.Item.DataItem("Item_Code").ToString() & ") " & e.Item.DataItem("Item_Name").ToString()
        lblSub_Dept_Name.Text = e.Item.DataItem("Sub_Dept_Name").ToString()
        lblUpdate.Text = GL.ReportThaiPassTime(e.Item.DataItem("Update_Time"))

        lnkRef.HRef = ""
        Dim Ref As String = e.Item.DataItem("Ref_Code").ToString.Substring(0, 1)
        Select Case Ref
            Case "A"
                lnkRef.HRef = "Assessment_Edit.aspx?Ass_ID=" & e.Item.DataItem("Ass_ID").ToString
                lnkRef.Style("color") = "Teal"
            Case "R"
                lnkRef.HRef = "Reassessment_Edit.aspx?Ass_ID=" & e.Item.DataItem("Ass_ID").ToString & "&ReAss_ID=" & e.Item.DataItem("ReAss_ID").ToString
                lnkRef.Style("color") = "Blue"

        End Select
        lnkRef.InnerText = e.Item.DataItem("Ref_Code").ToString

    End Sub

#Region "PrintButton"

    Protected Sub btnPDF_Click(sender As Object, e As System.EventArgs) Handles btnPDF.Click
        Session("RPT_NoApprovedVenderList_Count") = lblTotalRecord.Text
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/RPT_NoApprovedVenderList.aspx?Mode=PDF" & "');", True)

    End Sub
    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        Session("RPT_NoApprovedVenderList_Count") = lblTotalRecord.Text
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/RPT_NoApprovedVenderList.aspx?Mode=EXCEL" & "');", True)

    End Sub
#End Region


    Protected Sub ddl_Search_Dept_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddl_Search_Dept.SelectedIndexChanged
       
        BL.BindDDlSubDept_Search(ddl_Search_SUB_DEPT, ddl_Search_Dept.SelectedValue)
        ddl_Search_SUB_DEPT.SelectedIndex = 0
    End Sub


End Class

