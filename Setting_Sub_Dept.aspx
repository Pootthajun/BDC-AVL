﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Setting_Sub_Dept.aspx.vb" Inherits="Setting_Sub_Dept" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax"%>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link type="text/css" rel="stylesheet" href="assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <Ajax:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></Ajax:ToolkitScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
    <div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3>
							ฝ่าย/ภาค/งาน
						</h3>			
						<ul class="breadcrumb">
                            
                            <li><i class="icon-cogs"></i> <a href="javascript:;">ตั้งค่าระบบ</a><i class="icon-angle-right"></i></li>
                        	<li><i class="icon-sitemap"></i> <a href="javascript:;">ฝ่าย/ภาค/งาน</a></li>
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->	
				     </div>				
			    </div>
                <asp:Panel ID="pnlList" runat="server" DefaultButton="btnSearch"> 

                <div class="row-fluid">
               <div class="span12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
						<div class="portlet ">
							<div class="portlet-body">
                                <div class="table-toolbar">
									<div class="btn-group">
                                        <asp:LinkButton CssClass="btn green" id="btnAdd" runat="server">
										    เพิ่ม <i class="icon-plus"></i>
										    </asp:LinkButton>
									</div>
								<div class="btn-group">
                                        <asp:LinkButton CssClass="btn blue" id="btnSearch" runat="server">
										    ค้นหา <i class="icon-search"></i>
										    </asp:LinkButton>
									</div>
								</div>
                                 <%--แบ่งส่วนการค้นหา--%>
                                <fieldset>
                                    <legend><b>ค้นหา</b></legend>
                                </fieldset>
                                <div class="row-fluid form-horizontal">
									<div class="span5 ">
									<div class="control-group">
										<label class="control-label"> สำนักงาน</label>
										<div class="controls">
                                            <asp:DropDownList ID="ddlDept_Search" runat="server" CssClass="m-wrap large" >
											</asp:DropDownList>	
										</div>
									</div>
								</div>	
								<div class="span6 ">
									<div class="control-group">
										<label class="control-label"> ฝ่าย/ภาค/งาน</label>
										<div class="controls">
                                            <asp:TextBox ID="txt_Search_Name" runat="server"  CssClass="m-wrap large" placeholder="ค้นหาจากชื่อฝ่าย/ภาค/งาน"></asp:TextBox>
										</div>
									</div>
								</div>	
								</div>
                                <div class="row-fluid form-horizontal">
									<div class="span5 ">
										<div class="control-group">
											<label class="control-label"> สถานะ</label>
											<div class="controls">
												<asp:DropDownList ID="ddlStatus" runat="server" CssClass="m-wrap large">
				                                    <asp:ListItem Value="All" Selected="true">ทั้งหมด</asp:ListItem>
				                                    <asp:ListItem Value="1">ใช้งาน</asp:ListItem>
				                                    <asp:ListItem Value="0">ไม่ใช้งาน</asp:ListItem>
			                                    </asp:DropDownList>
											</div>
										</div>
									</div>	
									<div class="span6 ">
										<div class="control-group">
											
										</div>
									</div>	
								</div>
                                <div class="portlet-body no-more-tables">
                                    <asp:Label ID="lblTotalList" runat="server" Width="100%" Font-Size="16px" Font-Bold="True" style="text-align:center;"></asp:Label>    
                           
								    <table  class="table table-bordered table-advance"> 
<%--								    <table class="table table-full-width table-advance dataTable no-more-tables table-hover"> 
--%>                                                                   
									    <thead>
										    <tr>
											    <th style="text-align:center; vertical-align:middle;">สำนักงาน</th>
											    <th style="text-align:center; vertical-align:middle;">ฝ่าย/ภาค/งาน</th>
											    <th style="text-align:center; vertical-align:middle;" width="50">ใช้</th>
											    <th  id="thEdit_Header" runat="server" style="text-align:center; vertical-align:middle;" width="130">ดำเนินการ</th>
										    </tr>
									    </thead>
									    <tbody>
                                            <asp:Repeater ID="rptList" runat="server">
										        <ItemTemplate>
										        <tr>
											        <td data-title="สำนักงาน" id="tdDept" runat ="server"  style="border-right:1px solid #eeeeee; " >
                                                        <asp:Label ID="lblDept_Name" runat ="server"></asp:Label>
                                                        <asp:Label ID="lblDept_ID" runat="server" style=" display :none;"></asp:Label>
                                                    </td>
											        <td data-title="ฝ่าย/ภาค/งาน"  style="border-right:1px solid #eeeeee; " >
                                                        <asp:Label ID="lblSub_Dept_Name" runat ="server"></asp:Label>
                                                        <asp:Label ID="lblSub_Dept_ID" runat="server" style=" display :none;"></asp:Label>
                                                    </td>
											        <td data-title="ใช้" style="text-align:center;border-right:1px solid #eeeeee; " >
                                                        <a ID="img_Status" runat="server" target="_blank"></a>
                                                    </td>
											        <td  id="tdEdit_List" runat="server" data-title="ดำเนินการ" style="text-align:center;">
                                                        <asp:LinkButton ID="btnRptEdit" runat="server" CssClass="btn mini purple" CommandName="Edit"><i class="icon-edit"></i> แก้ไข</asp:LinkButton>
                                                        <asp:LinkButton ID="btnRptDelete" runat="server" CssClass="btn mini black"  CommandName="Delete"><i class="icon-trash"></i> ลบ</asp:LinkButton>
                                                            <Ajax:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" Enabled="true" ConfirmText="คุณต้องการลบข้อมูล ใช่หรือไม่?" TargetControlID="btnRptDelete">
                                                            </Ajax:ConfirmButtonExtender>
                                                    </td>
										        </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
									    </tbody>
								    </table>
                                    <asp:PageNavigation ID="Pager" MaximunPageCount="5" PageSize="20" runat="server" />
                                </div> 
							</div>
						</div>
						<!-- END SAMPLE TABLE PORTLET-->
					</div>
				</div>
                </asp:Panel>

                <asp:Panel ID="pnlEdit" runat="server" DefaultButton="btnSave" Visible="false">
                <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN VALIDATION STATES-->
						<div class="portlet box blue">
							<div class="portlet-title">
								<div class="caption"><i class="icon-user"></i>เพิ่ม/แก้ไข ฝ่าย/ภาค/งาน</div>
								<div class="tools">
								</div>
							</div>
							<div class="portlet-body form form-horizontal">
                                
                                <form class="form-horizontal" id="form_sample_2" action="#" novalidate="novalidate">
                                    <div class="control-group">
										<label class="control-label">สำนักงาน <font color="red">*</font>&nbsp;&nbsp;</label>
										<div class="controls">
                                            <asp:Label ID="lblDept_ID_Edit" runat="server"  style="display:none;"  ></asp:Label>
											<asp:DropDownList ID="ddlDept_Edit" runat="server"  CssClass="span6 m-wrap" AutoPostBack="true">
                                            </asp:DropDownList> 
										</div>
									</div>
                                    <div class="control-group">
										<label class="control-label">รหัสฝ่าย/ภาค/งาน <font color="red">*</font>&nbsp;&nbsp;</label>
										<div class="controls">
											<asp:TextBox ID="txtSub_Dept_ID"  runat="server" class="span6 m-wrap"  placeholder="กรอกรหัส 2 หลัก"  MaxLength="2" AutoPostBack ="true" ></asp:TextBox> 
										</div>
									</div> 
									<div class="control-group">
										<label class="control-label">ฝ่าย/ภาค/งาน <font color="red">*</font>&nbsp;&nbsp;</label>
										<div class="controls">
                                            <asp:Label ID="lblSubDept_ID_Edit" runat="server"  style="display:none;"  ></asp:Label>
											<asp:TextBox ID="txtSub_Dept_Name"  runat="server" class="span6 m-wrap"  AutoPostBack ="true" ></asp:TextBox> 
										</div>
									</div>                             								
                                    <div class="control-group">
										<label class="control-label" >ใช้งาน &nbsp;&nbsp;</label>
										<div class="controls">
                                           <asp:ImageButton ID="imgStatus" runat="server" ImageUrl="images/check.png" ToolTip="Click เพื่อเปลี่ยน" />
										</div>
									</div> 
									<div class="form-actions">
                                        <asp:Button ID="btnSave" runat="server" CssClass="btn blue" Text="บันทึก" />
										<asp:Button ID="btnCancel" runat="server"  CssClass="btn" Text="ยกเลิก" />
									</div>
								</form>

								<!-- END FORM-->
							</div>
						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>
                </asp:Panel>

</div>
    </ContentTemplate> 
    </asp:UpdatePanel>
    <script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>	
	<script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.js" type="text/javascript" ></script>
	<script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
    <script type="text/javascript" src="assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
</asp:Content>


