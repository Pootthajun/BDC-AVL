﻿
Imports System.Data
Imports System.Data.SqlClient

Partial Class BlackList_List
    Inherits System.Web.UI.Page

    Dim BL As New AVLBL
    Dim GL As New GenericLib

    Private ReadOnly Property PageName As String
        Get
            Return "BlackList_List.aspx"
        End Get
    End Property

    Private Property S_ID As Integer
        Get
            Try
                Return pnlEdit.Attributes("S_ID")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Integer)
            pnlEdit.Attributes("S_ID") = value
        End Set
    End Property

    Private ReadOnly Property User_ID As Integer
        Get
            Try
                If IsNumeric(Session("User_ID")) Then
                    Return Session("User_ID")
                Else
                    Return -1
                End If
            Catch ex As Exception
                Return -1
            End Try
        End Get
    End Property

    Private Property AccessMode As AVLBL.AccessRole
        Get
            Try
                Return ViewState("AccessMode")
            Catch ex As Exception
                Return AVLBL.AccessRole.None
            End Try
        End Get
        Set(value As AVLBL.AccessRole)
            ViewState("AccessMode") = value
        End Set
    End Property

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("User_ID")) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "ShowAlert('warning','กรุณาล็อกอินเข้าสู่ระบบ','Login.aspx');", True)
            Response.Redirect("Login.aspx")
            Exit Sub
        End If

        If Not IsPostBack Then
            SetUserRole()
            ClearSearchForm()
            ClearFormData()
            BindBlackList()
        End If
    End Sub

    Private Sub SetUserRole()

        '----------- Check สิทธิ์ในเมนู --------------
        Dim MT As DataTable = BL.GetUserMenuRole(User_ID, PageName)
        If MT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลนี้','Overview.aspx');", True)
            Exit Sub
        End If
        AccessMode = MT.Rows(0).Item("MR_ID") '--------- สิทธิ์ในเมนูของผู้ใช้คนปัจจุบัน -----------
        If AccessMode = AVLBL.AccessRole.None Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลนี้','Overview.aspx');", True)
            Exit Sub
        End If

        lnkAdd1.Visible = AccessMode = AVLBL.AccessRole.Edit
        txtEdit_Start.Enabled = AccessMode = AVLBL.AccessRole.Edit
        txtEdit_End.Enabled = AccessMode = AVLBL.AccessRole.Edit
        txtComment.Enabled = AccessMode = AVLBL.AccessRole.Edit
        btnSave.Enabled = AccessMode = AVLBL.AccessRole.Edit
        btnDelete.Enabled = AccessMode = AVLBL.AccessRole.Edit
    End Sub

    Private Sub ClearSearchForm()
        ddlStart_M.Items.Clear()
        ddlEnd_M.Items.Clear()
        For i As Integer = 1 To 12
            ddlStart_M.Items.Add(New ListItem(GL.ReportMonthThaiShort(i), i))
            ddlEnd_M.Items.Add(New ListItem(GL.ReportMonthThaiShort(i), i))
        Next
        ddlEnd_M.SelectedIndex = 11

        ddlStart_Y.Items.Clear()
        ddlEnd_Y.Items.Clear()
        For i As Integer = Now.Year - 2 To Now.Year + 8
            ddlStart_Y.Items.Add(New ListItem(i + 543, i))
            ddlEnd_Y.Items.Add(New ListItem(i + 543, i))
        Next
        ddlEnd_Y.SelectedIndex = ddlEnd_Y.Items.Count - 1
    End Sub

    Private Sub ClearFormData()
        S_ID = 0
        txt_S_Fullname.Text = ""
        txt_S_Alias.Text = ""
        txt_S_Tax_No.Text = ""
        txt_S_Address.Text = ""
        txt_S_Contact_Name.Text = ""
        txt_S_Email.Text = ""
        txt_S_Phone.Text = ""
        txt_S_Fax.Text = ""
        txtEdit_Start.Text = ""
        txtEdit_End.Text = ""
        txtComment.Text = ""
        btnSupplierDialog.Visible = AccessMode = AVLBL.AccessRole.Edit
    End Sub

    Private Sub BindBlackList()

        '------------- Time Condition--------------
        Dim StartMonth As Integer = (ddlStart_Y.SelectedValue * 12) + ddlStart_M.SelectedValue
        Dim EndMonth As Integer = (ddlEnd_Y.SelectedValue * 12) + ddlEnd_M.SelectedValue

        Dim Title As String = ""

        Dim SQL As String = "DECLARE @S AS INT=" & StartMonth & vbLf
        SQL &= "DECLARE @E AS INT=" & EndMonth & vbLf & vbLf
        SQL &= "SELECT Sup.S_ID,Sup.S_Name,Sup.S_Alias,Sup.S_Tax_No,Sup.S_Contact_Name,Sup.S_Phone,Sup.S_Email" & vbLf
        SQL &= " ,BL.B_StartDate,BL.B_EndDate,BL.B_Comment" & vbLf
        SQL &= " FROM tb_Sup Sup INNER JOIN tb_BlackList BL ON Sup.S_ID=BL.S_ID" & vbLf
        Dim Filter As String = "dbo.UDF_IsRangeIntersected(@S,@E,Month(B_StartDate)+(Year(B_StartDate)*12),Month(B_EndDate)+(Year(B_EndDate)*12))=1 AND " & vbLf
        If txt_Search_Name.Text <> "" Then
            Filter &= " (Sup.S_Name LIKE '%" & txt_Search_Name.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Sup.S_Alias LIKE '%" & txt_Search_Name.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Sup.S_Contact_Name LIKE '%" & txt_Search_Name.Text.Replace("'", "''") & "%' )" & vbLf
            Filter &= " AND " & vbLf
            Title &= " ชื่อ : " & txt_Search_Name.Text
        End If
        If txt_Search_TaxNo.Text <> "" Then
            Filter &= " Supplier.S_Tax_No LIKE '%" & txt_Search_TaxNo.Text.Replace("'", "''") & "%' AND " & vbLf
            Title &= " เลขประจำตัวผู้เสียภาษี : " & txt_Search_Name.Text
        End If
        Title &= " รายที่ติด Black List ในช่วง " & ddlStart_M.Items(ddlStart_M.SelectedIndex).Text & " " & ddlStart_Y.Items(ddlStart_Y.SelectedIndex).Text
        Title &= " ถึง "
        Title &= ddlStart_M.Items(ddlEnd_M.SelectedIndex).Text & " " & ddlEnd_Y.Items(ddlEnd_Y.SelectedIndex).Text & " "

        If Filter <> "" Then SQL &= " WHERE " & Filter.Substring(0, Filter.Length - 5) & vbLf
        SQL &= " ORDER BY Sup.S_Name"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        Session("ReportBlackList") = DT
        Session("ReportBlackList_Title") = Title

        Pager.SesssionSourceName = "ReportBlackList"
        Pager.RenderLayout()

        lblCountList.Text = Title
        If DT.Rows.Count = 0 Then
            lblCountList.Text &= "ไม่พบรายการดังกล่าว"
        Else
            lblCountList.Text &= "พบ " & FormatNumber(DT.Rows.Count, 0) & " รายการ"
        End If

        pnlEdit.Visible = False
        pnlData.Visible = True
        dialogSupplier.Visible = False

    End Sub

    Protected Sub Pager_PageChanging(ByVal Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptData
        lnkAdd2.Visible = lnkAdd1.Visible And rptData.Items.Count > 10
    End Sub
    Protected Sub btnSearch_Click(sender As Object, e As System.EventArgs) Handles btnSearch.Click, txt_Search_TaxNo.TextChanged, txt_Search_Name.TextChanged

        BindBlackList()
    End Sub

    Protected Sub ddlPeriod_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlEnd_M.SelectedIndexChanged, ddlEnd_Y.SelectedIndexChanged, ddlStart_M.SelectedIndexChanged, ddlStart_Y.SelectedIndexChanged

        Dim StartMonth As Integer = (ddlStart_Y.SelectedValue * 12) + ddlStart_M.SelectedValue
        Dim EndMonth As Integer = (ddlEnd_Y.SelectedValue * 12) + ddlEnd_M.SelectedValue

        If StartMonth > EndMonth Then
            Select Case True
                Case Equals(sender, ddlStart_M) Or Equals(sender, ddlStart_Y)
                    ddlEnd_M.SelectedIndex = ddlStart_M.SelectedIndex
                    ddlEnd_Y.SelectedIndex = ddlStart_Y.SelectedIndex
                Case Equals(sender, ddlEnd_M) Or Equals(sender, ddlEnd_Y)
                    ddlStart_M.SelectedIndex = ddlEnd_M.SelectedIndex
                    ddlStart_Y.SelectedIndex = ddlEnd_Y.SelectedIndex
            End Select
        End If

        BindBlackList()
    End Sub

    Protected Sub rptData_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptData.ItemCommand
        Select Case e.CommandName
            Case "Edit"
                ClearFormData()
                S_ID = e.CommandArgument
                Dim ST As DataTable = BL.GetSupplierDetail(S_ID)
                If ST.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','ไม่พบข้อมูลผู้ขายรายนี้','');", True)
                    Exit Sub
                End If
                '--------------- Update ข้อมูลเข้าไปในฟอร์ม ------------------------
                txt_S_Fullname.Text = ST.Rows(0).Item("S_Name").ToString
                txt_S_Alias.Text = ST.Rows(0).Item("S_Alias").ToString
                txt_S_Tax_No.Text = ST.Rows(0).Item("S_Tax_No").ToString
                txt_S_Address.Text = ST.Rows(0).Item("S_Address").ToString
                txt_S_Contact_Name.Text = ST.Rows(0).Item("S_Contact_Name").ToString
                txt_S_Email.Text = ST.Rows(0).Item("S_Email").ToString
                txt_S_Phone.Text = ST.Rows(0).Item("S_Phone").ToString
                txt_S_Fax.Text = ST.Rows(0).Item("S_Fax").ToString

                ST = New DataTable
                Dim DA As New SqlDataAdapter("SELECT * FROM tb_BlackList WHERE S_ID=" & S_ID, BL.ConnectionString)
                DA.Fill(ST)
                If ST.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','ผู้ขายรายนี้ไม่ได้ถูก Black List แล้ว','');", True)
                    BindBlackList()
                    Exit Sub
                End If
                Dim StartDate As DateTime = ST.Rows(0).Item("B_StartDate")
                Dim EndDate As DateTime = ST.Rows(0).Item("B_EndDate")
                txtEdit_Start.Text = StartDate.ToString("dd MMM yyyy")
                txtEdit_End.Text = EndDate.ToString("dd MMM yyyy")
                txtComment.Text = ST.Rows(0).Item("B_Comment").ToString

                '--------------- Bind Button ------------
                btnSupplierDialog.Visible = False
                btnSave.Visible = AccessMode = AVLBL.AccessRole.Edit
                btnDelete.Visible = AccessMode = AVLBL.AccessRole.Edit

                cfm_Delete.ConfirmText = "ยืนยันยกเลิก Black List " & txt_S_Fullname.Text & "?"

                pnlData.Visible = False
                pnlEdit.Visible = True

            Case "Delete"
                Dim Conn As New SqlConnection(BL.ConnectionString)
                Conn.Open()
                Dim Comm As New SqlCommand
                With Comm
                    .CommandType = CommandType.Text
                    .Connection = Conn
                    .CommandText = "DELETE FROM tb_BlackList_File WHERE S_ID=" & e.CommandArgument & vbLf
                    .CommandText &= "DELETE FROM tb_BlackList WHERE S_ID=" & e.CommandArgument & vbLf
                    .ExecuteNonQuery()
                    .Dispose()
                End With
                Conn.Close()
                Conn.Dispose()
                BindBlackList()
        End Select
    End Sub

    Protected Sub rptData_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lbl_Name As Label = e.Item.FindControl("lbl_Name")
        Dim lbl_TaxNo As Label = e.Item.FindControl("lbl_TaxNo")
        Dim lblId As Label = e.Item.FindControl("lblId")
        Dim lbl_NameAlias As Label = e.Item.FindControl("lbl_NameAlias")
        Dim lbl_ContactName As Label = e.Item.FindControl("lbl_ContactName")
        Dim lblPeriod As Label = e.Item.FindControl("lblPeriod")
        Dim lblB_Comment As Label = e.Item.FindControl("lblB_Comment")

        Dim btnRptEdit As LinkButton = e.Item.FindControl("btnRptEdit")
        Dim btnRptDelete As LinkButton = e.Item.FindControl("btnRptDelete")
        Dim cfm_Delete As AjaxControlToolkit.ConfirmButtonExtender = e.Item.FindControl("cfm_Delete")

        lblId.Text = e.Item.DataItem("S_ID").ToString
        lbl_TaxNo.Text = e.Item.DataItem("S_Tax_No").ToString
        lbl_Name.Text = "<b>" & e.Item.DataItem("S_Name").ToString & "</b>"
        lbl_NameAlias.Text = e.Item.DataItem("S_Alias").ToString

        Dim S_Date As DateTime = GL.IsValidDate(e.Item.DataItem("B_StartDate"), New System.Globalization.CultureInfo("th-TH"))
        Dim E_Date As DateTime = GL.IsValidDate(e.Item.DataItem("B_EndDate"), New System.Globalization.CultureInfo("th-TH"))
        lblPeriod.Text = S_Date.ToString("dd MMM yyyy") & " - " & E_Date.ToString("dd MMM yyyy")

        lbl_ContactName.Text = e.Item.DataItem("S_Contact_Name").ToString
        If e.Item.DataItem("S_Phone").ToString <> "" Then
            lbl_ContactName.Text = lbl_ContactName.Text & "  ( " & e.Item.DataItem("S_Phone").ToString & " )"
        End If
        If e.Item.DataItem("S_Email").ToString <> "" Then
            lbl_ContactName.Text = lbl_ContactName.Text & "  :  " & e.Item.DataItem("S_Email").ToString
        End If

        lblB_Comment.Text = e.Item.DataItem("B_Comment").ToString


        btnRptEdit.CommandArgument = e.Item.DataItem("S_ID")
        btnRptDelete.CommandArgument = e.Item.DataItem("S_ID")
        cfm_Delete.ConfirmText = "ยืนยันยกเลิก Black List " & e.Item.DataItem("S_Name").ToString & "?"
    End Sub

    Protected Sub lnkAdd_Click(sender As Object, e As System.EventArgs) Handles lnkAdd1.Click, lnkAdd2.Click
        ClearFormData()

        '--------------- Bind Button ------------
        btnSearchSupplier.Visible = False
        btnSave.Visible = AccessMode = AVLBL.AccessRole.Edit
        btnDelete.Visible = False

        pnlData.Visible = False
        pnlEdit.Visible = True
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
        BindBlackList()
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        '--------------- Validate-------------
        If S_ID = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','เลือกผู้ขายที่ต้องการ Black List','');", True)
            btnSupplierDialog_Click(Nothing, Nothing)
            Exit Sub
        End If
        Dim S_Date As DateTime = GL.IsValidDate(txtEdit_Start.Text, New System.Globalization.CultureInfo("th-TH"))
        If IsNothing(S_Date) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','เลือกช่วงเวลาที่ต้องการ Black List','');", True)
            txtEdit_Start.Focus()
            Exit Sub
        End If
        Dim E_Date As DateTime = GL.IsValidDate(txtEdit_End.Text, New System.Globalization.CultureInfo("th-TH"))
        If IsNothing(E_Date) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','เลือกช่วงเวลาที่ต้องการ Black List','');", True)
            txtEdit_End.Focus()
            Exit Sub
        End If
        If E_Date < S_Date Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','เวลาเริ่ม-สิ้นสุดไม่ถูกต้อง','');", True)
            txtEdit_Start.Focus()
            Exit Sub
        End If
        If E_Date < Now Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','เวลาสิ้นสุดต้องหลังจากปัจจุบัน','');", True)
            txtEdit_End.Focus()
            Exit Sub
        End If

        If txtComment.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','กรอกรายละเอียดการยกเลิกผู้ขาย','');", True)
            txtComment.Focus()
            Exit Sub
        End If

        Dim SQL As String = "SELECT * FROM tb_BlackList WHERE S_ID=" & S_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            DR("S_ID") = S_ID
            DT.Rows.Add(DR)
        Else
            DR = DT.Rows(0)
        End If

        DR("B_StartDate") = S_Date
        DR("B_EndDate") = E_Date
        DR("B_Comment") = txtComment.Text
        DR("Update_By") = User_ID
        DR("Update_Time") = Now
        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','Black List สำเร็จ','');", True)
        BindBlackList()

    End Sub

    Protected Sub btnDelete_Click(sender As Object, e As System.EventArgs) Handles btnDelete.Click
        Dim Conn As New SqlConnection(BL.ConnectionString)
        Conn.Open()
        Dim Comm As New SqlCommand
        With Comm
            .CommandType = CommandType.Text
            .Connection = Conn
            .CommandText = "DELETE FROM tb_BlackList_File WHERE S_ID=" & S_ID & vbLf
            .CommandText &= "DELETE FROM tb_BlackList WHERE S_ID=" & S_ID & vbLf
            .ExecuteNonQuery()
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('success','ยกเลิก Black List สำเร็จ','');", True)
        BindBlackList()
    End Sub

#Region "Dialog Supplier"

    Protected Sub lnkCloseSupplier_Click(sender As Object, e As System.EventArgs) Handles lnkCloseSupplier.Click
        dialogSupplier.Visible = False
    End Sub

    Protected Sub btnSupplierDialog_Click(sender As Object, e As System.EventArgs) Handles btnSupplierDialog.Click
        txtSearchTaxNo.Text = ""
        txtSearchName.Text = ""
        txtSearchAlias.Text = ""
        txtSearchContact.Text = ""
        BindSupplierList()
        dialogSupplier.Visible = True
    End Sub

    Protected Sub SearchSupplier_Changed(sender As Object, e As System.EventArgs) Handles btnSearchSupplier.Click, txtSearchTaxNo.TextChanged, txtSearchName.TextChanged, txtSearchAlias.TextChanged, txtSearchContact.TextChanged
        BindSupplierList()
    End Sub

    Private Sub BindSupplierList()
        Dim DT As DataTable = BL.GetSupplierList(True)
        Dim Filter As String = ""
        If txtSearchTaxNo.Text <> "" Then
            Filter &= " S_Tax_No LIKE '%" & txtSearchTaxNo.Text.Replace("'", "''") & "%' AND "
        End If
        If txtSearchName.Text <> "" Then
            Filter &= " S_Name LIKE '%" & txtSearchName.Text.Replace("'", "''") & "%' AND "
        End If
        If txtSearchAlias.Text <> "" Then
            Filter &= " S_Alias LIKE '%" & txtSearchAlias.Text.Replace("'", "''") & "%' AND "
        End If
        If txtSearchContact.Text <> "" Then
            Filter &= " (S_Address LIKE '%" & txtSearchContact.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Phone LIKE '%" & txtSearchContact.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Fax LIKE '%" & txtSearchContact.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Email LIKE '%" & txtSearchContact.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Contact_Name LIKE '%" & txtSearchContact.Text.Replace("'", "''") & "%' " & vbLf
            Filter &= " ) AND "
        End If
        If Filter <> "" Then DT.DefaultView.RowFilter = Filter.Substring(0, Filter.Length - 4)
        DT = DT.DefaultView.ToTable
        Session("BlackList_SupplierList") = DT
        PagerSupplier.SesssionSourceName = "BlackList_SupplierList"
        PagerSupplier.RenderLayout()

        If DT.Rows.Count = 0 Then
            lblTotalSupplier.Text = "ไม่พบรายการดังกล่าว"
        Else
            lblTotalSupplier.Text = "พบ " & FormatNumber(DT.Rows.Count, 0) & " รายการ"
        End If

    End Sub

    Protected Sub PagerSupplier_PageChanging(Sender As PageNavigation) Handles PagerSupplier.PageChanging
        PagerSupplier.TheRepeater = rptSupplierList
    End Sub

    Protected Sub rptSupplierList_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptSupplierList.ItemCommand
        Select Case e.CommandName
            Case "select"
                Dim _S_ID As Integer = e.CommandArgument
                Dim ST As DataTable = BL.GetSupplierDetail(_S_ID)
                If ST.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','ไม่พบข้อมูลผู้ขายรายนี้','');", True)
                    Exit Sub
                End If
                '--------------- Update ข้อมูลเข้าไปในฟอร์ม ------------------------
                S_ID = _S_ID

                txt_S_Fullname.Text = ST.Rows(0).Item("S_Name").ToString
                txt_S_Alias.Text = ST.Rows(0).Item("S_Alias").ToString
                txt_S_Tax_No.Text = ST.Rows(0).Item("S_Tax_No").ToString
                txt_S_Address.Text = ST.Rows(0).Item("S_Address").ToString
                txt_S_Contact_Name.Text = ST.Rows(0).Item("S_Contact_Name").ToString
                txt_S_Email.Text = ST.Rows(0).Item("S_Email").ToString
                txt_S_Phone.Text = ST.Rows(0).Item("S_Phone").ToString
                txt_S_Fax.Text = ST.Rows(0).Item("S_Fax").ToString
                dialogSupplier.Visible = False
        End Select
    End Sub

    Protected Sub rptSupplierList_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptSupplierList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lbl_Tax As Label = e.Item.FindControl("lbl_Tax")
        Dim lbl_Supplier As Label = e.Item.FindControl("lbl_Supplier")
        Dim lbl_Sales As Label = e.Item.FindControl("lbl_Sales")
        Dim lbl_Contact As Label = e.Item.FindControl("lbl_Contact")

        lbl_Tax.Text = e.Item.DataItem("S_Tax_No").ToString
        lbl_Supplier.Text = e.Item.DataItem("S_Name").ToString
        If Not IsDBNull(e.Item.DataItem("S_Alias")) AndAlso e.Item.DataItem("S_Alias") <> "" Then
            lbl_Supplier.Text &= "(" & e.Item.DataItem("S_Alias") & ")"
        End If
        lbl_Sales.Text = e.Item.DataItem("S_Contact_Name").ToString
        Dim Contact As String = ""
        If Not IsDBNull(e.Item.DataItem("S_Phone")) AndAlso e.Item.DataItem("S_Phone") <> "" Then
            Contact &= "<br/><i class='icon-briefcase'></i> " & e.Item.DataItem("S_Phone")
        End If
        If Not IsDBNull(e.Item.DataItem("S_Email")) AndAlso e.Item.DataItem("S_Email") <> "" Then
            Contact &= "<br/><i class='icon-envelope'></i> " & e.Item.DataItem("S_Email")
        End If
        If Not IsDBNull(e.Item.DataItem("S_Fax")) AndAlso e.Item.DataItem("S_Fax") <> "" Then
            Contact &= "<br/><i class='icon-print'></i> " & e.Item.DataItem("S_Fax")
        End If
        If Contact <> "" Then
            lbl_Contact.Text = Contact.Substring(5)
        End If

        Dim btnSelect As Button = e.Item.FindControl("btnSelect")
        For i As Integer = 1 To 4
            Dim td As HtmlTableCell = e.Item.FindControl("td" & i)
            td.Style("cursor") = "pointer"
            td.Attributes("onClick") = "document.getElementById('" & btnSelect.ClientID & "').click();"
        Next
        btnSelect.CommandArgument = e.Item.DataItem("S_ID")
    End Sub


#End Region

#Region "PrintButton"

    Protected Sub btnPDF_Click(sender As Object, e As System.EventArgs) Handles btnPDF.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/BlackList_List.aspx?Mode=PDF');", True)
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/BlackList_List.aspx?Mode=EXCEL');", True)
    End Sub

#End Region


End Class
