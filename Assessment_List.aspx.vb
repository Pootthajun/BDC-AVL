﻿
Imports System.Data
Imports System.Data.SqlClient

Partial Class Assessment_List
    Inherits System.Web.UI.Page

    Dim BL As New AVLBL
    Dim GL As New GenericLib

    Private ReadOnly Property PageName As String
        Get
            Return "Assessment_Edit.aspx"
        End Get
    End Property

    Private ReadOnly Property User_ID As Integer
        Get
            Try
                If IsNumeric(Session("User_ID")) Then
                    Return Session("User_ID")
                Else
                    Return -1
                End If
            Catch ex As Exception
                Return -1
            End Try
        End Get
    End Property

    Private Property HideRemark As Boolean
        Get
            Return ckHideRemark.Checked
        End Get
        Set(value As Boolean)
            ckHideRemark.Checked = value
        End Set
    End Property

    Private ReadOnly Property Dept_ID As String
        Get
            Try
                Return ddl_Dept.Items(ddl_Dept.SelectedIndex).Value
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Private ReadOnly Property Dept_Name As String
        Get
            Try
                Return ddl_Dept.Items(ddl_Dept.SelectedIndex).Text
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Private ReadOnly Property Sub_Dept_ID As String
        Get
            Try
                Return ddl_Sub_Dept.Items(ddl_Sub_Dept.SelectedIndex).Value
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Private ReadOnly Property Sub_Dept_Name As String
        Get
            Try
                Return ddl_Sub_Dept.Items(ddl_Sub_Dept.SelectedIndex).Text
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    'Private ReadOnly Property CatGroup_ID As Integer
    '    Get
    '        Try
    '            Return ddl_CatGroup.Items(ddl_CatGroup.SelectedIndex).Value
    '        Catch ex As Exception
    '            Return 0
    '        End Try
    '    End Get
    'End Property

    'Private ReadOnly Property Cat_ID As Integer
    '    Get
    '        Try
    '            Return ddl_Cat.Items(ddl_Cat.SelectedIndex).Value
    '        Catch ex As Exception
    '            Return 0
    '        End Try
    '    End Get
    '    'Set(value As Integer)
    '    '    BL.BindDDlCAT(ddl_Cat, User_ID, Dept_ID, value)
    '    'End Set
    'End Property

    'Private ReadOnly Property Sub_Cat_ID As Integer
    '    Get
    '        Try
    '            Return ddl_Sub_Cat.Items(ddl_Sub_Cat.SelectedIndex).Value
    '        Catch ex As Exception
    '            Return 0
    '        End Try
    '    End Get
    '    'Set(value As Integer)
    '    '    BL.BindDDlSub_Cat(ddl_Sub_Cat, User_ID, Cat_ID, value)
    '    'End Set
    'End Property

    'Private ReadOnly Property Type_ID As Integer
    '    Get
    '        Try
    '            Return ddl_Type.Items(ddl_Type.SelectedIndex).Value
    '        Catch ex As Exception
    '            Return 0
    '        End Try
    '    End Get
    '    'Set(value As Integer)
    '    '    BL.BindDDlType(ddl_Type, User_ID, Sub_Cat_ID, value)
    '    'End Set
    'End Property





    '//ค้นหาครุภัณฑ์

    Public Property Type_ID As String
        Get
            Try
                Return lblType_ID_dialog.Text
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As String)
            lblType_ID_dialog.Text = value
        End Set
    End Property
    Public Property Running_ID As String
        Get
            Try
                Return lblRunning_ID_dialog.Text
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As String)
            lblRunning_ID_dialog.Text = value
        End Set
    End Property

    Private Property Running_Name As String
        Get
            Try
                Return lblRunning_ID_dialog.Attributes("Running_Name")
            Catch ex As Exception
                Return ""
            End Try
        End Get
        Set(value As String)
            lblRunning_ID_dialog.Attributes("Running_Name") = value
        End Set
    End Property

    Private ReadOnly Property Search_CatGroup_ID As Integer
        Get
            Try
                Return ddl_CatGroup_dialog.Items(ddl_CatGroup_dialog.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
    End Property

    Private ReadOnly Property Search_Cat_ID As Integer
        Get
            Try
                Return ddl_Cat_dialog.Items(ddl_Cat_dialog.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
    End Property

    Private ReadOnly Property Search_Sub_Cat_ID As Integer
        Get
            Try
                Return ddl_Sub_Cat_dialog.Items(ddl_Sub_Cat_dialog.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
    End Property

    Private ReadOnly Property Search_Type_ID As Integer
        Get
            Try
                Return ddl_Type_dialog.Items(ddl_Type_dialog.SelectedIndex).Value
            Catch ex As Exception
                Return 0
            End Try
        End Get
    End Property

    Private Property AccessMode As AVLBL.AccessRole
        Get
            Try
                Return ViewState("AccessMode")
            Catch ex As Exception
                Return AVLBL.AccessRole.None
            End Try
        End Get
        Set(value As AVLBL.AccessRole)
            ViewState("AccessMode") = value
        End Set
    End Property

    Public Property Last_Search_Dept As String
        Get
            Try
                Return Session("Assessment_List_Search_Dept")
            Catch ex As Exception
                Return ""
            End Try
        End Get
        Set(value As String)
            Session("Assessment_List_Search_Dept") = value
        End Set
    End Property

    Public Property Last_Search_Item As String
        Get
            Try
                Return Session("Assessment_List_Search_Item")
            Catch ex As Exception
                Return ""
            End Try
        End Get
        Set(value As String)
            Session("Assessment_List_Search_Item") = value
        End Set
    End Property

    Public Property Last_Search_Sup As String
        Get
            Try
                Return Session("Assessment_List_Search_Sup")
            Catch ex As Exception
                Return ""
            End Try
        End Get
        Set(value As String)
            Session("Assessment_List_Search_Sup") = value
        End Set
    End Property

    Public Property Last_Search_AVL As String
        Get
            Try
                Return Session("Assessment_List_Search_AVL")
            Catch ex As Exception
                Return ""
            End Try
        End Get
        Set(value As String)
            Session("Assessment_List_Search_AVL") = value
        End Set
    End Property

    Public Property Last_Search_Ref As String
        Get
            Try
                Return Session("Assessment_List_Search_Ref")
            Catch ex As Exception
                Return ""
            End Try
        End Get
        Set(value As String)
            Session("Assessment_List_Search_Ref") = value
        End Set
    End Property

    Public Property Last_Search_Step As Integer
        Get
            Try
                Return Session("Assessment_List_Search_Step")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Integer)
            Session("Assessment_List_Search_Step") = value
        End Set
    End Property

    Public Property Last_Search_Page As Integer
        Get
            Try
                Return Session("Assessment_List_Search_Page")
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As Integer)
            Session("Assessment_List_Search_Page") = value
        End Set
    End Property

    Private Sub CollectLastCriteria()
        Last_Search_Dept = ddl_Search_Dept.SelectedValue
        Last_Search_Item = txt_Search_Item.Text
        Last_Search_Sup = txt_Search_Sup.Text
        Last_Search_AVL = txt_Search_AVL.Text
        Last_Search_Ref = txt_Search_Ref.Text
        Last_Search_Step = ddl_Search_Step.SelectedValue
        Last_Search_Page = Pager.CurrentPage
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        If IsNothing(Session("User_ID")) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "ShowAlert('warning','กรุณาล็อกอินเข้าสู่ระบบ','Login.aspx');", True)
            Response.Redirect("Login.aspx")
            Exit Sub
        End If

        '--------------- Check Role (Remain)-------------------
        If Not IsPostBack Then

            If Request.QueryString("Back") = Nothing Then
                Last_Search_Dept = ddl_Search_Dept.SelectedIndex = 0
                Last_Search_Item = ""
                Last_Search_Sup = ""
                Last_Search_AVL = ""
                Last_Search_Ref = ""
                Last_Search_Step = ddl_Search_Step.SelectedIndex = 0
                Last_Search_Page = Pager.CurrentPage
            End If


            Dim MT As DataTable = BL.GetUserMenuRole(User_ID, PageName)
            If MT.Rows.Count = 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลนี้','Overview.aspx');", True)
                Exit Sub
            End If
            AccessMode = MT.Rows(0).Item("MR_ID") '--------- สิทธิ์ของผู้ใช้คนปัจจุบัน -----------
            If AccessMode = AVLBL.AccessRole.None Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลนี้','Overview.aspx');", True)
                Exit Sub
            End If

            Dim RT As DataTable = BL.GetUserDeptRole(User_ID)
            lnkAdd1.Visible = BL.CanCreateAssessment(RT, "AR_ID") '--------- หาสิทธิ์ในการสร้างแบบประเมิน ---------

            '------------- Restore Last Search Criteria------------
            BL.BindDDlDEPT(ddl_Search_Dept, User_ID, Last_Search_Dept)
            txt_Search_Item.Text = Last_Search_Item
            txt_Search_Sup.Text = Last_Search_Sup
            txt_Search_AVL.Text = Last_Search_AVL
            txt_Search_Ref.Text = Last_Search_Ref
            ddl_Search_Step.SelectedValue = Last_Search_Step
            ddl_Search_Step.SelectedValue = Last_Search_Step
            ClearSearchForm()
            BindList()

            'BL.BindDDlDN(ddl_Search_DN)

            '-----------------Check permission for creating new assessment----------------
            dialogNewAssessment.Visible = False
            dialogProductType.Visible = False
           
        End If
    End Sub

    Private Sub ClearSearchForm()
        ddlStart_M.Items.Clear()
        ddlEnd_M.Items.Clear()
        For i As Integer = 1 To 12
            ddlStart_M.Items.Add(New ListItem(GL.ReportMonthThaiShort(i), i))
            ddlEnd_M.Items.Add(New ListItem(GL.ReportMonthThaiShort(i), i))
        Next
        ddlEnd_M.SelectedIndex = 11

        ddlStart_Y.Items.Clear()
        ddlEnd_Y.Items.Clear()
        For i As Integer = Now.Year - 2 To Now.Year + 8
            ddlStart_Y.Items.Add(New ListItem(i + 543, i))
            ddlEnd_Y.Items.Add(New ListItem(i + 543, i))
        Next
        ddlEnd_Y.SelectedIndex = ddlEnd_Y.Items.Count - 1
    End Sub
    Protected Sub ddlPeriod_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlEnd_M.SelectedIndexChanged, ddlEnd_Y.SelectedIndexChanged, ddlStart_M.SelectedIndexChanged, ddlStart_Y.SelectedIndexChanged

        Dim StartMonth As Integer = (ddlStart_Y.SelectedValue * 12) + ddlStart_M.SelectedValue
        Dim EndMonth As Integer = (ddlEnd_Y.SelectedValue * 12) + ddlEnd_M.SelectedValue

        If StartMonth > EndMonth Then
            Select Case True
                Case Equals(sender, ddlStart_M) Or Equals(sender, ddlStart_Y)
                    ddlEnd_M.SelectedIndex = ddlStart_M.SelectedIndex
                    ddlEnd_Y.SelectedIndex = ddlStart_Y.SelectedIndex
                Case Equals(sender, ddlEnd_M) Or Equals(sender, ddlEnd_Y)
                    ddlStart_M.SelectedIndex = ddlEnd_M.SelectedIndex
                    ddlStart_Y.SelectedIndex = ddlEnd_Y.SelectedIndex
            End Select
        End If

    End Sub

   
    Protected Sub Search_Changed(sender As Object, e As System.EventArgs) Handles btnSearch.Click ', ddl_Search_Dept.SelectedIndexChanged, txt_Search_Item.TextChanged, txt_Search_AVL.TextChanged, txt_Search_Sup.TextChanged, txt_Search_Ref.TextChanged, ddl_Search_Step.SelectedIndexChanged
        BindList()
    End Sub

    Private Sub BindList()
        Dim StartMonth As Integer = (ddlStart_Y.SelectedValue * 12) + ddlStart_M.SelectedValue
        Dim EndMonth As Integer = (ddlEnd_Y.SelectedValue * 12) + ddlEnd_M.SelectedValue
        Dim SQL As String = "DECLARE @S AS INT=" & StartMonth & vbLf
        SQL &= "DECLARE @E AS INT=" & EndMonth & vbLf & vbLf
        SQL &= " SELECT DISTINCT vw_Ass_Header.Ass_ID,Ref_Year,Ref_Month,Ref_Number,Ref_Code" & vbLf
        SQL &= " ,Item_No,vw_Ass_Header.Item_Name,Dept_ID,Dept_Name,Sub_Dept_ID,Sub_Dept_Name ,S_ID,S_Name,S_Alias" & vbLf
        SQL &= " ,Role_1_ID,Role_1_Title,Role_1_Fisrt_Name,Role_1_Last_Name,Role_1_Pos_Name" & vbLf
        SQL &= " ,Role_2_ID,Role_2_Title,Role_2_Fisrt_Name,Role_2_Last_Name,Role_2_Pos_Name" & vbLf
        SQL &= " ,Role_3_ID,Role_3_Title,Role_3_Fisrt_Name,Role_3_Last_Name,Role_3_Pos_Name" & vbLf
        SQL &= " ,Role_4_ID,Role_4_Title,Role_4_Fisrt_Name,Role_4_Last_Name,Role_4_Pos_Name" & vbLf
        SQL &= " ,dbo.udf_AVLCode(AVL_Y,AVL_M,AVL_Dept_ID,AVL_Sub_Dept_ID,AVL_No) AVL_Code" & vbLf
        SQL &= " ,AVL_Status,Get_Score,Pass_Score,Max_Score,ISNULL(Pass_Status,1) Pass_Status,Current_Step,Step_TH,Flag_Return,Update_Time" & vbLf
        SQL &= " ,Role_1_Comment,Role_2_Comment,Role_3_Comment,Role_4_Comment" & vbLf

        SQL &= " FROM vw_Ass_Header " & vbLf

        Dim Title As String = ""
        Dim Filter As String = ""
        If ddl_Search_Dept.SelectedIndex > 0 Then
            Filter &= " Dept_ID='" & ddl_Search_Dept.SelectedValue & "' AND "
            Title &= " " & ddl_Search_Dept.Items(ddl_Search_Dept.SelectedIndex).Text
        Else
            Filter &= " Dept_ID IN (" & vbLf
            Filter &= " SELECT D.Dept_ID FROM tb_Dept_Role R" & vbLf
            Filter &= " INNER JOIN tb_User U ON R.User_ID=U.User_ID AND U.Active_Status=1" & vbLf
            Filter &= " INNER JOIN tb_Dept D ON R.Dept_ID=D.Dept_ID AND D.Active_Status=1" & vbLf
            Filter &= " INNER JOIN tb_Ass_Role AR ON R.AR_ID=AR.AR_ID" & vbLf
            Filter &= " WHERE R.User_ID=" & User_ID & vbLf
            Filter &= " ) AND "
        End If
        If ddl_Search_SUB_DEPT.SelectedIndex > 0 Then
            Filter &= " Sub_Dept_ID='" & ddl_Search_SUB_DEPT.SelectedValue & "' AND "
            Title &= " ของฝ่าย/ภาค/งาน " & ddl_Search_SUB_DEPT.Items(ddl_Search_SUB_DEPT.SelectedIndex).Text
        End If

        If txt_Search_Item.Text <> "" Then
            Filter &= " (" & vbLf
            Filter &= " Item_No LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Item_Name LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " CAT_Name LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Sub_Cat_Name LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Type_Name LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR" & vbLf
            Filter &= " Running_Name LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' OR" & vbLf
            Filter &= " Cat_No + Sub_Cat_No+Type_No+Running_No LIKE '%" & txt_Search_Item.Text.Replace("'", "''") & "%' " & vbLf
            Filter &= ") AND "
            Title &= " พัสดุ/ครุภัณฑ์ จัดจ้างและบริการ : " & txt_Search_Item.Text
        End If
        If txt_Search_Sup.Text <> "" Then
            Filter &= " (" & vbLf
            Filter &= " S_Tax_No LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Name LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Alias LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Address LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Replace(Replace(S_Phone,'-',''),',','') LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Replace(Replace(S_Fax,'-',''),',','') LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Contact_Name LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " S_Email LIKE '%" & txt_Search_Sup.Text.Replace("'", "''") & "%' " & vbLf
            Filter &= ") AND "
            Title &= " ผู้ขาย : " & txt_Search_Sup.Text
        End If
        If txt_Search_AVL.Text <> "" Then
            Filter &= " dbo.udf_AVLCode(AVL_Y,AVL_M,AVL_Dept_ID,AVL_Sub_Dept_ID,AVL_No) LIKE '%" & txt_Search_AVL.Text.Replace("'", "''") & "%' AND " & vbLf
            Title &= " เลข AVL : " & txt_Search_AVL.Text
        End If
        If txt_Search_Ref.Text <> "" Then
            Filter &= " Ref_Code LIKE '%" & txt_Search_Ref.Text.Replace("'", "''") & "%' AND " & vbLf
            Title &= " เลขใบประเมิน : " & txt_Search_Ref.Text
        End If
        If ddl_Search_Step.SelectedIndex > 0 Then
            Filter &= "Current_Step=" & ddl_Search_Step.SelectedValue & " AND " & vbLf
            Title &= " ขั้นตอน" & ddl_Search_Step.Items(ddl_Search_Step.SelectedIndex).Text
        End If

        Filter &= " vw_Ass_Header.Ass_ID NOT IN ( "
        Filter &= " SELECT Fail_Ass_ID FROM tb_Ass_Header  WHERE Fail_Ass_ID IS NOT NULL "
        Filter &= " )" & " AND " & vbLf

        Filter &= " dbo.UDF_IsRangeIntersected(@S,@E,Month(Update_Time)+(Year(Update_Time)*12),Month(Update_Time)+(Year(Update_Time)*12))=1  AND " & vbLf
        Title &= " รายการประเมินในช่วง " & ddlStart_M.Items(ddlStart_M.SelectedIndex).Text & " " & ddlStart_Y.Items(ddlStart_Y.SelectedIndex).Text
        Title &= " ถึง "
        Title &= ddlStart_M.Items(ddlEnd_M.SelectedIndex).Text & " " & ddlEnd_Y.Items(ddlEnd_Y.SelectedIndex).Text & " "



        If Filter <> "" Then
            SQL &= "WHERE " & Filter.Substring(0, Filter.Length - 5) & vbLf
        End If


        SQL &= " ORDER BY  Dept_ID ,Ref_Code DESC" & vbLf

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        DA.Fill(DT)

        Session("Search_Assessment_List") = DT
        Session("Search_Assessment_List_Title") = Title

        lblTotalRecord.Text = Title
        If DT.Rows.Count = 0 Then
            lblTotalRecord.Text &= " ไม่พบรายการดังกล่าว"
        Else
            lblTotalRecord.Text &= " พบ " & FormatNumber(DT.Rows.Count, 0) & " ใบประเมิน"
        End If
        LastDept = ""
        '------------- Binding To List ---------------
        Pager.SesssionSourceName = "Search_Assessment_List"
        'Pager.CurrentPage = Last_Search_Page
        Pager.RenderLayout()

    End Sub

    Protected Sub Pager_PageChanging(Sender As PageNavigation) Handles Pager.PageChanging
        Pager.TheRepeater = rptList
        '----------- Collect Search Keyword To Session ----------
        CollectLastCriteria()

        pnlBottomForm.Visible = rptList.Items.Count > 10 And lnkAdd1.Visible '--------- แล้วแต่ความเหมาะสม ------------
    End Sub

    'Protected Sub btnAdd_Click(sender As Object, e As System.EventArgs) Handles btnAdd1.Click, btnAdd2.Click
    '    '----------- Collect Search Keyword To Session ----------
    '    CollectLastCriteria()
    '    '---------------- Redirect---------------
    '    Response.Redirect("Assessment_Edit.aspx")
    'End Sub

    Protected Sub rptList_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Select Case e.CommandName
            Case "View"
                '----------- Collect Search Keyword To Session ----------
                CollectLastCriteria()
                '---------------- Check role for department -------------
                Dim lblDept As Label = e.Item.FindControl("lblDept")
                Dim _dept_id As String = lblDept.Attributes("Dept_ID")
                Dim RT As DataTable = BL.GetUserDeptRole(User_ID, _dept_id)
                If RT.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่สามารถเข้าถึงใบประเมินดังกล่าวได้','');", True)
                    Exit Sub
                End If
                '---------------- Redirect---------------
                Response.Redirect("Assessment_Edit.aspx?Ass_ID=" & e.CommandArgument)
        End Select
    End Sub

    Dim LastDept As String = ""
    Protected Sub rptList_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblRef As Label = e.Item.FindControl("lblRef")
        Dim lblItemCode As Label = e.Item.FindControl("lblItemCode")
        Dim lblItemName As Label = e.Item.FindControl("lblItemName")
        Dim lblSub_Dept_Name As Label = e.Item.FindControl("lblSub_Dept_Name")
        Dim lblSup As Label = e.Item.FindControl("lblSup")
        Dim lblAVL As Label = e.Item.FindControl("lblAVL")
        Dim lblResult As Label = e.Item.FindControl("lblResult")
        Dim lblResultText As Label = e.Item.FindControl("lblResultText")

        Dim lblAssessor As Label = e.Item.FindControl("lblAssessor")
        Dim lblUpdate As Label = e.Item.FindControl("lblUpdate")
        Dim lblStep As Label = e.Item.FindControl("lblStep")

        Dim trRemark As HtmlTableRow = e.Item.FindControl("trRemark")
        Dim lblRole_1_Comment As Label = e.Item.FindControl("lblRole_1_Comment")
        Dim lblRole_2_Comment As Label = e.Item.FindControl("lblRole_2_Comment")
        Dim lblRole_3_Comment As Label = e.Item.FindControl("lblRole_3_Comment")
        Dim lblRole_4_Comment As Label = e.Item.FindControl("lblRole_4_Comment")

        Dim btnView As LinkButton = e.Item.FindControl("btnView")
        Dim pnl_icon As Panel = e.Item.FindControl("pnl_icon")

        Dim trDept As HtmlTableRow = e.Item.FindControl("trDept")
        Dim lblDept As Label = e.Item.FindControl("lblDept")
        If LastDept <> e.Item.DataItem("Dept_Name").ToString Then
            LastDept = e.Item.DataItem("Dept_Name").ToString
            lblDept.Text = LastDept
            trDept.Visible = True
        Else
            trDept.Visible = False
        End If
        lblDept.Attributes("Dept_ID") = e.Item.DataItem("Dept_ID")

        lblRef.Text = e.Item.DataItem("Ref_Code").ToString
        lblItemCode.Text = e.Item.DataItem("Item_No").ToString.Replace(vbLf, "<br>")
        lblItemName.Text = e.Item.DataItem("Item_Name").ToString.Replace(vbLf, "<br>")
        lblSub_Dept_Name.Text = e.Item.DataItem("Sub_Dept_Name").ToString
        'lblSup.Text = e.Item.DataItem("S_Alias").ToString
        lblSup.Text = e.Item.DataItem("S_Name").ToString

        lblAVL.Text = e.Item.DataItem("AVL_Code").ToString
        BL.DisplayAssessmentGridResultLabel(e.Item.DataItem("Get_Score"), e.Item.DataItem("Pass_Score"), e.Item.DataItem("Max_Score"), e.Item.DataItem("Pass_Status"), lblResult)
       

            '------Remark--------
            lblRole_1_Comment.Text = "ผู้จัดทำ : " & e.Item.DataItem("Role_1_Comment").ToString
            lblRole_2_Comment.Text = "ผู้ตรวจสอบ : " & e.Item.DataItem("Role_2_Comment").ToString
            lblRole_3_Comment.Text = "เจ้าหน้าที่พัสดุ : " & e.Item.DataItem("Role_3_Comment").ToString
            lblRole_4_Comment.Text = "หัวหน้าฝ่ายบริหารทั่วไป : " & e.Item.DataItem("Role_4_Comment").ToString

            If HideRemark Or (e.Item.DataItem("Role_1_Comment").ToString = "" And e.Item.DataItem("Role_2_Comment").ToString = "" And e.Item.DataItem("Role_3_Comment").ToString = "" And e.Item.DataItem("Role_4_Comment").ToString = "") Then
                trRemark.Style.Item("display") = "none"
            End If

            lblUpdate.Text = GL.ReportThaiPassTime(e.Item.DataItem("Update_Time"))
            If Not IsDBNull(e.Item.DataItem("Flag_Return")) Then
                pnl_icon.Visible = e.Item.DataItem("Flag_Return")
            End If

            lblStep.Text = e.Item.DataItem("Step_TH").ToString
        lblStep.ForeColor = BL.GetStepColor(e.Item.DataItem("Current_Step"))
        ' แสดง lbl สถานะของการประเมิน  เสร็จแล้ว , ยังไม่เสร็จ
        If (e.Item.DataItem("Current_Step").ToString() = 5) Then
            lblResultText.Text = "(เสร็จแล้ว)"
            lblResultText.Style("color") = "green"
        Else
            lblResultText.Text = "(ยังไม่เสร็จ)"
            lblResultText.Style("color") = "red"
        End If

 
        Dim Creater As String = Trim(e.Item.DataItem("Role_1_Fisrt_Name") & " " & e.Item.DataItem("Role_1_Last_Name"))
        Dim Auditor As String = Trim(e.Item.DataItem("Role_2_Fisrt_Name") & " " & e.Item.DataItem("Role_2_Last_Name"))
        Dim Supply_Officer As String = Trim(e.Item.DataItem("Role_3_Fisrt_Name") & " " & e.Item.DataItem("Role_3_Last_Name"))
        Dim Director As String = Trim(e.Item.DataItem("Role_4_Fisrt_Name") & " " & e.Item.DataItem("Role_4_Last_Name"))
        Select Case CType(e.Item.DataItem("Current_Step"), AVLBL.AssessmentStep)
            Case AVLBL.AssessmentStep.Creater
                lblAssessor.Text = Creater
            Case AVLBL.AssessmentStep.Auditor
                If Auditor <> "" Then
                    lblAssessor.Text = Auditor
                Else
                    lblAssessor.Text = Creater
                End If
            Case AVLBL.AssessmentStep.Supply_Officer
                If Supply_Officer <> "" Then
                    lblAssessor.Text = Supply_Officer
                ElseIf Auditor <> "" Then
                    lblAssessor.Text = Auditor
                Else
                    lblAssessor.Text = Creater
                End If
            Case AVLBL.AssessmentStep.Director, AVLBL.AssessmentStep.Completed

                If Director <> "" Then
                    lblAssessor.Text = Director
                ElseIf Supply_Officer <> "" Then
                    lblAssessor.Text = Supply_Officer
                ElseIf Auditor <> "" Then
                    lblAssessor.Text = Auditor
                Else
                    lblAssessor.Text = Creater
                End If
            Case Else
        End Select

        btnView.CommandArgument = e.Item.DataItem("Ass_ID")

    End Sub

    Protected Sub lnkCloseDialogNew_Click(sender As Object, e As System.EventArgs) Handles lnkCloseDialogNew.Click
        dialogNewAssessment.Visible = False
    End Sub

    Protected Sub lnkAdd_Click(sender As Object, e As System.EventArgs) Handles lnkAdd1.Click, lnkAdd2.Click
        BL.BindDDlDEPT(ddl_Dept, User_ID)
        ddl_Dept_SelectedIndexChanged(sender, e)
        dialogNewAssessment.Visible = True
        pnlCreate.Visible = False
        pnlbtnProductDialog.Visible = False
        pnlHeaderDialog.Visible = False
    End Sub

    Protected Sub ddl_Dept_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddl_Dept.SelectedIndexChanged
        BL.BindDDlSub_Dept(ddl_Sub_Dept, Dept_ID, User_ID, AVLBL.AssessmentRole.Creater, "", True)
        'ddl_Sub_Dept_SelectedIndexChanged(sender, e)
        pnlSubDept.Visible = Dept_ID.ToString() <> "0"

        pnlCreate.Visible = False
        pnlbtnProductDialog.Visible = False
        pnlHeaderDialog.Visible = False
        ddl_Sub_Dept.SelectedIndex = 0
    End Sub

    Protected Sub ddl_Sub_Dept_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddl_Sub_Dept.SelectedIndexChanged
  
        pnlbtnProductDialog.Visible = True
        pnlCreate.Visible = False
        pnlbtnProductDialog.Visible = Sub_Dept_ID.ToString() <> "0"
        pnlHeaderDialog.Visible = False
    End Sub


    'Protected Sub ddl_Sub_Dept_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddl_Sub_Dept.SelectedIndexChanged
    '    BL.BindDDlCatGroup(ddl_CatGroup, CatGroup_ID)

    '    ddl_CatGroup_SelectedIndexChanged(sender, e)
    '    pnlCatGroup.Visible = Sub_Dept_ID <> 0
    'End Sub


    'Protected Sub ddl_CatGroup_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddl_CatGroup.SelectedIndexChanged
    '    BL.BindDDlCAT(ddl_Cat, User_ID, Dept_ID, 0, True, ddl_CatGroup.SelectedValue)
    '    ddl_Cat_SelectedIndexChanged(sender, e)
    '    pnlCat.Visible = CatGroup_ID <> 0
    'End Sub

    'Protected Sub ddl_Cat_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddl_Cat.SelectedIndexChanged
    '    BL.BindDDlSub_Cat(ddl_Sub_Cat, User_ID, Cat_ID)
    '    ddl_Sub_Cat_SelectedIndexChanged(sender, e)
    '    pnlSubCat.Visible = Cat_ID <> 0
    'End Sub

    'Protected Sub ddl_Sub_Cat_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddl_Sub_Cat.SelectedIndexChanged
    '    BL.BindDDlType(ddl_Type, User_ID, Sub_Cat_ID)
    '    ddl_Type_SelectedIndexChanged(sender, e)
    '    pnlType.Visible = Sub_Cat_ID <> 0
    'End Sub

    'Protected Sub ddl_Type_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddl_Type.SelectedIndexChanged
    '    pnlCreate.Visible = Type_ID <> 0
    'End Sub

    Protected Sub btnCreateOK_Click(sender As Object, e As System.EventArgs) Handles btnCreateOK.Click
        Dim Ass_ID As Integer = BL.GetNewPrimaryID("tb_Ass_Header", "Ass_ID")

        '--------------- Save Header--------------------
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter("SELECT * FROM tb_Ass_Header WHERE 1=0", BL.ConnectionString)
        DA.Fill(DT)

        Dim DR As DataRow = DT.NewRow
        DR("Ass_ID") = Ass_ID
        DR("Ref_Year") = (Now.Year + 543).ToString.Substring(2)
        DR("Ref_Month") = Now.Month.ToString.PadLeft(2, "0")
        DR("Dept_ID") = Dept_ID
        DR("Sub_Dept_ID") = Sub_Dept_ID
        DR("Ref_Number") = GetNewRefNumber(DR("Ref_Year"), DR("Ref_Month"))
        DR("Dept_Name") = Dept_Name
        DR("Sub_Dept_Name") = Sub_Dept_Name
        'DR("S_ID") = DBNull.Value
        'DR("S_Tax_No") = DBNull.Value
        'DR("S_Name") = DBNull.Value
        'DR("S_Alias") = DBNull.Value
        'DR("S_Address") = DBNull.Value
        'DR("S_Phone") = DBNull.Value
        'DR("S_Fax") = DBNull.Value
        'DR("S_Contact_Name") = DBNull.Value
        'DR("S_Email") = DBNull.Value
        'DR("S_Type_ID") = DBNull.Value
        'DR("S_Type_Name") = DBNull.Value


        'DR("Item_Type") = Type_ID   '-----เดิมเก็บระดับ Type เปลี่ยนมาประเมินระดับ Running
        DR("Item_Type") = Running_ID
        
        'DR("Item_No") = DBNull.Value
        'DR("Item_Name") = Running_Name

        'DR("Qty") = DBNull.Value
        'DR("UOM") = DBNull.Value
        'DR("Lot") = DBNull.Value
        'DR("Brand") = DBNull.Value
        'DR("Model") = DBNull.Value
        'DR("Color") = DBNull.Value
        'DR("Size") = DBNull.Value
        'DR("Ref_Doc") = DBNull.Value
        'DR("RecievedDate") = DBNull.Value
        'DR("Price") = DBNull.Value
        DR("Role_1_ID") = User_ID
        DR("Role_1_Title") = Session("Title")
        DR("Role_1_Fisrt_Name") = Session("Fisrt_Name")
        DR("Role_1_Last_Name") = Session("Last_Name")
        DR("Role_1_Pos_Name") = Session("Pos_Name")
        DR("Role_1_Start") = Now
        'DR("Role_1_End") = DBNull.Value
        DR("Role_1_Active") = False
        'DR("Role_1_Comment") = DBNull.Value
        'DR("Role_2_ID") = DBNull.Value
        'DR("Role_2_Title") = DBNull.Value
        'DR("Role_2_Fisrt_Name") = DBNull.Value
        'DR("Role_2_Last_Name") = DBNull.Value
        'DR("Role_2_Pos_Name") = DBNull.Value
        'DR("Role_2_Start") = DBNull.Value
        'DR("Role_2_End") = DBNull.Value
        DR("Role_2_Active") = False
        'DR("Role_2_Comment") = DBNull.Value
        'DR("Role_3_ID") = DBNull.Value
        'DR("Role_3_Title") = DBNull.Value
        'DR("Role_3_Fisrt_Name") = DBNull.Value
        'DR("Role_3_Last_Name") = DBNull.Value
        'DR("Role_3_Pos_Name") = DBNull.Value
        'DR("Role_3_Start") = DBNull.Value
        'DR("Role_3_End") = DBNull.Value
        DR("Role_3_Active") = False
        'DR("Role_3_Comment") = DBNull.Value
        'DR("Role_4_ID") = DBNull.Value
        'DR("Role_4_Title") = DBNull.Value
        'DR("Role_4_Fisrt_Name") = DBNull.Value
        'DR("Role_4_Last_Name") = DBNull.Value
        'DR("Role_4_Pos_Name") = DBNull.Value
        'DR("Role_4_Start") = DBNull.Value
        'DR("Role_4_End") = DBNull.Value
        DR("Role_4_Active") = False
        'DR("Role_4_Comment") = DBNull.Value
        DR("Current_Step") = AVLBL.AssessmentStep.Creater
        'DR("AVL_Y") = DBNull.Value
        'DR("AVL_M") = DBNull.Value
        'DR("AVL_No") = DBNull.Value
        'DR("AVL_Status") = DBNull.Value
        DR("Flag_Return") = False
        DR("Update_By") = User_ID
        DR("Update_Time") = Now

        DT.Rows.Add(DR)
        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)
        Dim AssessmentNumber As String = "A" & DR("Ref_Year") & DR("Ref_Month") & DR("Dept_ID") & DR("Ref_Number")

        '--------------- Save Questionaire--------------------
        Dim QT As DataTable = BL.GetQuestionsByCurrentSetting(AVLBL.AssessmentType.Assessment)
        Dim Sql As String = "SELECT * FROM tb_Ass_Detail WHERE 1=0"
        DT = New DataTable
        DA = New SqlDataAdapter(Sql, BL.ConnectionString)
        DA.Fill(DT)

        For i As Integer = 0 To QT.Rows.Count - 1
            Dim Q As DataRow = DT.NewRow
            Q("Ass_ID") = Ass_ID
            Q("Q_GP_ID") = QT.Rows(i).Item("Q_GP_ID")
            Q("Q_ID") = QT.Rows(i).Item("Q_ID")
            'Q("AR_ID") = AVLBL.AssessmentRole.Creater
            Q("Q_GP_Order") = QT.Rows(i).Item("Q_GP_Order")
            Q("Q_Order") = QT.Rows(i).Item("Q_Order")
            Q("Q_GP_Name") = QT.Rows(i).Item("Q_GP_Name")
            Q("Q_Name") = QT.Rows(i).Item("Q_Name")
            Q("Max_Score") = QT.Rows(i).Item("Max_Score")
            Q("Pass_Score") = QT.Rows(i).Item("Pass_Score")
            Q("Get_Score") = 0
            '--------สิทธิ์การประเมินแต่ละข้อ--------
            Q("AR_ID") = QT.Rows(i).Item("AR_ID")
            Q("ckConfirm") = 0
            Q("Q_Comment") = ""
            DT.Rows.Add(Q)
        Next
        cmd = New SqlCommandBuilder(DA)
        DA.Update(DT)
        '---------- Update Header Summary Score--------
        BL.UpdateHeaderScore_Assessment(Ass_ID)
        cmd.Dispose()
        DA.Dispose()
        DT.Dispose()

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('success','สร้างใบประเมินเลขที่ " & AssessmentNumber & "<br>ต่อไปเป็นขั้นตอนของการประเมิน','Assessment_Edit.aspx?Ass_ID=" & Ass_ID & "');", True)

    End Sub

    Private Function GetNewRefNumber(ByVal YY As String, ByVal MM As String) As String
        Dim SQL As String = "SELECT ISNULL(MAX(CAST(Ref_Number AS INT)),0)+1 "
        SQL &= " FROM  tb_Ass_Header "
        SQL &= " WHERE Ref_Year='" & YY.Replace("'", "''") & "' AND Ref_Month='" & MM.Replace("'", "''") & "'"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0).ToString.PadLeft(5, "0")
    End Function

#Region "PrintButton"

    Protected Sub btnPDF_Click(sender As Object, e As System.EventArgs) Handles btnPDF.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/Assessment_List.aspx?Mode=PDF');", True)
    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As System.EventArgs) Handles btnExcel.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('Print/Assessment_List.aspx?Mode=EXCEL');", True)
    End Sub

#End Region




#Region "Dialog ProductType"
    Protected Sub lnkCloseProduct_Click(sender As Object, e As System.EventArgs) Handles lnkCloseProduct.Click
        dialogProductType.Visible = False
    End Sub

    Protected Sub btnProductDialog_Click(sender As Object, e As System.EventArgs) Handles btnProductDialog.Click
        BindListProductType()
        BL.BindDDlCatGroup(ddl_CatGroup_dialog)
        txtSearchProduct_dialog.Text = ""
        BL.BindDDlCAT(ddl_Cat_dialog, User_ID, Dept_ID)
        ddl_CatGroup_dialog_SelectedIndexChanged(sender, e)
        dialogProductType.Visible = True
    End Sub


    Protected Sub ddl_CatGroup_dialog_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddl_CatGroup_dialog.SelectedIndexChanged
        BL.BindDDlCAT(ddl_Cat_dialog, User_ID, Dept_ID, 0, True, ddl_CatGroup_dialog.SelectedValue)
        ddl_Cat_dialog_SelectedIndexChanged(sender, e)
        BindListProductType()
    End Sub

    Protected Sub ddl_Cat_dialog_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddl_Cat_dialog.SelectedIndexChanged
        BL.BindDDlSub_Cat(ddl_Sub_Cat_dialog, User_ID, Search_Cat_ID)
        ddl_Sub_Cat_dialog_SelectedIndexChanged(sender, e)
        BindListProductType()
    End Sub

    Protected Sub ddl_Sub_Cat_dialog_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddl_Sub_Cat_dialog.SelectedIndexChanged
        BL.BindDDlType(ddl_Type_dialog, User_ID, Search_Sub_Cat_ID)
        ddl_Type_dialog_SelectedIndexChanged(sender, e)
        BindListProductType()
    End Sub

    Protected Sub ddl_Type_dialog_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddl_Type_dialog.SelectedIndexChanged
        BindListProductType()
    End Sub

    Protected Sub btnSearchProduct_dialog_Click(sender As Object, e As System.EventArgs) Handles btnSearchProduct_dialog.Click
        BindListProductType()
    End Sub

    Private Sub BindListProductType()
        Dim DT As DataTable = BL.GetProductTypeByDept(Dept_ID)  '--------เฉพาะรายการที่มี RunningNo

        Dim Filter As String = ""

        If Search_CatGroup_ID <> 0 Then
            Filter &= " CG_ID=" & Search_CatGroup_ID & " AND "
        Else
            Filter &= " (CG_ID IN (1,2) OR CG_ID IS NULL) AND "
        End If

        If Search_Type_ID <> 0 Then
            Filter &= " Type_ID=" & Search_Type_ID & " AND "
        ElseIf Search_Sub_Cat_ID <> 0 Then
            Filter &= " Sub_Cat_ID=" & Search_Sub_Cat_ID & " AND "
        ElseIf Search_Cat_ID <> 0 Then
            Filter &= " Cat_ID=" & Search_Cat_ID & " AND "
        End If

        If txtSearchProduct_dialog.Text <> "" Then
            Filter &= "( Cat_No+Sub_Cat_No+Type_No+Running_No Like '%" & txtSearchProduct_dialog.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Cat_No+Sub_Cat_No+Type_No Like '%" & txtSearchProduct_dialog.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Cat_Name Like '%" & txtSearchProduct_dialog.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Sub_Cat_Name Like '%" & txtSearchProduct_dialog.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Type_Name Like '%" & txtSearchProduct_dialog.Text.Replace("'", "''") & "%' OR " & vbLf
            Filter &= " Running_Name Like '%" & txtSearchProduct_dialog.Text.Replace("'", "''") & "%' " & vbLf
            Filter &= " ) AND "
        End If

        If Filter <> "" Then DT.DefaultView.RowFilter = Filter.Substring(0, Filter.Length - 4)
        DT = DT.DefaultView.ToTable
        Session("Assessment_List_ProductTypeList") = DT
        PagerProduct.SesssionSourceName = "Assessment_List_ProductTypeList"
        PagerProduct.RenderLayout()

        If DT.Rows.Count = 0 Then
            lblTotalProduct.Text = "ไม่พบรายการดังกล่าว"
        Else
            lblTotalProduct.Text = "พบ " & FormatNumber(DT.Rows.Count, 0) & " รายการ"
        End If

    End Sub

    Protected Sub PagerProduct_PageChanging(Sender As PageNavigation) Handles PagerProduct.PageChanging
        PagerProduct.TheRepeater = rptProductTypeList
    End Sub

    Protected Sub rptProductTypeList_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptProductTypeList.ItemCommand
        Select Case e.CommandName
            Case "select"
                'Dim Running_ID As Integer
                Running_ID = e.CommandArgument
                'Dim lbl_Running As Label = e.Item.FindControl("lbl_Running")
                'Running_ID = lbl_Running.Attributes("Running_ID")
                'Dim TP As DataTable = BL.GetProductTypeByType(_Type_ID, Running_ID)
                Dim TP As DataTable = BL.GetProductTypeByRunningNo(Running_ID)

                If TP.Rows.Count = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','ไม่พบข้อมูลผู้ขายรายนี้','');", True)
                    Exit Sub
                End If
                '--------------- Update ข้อมูลเข้าไปในฟอร์ม ------------------------
                'Type_ID = _Type_ID

                'SaveProductType()
                'BindProductType()
                BindProductType()
                dialogProductType.Visible = False
                pnlCreate.Visible = Running_ID <> 0
        End Select
    End Sub

    Protected Sub rptProductTypeList_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptProductTypeList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lbl_Cat As Label = e.Item.FindControl("lbl_Cat")
        Dim lbl_SubCat As Label = e.Item.FindControl("lbl_SubCat")
        Dim lbl_Type As Label = e.Item.FindControl("lbl_Type")
        Dim lbl_Running As Label = e.Item.FindControl("lbl_Running")

        lbl_Cat.Text = e.Item.DataItem("Cat_No") & " : " & e.Item.DataItem("Cat_Name")
        lbl_SubCat.Text = e.Item.DataItem("Cat_No") & e.Item.DataItem("Sub_Cat_No") & " : " & e.Item.DataItem("Sub_Cat_Name")
        lbl_Type.Text = e.Item.DataItem("Cat_No") & e.Item.DataItem("Sub_Cat_No") & e.Item.DataItem("Type_No") & " : " & e.Item.DataItem("Type_Name")

        lbl_Running.Text = e.Item.DataItem("Cat_No") & e.Item.DataItem("Sub_Cat_No") & e.Item.DataItem("Type_No") & e.Item.DataItem("Running_No") & " : " & e.Item.DataItem("Running_Name")
        'lbl_Running.Attributes("Running_ID") = e.Item.DataItem("Running_ID")

       


        Dim btnSelect As Button = e.Item.FindControl("btnSelect")
        For i As Integer = 1 To 4
            Dim td As HtmlTableCell = e.Item.FindControl("td" & i)
            td.Attributes("onClick") = "document.getElementById('" & btnSelect.ClientID & "').click();"
        Next
        btnSelect.CommandArgument = e.Item.DataItem("Running_ID")
       
    End Sub

    Private Sub BindProductType()
        Dim DT As DataTable = BL.GetProductTypeByRunningNo(Running_ID)
        Dim Filter As String = ""
        Select Case DT.Rows(0).Item("CG_ID").ToString
            Case 0
                lblHeader_Item.Text = "สินค้า, พัสดุครุภัณฑ์ หรือ งานบริการจัดจ้าง"

            Case 2
                lblHeader_Item.Text = "จัดจ้างบริการ"

            Case Else
                lblHeader_Item.Text = "สินค้า,พัสดุครุภัณฑ์"
        End Select
        lbl_Item_Type.Text = DT.Rows(0).Item("Cat_No").ToString & DT.Rows(0).Item("Sub_Cat_No").ToString
        lbl_Item_Type.Text += DT.Rows(0).Item("Type_No").ToString & DT.Rows(0).Item("Running_No").ToString & " : " & DT.Rows(0).Item("Running_Name").ToString
        Running_Name = DT.Rows(0).Item("Running_Name").ToString
        'If Running_ID <> "" Then
        'Else
        '    lbl_Item_Type.Text += DT.Rows(0).Item("Type_No").ToString & " : " & DT.Rows(0).Item("Type_Name").ToString
        'End If

        pnlHeaderDialog.Visible = True

    End Sub

#End Region



    Protected Sub ddl_Search_Dept_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddl_Search_Dept.SelectedIndexChanged
        BL.BindDDlSub_Dept(ddl_Sub_Dept, Dept_ID, User_ID, AVLBL.AssessmentRole.Creater, "", True)
        'ddl_Sub_Dept_SelectedIndexChanged(sender, e)
        pnlSubDept.Visible = Dept_ID.ToString() <> "0"
       
        BL.BindDDlSubDept_Search(ddl_Search_SUB_DEPT, ddl_Search_Dept.SelectedValue)
        ddl_Search_SUB_DEPT.SelectedIndex = 0
    End Sub


End Class
