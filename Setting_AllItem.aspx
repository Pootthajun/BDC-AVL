﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Setting_AllItem.aspx.vb" Inherits="Setting_AllItem" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link type="text/css" rel="stylesheet" href="assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" />
    <style type="text/css">
        .white, .white:hover
        {
            background-color:White;
            }
            
           .btn-group
              {
                  white-space: normal;
                  margin-left:-15px;
               }
           .lnkheight
              {
                  line-height:30px;
                  padding-left:10px;
               }
           .btn
           {
              padding: 7px 5px
           }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>

<div class="container-fluid">
			<!-- BEGIN PAGE HEADER-->
			<div class="row-fluid">
				<div class="span12"> 
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3>
						สินค้า/พัสดุครุภัณฑ์
					</h3>		
					<ul class="breadcrumb">
                           
                        <li><i class="icon-cogs"></i> <a href="javascript:;">ตั้งค่าระบบ</a><i class="icon-angle-right"></i></li>
                        <li><i class="icon-barcode"></i> <a href="javascript:;"> สินค้า/พัสดุครุภัณฑ์</a></li>
                        	
                        <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                    </ul>
					<!-- END PAGE TITLE & BREADCRUMB-->	
				    </div>				
			</div>
            <asp:Panel ID="pnlList" runat="server" DefaultButton ="btnSearch">
                <div class="row-fluid">
                    <div class="span12">
						    <!-- BEGIN SAMPLE TABLE PORTLET-->
						                                                                                                                                                                                                                                                                                                                <div class="portlet">
							<div class="portlet-body" style="margin-bottom:-25px;">
                               
                               <div class="table-toolbar">
									<div class="btn-group" style=" margin-left: 5px;">
                                        <asp:LinkButton ID="btnAddCat1" CssClass="btn green" runat="server">เพิ่มประเภท <i class="icon-plus"></i></asp:LinkButton>
                                    </div>
                                    <div class="btn-group">
                                        <asp:LinkButton ID="btnAddSubCat1" runat="server" CssClass="btn green">เพิ่มชนิด <i class="icon-plus"></i></asp:LinkButton>                                        
                                    </div>
                                    <div class="btn-group">
                                        <asp:LinkButton ID="btnAddType1" runat="server" CssClass="btn green"> เพิ่มชนิดย่อย <i class="icon-plus"></i></asp:LinkButton>
                                    </div>
                                    <div class="btn-group">
                                        <asp:LinkButton ID="btnAddRunning1" runat="server" CssClass="btn green"> เพิ่ม RunningNo <i class="icon-plus"></i></asp:LinkButton>

                                    </div>
                                    <div class="btn-group">
                                        <asp:LinkButton ID="btnSearch" runat="server" CssClass="btn blue "> ค้นหา <i class="icon-search"></i></asp:LinkButton>
                                    </div>
                                  
								</div>
                                 <%--แบ่งส่วนการค้นหา--%>
                                <fieldset>
                                    <legend><b>ค้นหา</b></legend>
                                </fieldset>
                                <div class="row-fluid form-horizontal">
                                   
									<div class="row-fluid form-horizontal">
                                        <div class="span4 " id="divDept" runat="server" visible ="false">
										        <div class="control-group">
											        <label class="control-label"><i class="icon-sitemap"></i> สำนักงาน</label>
											        <div class="controls" >
													    <asp:DropDownList ID="ddlDept_Search" runat="server" CssClass="medium m-wrap">
													    </asp:DropDownList>													               
												    </div>														           												            
											    </div>
                                        </div>

                                        <div class="span4 ">
                                                <div class="control-group">
											       <label class="control-label"><i class="icon-bolt"></i> กลุ่ม</label>
                                                    <div class="controls" >
                                                        <asp:DropDownList ID="ddlGroupCat" runat="server" CssClass="medium m-wrap" AutoPostBack="true">
                                                            <asp:ListItem Text="ทั้งหมด" Selected="True"></asp:ListItem>
                                                            <asp:ListItem Text="สินค้า/พัสดุครุภัณฑ์" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="จัดจ้าง/บริการ" Value="2"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>								           												            
											    </div>
                                        </div>

                                        <div class="span6 ">
										        <div class="control-group">											       	
                                                    <label class="control-label"><i class="icon-barcode"></i> รหัสครุภัณฑ์</label>
                                                    <div class="controls" >
                                                        <asp:TextBox ID="txtItemCode_Search" runat="server" CssClass="medium m-wrap" ></asp:TextBox>
                                                    </div>													           												            
											    </div>
                                        </div>
                                        
                                    </div>                                   
                                    	                                  
                                    <div class="row-fluid form-horizontal">  
                                        <div class="span4 ">
                                                <div class="control-group">
                                                    <label class="control-label"><i class="icon-search"></i> ชื่อ/คำค้น</label>
                                                    <div class="controls" >
                                                        <asp:TextBox ID="txtKeyword_Search" runat="server" CssClass="medium m-wrap" ></asp:TextBox>
                                                    </div>
                                                </div>
                                        </div>
                                       <div class="span6 ">
										        <div class="control-group">
											       <label class="control-label"><i class="icon-bolt"></i> สถานะ</label>
                                                    <div class="controls" >
                                                        <asp:DropDownList ID="ddlStatus_Search" runat="server" CssClass="medium m-wrap" >
                                                            <asp:ListItem Text="ทั้งหมด" Selected="True"></asp:ListItem>
                                                            <asp:ListItem Text="ใช้ในระบบ"></asp:ListItem>
                                                            <asp:ListItem Text="ไม่ได้ใช้ในระบบ"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>								           												            
											    </div>
                                        </div>
                                    </div> 
                                    <div class="row-fluid form-horizontal" id="dvGroupCat" runat="server" visible="true">  
                                        <%--<div class="span4 ">
                                                <div class="control-group">
											       <label class="control-label"><i class="icon-bolt"></i> กลุ่ม</label>
                                                    <div class="controls" >
                                                        <asp:DropDownList ID="ddlGroupCat" runat="server" CssClass="medium m-wrap" AutoPostBack="true">
                                                            <asp:ListItem Text="ทั้งหมด" Selected="True"></asp:ListItem>
                                                            <asp:ListItem Text="สินค้า/พัสดุครุภัณฑ์" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="จัดจ้าง/บริการ" Value="2"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>								           												            
											    </div>
                                        </div>--%>
                                       <div class="span6 ">
										        
                                        </div>
                                    </div> 
                                                                       
                                  </div>                                    
							 </div> 
						</div>
                                <div class="portlet-body no-more-tables"> 
                                   <asp:Label ID="lblTotalList" runat="server" Width="100%" Font-Size="16px" Font-Bold="True" style="text-align:center;"></asp:Label>    
                                   <table class="table table-bordered table-advance">                                                                                                                                                                                                 
									    <thead>
										    <tr>											
                                                
                                                <th style="text-align:center;"><i class="icon-list"></i> ประเภท</th>
											    <th style="text-align:center;"><i class="icon-list"></i> ชนิด</th>
											    <th style="text-align:center;"><i class="icon-list"></i> ชนิดย่อย</th>  
                                                <th style="text-align:center;"><i class="icon-list"></i> RunningNo</th>                                            
										    </tr>
									    </thead>
									    <tbody>
                                          <asp:Repeater ID="rptList" runat="server">								   
                                            <ItemTemplate> 
                                                <tr id="trDept" runat="server">
                                                    <td data-title="สำนักงาน" colspan="4" style="text-align: center">
                                                        <asp:Label ID="lblDept" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
                                                    </td>
                                                </tr>
										        <tr id="tr_Edit" runat ="server" visible ="True">											
                                                    
                                                    <td id="tdCat" runat="server" style="border-right:1px solid #eeeeee; ">
                                                        <div class="btn-group lnkheight">
                                                            <asp:LinkButton ID="lnk_Cat" runat="server" CssClass="btn white dropdown-toggle" data-toggle="dropdown"></asp:LinkButton>
												            <ul class="dropdown-menu">
													            <li><asp:LinkButton ID="lnk_Cat_Edit" runat="server" CommandName="EditCat"><i class="icon-edit"></i> แก้ไข</asp:LinkButton></li>
													            <li><asp:LinkButton ID="lnk_Cat_AddSub" runat="server" CommandName="AddSubCat"><i class="icon-plus-sign"></i> เพิ่มชนิด</asp:LinkButton></li>
													            <li><asp:LinkButton ID="lnk_Cat_Toggle" runat="server" CommandName="ToggleCat"><i class="icon-remove"></i> ไม่ได้ใช้ในระบบ</asp:LinkButton></li><% 'icon-ok %>
													            <li><asp:LinkButton ID="lnk_Cat_Delete" runat="server" CommandName="DeleteCat"><i class="icon-trash"></i> ลบประเภทนี้</asp:LinkButton></li>
												            </ul>
                                                        </div>
                                                    </td>
											        <td id="tdSubCat" runat="server" style="border-right:1px solid #eeeeee; ">
                                                        <div class="btn-group lnkheight">
                                                            <asp:LinkButton ID="lnk_SubCat" runat="server" CssClass="btn white dropdown-toggle" data-toggle="dropdown"></asp:LinkButton>
                                                            <ul class="dropdown-menu">
													            <li><asp:LinkButton ID="lnk_SubCat_Edit" runat="server" CommandName="EditSubCat"><i class="icon-edit"></i> แก้ไข</asp:LinkButton></li>
													            <li><asp:LinkButton ID="lnk_SubCat_AddSub" runat="server" CommandName="AddType"><i class="icon-plus-sign"></i> เพิ่มชนิดย่อย</asp:LinkButton></li>
													            <li><asp:LinkButton ID="lnk_SubCat_Toggle" runat="server" CommandName="ToggleSubCat"><i class="icon-remove"></i> ไม่ได้ใช้ในระบบ</asp:LinkButton></li><% 'icon-ok %>
													            <li><asp:LinkButton ID="lnk_SubCat_Delete" runat="server" CommandName="DeleteSubCat"><i class="icon-trash"></i> ลบชนิดนี้</asp:LinkButton></li>
												            </ul>
                                                        </div>                                                            
                                                    </td>
											        <td id="tdType" runat="server" style="border-right:1px solid #eeeeee; ">
                                                        <div class="btn-group lnkheight">
                                                            <asp:LinkButton ID="lnk_Type" runat="server" CssClass="btn white dropdown-toggle" data-toggle="dropdown"></asp:LinkButton>
                                                            <ul class="dropdown-menu">
													            <li><asp:LinkButton ID="lnk_Type_Edit" runat="server" CommandName="EditType"><i class="icon-edit"></i> แก้ไข</asp:LinkButton></li>
                                                                <li><asp:LinkButton ID="lnk_Type_AddSub" runat="server" CommandName="AddRunningNo"><i class="icon-plus-sign"></i> เพิ่ม RunningNo</asp:LinkButton></li>
													            <li><asp:LinkButton ID="lnk_Type_Toggle" runat="server" CommandName="ToggleType"><i class="icon-remove"></i> ไม่ได้ใช้ในระบบ</asp:LinkButton></li><% 'icon-ok %>
													            <li><asp:LinkButton ID="lnk_Type_Delete" runat="server" CommandName="DeleteType"><i class="icon-trash"></i> ลบรหัสนี้</asp:LinkButton></li>
												            </ul>
                                                        </div>
                                                    </td>
                                                    <td id="tdRunningNo" runat="server" style="border-right:1px solid #eeeeee; ">
                                                        <div class="btn-group lnkheight" >
                                                            <asp:LinkButton ID="lnk_RunningNo" runat="server" CssClass="btn white dropdown-toggle" data-toggle="dropdown"></asp:LinkButton>
                                                            <ul class="dropdown-menu">
													            <li><asp:LinkButton ID="lnk_RunningNo_Edit" runat="server" CommandName="EditRunningNo"><i class="icon-edit"></i> แก้ไข</asp:LinkButton></li>
													            <li><asp:LinkButton ID="lnk_RunningNo_Toggle" runat="server" CommandName="ToggleRunningNo"><i class="icon-remove"></i> ไม่ได้ใช้ในระบบ</asp:LinkButton></li><% 'icon-ok %>
													            <li><asp:LinkButton ID="lnk_RunningNo_Delete" runat="server" CommandName="DeleteRunningNo"><i class="icon-trash"></i> ลบรหัสนี้</asp:LinkButton></li>

												            </ul>
                                                        </div>
                                                    </td>
										        </tr>
										        <tr id="tr_View" runat ="server" visible ="false">											
                                                    <td id="tdCat_View" runat="server" style="border-right:1px solid #eeeeee; ">
                                                        
												            <asp:Label ID="lbl_Cat_View" runat="server"></asp:Label>
                                                        
                                                    </td>
											        <td id="tdSubCat_View" runat="server" style="border-right:1px solid #eeeeee; ">
                                                        
                                                            <asp:Label ID="lbl_SubCat_View" runat="server"></asp:Label>
                                                                                                                   
                                                    </td>
											        <td id="tdType_View" runat="server" style="border-right:1px solid #eeeeee; ">
                                                        
                                                            <asp:Label ID="lbl_Type_View" runat="server"></asp:Label>
                                                        
                                                    </td>
                                                    <td id="tdRunningNo_View" runat="server" style="border-right:1px solid #eeeeee; ">
                                                        
                                                            <asp:Label ID="lbl_RunningNo_View" runat="server"></asp:Label>
                                                        
                                                    </td>
										        </tr>

                                                <asp:ConfirmButtonExtender ID="cfmDeleteCat" runat="server" TargetControlID="lnk_Cat_Delete" ConfirmText="การลบประเภททำให้ข้อมูลที่เกี่ยวข้องถูกลบไปด้วย
ยืนยันลบประเภท "></asp:ConfirmButtonExtender>
                                                <asp:ConfirmButtonExtender ID="cfmDeleteSubCat" runat="server" TargetControlID="lnk_SubCat_Delete" ConfirmText="การลบชนิดทำให้ข้อมูลที่เกี่ยวข้องถูกลบไปด้วย
ยืนยันลบชนิด "></asp:ConfirmButtonExtender>
                                                <asp:ConfirmButtonExtender ID="cfmDeleteType" runat="server" TargetControlID="lnk_Type_Delete" ConfirmText="การลบชนิดย่อยทำให้ข้อมูลที่เกี่ยวข้องถูกลบไปด้วย
ยืนยันลบชนิดย่อย "></asp:ConfirmButtonExtender>
                                                <asp:ConfirmButtonExtender ID="cfmDeleteRunningNo" runat="server" TargetControlID="lnk_RunningNo_Delete" ConfirmText="การลบ RunningNo ทำให้ข้อมูลที่เกี่ยวข้องถูกลบไปด้วย
ยืนยันลบ RunningNo "></asp:ConfirmButtonExtender>
                                                </ItemTemplate>
                                            </asp:Repeater>
									</tbody>
								</table>
                                <asp:PageNavigation ID="Pager" runat="server" />
                                </div>   
                               
                                                                                            
                                                                   
                            <!-- END SAMPLE TABLE PORTLET-->
				    </div>
                    
			    </div>

               <div class="form-actions" style="margin-top:0px; margin-bottom:0px;" id="pnlBottomForm" runat="server">
						<asp:LinkButton ID="btnAddCat2" CssClass="btn green" runat="server">เพิ่มประเภท <i class="icon-plus"></i></asp:LinkButton>
						<asp:LinkButton ID="btnAddSubCat2" runat="server" CssClass="btn green">เพิ่มชนิด <i class="icon-plus"></i></asp:LinkButton>
                        <asp:LinkButton ID="btnAddType2" runat="server" CssClass="btn green">เพิ่มชนิดย่อย <i class="icon-plus"></i></asp:LinkButton>
                        <asp:LinkButton ID="btnAddRunning2" runat="server" CssClass="btn green"> เพิ่ม RunningNo <i class="icon-plus"></i></asp:LinkButton>
                                    
			    </div>
         </asp:Panel>
</div>


            <asp:Panel ID="pnlEdit" runat="server" DefaultButton="btnSave" >
                    <div style="z-index: 1049;" class="modal-backdrop fade in"></div>
                    <div style="z-index: 1050; top:10%;" class="modal">
                        
				            <div class="modal-header">
	                            <h3><i class="icon-edit"></i> 
                                    <asp:Label ID="lblEditMode" runat="server"></asp:Label>
                                    <asp:Label ID="lblEditItem" runat="server"></asp:Label>
                                </h3>
                            </div>
                            <div class="modal-body" >
																													
									<div class="row-fluid form-horizontal" id="div_Dept_All" runat="server" visible ="false">
										<div class="span10">
											<div class="control-group">
												<label class="control-label"><i class="icon-sitemap"></i> สำนักงาน</label>
												<div class="controls">
													<asp:DropDownList ID="ddlDeptEdit" runat="server"  CssClass="medium m-wrap" AutoPostBack="true">
                                                    </asp:DropDownList>                                                                   
												</div>
											</div>
										</div>
                                    </div>   
                                     <div class="row-fluid form-horizontal" id="div_Cat_Group_ddl" runat="server">
                                        <div class="span10">
											<div class="control-group">
												<label class="control-label"><i class="icon-list"></i> สินค้า/บริการ</label>
												<div class="controls">
													<asp:DropDownList ID="ddlCatGroupEdit" runat="server" CssClass="medium m-wrap" AutoPostBack="true" >
                                                    </asp:DropDownList>                                                                   
												</div>
											</div>
										</div>
                                    </div>                                    
                                    
                                                                             
                                     <div class="row-fluid form-horizontal" id="div_Cat_ddl" runat="server">
                                        <div class="span10">
											<div class="control-group">
												<label class="control-label"><i class="icon-list"></i> รหัสประเภท</label>
												<div class="controls">
													<asp:DropDownList ID="ddlCatEdit" runat="server" CssClass="medium m-wrap" AutoPostBack="true" >
                                                    </asp:DropDownList>                                                                   
												</div>
											</div>
										</div>
                                    </div>
                                    <div class="row-fluid form-horizontal" id="div_Cat_txt" runat="server">
                                        <div class="span10">
											<div class="control-group">
												<label class="control-label"><i class="icon-list"></i> รหัสประเภท</label>
												<div class="controls">
													<asp:TextBox ID="txtCatEdit" runat="server" CssClass="m-wrap medium" placeholder="กรอกรหัส 4 หลัก" MaxLength="4"></asp:TextBox>                                                                  
												<asp:FilteredTextBoxExtender ID="txtCatEdit_FilteredTextBoxExtender1" 
                                                    runat="server" Enabled="True" TargetControlID="txtCatEdit" 
                                                FilterType="Numbers" InvalidChars="1234567890" >
                                                </asp:FilteredTextBoxExtender>
                                                </div>
											</div>
										</div>
									</div>

                                    <div class="row-fluid form-horizontal" id="div_SubCat_ddl" runat="server">
                                        <div class="span10">
											<div class="control-group">
												<label class="control-label"><i class="icon-list"></i> รหัสชนิด</label>
												<div class="controls">
													<asp:DropDownList ID="ddlSubCatEdit" runat="server"  AutoPostBack="true"  CssClass="medium m-wrap">
                                                    </asp:DropDownList>                                                                   
												</div>
											</div>
										</div>
                                    </div>
                                    <div class="row-fluid form-horizontal" id="div_SubCat_txt" runat="server">
                                        <div class="span10">
											<div class="control-group">
												<label class="control-label"><i class="icon-list"></i> รหัสชนิด</label>
												<div class="controls">
													<asp:TextBox ID="txtSubCatEdit" runat="server" CssClass="m-wrap medium" placeholder="กรอกรหัส 3 หลัก" MaxLength="3"></asp:TextBox>                                                                  
												    <asp:FilteredTextBoxExtender ID="txtSubCatEdit_FilteredTextBoxExtender1" 
                                                    runat="server" Enabled="True" TargetControlID="txtSubCatEdit" 
                                                FilterType="Numbers" InvalidChars="1234567890" >
                                                </asp:FilteredTextBoxExtender>
                                                </div>
											</div>
										</div>
									</div>
                                    <div class="row-fluid form-horizontal" id="div_Type_ddl" runat="server">
                                        <div class="span10">
											<div class="control-group">
												<label class="control-label"><i class="icon-list"></i> รหัสชนิดย่อย</label>
												<div class="controls">
													<asp:DropDownList ID="ddlTypeEdit" runat="server"  CssClass="medium m-wrap">
                                                    </asp:DropDownList>                                                                   
												</div>
											</div>
										</div>
                                    </div>
                                    <div class="row-fluid form-horizontal" id="div_Type_txt" runat="server">
                                        <div class="span10">
											<div class="control-group">
												<label class="control-label"><i class="icon-list"></i> รหัสชนิดย่อย</label>
												<div class="controls">
													<asp:TextBox ID="txtTypeEdit" runat="server" CssClass="m-wrap medium" placeholder="กรอกรหัส 2 หลัก" MaxLength="2"></asp:TextBox>                                                                  
												<asp:FilteredTextBoxExtender ID="txtTypeEdit_FilteredTextBoxExtender1" 
                                                    runat="server" Enabled="True" TargetControlID="txtTypeEdit" 
                                                FilterType="Numbers" InvalidChars="1234567890" >
                                                </asp:FilteredTextBoxExtender>
                                                </div>
											</div>
										</div>
									</div>
                                    <div class="row-fluid form-horizontal" id="div_RunningNo_txt" runat="server">
                                        <div class="span10">
											<div class="control-group">
												<label class="control-label"><i class="icon-list"></i> รหัส RunningNo</label>
												<div class="controls">
													<asp:TextBox ID="txtRunningNoEdit" runat="server" CssClass="m-wrap medium" placeholder="กรอกรหัส 3 หลัก" MaxLength="3"></asp:TextBox>                                                                  
												<asp:FilteredTextBoxExtender ID="txtRunningNoEdit_FilteredTextBoxExtender" 
                                                    runat="server" Enabled="True" TargetControlID="txtRunningNoEdit" 
                                                FilterType="Numbers" InvalidChars="1234567890" >
                                                </asp:FilteredTextBoxExtender>
                                                
                                                </div>
											</div>
										</div>
									</div>
                                    <div class="row-fluid form-horizontal">
                                        <div class="span10">
											<div class="control-group">
												<label class="control-label"><i class="icon-barcode"></i> <asp:Label ID="lblEditName" runat="server"></asp:Label></label>
												<div class="controls">
													<asp:TextBox ID="txtEditName" runat="server" CssClass="m-wrap large" placeholder="กรอกชื่อ" MaxLength="200"></asp:TextBox>                                                                  
												</div>
											</div>
										</div>
									</div>
								</div>	
                                <div class="form-actions" style="margin-top:0px; margin-bottom:0px; text-align:right;">
									                <asp:Button ID="btnCancel" CssClass="btn" runat="server" Text="ยกเลิก" />
										            <asp:Button ID="btnSave" runat="server" CssClass="btn purple" Text="บันทึก" />
							    </div>     
                                <asp:LinkButton ID="btnCloseDialog" runat="server" CssClass="fancybox-item fancybox-close" ToolTip="Close" />                     
                    </div>
                     <asp:TextBox ID="txt_EditMode" runat="server" style="display:none;"></asp:TextBox>
                     <asp:TextBox ID="txt_CatGroup_ID" runat="server" style="display:none;"></asp:TextBox>
                     <asp:TextBox ID="txt_Cat_ID" runat="server" style="display:none;"></asp:TextBox>
                     <asp:TextBox ID="txt_SubCat_ID" runat="server" style="display:none;"></asp:TextBox>
                     <asp:TextBox ID="txt_Type_ID" runat="server" style="display:none;"></asp:TextBox>
                     <asp:TextBox ID="txt_RunningNo_ID" runat="server" style="display:none;"></asp:TextBox>
                 </asp:Panel>

    </ContentTemplate>
</asp:UpdatePanel>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptPlaceHolder" Runat="Server">
</asp:Content>

