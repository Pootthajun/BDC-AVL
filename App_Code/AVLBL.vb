﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System.Configuration
Imports System.IO
Imports System.Drawing
Imports System

Public Class AVLBL

    Public ConnectionString As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString

    Public ReadOnly Property ReAssessmentPeriod As Integer
        Get
            Dim SQL As String = "SELECT ConfigValue FROM tb_Config WHERE ConfigKey='ReAssessmentPeriod'"
            Dim DA As New SqlDataAdapter(SQL, ConnectionString)
            Dim DT As New DataTable
            Try
                DA.Fill(DT)
                Return DT.Rows(0)(0)
            Catch ex As Exception
                Return 2
            End Try
        End Get
    End Property

    Public Structure FileStructure
        Public FileName As String
        Public FileContent As Byte()
        Public ContentType As String
    End Structure

    Public Enum AssessmentType
        ''' <summary>
        ''' ประเมินผู้ขายใหม่
        ''' </summary>
        Assessment = 1
        ''' <summary>
        ''' ประเมินทบทวน
        ''' </summary>
        ReAssessment = 2
    End Enum

    Public Class Q_GP_Info
        ''' <summary>
        ''' รหัสหมวดคำถาม
        ''' </summary>
        Public Q_GP_ID As Integer = 0
        ''' <summary>
        ''' ชื่อหมวดคำถาม
        ''' </summary>
        Public Q_GP_Name As String = ""
        ''' <summary>
        ''' ลำดับข้อ
        ''' </summary>
        Public Q_GP_Order As Integer = 0
        ''' <summary>
        ''' ใช้งานในระบบหรือไม่
        ''' </summary>
        Public Active_Status As String = ""
    End Class

    Public Class UserInfo
        Public User_ID As Integer = -1
        Public User_Login As String = ""
        Public User_Password As String = ""
        Public Dept_ID As String = ""
        Public Dept_Name As String = ""
        Public Prefix_ID As String = ""
        Public Prefix_Name As String = ""
        Public Fisrt_Name As String = ""
        Public Last_Name As String = ""
        Public Pos_Name As String = ""
        Public Cancel_Role As Boolean = False
        Public Active_Status As Boolean = False
    End Class

    Public Class CatGroup_Info
        Public CG_ID As Integer = 0
        Public CG_Name As String = "สินค้าครุภัณฑ์/บริการและจัดจ้าง"
        Public Active_ALL As Boolean = True
    End Class
    Public Enum AssessmentRole
        ''' <summary>
        ''' ไม่มีสิทธิ์
        ''' </summary>
        None = -1
        ''' <summary>
        ''' ผู้จัดทำ
        ''' </summary>
        Creater = 1
        ''' <summary>
        ''' ผู้ตรวจสอบ
        ''' </summary>
        Auditor = 2
        ''' <summary>
        ''' เจ้าหน้าที่พัสดุ
        ''' </summary>
        Supply_Officer = 3
        ''' <summary>
        ''' ผู้บริหาร
        ''' </summary>
        Director = 4
    End Enum

    Public Enum AccessRole
        ''' <summary>
        ''' ไม่มีสิทธิ์
        ''' </summary>
        None = -1
        ''' <summary>
        ''' ดูอย่างเดียว
        ''' </summary>
        View = 1
        ''' <summary>
        ''' เพิ่มเติมแก้ไข
        ''' </summary>
        Edit = 2
    End Enum

    Public Enum AssessmentStep
        ''' <summary>
        ''' ยกเลิก
        ''' </summary>
        NotFound = 0
        ''' <summary>
        ''' ผู้จัดทำ
        ''' </summary>
        Creater = 1
        ''' <summary>
        ''' ผู้ตรวจสอบ
        ''' </summary>
        Auditor = 2
        ''' <summary>
        ''' ฝ่ายพัสดุ
        ''' </summary>
        Supply_Officer = 3
        ''' <summary>
        ''' ผู้บริหาร
        ''' </summary>
        Director = 4
        ''' <summary>
        ''' เสร็จสมบูรณ์
        ''' </summary>
        Completed = 5
    End Enum

    ''' <summary>
    ''' แสดงสีของขั้นตอนการประเมิน
    ''' </summary>
    ''' <param name="AssessmentStep">ขั้นตอนการประเมิน</param>
    Public Function GetStepColor(ByVal AssessmentStep As AssessmentStep) As System.Drawing.Color
        Select Case AssessmentStep
            Case AssessmentRole.Creater
                Return System.Drawing.Color.Orange
            Case AssessmentRole.Auditor
                Return System.Drawing.Color.Brown
            Case AssessmentRole.Supply_Officer
                Return System.Drawing.Color.SteelBlue
            Case AssessmentRole.Director
                Return System.Drawing.Color.BlueViolet
            Case AVLBL.AssessmentStep.Completed
                Return System.Drawing.Color.Green
        End Select
    End Function




    '------Reassessment-----

    Public Enum ReassessmentRole
        ''' <summary>
        ''' ไม่มีสิทธิ์
        ''' </summary>
        None = -1
        ''' <summary>
        ''' ผู้จัดทำ
        ''' </summary>
        Creater = 1
        ''' <summary>
        ''' ผู้ตรวจสอบ
        ''' </summary>
        Auditor = 2
        ''' <summary>
        ''' ฝ่ายพัสดุ
        ''' </summary>
        Supply_Officer = 3
        ''' <summary>
        ''' ผู้บริหาร
        ''' </summary>
        Director = 4
    End Enum

    Public Enum ReassessmentStep
        ''' <summary>
        ''' ยกเลิก
        ''' </summary>
        NotFound = 0
        ''' <summary>
        ''' ผู้จัดทำ
        ''' </summary>
        Creater = 1
        ''' <summary>
        ''' ผู้ตรวจสอบ
        ''' </summary>
        Auditor = 2
        ''' <summary>
        ''' ฝ่ายพัสดุ
        ''' </summary>
        Supply_Officer = 3
        ''' <summary>
        ''' ผู้บริหาร
        ''' </summary>
        Director = 4
        ''' <summary>
        ''' เสร็จสมบูรณ์
        ''' </summary>
        Completed = 5
    End Enum

    ''' <summary>
    ''' แสดงสีของขั้นตอนการประเมินทบทวน
    ''' </summary>
    ''' <param name="ReassessmentStep">ขั้นตอนการประเมินทบทวน</param>
    Public Function GetStepColor_Reass(ByVal ReassessmentStep As ReassessmentStep) As System.Drawing.Color
        Select Case ReassessmentStep
            Case ReassessmentRole.Creater
                Return System.Drawing.Color.Orange
            Case ReassessmentRole.Auditor
                Return System.Drawing.Color.Brown
            Case ReassessmentRole.Supply_Officer
                Return System.Drawing.Color.SteelBlue
            Case ReassessmentRole.Director
                Return System.Drawing.Color.BlueViolet
            Case AVLBL.ReassessmentStep.Completed
                Return System.Drawing.Color.Green
        End Select
    End Function

#Region "BindDDL"
    ''' <summary>
    ''' ใส่ประเภทผู้ขายใน Dropdownlist
    ''' </summary>
    ''' ---DDL ประเภทผู้ขาย--
    ''' <param name="ddl">Dropdownlist ที่ต้องการใส่ข้อมูล</param>
    ''' <param name="SelectedValue">ให้ Dropdownlist แสดงข้อมูลที่ต้องการ</param>
    Public Sub BindDDlSupplierType(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim SQL As String = "SELECT S_Type_ID,S_Type_Name" & vbLf
        SQL &= " FROM tb_Sup_Type " & vbLf
        SQL &= " WHERE Active_Status=1 ORDER BY S_Type_Name   " & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("เลือก", -1))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("S_Type_Name"), DT.Rows(i).Item("S_Type_ID"))
            ddl.Items.Add(Item)
        Next

        ddl.SelectedIndex = 0
        If SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value = SelectedValue Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    ''' <summary>
    ''' ใส่ข้อมูลประเภทหนังสือใน Dropdownlist ที่ต้องการ
    ''' </summary>
    ''' <param name="ddl">Dropdownlist ที่ต้องการใส่ข้อมูล</param>
    ''' <param name="TextIndex">ข้อความตั้งต้นใน Dropdownlist</param>
    ''' <param name="SelectedValue">แสดง Default ที่ระบุตาม Parameter นี้</param>
    Public Sub BindDDlCertificate(ByRef ddl As DropDownList, Optional ByVal TextIndex As String = "", Optional ByVal SelectedValue As Integer = -1)
        Dim SQL As String = "SELECT Cer_No,Cer_Name" & vbLf
        SQL &= " FROM MS_Certificate_Type " & vbLf
        SQL &= " WHERE Active_Status=1"
        SQL &= " ORDER BY Cer_Name  " & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        ddl.Items.Clear()

        ddl.Items.Add(New ListItem(TextIndex, -1))

        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("Cer_Name"), DT.Rows(i).Item("Cer_No"))
            ddl.Items.Add(Item)
        Next

        ddl.SelectedIndex = 0
        If SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value = SelectedValue Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub


    ''' <summary>
    ''' ดึงประเภทพัสดุใส่ Dropdownlist
    ''' </summary>
    ''' <param name="ddl">ใส่ข้อมูลประเภทหนังสือใน Dropdownlist ที่ต้องการ</param>
    ''' <param name="User_ID">รหัส User ที่ใช้ระบบเพราะต้องดูสิทธิ์ด้วย</param>
    ''' <param name="DEPT_ID">ดึงข้อมุลตามแผนกที่ระบุ</param>
    ''' <param name="Cat_Group">รวมถึงประเภทที่ Inactive ด้วย</param>
    ''' <param name="SelectedValue">แสดง Default ที่ระบุตาม Parameter นี้</param>
    ''' <param name="IncludedInactive">รวมถึงประเภทที่ Inactive ด้วย</param>
    Public Sub BindDDlCAT(ByRef ddl As DropDownList, User_ID As Integer, ByVal DEPT_ID As String, Optional ByVal SelectedValue As Integer = 0, Optional IncludedInactive As Boolean = True, Optional ByVal Cat_Group As Integer = 0)

        Dim SQL As String = " " & vbLf
        SQL &= " SELECT DISTINCT Cat.Cat_ID,Cat.Cat_No,Cat.Cat_Name" & vbLf
        SQL &= " FROM tb_Cat Cat" & vbLf
        SQL &= " INNER JOIN tb_DEPT DEPT ON Cat.DEPT_ID=DEPT.DEPT_ID" & vbLf
        'SQL &= " WHERE DEPT.DEPT_ID='" & DEPT_ID.Replace("'", "''") & "'" & vbLf

        SQL &= " WHERE DEPT.DEPT_ID IS NOT NULL" & vbLf
        If Not IncludedInactive Then
            SQL &= " AND Cat.Active_Status=1" & vbLf
        End If
        Select Case Cat_Group
            Case 0

            Case 2  '--------------ประเภทบริการ จัดจ้าง---------------
                SQL &= " AND Cat.CG_ID=2" & vbLf
            Case Else  '-----------1---ยกเว้น บริการ จัดจ้าง รวมถึงที่คีย์มาก่อนหน้านี้---------------
                SQL &= " AND ( Cat.CG_ID NOT IN (2) OR Cat.CG_ID IS NULL )" & vbLf

        End Select

        SQL &= " ORDER BY Cat.Cat_No"
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)


        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("เลือกประเภท", "0"))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("Cat_No").ToString & " : " & DT.Rows(i).Item("Cat_Name").ToString, DT.Rows(i).Item("Cat_ID"))
            ddl.Items.Add(Item)
        Next

        For i As Integer = 0 To ddl.Items.Count - 1
            If ddl.Items(i).Value = SelectedValue Then
                ddl.SelectedIndex = i
                Exit For
            End If
        Next

    End Sub

    ''' <summary>
    ''' ดึงชนิดพัสดุใส่ Dropdownlist
    ''' </summary>
    ''' <param name="ddl">Dropdownlist ที่ต้องการ</param>
    ''' <param name="User_ID">รหัส User ที่ใช้ระบบเพราะต้องดูสิทธิ์ด้วย</param>
    ''' <param name="Cat_ID">ดึงข้อมุลตามประเภทที่ระบุ</param>
    ''' <param name="SelectedValue">แสดง Default ที่ระบุตาม Parameter นี้</param>
    ''' <param name="IncludedInactive">รวมถึงชนิดที่ Inactive ด้วย</param>
    Public Sub BindDDlSub_Cat(ByRef ddl As DropDownList, User_ID As Integer, ByVal Cat_ID As Integer, Optional ByVal SelectedValue As Integer = 0, Optional IncludedInactive As Boolean = True)
        Dim SQL As String = " " & vbLf
        SQL &= " SELECT Sub_Cat.Sub_Cat_ID,Sub_Cat.Sub_Cat_No,Sub_Cat.Sub_Cat_Name" & vbLf
        SQL &= " FROM tb_Cat Cat" & vbLf
        SQL &= " INNER JOIN tb_Sub_Cat Sub_Cat ON Cat.Cat_ID=Sub_Cat.Cat_ID" & vbLf
        SQL &= " WHERE Cat.Cat_ID=" & Cat_ID & vbLf
        If Not IncludedInactive Then
            SQL &= " AND Sub_Cat.Active_Status=1" & vbLf
        End If
        SQL &= " ORDER BY Sub_Cat.Sub_Cat_No" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("เลือกชนิด", "0"))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("Sub_Cat_No").ToString & " : " & DT.Rows(i).Item("Sub_Cat_Name").ToString, DT.Rows(i).Item("Sub_Cat_ID"))
            ddl.Items.Add(Item)
        Next

        For i As Integer = 0 To ddl.Items.Count - 1
            If ddl.Items(i).Value = SelectedValue Then
                ddl.SelectedIndex = i
                Exit For
            End If
        Next
    End Sub

    ''' <summary>
    ''' ดึงชนิดย่อยใส่ Dropdownlist
    ''' </summary>
    ''' <param name="ddl">Dropdownlist ที่ต้องการ</param>
    ''' <param name="User_ID">รหัส User ที่ใช้ระบบเพราะต้องดูสิทธิ์ด้วย</param>
    ''' <param name="Sub_Cat_ID">ดึงข้อมุลตามชนิดที่ระบุ</param>
    ''' <param name="SelectedValue">แสดง Default ที่ระบุตาม Parameter นี้</param>
    ''' <param name="IncludedInactive">รวมถึงชนิดที่ Inactive ด้วย</param>
    Public Sub BindDDlType(ddl As System.Web.UI.WebControls.DropDownList, User_ID As Integer, Sub_Cat_ID As Integer, Optional SelectedValue As Integer = 0, Optional IncludedInactive As Boolean = True)
        Dim SQL As String = " " & vbLf
        SQL &= " SELECT _TYPE.Type_ID,_TYPE.Type_No,_TYPE.Type_Name" & vbLf
        SQL &= " FROM tb_Type _TYPE" & vbLf
        SQL &= " INNER JOIN tb_Sub_Cat Sub_Cat ON _TYPE.Sub_Cat_ID=Sub_Cat.Sub_Cat_ID" & vbLf
        SQL &= " WHERE Sub_Cat.Sub_Cat_ID=" & Sub_Cat_ID & vbLf
        If Not IncludedInactive Then
            SQL &= " AND _TYPE.Active_Status=1" & vbLf
        End If
        SQL &= " ORDER BY _TYPE.Type_No" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("เลือกชนิดย่อย", "0"))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("Type_No").ToString & " : " & DT.Rows(i).Item("Type_Name").ToString, DT.Rows(i).Item("Type_ID"))
            ddl.Items.Add(Item)
        Next

        For i As Integer = 0 To ddl.Items.Count - 1
            If ddl.Items(i).Value = SelectedValue Then
                ddl.SelectedIndex = i
                Exit For
            End If
        Next
    End Sub


    ''' <summary>
    ''' ดึงชนิดย่อยใส่ Dropdownlist
    ''' </summary>
    ''' <param name="ddl">Dropdownlist ที่ต้องการ</param>
    ''' <param name="User_ID">รหัส User ที่ใช้ระบบเพราะต้องดูสิทธิ์ด้วย</param>
    ''' <param name="Sub_Cat_ID">ดึงข้อมุลตามชนิดที่ระบุ</param>
    ''' <param name="SelectedValue">แสดง Default ที่ระบุตาม Parameter นี้</param>
    ''' <param name="IncludedInactive">รวมถึงชนิดที่ Inactive ด้วย</param>
    Public Sub BindDDlRunning(ddl As System.Web.UI.WebControls.DropDownList, User_ID As Integer, Sub_Cat_ID As Integer, Optional SelectedValue As Integer = 0, Optional IncludedInactive As Boolean = True)
        Dim SQL As String = " " & vbLf
        SQL &= " SELECT _TYPE.Type_ID,_TYPE.Type_No,_TYPE.Type_Name" & vbLf
        SQL &= " FROM tb_Type _TYPE" & vbLf
        SQL &= " INNER JOIN tb_Sub_Cat Sub_Cat ON _TYPE.Sub_Cat_ID=Sub_Cat.Sub_Cat_ID" & vbLf
        SQL &= " WHERE Sub_Cat.Sub_Cat_ID=" & Sub_Cat_ID & vbLf
        If Not IncludedInactive Then
            SQL &= " AND _TYPE.Active_Status=1" & vbLf
        End If
        SQL &= " ORDER BY _TYPE.Type_No" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("เลือกประเภท", "0"))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("Type_No").ToString & " : " & DT.Rows(i).Item("Type_Name").ToString, DT.Rows(i).Item("Type_ID"))
            ddl.Items.Add(Item)
        Next

        For i As Integer = 0 To ddl.Items.Count - 1
            If ddl.Items(i).Value = SelectedValue Then
                ddl.SelectedIndex = i
                Exit For
            End If
        Next
    End Sub

    ''' <summary>
    ''' ดึงชื่อแผนกใส่ Dropdownlist
    ''' </summary>
    ''' <param name="ddl">Dropdownlist ที่ต้องการใส่ข้อมูล</param>
    ''' <param name="User_ID">รหัส User ที่ใช้ระบบเพราะต้องดูสิทธิ์ด้วย</param>
    ''' <param name="SelectedValue">แสดง Default ที่ระบุตาม Parameter นี้</param>
    ''' 
    Public Sub BindDDlDEPT(ByRef ddl As DropDownList, User_ID As Integer, Optional ByVal SelectedValue As String = "")

        Dim DT As DataTable = GetUserDeptRole(User_ID)
        Dim Col() As String = {"Dept_ID", "Dept_Name"}
        DT = DT.DefaultView.ToTable(True, Col)
        ddl.Items.Clear()

        ddl.Items.Add(New ListItem("เลือกสำนักงาน", 0)) '--""-----

        '------------- is matched for user role ----------
        Dim RT As DataTable = GetUserDeptRole(User_ID)

        For i As Integer = 0 To DT.Rows.Count - 1
            RT.DefaultView.RowFilter = "Dept_ID='" & DT.Rows(i).Item("Dept_ID") & "'"
            If RT.DefaultView.Count > 0 Then
                Dim Item As New ListItem(DT.Rows(i).Item("Dept_Name").ToString.Replace("บริการโลหิตแห่งชาติที่", "ที่").Replace("สภากาชาดไทย", "ฯ"), DT.Rows(i).Item("Dept_ID"))
                ddl.Items.Add(Item)
            End If
        Next

        ddl.SelectedIndex = 0
        If SelectedValue <> "" Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value = SelectedValue Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub



    ''' <summary>
    ''' ใส่ข้อมูลสำนักงานทั้งหมดใน Dropdownlist ที่ต้องการ
    ''' </summary>
    ''' <param name="ddl">Dropdownlist ที่ต้องการใส่ข้อมูล</param>
    ''' <param name="SelectedValue">แสดง Default ที่ระบุตาม Parameter นี้</param>
    Public Sub BindDDlDeptALL(ByRef ddl As DropDownList, Optional ByVal SelectedValue As String = "")
        Dim SQL As String = "SELECT * " & vbLf
        SQL &= " FROM  tb_Dept" & vbLf
        SQL &= " WHERE Active_Status=1"
        SQL &= " " & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        ddl.Items.Clear()

        ddl.Items.Add(New ListItem("เลือกสำนักงาน", 0))

        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("Dept_Name"), DT.Rows(i).Item("Dept_ID"))
            ddl.Items.Add(Item)
        Next

        ddl.SelectedIndex = 0
        If SelectedValue <> "" Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value = SelectedValue Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    ''' <summary>
    ''' ใส่ข้อมูล SubDeptทั้งหมดใน Dropdownlist ที่ต้องการ
    ''' </summary>
    ''' <param name="ddl">Dropdownlist ที่ต้องการใส่ข้อมูล</param>
    ''' <param name="SelectedValue">แสดง Default ที่ระบุตาม Parameter นี้</param>
    Public Sub BindDDlDeptN(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim SQL As String = "SELECT * " & vbLf
        SQL &= " FROM  tb_Sub_Dept" & vbLf
        SQL &= " WHERE Active_Status=1"
        SQL &= " " & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        ddl.Items.Clear()

        ddl.Items.Add(New ListItem("เลือกสำนักงาน", -1))

        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("Sub_Dept_Name"), DT.Rows(i).Item("Sub_Dept_ID"))
            ddl.Items.Add(Item)
        Next

        ddl.SelectedIndex = 0
        If SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value = SelectedValue Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub


    ''' <summary>
    ''' ใส่ข้อมูลสำนักงานทั้งหมดใน Dropdownlist ที่ต้องการ
    ''' </summary>
    ''' <param name="ddl">Dropdownlist ที่ต้องการใส่ข้อมูล</param>
    ''' <param name="SelectedValue">แสดง Default ที่ระบุตาม Parameter นี้</param>
    Public Sub BindDDlSubDept_Search(ByRef ddl As DropDownList, ByRef DEPT_ID As String, Optional ByVal SelectedValue As Integer = -1)
        Dim SQL As String = "SELECT * " & vbLf
        SQL &= " FROM  tb_Sub_Dept" & vbLf
        SQL &= " WHERE Dept_ID='" & DEPT_ID & "'"
        SQL &= " " & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        ddl.Items.Clear()

        ddl.Items.Add(New ListItem("ทั้งหมด", -1))

        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("Sub_Dept_Name"), DT.Rows(i).Item("Sub_Dept_ID"))
            ddl.Items.Add(Item)
        Next

        ddl.SelectedIndex = 0
        If SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value = SelectedValue Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub


    ''' <summary>
    ''' ใส่ข้อมูลฝ่าย/ภาค/งานทั้งหมดในสำนักงานที่เลือกใน Dropdownlist ที่ต้องการ
    ''' </summary>
    ''' <param name="ddl">Dropdownlist ที่ต้องการใส่ข้อมูล</param>
    ''' <param name="Dept_ID"> ID ของ ฝ่าย/ภาค/งาน</param>
    ''' <param name="User_ID"> ID ของ User</param>  
    ''' <param name="SelectedValue">แสดง Default ที่ระบุตาม Parameter นี้</param>
    Public Sub BindDDlSub_Dept(ByRef ddl As DropDownList, ByRef Dept_ID As String, ByRef User_ID As Integer, ByVal Role As AssessmentRole, Optional ByVal SelectedValue As String = "", Optional ByVal Start As Boolean = False)
        Dim SQL As String = ""
        SQL &= " SELECT Dept.Dept_ID ,Dept .Dept_Name ,Sub_Dept .Sub_Dept_ID ,Sub_Dept .Sub_Dept_Name ,Sub_Dept .Active_Status  " & vbLf
        SQL &= " FROM tb_Sub_Dept Sub_Dept" & vbLf
        SQL &= " LEFT JOIN tb_Dept Dept ON Dept.Dept_ID =Sub_Dept .Dept_ID " & vbLf
        SQL &= " LEFT JOIN tb_Dept_Role Role ON Role.Dept_ID =Dept .Dept_ID AND Role .Sub_Dept_ID = Sub_Dept .Sub_Dept_ID " & vbLf
        SQL &= " WHERE Sub_Dept.Dept_ID = '" & Dept_ID & "' AND Sub_Dept .Active_Status = 1" & vbLf

        If Start Then
            SQL &= " AND  Role .User_ID ='" & User_ID & "'" & vbLf
            SQL &= " AND  Role.AR_ID =1" & vbLf
        End If
        SQL &= " ORDER BY Sub_Dept .Sub_Dept_Name"
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        ddl.Items.Clear()

        ddl.Items.Add(New ListItem("เลือกฝ่าย/ภาค/งาน", 0))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("Sub_Dept_Name").ToString(), DT.Rows(i).Item("Sub_Dept_ID").ToString())
            ddl.Items.Add(Item)
        Next
        ddl.SelectedIndex = 0
        If SelectedValue <> "" Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value = SelectedValue Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    ''' <summary>
    ''' ใส่ข้อมูลกลุ่มสินค้าหรือบริการทั้งหมดใน Dropdownlist ที่ต้องการ
    ''' </summary>
    ''' <param name="ddl">Dropdownlist ที่ต้องการใส่ข้อมูล</param>
    ''' <param name="SelectedValue">แสดง Default ที่ระบุตาม Parameter นี้</param>
    Public Sub BindDDlCatGroup(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = -1)
        Dim SQL As String = "SELECT CG_ID,CG_Name,Active_ALL " & vbLf
        SQL &= " FROM  tb_Cat_Group" & vbLf
        SQL &= " ORDER BY CG_ID" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        ddl.Items.Clear()

        ddl.Items.Add(New ListItem("เลือกกลุ่มสินค้าหรือบริการ", 0))

        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("CG_Name"), DT.Rows(i).Item("CG_ID"))
            ddl.Items.Add(Item)
        Next

        ddl.SelectedIndex = 0
        If SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value = SelectedValue Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    ''' <summary>
    ''' ใส่ลำดับคำถามใน Dropdownlist
    ''' </summary>
    ''' ---DDL ลำดับคำถาม--
    ''' <param name="ddl">Dropdownlist ที่ต้องการใส่ข้อมูล</param>
    ''' <param name="SelectedValue">ให้ Dropdownlist แสดงข้อมูลที่ต้องการ</param>
    Public Sub BindDDlQ_Group_Order(ByRef ddl As DropDownList, ByVal AssessmentType As AssessmentType, Optional ByVal SelectedValue As Integer = -1, Optional ByVal AddNewItem As Integer = 0)
        Dim SQL As String = "SELECT Q_GP_ID,Q_GP_Name,Q_GP_Order,Active_Status" & vbLf

        Select Case AssessmentType
            Case AVLBL.AssessmentType.Assessment
                SQL &= " FROM tb_Ass_Q_Group" & vbLf
            Case AVLBL.AssessmentType.ReAssessment
                SQL &= " FROM tb_Re_Q_Group" & vbLf
        End Select
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        If SelectedValue > 0 Then
            For i As Integer = 1 To DT.Rows.Count
                Dim Item As New ListItem(i, i)
                ddl.Items.Add(Item)
            Next
        Else
            For i As Integer = 1 To DT.Rows.Count + 1
                Dim Item As New ListItem(i, i)
                ddl.Items.Add(Item)
            Next
        End If

        ddl.SelectedIndex = 0
        If SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value = SelectedValue Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        Else
            ddl.SelectedIndex = ddl.Items.Count - 1
        End If
    End Sub



    ''' <summary>
    ''' ใส่ลำดับคำถามใน Dropdownlist
    ''' </summary>
    ''' ---DDL ลำดับคำถาม--
    ''' <param name="ddl">Dropdownlist ที่ต้องการใส่ข้อมูล</param>
    ''' <param name="Q_GP_ID">รหัสกลุ่มคำถาม</param>
    ''' <param name="SelectedValue">ให้ Dropdownlist แสดงข้อมูลที่ต้องการ</param>
    Public Sub BindDDlQ_Order(ByRef ddl As DropDownList, ByRef Q_GP_ID As Integer, ByVal AssessmentType As AssessmentType, Optional ByVal SelectedValue As Integer = -1, Optional ByVal AddNewItem As Integer = 0)
        Dim SQL As String = "SELECT Q_ID,Q_GP_ID,Q_Name,Q_Order,Active_Status" & vbLf

        Select Case AssessmentType
            Case AVLBL.AssessmentType.Assessment
                SQL &= " FROM tb_Ass_Q" & vbLf
            Case AVLBL.AssessmentType.ReAssessment
                SQL &= " FROM tb_Re_Q" & vbLf
        End Select
        SQL &= " WHERE Q_GP_ID=" & Q_GP_ID & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        'ddl.Items.Add(New ListItem("เลือก", -1))
        If SelectedValue > 0 Then
            For i As Integer = 1 To DT.Rows.Count
                Dim Item As New ListItem(i, i)
                ddl.Items.Add(Item)
            Next
        Else
            For i As Integer = 1 To DT.Rows.Count + 1
                Dim Item As New ListItem(i, i)
                ddl.Items.Add(Item)
            Next
        End If

        ddl.SelectedIndex = 0
        If SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value = SelectedValue Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        Else
            ddl.SelectedIndex = ddl.Items.Count - 1
        End If
    End Sub




    ''' <summary>
    ''' ดึงคำนำหน้าใส่ Dropdownlist
    ''' </summary>
    ''' <param name="ddl">ใส่ข้อมูลคำนำหน้าใน Dropdownlist ที่ต้องการ</param>
    ''' <param name="SelectedValue">แสดง Default ที่ระบุตาม Parameter นี้</param>
    Public Sub BindDDlPrefix(ByRef ddl As DropDownList, Optional ByVal SelectedValue As Integer = 0)

        Dim SQL As String = " " & vbLf
        SQL &= " SELECT Prefix_ID,Prefix_Name,Prefix_ABBR" & vbLf
        SQL &= " FROM tb_Prefix" & vbLf
        SQL &= " ORDER BY Prefix_Name "
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("เลือกคำนำหน้าชื่อ", "0"))
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("Prefix_Name").ToString & " : " & DT.Rows(i).Item("Prefix_ABBR").ToString, DT.Rows(i).Item("Prefix_ID"))
            ddl.Items.Add(Item)
        Next

        For i As Integer = 0 To ddl.Items.Count - 1
            If ddl.Items(i).Value = SelectedValue Then
                ddl.SelectedIndex = i
                Exit For
            End If
        Next

    End Sub



#End Region

    ''' <summary>
    ''' ดึงข้อมูลหมวดคำถาม
    ''' </summary>
    ''' <param name="CG_ID">รหัสกลุ่มสินค้า/บริการ</param>
    Public Function GetCatGroup_Info(ByVal CG_ID As Integer) As CatGroup_Info
        Dim Result As New CatGroup_Info
        Dim SQL As String = ""
        SQL &= "  SELECT CG_ID,CG_Name,Active_ALL FROM tb_Cat_Group" & vbLf
        SQL &= "  WHERE CG_ID='" & CG_ID & "'" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count = 0 Then Return Result
        With Result
            .CG_ID = DT.Rows(0).Item("CG_ID")
            .CG_Name = DT.Rows(0).Item("CG_Name").ToString
            .Active_ALL = DT.Rows(0).Item("Active_ALL")
        End With

        Return Result

    End Function


    ''' <summary>
    ''' ดึงข้อมูลผู้ขายทั้งหมด
    ''' </summary>
    Public Function GetSupplierList(ByVal WithOutBlackList As Boolean) As DataTable
        Dim SQL As String = ""
        SQL &= "  SELECT Supplier.* " & vbLf
        SQL &= "  FROM tb_Sup Supplier" & vbLf
        If WithOutBlackList Then
            SQL &= " LEFT JOIN tb_BlackList BL  ON Supplier.S_ID=BL.S_ID AND BL.B_EndDate > GETDATE() " & vbLf
        End If
        SQL &= "  WHERE Supplier.S_Tax_No IS NOT NULL " & vbLf
        If WithOutBlackList Then
            SQL &= " AND BL.S_ID IS NULL " & vbLf
        End If

        SQL &= "  ORDER BY Active_Status desc,Supplier.S_Name" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    ''' <summary>
    ''' ดึงข้อมูลผู้ขายรายคน
    ''' </summary>
    Public Function GetSupplierDetail(ByVal S_ID As Integer) As DataTable
        Dim SQL As String = ""
        SQL &= "  SELECT Supplier.* " & vbLf
        SQL &= "  FROM tb_Sup Supplier" & vbLf
        SQL &= "  WHERE Supplier.S_Tax_No IS NOT NULL " & vbLf
        SQL &= "  AND Supplier.S_ID=" & S_ID & vbLf
        'If txt_Search_TaxNo.Text <> "" Then
        '    SQL &= " AND (Supplier.S_Tax_No LIKE '%" & txt_Search_TaxNo.Text.Replace("'", "''") & "%' ) " & vbLf
        'End If
        'If txt_Search_Name.Text <> "" Then
        '    SQL &= " AND (Supplier.S_Name LIKE '%" & txt_Search_Name.Text.Replace("'", "''") & "%' OR " & vbLf
        '    SQL &= " Supplier.S_Alias LIKE '%" & txt_Search_Name.Text.Replace("'", "''") & "%' OR " & vbLf
        '    SQL &= " Supplier.Contact_Name LIKE '%" & txt_Search_Name.Text.Replace("'", "''") & "%' )" & vbLf
        'End If
        'If ddlStatus.Text <> "All" Then
        '    SQL &= " AND (Supplier.Active_Status=" & ddlStatus.Items(ddlStatus.SelectedIndex).Value & " ) " & vbLf
        'End If
        SQL &= "  ORDER BY Active_Status desc,Supplier.S_Name" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    ''' <summary>
    ''' ดึงข้อมูลประเภทสินค้าพัสดุครุภัณฑ์ในแต่ละฝ่าย
    ''' </summary>
    ''' <param name="DEPT_ID">ID ฝ่าย</param>
    Public Function GetProductTypeByDept(ByVal DEPT_ID As Integer) As DataTable
        Dim SQL As String = " SELECT DEPT.Dept_ID,DEPT.Dept_Name," & vbLf
        SQL &= " CAT.Cat_ID,CAT.Cat_No,CAT.Cat_Name," & vbLf
        SQL &= " SUB_CAT.Sub_Cat_ID,SUB_CAT.Sub_Cat_No,SUB_CAT.Sub_Cat_Name," & vbLf
        SQL &= " _TYPE.Type_ID,_TYPE.Type_No,_TYPE.Type_Name" & vbLf
        SQL &= " ,CAT.Active_Status CAT_Status,SUB_CAT.Active_Status SUB_CAT_Status,_TYPE.Active_Status Type_Status" & vbLf

        SQL &= " FROM tb_Dept DEPT" & vbLf
        SQL &= " INNER JOIN tb_Cat CAT ON DEPT.Dept_ID=CAT.Dept_ID" & vbLf
        SQL &= " INNER JOIN tb_Sub_Cat SUB_CAT ON CAT.Cat_ID=SUB_CAT.Cat_ID " & vbLf
        SQL &= " INNER JOIN tb_Type _TYPE ON SUB_CAT.Sub_Cat_ID=_TYPE.Sub_Cat_ID" & vbLf
        SQL &= " WHERE DEPT_ID=" & DEPT_ID
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    ''' <summary>
    ''' ดึงข้อมูลประเภทสินค้าพัสดุครุภัณฑ์ที่อยู่ในใบประเมิน
    ''' </summary>
    ''' <param name="Ass_ID">ID ใบประเมิน</param>
    Public Function GetProductTypeByAssessment(ByVal Ass_ID As Integer) As DataTable

        Dim SQL As String = " "
        SQL &= " SELECT DISTINCT DEPT.Dept_ID,DEPT.Dept_Name," & vbLf
        SQL &= " C.Cat_ID,C.Cat_No,C.Cat_Name, ISNULL(C.CG_ID,1) CG_ID," & vbLf
        SQL &= " S.Sub_Cat_ID, S.Sub_Cat_No, S.Sub_Cat_Name," & vbLf
        SQL &= " T.Type_ID, T.Type_No, T.Type_Name, Item_Name" & vbLf
        SQL &= " ,R.Running_ID, R.Running_No, R.Running_Name" & vbLf
        SQL &= " ,C.Active_Status CAT_Status,S.Active_Status SUB_CAT_Status,T.Active_Status Type_Status ,R.Active_Status Running_Status " & vbLf
        SQL &= " FROM tb_Running R" & vbLf
        SQL &= " INNER JOIN tb_Type T ON  R.Type_ID = T.Type_ID AND T.Active_Status=1 " & vbLf
        SQL &= " INNER JOIN tb_Sub_Cat S ON T.Sub_Cat_ID=S.Sub_Cat_ID  AND S.Active_Status=1 " & vbLf
        SQL &= " INNER JOIN tb_Cat C ON C.Cat_ID = S.Cat_ID   AND C.Active_Status=1 " & vbLf
        SQL &= " INNER JOIN  tb_Dept DEPT ON  DEPT.Dept_ID=C.Dept_ID   AND DEPT.Active_Status=1 " & vbLf
        SQL &= " INNER JOIN tb_Ass_Header Ass ON R.Running_ID=Ass.Item_Type" & vbLf
        SQL &= " WHERE Ass.Ass_ID=" & Ass_ID
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    ''' <summary>
    ''' ดึงข้อมูลไฟล์แนบในใบประเมิน
    ''' </summary>
    ''' <param name="Ass_ID">ID ใบประเมิน</param>
    Public Function GetFileByAssessment(ByVal Ass_ID As Integer) As DataTable
        Dim SQL As String = "SELECT File_ID,File_Name,Content_Type,File_Size,isDefault" & vbLf
        SQL &= " FROM tb_Ass_Header INNER JOIN tb_Ass_File ON tb_Ass_Header.Ass_ID=tb_Ass_File.Ass_ID" & vbLf
        SQL &= " WHERE tb_Ass_Header.Ass_ID=" & Ass_ID & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    ''' <summary>
    ''' ดึงข้อมูลเลขพัสดุครุภัณฑ์ที่อยู่ในใบประเมิน
    ''' </summary>
    ''' <param name="Ass_ID">ID ใบประเมิน</param>
    Public Function GetItemNoByAssessment(ByVal Ass_ID As Integer) As DataTable
        Dim SQL As String = "SELECT Item_ID,Item_Type,tb_Item.Item_No,Stock_No " & vbLf
        SQL &= " FROM tb_Item " & vbLf
        SQL &= " INNER JOIN tb_Ass_Header ON tb_Item.Ass_ID=tb_Ass_Header.Ass_ID" & vbLf
        SQL &= " WHERE tb_Item.Ass_ID = " & Ass_ID & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function


    ''' <summary>
    ''' แสดงรายการ File ตาม ID ที่ระบุ
    ''' </summary>
    ''' <param name="Ass_ID">ID ใบประเมิน รวมทั้ง Binary ด้วย</param>
    ''' <param name="File_ID">ID ของ File </param>
    ''' <param name="ServerMapPath">Physical Root Path</param>
    Public Function GetFileByID(ByVal Ass_ID As Integer, ByVal File_ID As Integer, ByVal ServerMapPath As String) As DataTable
        Dim SQL As String = "SELECT Ass_ID,File_ID,File_Name,Content_Type,File_Size,IsDefault" & vbLf
        SQL &= " FROM tb_Ass_File WHERE Ass_ID=" & Ass_ID & " AND File_ID=" & File_ID
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        DT.Columns.Add("Content", GetType(Byte()))

        If DT.Rows.Count > 0 Then
            Dim Path As String = ServerMapPath & "/" & "FileUpload/Ass/" & Ass_ID & "/" & File_ID
            If File.Exists(Path) Then
                Dim F As FileStream = File.Open(Path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                Dim C As New Converter
                DT.Rows(0).Item("Content") = C.StreamToByte(F)
                F.Close()
            End If
        End If
        Return DT
    End Function

    ''' <summary>
    ''' ดึงข้อมูลของไฟล์รวมถึง Binary ของ File ส่งไปในรูปแบบ FileStructure
    ''' </summary>
    ''' <param name="Ass_ID">ID ใบประเมิน รวมทั้ง Binary ด้วย</param>
    ''' <param name="File_ID">ID ของ File </param>
    ''' <param name="ServerMapPath">Physical Root Path</param>
    Public Function GetFileStructure(ByVal Ass_ID As Integer, ByVal File_ID As Integer, ByVal ServerMapPath As String) As FileStructure
        Dim DT As DataTable = GetFileByID(Ass_ID, File_ID, ServerMapPath)
        If DT.Rows.Count = 0 Then Return Nothing
        Dim Result As New FileStructure
        With Result
            .FileName = DT.Rows(0).Item("File_Name").ToString
            .ContentType = DT.Rows(0).Item("Content_Type").ToString
            .FileContent = DT.Rows(0).Item("Content")
        End With
        Return Result
    End Function


    ''' <summary>
    ''' ดึงข้อมูลหมวดคำถาม
    ''' </summary>
    ''' <param name="Q_GP_ID">รหัสหมวดคำถาม</param>
    Public Function GetQ_GP_Info(ByVal Q_GP_ID As Integer, ByVal AssessmentType As AssessmentType) As Q_GP_Info
        Dim Result As New Q_GP_Info
        Dim SQL As String = ""
        SQL &= "  SELECT * " & vbLf
        Select Case AssessmentType
            Case AVLBL.AssessmentType.Assessment
                SQL &= "  FROM tb_Ass_Q_Group" & vbLf
            Case AVLBL.AssessmentType.ReAssessment
                SQL &= "  FROM tb_Re_Q_Group" & vbLf
        End Select
        SQL &= "  WHERE Q_GP_ID=" & Q_GP_ID & "" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count = 0 Then Return Result
        With Result
            .Q_GP_ID = DT.Rows(0).Item("Q_GP_ID")
            .Q_GP_Name = DT.Rows(0).Item("Q_GP_Name").ToString
            .Q_GP_Order = DT.Rows(0).Item("Q_GP_Order")
            .Active_Status = DT.Rows(0).Item("Active_Status").ToString
        End With

        Return Result

    End Function

    ''' <summary>
    ''' ดึงชุดคำถามย่อย
    ''' </summary>
    ''' <param name="Q_GP_ID">รหัสกลุ่มคำถาม</param>
    ''' <param name="AssessmentType">ประเภทชุดคำถาม</param>
    Public Function GetSubQuestionsByCurrentSetting(ByVal Q_GP_ID As Integer, ByVal AssessmentType As AssessmentType) As DataTable
        Dim SQL As String = "SELECT Q_ID,Q_GP_ID,Q_Name,Q_Order,Active_Status" & vbLf
        Select Case AssessmentType
            Case AVLBL.AssessmentType.Assessment
                SQL &= " FROM tb_Ass_Q" & vbLf
            Case AVLBL.AssessmentType.ReAssessment
                SQL &= " FROM tb_Re_Q" & vbLf
        End Select
        SQL &= " WHERE Active_Status=1 AND Q_GP_ID=" & Q_GP_ID & vbLf
        SQL &= " ORDER BY Q_Order" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        Return DT
    End Function

    ''' <summary>
    ''' ดึงชุดคำถามที่มีอยู่ในปัจจุบัน
    ''' </summary>
    ''' <param name="AssessmentType">ประเภทชุดคำถาม</param>
    Public Function GetQuestionsByCurrentSetting(ByVal AssessmentType As AssessmentType) As DataTable
        Dim SQL As String = "SELECT G.Q_GP_ID,Q_GP_Order,Q_GP_Name,Q_ID,Q_Order,Q_Name,Max_Score,Pass_Score,AR_ID,0 Get_Score" & vbLf
        Select Case AssessmentType
            Case AVLBL.AssessmentType.Assessment
                SQL &= " FROM tb_Ass_Q_Group G" & vbLf
                SQL &= " INNER JOIN tb_Ass_Q Q ON G.Q_GP_ID=Q.Q_GP_ID" & vbLf
            Case AVLBL.AssessmentType.ReAssessment
                SQL &= " FROM tb_Re_Q_Group G" & vbLf
                SQL &= " INNER JOIN tb_Re_Q Q ON G.Q_GP_ID=Q.Q_GP_ID" & vbLf
        End Select
        SQL &= " WHERE G.Active_Status = 1 And Q.Active_Status = 1" & vbLf
        SQL &= " ORDER BY G.Q_GP_Order,Q.Q_Order" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    ''' <summary>
    ''' ดึงชุดคำถามที่มีอยู่ในแบบประเมิน
    ''' </summary>
    ''' <param name="Ass_ID">รหัสแบบประเมิน</param>
    Public Function GetQuestionsByAssessment(ByVal Ass_ID As Integer) As DataTable
        Dim SQL As String = "SELECT Q_GP_ID,Q_GP_Name,Q_GP_Order,Q_ID,Q_Name,Q_Order,tb_Ass_Detail.Max_Score,tb_Ass_Detail.Pass_Score,tb_Ass_Detail.AR_ID,tb_Ass_Detail.Get_Score,tb_Ass_Detail.ckConfirm" & vbLf
        SQL &= " FROM tb_Ass_Detail" & vbLf
        SQL &= " INNER JOIN tb_Ass_Header ON tb_Ass_Detail.Ass_ID=tb_Ass_Header.Ass_ID" & vbLf
        SQL &= " WHERE tb_Ass_Header.Ass_ID =" & Ass_ID & vbLf
        SQL &= " ORDER BY Q_GP_Order,Q_Order" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        Return DT
    End Function

    ''' <summary>
    ''' ดึงชุดคำถามที่มีอยู่ในแบบประเมินทบทวน
    ''' </summary>
    ''' <param name="ReAss_ID">รหัสแบบประเมินทบทวน</param>
    Public Function GetQuestionsByReassessment(ByVal ReAss_ID As Integer) As DataTable
        Dim SQL As String = "SELECT Q_GP_ID,Q_GP_Name,Q_GP_Order,Q_ID,Q_Name,Q_Order,tb_ReAss_Detail.Max_Score,tb_ReAss_Detail.Pass_Score,tb_ReAss_Detail.AR_ID,tb_ReAss_Detail.Get_Score,tb_ReAss_Detail.ckConfirm" & vbLf
        SQL &= " FROM tb_ReAss_Detail" & vbLf
        SQL &= " INNER JOIN tb_ReAss_Header ON tb_ReAss_Detail.ReAss_ID=tb_ReAss_Header.ReAss_ID" & vbLf
        SQL &= " WHERE tb_ReAss_Header.ReAss_ID =" & ReAss_ID & vbLf
        SQL &= " ORDER BY Q_GP_Order,Q_Order" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        Return DT
    End Function

    ''' <summary>
    ''' ดึงชุดคำถามที่มีอยู่ในปัจจุบัน
    ''' </summary>
    ''' <param name="AssessmentType">ประเภทชุดคำถาม</param>
    ''' <param name="Ass_ID">รหัสแบบประเมิน</param>
    Public Function GetAssessmentQuestions(ByVal AssessmentType As AssessmentType, ByVal Ass_ID As Integer, ByVal Role As AssessmentRole) As DataTable
        Dim SQL As String = "SELECT Q_GP_ID,Q_ID,AR_ID,AR_Name,Q_GP_Order,Q_Order,Q_GP_Name,Q_Name,Max_Score,Pass_Score,Get_Score,Q_Comment" & vbLf
        Select Case AssessmentType
            Case AVLBL.AssessmentType.Assessment
                SQL &= " FROM tb_Ass_Detail" & vbLf
            Case AVLBL.AssessmentType.ReAssessment
                SQL &= " FROM tb_ReAss_Detail" & vbLf
        End Select
        SQL &= " WHERE Ass_ID=" & Ass_ID & vbLf
        SQL &= " AND AR_ID=" & CInt(Role) & vbLf
        SQL &= " ORDER BY Q_GP_Order,Q_Order" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    ''' <summary>
    ''' ดึงเกณฑ์คะแนนสรุปการประเมินทบทวน
    ''' </summary>
    Public Function GetReQuestionsCriterion() As DataTable
        Dim SQL As String = "SELECT ISNULL(Excellent,0) Excellent,ISNULL(Good,0) Good,ISNULL(Middle,0) Middle FROM tb_Re_Q_Criterion" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    ''' <summary>
    ''' Update ผลรวมคะแนนในตารางประเมินผู้ขายใหม่
    ''' </summary>
    ''' <param name="Ass_ID">ID ใบประเมินผู้ขายใหม่</param>
    Public Function UpdateHeaderScore_Assessment(ByVal Ass_ID As Integer) As DataTable
        Dim SQL As String = "EXEC dbo.sp_UpdateHeaderScore_Assessment " & Ass_ID & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    ''' <summary>
    ''' Update ผลรวมคะแนนในตารางประเมินทบทวน
    ''' </summary>
    ''' <param name="ReAss_ID">ID ใบประเมินทบทวน</param>
    Public Function UpdateHeaderScore_ReAssessment(ByVal ReAss_ID As Integer) As DataTable
        Dim SQL As String = "EXEC dbo.sp_UpdateHeaderScore_ReAssessment " & ReAss_ID & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    ''' <summary>
    ''' หา Key ใหม่ ของตาราง และคอลัมน์ ที่กำหนด
    ''' </summary>
    ''' <param name="TableName">ชื่อตารางที่ต้องการหา Key</param>
    ''' <param name="ColumnName">ชื่อคอลัมน์ที่ต้องการหา Key</param>
    Public Function GetNewPrimaryID(ByVal TableName As String, ByVal ColumnName As String) As String
        Dim id As String = ""
        Dim SQL As String = ""
        SQL = "SELECT IsNull(MAX(" & ColumnName & "),0)+1 FROM " & TableName.Replace("'", "''")
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)
    End Function


    ''' <summary>
    ''' ดึงข้อมูลพนักงาน
    ''' </summary>
    ''' <param name="User_ID">ID พนักงานผู้ใช้ระบบ</param>
    Public Function GetUserInfo(ByVal User_ID As Integer) As UserInfo

        Dim Result As New UserInfo
        Dim SQL As String = ""

        SQL &= "  SELECT tb_User.User_ID,tb_User.User_Login,tb_User.User_Password,tb_Dept.Dept_ID,Dept_Name" & vbLf
        SQL &= "  ,tb_User.Prefix_ID" & vbLf
        SQL &= "  ,CASE WHEN tb_Prefix.Prefix_ABBR IS NOT NULL THEN tb_Prefix.Prefix_ABBR ELSE tb_Prefix.Prefix_Name END Prefix_Name" & vbLf
        SQL &= "  ,Fisrt_Name,Last_Name,Pos_Name,tb_User.Cancel_Role,tb_User.Active_Status" & vbLf
        SQL &= "  FROM tb_User" & vbLf
        SQL &= "  INNER JOIN tb_Dept ON tb_Dept.Dept_ID LIKE tb_User.Dept_ID+'%'" & vbLf
        SQL &= "  LEFT JOIN tb_Prefix ON tb_User.Prefix_ID = tb_Prefix.Prefix_ID " & vbLf
        SQL &= "  WHERE  User_ID='" & User_ID & "'" & vbLf
        SQL &= "  ORDER BY tb_Dept.Dept_ID" & vbLf

        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count = 0 Then Return Result

        With Result
            .User_ID = DT.Rows(0).Item("User_ID")
            .User_Login = DT.Rows(0).Item("User_Login").ToString()
            .User_Password = DT.Rows(0).Item("User_Password").ToString
            .Dept_ID = DT.Rows(0).Item("Dept_ID").ToString
            .Dept_Name = DT.Rows(0).Item("Dept_Name").ToString
            .Prefix_ID = DT.Rows(0).Item("Prefix_ID").ToString
            .Prefix_Name = DT.Rows(0).Item("Prefix_Name").ToString
            .Fisrt_Name = DT.Rows(0).Item("Fisrt_Name").ToString
            .Last_Name = DT.Rows(0).Item("Last_Name").ToString
            .Pos_Name = DT.Rows(0).Item("Pos_Name").ToString
            .Cancel_Role = DT.Rows(0).Item("Cancel_Role")
            .Active_Status = DT.Rows(0).Item("Active_Status")
        End With

        Return Result

    End Function

    ''' <summary>
    ''' ดึงข้อมูลพนักงาน
    ''' </summary>
    ''' <param name="User_Login">ชื่อ Login ผู้ใช้ระบบ</param>
    Public Function GetUserInfo(ByVal User_Login As String, ByVal Password As String) As UserInfo

        Dim Result As New UserInfo
        Dim SQL As String = ""

        SQL &= "  SELECT tb_User.User_ID FROM tb_User" & vbLf
        SQL &= "  WHERE  User_Login='" & User_Login.Replace("'", "''") & "'  AND User_Password= '" & Password.Replace("'", "''") & "'" & vbLf

        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count = 0 Then Return Result

        Return GetUserInfo(CInt(DT.Rows(0).Item("User_ID")))
    End Function

    ''' <summary>
    ''' ดึงข้อมูลสิทธิ์การประเมินของพนักงาน
    ''' </summary>
    ''' <param name="User_ID">รหัสพนักงานผู้ใช้ระบบ</param>
    Public Function GetDeptRoleUserInfo(ByVal User_ID As Integer) As String

        Dim Result As New UserInfo
        Dim SQL_D As String = " SELECT Count(Dept_ID) Count_Dept FROM tb_Dept WHERE Active_Status=1"
        Dim DA_D As New SqlDataAdapter(SQL_D, ConnectionString)
        Dim DT_D As New DataTable
        DA_D.Fill(DT_D)
        Dim Count_Dept As Integer = 0
        If DT_D.Rows.Count > 0 Then
            Count_Dept = DT_D.Rows(0).Item("Count_Dept")
        End If

        Dim SQL As String = ""
        SQL &= "  SELECT DISTINCT tb_User.User_ID ,tb_Dept.Dept_ID ,tb_Dept.Dept_Name " & vbLf
        'SQL &= "  ,CASE WHEN  tb_Dept_Role.AR_ID=4 THEN tb_Dept_Role.AR_ID ELSE 0 END AR_ID" & vbLf
        SQL &= "  FROM tb_User " & vbLf
        SQL &= "  LEFT JOIN tb_Dept_Role ON tb_User.User_ID =tb_Dept_Role.User_ID " & vbLf
        SQL &= "  LEFT JOIN tb_Dept ON tb_Dept.Dept_ID =tb_Dept_Role.Dept_ID " & vbLf
        SQL &= "  WHERE  tb_User.User_ID='" & User_ID & "'  AND tb_Dept.Active_Status=1" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim Role As String = ""
        'If DT.Rows.Count > 0 And Not IsDBNull(DT.Rows(0).Item("Dept_ID")) Then
        '    If (DT.Rows(0).Item("AR_ID")) = 4 Or (DT.Rows.Count = Count_Dept) Then
        '        Role = "มีสิทธิ์ในการประเมินทุกฝ่าย  "
        '    Else
        '        For i As Integer = 0 To DT.Rows.Count - 1
        '            Role &= " " & DT.Rows(i).Item("Dept_Name").ToString & " ,"
        '            If i > 1 Then
        '                Role = Role.Substring(0, Role.Length - 1)
        '                Role &= " และอีก " & (DT.Rows.Count) - 3 & " สิทธิ์ "
        '                Exit For
        '            End If
        '        Next
        '    End If

        'ElseIf IsDBNull(DT.Rows(0).Item("Dept_ID")) And (DT.Rows(0).Item("AR_ID")) = 4 Then
        '    Role = "มีสิทธิ์ในการประเมินทุกฝ่าย "
        'Else
        '    Role = "ไม่มีสิทธิ์ในการประเมิน  "
        'End If

        If DT.Rows.Count > 0 Then
            For i As Integer = 0 To DT.Rows.Count - 1
                If i < 3 Then
                    Role &= " " & DT.Rows(i).Item("Dept_Name").ToString & " ,"
                Else
                    Role = Role.Substring(0, Role.Length - 1)
                    Role &= " และอีก " & (DT.Rows.Count) - 3 & " สิทธิ์  "
                    Exit For
                End If
            Next
        Else
            Role = "ไม่มีสิทธิ์ในการประเมิน  "
        End If

        Role = Role.Substring(0, Role.Length - 1)
        Return Role

    End Function

    'Public Function GetAssHeaderInfo(ByVal Ref_Year As String, ByVal Ref_Month As String, ByVal Dept_ID As String, ByVal Ref_Number As String) As Ass_Header_Info

    '    Dim Result As New Ass_Header_Info

    '    Dim SQL As String = ""
    '    SQL &= "  SELECT * FROM vw_Ass_Header " & vbLf
    '    SQL &= "  WHERE Ref_Year='" & Ref_Year & "'" & vbLf
    '    SQL &= "  AND Ref_Month='" & Ref_Month & "'" & vbLf
    '    SQL &= "  AND Dept_ID='" & Dept_ID & "'" & vbLf
    '    SQL &= "  AND Ref_Number='" & Ref_Number & "'" & vbLf
    '    Dim DA As New SqlDataAdapter(SQL, ConnectionString)
    '    Dim DT As New DataTable
    '    DA.Fill(DT)

    '    If DT.Rows.Count = 0 Then Return Result

    '    With Result

    '        If Not IsDBNull(DT.Rows(0).Item("Ass_ID")) Then
    '            .Ass_ID = DT.Rows(0).Item("Ass_ID")
    '        End If
    '        .Ref_Year = DT.Rows(0).Item("Ref_Year").ToString
    '        .Ref_Month = DT.Rows(0).Item("Ref_Month").ToString
    '        .Dept_ID = DT.Rows(0).Item("Dept_ID").ToString
    '        .Ref_Number = DT.Rows(0).Item("Ref_Number").ToString
    '        .Dept_Name = DT.Rows(0).Item("Dept_Name").ToString
    '        If Not IsDBNull(DT.Rows(0).Item("S_ID")) Then
    '            .S_ID = DT.Rows(0).Item("S_ID")
    '        End If
    '        .S_Tax_No = DT.Rows(0).Item("S_Tax_No").ToString
    '        .S_Name = DT.Rows(0).Item("S_Name").ToString
    '        .S_Alias = DT.Rows(0).Item("S_Alias").ToString
    '        .S_Address = DT.Rows(0).Item("S_Address").ToString
    '        .S_Phone = DT.Rows(0).Item("S_Phone").ToString
    '        .S_Fax = DT.Rows(0).Item("S_Fax").ToString
    '        .S_Contact_Name = DT.Rows(0).Item("S_Contact_Name").ToString
    '        .S_Email = DT.Rows(0).Item("S_Email").ToString
    '        If Not IsDBNull(DT.Rows(0).Item("S_Type_ID")) Then
    '            .S_Type_ID = DT.Rows(0).Item("S_Type_ID")
    '        End If
    '        .S_Type_Name = DT.Rows(0).Item("S_Type_Name").ToString

    '        .FSN = DT.Rows(0).Item("FSN").ToString
    '        .Cat = DT.Rows(0).Item("Cat").ToString
    '        .Sub_Cat = DT.Rows(0).Item("Sub_Cat").ToString
    '        .Type = DT.Rows(0).Item("Type").ToString

    '        If Not IsDBNull(DT.Rows(0).Item("Item_ID")) Then
    '            .Item_ID = DT.Rows(0).Item("Item_ID")
    '        End If
    '        .Item_Name = DT.Rows(0).Item("Item_Name").ToString
    '        .Unit = DT.Rows(0).Item("Unit").ToString
    '        .Lot = DT.Rows(0).Item("Lot").ToString
    '        .DateRecieved = DT.Rows(0).Item("DateRecieved").ToString
    '        If Not IsDBNull(DT.Rows(0).Item("Price")) Then
    '            .Price = FormatNumber(DT.Rows(0).Item("Price"), 2)
    '        End If
    '        If Not IsDBNull(DT.Rows(0).Item("Role_1_ID")) Then
    '            .Role_1_ID = DT.Rows(0).Item("Role_1_ID")
    '        End If
    '        '.Role_1_Title = DT.Rows(0).Item("Role_1_Title").ToString
    '        '.Role_1_Fisrt_Name = DT.Rows(0).Item("Role_1_Fisrt_Name").ToString
    '        '.Role_1_Last_Name = DT.Rows(0).Item("Role_1_Last_Name").ToString
    '        .Role_1_Fullname = DT.Rows(0).Item("Role_1_Title").ToString & DT.Rows(0).Item("Role_1_Fisrt_Name").ToString & "   " & DT.Rows(0).Item("Role_1_Last_Name").ToString
    '        .Role_1_Pos_Name = DT.Rows(0).Item("Role_1_Pos_Name").ToString
    '        .Role_1_Start = DT.Rows(0).Item("Role_1_Start").ToString
    '        .Role_1_End = DT.Rows(0).Item("Role_1_End").ToString
    '        .Role_1_Comment = DT.Rows(0).Item("Role_1_Comment").ToString
    '        If Not IsDBNull(DT.Rows(0).Item("Role_2_ID")) Then
    '            .Role_2_ID = DT.Rows(0).Item("Role_2_ID")
    '        End If
    '        '.Role_2_Title = DT.Rows(0).Item("Role_2_Title").ToString
    '        '.Role_2_Fisrt_Name = DT.Rows(0).Item("Role_2_Fisrt_Name").ToString
    '        '.Role_2_Last_Name = DT.Rows(0).Item("Role_2_Last_Name").ToString
    '        .Role_2_Fullname = DT.Rows(0).Item("Role_2_Title").ToString & DT.Rows(0).Item("Role_2_Fisrt_Name").ToString & "   " & DT.Rows(0).Item("Role_2_Last_Name").ToString
    '        .Role_2_Pos_Name = DT.Rows(0).Item("Role_2_Pos_Name").ToString
    '        .Role_2_Start = DT.Rows(0).Item("Role_2_Start").ToString
    '        .Role_2_End = DT.Rows(0).Item("Role_2_End").ToString
    '        .Role_2_Comment = DT.Rows(0).Item("Role_2_Comment").ToString
    '        If Not IsDBNull(DT.Rows(0).Item("Role_3_ID")) Then
    '            .Role_3_ID = DT.Rows(0).Item("Role_3_ID")
    '        End If
    '        '.Role_3_Title = DT.Rows(0).Item("Role_3_Title").ToString
    '        '.Role_3_Fisrt_Name = DT.Rows(0).Item("Role_3_Fisrt_Name").ToString
    '        '.Role_3_Last_Name = DT.Rows(0).Item("Role_3_Last_Name").ToString
    '        .Role_3_Fullname = DT.Rows(0).Item("Role_3_Title").ToString & DT.Rows(0).Item("Role_3_Fisrt_Name").ToString & "   " & DT.Rows(0).Item("Role_3_Last_Name").ToString
    '        .Role_3_Pos_Name = DT.Rows(0).Item("Role_3_Pos_Name").ToString
    '        .Role_3_Start = DT.Rows(0).Item("Role_3_Start").ToString
    '        .Role_3_End = DT.Rows(0).Item("Role_3_End").ToString
    '        .Role_3_Comment = DT.Rows(0).Item("Role_3_Comment").ToString
    '        If Not IsDBNull(DT.Rows(0).Item("Role_4_ID")) Then
    '            .Role_4_ID = DT.Rows(0).Item("Role_4_ID")
    '        End If
    '        '.Role_4_Title = DT.Rows(0).Item("Role_4_Title").ToString
    '        '.Role_4_Fisrt_Name = DT.Rows(0).Item("Role_4_Fisrt_Name").ToString
    '        '.Role_4_Last_Name = DT.Rows(0).Item("Role_4_Last_Name").ToString
    '        .Role_4_Fullname = DT.Rows(0).Item("Role_4_Title").ToString & DT.Rows(0).Item("Role_4_Fisrt_Name").ToString & "   " & DT.Rows(0).Item("Role_4_Last_Name").ToString
    '        .Role_4_Pos_Name = DT.Rows(0).Item("Role_4_Pos_Name").ToString
    '        .Role_4_Start = DT.Rows(0).Item("Role_4_Start").ToString
    '        .Role_4_End = DT.Rows(0).Item("Role_4_End").ToString
    '        .Role_4_Comment = DT.Rows(0).Item("Role_4_Comment").ToString
    '        If Not IsDBNull(DT.Rows(0).Item("Current_Step")) Then
    '            .Current_Step = DT.Rows(0).Item("Current_Step")
    '        End If
    '        If Not IsDBNull(DT.Rows(0).Item("Pass_Score")) Then
    '            .Pass_Score = FormatNumber(DT.Rows(0).Item("Pass_Score"), 0)
    '        End If

    '        .AVL_Y = DT.Rows(0).Item("AVL_Y").ToString
    '        .AVL_M = DT.Rows(0).Item("AVL_M").ToString
    '        .AVL_No = DT.Rows(0).Item("AVL_No").ToString
    '        .AVL = "AVL" & DT.Rows(0).Item("AVL_Y").ToString & DT.Rows(0).Item("AVL_M").ToString & DT.Rows(0).Item("AVL_No").ToString
    '        If Not IsDBNull(DT.Rows(0).Item("")) Then
    '            .AVL_Status = DT.Rows(0).Item("AVL_Status")
    '        End If

    '    End With

    '    Return Result

    'End Function

    ''' <summary>
    ''' เจ้าหน้าที่มีสิทธิ์ประเมินหรือไม่
    ''' </summary>
    ''' <param name="Role">รหัสพนักงานผู้ใช้ระบบ</param>
    Public Function CanCreateAssessment(ByVal Role As AssessmentRole) As Boolean
        Return Role = AssessmentRole.Creater
    End Function

    ''' <summary>
    ''' เจ้าหน้าที่มีสิทธิ์ประเมินหรือไม่
    ''' </summary>
    ''' <param name="RoleTable">ตารางสิทธิ์ของเจ้าหน้าที่</param>
    Public Function CanCreateAssessment(ByVal RoleTable As DataTable, ByVal RoleColumnName As String) As Boolean
        Dim TMP As DataTable = RoleTable.Copy
        TMP.DefaultView.RowFilter = RoleColumnName & "=" & CInt(AssessmentRole.Creater)
        Return TMP.DefaultView.Count > 0
    End Function
    'Public Function CanSelectSupplierAssessment(ByVal Dept_ID As String, ByVal Sub_Dept_ID As String, ByVal Check_ColName As String, ByVal Check_Value As String, ByVal Value_Select As Integer, ByVal Item_Type As Integer) As Boolean

    ''' <summary>
    ''' สามารถเลือกผู้ขาย สินค้า ฝ่ายของผู้จัดทำ ภายในปีเดียวกันเพื่อประเมินได้หรือไม่
    ''' </summary>
    ''' <param name="Dept_ID">ID สำนักงาน ที่เลือก</param>
    ''' <param name="Sub_Dept_ID">ID ฝ่าย/ภาค/งาน ที่เลือก</param>
    ''' <param name="S_ID" >ID ผู้ขาย ที่เลือก</param>
    ''' <param name="Running_ID">ID สินค้า ที่เลือก</param>    ''' 
    ''' <param name="Check_ColName">ชื่อคอลัมน์ ที่ตรวจสอบยกเว้น</param>
    ''' <param name="Check_Value">ID เดิมของคอลัมน์</param>
    Public Function CanSelect_Sup_Type_SubDept_Assessment(ByVal Current_Ass_ID As String, ByVal Dept_ID As String, ByVal Sub_Dept_ID As String, ByVal S_ID As Integer, ByVal Running_ID As Integer, ByVal Check_ColName As String, ByVal Check_Value As String) As Boolean

        '--------------ค้นหาใบประเมินผู้ขายใหม่ที่ไม่ซ้ำกับ สินค้า ฝ่าย และ สินค้า ที่ประเมินแล้วในปี-------------------

        Dim SQL As String = ""
        SQL &= " Select Top 1 Ass_ID" & vbLf
        SQL &= " ,Ref_Year" & vbLf
        SQL &= " ,Ref_Month" & vbLf
        SQL &= " ,Dept_ID" & vbLf
        SQL &= " ,Sub_Dept_ID" & vbLf
        SQL &= " ,S_ID,Item_Type,Item_Running" & vbLf
        SQL &= " ,Current_Step  ,Get_Score ,Pass_Score ,Pass_Status,Update_Time"
        SQL &= " FROM tb_Ass_Header " & vbLf
        SQL &= " WHERE  "
        'SQL &= " Ref_Year = '" & (Now.Year + 543).ToString.Substring(2) & "'" & vbLf
        'SQL &= " AND"
        SQL &= " Dept_ID = '" & Dept_ID & "'" & vbLf
        SQL &= " AND Sub_Dept_ID = '" & Sub_Dept_ID & "'" & vbLf
        SQL &= " AND S_ID = " & S_ID & vbLf
        SQL &= " AND Item_Type = " & Running_ID & vbLf

        SQL &= " AND " & Check_ColName & " NOT IN ('" & Check_Value & "')"

        SQL &= " ORDER BY Update_Time DESC"
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim result As Boolean = False

        DT.DefaultView.RowFilter = "Current_Step = 5 AND  Pass_Status=0"  '----ยอมให้ประเมินใหม่ได้
        Dim DR As DataRow
        SQL = " SELECT Ass_ID,Fail_Ass_ID FROM  tb_Ass_Header WHERE Ass_ID= " & Current_Ass_ID
        DA = New SqlDataAdapter(SQL, ConnectionString)
        Dim DT_Update = New DataTable
        DA.Fill(DT_Update)
        DR = DT_Update.Rows(0)

        If DT.Rows.Count > 0 Then
            If DT.DefaultView.Count = 1 Then
                result = True   '----สร้างใบใหม่ได้ เพราะใบก่อนหน้าไม่ผ่านประเมิน 
                DR("Fail_Ass_ID") = DT.DefaultView(0).Item("Ass_ID")
            Else
                DR("Fail_Ass_ID") = DBNull.Value
            End If
        Else
            result = True
            DR("Fail_Ass_ID") = DBNull.Value
        End If

        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT_Update)
        Return result

        '-----สร้างได้หรือไม่ True ได้ Falseไม่ได้


    End Function

    ''' <summary>
    ''' หาข้อมูลการประเมินผู้ขายใหม่
    ''' </summary>
    Public Function GetAssessmentHeader(ByVal Ass_ID As Integer) As DataTable
        Dim SQL As String = "SELECT * FROM vw_Ass_Header WHERE Ass_ID=" & Ass_ID
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    ''' <summary>
    ''' หาข้อมูลการประเมินmทบทวน
    ''' </summary>
    Public Function GetReassessmentHeader(ByVal Ass_ID As Integer, Optional ByVal ReAss_ID As Integer = 0) As DataTable
        Dim SQL As String = "SELECT * FROM vw_ReAss_Header WHERE Ass_ID=" & Ass_ID
        If ReAss_ID <> 0 Then
            SQL &= " And ReAss_ID = " & ReAss_ID
        End If
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function


    ''' <summary>
    ''' หาบทบาทการประเมินของ User ในแต่ละสำนักงาน
    ''' </summary>
    ''' <param name="User_ID">ID ของ User</param>
    Public Function GetUserDeptRole(User_ID As Integer) As DataTable
        Dim SQL As String = "SELECT R.User_ID,R.Dept_ID,D.Dept_Name,R.AR_ID,AR_Name_TH" & vbLf
        SQL &= " FROM tb_Dept_Role R" & vbLf
        SQL &= " INNER JOIN tb_User U ON R.User_ID=U.User_ID AND U.Active_Status=1" & vbLf
        SQL &= " INNER JOIN tb_Dept D ON R.Dept_ID=D.Dept_ID AND D.Active_Status=1" & vbLf
        SQL &= " INNER JOIN tb_Ass_Role AR ON R.AR_ID=AR.AR_ID" & vbLf
        SQL &= " WHERE R.User_ID=" & User_ID & vbLf
        SQL &= " ORDER BY R.Dept_ID,R.AR_ID"
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    ''' <summary>
    ''' หาสิทธิ์ User ในสำนักงาน
    ''' </summary>
    ''' <param name="User_ID">ID ของ User</param>
    ''' <param name="Dept_ID">ID ของ ฝ่าย</param>
    Public Function GetUserDeptRole(User_ID As Integer, ByVal Dept_ID As String) As DataTable
        Dim SQL As String = "SELECT R.User_ID,R.Dept_ID,D.Dept_Name,R.Sub_Dept_ID,SD.Sub_Dept_Name ,R.AR_ID,AR_Name_TH" & vbLf
        SQL &= " FROM tb_Dept_Role R" & vbLf
        SQL &= " INNER JOIN tb_User U ON R.User_ID=U.User_ID AND U.Active_Status=1" & vbLf
        SQL &= " INNER JOIN tb_Dept D ON R.Dept_ID=D.Dept_ID AND D.Active_Status=1" & vbLf
        SQL &= " INNER JOIN tb_Sub_Dept SD ON R.Sub_Dept_ID = SD.Sub_Dept_ID  AND SD.Active_Status =1" & vbLf
        SQL &= " INNER JOIN tb_Ass_Role AR ON R.AR_ID=AR.AR_ID" & vbLf
        SQL &= " WHERE R.User_ID=" & User_ID & vbLf
        SQL &= " AND R.Dept_ID='" & Dept_ID.Replace("'", "''") & "'" & vbLf
        SQL &= " ORDER BY R.Dept_ID,R.AR_ID" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    ''' <summary>
    ''' หาสิทธิ์ User ในฝ่ายแต่ละสำนักงาน
    ''' </summary>
    ''' <param name="User_ID">ID ของ User</param>
    ''' <param name="Dept_ID">ID ของ สำนักงาน</param>
    ''' <param name="Sub_Dept_ID">ID ของ ฝ่าย</param>
    Public Function GetUserDeptRole(User_ID As Integer, ByVal Dept_ID As String, ByVal Sub_Dept_ID As String) As DataTable
        Dim SQL As String = " SELECT User_ID ,R.Dept_ID ,R.Sub_Dept_ID ,AR_ID" & vbLf
        SQL &= " FROM tb_Dept_Role R" & vbLf
        SQL &= " WHERE R.User_ID=" & User_ID & vbLf
        SQL &= " AND R.Dept_ID='" & Dept_ID.Replace("'", "''") & "'" & vbLf
        SQL &= " AND R.Sub_Dept_ID='" & Sub_Dept_ID.Replace("'", "''") & "'" & vbLf
        SQL &= " ORDER BY R.Dept_ID,R.AR_ID" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function


    ''' <summary>
    ''' หาสิทธิ์การใช้เมนูของแต่ละ User
    ''' </summary>
    ''' <param name="User_ID">ID ของ User</param>
    Public Function GetUserMenuRole(User_ID As Integer) As DataTable
        Dim SQL As String = "SELECT MR.User_ID,MR.Menu_ID,M.Menu_Name,M.Relative_Path,MR.MR_ID,MRN.MR_Name" & vbLf
        SQL &= " FROM tb_Menu_Role MR" & vbLf
        SQL &= " INNER JOIN tb_User U ON MR.User_ID=U.User_ID AND U.Active_Status=1" & vbLf
        SQL &= " INNER JOIN tb_Menu M ON MR.Menu_ID=M.Menu_ID" & vbLf
        SQL &= " INNER JOIN tb_Menu_Role_Name MRN ON MR.MR_ID=MRN.MR_ID" & vbLf
        SQL &= " WHERE MR.User_ID =" & User_ID
        SQL &= " ORDER BY MR.Menu_ID"
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    ''' <summary>
    ''' หาสิทธิ์การใช้เมนูของแต่ละ User
    ''' </summary>
    ''' <param name="User_ID">ID ของ User</param>
    Public Function GetUserMenuRole(User_ID As Integer, ByVal Relative_Path As String) As DataTable
        Dim SQL As String = "SELECT MR.User_ID,MR.Menu_ID,M.Menu_Name,M.Relative_Path,MR.MR_ID,MRN.MR_Name" & vbLf
        SQL &= " FROM tb_Menu_Role MR" & vbLf
        SQL &= " INNER JOIN tb_User U ON MR.User_ID=U.User_ID AND U.Active_Status=1" & vbLf
        SQL &= " INNER JOIN tb_Menu M ON MR.Menu_ID=M.Menu_ID" & vbLf
        SQL &= " INNER JOIN tb_Menu_Role_Name MRN ON MR.MR_ID=MRN.MR_ID" & vbLf
        SQL &= " WHERE MR.User_ID =" & User_ID & " AND M.Relative_Path='" & Relative_Path.Replace("'", "''") & "'" & vbLf
        SQL &= " ORDER BY MR.Menu_ID"
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    ''' <summary>
    ''' ดึงชนิดสินค้าที่ Active อยู่ทั้งหมดของฝ่ายที่ระบุ
    ''' </summary>
    ''' <param name="Dept_ID">ID ของ ฝ่าย</param>
    Public Function GetProductTypeByDept(ByVal Dept_ID As String) As DataTable
        Dim SQL As String = "SELECT C.Cat_ID,C.Cat_No,C.Cat_Name,C.CG_ID," & vbLf
        SQL &= " S.Sub_Cat_ID, S.Sub_Cat_No, S.Sub_Cat_Name," & vbLf
        SQL &= " T.Type_ID, T.Type_No, T.Type_Name," & vbLf
        SQL &= " R.Running_ID, R.Running_No, R.Running_Name" & vbLf

        SQL &= " FROM tb_Running R" & vbLf
        SQL &= " INNER JOIN tb_Type T ON  R.Type_ID = T.Type_ID AND T.Active_Status=1 " & vbLf
        SQL &= " INNER JOIN tb_Sub_Cat S ON T.Sub_Cat_ID=S.Sub_Cat_ID  AND S.Active_Status=1 " & vbLf
        SQL &= " INNER JOIN tb_Cat C ON C.Cat_ID = S.Cat_ID   AND C.Active_Status=1 " & vbLf
        'SQL &= " INNER JOIN  tb_Dept DEPT ON  DEPT.Dept_ID=C.Dept_ID   AND DEPT.Active_Status=1 " & vbLf
        'SQL &= " WHERE R.Active_Status=1 AND T.Dept_ID='" & Dept_ID.Replace("'", "''") & "' " & vbLf

        SQL &= " --INNER JOIN  tb_Dept DEPT ON  DEPT.Dept_ID=C.Dept_ID   AND DEPT.Active_Status=1 " & vbLf
        SQL &= " WHERE R.Active_Status=1 --AND T.Dept_ID='" & Dept_ID.Replace("'", "''") & "' " & vbLf

        SQL &= " ORDER BY C.Cat_No,S.Sub_Cat_No,T.Type_No,R.Running_No" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    ''' <summary>
    ''' ดึงชนิดย่อยสินค้าที่ Active ตามที่ระบุ
    ''' </summary>
    ''' <param name="Type_ID">ID ของชนิดย่อย</param>
    ''' <param name="Running_ID">ID ของ Running</param>
    Public Function GetProductTypeByType(ByVal Type_ID As Integer, ByVal Running_ID As Integer) As DataTable
        Dim SQL As String = "SELECT C.Cat_ID,C.Cat_No,C.Cat_Name," & vbLf
        SQL &= " S.Sub_Cat_ID, S.Sub_Cat_No, S.Sub_Cat_Name," & vbLf
        SQL &= " T.Type_ID, T.Type_No, T.Type_Name" & vbLf
        SQL &= " ,R.Running_ID, R.Running_No, R.Running_Name" & vbLf

        SQL &= " FROM tb_Dept DEPT"
        SQL &= " INNER JOIN tb_Cat C ON DEPT.Dept_ID=C.Dept_ID"
        SQL &= " LEFT JOIN tb_Sub_Cat S ON C.Cat_ID=S.Cat_ID"
        SQL &= " LEFT JOIN tb_Type T ON S.Sub_Cat_ID=T.Sub_Cat_ID"
        SQL &= " LEFT JOIN tb_Running R ON  R.Type_ID = T.Type_ID AND R.Active_Status=1"

        SQL &= " WHERE T.Active_Status=1 AND T.Type_ID IS NOT NULL AND T.Type_ID=" & Type_ID & vbLf
        If Running_ID <> 0 Then
            SQL &= " AND R.Running_ID=" & Running_ID & vbLf
        End If
        SQL &= " ORDER BY C.Cat_No,S.Sub_Cat_No,T.Type_No,R.Running_No" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    ''' <summary>
    ''' ดึงชนิดย่อยสินค้าที่ Active ตามที่ระบุ
    ''' </summary>
    ''' <param name="Running_ID">ID ของ Running</param>
    Public Function GetProductTypeByRunningNo(ByVal Running_ID As Integer) As DataTable
        Dim SQL As String = "SELECT C.Cat_ID,C.Cat_No,C.Cat_Name,C.CG_ID," & vbLf
        SQL &= " S.Sub_Cat_ID, S.Sub_Cat_No, S.Sub_Cat_Name," & vbLf
        SQL &= " T.Type_ID, T.Type_No, T.Type_Name" & vbLf
        SQL &= " ,R.Running_ID, R.Running_No, R.Running_Name,C.Cat_No + S.Sub_Cat_No + T.Type_No+ R.Running_No Item_Code" & vbLf
        SQL &= " FROM tb_Running R" & vbLf
        SQL &= " INNER JOIN tb_Type T ON  R.Type_ID = T.Type_ID AND T.Active_Status=1 " & vbLf
        SQL &= " INNER JOIN tb_Sub_Cat S ON T.Sub_Cat_ID=S.Sub_Cat_ID  AND S.Active_Status=1 " & vbLf
        SQL &= " INNER JOIN tb_Cat C ON C.Cat_ID = S.Cat_ID   AND C.Active_Status=1 " & vbLf
        SQL &= " INNER JOIN  tb_Dept DEPT ON  DEPT.Dept_ID=C.Dept_ID   AND DEPT.Active_Status=1 " & vbLf

        SQL &= " WHERE R.Active_Status=1 AND R.Running_ID=" & Running_ID & vbLf


        SQL &= " ORDER BY C.Cat_No,S.Sub_Cat_No,T.Type_No,R.Running_No" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function


    ''' <summary>
    ''' หาว่าใบประเมินนั้นผ่านการประเมินหรือไม่ และมีเลข AVL หรือไม่
    ''' </summary>
    Public Function GetAVLNoFromNewAssessment(ByVal Ass_ID As Integer, ByVal AssessmentType As AssessmentType, Optional ByVal ReAss_ID As Integer = -1) As AVLCode
        Dim SQL As String = "SELECT * " & vbLf

        Select Case AssessmentType
            Case AVLBL.AssessmentType.Assessment
                SQL &= " FROM tb_Ass_Header WHERE Ass_ID=" & Ass_ID & vbLf
            Case AVLBL.AssessmentType.ReAssessment
                SQL &= " FROM tb_ReAss_Header WHERE Ass_ID=" & Ass_ID & " AND ReAss_ID=" & ReAss_ID & vbLf
        End Select

        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable '---------Header Table----------
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then Return New AVLCode

        Dim Result As New AVLCode
        With Result
            .AVL_Y = DT.Rows(0).Item("AVL_Y").ToString
            .AVL_M = DT.Rows(0).Item("AVL_M").ToString
            .AVL_Dept_ID = DT.Rows(0).Item("AVL_Dept_ID").ToString
            .AVL_Sub_Dept_ID = DT.Rows(0).Item("AVL_Sub_Dept_ID").ToString
            .AVL_No = DT.Rows(0).Item("AVL_No").ToString
            If .AVL_Y.Length = 2 And .AVL_M.Length = 2 And .AVL_No.Length = 5 Then
                .AVL_Code = "AVL" & .AVL_Y & .AVL_M & .AVL_Dept_ID & .AVL_Sub_Dept_ID & .AVL_No
            End If
        End With
        Return Result

    End Function


    Public Sub UpdateAVLStatus(ByVal AVL_Code As AVLCode, ByVal Status As Boolean, ByVal Update_By As Integer)
        Dim SQL As String = ""
        SQL &= " UPDATE tb_Ass_Header SET AVL_Status=" & CInt(Status) & vbLf
        SQL &= " WHERE AVL_Y='" & AVL_Code.AVL_Y.Replace("'", "''") & "' AND AVL_M='" & AVL_Code.AVL_M.Replace("'", "''") & "' AND AVL_Dept_ID='" & AVL_Code.AVL_Dept_ID.Replace("'", "''") & "' AND AVL_Sub_Dept_ID='" & AVL_Code.AVL_Sub_Dept_ID.Replace("'", "''") & "' AND AVL_No='" & AVL_Code.AVL_No.Replace("'", "''") & "'" & vbLf
        SQL &= " " & vbLf

        SQL &= " UPDATE tb_ReAss_Header SET AVL_Status=" & CInt(Status) & vbLf
        SQL &= " WHERE AVL_Y='" & AVL_Code.AVL_Y.Replace("'", "''") & "' AND AVL_M='" & AVL_Code.AVL_M.Replace("'", "''") & "' AND AVL_Dept_ID='" & AVL_Code.AVL_Dept_ID.Replace("'", "''") & "' AND AVL_Sub_Dept_ID='" & AVL_Code.AVL_Sub_Dept_ID.Replace("'", "''") & "' AND AVL_No='" & AVL_Code.AVL_No.Replace("'", "''") & "'" & vbLf
        SQL &= " " & vbLf

        SQL &= " UPDATE tb_AVL_List SET AVL_Status=" & CInt(Status) & vbLf
        SQL &= " WHERE AVL_Y='" & AVL_Code.AVL_Y.Replace("'", "''") & "' AND AVL_M='" & AVL_Code.AVL_M.Replace("'", "''") & "' AND AVL_Dept_ID='" & AVL_Code.AVL_Dept_ID.Replace("'", "''") & "' AND AVL_Sub_Dept_ID='" & AVL_Code.AVL_Sub_Dept_ID.Replace("'", "''") & "' AND AVL_No='" & AVL_Code.AVL_No.Replace("'", "''") & "'" & vbLf
        SQL &= " " & vbLf


        'SQL &= " UPDATE tb_Ass_Header SET AVL_Status=" & CInt(Status) & vbLf
        'SQL &= " WHERE Ass_ID=(SELECT Ass_ID FROM tb_Ass_Header WHERE AVL_Y='" & AVL_Code.AVL_Y.Replace("'", "''") & "' AND AVL_M='" & AVL_Code.AVL_M.Replace("'", "''") & "' AND AVL_Dept_ID='" & AVL_Code.AVL_Dept_ID.Replace("'", "''") & "' AND AVL_Sub_Dept_ID='" & AVL_Code.AVL_Sub_Dept_ID.Replace("'", "''") & "' AND AVL_No='" & AVL_Code.AVL_No.Replace("'", "''") & "')" & vbLf
        'SQL &= " " & vbLf


        'SQL &= " UPDATE tb_ReAss_Header SET AVL_Status=" & CInt(Status) & vbLf
        'SQL &= " WHERE ReAss_ID=(SELECT ReAss_ID FROM tb_ReAss_Header WHERE AVL_Y='" & AVL_Code.AVL_Y.Replace("'", "''") & "' AND AVL_M='" & AVL_Code.AVL_M.Replace("'", "''") & "' AND AVL_Dept_ID='" & AVL_Code.AVL_Dept_ID.Replace("'", "''") & "' AND AVL_Sub_Dept_ID='" & AVL_Code.AVL_Sub_Dept_ID.Replace("'", "''") & "' AND AVL_No='" & AVL_Code.AVL_No.Replace("'", "''") & "')" & vbLf
        'SQL &= " " & vbLf

        Dim Conn As New SqlConnection(ConnectionString)
        Conn.Open()
        Dim Comm As New SqlCommand
        With Comm
            .Connection = Conn
            .CommandType = CommandType.Text
            .CommandText = SQL
            .ExecuteNonQuery()
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()
    End Sub

    ''' <summary>
    ''' บันทึกเลข AVL ให้กับผู้ขายที่ผ่านการประเมินผู้ขายใหม่
    ''' </summary>
    ''' <param name="Ass_ID">ID ใบประเมิน</param>
    ''' <param name="Update_By">ID ของ User ที่ Update</param>
    Public Sub SetAVLNoForNewAssessment(ByVal Ass_ID As Integer, ByVal AVL_Code As AVLCode, ByVal Update_By As Integer, ByVal AssessmentType As AssessmentType, Optional ByVal ReAss_ID As Integer = -1)

        Dim SQL As String = "SELECT Ass_ID,AVL_Y,AVL_M,AVL_Dept_ID,AVL_Sub_Dept_ID,AVL_No,AVL_Status,Update_By,Update_Time " & vbLf
        Select Case AssessmentType
            Case AVLBL.AssessmentType.Assessment
                SQL &= " FROM tb_Ass_Header WHERE Ass_ID=" & Ass_ID & vbLf
            Case AVLBL.AssessmentType.ReAssessment
                SQL &= " ,ReAss_ID"
                SQL &= " FROM tb_ReAss_Header WHERE Ass_ID=" & Ass_ID & " AND ReAss_ID=" & ReAss_ID & vbLf
        End Select

        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable '---------Header Table----------
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then Return '---------- Not Found in header ----------

        DT.Rows(0).Item("AVL_Y") = AVL_Code.AVL_Y
        DT.Rows(0).Item("AVL_M") = AVL_Code.AVL_M
        DT.Rows(0).Item("AVL_No") = AVL_Code.AVL_No
        DT.Rows(0).Item("AVL_Dept_ID") = AVL_Code.AVL_Dept_ID
        DT.Rows(0).Item("AVL_Sub_Dept_ID") = AVL_Code.AVL_Sub_Dept_ID
        DT.Rows(0).Item("AVL_Status") = True
        DT.Rows(0).Item("Update_By") = Update_By
        DT.Rows(0).Item("Update_Time") = Now

        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)

        '------------- Save To AVL List-----------------
        SQL = "SELECT * FROM tb_AVL_List " & vbLf
        SQL &= " WHERE (AVL_Y='" & AVL_Code.AVL_Y & "' AND AVL_M='" & AVL_Code.AVL_M & "' AND AVL_Dept_ID='" & AVL_Code.AVL_Dept_ID & "' AND AVL_Sub_Dept_ID='" & AVL_Code.AVL_Sub_Dept_ID & "' AND AVL_No='" & AVL_Code.AVL_No & "') OR Ass_ID=" & Ass_ID
        DT = New DataTable '------------- AVL Table -----------
        DA = New SqlDataAdapter(SQL, ConnectionString)
        DA.Fill(DT)
        Dim HT As DataTable = GetAssessmentHeader(Ass_ID)

        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            DT.Rows.Add(DR)
        Else
            DR = DT.Rows(0)
        End If

        DR("AVL_Y") = AVL_Code.AVL_Y
        DR("AVL_M") = AVL_Code.AVL_M
        DR("AVL_Dept_ID") = AVL_Code.AVL_Dept_ID
        DR("AVL_Sub_Dept_ID") = AVL_Code.AVL_Sub_Dept_ID
        DR("AVL_No") = AVL_Code.AVL_No
        DR("Ass_ID") = Ass_ID
        Select Case AssessmentType
            Case AVLBL.AssessmentType.Assessment
                DR("ReAss_ID") = DBNull.Value
            Case AVLBL.AssessmentType.ReAssessment
                DR("ReAss_ID") = ReAss_ID
        End Select
        DR("Dept_ID") = HT.Rows(0).Item("Dept_ID")
        DR("Sub_Dept_ID") = HT.Rows(0).Item("Sub_Dept_ID")
        DR("Ref_Number") = HT.Rows(0).Item("Ref_Number")
        DR("Dept_Name") = HT.Rows(0).Item("Dept_Name")
        DR("Sub_Dept_Name") = HT.Rows(0).Item("Sub_Dept_Name")
        DR("S_ID") = HT.Rows(0).Item("S_ID")
        DR("S_Tax_No") = HT.Rows(0).Item("S_Tax_No")
        DR("S_Name") = HT.Rows(0).Item("S_Name")
        DR("S_Alias") = HT.Rows(0).Item("S_Alias")
        DR("S_Address") = HT.Rows(0).Item("S_Address")
        DR("S_Phone") = HT.Rows(0).Item("S_Phone")
        DR("S_Fax") = HT.Rows(0).Item("S_Fax")
        DR("S_Contact_Name") = HT.Rows(0).Item("S_Contact_Name")
        DR("S_Email") = HT.Rows(0).Item("S_Email")
        DR("S_Type_ID") = HT.Rows(0).Item("S_Type_ID")
        DR("S_Type_Name") = HT.Rows(0).Item("S_Type_Name")
        DR("Item_Type") = HT.Rows(0).Item("Type_ID")
        DR("Item_No") = HT.Rows(0).Item("Item_No")
        DR("Item_Name") = HT.Rows(0).Item("Item_Name")
        DR("Qty") = HT.Rows(0).Item("Qty")
        DR("UOM") = HT.Rows(0).Item("UOM")
        DR("Lot") = HT.Rows(0).Item("Lot")
        DR("Brand") = HT.Rows(0).Item("Brand")
        DR("Model") = HT.Rows(0).Item("Model")
        DR("Color") = HT.Rows(0).Item("Color")
        DR("Size") = HT.Rows(0).Item("Size")
        DR("Ref_Doc") = HT.Rows(0).Item("Ref_Doc")
        DR("RecievedDate") = HT.Rows(0).Item("RecievedDate")
        DR("Price") = HT.Rows(0).Item("Price")
        '----------- Get Score ---------
        DR("Get_Score") = HT.Rows(0).Item("Get_Score")
        DR("Pass_Score") = HT.Rows(0).Item("Pass_Score")
        DR("Max_Score") = HT.Rows(0).Item("Max_Score")
        DR("AVL_Status") = True
        DR("Update_By") = Update_By
        DR("Update_Time") = Now

        HT.Dispose()
        cmd = New SqlCommandBuilder(DA)
        DA.Update(DT)
        DT.Dispose()
        DA.Dispose()

    End Sub

    Public Sub DisableAVLStatus(ByVal Ass_ID As Integer, ByVal Update_By As Integer, ByVal AssessmentType As AssessmentType, Optional ByVal ReAss_ID As Integer = -1)
        Dim SQL As String = ""
        '-------------Disable AVL ก่อนหน้าหรือถอย step----------------
        SQL &= " UPDATE tb_AVL_List SET AVL_Status=0" & vbLf
        SQL &= " WHERE Ass_ID='" & Ass_ID & "'" & vbLf
        SQL &= " " & vbLf
        SQL &= " UPDATE tb_Ass_Header SET AVL_Status=0" & vbLf
        SQL &= " WHERE Ass_ID='" & Ass_ID & "'" & vbLf
        SQL &= " " & vbLf
        If AssessmentType = AVLBL.AssessmentType.ReAssessment Then
            SQL &= " UPDATE tb_ReAss_Header SET AVL_Status=0" & vbLf
            SQL &= " WHERE Ass_ID='" & Ass_ID & "'  AND ReAss_ID =" & ReAss_ID & vbLf
        End If
        Dim Conn As New SqlConnection(ConnectionString)
        Conn.Open()
        Dim Comm As New SqlCommand
        With Comm
            .Connection = Conn
            .CommandType = CommandType.Text
            .CommandText = SQL
            .ExecuteNonQuery()
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()

    End Sub

    Public Structure AVLCode
        Public AVL_Y As String
        Public AVL_M As String
        Public AVL_Dept_ID As String
        Public AVL_Sub_Dept_ID As String
        Public AVL_No As String
        Public AVL_Code As String
    End Structure

    ''' <summary>
    ''' คำนวนหาเลข Autorun ของ AVL ของแต่ละเดือน
    ''' </summary>
    ''' <param name="Y">ปี</param>
    ''' <param name="M">เดือน</param>
    ''' <param name="Dept_ID">รหัสสำนักงาน</param>
    ''' <param name="Sub_Dept_ID">รหัสฝ่าย</param>

    Public Function GetNewAVLNo(ByVal Y As String, ByVal M As String, ByVal Dept_ID As String, ByVal Sub_Dept_ID As String) As AVLCode
        Dim SQL As String = "SELECT ISNULL(MAX(AVL_No),0)+1 AVL_No" & vbLf
        SQL &= " FROM tb_AVL_List WHERE AVL_Y='" & Y.Replace("'", "''") & "' AND AVL_M='" & M.Replace("'", "''") & "'"
        SQL &= " AND AVL_Dept_ID='" & Dept_ID.Replace("'", "''") & "' AND AVL_Sub_Dept_ID='" & Sub_Dept_ID.Replace("'", "''") & "'"

        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim Result As New AVLCode
        With Result
            .AVL_Y = Y.PadLeft(2, "0")
            .AVL_M = M.PadLeft(2, "0")
            .AVL_Dept_ID = Dept_ID.PadLeft(2, "0")
            .AVL_Sub_Dept_ID = Sub_Dept_ID.PadLeft(2, "0")
            .AVL_No = DT.Rows(0)(0).ToString.PadLeft(5, "0")
            .AVL_Code = "AVL" & .AVL_Y & .AVL_M & .AVL_Dept_ID & .AVL_Sub_Dept_ID & .AVL_No
        End With
        Return Result
    End Function

    ''' <summary>
    ''' ลบข้อมูลเลข AVL ที่สร้างการประเมินผู้ขายใหม่
    ''' </summary>
    ''' <param name="Ass_ID">ID ใบประเมิน</param>
    ''' <param name="Update_By">ID ของ User ที่ Update</param>
    Public Sub RemoveAVLFromAssessment(ByVal ASS_ID As Integer, ByVal Update_By As Integer)
        Dim SQL As String = "DELETE FROM tb_AVL_List WHERE Ass_ID=" & ASS_ID & vbLf
        SQL &= "UPDATE tb_Ass_Header SET " & vbLf
        SQL &= " AVL_Y=NULL,AVL_M=NULL,AVL_Dept_ID=NULL,AVL_Sub_Dept_ID=NULL,AVL_No=NULL,AVL_Status=NULL,Update_By=NULL,Update_Time=GetDate() " & vbLf
        SQL &= " WHERE ASS_ID = " & ASS_ID & vbLf
        SQL &= "SELECT 1" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        'Dim Conn As New SqlConnection(ConnectionString)
        'Conn.Open()
        'Dim Comm As New SqlCommand
        'With Comm
        '    .Connection = Conn
        '    .CommandType = CommandType.Text
        '    .CommandText = SQL
        '    .ExecuteNonQuery()
        '    .Dispose()
        'End With
        'Conn.Close()
    End Sub

    ''' <summary>
    ''' ลบข้อมูลเลข AVL ที่สร้างการประเมินทบทวน
    ''' </summary>
    ''' <param name="ReAss_ID">ID ใบประเมินทบทวน</param>
    ''' <param name="Update_By">ID ของ User ที่ Update</param>
    Public Sub RemoveAVLFromReAssessment(ByVal ReAss_ID As Integer, ByVal Update_By As Integer)
        Dim SQL As String = "DELETE FROM tb_AVL_List WHERE ReAss_ID=" & ReAss_ID & vbLf
        SQL &= "UPDATE tb_ReAss_Header SET " & vbLf
        SQL &= " AVL_Y=NULL,AVL_M=NULL,AVL_Dept_ID=NULL,AVL_Sub_Dept_ID=NULL,AVL_No=NULL,AVL_Status=NULL,Update_By=NULL,Update_Time=GetDate() " & vbLf
        SQL &= " WHERE ReAss_ID = " & ReAss_ID & vbLf
        Dim Conn As New SqlConnection(ConnectionString)
        Conn.Open()
        Dim Comm As New SqlCommand
        With Comm
            .Connection = Conn
            .CommandType = CommandType.Text
            .CommandText = SQL
            .ExecuteNonQuery()
            .Dispose()
        End With
        Conn.Close()
    End Sub

    ''' <summary>
    ''' ลบข้อมูลเลข AVL ที่สร้างการประเมินผู้ขายใหม่
    ''' </summary>
    ''' <param name="AVL_Y">ปี</param>
    ''' <param name="AVL_M">เดือน</param>
    ''' <param name="AVL_Dept_ID">รหัสสำนักงาน</param>
    ''' <param name="AVL_Sub_Dept_ID">รหัสฝ่าย</param>
    ''' <param name="AVL_No">เลข Running</param>
    ''' <param name="Update_By">ID ของ User ที่ Update</param>
    Public Sub RemoveAVLFromAVLNo(ByVal AVL_Y As String, ByVal AVL_M As String, ByVal AVL_Dept_ID As String, ByVal AVL_Sub_Dept_ID As String, ByVal AVL_No As String, ByVal Update_By As Integer)
        Dim SQL As String = "DELETE FROM tb_AVL_List WHERE AVL_Y='" & AVL_Y.Replace("'", "''") & "' AND AVL_M='" & AVL_M.Replace("'", "''") & ",AVL_Dept_ID='" & AVL_Dept_ID.Replace("'", "''") & " AND AVL_Sub_Dept_ID='" & AVL_Sub_Dept_ID.Replace("'", "''") & "' AND AVL_No='" & AVL_No.Replace("'", "''") & "'" & vbLf

        '------------ Update Ass_Header ------------
        SQL &= "UPDATE tb_Ass_Header " & vbLf
        SQL &= " SET AVL_Y=NULL,AVL_M=NULL,AVL_Dept_ID=NULL,AVL_Sub_Dept_ID=NULL,AVL_No=NULL,AVL_Status=NULL,Update_By=NULL,Update_Time=GetDate() " & vbLf
        SQL &= " WHERE AVL_Y='" & AVL_Y.Replace("'", "''") & "' AND AVL_M='" & AVL_M.Replace("'", "''") & ",AVL_Dept_ID='" & AVL_Dept_ID.Replace("'", "''") & " AND AVL_Sub_Dept_ID='" & AVL_Sub_Dept_ID.Replace("'", "''") & "' AND AVL_No='" & AVL_No.Replace("'", "''") & "'" & vbLf
        '------------ Update ReAss_Header ------------
        SQL &= "UPDATE tb_ReAss_Header " & vbLf
        SQL &= " SET AVL_Y=NULL,AVL_M=NULL,AVL_Dept_ID=NULL,AVL_Sub_Dept_ID=NULL,AVL_No=NULL,AVL_Status=NULL,Update_By=NULL,Update_Time=GetDate() " & vbLf
        SQL &= " WHERE AVL_Y='" & AVL_Y.Replace("'", "''") & "' AND AVL_M='" & AVL_M.Replace("'", "''") & ",AVL_Dept_ID='" & AVL_Dept_ID.Replace("'", "''") & " AND AVL_Sub_Dept_ID='" & AVL_Sub_Dept_ID.Replace("'", "''") & "' AND AVL_No='" & AVL_No.Replace("'", "''") & "'" & vbLf
        Dim Conn As New SqlConnection(ConnectionString)
        Conn.Open()
        Dim Comm As New SqlCommand
        With Comm
            .Connection = Conn
            .CommandType = CommandType.Text
            .CommandText = SQL
            .ExecuteNonQuery()
            .Dispose()
        End With
        Conn.Close()
    End Sub

    ''' <summary>
    ''' แสดงรูปแบบผลการประเมินใน Grid
    ''' </summary>
    ''' <param name="Get_Score">คะแนนที่ได้</param>
    ''' <param name="Pass_Score">เกณฑ์ผ่าน</param>
    ''' <param name="Max_Score">คะแนนเต็ม</param>
    Public Sub DisplayAssessmentGridResultLabel(ByVal Get_Score As Object, ByVal Pass_Score As Object, ByVal Max_Score As Object, ByVal Pass_Status As Boolean, ByRef lbl As Label)
        If Not IsDBNull(Max_Score) And Not IsDBNull(Pass_Score) Then
            If IsDBNull(Get_Score) Then
                lbl.Text = "ยังไม่ประเมิน"
                lbl.ForeColor = Drawing.Color.Orange
            ElseIf Pass_Status And (Get_Score >= Pass_Score) Then
                lbl.Text = "<i class='icon-ok-sign'></i>  " & Get_Score & " / " & Max_Score
                lbl.ForeColor = Drawing.Color.Green
            Else
                lbl.Text = "<i class='icon-remove-sign'></i>  " & Get_Score & " / " & Max_Score
                lbl.ForeColor = Drawing.Color.Red
            End If
        End If
    End Sub

#Region "Compare Supplier"
    ''' <summary>
    ''' ดึงข้อมูลผู้ขายที่เคยถูกประเมินตามสินค้าที่เลือก
    ''' </summary>
    Public Function GetSupplierList_Compare(ByVal WithOutBlackList As Boolean, ByRef Running_ID As Integer, Optional ByRef S_ID_Select As String = "", Optional ByRef Dept_ID As String = "") As DataTable
        Dim SQL As String = ""
        SQL &= "  SELECT DISTINCT Supplier.* " & vbLf
        SQL &= "  FROM tb_Sup Supplier" & vbLf
        If WithOutBlackList Then
            SQL &= " LEFT JOIN tb_BlackList BL ON Supplier.S_ID=BL.S_ID AND BL.B_EndDate > GETDATE()" & vbLf
        End If
        SQL &= "  INNER JOIN vw_Ass_Header vw ON vw.S_ID=Supplier.S_ID" & vbLf
        SQL &= "  WHERE vw.Current_Step =5 " & vbLf  '--Supplier.S_Tax_No IS NOT NULL  AND 
        If WithOutBlackList Then
            SQL &= " AND BL.S_ID IS NULL AND vw.Running_ID='" & Running_ID & "' AND vw.Dept_ID ='" & Dept_ID & "'" & vbLf
            If S_ID_Select <> "" Then
                SQL &= " AND Supplier.S_ID NOT IN (" & S_ID_Select & ")" & vbLf

            End If
        End If

        SQL &= "  ORDER BY Active_Status desc,Supplier.S_Name" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function


    ''' <summary>
    ''' หาข้อมูลการประเมินผู้ขายใหม่_ของผู้ขาย
    ''' </summary>
    Public Function GetAssessmentHeader_Compare(ByVal S_ID As Integer, ByVal Running_ID As Integer) As DataTable
        Dim SQL As String = "SELECT * FROM vw_Ass_Header WHERE S_ID=" & S_ID & " And Running_ID = " & Running_ID & "  AND Current_Step =5 --AND AVL_No IS NOT NULL"
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

    ''' <summary>
    ''' หาข้อมูลการประเมินทบทวน_ของผู้ขาย
    ''' </summary>
    Public Function GetReassessmentHeader_Compare(ByVal S_ID As Integer, ByVal Running_ID As Integer) As DataTable
        Dim SQL As String = "SELECT * FROM vw_ReAss_Header  WHERE S_ID=" & S_ID & " And Running_ID = " & Running_ID & "  AND Current_Step =5 --AND AVL_No IS NOT NULL"
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function

#End Region

    ''' <summary>
    ''' ใส่ข้อมูลประเภทหนังสือใน Dropdownlist ที่ต้องการ
    ''' </summary>
    ''' <param name="ddl">Dropdownlist ที่ต้องการใส่ข้อมูล</param>
    ''' <param name="TextIndex">ข้อความตั้งต้นใน Dropdownlist</param>
    ''' <param name="SelectedValue">แสดง Default ที่ระบุตาม Parameter นี้</param>
    Public Sub BindDDlDN(ByRef ddl As DropDownList, Optional ByVal TextIndex As String = "", Optional ByVal SelectedValue As Integer = -1)
        Dim SQL As String = "SELECT * FROM [tb_Sub_Dept]" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        ddl.Items.Add(New ListItem("เลือกฝ่าย/ภาค/งาน", 0))

        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("Sub_Dept_Name"), DT.Rows(i).Item("Sub_Dept_ID"))
            ddl.Items.Add(Item)
        Next

        ddl.SelectedIndex = 0
        If SelectedValue > 0 Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value = SelectedValue Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub
End Class
