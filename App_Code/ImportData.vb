﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System.Configuration
Imports System.IO
Imports System.Drawing

Public Class ImportData

    Public ConnectionString As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString

    ''' <summary>
    ''' หา Key ใหม่ ของตาราง และคอลัมน์ ที่กำหนด
    ''' </summary>
    ''' <param name="TableName">ชื่อตารางที่ต้องการหา Key</param>
    ''' <param name="ColumnName">ชื่อคอลัมน์ที่ต้องการหา Key</param>
    Public Function GetNewPrimaryID(ByVal TableName As String, ByVal ColumnName As String) As String
        Dim id As String = ""
        Dim SQL As String = ""
        SQL = "SELECT IsNull(MAX(" & ColumnName & "),0)+1 FROM " & TableName.Replace("'", "''")
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)
    End Function




    ''' <summary>
    ''' 
    ''' </summary>
    Public Function GetReassessmentHeader(ByVal Ass_ID As Integer, Optional ByVal ReAss_ID As Integer = 0) As DataTable
        Dim SQL As String = "SELECT * FROM vw_ReAss_Header WHERE Ass_ID=" & Ass_ID
        If ReAss_ID <> 0 Then
            SQL &= " And ReAss_ID = " & ReAss_ID
        End If
        Dim DA As New SqlDataAdapter(SQL, ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT
    End Function









End Class
