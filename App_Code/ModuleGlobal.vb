﻿Imports System.IO
Imports System.Drawing
Imports System

Public Module ModuleGlobal


    Public Structure FileStructure
        Public ContentType As String
        Public FileName As String
        Public Content As Byte()
    End Structure

#Region "ImageProcessing"

    Public Function GetImageContentType(ByVal Image As Drawing.Image) As String
        Select Case Image.RawFormat.Guid
            Case Drawing.Imaging.ImageFormat.Bmp.Guid
                Return "image/x-ms-bmp"
            Case Drawing.Imaging.ImageFormat.Jpeg.Guid
                Return "image/jpeg"
            Case Drawing.Imaging.ImageFormat.Gif.Guid
                Return "image/gif"
            Case Drawing.Imaging.ImageFormat.Png.Guid
                Return "image/png"
            Case Else
                Return ""
        End Select
    End Function




#End Region
End Module
