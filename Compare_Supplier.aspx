﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Compare_Supplier.aspx.vb" Inherits="Compare_Supplier" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="asp" %>
<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link type="text/css" rel="stylesheet" href="assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" />
    <link href="assets/css/pages/pricing-tables.css" rel="stylesheet" type="text/css">
    <link href="assets/plugins/glyphicons/css/glyphicons.css" rel="stylesheet">

    <link href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" rel="stylesheet"
        type="text/css" />
    <link href="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
        type="text/css" />
    <link href="assets/plugins/chosen-bootstrap/chosen/chosen.css" rel="stylesheet" type="text/css" />
        <style type="text/css">
        .gridResult tr td
        {
            background-color: white;
            cursor: pointer;
        }
        
        .gridResult tr:hover td
        {
            background-color: #ccccff;
        }
    </style>
     <link href="style/style.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

 <asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>

    <div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							เปรียบเทียบผู้ขาย
						</h3>	
									
						<ul class="breadcrumb">
                            
                            <li>
                            	<i class="icon-briefcase"></i><a href="javascript:;">ประวัติ/รายงานสรุป</a><i class="icon-angle-right"></i>
                            </li>
                        	<li><i class="icon-align-justify"></i> <a href="javascript:;">เปรียบเทียบผู้ขาย</a></li>
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
				     </div>				
			    </div>
<asp:UpdatePanel ID="udpDialogItem" runat="server">
<ContentTemplate>



<div class="row-fluid">
	<div class="span12">
	    <div class="portlet-body">
            <div class="table-toolbar">
			    <div class="btn-group">     
                    <asp:LinkButton CssClass="btn green" id="lnkSelectItem" runat="server" ToolTip="ต้องการประเมินผู้ขายรายใหม่">เลือก พัสดุ/ครุภัณฑ์เพื่อเปรียบเทียบ Click  <i class="icon-plus"></i></asp:LinkButton>
                    </div>
                    <div class="btn-group">     
                    <asp:LinkButton CssClass="btn green" id="btnPrint" runat="server" ToolTip="พิมพ์หน้าจอ"> พิมพ์  <i class="icon-print"></i></asp:LinkButton>
                    </div>
                <div class="btn-group pull-right" id="dvbtnPrint" runat="server" visible="false">                                    
				    <button data-toggle="dropdown" class="btn dropdown-toggle" id="Button1">พิมพ์ <i class="icon-angle-down"></i></button>										
				    <ul class="dropdown-menu pull-right">
					    <li><asp:LinkButton ID="btnPDF" runat="server">รูปแบบ PDF</asp:LinkButton></li>
					    <li><asp:LinkButton ID="btnExcel" runat="server">รูปแบบ Excel</asp:LinkButton></li>											
				    </ul>
			    </div>
		    </div>

            
	    <div role="grid" class="dataTables_wrapper form-inline" id="sample_editable_1_wrapper">
           <div class="portlet-body no-more-tables">
                <asp:Panel ID="pnlHeader_Item" runat="server" >
				    <b><h4><asp:Label ID="lblHeader_ItemLeft" runat="server" Text ="" ></asp:Label>   <asp:Label ID="lblHeader_Item" runat="server" ></asp:Label></h4></b>
                </asp:Panel>                
				    <asp:Panel ID="pnl_Product" runat="server" Visible ="false"  CssClass="span12"> 
                        <ul class="breadcrumb">
						    <li>
							    <i class="icon-barcode"></i>
                                <asp:Label ID="lbl_Cat" runat="server"></asp:Label>
							    <span class="icon-angle-right"></span>
						    </li>
						    <li>
							    <asp:Label ID="lbl_Sub_Cat" runat="server"></asp:Label>
							    <span class="icon-angle-right"></span>
						    </li>
						    <li>
                                <asp:Label ID="lbl_Type" runat="server"></asp:Label>  
                                <span class="icon-angle-right"></span>                                             
                            </li>
                            <li>
                                <asp:Label ID="lbl_Running" runat="server"></asp:Label>  
                                                                        
                            </li>
					    </ul>
                    </asp:Panel>			
				                
                            
				<asp:Panel ID="PnlCompare" runat="server" > 
                    <div class="row-fluid" class="flow_container" style=" ">
                        <table style =" width :auto ; color :Black; "  border="1"    class="table table-full-width table-advance dataTable no-more-tables table-hover " >
                            <thead>
								<tr align ="center"  id="trcount" runat ="server" visible ="false"   >
									<td id="Td1" runat ="server"  style ="  color :Black ; cursor: pointer; text-align :center;   vertical-align :middle ; "  >พัสดุ/ครุภัณฑ์</td>
                                    <td ID="Td2" runat="server" align="center" style="  color :Black ;  text-align :center; vertical-align : middle; " colspan="4"><asp:Label ID="lblCount_S" runat="server" ></asp:Label></td>
								</tr>
                                <tr align="center">
                                    <td ID="Td3" runat="server"   style=" width :230px; background-color :Silver ; color :Black ; cursor: pointer; text-align :center;   vertical-align :middle ; " onclick="document.getElementById('lnkAddSupplier').click();">
                                        <b><h5> เพิ่มผู้ขาย&nbsp;&nbsp; <asp:LinkButton ID="lnkAddSupplier" runat="server"  class=" glyphicons no-js user_add  " ClientIDMode="Static"  style=" "><i></i></asp:LinkButton></h5></b>
                                        
                                    </td>
                                    <td ID="Td4" runat="server" align="center"  style="width: 150px;  background-color : #E6CC76 ; color :Black ; text-align :center;  vertical-align : middle  ; ">
                                        <b><h5> รูปภาพ</h5></b>
                                    </td>
                                    <td ID="Td5" runat="server" align="center"  style=" background-color : #E6CC76 ; color :Black ;text-align :center;  vertical-align : middle  ; ">
                                        <b><h5> รายละเอียด </h5></b>
                                    </td>
                                    <%--<td id="Td6"  runat="server" align="center"  style=" background-color : #E6CC76 ; color :Black ;text-align :center; vertical-align : middle  ; ">
                                        <h5> ราคา (บาท)</h5>
                                    </td>--%>
                                    <td id="Td7"  runat="server" align="center" 
                                        style=" background-color : #E6CC76 ; color :Black ;text-align :center;   vertical-align : middle  ; ">
                                        <b><h5> ประวัติการประเมิน </h5></b>
                                    </td>
                                </tr>
                            </thead>
                        <asp:Repeater ID="rptCompare" runat="server">								   
                             <ItemTemplate> 
						<tr>
                            <td id="Td8"  runat="server" align="center"  style="  background-color : #E6CC76 ; color :Black ; text-align :center; vertical-align : middle  ; ">
                                <asp:Panel ID="Panel2" runat="server">
                                    <b><asp:Label ID="lblS_Name" runat="server" Font-Size ="14px" ></asp:Label></b><asp:Label ID="lblS_ID" style=" display :none ;" runat="server" ></asp:Label>
                                    <div class="tools" style ="margin-top :0.5em;" >
                                        <asp:LinkButton ID="btnViewDetail_S" runat="server" CssClass="btn mini purple" CommandName="View"><i class="icon-search "></i> ข้อมูลผู้ขาย</asp:LinkButton>
                                        <asp:LinkButton ID="btnRemove_S" runat="server" CssClass="btn mini red " CommandName="Remove"><i class="icon-remove "></i> เอาออก</asp:LinkButton>
                                    </div>
                                </asp:Panel>
                            </td>
                            <td ID="Td9" runat="server"  style=" vertical-align :middle ; text-align :center ;">
                                <a ID="lnk_File_Dialog" runat="server" class="fancybox-button" data-rel="fancybox-button" target="_blank" title="Photo">
                                                        
                                    <div class="zoom">
                                        <%--<div class="img_Default" id="imgDefault" runat="server">
                                            <i class="icon-ok-sign"></i>
                                        </div>--%>
                                        <asp:Image ID="img_File" runat="server" AlternateText="" style="width: 130px;" />              
										<div class="zoom-icon"></div>                                                            
									</div>
                                </a>
                            </td>
                            <td ID="Td10" runat="server" style=" vertical-align :top ;">
                                <asp:Panel ID="pnlDetail" runat="server">
                                    <ul class=" pricing-content_Property unstyled" style=" border-bottom :-2px; color  :Black ; ">
                                        <li><b>ยี่ห้อ :</b> <asp:Label ID="lblDetail_Brand" runat="server" ></asp:Label></li>
                                        <li><b>รุ่น/Model :</b> <asp:Label ID="lblDetail_Model" runat="server" ></asp:Label> </li>
                                        <li><b>สี :</b> <asp:Label ID="lblDetail_Color" runat="server" ></asp:Label> </li>
                                        <li><b>ขนาด :</b> <asp:Label ID="lblDetail_Size" runat="server" ></asp:Label></li>
                                        <li><b>วันที่ได้มา :</b> <asp:Label ID="lblDetail_RecievedDate" runat="server" ></asp:Label></li>
                                        <li><b>ราคา :</b> <asp:Label ID="lblPrice" runat="server" ></asp:Label></li>
                                    </ul>
                                </asp:Panel>
                            </td>
                            <%--<td id="Td11"  runat="server" style=" vertical-align :middle ;">
                                <asp:Panel ID="pnlPrice" runat="server">
                                    <ul class="pricing-content_Property unstyled" style=" border-bottom :0px; text-align :right ;">
                                        <li><asp:Label ID="lblPrice_" runat="server" ></asp:Label></li>
                                    </ul>
                                </asp:Panel>
                            </td>--%>
                            <td id="Td12"  runat="server" 
                                style=" vertical-align :middle ;">
                                <asp:Panel ID="pnlHistoryAVL" runat="server">

                                    <table class="table table-full-width table-advance dataTable no-more-tables table-hover">                                
									    <thead>
										    <tr>											            
											    <th style="text-align:center; vertical-align:middle;">เลขใบประเมิน</th>                                                
                                                <th style="text-align:center; vertical-align:middle;">คะแนนประเมิน</th>                                                                                                            
                                                <th style="text-align:center; vertical-align:middle;">เลข AVL</th>
                                                <th style="text-align:center; vertical-align:middle;">วันที่ประเมิน</th>
										    </tr>
									    </thead>
									    <tbody>
                                            <asp:Repeater ID="rptHistory" runat="server">
                                            <ItemTemplate>
                                                <tr>
											        <td data-title="เลขใบประเมิน" style="text-align:center;"><asp:Label ID="lblRef_Code" runat="server" style="text-decoration:none; width:80%; float:left; cursor:pointer;" target="_blank"></asp:Label> </td>                                                    
                                                    <td data-title="คะแนนประเมิน" style="text-align:center;"><asp:Label ID="lblScore" runat="server" ></asp:Label></td>
                                                    <td data-title="เลข AVL" style="text-align:center;"> <asp:Label ID="lblAVL" runat="server"></asp:Label></td>
                                                    <td data-title="วันที่ประเมิน" style="text-align:center;"><asp:Label ID="lblUpdateDate" runat="server" ></asp:Label></td>
										        </tr>
                                            </ItemTemplate>
                                            </asp:Repeater>										            
									    </tbody>
								    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                             </ItemTemplate>
                        </asp:Repeater>

					</table>

                </div>
				
          </asp:Panel>

           </div>                      
        </div>
            

	</div>
	</div>
</div>    


      
                         <asp:Panel CssClass="modal-scrollable" id="dialogSelectItem" runat="server">
                            <div class="modal fade in modal-overflow" style="display: block; margin-top: 30px; ">
				                <div class="modal-header">
					                <h3>เลือกประเภทสินค้า / พัสดุครุภัณฑ์ งานจ้างและบริการ</h3>
				                </div>
				                <div class="modal-body">
                                    <div id="ctl00_ContentPlaceHolder1_udpDialog">
                                        <div class="row-fluid form-horizontal">							
								                <div class="control-group">
									                <label class="control-label"> สำนักงาน</label>
									                <div class="controls">
                                                       <asp:DropDownList ID="ddl_Dept" runat="server" Font-Bold="True" CssClass="span12" AutoPostBack="True"></asp:DropDownList>	       
									                </div>
								                </div>                                           
                                        </div>
                                        <div class="row-fluid form-horizontal" id="pnlbtnProductDialog" runat="server" style="margin-bottom: 15px;">
                                            <div class="btn-group pull-right">
                                                <asp:LinkButton CssClass="btn blue" ID="btnProductDialog" runat="server"><i class="icon-barcode"></i> เลือกประเภท</asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="row-fluid form-horizontal" id="pnlHeaderDialog" runat="server">
                                            <div class="control-group">
                                                <label class="control-label">
                                                    <asp:Label ID="lblHeader_ItemDialog" runat="server"></asp:Label></label>
                                                <div class="controls">
                                                    <asp:Label ID="lbl_Item_TypeDialog" runat="server" Font-Bold="True" CssClass="span12"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                        <%--<div class="row-fluid form-horizontal" id="pnlCat" runat="server">							
								                <div class="control-group">
									                <label class="control-label"> ประเภท</label>
									                <div class="controls">
                                                      <asp:DropDownList ID="ddl_Cat" runat="server" Font-Bold="True" CssClass="span12" AutoPostBack="True"></asp:DropDownList>       
									                </div>
								                </div>                                           
                                        </div>
                                        <div class="row-fluid form-horizontal" id="pnlSubCat" runat="server">							
								                <div class="control-group">
									                <label class="control-label"> ชนิด</label>
									                <div class="controls">
                                                      <asp:DropDownList ID="ddl_Sub_Cat" runat="server" Font-Bold="True" CssClass="span12" AutoPostBack="True"></asp:DropDownList>	       
									                </div>
								                </div>                                           
                                        </div>
                                        <div class="row-fluid form-horizontal" id="pnlType" runat="server">							
								                <div class="control-group">
									                <label class="control-label"> ชนิดย่อย</label>
									                <div class="controls">
                                                        <asp:DropDownList ID="ddl_Type" runat="server" Font-Bold="True" CssClass="span12" AutoPostBack="True"></asp:DropDownList>	       
									                </div>
								                </div>                                           
                                        </div>--%>
                                    </div>					    
				                </div>
				                <div class="modal-footer" id="pnlCreate" runat="server">                        
                                        <asp:Button CssClass="btn red" ID="btnCreateCompare" runat="server" Text="สร้างการเปรียบเทียบ"></asp:Button>                                
				                </div>
                                <asp:LinkButton CssClass="fancybox-item fancybox-close" ToolTip="ปิด" id="lnkCloseDialogSelectItem" runat="server"></asp:LinkButton>
			                </div>
                            <div class="modal-backdrop" style="z-index: 10049;"></div>
                        </asp:Panel>



                    <asp:Panel CssClass="modal-scrollable" id="dialogSupplier" runat="server" Visible="false" style="margin-left:-450px;" DefaultButton="btnSearchSupplier">
                        <div class="modal fade in modal-overflow" style="display: block; margin-top: 30px; width:900px;">
				        <div class="modal-header">
					        <h3><i class="icon-user"></i> เลือกผู้ขาย / ผู้ให้บริการเพื่อเปรียบเทียบ</h3>
				        </div>
				        <div class="modal-body">                                                              
                                <div class="row-fluid form-horizontal">							
								        <div class="control-group span6">
									        <label class="control-label"> เลขผู้เสียภาษี</label>
									        <div class="controls">
                                                <asp:TextBox ID="txtSearchTaxNo" runat="server" CssClass="m-wrap" AutoPostBack="True" Placeholder="เลขประจำตัวผู้เสียภาษี"></asp:TextBox>       
									        </div>
								        </div> 
                                        <div class="control-group span6">
									        <label class="control-label"> ค้นจากชื่อ</label>
									        <div class="controls">
                                                <asp:TextBox ID="txtSearchName" runat="server" CssClass="m-wrap" AutoPostBack="True" Placeholder="ชื่อผู้ขาย บริษัท/หจก. ชื่อย่อ"></asp:TextBox>       
									        </div>
								        </div>                                                
                                </div>
                                    <div class="row-fluid form-horizontal">
                                        <div class="control-group span6">
									            <label class="control-label"> ผู้ติดต่อ</label>
									            <div class="controls">
                                                    <asp:TextBox ID="txtSearchAlias" runat="server" CssClass="m-wrap" AutoPostBack="True" Placeholder="ชื่อย่อ"></asp:TextBox>                                                       
									            </div>
								        </div>
                                        <div class="control-group span6">
									            <label class="control-label"> ข้อมูลติดต่อ</label>
									            <div class="controls">
                                                    <asp:TextBox ID="txtSearchContact" runat="server" CssClass="m-wrap" AutoPostBack="True" Placeholder="ชื่อผู้ติดต่อ เบอร์โทร Email Fax"></asp:TextBox>
                                                    <asp:Button ID="btnSearchSupplier" runat="server" style="display:none;" />  
									            </div>
								        </div>
                                    </div>
                                    <div class="portlet-body no-more-tables">
                                    <asp:Label ID="lblTotalSupplier" runat="server" Width="100%" Font-Size="16px" Font-Bold="True" style="text-align:center;"></asp:Label>
                                    <table class="table table-bordered table-advance gridResult">                                                                                                                                                                                                 
									    <thead>
										    <tr>											
                                                <th style="text-align:center;"><i class="icon-bookmark"></i> เลขผู้เสียภาษี</th>
											    <th style="text-align:center;"><i class="icon-user"></i> ชื่อผู้ขาย บริษัท/หจก.</th>
											    <th style="text-align:center;"><i class="icon-user"></i> ผู้ติดต่อ</th>    
                                                <th style="text-align:center;"><i class="icon-comments"></i> ข้อมูลการติดต่อ</th>                                         
										    </tr>
									    </thead>
									    <tbody>
                                            <asp:Repeater ID="rptSupplierList" runat="server">								   
                                            <ItemTemplate>                                                 
										        <tr>	
                                                    <td id="td_S1" runat="server" data-title="เลขผู้เสียภาษี" style="border-right:1px solid #eeeeee; cursor: pointer;  "><asp:Label ID="lbl_Tax" runat="server"></asp:Label></td>
											        <td id="td_S2" runat="server" data-title="ชื่อผู้ขาย บริษัท/หจก." style="border-right:1px solid #eeeeee; cursor: pointer;  "><asp:Label ID="lbl_Supplier" runat="server"></asp:Label></td>
											        <td id="td_S3" runat="server" data-title="ผู้ติดต่อ" style="border-right:1px solid #eeeeee; cursor: pointer;  "><asp:Label ID="lbl_Sales" runat="server"></asp:Label></td>
                                                    <td id="td_S4" runat="server" data-title="ข้อมูลการติดต่อ" style="border-right:1px solid #eeeeee;  cursor: pointer; "><asp:Label ID="lbl_Contact" runat="server"></asp:Label>
                                                    <asp:Button ID="btnSelect" runat="server" style="display:none;" CommandName="select" />
                                                    </td>                                                    
										        </tr>                                                       
                                            </ItemTemplate>
                                        </asp:Repeater>
								    </tbody>
							        </table>
                                    <asp:PageNavigation ID="PagerSupplier" runat="server"  PageSize="10" />
                                </div>
				        </div>				                
                        <asp:LinkButton CssClass="fancybox-item fancybox-close" ToolTip="ปิด" id="lnkCloseSupplier" runat="server"></asp:LinkButton>
			        </div>
                    <div class="modal-backdrop" style="z-index: 10049;"></div>
                    </asp:Panel>



                    <asp:Panel CssClass="modal-scrollable" id="dialogSupplierInfo" runat="server" Visible="false" style="margin-left:-450px;" >
                        <div class="modal fade in modal-overflow" style="display: block; margin-top: 30px; width:900px;">
				        <div class="modal-header">
					        <h3><i class="icon-user"></i> ข้อมูล : <asp:Label ID="lbl_S_Fullname" runat="server"></asp:Label></h3>
				        </div>
				        <div class="modal-body">                                                              
                          
                                    <div class="row-fluid form-horizontal">
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label">
                                                ชื่อย่อ</label>
                                                <div class="controls">
                                                        <asp:Label ID="lbl_S_Alias" runat="server" AutoPostBack="True" 
                                                        Font-Bold="true" Font-Size="Large" style="top:7px; position:relative;"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label">
                                                เลขผู้เสียภาษี</label>
                                                <div class="controls">
                                                    <asp:Label ID="lbl_S_Tax_No" runat="server" AutoPostBack="True" 
                                                        Font-Bold="true" Font-Size="Large" style="top:7px; position:relative;"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-fluid form-horizontal">
                                        <div class="span12 ">
                                            <div class="control-group">
                                                <label class="control-label">
                                                ที่อยู่</label>
                                                <div class="controls">
                                                    <asp:Label ID="lbl_S_Address" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-fluid form-horizontal">
                                        <!--/span-->
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label">
                                                ผู้ติดต่อ
                                                </label>
                                                <div class="controls">
                                                    <asp:Label ID="lbl_S_Contact_Name" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label">
                                                Email
                                                </label>
                                                <div class="controls">
                                                    <asp:Label ID="lbl_S_Email" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-fluid form-horizontal">
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label">
                                                โทรศัพท์</label>
                                                <div class="controls">
                                                    <asp:Label ID="lbl_S_Phone" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label">
                                                โทรสาร
                                                </label>
                                                <div class="controls">
                                                    <asp:Label ID="lbl_S_Fax" runat="server"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>

				        </div>				                
                        <asp:LinkButton CssClass="fancybox-item fancybox-close" ToolTip="ปิด" id="lnkCloseSupplierInfo" runat="server"></asp:LinkButton>
			        </div>
                    <div class="modal-backdrop" style="z-index: 10049;"></div>
                    </asp:Panel>



                    </ContentTemplate>
                </asp:UpdatePanel>
                        <asp:UpdatePanel ID="udpSelectProduct" runat="server">
                            <ContentTemplate>
                                <asp:Panel CssClass="modal-scrollable" ID="dialogProductType" runat="server" Visible="false"
                                    Style="margin-left: -450px;" DefaultButton="btnSearchProduct_dialog">
                                    <div class="modal fade in modal-overflow" style="display: block; margin-top: 30px;
                                        width: 1100px;">
                                        <div class="modal-header">
                                            <h3>
                                                <i class="icon-barcode"></i>เลือกประเภทสินค้า / พัสดุครุภัณฑ์ งานจ้างและบริการ</h3>
                                            <asp:Label ID="lblType_ID_dialog" runat="server" Style="display: none;"></asp:Label>
                                            <asp:Label ID="lblRunning_ID_dialog" runat="server" Style="display: none;"></asp:Label>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="table-toolbar">
                                                    <div class="btn-group" style="padding-left: 50px;">
                                                        <asp:Button ID="btnSearchProduct_dialog" CssClass="btn green" runat="server" Text="ค้นหา" />
                                                    </div>
                                                </div>
                                            </div>
                           
                                            <div class="row-fluid form-horizontal">
                                                <div class="control-group span6">
                                                    <label class="control-label">
                                                        กลุ่มสินค้า/บริการ</label>
                                                    <div class="controls">
                                                        <asp:DropDownList ID="ddl_CatGroup_dialog" runat="server" Font-Bold="True" CssClass="m-wrap"
                                                            AutoPostBack="True">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="control-group span6">
                                                    <label class="control-label">
                                                        ประเภท</label>
                                                    <div class="controls">
                                                        <asp:DropDownList ID="ddl_Cat_dialog" runat="server" Font-Bold="True" CssClass="m-wrap"
                                                            AutoPostBack="True">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row-fluid form-horizontal">
                                                <div class="control-group span6">
                                                    <label class="control-label">
                                                        ชนิด</label>
                                                    <div class="controls">
                                                        <asp:DropDownList ID="ddl_Sub_Cat_dialog" runat="server" Font-Bold="True" CssClass="m-wrap"
                                                            AutoPostBack="True">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="control-group span6">
                                                    <label class="control-label">
                                                        ชนิดย่อย</label>
                                                    <div class="controls">
                                                        <asp:DropDownList ID="ddl_Type_dialog" runat="server" Font-Bold="True" CssClass="m-wrap"
                                                            AutoPostBack="True">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row-fluid form-horizontal">
                                                <div class="control-group span6">
                                                    <label class="control-label">
                                                        ค้นจากชื่อ</label>
                                                    <div class="controls">
                                                        <asp:TextBox ID="txtSearchProduct_dialog" runat="server" CssClass="span11 m-wrap"
                                                            AutoPostBack="True" Placeholder="ชื่อชนิด ชนิดย่อย ประเภท รหัสครุภัณฑ์ "></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="control-group span6">
                                   
                                                </div>
                                            </div>
                                            <div class="portlet-body no-more-tables">
                                                <asp:Label ID="lblTotalProduct" runat="server" Width="100%" Font-Size="16px" Font-Bold="True"
                                                    Style="text-align: center;"></asp:Label>
                                                <div>
                                                </div>
                                                <table class="table table-bordered table-advance gridResult">
                                                    <thead>
                                                        <tr>
                                                            <th style="text-align: center;">
                                                                <i class="icon-list"></i>ประเภท
                                                            </th>
                                                            <th style="text-align: center;">
                                                                <i class="icon-list"></i>ชนิด
                                                            </th>
                                                            <th style="text-align: center;">
                                                                <i class="icon-list"></i>ชนิดย่อย
                                                            </th>
                                                            <th style="text-align: center;">
                                                                <i class="icon-list"></i>Running
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <asp:Repeater ID="rptProductTypeList" runat="server">
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td id="td1" runat="server" data-title="ประเภท" style="border-right: 1px solid #eeeeee;">
                                                                        <asp:Label ID="lbl_Cat" runat="server"></asp:Label>
                                                                    </td>
                                                                    <td id="td2" runat="server" data-title="ชนิด" style="border-right: 1px solid #eeeeee;">
                                                                        <asp:Label ID="lbl_SubCat" runat="server"></asp:Label>
                                                                    </td>
                                                                    <td id="td3" runat="server" data-title="ชนิดย่อย" style="border-right: 1px solid #eeeeee;">
                                                                        <asp:Label ID="lbl_Type" runat="server"></asp:Label>                                                        
                                                                    </td>
                                                                    <td id="td4" runat="server" data-title="Running" style="border-right: 1px solid #eeeeee;">
                                                                        <asp:Button ID="btnSelect" runat="server" Style="display: none;" CommandName="select" />
                                                                        <asp:Label ID="lbl_Running" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </tbody>
                                                </table>
                                                <asp:PageNavigation ID="PagerProduct" runat="server" PageSize="8" />
                                            </div>
                                        </div>
                                        <asp:LinkButton CssClass="fancybox-item fancybox-close" ToolTip="ปิด" ID="lnkCloseProduct"
                                            runat="server"></asp:LinkButton>
                                    </div>
                                    <div class="modal-backdrop" style="z-index: 10049;">
                                    </div>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>


</div>
    <script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>	
	<script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.js" type="text/javascript" ></script>
	<script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>
</asp:Content>

