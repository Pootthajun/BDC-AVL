﻿
Imports System.Data
Imports System.Data.SqlClient

Partial Class Setting_Dept
    Inherits System.Web.UI.Page

    Dim BL As New AVLBL

    Private ReadOnly Property PageName As String
        Get
            Return "Setting_Dept.aspx"
        End Get
    End Property
    Private Property AccessMode As AVLBL.AccessRole
        Get
            Try
                Return ViewState("AccessMode")
            Catch ex As Exception
                Return AVLBL.AccessRole.None
            End Try
        End Get
        Set(value As AVLBL.AccessRole)
            ViewState("AccessMode") = value
        End Set
    End Property
    Private ReadOnly Property User_ID As Integer
        Get
            Try
                If IsNumeric(Session("User_ID")) Then
                    Return Session("User_ID")
                Else
                    Return -1
                End If
            Catch ex As Exception
                Return -1
            End Try
        End Get
    End Property

    Public Property DEPT_ID As String
        Get
            Try
                Return lblDept_ID.Text
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(value As String)
            lblDept_ID.Text = value
        End Set
    End Property


    Public Property IsActive As Boolean
        Get
            Return imgStatus.ImageUrl = "images/check.png"
        End Get
        Set(value As Boolean)
            If value Then
                imgStatus.ImageUrl = "images/check.png"
            Else
                imgStatus.ImageUrl = "images/none.png"
            End If
        End Set
    End Property

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("User_ID")) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "ShowAlert('warning','กรุณาล็อกอินเข้าสู่ระบบ','Login.aspx');", True)
            Response.Redirect("Login.aspx")
            Exit Sub
        End If

        If Not IsPostBack Then
            SetUserRole()
            ClearData()
        End If
    End Sub

    Private Sub SetUserRole()

        '----------- Check สิทธิ์ในเมนู --------------
        Dim MT As DataTable = BL.GetUserMenuRole(User_ID, PageName)
        If MT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลนี้','Overview.aspx');", True)
            Exit Sub
        End If
        AccessMode = MT.Rows(0).Item("MR_ID") '--------- สิทธิ์ในเมนูของผู้ใช้คนปัจจุบัน -----------
        If AccessMode = AVLBL.AccessRole.None Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลนี้','Overview.aspx');", True)
            Exit Sub
        End If

        btnAdd.Visible = AccessMode = AVLBL.AccessRole.Edit
        thEdit_Header.Visible = AccessMode = AVLBL.AccessRole.Edit
    End Sub


    Protected Sub btnAdd_Click(sender As Object, e As System.EventArgs) Handles btnAdd.Click
        pnlEdit.Visible = True
        pnlData.Visible = False
        txtDept_ID.Enabled = True
        txtDept_ID.Text = ""
        txtName.Text = ""
        IsActive = True
        txtName.Attributes.Add("ID", "")
    End Sub

    Sub ClearData()
        pnlEdit.Visible = False
        pnlData.Visible = True
        txtName.Text = ""
        IsActive = True
        BindData()
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
        ClearData()
    End Sub

    Sub BindData()
        Dim SQL As String = "SELECT * FROM tb_Dept ORDER BY Dept_ID" & vbLf
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        rptData.DataSource = DT
        rptData.DataBind()
    End Sub

    Protected Sub rptData_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim lblId As Label = e.Item.FindControl("lblId")
        Dim lblName As Label = e.Item.FindControl("lblName")
        Dim imgStatus As Image = e.Item.FindControl("imgStatus")
        Dim tdEdit_List As HtmlTableCell = e.Item.FindControl("tdEdit_List")

        tdEdit_List.Visible = AccessMode = AVLBL.AccessRole.Edit
        lblId.Text = e.Item.DataItem("Dept_ID").ToString
        lblName.Text = e.Item.DataItem("Dept_Name").ToString

        If CBool(e.Item.DataItem("Active_Status")) = True Then
            imgStatus.ImageUrl = "images/check.png"
        Else
            imgStatus.ImageUrl = "images/none.png"
        End If
    End Sub

    Protected Sub rptData_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptData.ItemCommand
        Dim lblId As Label = e.Item.FindControl("lblId")

        Select Case e.CommandName
            Case "Edit"
                pnlEdit.Visible = True
                pnlData.Visible = False
                Dim lblName As Label = e.Item.FindControl("lblName")
                Dim imgStatus As Image = e.Item.FindControl("imgStatus")
                DEPT_ID = lblId.Text
                txtName.Attributes.Add("ID", lblId.Text)
                txtDept_ID.Text = lblId.Text
                txtDept_ID.Enabled = False
                txtName.Text = lblName.Text
                IsActive = imgStatus.ImageUrl = "images/check.png"
                txtName.Enabled = AccessMode = AVLBL.AccessRole.Edit
                lblValidate_Dept_ID.Visible = AccessMode = AVLBL.AccessRole.Edit
                lblValidate_txtName.Visible = AccessMode = AVLBL.AccessRole.Edit

            Case "Delete"
                'Dim Script As String = ""
                'Script &= "$('#btnRptDelete').click(function () { if(confirm('ยืนยันผลการประเมินทั้งหมด ?'))$('#btnRptDelete').click(); });" & vbLf
                'ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "StoreJQuery", Script, True)

                Dim SQL As String = ""
                For i As Integer = 0 To 5
                    If i = 0 Then
                        SQL = " SELECT Dept_ID FROM tb_User WHERE Dept_ID='" + lblId.Text + "'"
                    ElseIf i = 1 Then
                        SQL = " SELECT Dept_ID FROM tb_Cat WHERE Dept_ID='" + lblId.Text + "'"
                    ElseIf i = 2 Then
                        SQL = " SELECT Dept_ID FROM tb_Dept_Role WHERE Dept_ID='" + lblId.Text + "'"
                    ElseIf i = 3 Then
                        SQL = " SELECT Dept_ID FROM tb_Log_Delete WHERE Dept_ID='" + lblId.Text + "'"
                    ElseIf i = 4 Then
                        SQL = " SELECT Dept_ID FROM tb_Ass_Header WHERE Dept_ID='" + lblId.Text + "'"
                    ElseIf i = 5 Then
                        SQL = " SELECT Dept_ID FROM tb_ReAss_Header WHERE Dept_ID='" + lblId.Text + "'"
                    End If

                    Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
                    Dim DT As New DataTable
                    DA.Fill(DT)
                    If (DT.Rows.Count > 0) Then
                        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','ไม่สามารถลบข้อมูลได้ เนื่องจากมีการใช้งานข้อมูลนี้แล้วในระบบ','');", True)
                        Exit Sub
                    End If

                Next

                '----------ก่อนลบตรวจสอบก่อนว่า มีข้อมูลอยู่ในตาราง --------------
                Dim SQLDEL As String = ""
                SQLDEL &= "DELETE FROM tb_Dept WHERE Dept_ID = '" & lblId.Text & "'"
                Dim conn As New SqlConnection(BL.ConnectionString)
                conn.Open()
                Dim SqlCmd As SqlCommand
                SqlCmd = New SqlCommand(SQLDEL, conn)
                SqlCmd.ExecuteNonQuery()
                conn.Close()
                ClearData()

                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('success','ลบเรียบร้อย','');", True)
                Exit Sub
        End Select
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        If txtDept_ID.Text.Trim = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','กรุณากรอก รหัสสำนักงาน','');", True)
            txtDept_ID.Focus()
            Exit Sub
        End If
        If Len(txtDept_ID.Text) <> 2 Or txtDept_ID.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','กรุณากรอก รหัสสำนักงาน 2 หลัก','');", True)
            txtDept_ID.Focus()
            Exit Sub
        End If
        If txtName.Text.Trim = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','กรุณากรอก ชื่อสำนักงาน','');", True)
            txtName.Focus()
            Exit Sub
        End If

        Dim SQL As String = ""
        SQL &= "SELECT * FROM tb_Dept" & vbCrLf
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 And DEPT_ID = "" Then
            DT.DefaultView.RowFilter = "Dept_ID ='" & txtDept_ID.Text.Replace("'", "''") & "' OR Dept_Name='" & txtName.Text.Replace("'", "''") & "'"
            If (DT.DefaultView.Count > 0) Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','กรุณาตรวจสอบข้อมูลซ้ำ','');", True)
                txtDept_ID.Focus()
                Exit Sub
            End If
        End If

        Dim DR As DataRow
        'Add
        SQL = "SELECT * FROM tb_Dept WHERE Dept_ID='" & DEPT_ID & "'" & vbCrLf
        DA = New SqlDataAdapter(SQL, BL.ConnectionString)
        DT = New DataTable
        DA.Fill(DT)
        If DEPT_ID = "" Then
            DR = DT.NewRow
            DR("Dept_ID") = txtDept_ID.Text
            DR("Dept_Name") = txtName.Text
            DR("Active_Status") = IsActive
            DR("Update_By") = Session("User_ID")
            DR("Update_Time") = Now
            DT.Rows.Add(DR)
            Dim cmd As New SqlCommandBuilder(DA)
            DA.Update(DT)
        Else
            DR = DT.Rows(0)
            DR("Dept_ID") = DEPT_ID
            DR("Dept_Name") = txtName.Text
            DR("Active_Status") = IsActive
            DR("Update_By") = Session("User_ID")
            DR("Update_Time") = Now
            Dim cmd As New SqlCommandBuilder(DA)
            DA.Update(DT)
        End If
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('success','บันทึกเรียบร้อย','');", True)

        ClearData()

    End Sub

    Protected Sub imgStatus_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgStatus.Click
        IsActive = Not IsActive
    End Sub
End Class
