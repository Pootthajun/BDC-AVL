﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Setting_User.aspx.vb" Inherits="Setting_User" %>

<%@ Register src="WUC_DateReporter.ascx" tagname="WUC_DateReporter" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link type="text/css" rel="stylesheet" href="assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ToolkitScriptManager ID="toolkit1" runat="server"></asp:ToolkitScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>

    <div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12"> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3>
							ผู้ใช้ระบบ
						</h3>		
						
									
						<ul class="breadcrumb">
                           
                            <li><i class="icon-cogs"></i> <a href="javascript:;">ตั้งค่าระบบ</a><i class="icon-angle-right"></i></li>
                        	<li><i class="icon-user"></i> <a href="javascript:;">ผู้ใช้ระบบ</a></li>
                        	
                            <uc1:WUC_DateReporter ID="WUC_DateReporter1" runat="server" />
                        </ul>
						<!-- END PAGE TITLE & BREADCRUMB-->	
				     </div>				
			    </div>
                <asp:Panel ID="pnlList" runat="server" DefaultButton ="btnSearch">
               <div class="row-fluid">
               <div class="span12">
						<!-- BEGIN SAMPLE TABLE PORTLET-->
						<div class="portlet">
							

							<div class="portlet-body">
                                <div class="table-toolbar">
									<%--<div class="btn-group">
										<asp:Button ID="btnAdd_User" runat="server" CssClass="btn green" Text="เพิ่มผู้ใช้ระบบ" />
									</div>--%>
                                    <div class="btn-group">
                                        <asp:LinkButton CssClass="btn green" id="btnAdd_User" runat="server">
										    เพิ่มผู้ใช้ระบบ <i class="icon-plus"></i>
										    </asp:LinkButton>
                                    </div>
								    <div class="btn-group">
                                            <asp:LinkButton CssClass="btn blue" id="btnSearch" runat="server">
										        ค้นหา <i class="icon-search"></i>
										        </asp:LinkButton>
									    </div>
								    </div>
                                     <%--แบ่งส่วนการค้นหา--%>
                                    <fieldset>
                                        <legend><b>ค้นหา</b></legend>
                                    </fieldset>
                                <div class="row-fluid form-horizontal">
                                                        
                                                        <div class="span4 ">
														    <div class="control-group">
													            <label class="control-label"> ชื่อเข้าใช้ระบบ</label>
													            <div class="controls">
														            <asp:TextBox id="txtSearchUserName" runat="server" CssClass="m-wrap medium" placeholder="ค้นหาจากชื่อเข้าใช้ระบบ"></asp:TextBox>
														        </div>
												            </div>
													    </div>	
													    <div class="span6 ">
														    <div class="control-group">
													            <label class="control-label" style="width:180px;"> ชื่อ</label>
													            <div class="controls">
														            &nbsp; <asp:TextBox id="txtSearchFullname" runat="server" CssClass="m-wrap medium" placeholder="ค้นหาจากชื่อ" ></asp:TextBox>
														        </div>
												            </div>
													    </div>	
												          </div>
                                                          <div class="row-fluid form-horizontal">
													<div class="span4 ">
														<div class="control-group">
													        <label class="control-label"><i class="icon-lock"></i> สิทธิ์ผู้ประเมิน</label>
													        <div class="controls" >
													                     <asp:DropDownList ID="ddlSearchCreater" runat="server" CssClass="medium m-wrap" >
													                        <asp:ListItem Text="แสดงทั้งหมด"></asp:ListItem>
													                        <asp:ListItem Text="ได้"></asp:ListItem>
													                        <asp:ListItem Text="ไม่ได้"></asp:ListItem>
													                    </asp:DropDownList>
													               
													         </div>														           												            
														 </div>	
                                                         <div class="control-group">
												            <label class="control-label"><i class="icon-lock"></i> สิทธิ์ผู้ตรวจสอบ </label>
												            <div class="controls" >
									                            <asp:DropDownList ID="ddlSearchAuditor" runat="server" CssClass="medium m-wrap" >
										                            <asp:ListItem Text="แสดงทั้งหมด"></asp:ListItem>
										                            <asp:ListItem Text="ได้"></asp:ListItem>
										                            <asp:ListItem Text="ไม่ได้"></asp:ListItem>
										                        </asp:DropDownList>
												            </div>
												        </div>											       
													</div>	

													<div class="span6 ">
														<div class="control-group">
													        <label class="control-label" style="width:180px;"><i class="icon-lock"></i> สิทธิ์เจ้าหน้าที่พัสดุ</label>
													        <div class="controls" >&nbsp; 
													                     <asp:DropDownList ID="ddlSearchSuppliesUnit" runat="server" CssClass="medium m-wrap" >
													                        <asp:ListItem Text="แสดงทั้งหมด"></asp:ListItem>
													                        <asp:ListItem Text="ได้"></asp:ListItem>
													                        <asp:ListItem Text="ไม่ได้"></asp:ListItem>
													                    </asp:DropDownList>													               
													         </div>														           												            
														 </div>	
                                                         <div class="control-group">
												            <label class="control-label" style="width:180px;"><i class="icon-lock"></i> สิทธิ์หัวหน้าฝ่ายบริหาร </label>
												            <div class="controls" >&nbsp; 
									                            <asp:DropDownList ID="ddlSearchDirector" runat="server" CssClass="medium m-wrap" >
										                            <asp:ListItem Text="แสดงทั้งหมด"></asp:ListItem>
										                            <asp:ListItem Text="ได้"></asp:ListItem>
										                            <asp:ListItem Text="ไม่ได้"></asp:ListItem>
										                        </asp:DropDownList>
												            </div>
												        </div>	
                                                        <div class="control-group">
												            <label class="control-label"  style="width:180px;"><i class="icon-lock"></i> ใช้ระบบ</label>
												            <div class="controls" >&nbsp; 
									                            <asp:DropDownList ID="ddlSearchAllow" runat="server" CssClass="medium m-wrap" >
										                            <asp:ListItem Text="แสดงทั้งหมด"></asp:ListItem>
                                                                    <asp:ListItem Text="อนุญาต"></asp:ListItem>
										                            <asp:ListItem Text="ไม่อนุญาต"></asp:ListItem>
										                        </asp:DropDownList>
												            </div>
												        </div>										       
													</div>	

												     </div>


                                <div class="portlet-body no-more-tables">                                            
								<table class="table table-full-width table-advance dataTable no-more-tables table-hover">                                
									<thead>
										<tr>
											<th style="text-align:center; vertical-align:middle;">ชื่อเข้าใช้ระบบ</th>
											<th style="text-align:center; vertical-align:middle;">ชื่อ</th>
											<th style="text-align:center; vertical-align:middle;">สิทธิ์การใช้ระบบ</th>
											<th style="text-align:center; vertical-align:middle;">ผู้ประเมิน</th>
											<th style="text-align:center; vertical-align:middle;">ผู้ตรวจสอบ</th>
											<th style="text-align:center; vertical-align:middle;">เจ้าหน้าที่พัสดุ</th>
											<th style="text-align:center; vertical-align:middle;">หัวหน้าฝ่ายบริหาร</th>
											<th style="text-align:center; vertical-align:middle;">อนุญาต</th>
                                            <th  id="thEdit_Header" runat="server" style="text-align:center; vertical-align:middle;">ดำเนินการ</th>
										</tr>
									</thead>
									<tbody>
                                    <asp:Repeater ID="rptList" runat="server">
										<ItemTemplate>
										<tr>
											<td data-title="ชื่อผู้ใช้ระบบ"><asp:Label ID="lblUser_Login" runat="server"></asp:Label></td>
											<td data-title="ชื่อ-สกุล"><asp:Label ID="lblFullname" runat="server"></asp:Label></td>
											<td data-title="สิทธิ์การใช้ระบบ"  style="text-align:center; width :250px;"><asp:Label ID="lblRole_Name" runat="server"></asp:Label></td>
											<td data-title="ผู้ประเมิน" style="text-align:center;"><a ID="img_Creater" runat="server" target="_blank"></a></td>
											<td data-title="ผู้ตรวจสอบ" style="text-align:center;"><a ID="img_Auditor" runat="server" target="_blank"></a></td>
											<td data-title="เจ้าหน้าที่พัสดุ"  style="text-align:center;"><a ID="img_SuppliesUnit" runat="server" target="_blank"></a></td>
											<td data-title="หัวหน้าฝ่ายบริหาร"  style="text-align:center;"><a ID="img_Director" runat="server" target="_blank"></a></td>
											<td data-title="สถานะ"  style="text-align:center;"><a ID="img_Status" runat="server" target="_blank"></a></td>
                                            <td id="tdEdit_List" runat="server" data-title="ดำเนินการ" style="text-align:center; ">
                                                <asp:Button ID="btnEdit" runat="server" CssClass="btn mini purple" CommandName="Edit" Text="กำหนด" />
                                            </td>
										</tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
									</tbody>
								</table>


                            </div> 
                                                                                      
                                <div class="row-fluid">
                                    <br />
                                </div>                                   

							</div>
						</div>
						<!-- END SAMPLE TABLE PORTLET-->
					</div>
				</div>
                </asp:Panel>
                <asp:Panel ID="pnlEdit" runat="server"  DefaultButton="btnOK" >
                <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN VALIDATION STATES-->
						<div class="portlet box blue">
							<div class="portlet-title">
								<div class="caption"><i class="icon-user"></i>เพิ่ม/แก้ไข ข้อมูลผู้ใช้ระบบ</div>
								<div class="tools">
								</div>
							</div>
							<div class="portlet-body form form-horizontal">
                                <form class="form-horizontal" id="form_sample_2" action="#" novalidate="novalidate">

                                    <div class="control-group">
										<label class="control-label">คำนำหน้า <font color="red">*</font>&nbsp;&nbsp;</label>
										<div class="controls">
                                            <asp:Label ID="lblUser_ID" runat="server"  style="display:none;"  ></asp:Label>
											<asp:DropDownList ID="ddlPrefix" runat="server" AutoPostBack="false"  CssClass="m-wrap" style=" width:auto !important; padding:0px;"></asp:DropDownList>

										</div>
									</div>
                                    <div class="control-group">
										<label class="control-label">ชื่อ <font color="red">*</font>&nbsp;&nbsp;</label>
										<div class="controls">
											<asp:TextBox ID="txtFisrt_Name"  runat="server" class="span6 m-wrap" ></asp:TextBox>
										</div>
									</div>
                                    <div class="control-group">
										<label class="control-label">นามสกุล <font color="red">*</font>&nbsp;&nbsp;</label>
										<div class="controls">
											<asp:TextBox ID="txtLast_Name"  AutoPostBack="false"  runat="server" class="span6 m-wrap" ></asp:TextBox>
										</div>
									</div>
                                    <div class="control-group" id="divDept" runat ="server" visible ="false" >
										<label class="control-label">ฝ่าย <font color="red">*</font>&nbsp;&nbsp;</label>
										<div class="controls">
											<asp:DropDownList ID="ddlDept_Name" runat="server" AutoPostBack="false"  CssClass="m-wrap" style=" width:auto !important; padding:0px;"></asp:DropDownList>
										</div>
									</div>
                                    <div class="control-group">
										<label class="control-label">ตำแหน่ง&nbsp;&nbsp;</label>
										<div class="controls">
											<asp:TextBox ID="txtPos_Name" AutoPostBack="false"   runat="server" class="span6 m-wrap" ></asp:TextBox>
										</div>
									</div>
                                    <div class="control-group">
										<label class="control-label">ชื่อผู้ใช้ระบบ <font color="red">*</font>&nbsp;&nbsp;</label>
										<div class="controls">
											<asp:TextBox ID="txtUser_Login"  AutoPostBack="false"  runat="server" class="span6 m-wrap" ></asp:TextBox>
										</div>
									</div>
									<div class="control-group password-strength">
										<label class="control-label">รหัสผ่าน <font color="red">*</font>&nbsp;&nbsp;</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtUser_Password"  AutoPostBack="false"  runat="server" class="span6 m-wrap" ></asp:TextBox>
										</div>									
                                     </div>
									<div class="control-group">
										<label class="control-label">ยืนยันรหัสผ่าน <font color="red">*</font>&nbsp;&nbsp;</label>
										<div class="controls">
											<asp:TextBox ID="txtUser_Password_Confirm"  AutoPostBack="false"  runat="server" class="span6 m-wrap" ></asp:TextBox>
									    </div>
									</div>

                                    <asp:Panel ID="pnlEdit_btnOK" runat="server" >
                                    <%--<div class="form-actions">
                                        <asp:Button ID="btnOK" runat="server" CssClass="btn blue" Text="บันทึข้อมูลกผู้ใช้"    />
									</div>--%>
                                    </asp:Panel>


<%--                                    <div class="control-group">
                                    <label class="control-label">
                                        สิทธิ์การใช้งาน &nbsp;&nbsp;</label>
                                    <div class="controls">
                                        <label class="checkbox line">
                                            <div style="display: inline;">
                                                <input type="checkbox" value="" checked></div>
                                            ผู้ดูแลระบบ
                                        </label>
                                        <label class="checkbox line">
                                            <div style="display: inline;">
                                                <input type="checkbox" value=""></div>
                                            พนักงาน
                                        </label>
                                    </div>
                                </div>
--%>                                    
<%--                                <div class="control-group">
										<label class="control-label">สิทธิ์การใช้งาน &nbsp;&nbsp;</label>
										<div class="controls">
											<label class="radio">
											<div class="radio"><span class=""><input type="radio" value="option1" name="optionsRadios1"></span></div>
											ผู้ดูแลระบบ
											</label>
											<label class="radio">
											<div class="radio"><span><input type="radio" checked="" value="option2" name="optionsRadios1"></span></div>
											พนักงาน
											</label>  
											 
										</div>
									</div>
--%>

                                    <asp:Panel ID="pnlEdit_Role" runat="server" >
									
                                    <div class="control-group">
										<label class="control-label" style="font-weight:bold;" >สิทธิ์การประเมิน&nbsp;&nbsp;</label>
										<div class="controls">
								            <table class="table table-full-width table-advance dataTable no-more-tables table-hover inline "  style ="margin-top: -20px;"> 
                                                                           
									            <thead>
										            <tr>
											            <th style="vertical-align:middle; text-align :center ; ">สำนักงาน&nbsp;</th>
											            <th style="vertical-align:middle; text-align :center ; ">ฝ่าย/ภาค/งาน&nbsp;</th>
											            <th style="vertical-align:middle; text-align :center ; "><asp:ImageButton ID="btnAll_Creater" runat="server"  ImageUrl="images/none.png" CommandName ="Creater" ToolTip="Click เพื่อเปลี่ยน" /> <br />ผู้ประเมิน</th>
											            <th style="vertical-align:middle; text-align :center ; "><asp:ImageButton ID="btnAll_Auditor" runat="server" ImageUrl="images/none.png"  CommandName ="Auditor" ToolTip="Click เพื่อเปลี่ยน" /> <br />ผู้ตรวจสอบ</th>
                                                        <th style="vertical-align:middle; text-align :center ; "><asp:ImageButton ID="btnAll_SuppliesUnit" runat="server" ImageUrl="images/none.png"  CommandName ="SuppliesUnit" ToolTip="Click เพื่อเปลี่ยน" /> <br />เจ้าหน้าที่พัสดุ</th>
                                                        <th style="vertical-align:middle; text-align :center ; "><asp:ImageButton ID="btnAll_Director" runat="server" ImageUrl="images/none.png"  CommandName ="Director" ToolTip="Click เพื่อเปลี่ยน" /> <br />หัวหน้าฝ่ายบริหาร</th>
											            <th style="vertical-align:middle; text-align :center ; "><asp:ImageButton ID="btnAll_NoneRole" runat="server" ImageUrl="images/none.png"  CommandName ="NoneRole" ToolTip="Click เพื่อเปลี่ยน" /> <br />ไม่มีสิทธิ์</th>
										            </tr>
									            </thead>
									            <tbody>
                                                <asp:Repeater ID="rptDept_Role" runat="server">
										            <ItemTemplate>
										            <tr>
											            <td data-title="สำนักงาน"  style=" background-color:white;" id="tdDept" runat ="server" ><asp:Label ID="lblDept_Name" runat="server"></asp:Label></td>
											            <td data-title="ฝ่าย/ภาค/งาน" style=" background-color:white; "><asp:Label ID="lblSub_Dept_Name" runat="server"></asp:Label></td>
											            <td data-title="ผู้จัดทำ"  style="background-color:white; text-align :center ;"><asp:ImageButton ID="imgCreater"  runat="server" ImageUrl="images/none.png"  CommandName="Creater" ToolTip="Click เพื่อเปลี่ยน" /></td>
											            <td data-title="ผู้ตรวจสอบ"  style="background-color:white; text-align :center ;"><asp:ImageButton ID="imgAuditor" runat="server" ImageUrl="images/none.png" CommandName="Auditor" ToolTip="Click เพื่อเปลี่ยน" /></td>
											            <td data-title="พัสดุ"  style="background-color:white; text-align :center ;"><asp:ImageButton ID="imgSuppliesUnit" runat="server" ImageUrl="images/none.png" CommandName="SuppliesUnit" ToolTip="Click เพื่อเปลี่ยน" /></td>
                                                        <td data-title="หัวหน้าฝ่ายบริหาร"  style="background-color:white; text-align :center ;"><asp:ImageButton ID="imgDirector" runat="server" ImageUrl="images/none.png" CommandName="Director" ToolTip="Click เพื่อเปลี่ยน" /></td>
                                                        <td data-title="ไม่มีสิทธิ์"  style="background-color:white; text-align :center ;"><asp:ImageButton ID="imgNoneRole" runat="server" ImageUrl="images/none.png" CommandName="NoneRole" ToolTip="Click เพื่อเปลี่ยน" /></td>
											        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
									            </tbody>
								            </table>
                                                
										</div>
									</div> 
                                    <div class="control-group">
										<label class="control-label"  style="font-weight:bold;" >สิทธิ์ยกเลิกใบประเมิน&nbsp;&nbsp;</label>
										<div class="controls">
                                                <asp:ImageButton ID="imgCancel_Role" runat="server" ImageUrl="images/none.png" ToolTip="Click เพื่อเปลี่ยน" />
										</div>
									</div> 
                                    <div class="control-group form-horizontal">
										<label class="control-label" style="font-weight:bold;">สิทธิ์การใช้เมนู&nbsp;&nbsp;</label>
										<div class="controls">
								            <table class="table table-full-width table-advance dataTable no-more-tables table-hover inline span8">                                
									            <thead>
										            <tr>
											            <th style="text-align:center; vertical-align:middle;">เมนู</th>
											            <th style="text-align:center; vertical-align:middle;"><asp:ImageButton ID="btnAll_View" runat="server" ImageUrl="images/none.png" CommandName ="View" ToolTip="Click เพื่อเปลี่ยนทั้งหมด" />แสดง</th>
											            <th style="text-align:center; vertical-align:middle;"><asp:ImageButton ID="btnAll_Edit" runat="server" ImageUrl="images/none.png" CommandName ="Edit" ToolTip="Click เพื่อเปลี่ยนทั้งหมด" />แก้ไข</th>
											            <th style="text-align:center; vertical-align:middle;"><asp:ImageButton ID="btnAll_None" runat="server" ImageUrl="images/none.png" CommandName ="None" ToolTip="Click เพื่อเปลี่ยนทั้งหมด" />ทำไม่ได้</th>
										            </tr>
									            </thead>
									            <tbody>
                                                <asp:Repeater ID="rptMenu_Role" runat="server">
										            <ItemTemplate>
										            <tr>
											            <td data-title="เมนู"><asp:Label ID="lblMenu_Name" runat="server"></asp:Label></td>
											            <td data-title="แสดง"  style="text-align:center;"><asp:ImageButton ID="imgView"  runat="server" ImageUrl="images/check.png"   CommandName="View" ToolTip="Click เพื่อเปลี่ยน" /></td>
											            <td data-title="แก้ไข"  style="text-align:center;"><asp:ImageButton ID="imgEdit"  runat="server" ImageUrl="images/check.png"   CommandName="Edit" ToolTip="Click เพื่อเปลี่ยน" /></td>
											            <td data-title="ทำไม่ได้"  style="text-align:center;"><asp:ImageButton ID="imgNoneRole"  runat="server" ImageUrl="images/check.png"   CommandName="NoneRole" ToolTip="Click เพื่อเปลี่ยน" /></td>
											         </tr>
                                                     </ItemTemplate>
                                                </asp:Repeater>
									            </tbody>
								            </table>
                                                
										</div>
									</div> 
                                    <div class="control-group">
										<label class="control-label" >อนุญาตใช้ระบบ&nbsp;&nbsp;</label>
										<div class="controls">
                                                <asp:ImageButton ID="imgStatus" runat="server" ImageUrl="images/check.png" ToolTip="Click เพื่อเปลี่ยน" />
										</div>
									</div> 
                                    </asp:Panel>

									<div class="form-actions">
                                        <%--<asp:Button ID="btnOK" runat="server" CssClass="btn blue" Text="บันทึก"  Visible ="false"  />--%>
                                        <asp:Button ID="btnOK" runat="server" CssClass="btn blue" Text="บันทึกข้อมูลผู้ใช้"    />
                                        <asp:Button ID="btnCancel" runat="server" CssClass="btn" Text="ย้อนกลับ" />
                                        <%--<asp:Button ID="btnDelete" runat="server" CssClass="btn red" Text="ลบผู้ใช้" />
                                        <asp:ConfirmButtonExtender ID="btnDelete_Confirm" runat="server" ConfirmText="ลบประเภทสินค้านี้ทั้งหมด?" TargetControlID="btnDelete"></asp:ConfirmButtonExtender>
										--%>
									</div>
								</form>
								<!-- END FORM-->
							</div>
						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>
                </asp:Panel>
    </div>

        </ContentTemplate>
    </asp:UpdatePanel>
    <script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>	
	<script src="assets/plugins/bootstrap-switch/static/js/bootstrap-switch.js" type="text/javascript" ></script>
	<script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>

</asp:Content>

