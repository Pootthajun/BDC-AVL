﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Login
    Inherits System.Web.UI.Page

    Dim BL As New AVLBL

    Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogin.Click

        Dim UInfo As AVLBL.UserInfo = BL.GetUserInfo(txtUserName.Text, txtPassword.Text)
        If UInfo.User_ID = -1 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Login", "ShowAlert('warning','ชื่อผู้ใช้และรหัสผ่านไม่ถูกต้อง','');", True)
            Exit Sub
        End If

        Session("User_ID") = UInfo.User_ID
        Session("User_Login") = UInfo.User_Login
        Session("Title") = UInfo.Prefix_Name
        Session("Fisrt_Name") = UInfo.Fisrt_Name
        Session("Last_Name") = UInfo.Last_Name
        Session("Pos_Name") = UInfo.Pos_Name

        Response.Redirect("Overview.aspx")
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        'Dim SQL As String = ""

        'Session("User_ID") = 0
        'Session("User_Login") = "TIT"
        'Session("Title") = "TIT"
        'Session("Fisrt_Name") = "Enterprise"
        'Session("Last_Name") = "Development"
        'Session("Pos_Name") = "Implementer"

    End Sub

End Class
