﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Setting_Sub_Dept
    Inherits System.Web.UI.Page

    Dim BL As New AVLBL

    Private ReadOnly Property PageName As String
        Get
            Return "Setting_Sub_Dept.aspx"
        End Get
    End Property
    Private Property AccessMode As AVLBL.AccessRole
        Get
            Try
                Return ViewState("AccessMode")
            Catch ex As Exception
                Return AVLBL.AccessRole.None
            End Try
        End Get
        Set(value As AVLBL.AccessRole)
            ViewState("AccessMode") = value
        End Set
    End Property
    Private ReadOnly Property User_ID As Integer
        Get
            Try
                If IsNumeric(Session("User_ID")) Then
                    Return Session("User_ID")
                Else
                    Return -1
                End If
            Catch ex As Exception
                Return -1
            End Try
        End Get
    End Property


    Public Property IsActive As Boolean
        Get
            Return imgStatus.ImageUrl = "images/check.png"
        End Get
        Set(value As Boolean)
            If value Then
                imgStatus.ImageUrl = "images/check.png"
            Else
                imgStatus.ImageUrl = "images/none.png"
            End If
        End Set
    End Property


    Public ReadOnly Property Search_DEPT_ID As String
        Get
            Return ddlDept_Search.Items(ddlDept_Search.SelectedIndex).Value
        End Get
    End Property

    Public Property Old_Dept_ID As String
        Get
            Return lblDept_ID_Edit.Attributes("Old_Dept_ID")
        End Get
        Set(value As String)
            lblDept_ID_Edit.Attributes("Old_Dept_ID") = value
        End Set
    End Property

    Public Property Dept_ID As String
        Get
            Return (lblDept_ID_Edit.Text)
        End Get
        Set(value As String)
            lblDept_ID_Edit.Text = value
        End Set
    End Property

    Public Property Old_Sub_Dept_ID As String
        Get
            Return lblSubDept_ID_Edit.Attributes("Old_Sub_Dept_ID")
        End Get
        Set(value As String)
            lblSubDept_ID_Edit.Attributes("Old_Sub_Dept_ID") = value
        End Set
    End Property

    Public Property Sub_Dept_ID As String
        Get
            Return (lblSubDept_ID_Edit.Text)
        End Get
        Set(value As String)
            lblSubDept_ID_Edit.Text = value
        End Set
    End Property

    Public Property Old_Sub_Dept_Name As String
        Get
            Return lblSubDept_ID_Edit.Attributes("Old_Sub_Dept_Name")
        End Get
        Set(value As String)
            lblSubDept_ID_Edit.Attributes("Old_Sub_Dept_Name") = value
        End Set
    End Property

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("User_ID")) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "ShowAlert('warning','กรุณาล็อกอินเข้าสู่ระบบ','Login.aspx');", True)
            Response.Redirect("Login.aspx")
            Exit Sub
        End If

        If Not IsPostBack Then
            SetUserRole()
            BindList()
            ClearForm()
        End If
    End Sub

    Private Sub SetUserRole()

        '----------- Check สิทธิ์ในเมนู --------------
        Dim MT As DataTable = BL.GetUserMenuRole(User_ID, PageName)
        If MT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลนี้','Overview.aspx');", True)
            Exit Sub
        End If
        AccessMode = MT.Rows(0).Item("MR_ID") '--------- สิทธิ์ในเมนูของผู้ใช้คนปัจจุบัน -----------
        If AccessMode = AVLBL.AccessRole.None Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลนี้','Overview.aspx');", True)
            Exit Sub
        End If

        btnAdd.Visible = AccessMode = AVLBL.AccessRole.Edit
        thEdit_Header.Visible = AccessMode = AVLBL.AccessRole.Edit
    End Sub


    Private Sub ClearForm()
        BL.BindDDlDeptALL(ddlDept_Search)
        pnlList.Visible = True
        pnlEdit.Visible = False
    End Sub

#Region "rptList"
    Private Sub BindList()
        Dim SQL As String = " "
        SQL &= " SELECT tb_Dept.Dept_ID ,tb_Dept.Dept_Name , tb_Sub_Dept.Sub_Dept_ID ,tb_Sub_Dept.Sub_Dept_Name ,tb_Sub_Dept.Active_Status " & vbLf
        SQL &= " FROM tb_Sub_Dept " & vbLf
        SQL &= " LEFT JOIN tb_Dept ON tb_Dept.Dept_ID = tb_Sub_Dept.Dept_ID WHERE tb_Dept.Active_Status=1" & vbLf
        Dim Filter As String = ""

        If txt_Search_Name.Text <> "" Then
            Filter &= "((tb_Sub_Dept.Sub_Dept_Name LIKE '%" & txt_Search_Name.Text.Replace("'", "''") & "%') OR "
            Filter &= "(tb_Sub_Dept.Sub_Dept_ID LIKE '%" & txt_Search_Name.Text.Replace("'", "''") & "%')) AND "
        End If
        If ddlDept_Search.SelectedIndex > 0 Then
            Filter &= "(tb_Dept.Dept_ID = '" & ddlDept_Search.SelectedValue.Replace("'", "''") & "') AND "
        End If

        Select Case ddlStatus.SelectedIndex
            Case 1
                Filter &= "tb_Sub_Dept.Active_Status > 0 AND "
            Case 2
                Filter &= "tb_Sub_Dept.Active_Status = 0 AND "
        End Select

        If Filter <> "" Then
            SQL &= " AND " & Filter.Substring(0, Filter.Length - 4) & vbLf
        End If

        SQL &= " ORDER BY tb_Dept.Dept_ID,tb_Sub_Dept.Sub_Dept_ID" & vbLf

        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        lblTotalList.Text = "พบ " & FormatNumber(DT.Rows.Count, 0) & " เรคอร์ด"
        LastDept = ""
        rptList.DataSource = DT
        rptList.DataBind()

    End Sub

    Protected Sub rptList_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptList.ItemCommand

        Select Case e.CommandName
            Case "Edit"
                Dim lblDept_Name As Label = e.Item.FindControl("lblDept_Name")
                Dim lblDept_ID As Label = e.Item.FindControl("lblDept_ID")
                Dim lblSub_Dept_Name As Label = e.Item.FindControl("lblSub_Dept_Name")
                Dim lblSub_Dept_ID As Label = e.Item.FindControl("lblSub_Dept_ID")
                Dim img_Status As HtmlAnchor = e.Item.FindControl("img_Status")
                Dim btnEdit As Button = e.Item.FindControl("btnEdit")

                Dept_ID = lblDept_ID.Text
                Sub_Dept_ID = lblSub_Dept_ID.Text
                BL.BindDDlDeptALL(ddlDept_Edit, Dept_ID.ToString())
                ddlDept_Edit.Enabled = False
                txtSub_Dept_ID.Text = Sub_Dept_ID
                txtSub_Dept_ID.Enabled = False
                txtSub_Dept_Name.Text = lblSub_Dept_Name.Attributes("Sub_Dept_Name").ToString
                IsActive = img_Status.Attributes("Active_Status")
                '--------บทบาทการประเมิน/เมนู----------

                pnlList.Visible = False
                pnlEdit.Visible = True

                '---------ข้อมูลเดิมก่อนแก้ไข------------------------
                Old_Dept_ID = lblDept_ID.Text
                Old_Sub_Dept_ID = lblSub_Dept_ID.Text
                Old_Sub_Dept_Name = lblSub_Dept_Name.Attributes("Sub_Dept_Name").ToString
            Case "Delete"

                Dim lblDept_ID As Label = e.Item.FindControl("lblDept_ID")
                Dim lblSub_Dept_ID As Label = e.Item.FindControl("lblSub_Dept_ID")

                Dept_ID = lblDept_ID.Text
                Sub_Dept_ID = lblSub_Dept_ID.Text

                Dim Sql As String = ""

                For i As Integer = 0 To 3
                    If i = 0 Then
                        Sql = " SELECT * FROM tb_Ass_Header WHERE  Sub_Dept_ID='" + Sub_Dept_ID + "' AND Dept_ID ='" + Dept_ID + "' "
                    ElseIf i = 1 Then
                        Sql = " SELECT * FROM tb_ReAss_Header WHERE  Sub_Dept_ID='" + Sub_Dept_ID + "' AND Dept_ID ='" + Dept_ID + "' "

                    ElseIf i = 2 Then
                        Sql = " SELECT Dept_ID FROM tb_Dept_Role WHERE  Sub_Dept_ID='" + Sub_Dept_ID + "' AND Dept_ID ='" + Dept_ID + "' "
                    ElseIf i = 3 Then
                        Sql = " SELECT Dept_ID FROM tb_Log_Delete WHERE  Sub_Dept_ID='" + Sub_Dept_ID + "' AND Dept_ID ='" + Dept_ID + "' "
                    End If

                    Dim DA As New SqlDataAdapter(Sql, BL.ConnectionString)
                    Dim DT As New DataTable
                    DA.Fill(DT)
                    If (DT.Rows.Count > 0) Then
                        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','ไม่สามารถลบข้อมูลได้ เนื่องจากมีการใช้งานข้อมูลนี้แล้วในระบบ','');", True)
                        Exit Sub
                    End If

                Next

                Dim SqlDEL As String = ""
                SqlDEL &= " DELETE FROM tb_Sub_Dept WHERE Dept_ID='" & Dept_ID & "' AND Sub_Dept_ID='" & Sub_Dept_ID & "'" & vbLf
                Dim conn As New SqlConnection(BL.ConnectionString)
                conn.Open()
                Dim SqlCmd As SqlCommand
                SqlCmd = New SqlCommand(SqlDEL, conn)
                SqlCmd.ExecuteNonQuery()
                conn.Close()
                BindList()

                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('success','ลบเรียบร้อย','');", True)
                Exit Sub
        End Select

    End Sub

    Dim LastDept As String = ""
    Protected Sub rptList_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptList.ItemDataBound

        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblDept_Name As Label = e.Item.FindControl("lblDept_Name")
        Dim lblDept_ID As Label = e.Item.FindControl("lblDept_ID")
        Dim lblSub_Dept_Name As Label = e.Item.FindControl("lblSub_Dept_Name")
        Dim lblSub_Dept_ID As Label = e.Item.FindControl("lblSub_Dept_ID")
        Dim img_Status As HtmlAnchor = e.Item.FindControl("img_Status")
        Dim btnEdit As Button = e.Item.FindControl("btnEdit")
        Dim tdDept As HtmlTableCell = e.Item.FindControl("tdDept")
        Dim tdEdit_List As HtmlTableCell = e.Item.FindControl("tdEdit_List")

        tdEdit_List.Visible = AccessMode = AVLBL.AccessRole.Edit
        Dim DuplicatedStyleTop As String = "border-top:none;"
        If (e.Item.DataItem("Dept_ID").ToString & ": " & e.Item.DataItem("Dept_Name").ToString) <> LastDept Then
            LastDept = e.Item.DataItem("Dept_ID").ToString & ": " & e.Item.DataItem("Dept_Name").ToString
            lblDept_Name.Text = LastDept
        Else
            lblDept_Name.Text = ""
            tdDept.Attributes("style") &= DuplicatedStyleTop
        End If

        lblDept_ID.Text = e.Item.DataItem("Dept_ID").ToString
        lblSub_Dept_Name.Text = e.Item.DataItem("Sub_Dept_ID").ToString & ": " & e.Item.DataItem("Sub_Dept_Name").ToString
        lblSub_Dept_Name.Attributes("Sub_Dept_Name") = e.Item.DataItem("Sub_Dept_Name").ToString
        lblSub_Dept_ID.Text = e.Item.DataItem("Sub_Dept_ID").ToString
        If e.Item.DataItem("Active_Status") Then
            img_Status.InnerHtml = "<img src='images/check.png' />"
            img_Status.Title = "แสดงในระบบ"
        Else
            img_Status.InnerHtml = "<img src='images/none.png' />"
            img_Status.Title = "ไม่แสดงในระบบ"
        End If
        img_Status.Attributes("Active_Status") = e.Item.DataItem("Active_Status")
    End Sub


#End Region


    Protected Sub btnAdd_Click(sender As Object, e As System.EventArgs) Handles btnAdd.Click
        Sub_Dept_ID = ""
        'BL.BindDDlDEPT(ddlDept_Edit, Session("User_ID"))
        BL.BindDDlDeptALL(ddlDept_Edit)
        ddlDept_Edit.Enabled = True
        txtSub_Dept_ID.Text = ""
        txtSub_Dept_ID.Enabled = True
        txtSub_Dept_Name.Text = ""
        pnlList.Visible = False
        pnlEdit.Visible = True
        IsActive = True
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        If ddlDept_Edit.SelectedIndex <= 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','กรุณาเลือกสำนักงาน','');", True)
            ddlDept_Edit.Focus()
            Exit Sub
        End If

        If Len(txtSub_Dept_ID.Text) <> 2 Or txtSub_Dept_ID.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','กรุณากรอกข้อมูลรหัสฝ่าย/ภาค/งาน 2 หลัก','');", True)
            txtSub_Dept_Name.Focus()
            Exit Sub
        End If

        If txtSub_Dept_Name.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','กรุณากรอกข้อมูล ฝ่าย/ภาค/งาน','');", True)
            txtSub_Dept_Name.Focus()
            Exit Sub
        End If
        Dim Sql As String = ""
        Dim DR As DataRow
         Sql = "SELECT * FROM tb_Sub_Dept WHERE Dept_ID='" & ddlDept_Edit.SelectedValue & "' AND  Sub_Dept_ID ='" & txtSub_Dept_ID.Text.Replace("'", "''") & "'"
        Dim DA As New SqlDataAdapter(Sql, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
        Else
            DR = DT.Rows(0)
        End If
        DR("Dept_ID") = ddlDept_Edit.SelectedValue
        DR("Sub_Dept_ID") = txtSub_Dept_ID.Text
        DR("Sub_Dept_Name") = txtSub_Dept_Name.Text
        DR("ACTIVE_STATUS") = IsActive
        DR("Update_By") = Session("User_ID")
        DR("Update_Time") = Now
        If DT.Rows.Count = 0 Then DT.Rows.Add(DR)
        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('success','บันทึกเรียบร้อย','');", True)

        BindList()
        pnlList.Visible = True
        pnlEdit.Visible = False

    End Sub

    Protected Sub imgStatus_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgStatus.Click
        IsActive = Not IsActive
    End Sub
    Protected Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
        pnlList.Visible = True
        pnlEdit.Visible = False
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As System.EventArgs) Handles btnSearch.Click, ddlDept_Search.SelectedIndexChanged, txt_Search_Name.TextChanged, ddlStatus.SelectedIndexChanged
        BindList()
    End Sub

    Protected Sub txtSub_Dept_ID_TextChanged(sender As Object, e As System.EventArgs) Handles txtSub_Dept_ID.TextChanged, txtSub_Dept_Name.TextChanged, ddlDept_Edit.SelectedIndexChanged

        Dim Sql As String = ""
        Dim DA As New SqlDataAdapter
        Dim DT As New DataTable
        If Old_Sub_Dept_ID <> txtSub_Dept_ID.Text Then
            Sql = "SELECT * FROM tb_Sub_Dept WHERE Dept_ID='" & ddlDept_Edit.SelectedValue & "'  AND  Sub_Dept_ID ='" & txtSub_Dept_ID.Text.Replace("'", "''") & "'"
            DA = New SqlDataAdapter(Sql, BL.ConnectionString)
            DT = New DataTable
            DA.Fill(DT)
            If DT.Rows.Count > 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','ชื่อฝ่าย/ภาค/งาน ซ้ำกับข้อมูลในระบบ','');", True)
                txtSub_Dept_Name.Focus()
                Exit Sub
            End If
        End If


        If Old_Sub_Dept_Name <> txtSub_Dept_Name.Text Then

            Sql = "SELECT * FROM tb_Sub_Dept WHERE Dept_ID='" & ddlDept_Edit.SelectedValue & "' AND  Sub_Dept_Name ='" & txtSub_Dept_Name.Text.Replace("'", "''") & "'"
            DA = New SqlDataAdapter(Sql, BL.ConnectionString)
            DT = New DataTable
            DA.Fill(DT)
            If DT.Rows.Count > 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','ชื่อฝ่าย/ภาค/งาน ซ้ำกับข้อมูลในระบบ','');", True)
                txtSub_Dept_Name.Focus()
                Exit Sub
            End If
        End If

    End Sub
End Class
