var UISliders = function () {

    return {
        //main function to initiate the module
        initSliders: function () {
            // basic
            $(".slider-basic").slider(); // basic sliders

            // snap inc
            $("#slider-snap-inc").slider({
                value: 100,
                min: 0,
                max: 1000,
                step: 100,
                slide: function (event, ui) {
                    $("#slider-snap-inc-amount").text("$" + ui.value);
                }
            });
            $("#slider-snap-inc-amount").text("$" + $("#slider-snap-inc").slider("value"));

            // range slider
            $("#slider-range").slider({
                range: true,
                min: 0,
                max: 500,
                values: [75, 300],
                slide: function (event, ui) {
                    $("#slider-range-amount").text("$" + ui.values[0] + " - $" + ui.values[1]);
                }
            });

            $("#slider-range-amount").text("$" + $("#slider-range").slider("values", 0) + " - $" + $("#slider-range").slider("values", 1));

            //range max

            $("#slider-range-max").slider({
                range: "max",
                min: 1,
                max: 10,
                value: 2,
                slide: function (event, ui) {
                    $("#slider-range-max-amount").text(ui.value);
                }
            });

            $("#slider-range-max-amount").text($("#slider-range-max").slider("value"));

            //range max No1
            $("#slider-range-max_1").slider({
                range: "max",
                min: 1,
                max: 20,
                value: 15,
                slide: function (event, ui) {
                    $("#slider-range-max-amount_1").text(ui.value);
                }
            });

            $("#slider-range-max-amount_1").text($("#slider-range-max_1").slider("value"));

            //range max No2
            $("#slider-range-max_2").slider({
                range: "max",
                min: 1,
                max: 10,
                value: 5,
                slide: function (event, ui) {
                    $("#slider-range-max-amount_2").text(ui.value);
                }
            });

            $("#slider-range-max-amount_2").text($("#slider-range-max_2").slider("value"));

            //range max No3
            $("#slider-range-max_3").slider({
                range: "max",
                min: 1,
                max: 10,
                value: 7,
                slide: function (event, ui) {
                    $("#slider-range-max-amount_3").text(ui.value);
                }
            });

            $("#slider-range-max-amount_3").text($("#slider-range-max_3").slider("value"));


            //range max No4
            $("#slider-range-max_4").slider({
                range: "max",
                min: 1,
                max: 10,
                value: 4,
                slide: function (event, ui) {
                    $("#slider-range-max-amount_4").text(ui.value);
                }
            });

            $("#slider-range-max-amount_4").text($("#slider-range-max_4").slider("value"));

            //range max No5
            $("#slider-range-max_5").slider({
                range: "max",
                min: 1,
                max: 10,
                value: 8,
                slide: function (event, ui) {
                    $("#slider-range-max-amount_5").text(ui.value);
                }
            });

            $("#slider-range-max-amount_5").text($("#slider-range-max_5").slider("value"));


            //range max No6
            $("#slider-range-max_6").slider({
                range: "max",
                min: 1,
                max: 10,
                value: 10,
                slide: function (event, ui) {
                    $("#slider-range-max-amount_6").text(ui.value);
                }
            });

            $("#slider-range-max-amount_6").text($("#slider-range-max_6").slider("value"));

            //range max No7
            $("#slider-range-max_7").slider({
                range: "max",
                min: 1,
                max: 10,
                value: 1,
                slide: function (event, ui) {
                    $("#slider-range-max-amount_7").text(ui.value);
                }
            });

            $("#slider-range-max-amount_7").text($("#slider-range-max_7").slider("value"));

            

            /*range max No8
            $("#slider-range-max_8").slider({
                range: "max",
                min: 1,
                max: 10,
                value: 1,
                slide: function (event, ui) {
                    $("#slider-range-max-amount_8").text(ui.value);
                }
            });

            $("#slider-range-max-amount_8").text($("#slider-range-max_8").slider("value"));*/

            //range max No9
            $("#slider-range-max_9").slider({
                range: "max",
                min: 1,
                max: 20,
                value: 1,
                slide: function (event, ui) {
                    $("#slider-range-max-amount_9").text(ui.value);
                }
            });

            $("#slider-range-max-amount_9").text($("#slider-range-max_9").slider("value"));

            //range max NoSUM
            $("#slider-range-max_Sum").slider({
                range: "max",
                min: 1,
                max: 100,
                value: 49,
                slide: function (event, ui) {
                    $("#slider-range-max-amount_Sum").text(ui.value);
                }
            });

            $("#slider-range-max-amount_Sum").text($("#slider-range-max_Sum").slider("value"));


            //range max NoSUMReass
            $("#slider-range-max_SUMReass").slider({
                range: "max",
                min: 1,
                max: 100,
                value: 41,
                slide: function (event, ui) {
                    $("#slider-range-max-amount_SUMReass").text(ui.value);
                }
            });

            $("#slider-range-max-amount_SUMReass").text($("#slider-range-max_SUMReass").slider("value"));


            // range min
            $("#slider-range-min").slider({
                range: "min",
                value: 37,
                min: 1,
                max: 700,
                slide: function (event, ui) {
                    $("#slider-range-min-amount").text("$" + ui.value);
                }
            });

            $("#slider-range-min-amount").text("$" + $("#slider-range-min").slider("value"));

            // 
            // setup graphic EQ
            $("#slider-eq > span").each(function () {
                // read initial values from markup and remove that
                var value = parseInt($(this).text(), 10);
                $(this).empty().slider({
                    value: value,
                    range: "min",
                    animate: true,
                    orientation: "vertical"
                });
            });

            // vertical slider
            $("#slider-vertical").slider({
                orientation: "vertical",
                range: "min",
                min: 0,
                max: 100,
                value: 60,
                slide: function (event, ui) {
                    $("#slider-vertical-amount").text(ui.value);
                }
            });
            $("#slider-vertical-amount").text($("#slider-vertical").slider("value"));

            // vertical range sliders
            $("#slider-range-vertical").slider({
                orientation: "vertical",
                range: true,
                values: [17, 67],
                slide: function (event, ui) {
                    $("#slider-range-vertical-amount").text("$" + ui.values[0] + " - $" + ui.values[1]);
                }
            });

            $("#slider-range-vertical-amount").text("$" + $("#slider-range-vertical").slider("values", 0) + " - $" + $("#slider-range-vertical").slider("values", 1));

        },

        initKnowElements: function () {
            //knob does not support ie8 so skip it
            if (!jQuery().knob || App.isIE8()) {
                return;
            }

            $(".knob").knob({
                'dynamicDraw': true,
                'thickness': 0.2,
                'tickColorizeValues': true,
                'skin': 'tron'
            });

            if ($(".knobify").size() > 0) {
                $(".knobify").knob({
                    readOnly: true,
                    skin: "tron",
                    'width': 100,
                    'height': 100,
                    'dynamicDraw': true,
                    'thickness': 0.2,
                    'tickColorizeValues': true,
                    'skin': 'tron',
                    draw: function () {
                        // "tron" case
                        if (this.$.data('skin') == 'tron') {

                            var a = this.angle(this.cv) // Angle
                                ,
                                sa = this.startAngle // Previous start angle
                                ,
                                sat = this.startAngle // Start angle
                                ,
                                ea // Previous end angle
                                ,
                                eat = sat + a // End angle
                                ,
                                r = 1;

                            this.g.lineWidth = this.lineWidth;

                            this.o.cursor && (sat = eat - 0.3) && (eat = eat + 0.3);

                            if (this.o.displayPrevious) {
                                ea = this.startAngle + this.angle(this.v);
                                this.o.cursor && (sa = ea - 0.3) && (ea = ea + 0.3);
                                this.g.beginPath();
                                this.g.strokeStyle = this.pColor;
                                this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
                                this.g.stroke();
                            }

                            this.g.beginPath();
                            this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
                            this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
                            this.g.stroke();

                            this.g.lineWidth = 2;
                            this.g.beginPath();
                            this.g.strokeStyle = this.o.fgColor;
                            this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
                            this.g.stroke();

                            return false;

                        }
                    }
                });
            }
        }

    };

}();