﻿
function browseFile() {
    $("#ctl00_ContentPlaceHolder1_ful").click();
}

function asyncUpload() {
    var ful = $("#ctl00_ContentPlaceHolder1_ful");
    var filename = ful.val();
    var filecontent = ful.prop("files")[0];
    if (filecontent.size > (4 * 1024 * 1024)) {
        alert("ไฟล์ที่อัพโหลดต้องไม่เกิน 4 Mb");
        return;
    }
    if (!inFileNameValid(filename)) return;

    var formData = new FormData();
    formData.append("filecontent", filecontent);
    sendPostDataToURL("Assessment_Upload.aspx", formData);
}

function sendPostDataToURL(url, formData) {
    $.ajax({
        url: url,
        type: 'POST',
        success: function () { $("#ctl00_ContentPlaceHolder1_btnUpdateFile").click(); },
        data: formData,
        cache: false,
        contentType: false,
        processData: false
    });
}

function inFileNameValid(filename) {
    var lastDot = filename.lastIndexOf(".");
    var lastSlash = filename.lastIndexOf("\\");
    if (lastDot == -1 || lastSlash == -1) {
        alert('รูปแบบชื่อไฟล์ไม่ถูกต้อง\n\nระบบรองรับไฟล์ภาพประเภท png jpg jpeg gif\nหรือไฟล์ pdf เท่านั้น');
        return false;
    }
    var ext = filename.substring(lastDot + 1, filename.length).toLowerCase();
    switch (ext) {
        case 'jpg':
        case 'jpeg':
        case 'png':
        case 'gif':
        case 'pdf': return true;
        default:
            alert('รูปแบบชื่อไฟล์ไม่ถูกต้อง\n\nระบบรองรับไฟล์ภาพประเภท png jpg jpeg gif\nหรือไฟล์ pdf เท่านั้น');
            return false;
    }
}
                                                     