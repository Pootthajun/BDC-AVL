﻿package com.java.myapp;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class MyForm extends JFrame {

    
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				MyForm form = new MyForm();
				form.setVisible(true);
			}
		});
	}
	
	public MyForm() {
            
	// Create Form Frame
		super("ThaiCreate.Com Java GUI Tutorial");
		setSize(450, 300);
		setLocation(500, 280);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		// Create Button Open
		JButton btnButton = new JButton("Open Message Box");
		btnButton.setBounds(128, 93, 162, 23);
        btnButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
            	
            	JOptionPane.showMessageDialog(null,
            			"Hi!");

            }
        });
		getContentPane().add(btnButton);	
	}
		
}
