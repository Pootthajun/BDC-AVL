﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Setting_User
    Inherits System.Web.UI.Page

    Dim BL As New AVLBL

    Private ReadOnly Property PageName As String
        Get
            Return "Setting_User.aspx"
        End Get
    End Property
    Private Property AccessMode As AVLBL.AccessRole
        Get
            Try
                Return ViewState("AccessMode")
            Catch ex As Exception
                Return AVLBL.AccessRole.None
            End Try
        End Get
        Set(value As AVLBL.AccessRole)
            ViewState("AccessMode") = value
        End Set
    End Property
    Private ReadOnly Property User_ID_Role As Integer
        Get
            Try
                If IsNumeric(Session("User_ID")) Then
                    Return Session("User_ID")
                Else
                    Return -1
                End If
            Catch ex As Exception
                Return -1
            End Try
        End Get
    End Property


    Public Property User_ID() As Integer
        Get
            Return lblUser_ID.Text
        End Get
        Set(ByVal value As Integer)
            lblUser_ID.Text = value
        End Set
    End Property

    Public Property IsActive As Boolean
        Get
            Return imgStatus.ImageUrl = "images/check.png"
        End Get
        Set(value As Boolean)
            If value Then
                imgStatus.ImageUrl = "images/check.png"
            Else
                imgStatus.ImageUrl = "images/none.png"
            End If
        End Set
    End Property

    Public Property IsCancel_Role As Boolean
        Get
            Return imgCancel_Role.ImageUrl = "images/check.png"
        End Get
        Set(value As Boolean)
            If value Then
                imgCancel_Role.ImageUrl = "images/check.png"
            Else
                imgCancel_Role.ImageUrl = "images/none.png"
            End If
        End Set
    End Property

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        If IsNothing(Session("User_ID")) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "ShowAlert('warning','กรุณาล็อกอินเข้าสู่ระบบ','Login.aspx');", True)
            Response.Redirect("Login.aspx")
            Exit Sub
        End If

        If Not IsPostBack Then
            SetUserRole()
            BindList()
            ClearForm()
        End If

    End Sub


    Private Sub SetUserRole()

        '----------- Check สิทธิ์ในเมนู --------------
        Dim MT As DataTable = BL.GetUserMenuRole(User_ID_Role, PageName)
        If MT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลนี้','Overview.aspx');", True)
            Exit Sub
        End If
        AccessMode = MT.Rows(0).Item("MR_ID") '--------- สิทธิ์ในเมนูของผู้ใช้คนปัจจุบัน -----------
        If AccessMode = AVLBL.AccessRole.None Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลนี้','Overview.aspx');", True)
            Exit Sub
        End If

        btnAdd_User.Visible = AccessMode = AVLBL.AccessRole.Edit
        thEdit_Header.Visible = AccessMode = AVLBL.AccessRole.Edit
    End Sub


    Private Sub ClearForm()
        pnlList.Visible = True
        pnlEdit.Visible = False
    End Sub

    Protected Sub btnAdd_User_Click(sender As Object, e As System.EventArgs) Handles btnAdd_User.Click
        User_ID = 0
        pnlEdit_btnOK.Visible = True
        pnlEdit_Role.Visible = False
        BL.BindDDlPrefix(ddlPrefix)
        txtFisrt_Name.Text = ""
        txtLast_Name.Text = ""
        BL.BindDDlDEPT(ddlDept_Name, Session("User_ID"))
        txtPos_Name.Text = ""
        txtUser_Login.Text = ""
        txtUser_Password.Text = ""
        txtUser_Password_Confirm.Text = ""
        pnlList.Visible = False
        pnlEdit.Visible = True
        '---------ดึงรายการฝ่ายทั้งหมดที่มี-------------------
        BindDept_Role()

        btnAll_Creater.ImageUrl = "images/none.png"
        btnAll_Auditor.ImageUrl = "images/none.png"
        btnAll_SuppliesUnit.ImageUrl = "images/none.png"
        btnAll_NoneRole.ImageUrl = "images/check.png"

        '---------ดึงรายการเมนูที่มี-------------------
        BindMenu_Role()
    End Sub

#Region "rptList"
    Private Sub BindList()
        Dim SQL As String = " "
        SQL &= " SELECT "
        SQL &= " User_ID,User_Login,FullName,MAX(Creater) Creater,MAX(Auditor) Auditor,MAX(SupplyOfficer) SupplyOfficer,MAX(Director) Director" & vbLf
        SQL &= " ,MAX(Active_Status) Active_Status" & vbLf
        SQL &= " FROM (" & vbLf
        SQL &= " SELECT tb_User.User_ID,tb_User.User_Login" & vbLf
        SQL &= " ,(CASE WHEN tb_Prefix.Prefix_ABBR IS NOT NULL THEN tb_Prefix.Prefix_ABBR ELSE tb_Prefix.Prefix_Name END ) +tb_User.Fisrt_Name+ '  '+ tb_User.Last_Name FullName" & vbLf
        SQL &= " ,CASE WHEN tb_Dept_Role.AR_ID =1 THEN COUNT(tb_Dept_Role.User_ID) ELSE 0 END Creater" & vbLf
        SQL &= " ,CASE WHEN tb_Dept_Role.AR_ID =2 THEN COUNT(tb_Dept_Role.User_ID) ELSE 0 END Auditor" & vbLf
        SQL &= " ,CASE WHEN tb_Dept_Role.AR_ID =3 THEN COUNT(tb_Dept_Role.User_ID) ELSE 0 END SupplyOfficer" & vbLf
        SQL &= " ,CASE WHEN tb_Dept_Role.AR_ID =4 THEN COUNT(tb_Dept_Role.User_ID) ELSE 0 END Director" & vbLf
        SQL &= " ,CASE WHEN tb_User.Active_Status = 1 THEN 1 ELSE 0 END Active_Status" & vbLf
        SQL &= " FROM  tb_User" & vbLf
        SQL &= " LEFT JOIN  tb_Dept_Role ON tb_User.User_ID = tb_Dept_Role.User_ID " & vbLf
        SQL &= " LEFT JOIN tb_Prefix ON tb_User.Prefix_ID = tb_Prefix.Prefix_ID  " & vbLf

        SQL &= " WHERE tb_User.User_ID>0 " & vbLf
        SQL &= " GROUP BY tb_Dept_Role.AR_ID,tb_User.User_ID,tb_User.User_Login" & vbLf
        SQL &= " ,tb_Prefix.Prefix_Name,tb_Prefix.Prefix_ABBR,tb_User.Fisrt_Name,tb_User.Last_Name,tb_User.Active_Status" & vbLf
        SQL &= " ) AS TB" & vbLf

        Dim Filter As String = ""

        If txtSearchUserName.Text <> "" Then
            Filter &= "(User_Login LIKE '%" & txtSearchUserName.Text.Replace("'", "''") & "%') AND "
        End If
        If txtSearchFullname.Text <> "" Then
            Filter &= "(FullName LIKE '%" & txtSearchFullname.Text.Replace("'", "''") & "%') AND "
        End If

        Select Case ddlSearchCreater.SelectedIndex
            Case 1
                Filter &= "Creater > 0 AND "
            Case 2
                Filter &= "Creater = 0 AND "
        End Select
        Select Case ddlSearchAuditor.SelectedIndex
            Case 1
                Filter &= "Auditor > 0 AND "
            Case 2
                Filter &= "Auditor = 0 AND "
        End Select
        Select Case ddlSearchSuppliesUnit.SelectedIndex
            Case 1
                Filter &= "SupplyOfficer > 0 AND "
            Case 2
                Filter &= "SupplyOfficer = 0 AND "
        End Select
        Select Case ddlSearchDirector.SelectedIndex
            Case 1
                Filter &= "Director > 0 AND "
            Case 2
                Filter &= "Director = 0 AND "
        End Select
        Select Case ddlSearchAllow.SelectedIndex
            Case 1
                Filter &= "Active_Status > 0 AND "
            Case 2
                Filter &= "Active_Status = 0 AND "
        End Select
        If Filter <> "" Then
            SQL &= " WHERE " & Filter.Substring(0, Filter.Length - 4) & vbLf
        End If
        SQL &= " GROUP BY User_ID,FullName,User_Login" & vbLf
        SQL &= " ORDER BY FullName" & vbLf

        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        rptList.DataSource = DT
        rptList.DataBind()

    End Sub

    Protected Sub rptList_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptList.ItemCommand

        Select Case e.CommandName
            Case "Edit"
                Dim lblUser_Login As Label = e.Item.FindControl("lblUser_Login")
                Dim lblFullname As Label = e.Item.FindControl("lblFullname")
                Dim img_Status As HtmlAnchor = e.Item.FindControl("img_Status")
                Dim btnEdit As Button = e.Item.FindControl("btnEdit")
                User_ID = btnEdit.CommandArgument
                Dim UserInfo As AVLBL.UserInfo = BL.GetUserInfo(User_ID)

                BL.BindDDlPrefix(ddlPrefix, UserInfo.Prefix_ID)
                txtFisrt_Name.Text = UserInfo.Fisrt_Name
                txtLast_Name.Text = UserInfo.Last_Name
                BL.BindDDlDEPT(ddlDept_Name, Session("User_ID"), UserInfo.Dept_ID)
                txtPos_Name.Text = UserInfo.Pos_Name
                txtUser_Login.Text = UserInfo.User_Login
                txtUser_Password.Text = UserInfo.User_Password
                txtUser_Password_Confirm.Text = UserInfo.User_Password
                IsCancel_Role = UserInfo.Cancel_Role
                IsActive = UserInfo.Active_Status


                '--------บทบาทการประเมิน/เมนู----------
                BindDept_Role()
                BindMenu_Role()

                btnAll_View.ImageUrl = "images/none.png"
                btnAll_Edit.ImageUrl = "images/none.png"
                btnAll_None.ImageUrl = "images/none.png"

                pnlList.Visible = False
                pnlEdit.Visible = True
        End Select

    End Sub

    Protected Sub rptList_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptList.ItemDataBound

        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblUser_Login As Label = e.Item.FindControl("lblUser_Login")
        Dim lblFullname As Label = e.Item.FindControl("lblFullname")
        Dim lblRole_Name As Label = e.Item.FindControl("lblRole_Name")
        Dim img_Creater As HtmlAnchor = e.Item.FindControl("img_Creater")
        Dim img_Auditor As HtmlAnchor = e.Item.FindControl("img_Auditor")
        Dim img_SuppliesUnit As HtmlAnchor = e.Item.FindControl("img_SuppliesUnit")
        Dim img_Director As HtmlAnchor = e.Item.FindControl("img_Director")
        Dim img_Status As HtmlAnchor = e.Item.FindControl("img_Status")
        Dim btnEdit As Button = e.Item.FindControl("btnEdit")
        Dim tdEdit_List As HtmlTableCell = e.Item.FindControl("tdEdit_List")

        tdEdit_List.Visible = AccessMode = AVLBL.AccessRole.Edit

        lblUser_Login.Text = e.Item.DataItem("User_Login").ToString
        lblFullname.Text = "<B>" & e.Item.DataItem("FullName").ToString & "</B>"
        lblRole_Name.Text = BL.GetDeptRoleUserInfo(e.Item.DataItem("User_ID"))
        Select Case BL.GetDeptRoleUserInfo(e.Item.DataItem("User_ID"))
            Case "ไม่มีสิทธิ์ในการประเมิน "
                lblRole_Name.Style("color") = "red"
            Case Else
                lblRole_Name.Style("color") = "green"
        End Select

        If e.Item.DataItem("Creater") = 0 Then
            img_Creater.InnerHtml = "<img src='images/none.png' />"
            img_Creater.Title = "ไม่แสดงในระบบ"
        Else
            img_Creater.InnerHtml = "<img src='images/check.png' />"
            img_Creater.Title = "แสดงในระบบ"
        End If
        If e.Item.DataItem("Auditor") = 0 Then
            img_Auditor.InnerHtml = "<img src='images/none.png' />"
            img_Auditor.Title = "ไม่แสดงในระบบ"
        Else
            img_Auditor.InnerHtml = "<img src='images/check.png' />"
            img_Auditor.Title = "แสดงในระบบ"
        End If
        If e.Item.DataItem("SupplyOfficer") = 0 Then
            img_SuppliesUnit.InnerHtml = "<img src='images/none.png' />"
            img_SuppliesUnit.Title = "ไม่แสดงในระบบ"
        Else
            img_SuppliesUnit.InnerHtml = "<img src='images/check.png' />"
            img_SuppliesUnit.Title = "แสดงในระบบ"
        End If
        If e.Item.DataItem("Director") = 0 Then
            img_Director.InnerHtml = "<img src='images/none.png' />"
            img_Director.Title = "ไม่แสดงในระบบ"
        Else
            img_Director.InnerHtml = "<img src='images/check.png' />"
            img_Director.Title = "แสดงในระบบ"
        End If
        If e.Item.DataItem("Active_Status") Then
            img_Status.InnerHtml = "<img src='images/check.png' />"
            img_Status.Title = "แสดงในระบบ"
            lblUser_Login.Style("color") = "black"
            lblFullname.Style("color") = "black"
        Else
            img_Status.InnerHtml = "<img src='images/none.png' />"
            img_Status.Title = "ไม่แสดงในระบบ"
            lblUser_Login.Style("color") = "gray"
            lblFullname.Style("color") = "gray"
        End If
        btnEdit.CommandArgument = e.Item.DataItem("User_ID")

    End Sub


#End Region

#Region "rptDept_Role"

    Public Function GetSubDept() As DataTable
        Dim SQL As String = " "
        SQL &= " SELECT  " & vbLf
        SQL &= " tb_Dept.Dept_ID," & vbLf
        SQL &= " tb_Dept.Dept_Name," & vbLf
        SQL &= " tb_Sub_Dept.Sub_Dept_ID," & vbLf
        SQL &= " tb_Sub_Dept.Sub_Dept_Name " & vbLf
        SQL &= " FROM tb_Dept " & vbLf
        SQL &= " LEFT JOIN tb_Sub_Dept ON tb_Sub_Dept.Dept_ID =tb_Dept.Dept_ID" & vbLf
        SQL &= " LEFT JOIN tb_Dept_Role  ON tb_Dept_Role.Dept_ID=tb_Dept .Dept_ID  " & vbLf
        'SQL &= " WHERE tb_Dept.Dept_ID='01'" & vbLf   '---------------กำหนดแค่สำนักงานเดียว--------------------
        SQL &= " WHERE tb_Dept.Active_Status=1" & vbLf
        SQL &= " Group BY " & vbLf
        SQL &= " tb_Dept.Dept_ID," & vbLf
        SQL &= " tb_Dept.Dept_Name, tb_Sub_Dept.Sub_Dept_ID," & vbLf
        SQL &= " tb_Sub_Dept.Sub_Dept_Name" & vbLf
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT

    End Function

    Private Sub BindDept_Role()
        Dim SQL As String = " "
        Dim DT As New DataTable
        DT = GetSubDept()

        DT.Columns.Add("AR_ID")
        DT.Columns.Add("User_ID")

        DT.Columns.Add("Role_1_ID", GetType(Boolean))
        DT.Columns.Add("Role_2_ID", GetType(Boolean))
        DT.Columns.Add("Role_3_ID", GetType(Boolean))
        DT.Columns.Add("Role_4_ID", GetType(Boolean))
        DT.Columns.Add("NoneRole", GetType(Boolean))

        If DT.Rows.Count > 0 Then
            SQL = ""
            SQL &= " "
            SQL &= " SELECT tb_Dept_Role.AR_ID,tb_Dept_Role.Dept_ID ,tb_Dept_Role.Sub_Dept_ID  ,tb_Dept_Role.User_ID , tb_Ass_Role.AR_Name_TH  "
            SQL &= " FROM tb_Dept_Role "
            SQL &= " LEFT JOIN tb_Ass_Role ON tb_Ass_Role.AR_ID =tb_Dept_Role.AR_ID"
            SQL &= " WHERE tb_Dept_Role.User_ID ='" & User_ID & "'"
            Dim DA_Temp As New SqlDataAdapter(SQL, BL.ConnectionString)
            Dim DT_Temp As New DataTable
            DA_Temp.Fill(DT_Temp)
            If DT_Temp.Rows.Count > 0 Then
                For i As Integer = 0 To DT.Rows.Count - 1
                    DT_Temp.DefaultView.RowFilter = " Dept_ID='" & DT.Rows(i).Item("Dept_ID") & "' AND Sub_Dept_ID ='" & DT.Rows(i).Item("Sub_Dept_ID") & "'"
                    If DT_Temp.DefaultView.Count > 0 Then
                        For j As Integer = 0 To DT_Temp.DefaultView.Count - 1
                            
                            DT.Rows(i).Item("Role_" & DT_Temp.DefaultView(j).Item("AR_ID") & "_ID") = True
                        Next
                        For k As Integer = 1 To 4
                            Select Case DT.Rows(i).Item("Role_" & k & "_ID").ToString
                                Case ""
                                    DT.Rows(i).Item("Role_" & k & "_ID") = False
                            End Select
                        Next

                        DT.Rows(i).Item("NoneRole") = False
                    Else
                        DT.Rows(i).Item("AR_ID") = 0
                        DT.Rows(i).Item("Role_1_ID") = False
                        DT.Rows(i).Item("Role_2_ID") = False
                        DT.Rows(i).Item("Role_3_ID") = False
                        DT.Rows(i).Item("Role_4_ID") = False
                        DT.Rows(i).Item("NoneRole") = True
                    End If
                    DT.Rows(i).Item("User_ID") = DT_Temp.Rows(0).Item("User_ID")

                Next
            Else
                For i As Integer = 0 To DT.Rows.Count - 1
                    DT.Rows(i).Item("AR_ID") = 0
                    DT.Rows(i).Item("User_ID") = DBNull.Value
                    DT.Rows(i).Item("Role_1_ID") = False
                    DT.Rows(i).Item("Role_2_ID") = False
                    DT.Rows(i).Item("Role_3_ID") = False
                    DT.Rows(i).Item("Role_4_ID") = False
                    DT.Rows(i).Item("NoneRole") = True
                Next
            End If

            ''---------------ดูว่าแต่ละฝ่ายได้สิทธิ์เดียวกันหรือไม่----------------------

            btnAll_Creater.ImageUrl = "images/none.png"
            btnAll_Auditor.ImageUrl = "images/none.png"
            btnAll_SuppliesUnit.ImageUrl = "images/none.png"
            btnAll_Director.ImageUrl = "images/none.png"
            btnAll_NoneRole.ImageUrl = "images/none.png"

            'Dim DT_RowFilter As DataTable = DT.Copy

            '-------------ปรับ checkAll-------------หลังจากปรับฝ่ายได้หลายสิทธิ์
            For i As Integer = 1 To 4
                Dim DT_RowFilter As DataTable = DT.Copy
                DT_RowFilter.DefaultView.RowFilter = "Sub_Dept_ID <>'' AND Role_" & i & "_ID = false "

                'DT_RowFilter.DefaultView.RowFilter = " AR_ID <> " & DT.Rows(0).Item("AR_ID").ToString()
                If DT_RowFilter.DefaultView.Count = 0 Then   '--------------------
                    Select Case i
                        Case 1
                            btnAll_Creater.ImageUrl = "images/check.png"
                        Case 2
                            btnAll_Auditor.ImageUrl = "images/check.png"
                        Case 3
                            btnAll_SuppliesUnit.ImageUrl = "images/check.png"
                        Case 4
                            btnAll_Director.ImageUrl = "images/check.png"
                        Case Else
                            btnAll_NoneRole.ImageUrl = "images/check.png"
                    End Select
                End If
            Next

            If btnAll_Creater.ImageUrl = "images/check.png" Or btnAll_Auditor.ImageUrl = "images/check.png" Or btnAll_SuppliesUnit.ImageUrl = "images/check.png" Or btnAll_Director.ImageUrl = "images/check.png" Then
                btnAll_NoneRole.ImageUrl = "images/none.png"
            Else
                btnAll_NoneRole.ImageUrl = "images/check.png"
            End If
        End If

        rptDept_Role.DataSource = DT
        rptDept_Role.DataBind()

        'Dim SQL As String = " "
        'SQL &= " SELECT " & vbLf
        'SQL &= " tb_User.User_ID," & vbLf
        'SQL &= " tb_Dept.Dept_ID ," & vbLf
        'SQL &= " tb_Dept.Dept_Name," & vbLf
        'SQL &= " ISNULL(tb_Ass_Role.AR_ID,0) AR_ID" & vbLf
        'SQL &= " FROM tb_Dept " & vbLf
        'SQL &= " LEFT JOIN tb_Dept_Role  ON tb_Dept_Role.Dept_ID=tb_Dept .Dept_ID " & vbLf
        'SQL &= " LEFT JOIN tb_Ass_Role ON tb_Dept_Role.AR_ID=tb_Ass_Role.AR_ID" & vbLf
        'SQL &= " LEFT JOIN tb_User ON tb_User.User_ID=tb_Dept_Role.User_ID" & vbLf
        'SQL &= " WHERE tb_Dept_Role.User_ID=" & User_ID & " AND tb_Dept.Dept_ID='01'"
        'Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        'Dim DT As New DataTable
        'DA.Fill(DT)
        'rptDept_Role.DataSource = DT
        'rptDept_Role.DataBind()
        '-----------------เดิมมีสิทธิ์แค่ฝ่ายเดียว---------------------------
        'btnAll_Creater.ImageUrl = "images/none.png"
        'btnAll_Auditor.ImageUrl = "images/none.png"
        'btnAll_SuppliesUnit.ImageUrl = "images/none.png"
        'btnAll_Director.ImageUrl = "images/none.png"
        'btnAll_NoneRole.ImageUrl = "images/none.png"
        'If DT.Rows.Count = 0 Then
        '    btnAll_NoneRole.ImageUrl = "images/check.png"
        'Else
        '    Select Case DT.Rows(0).Item("AR_ID")
        '        Case 1
        '            btnAll_Creater.ImageUrl = "images/check.png"
        '        Case 2
        '            btnAll_Auditor.ImageUrl = "images/check.png"
        '        Case 3
        '            btnAll_SuppliesUnit.ImageUrl = "images/check.png"
        '        Case 4
        '            btnAll_Director.ImageUrl = "images/check.png"
        '        Case Else
        '            btnAll_NoneRole.ImageUrl = "images/check.png"
        '    End Select
        'End If


    End Sub


    Protected Sub rptDept_Role_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptDept_Role.ItemCommand

        'If User_ID = 0 Then
        '    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','กรอกรายละเอียดก่อนกำหนดสิทธิ์การประเมิน','');", True)
        '    Exit Sub
        'Else
        '    AutoSave()
        'End If


        Dim lblDept_Name As Label = e.Item.FindControl("lblDept_Name")
        Dim lblSub_Dept_Name As Label = e.Item.FindControl("lblSub_Dept_Name")
        Dim Dept_ID As String = lblDept_Name.Attributes("Dept_ID")
        'Dim AR_ID As Integer = lblDept_Name.Attributes("AR_ID")
        Dim Sub_Dept_ID As String = lblSub_Dept_Name.Attributes("Sub_Dept_ID").ToString

        Dim Del_AR_ID As String = ""
        Select Case e.CommandName
            Case "Creater"  '------ผู้จัดทำ---------
                Del_AR_ID = 1
            Case "Auditor"  '------ผู้ตรวจสอบ---------
                Del_AR_ID = 2
            Case "SuppliesUnit"  '------พัสดุ---------
                Del_AR_ID = 3
            Case "Director"  '------พัสดุ---------
                Del_AR_ID = 4
        End Select


        Dim SQL As String = "SELECT * FROM tb_Dept_Role WHERE Dept_ID='" & Dept_ID & "' AND User_ID='" & User_ID & "' AND AR_ID='" & Del_AR_ID & "' AND Sub_Dept_ID='" & Sub_Dept_ID & "'"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        Select Case e.CommandName
            Case "NoneRole" '------ไม่มีสิทธิ์---------
                SQL = " DELETE FROM tb_Dept_Role WHERE Dept_ID=" & Dept_ID & " AND User_ID=" & User_ID & " AND Sub_Dept_ID='" & Sub_Dept_ID & "'"
                DA = New SqlDataAdapter(SQL, BL.ConnectionString)
                DT = New DataTable
                DA.Fill(DT)
                BindDept_Role()

            Case Else
                Dim img As ImageButton = e.Item.FindControl("img" & e.CommandName)

                ''---------------ผู้ใช้ 1 คนสามารถมีสิทธิ์การประเมินได้หลายสิทธิ์ในฝ่าย  เดียวกัน----------------------
                Select Case img.ImageUrl
                    Case "images/none.png"
                        If User_ID = 0 Then User_ID = BL.GetNewPrimaryID("tb_User", "User_ID")
                        Dim DR As DataRow
                        If DT.Rows.Count = 0 Then
                            DR = DT.NewRow
                            DR("User_ID") = User_ID
                            DR("Dept_ID") = Dept_ID
                            DR("Sub_Dept_ID") = Sub_Dept_ID
                        Else
                            DR = DT.Rows(0)
                        End If
                        Select Case e.CommandName
                            Case "Creater"  '------ผู้จัดทำ---------
                                DR("AR_ID") = 1
                            Case "Auditor"  '------ผู้ตรวจสอบ---------
                                DR("AR_ID") = 2
                            Case "SuppliesUnit"  '------พัสดุ---------
                                DR("AR_ID") = 3
                            Case "Director"  '------พัสดุ---------
                                DR("AR_ID") = 4
                        End Select
                        DR("Update_By") = Session("User_ID")
                        DR("Update_Time") = Now
                        If DT.Rows.Count = 0 Then DT.Rows.Add(DR)
                        Dim cmd As New SqlCommandBuilder()
                        cmd = New SqlCommandBuilder(DA)
                        DA.Update(DT)
                    Case Else
                        '------------------ลบรายการที่เอาออก-----------------
                        SQL = " DELETE FROM tb_Dept_Role WHERE Dept_ID=" & Dept_ID & " AND User_ID=" & User_ID & " AND Sub_Dept_ID='" & Sub_Dept_ID & "'" & " AND AR_ID='" & Del_AR_ID & "'"
                        DA = New SqlDataAdapter(SQL, BL.ConnectionString)
                        DT = New DataTable
                        DA.Fill(DT)
                End Select


                BindDept_Role()
        End Select

    End Sub

    Dim LastDept As String = ""
    Protected Sub rptDept_Role_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptDept_Role.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub


        Dim lblDept_Name As Label = e.Item.FindControl("lblDept_Name")
        Dim lblSub_Dept_Name As Label = e.Item.FindControl("lblSub_Dept_Name")

        Dim imgCreater As ImageButton = e.Item.FindControl("imgCreater")
        Dim imgAuditor As ImageButton = e.Item.FindControl("imgAuditor")
        Dim imgSuppliesUnit As ImageButton = e.Item.FindControl("imgSuppliesUnit")
        Dim imgDirector As ImageButton = e.Item.FindControl("imgDirector")
        Dim imgNoneRole As ImageButton = e.Item.FindControl("imgNoneRole")
        Dim tdDept As HtmlTableCell = e.Item.FindControl("tdDept")
        lblDept_Name.Text = e.Item.DataItem("Dept_Name").ToString
        lblDept_Name.Attributes("Dept_ID") = e.Item.DataItem("Dept_ID")
        'lblDept_Name.Attributes("AR_ID") = e.Item.DataItem("AR_ID")
        lblSub_Dept_Name.Text = e.Item.DataItem("Sub_Dept_Name").ToString()
        lblSub_Dept_Name.Attributes("Sub_Dept_ID") = e.Item.DataItem("Sub_Dept_ID").ToString()
        Dim DuplicatedStyleTop As String = "border-top:none;"
        If e.Item.DataItem("Dept_Name").ToString <> LastDept Then
            LastDept = e.Item.DataItem("Dept_Name").ToString
            lblDept_Name.Text = LastDept
        Else
            lblDept_Name.Text = ""
            tdDept.Attributes("style") &= DuplicatedStyleTop
        End If

        imgCreater.Visible = True
        imgAuditor.Visible = True
        imgSuppliesUnit.Visible = True
        imgDirector.Visible = True
        imgNoneRole.Visible = True
        If IsDBNull(e.Item.DataItem("Sub_Dept_ID")) Or e.Item.DataItem("Sub_Dept_ID").ToString = "" Then
            '--------สำนักงานยังไม่มีฝ่าย----------------
            '-------1------Creater---ผู้จัดทำ------
            imgCreater.ImageUrl = "images/none.png"
            '-------2------Auditor---ผู้ตรวจสอบ------
            imgAuditor.ImageUrl = "images/none.png"
            '-------3------SuppliesUnit---พัสดุ------
            imgSuppliesUnit.ImageUrl = "images/none.png"
            '-------4------Director---หัวหน้าฝ่าย------
            imgDirector.ImageUrl = "images/none.png"
            '-------0------ไม่มีสิทธิ์---------
            imgNoneRole.ImageUrl = "images/check.png"

            imgCreater.Visible = False
            imgAuditor.Visible = False
            imgSuppliesUnit.Visible = False
            imgDirector.Visible = False
            imgNoneRole.Visible = True

            'End If


        ElseIf Not IsDBNull(e.Item.DataItem("User_ID")) Then
            If e.Item.DataItem("NoneRole") Then
                '---------ไม่มีสิทธิ์ในฝ่าย----------------
                '-------1------Creater---ผู้จัดทำ------
                imgCreater.ImageUrl = "images/none.png"
                '-------2------Auditor---ผู้ตรวจสอบ------
                imgAuditor.ImageUrl = "images/none.png"
                '-------3------SuppliesUnit---พัสดุ------
                imgSuppliesUnit.ImageUrl = "images/none.png"
                '-------4------Director---หัวหน้าฝ่าย------
                imgDirector.ImageUrl = "images/none.png"
                '-------0------ไม่มีสิทธิ์---------
                imgNoneRole.ImageUrl = "images/check.png"
            Else

                For i As Integer = 1 To 4
                    Select Case e.Item.DataItem("Role_" & i & "_ID")
                        Case True
                            If i = 1 Then
                                '-------1------Creater---ผู้จัดทำ------
                                imgCreater.ImageUrl = "images/check.png"

                            ElseIf i = 2 Then
                                '-------2------Auditor---ผู้ตรวจสอบ------
                                imgAuditor.ImageUrl = "images/check.png"

                            ElseIf i = 3 Then
                                '-------3------SuppliesUnit---พัสดุ------
                                imgSuppliesUnit.ImageUrl = "images/check.png"
                            ElseIf i = 4 Then
                                '-------4------Director---หัวหน้าฝ่าย------
                                imgDirector.ImageUrl = "images/check.png"
                            End If


                        Case Else


                    End Select
                Next
            End If

        Else
            '------------------User ใหม่---------------------------
            '-------1------Creater---ผู้จัดทำ------
            imgCreater.ImageUrl = "images/none.png"
            '-------2------Auditor---ผู้ตรวจสอบ------
            imgAuditor.ImageUrl = "images/none.png"
            '-------3------SuppliesUnit---พัสดุ------
            imgSuppliesUnit.ImageUrl = "images/none.png"
            '-------4------Director---หัวหน้าฝ่าย------
            imgDirector.ImageUrl = "images/none.png"
            '-------0------ไม่มีสิทธิ์---------
            imgNoneRole.ImageUrl = "images/check.png"
        End If

    End Sub

    Protected Sub btnAllRole_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btnAll_Creater.Click, btnAll_Auditor.Click, btnAll_SuppliesUnit.Click, btnAll_Director.Click, btnAll_NoneRole.Click

        Dim btn As ImageButton = sender
        Select Case btn.ImageUrl
            Case "images/check.png"

                'btnAll_Creater.ImageUrl = "images/none.png"
                'btnAll_Auditor.ImageUrl = "images/none.png"
                'btnAll_SuppliesUnit.ImageUrl = "images/none.png"
                'btnAll_Director.ImageUrl = "images/none.png"
                'btnAll_NoneRole.ImageUrl = "images/none.png"

                Dim SQL As String = " DELETE FROM tb_Dept_Role WHERE  User_ID=" & User_ID
                Select Case btn.CommandName
                    Case "Creater"
                        SQL &= "  AND AR_ID=1"
                    Case "Auditor"
                        SQL &= "  AND AR_ID=2"
                    Case "SuppliesUnit"
                        SQL &= "  AND AR_ID=3"
                    Case "Director"
                        SQL &= "  AND AR_ID=4"
                    Case "NoneRole"
                        SQL &= ""
                        btn.ImageUrl = "images/none.png"
                End Select



                Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
                Dim DT As New DataTable
                DA.Fill(DT)

            Case Else
                'btnAll_Creater.ImageUrl = "images/none.png"
                'btnAll_Auditor.ImageUrl = "images/none.png"
                'btnAll_SuppliesUnit.ImageUrl = "images/none.png"
                'btnAll_Director.ImageUrl = "images/none.png"
                'btnAll_NoneRole.ImageUrl = "images/none.png"

                btn.ImageUrl = "images/check.png"
                If btn.CommandName = "NoneRole" Then
                    Dim SQL_Del As String = " DELETE FROM tb_Dept_Role WHERE  User_ID=" & User_ID

                    Dim DA_Del As New SqlDataAdapter(SQL_Del, BL.ConnectionString)
                    Dim DT_Del As New DataTable
                    DA_Del.Fill(DT_Del)
                Else
                    Dim AR_ID As String = ""
                    Select Case btn.CommandName
                        Case "Creater"
                            AR_ID &= "  AND AR_ID=1"
                        Case "Auditor"
                            AR_ID &= "  AND AR_ID=2"
                        Case "SuppliesUnit"
                            AR_ID &= "  AND AR_ID=3"
                        Case "Director"
                            AR_ID &= "  AND AR_ID=4"
                        Case "NoneRole"
                            AR_ID &= ""
                    End Select

                    Dim DT_Dept As New DataTable
                    DT_Dept = GetSubDept()
                    Dim SQL As String = ""
                    SQL &= " DELETE FROM tb_Dept_Role WHERE  User_ID=" & User_ID & AR_ID
                    SQL &= " SELECT * FROM tb_Dept_Role WHERE 0=1 " & vbLf
                    Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
                    Dim DT_Role As New DataTable
                    DA.Fill(DT_Role)
                    If User_ID = 0 Then User_ID = BL.GetNewPrimaryID("tb_User", "User_ID")
                    Dim DR As DataRow
                    For i As Integer = 0 To DT_Dept.Rows.Count - 1
                        If DT_Dept.Rows(i).Item("Sub_Dept_ID").ToString <> "" Then
                            DR = DT_Role.NewRow
                            DR("User_ID") = User_ID
                            DR("Dept_ID") = DT_Dept.Rows(i).Item("Dept_ID").ToString()
                            DR("Sub_Dept_ID") = DT_Dept.Rows(i).Item("Sub_Dept_ID").ToString()

                            DR("Update_Time") = Now
                            Select Case btn.CommandName
                                Case "Creater"
                                    DR("AR_ID") = 1
                                Case "Auditor"
                                    DR("AR_ID") = 2
                                Case "SuppliesUnit"
                                    DR("AR_ID") = 3
                                Case "Director"
                                    DR("AR_ID") = 4
                            End Select
                            DT_Role.Rows.Add(DR)
                        End If
                    Next
                    Dim cmd As New SqlCommandBuilder()
                    cmd = New SqlCommandBuilder(DA)
                    DA.Update(DT_Role)
                End If

        End Select

        BindDept_Role()


    End Sub

#End Region

#Region "rptMenu_Role"

    Private Sub BindMenu_Role()
        Dim SQL As String = " "
        SQL &= " SELECT  tb_Menu.Menu_ID, tb_Menu.Menu_Name,tb_User.Dept_ID" & vbLf
        SQL &= " ,ISNULL(tb_Menu_Role_Name.MR_ID,0) MR_ID " & vbLf
        SQL &= " ,ISNULL(tb_Menu_Role_Name.MR_Name,'ทำไม่ได้') MR_Name" & vbLf
        SQL &= " ,tb_User.User_ID"
        SQL &= " ,(CASE WHEN tb_Prefix.Prefix_ABBR IS NOT NULL THEN tb_Prefix.Prefix_ABBR ELSE tb_Prefix.Prefix_Name END ) +tb_User.Fisrt_Name+ '  '+ tb_User.Last_Name FullName" & vbLf

        SQL &= " FROM tb_Menu " & vbLf
        SQL &= " LEFT JOIN tb_Menu_Role ON tb_Menu.Menu_ID = tb_Menu_Role.Menu_ID  AND tb_Menu_Role.User_ID=" & User_ID & vbLf
        SQL &= " LEFT JOIN tb_User ON tb_Menu_Role.User_ID = tb_User.User_ID" & vbLf
        SQL &= " LEFT JOIN tb_Menu_Role_Name ON tb_Menu_Role_Name.MR_ID = tb_Menu_Role.MR_ID" & vbLf
        SQL &= " LEFT JOIN tb_Prefix ON tb_User.Prefix_ID = tb_Prefix.Prefix_ID  " & vbLf
        SQL &= " WHERE tb_Menu.Active_Status=1  " & vbLf

        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        rptMenu_Role.DataSource = DT
        rptMenu_Role.DataBind()

    End Sub

    Protected Sub rptMenu_Role_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptMenu_Role.ItemCommand

        If User_ID = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','กรอกรายละเอียดก่อนกำหนดสิทธิ์เมนู','');", True)
            Exit Sub
        End If

        Dim lblMenu_Name As Label = e.Item.FindControl("lblMenu_Name")
        Dim Menu_ID As String = lblMenu_Name.Attributes("Menu_ID")
        Dim MR_ID As Integer = lblMenu_Name.Attributes("MR_ID")

        Dim SQL As String = "SELECT * FROM tb_Menu_Role WHERE Menu_ID=" & Menu_ID & " AND User_ID=" & User_ID & " AND MR_ID=" & MR_ID
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        Select Case e.CommandName
            Case "NoneRole" '------ไม่มีสิทธิ์---------
                SQL = " DELETE FROM tb_Menu_Role WHERE Menu_ID=" & Menu_ID & " AND User_ID=" & User_ID
                DA = New SqlDataAdapter(SQL, BL.ConnectionString)
                DT = New DataTable
                DA.Fill(DT)
                BindMenu_Role()
            Case Else
                If User_ID = 0 Then User_ID = BL.GetNewPrimaryID("tb_User", "User_ID")
                Dim DR As DataRow
                If DT.Rows.Count = 0 Then
                    DR = DT.NewRow
                    DR("User_ID") = User_ID
                    DR("Menu_ID") = Menu_ID
                Else
                    DR = DT.Rows(0)
                End If
                Select Case e.CommandName
                    Case "View"  '------View---------
                        DR("MR_ID") = 1
                    Case "Edit"  '------Edit---------
                        DR("MR_ID") = 2
                End Select
                'DR("Update_By") = Session("")
                DR("Update_Time") = Now
                If DT.Rows.Count = 0 Then DT.Rows.Add(DR)
                Dim cmd As New SqlCommandBuilder()
                cmd = New SqlCommandBuilder(DA)
                DA.Update(DT)

                BindMenu_Role()
        End Select

    End Sub

    Protected Sub rptMenu_Role_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptMenu_Role.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblMenu_Name As Label = e.Item.FindControl("lblMenu_Name")
        Dim imgView As ImageButton = e.Item.FindControl("imgView")
        Dim imgEdit As ImageButton = e.Item.FindControl("imgEdit")
        Dim imgNoneRole As ImageButton = e.Item.FindControl("imgNoneRole")

        lblMenu_Name.Text = e.Item.DataItem("Menu_Name").ToString
        lblMenu_Name.Attributes("Menu_ID") = e.Item.DataItem("Menu_ID")
        lblMenu_Name.Attributes("MR_ID") = e.Item.DataItem("MR_ID")
        lblMenu_Name.Attributes("MR_Name") = e.Item.DataItem("MR_Name")

        If Not IsDBNull(e.Item.DataItem("User_ID")) Then

            '------------------User เดิม---------------------------
            Select Case (e.Item.DataItem("MR_ID"))
                Case 1
                    '-------1------View----------
                    imgView.ImageUrl = "images/check.png"
                    '-------2------Edit-----------
                    imgEdit.ImageUrl = "images/none.png"
                    '-------0------NoneRole---------
                    imgNoneRole.ImageUrl = "images/none.png"
                Case 2
                    '-------1------View----------
                    imgView.ImageUrl = "images/none.png"
                    '-------2------Edit-----------
                    imgEdit.ImageUrl = "images/check.png"
                    '-------0------NoneRole---------
                    imgNoneRole.ImageUrl = "images/none.png"
                Case 0
                    '-------1------View----------
                    imgView.ImageUrl = "images/none.png"
                    '-------2------Edit-----------
                    imgEdit.ImageUrl = "images/none.png"
                    '-------0------NoneRole---------
                    imgNoneRole.ImageUrl = "images/check.png"
                Case Else
                    '-------1------View----------
                    imgView.ImageUrl = "images/none.png"
                    '-------2------Edit-----------
                    imgEdit.ImageUrl = "images/none.png"
                    '-------0------NoneRole---------
                    imgNoneRole.ImageUrl = "images/none.png"


            End Select
        Else
            '------------------User ใหม่---------------------------
            '-------1------View----------
            imgView.ImageUrl = "images/none.png"
            '-------2------Edit-----------
            imgEdit.ImageUrl = "images/none.png"
            '-------0------NoneRole---------
            imgNoneRole.ImageUrl = "images/check.png"
        End If







    End Sub

    Protected Sub btnAll_View_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles btnAll_View.Click, btnAll_Edit.Click, btnAll_None.Click
        Dim btn As ImageButton = sender
        Select Case btn.ImageUrl
            Case "images/check.png"
                btnAll_View.ImageUrl = "images/none.png"
                btnAll_Edit.ImageUrl = "images/none.png"
                btnAll_None.ImageUrl = "images/none.png"

                Dim SQL As String = " DELETE FROM tb_Menu_Role WHERE  User_ID=" & User_ID
                Select Case btn.CommandName
                    Case "View"
                        SQL &= "  AND MR_ID=1"
                    Case "Edit"
                        SQL &= "  AND MR_ID=2"
                    Case "None"
                        SQL &= ""
                        btn.ImageUrl = "images/none.png"
                End Select

                Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
                Dim DT As New DataTable
                DA.Fill(DT)
                BindMenu_Role()

            Case Else
                btn.ImageUrl = "images/check.png"

                Select Case btn.CommandName
                    Case "None"
                        Dim SQL_Del As String = " DELETE FROM tb_Menu_Role WHERE  User_ID=" & User_ID
                        Dim DA_Del As New SqlDataAdapter(SQL_Del, BL.ConnectionString)
                        Dim DT_Del As New DataTable
                        DA_Del.Fill(DT_Del)
                        btnAll_View.ImageUrl = "images/none.png"
                        btnAll_Edit.ImageUrl = "images/none.png"
                        btnAll_None.ImageUrl = "images/check.png"

                    Case Else

                        Dim SQL As String = " SELECT * FROM tb_Menu"
                        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
                        Dim DT_Menu As New DataTable
                        DA.Fill(DT_Menu)

                        SQL = ""
                        SQL &= " DELETE FROM tb_Menu_Role WHERE  User_ID=" & User_ID
                        SQL &= " SELECT * FROM tb_Menu_Role WHERE 0=1 " & vbLf
                        DA = New SqlDataAdapter(SQL, BL.ConnectionString)
                        Dim DT_Role As New DataTable
                        DA.Fill(DT_Role)
                        If User_ID = 0 Then User_ID = BL.GetNewPrimaryID("tb_User", "User_ID")
                        Dim DR As DataRow
                        For i As Integer = 0 To DT_Menu.Rows.Count - 1
                            DR = DT_Role.NewRow
                            DR("User_ID") = User_ID
                            DR("Menu_ID") = DT_Menu.Rows(i).Item("Menu_ID")
                            'DR("Update_By") = Session("")
                            DR("Update_Time") = Now
                            Select Case btn.CommandName
                                Case "View"
                                    DR("MR_ID") = 1
                                Case "Edit"
                                    DR("MR_ID") = 2
                            End Select
                            DT_Role.Rows.Add(DR)
                        Next
                        Dim cmd As New SqlCommandBuilder()
                        cmd = New SqlCommandBuilder(DA)
                        DA.Update(DT_Role)
                End Select


                Select Case btn.CommandName
                    Case "View"
                        btnAll_View.ImageUrl = "images/check.png"
                        btnAll_Edit.ImageUrl = "images/none.png"
                        btnAll_None.ImageUrl = "images/none.png"

                    Case "Edit"
                        btnAll_View.ImageUrl = "images/none.png"
                        btnAll_Edit.ImageUrl = "images/check.png"
                        btnAll_None.ImageUrl = "images/none.png"
                End Select
        End Select
        BindMenu_Role()
    End Sub

#End Region

#Region "Button"
    Private Sub SaveFlag()

        '---------------------------รายละเอียด-----------------------------------
        Dim Sql As String = ""
        Sql &= " SELECT * " & vbLf
        Sql &= " FROM tb_User" & vbLf
        Sql &= " WHERE User_ID=" & User_ID & "" & vbLf
        Dim DA As New SqlDataAdapter(Sql, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        DA.Fill(DT)
        If User_ID = 0 Then User_ID = BL.GetNewPrimaryID("tb_User", "User_ID")
        '------------บันทึก --------------
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            DR("User_ID") = User_ID
        Else
            DR = DT.Rows(0)
            DR("Update_By") = Session("User_ID")
            DR("Update_Time") = Now
        End If
        'DR("User_Login") = txtUser_Login.Text
        'DR("User_Password") = txtUser_Password.Text
        'DR("Dept_ID") = ddlDept_Name.SelectedValue
        'DR("Prefix_ID") = ddlPrefix.SelectedValue
        'DR("Fisrt_Name") = txtFisrt_Name.Text
        'DR("Last_Name") = txtLast_Name.Text
        'DR("Pos_Name") = txtPos_Name.Text

        '--------------- ขาดเรื่อง สิทธิ์ตามฝ่าย โดย Phase นี้ Lock ไว้ที่ศูนย์บริการโลหิตแห่งชาติสภากาชาดไทย------------
        DR("Cancel_Role") = IsCancel_Role
        DR("Active_Status") = IsActive

        If DT.Rows.Count = 0 Then DT.Rows.Add(DR)
        Dim cmd As New SqlCommandBuilder()
        cmd = New SqlCommandBuilder(DA)
        DA.Update(DT)

    End Sub
    Private Sub AutoSave()
        '----------------ตรวจสอบ Validation-----------------------------

        If ddlPrefix.SelectedIndex = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','เลือกคำนำหน้า','');", True)
            ddlPrefix.Focus()
            Exit Sub
        End If
        If txtFisrt_Name.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','กรอกชื่อ','');", True)
            txtFisrt_Name.Focus()
            Exit Sub
        End If
        If txtLast_Name.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','กรอกนามสกุลผู้ใช้','');", True)
            txtLast_Name.Focus()
            Exit Sub
        End If
        If txtUser_Login.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','กรอกชื่อผู้ใช้','');", True)
            txtUser_Login.Focus()
            Exit Sub
        End If
        If txtUser_Password.Text = "" Or txtUser_Password_Confirm.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','กรอกรหัสผ่าน','');", True)
            txtUser_Password.Focus()
            Exit Sub
        End If
        If txtUser_Password.Text <> txtUser_Password_Confirm.Text Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','ตรวจสอบรหัสผ่านให้ตรงกัน','');", True)
            txtUser_Password.Focus()
            Exit Sub
        End If

        'If ddlPrefix.SelectedIndex = 0 Or txtFisrt_Name.Text = "" Or txtLast_Name.Text = "" Then
        '    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','ข้อมูลให้ครบ','');", True)
        '    If ddlPrefix.SelectedIndex = 0 Then
        '        ddlPrefix.Focus()
        '    ElseIf txtFisrt_Name.Text = "" Then
        '        txtFisrt_Name.Focus()
        '    Else
        '        txtLast_Name.Focus()
        '    End If
        '    Exit Sub
        'End If

        '------------check ซ้ำ-----------------
        Dim SQL As String = ""
        SQL &= " SELECT * FROM tb_User" & vbLf
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        DT.DefaultView.RowFilter = " User_Login='" & txtUser_Login.Text & "' AND User_Password ='" & txtUser_Password.Text & "'" & " AND User_ID <>'" & User_ID & "'"
        If DT.DefaultView.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','User และ Password มีอยู่แล้ว','');", True)
            txtUser_Login.Focus()
            Exit Sub
        End If

        '---------------------------รายละเอียด-----------------------------------
        SQL = ""
        SQL &= " SELECT * " & vbLf
        SQL &= " FROM tb_User" & vbLf
        SQL &= " WHERE User_ID=" & User_ID & "" & vbLf
        DA = New SqlDataAdapter(SQL, BL.ConnectionString)
        DT = New DataTable
        DA.Fill(DT)
        If User_ID = 0 Then User_ID = BL.GetNewPrimaryID("tb_User", "User_ID")
        '------------บันทึก --------------
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            DR("User_ID") = User_ID
        Else
            DR = DT.Rows(0)
            DR("Update_By") = Session("User_ID")
            DR("Update_Time") = Now
        End If
        DR("User_Login") = txtUser_Login.Text
        DR("User_Password") = txtUser_Password.Text
        DR("Dept_ID") = ddlDept_Name.SelectedValue
        DR("Prefix_ID") = ddlPrefix.SelectedValue
        DR("Fisrt_Name") = txtFisrt_Name.Text
        DR("Last_Name") = txtLast_Name.Text
        DR("Pos_Name") = txtPos_Name.Text

        '--------------- ขาดเรื่อง สิทธิ์ตามฝ่าย โดย Phase นี้ Lock ไว้ที่ศูนย์บริการโลหิตแห่งชาติสภากาชาดไทย------------
        DR("Cancel_Role") = IsCancel_Role
        DR("Active_Status") = IsActive

        If DT.Rows.Count = 0 Then DT.Rows.Add(DR)
        Dim cmd As New SqlCommandBuilder()
        cmd = New SqlCommandBuilder(DA)
        DA.Update(DT)

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('success','บันทึกเรียบร้อย','');", True)
        pnlEdit_btnOK.Visible = True
        pnlEdit_Role.Visible = True

    End Sub

    Protected Sub btnOK_Click(sender As Object, e As System.EventArgs) Handles btnOK.Click ', ddlPrefix.SelectedIndexChanged, txtFisrt_Name.TextChanged, txtLast_Name.TextChanged, ddlDept_Name.SelectedIndexChanged, txtUser_Login.TextChanged, txtUser_Password.TextChanged
        AutoSave()
    End Sub

    Protected Sub imgStatus_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgStatus.Click
        IsActive = Not IsActive
        'AutoSave()
        SaveFlag()
    End Sub

#End Region

    Protected Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
        pnlList.Visible = True
        pnlEdit.Visible = False
        BindList()
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As System.EventArgs) Handles btnSearch.Click, txtSearchUserName.TextChanged, txtSearchFullname.TextChanged, ddlSearchCreater.SelectedIndexChanged, ddlSearchAuditor.SelectedIndexChanged, ddlSearchSuppliesUnit.SelectedIndexChanged, ddlSearchDirector.SelectedIndexChanged, ddlSearchAllow.SelectedIndexChanged
        'IsActive = Not IsActive
        BindList()
    End Sub

    Protected Sub imgCancel_Role_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgCancel_Role.Click
        IsCancel_Role = Not IsCancel_Role
        'AutoSave()
        SaveFlag()
    End Sub
End Class
