﻿Imports System.Data
Imports System.IO

Partial Class RenderFile
    Inherits System.Web.UI.Page

    Dim BL As New AVLBL
    Dim PDFLib As New PdfLibrary

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Select Case True
            Case Not IsNothing(Request.QueryString("S")) AndAlso Request.QueryString("S") = "Assessment_Preview_File_New"
                Dim UploadedFile As AVLBL.FileStructure = Session("Assessment_Preview_File_New")
                Response.Clear()

                'Select Case Request.QueryString("Mode")
                '    Case "Thumbnail"
                '        If UploadedFile.ContentType = "application/pdf" Then
                '            '--------- Save Stream To Temp File----------
                '            Dim Path As String = Server.MapPath("Temp") & "\Assessment_Preview_File_New_" & Now.ToOADate.ToString.Replace(".", "") & ".pdf"
                '            Dim F As New FileStream(Path, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite)
                '            F.Write(UploadedFile.FileContent, 0, UploadedFile.FileContent.Length)
                '            F.Close()
                '            'PDFLib.ConvertPdfToImage(Path)
                '            'Response.AppendHeader("Content-Type", UploadedFile.ContentType)
                '            'Response.BinaryWrite(UploadedFile.FileContent)
                '        Else
                '            Response.AppendHeader("Content-Type", UploadedFile.ContentType)
                '            Response.BinaryWrite(UploadedFile.FileContent)
                '        End If
                '    Case "File"
                '        Response.AppendHeader("Content-Type", UploadedFile.ContentType)
                '        Response.BinaryWrite(UploadedFile.FileContent)
                'End Select

                Response.AppendHeader("Content-Type", UploadedFile.ContentType)
                Response.BinaryWrite(UploadedFile.FileContent)
            Case IsNumeric(Request.QueryString("A")) AndAlso IsNumeric(Request.QueryString("F"))
                Dim DT As DataTable = BL.GetFileByID(Request.QueryString("A"), Request.QueryString("F"), Server.MapPath(""))
                If DT.Rows.Count = 0 Then
                    Response.Redirect("images/BlackDot.png")
                ElseIf IsDBNull(DT.Rows(0).Item("Content")) Then
                    Response.Redirect("images/BlackDot.png")
                Else
                    Response.Clear()
                    Response.AppendHeader("Content-Type", DT.Rows(0).Item("Content_Type"))
                    Response.BinaryWrite(DT.Rows(0).Item("Content"))
                End If
            Case Else
                Response.Redirect("images/BlackDot.png")
        End Select
    End Sub
End Class
