﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Setting_ReAss_Question
    Inherits System.Web.UI.Page

    Dim BL As New AVLBL
    Dim CL As New textControlLib
    Dim GL As New GenericLib

    Private ReadOnly Property PageName As String
        Get
            Return "Setting_ReAss_Question.aspx"
        End Get
    End Property
    Private Property AccessMode As AVLBL.AccessRole
        Get
            Try
                Return ViewState("AccessMode")
            Catch ex As Exception
                Return AVLBL.AccessRole.None
            End Try
        End Get
        Set(value As AVLBL.AccessRole)
            ViewState("AccessMode") = value
        End Set
    End Property
    Private ReadOnly Property User_ID As Integer
        Get
            Try
                If IsNumeric(Session("User_ID")) Then
                    Return Session("User_ID")
                Else
                    Return -1
                End If
            Catch ex As Exception
                Return -1
            End Try
        End Get
    End Property

    Public Property Q_GP_ID() As Integer
        Get
            Return lblQ_GP_ID.Text
        End Get
        Set(ByVal value As Integer)
            lblQ_GP_ID.Text = value
        End Set
    End Property

    Public Property Q_ID() As Integer
        Get
            Return lblDialog_Q_ID.Text
        End Get
        Set(ByVal value As Integer)
            lblDialog_Q_ID.Text = value
        End Set
    End Property

    Public Property IsGroupActive As Boolean
        Get
            Return imgGroupStatus.ImageUrl = "images/check.png"
        End Get
        Set(value As Boolean)
            If value Then
                imgGroupStatus.ImageUrl = "images/check.png"
            Else
                imgGroupStatus.ImageUrl = "images/none.png"
            End If
        End Set
    End Property

    Public Property IsQuestionActive As Boolean
        Get
            Return imgQuestionStatus.ImageUrl = "images/check.png"
        End Get
        Set(value As Boolean)
            If value Then
                imgQuestionStatus.ImageUrl = "images/check.png"
            Else
                imgQuestionStatus.ImageUrl = "images/none.png"
            End If
        End Set
    End Property

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        If IsNothing(Session("User_ID")) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "ShowAlert('warning','กรุณาล็อกอินเข้าสู่ระบบ','Login.aspx');", True)
            Response.Redirect("Login.aspx")
            Exit Sub
        End If

        If Not IsPostBack Then
            SetUserRole()
            BindQuestion_List()
            ClearForm()

        End If
    End Sub

    Private Sub SetUserRole()

        '----------- Check สิทธิ์ในเมนู --------------
        Dim MT As DataTable = BL.GetUserMenuRole(User_ID, PageName)
        If MT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลนี้','Overview.aspx');", True)
            Exit Sub
        End If
        AccessMode = MT.Rows(0).Item("MR_ID") '--------- สิทธิ์ในเมนูของผู้ใช้คนปัจจุบัน -----------
        If AccessMode = AVLBL.AccessRole.None Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Out", "ShowAlert('warning','คุณไม่ได้รับสิทธิ์ให้เข้าถึงข้อมูลนี้','Overview.aspx');", True)
            Exit Sub
        End If

        btnAdd_Group_Question.Visible = AccessMode = AVLBL.AccessRole.Edit
        thEdit_Header.Visible = AccessMode = AVLBL.AccessRole.Edit
    End Sub

    Private Sub ClearForm()
        pnlList.Visible = True
        pnlEdit.Visible = False
        Modal_Add.Visible = False
        Q_GP_ID = 0
        Q_ID = 0
    End Sub


#Region "rptList"

    Private Sub BindQuestion_List()
        Dim SQL As String = " "
        SQL &= " SELECT Q_Group.Q_GP_ID,Q_Group.Q_GP_Name,Q_Group.Q_GP_Order,Q_Group.Active_Status QG_Status," & vbLf
        SQL &= " Q.Q_ID,Q.Q_Name,Q.AR_ID ,tb_Ass_Role.AR_Name_TH ,Q.Q_Order,ISNULL(Q.Pass_Score,0) Pass_Score,ISNULL(Q.Max_Score,0) Max_Score,Q.Active_Status Q_Status" & vbLf
        SQL &= " FROM tb_Re_Q_Group Q_Group " & vbLf
        SQL &= " LEFT JOIN tb_Re_Q Q ON Q_Group.Q_GP_ID=Q.Q_GP_ID" & vbLf
        SQL &= " LEFT JOIN tb_Ass_Role ON Q.AR_ID  = tb_Ass_Role.AR_ID"
        SQL &= "  ORDER BY Q_GP_Order,Q_GP_Name ,Q_Order,Q_Name" & vbLf
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then

            Dim SumScore_Question As Integer = DT.Compute("SUM(Max_Score)", " Q_Status =1 AND Q_Status IS NOT NULL ")
            Dim SumScore_Pass As Object = DT.Compute("SUM(Pass_Score)", " Q_Status =1 AND Q_Status IS NOT NULL ")

            If Not IsDBNull(SumScore_Question) Then
                lblSUM_Max_Score.Text = FormatNumber(SumScore_Question, 0)
            Else
                lblSUM_Max_Score.Text = "-"
            End If

            If Not IsDBNull(SumScore_Pass) Then
                lblSUM_Pass_Score.Text = FormatNumber(SumScore_Pass, 0)
            Else
                lblSUM_Pass_Score.Text = "-"
            End If

        Else
            trFooter.Visible = False
        End If

        rptList.DataSource = DT
        rptList.DataBind()

    End Sub

    Protected Sub rptList_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptList.ItemCommand
        Select Case e.CommandName
            Case "Edit"
                Dim btnEdit As Button = e.Item.FindControl("btnEdit")
                Q_GP_ID = btnEdit.CommandArgument
                Dim QuestionInfo As AVLBL.Q_GP_Info = BL.GetQ_GP_Info(Q_GP_ID, AVLBL.AssessmentType.ReAssessment)
                txtQ_GP_Name.Text = QuestionInfo.Q_GP_Name
                txtQ_GP_Order.Text = QuestionInfo.Q_GP_Order
                IsGroupActive = QuestionInfo.Active_Status
                pnlList.Visible = False
                pnlEdit.Visible = True
                btnDelete.Visible = True
                BL.BindDDlQ_Group_Order(ddlQ_GP_Order, AVLBL.AssessmentType.ReAssessment, QuestionInfo.Q_GP_Order, 1)
                BindSubQuestion_List()
                btnAdd_Question.Visible = True
        End Select
    End Sub

    Dim LastName As String = ""
 



    Protected Sub rptList_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lblQ_GP_Name As Label = e.Item.FindControl("lblQ_GP_Name")
        Dim lblQ_Name As Label = e.Item.FindControl("lblQ_Name")
        Dim lblPass_Score As Label = e.Item.FindControl("lblPass_Score")
        Dim lblName_Role As Label = e.Item.FindControl("lblName_Role")

        Dim lblMax_Score As Label = e.Item.FindControl("lblMax_Score")
        '-------- Partition Layout ---------------
        Dim td1 As HtmlTableCell = e.Item.FindControl("td1")
        Dim td2 As HtmlTableCell = e.Item.FindControl("td2")

        td1.Visible = AccessMode = AVLBL.AccessRole.Edit

        Dim img_Status As HtmlAnchor = e.Item.FindControl("img_Status")
        Dim btnEdit As Button = e.Item.FindControl("btnEdit")
        Dim tr_rptList As HtmlTableRow = e.Item.FindControl("tr_rptList")

        Dim DuplicatedStyleTop As String = "border-top:none;"
        If LastName <> e.Item.DataItem("Q_GP_Name").ToString Then
            LastName = e.Item.DataItem("Q_GP_Name").ToString
            lblQ_GP_Name.Text = e.Item.DataItem("Q_GP_Name").ToString
            btnEdit.CommandArgument = e.Item.DataItem("Q_GP_ID").ToString
        Else
            btnEdit.Visible = False
            td1.Attributes("style") &= DuplicatedStyleTop
            td2.Attributes("style") &= DuplicatedStyleTop
        End If
        lblName_Role.Text = e.Item.DataItem("AR_Name_TH").ToString
            lblQ_Name.Text = e.Item.DataItem("Q_Name").ToString
            If Not IsDBNull(e.Item.DataItem("Pass_Score")) Then
                lblPass_Score.Text = FormatNumber(e.Item.DataItem("Pass_Score"), 0)
            End If
            If Not IsDBNull(e.Item.DataItem("Max_Score")) Then
                lblMax_Score.Text = FormatNumber(e.Item.DataItem("Max_Score"), 0)
            End If

            If e.Item.DataItem("QG_Status") Then
                If Not IsDBNull(e.Item.DataItem("Q_Status")) Then
                    If e.Item.DataItem("Q_Status") Then
                        img_Status.InnerHtml = "<img src='images/check.png' />"
                        img_Status.Title = "แสดงในระบบ"
                        lblQ_GP_Name.Style("color") = "black"
                        lblQ_Name.Style("color") = "black"
                        lblMax_Score.Style("color") = "black"
                        lblPass_Score.Style("color") = "black"
                    Else
                        img_Status.InnerHtml = "<img src='images/none.png' />"
                        img_Status.Title = "ไม่แสดงในระบบ"
                        lblQ_GP_Name.Style("color") = "gray"
                        lblQ_Name.Style("color") = "gray"
                        lblMax_Score.Style("color") = "gray"
                        lblPass_Score.Style("color") = "gray"
                    End If
                Else
                    img_Status.InnerHtml = ""
                End If
            Else
                img_Status.InnerHtml = "<img src='images/none.png' />"
                img_Status.Title = "ไม่แสดงในระบบ"
                lblQ_GP_Name.Style("color") = "gray"
                lblQ_Name.Style("color") = "gray"
                lblMax_Score.Style("color") = "gray"

            End If

    End Sub

#End Region


#Region "button"

    '--------Group Question---------------

    Protected Sub btnAdd_Group_Question_Click(sender As Object, e As System.EventArgs) Handles btnAdd_Group_Question.Click
        btnAdd_Question.Visible = False
        txtQ_GP_Name.Text = ""
        txtQ_GP_Order.Text = ""
        Q_GP_ID = 0
        IsGroupActive = True
        pnlList.Visible = False
        pnlEdit.Visible = True
        pnlList_Question.Visible = False
        btnDelete.Visible = False
        BL.BindDDlQ_Group_Order(ddlQ_GP_Order, AVLBL.AssessmentType.ReAssessment, 0, 1)
    End Sub

    '-------- Question---------------
    Protected Sub btnAdd_Question_Click(sender As Object, e As System.EventArgs) Handles btnAdd_Question.Click

        '----------------ตรวจสอบ Validation-----------------------------
        If txtQ_GP_Name.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','กรอกหมวดคำถาม ก่อนเพิ่มหัวข้อการประเมิน','');", True)
            txtQ_GP_Name.Focus()
            Exit Sub
        End If
        Modal_Add.Visible = True
        lblDialog_Q_ID.Text = 0
        txtDialog_Q_Name.Text = ""
        txtDialog_Pass_Score.Text = ""
        txtDialog_Max_Score.Text = ""
        txtDialog_Q_Order.Text = ""
        CL.ImplementJavaIntegerText(txtDialog_Max_Score)
        CL.ImplementJavaIntegerText(txtDialog_Pass_Score)
        IsQuestionActive = True
        BL.BindDDlQ_Order(ddl_Q_Order, Q_GP_ID, AVLBL.AssessmentType.ReAssessment, 0, 1)

    End Sub


    Protected Sub btnOK_Click(sender As Object, e As System.EventArgs) Handles btnOK.Click
        SaveQuestion()

        If Not IsGroupActive Then
            Dim SQL As String = ""
            SQL &= " UPDATE  " & vbLf
            SQL &= " tb_Re_Q" & vbLf
            SQL &= " SET Active_Status=0"
            SQL &= " WHERE Q_GP_ID=" & Q_GP_ID & "" & vbLf
            Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
            Dim DT As New DataTable
            DA.Fill(DT)
        End If
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('Success',' บันทึกสำเร็จ','');", True)

    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
        pnlList.Visible = True
        pnlEdit.Visible = False
        BindQuestion_List()
    End Sub

    Protected Sub btnDelete_Click(sender As Object, e As System.EventArgs) Handles btnDelete.Click
        Del_Q_GP_ID()
    End Sub

    Protected Sub btnClose_Click(sender As Object, e As System.EventArgs) Handles btnClose.Click
        Modal_Add.Visible = False
    End Sub

    Protected Sub btnOK_Sub_Click(sender As Object, e As System.EventArgs) Handles btnOK_Sub.Click

        '----------------ตรวจสอบ Validation-----------------------------
        If txtDialog_Q_Name.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','กรอกหัวข้อย่อย','');", True)
            txtDialog_Q_Name.Focus()
            Exit Sub
        End If
        If ddlAss_Role.SelectedIndex = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','เลือกสิทธิผู้ประเมิน','');", True)
            ddlAss_Role.Focus()
            Exit Sub
        End If
        If (Val(txtDialog_Pass_Score.Text) > Val(txtDialog_Max_Score.Text)) Or txtDialog_Pass_Score.Text = "" Or txtDialog_Max_Score.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','ตรวจสอบเกณฑ์คะแนน','');", True)
            ddlAss_Role.Focus()
            Exit Sub
        End If
        '------------check หัวข้อซ้ำ-----------------
        Dim SQL As String = ""
        SQL &= " SELECT * FROM tb_Re_Q" & vbLf
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        DT.DefaultView.RowFilter = " Q_Name='" & txtDialog_Q_Name.Text & "' AND Q_ID <>" & Q_ID & ""
        If DT.DefaultView.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','หัวข้อย่อยซ้ำ','');", True)
            txtDialog_Q_Name.Focus()
            Exit Sub
        End If

        If txtDialog_Max_Score.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','กรอกคะแนนเต็ม','');", True)
            txtDialog_Max_Score.Focus()
            Exit Sub
        End If

        '--------------------------------------------------------------
        SQL = ""
        SQL &= " SELECT * " & vbLf
        SQL &= " FROM tb_Re_Q" & vbLf
        SQL &= " WHERE Q_GP_ID=" & Q_GP_ID & " AND Q_ID=" & Q_ID & vbLf
        DA = New SqlDataAdapter(SQL, BL.ConnectionString)
        DT = New DataTable
        DA.Fill(DT)
        If Q_ID = 0 Then Q_ID = BL.GetNewPrimaryID("tb_Re_Q", "Q_ID")
        '------------บันทึกตาราง tb_Re_Q --------------
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            DR("Q_GP_ID") = Q_GP_ID
            DR("Q_ID") = Q_ID
        Else
            DR = DT.Rows(0)
        End If
        DR("AR_ID") = ddlAss_Role.SelectedIndex
        DR("Update_By") = Session("User_ID")
        DR("Update_Time") = Now
        DR("Q_Name") = txtDialog_Q_Name.Text

        If txtDialog_Pass_Score.Text <> "" Then
            DR("Pass_Score") = txtDialog_Pass_Score.Text
        Else
            DR("Pass_Score") = DBNull.Value
        End If
        If txtDialog_Max_Score.Text <> "" Then
            DR("Max_Score") = txtDialog_Max_Score.Text
        Else
            DR("Max_Score") = DBNull.Value
        End If
        If ddl_Q_Order.SelectedIndex <> 0 Then
            If Val(txtDialog_Q_Order.Text) > Val(ddl_Q_Order.SelectedItem.ToString()) Then
                DR("Q_Order") = Val(ddl_Q_Order.SelectedItem.ToString()) - 0.5
            Else
                DR("Q_Order") = Val(ddl_Q_Order.SelectedItem.ToString()) + 0.5
            End If
        End If
        DR("Active_Status") = IsQuestionActive

        If DT.Rows.Count = 0 Then DT.Rows.Add(DR)
        Dim cmd As New SqlCommandBuilder()
        cmd = New SqlCommandBuilder(DA)
        DA.Update(DT)
        REORDER("Sub_Group")
        BindSubQuestion_List()
        Modal_Add.Visible = False
    End Sub

    Protected Sub imgGroupStatus_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgGroupStatus.Click
        IsGroupActive = Not IsGroupActive
        'SaveQuestion()
    End Sub

    Protected Sub imgQuestionStatus_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles imgQuestionStatus.Click
        IsQuestionActive = Not IsQuestionActive
    End Sub

    Private Sub SaveQuestion()

        '----------------ตรวจสอบ Validation-----------------------------
        If txtQ_GP_Name.Text.Trim = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','กรอกหมวดคำถาม','');", True)
            txtQ_GP_Name.Focus()
            Exit Sub
        End If

        '------------check หมวดคำถามซ้ำ-----------------
        Dim SQL As String = ""
        SQL &= " SELECT * FROM tb_Re_Q_Group" & vbLf
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)

        DT.DefaultView.RowFilter = " Q_GP_Name='" & txtQ_GP_Name.Text.Trim & "' AND Q_GP_ID <>" & Q_GP_ID & ""
        If DT.DefaultView.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','มีหมวดคำถามนี้แล้ว','');", True)
            txtQ_GP_Name.Focus()
            Exit Sub
        End If

        '---------------------------หมวดคำถาม-----------------------------------
        SQL = ""
        SQL &= " SELECT * " & vbLf
        SQL &= " FROM tb_Re_Q_Group" & vbLf
        SQL &= " WHERE Q_GP_ID=" & Q_GP_ID & "" & vbLf
        DA = New SqlDataAdapter(SQL, BL.ConnectionString)
        DT = New DataTable
        DA.Fill(DT)
        If Q_GP_ID = 0 Then Q_GP_ID = BL.GetNewPrimaryID("tb_Re_Q_Group", "Q_GP_ID")
        '------------บันทึกตาราง tb_Re_Q_Group --------------
        Dim DR As DataRow
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            DR("Q_GP_ID") = Q_GP_ID
        Else
            DR = DT.Rows(0)
        End If
        DR("Update_By") = Session("User_ID")
        DR("Update_Time") = Now
        DR("Q_GP_Name") = txtQ_GP_Name.Text
        If ddlQ_GP_Order.SelectedValue <> 0 Then
            If Val(txtQ_GP_Order.Text) > Val(ddlQ_GP_Order.SelectedItem.ToString()) Then
                DR("Q_GP_Order") = Val(ddlQ_GP_Order.SelectedItem.ToString()) - 0.5
            Else
                DR("Q_GP_Order") = Val(ddlQ_GP_Order.SelectedItem.ToString()) + 0.5
            End If
        End If
        DR("Active_Status") = IsGroupActive
        If DT.Rows.Count = 0 Then DT.Rows.Add(DR)
        Dim cmd As New SqlCommandBuilder()
        cmd = New SqlCommandBuilder(DA)
        DA.Update(DT)
        REORDER("Group")
        btnAdd_Question.Visible = True
    End Sub

#End Region


#Region "Group Question"

    Private Sub Del_Q_GP_ID()
        '-----------------------ตรวจสอบในตารางอื่นๆก่อนการลบ MASTER หัวข้อการประเมิน-----------------------------------------------------
        Dim SQL As String = " SELECT * FROM tb_ReAss_Detail"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        DT.DefaultView.RowFilter = " Q_GP_ID=" & Q_GP_ID & vbLf

        If DT.DefaultView.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning',' มีประวัติการออกรายงานนี้อยู่ ไม่สามารถลบข้อมูลได้','');", True)
            Exit Sub
        Else
            '---------------------------------------------------
            SQL = " "
            SQL = " DELETE  FROM tb_Re_Q WHERE Q_GP_ID=" & Q_GP_ID & vbLf
            DA = New SqlDataAdapter(SQL, BL.ConnectionString)
            DT = New DataTable
            DA.Fill(DT)
            SQL = " DELETE  FROM tb_Re_Q_Group WHERE Q_GP_ID=" & Q_GP_ID & vbLf
            DA = New SqlDataAdapter(SQL, BL.ConnectionString)
            DT = New DataTable
            DA.Fill(DT)
            BindSubQuestion_List()
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('success',' ลบเรียบร้อยแล้ว','');", True)
            pnlList.Visible = True
            pnlEdit.Visible = False
            BindQuestion_List()
            Exit Sub

        End If
    End Sub

    Protected Sub txtQ_GP_Name_TextChanged(sender As Object, e As System.EventArgs) Handles txtQ_GP_Name.TextChanged, txtQ_GP_Order.TextChanged
        SaveQuestion()
    End Sub


#End Region


#Region "Question"

    Private Sub BindSubQuestion_List()
        Dim SQL As String = " SELECT tb_Re_Q.* ,tb_Ass_Role.AR_Name_TH FROM tb_Re_Q " & vbLf
        SQL &= " LEFT JOIN tb_Ass_Role ON tb_Ass_Role.AR_ID =tb_Re_Q.AR_ID" & vbLf
        SQL &= " WHERE Q_GP_ID=" & Q_GP_ID
        SQL &= " ORDER BY Q_Order"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        rptQuestion.DataSource = DT
        rptQuestion.DataBind()

        DT.DefaultView.RowFilter = "  Active_Status =1"
        If DT.DefaultView.Count > 0 Then
            Dim SumPass_SubQuestion As Object = DT.Compute("SUM(Pass_Score)", " Active_Status =1 ")
            Dim SumScore_SubQuestion As Integer = DT.Compute("SUM(Max_Score)", " Active_Status =1 ")

            If Not IsDBNull(SumScore_SubQuestion) Then
                lbl_Question_SUM_Max_Score.Text = FormatNumber(SumScore_SubQuestion, 0)
            Else
                lbl_Question_SUM_Max_Score.Text = "-"
            End If
            If Not IsDBNull(SumPass_SubQuestion) Then
                lbl_Question_SUM_Pass_Score.Text = FormatNumber(SumPass_SubQuestion, 0)
            Else
                lbl_Question_SUM_Pass_Score.Text = "-"
            End If
        Else
            lbl_Question_SUM_Max_Score.Text = "-"
        End If

        If DT.Rows.Count = 0 Then
            pnlList_Question.Visible = False
        Else
            pnlList_Question.Visible = True
        End If

    End Sub

    Protected Sub rptQuestion_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptQuestion.ItemCommand
        Select Case e.CommandName
            Case "Edit"
                Dim lbl_Question_Q_Name As Label = e.Item.FindControl("lbl_Question_Q_Name")
                Dim lbl_Question_Pass_Score As Label = e.Item.FindControl("lbl_Question_Pass_Score")
                Dim lbl_Question_Max_Score As Label = e.Item.FindControl("lbl_Question_Max_Score")
                Dim btnEdit_Question As Button = e.Item.FindControl("btnEdit_Question")
                Dim lbl_AR_Name As Label = e.Item.FindControl("lbl_AR_Name")

                Q_ID = btnEdit_Question.CommandArgument
                txtDialog_Q_Name.Text = lbl_Question_Q_Name.Text
                ddlAss_Role.SelectedIndex = lbl_AR_Name.Attributes("AR_ID")
                txtDialog_Pass_Score.Text = lbl_Question_Pass_Score.Text
                txtDialog_Max_Score.Text = lbl_Question_Max_Score.Text
                txtDialog_Q_Order.Text = lbl_Question_Q_Name.Attributes("Q_Order")
                IsQuestionActive = lbl_Question_Q_Name.Attributes("Active_Status")

                Modal_Add.Visible = True
                BL.BindDDlQ_Order(ddl_Q_Order, Q_GP_ID, AVLBL.AssessmentType.ReAssessment, lbl_Question_Q_Name.Attributes("Q_Order"))

            Case "Delete"
                Dim btnDel_Question As Button = e.Item.FindControl("btnDel_Question")
                Q_ID = btnDel_Question.CommandArgument

                '---------ลบตารางรายละเอียด----tb_Re_Q-------
                Del_Q_ID(Q_ID)

        End Select
    End Sub

    Protected Sub rptQuestion_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptQuestion.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim lbl_Question_Q_Name As Label = e.Item.FindControl("lbl_Question_Q_Name")
        Dim lbl_AR_Name As Label = e.Item.FindControl("lbl_AR_Name")
        Dim lbl_Question_Pass_Score As Label = e.Item.FindControl("lbl_Question_Pass_Score")
        Dim lbl_Question_Max_Score As Label = e.Item.FindControl("lbl_Question_Max_Score")
        Dim img_Status_Question As HtmlAnchor = e.Item.FindControl("img_Status_Question")
        Dim btnEdit_Question As Button = e.Item.FindControl("btnEdit_Question")
        Dim btnDel_Question As Button = e.Item.FindControl("btnDel_Question")

        lbl_Question_Q_Name.Text = e.Item.DataItem("Q_Name").ToString
        lbl_AR_Name.Text = e.Item.DataItem("AR_Name_TH").ToString
        If Not IsDBNull(e.Item.DataItem("AR_Name_TH")) Then
            lbl_AR_Name.Attributes("AR_ID") = Val(e.Item.DataItem("AR_ID"))
        Else
            lbl_AR_Name.Attributes("AR_ID") = 0
        End If
        lbl_Question_Pass_Score.Text = FormatNumber(e.Item.DataItem("Pass_Score"), 0)
        lbl_Question_Max_Score.Text = FormatNumber(e.Item.DataItem("Max_Score"), 0)
        lbl_Question_Q_Name.Attributes("Active_Status") = e.Item.DataItem("Active_Status")
        If e.Item.DataItem("Active_Status") Then
            img_Status_Question.InnerHtml = "<img src='images/check.png' />"
            img_Status_Question.Title = "แสดงในระบบ"

            lbl_Question_Q_Name.Style("color") = "black"
            lbl_Question_Max_Score.Style("color") = "black"
        Else
            img_Status_Question.InnerHtml = "<img src='images/none.png' />"
            img_Status_Question.Title = "ไม่แสดงในระบบ"

            lbl_Question_Q_Name.Style("color") = "gray"
            lbl_Question_Max_Score.Style("color") = "gray"
        End If
        lbl_Question_Q_Name.Attributes("Q_Order") = e.Item.DataItem("Q_Order")
        btnEdit_Question.CommandArgument = e.Item.DataItem("Q_ID").ToString
        btnDel_Question.CommandArgument = e.Item.DataItem("Q_ID").ToString


    End Sub

    Private Sub Del_Q_ID(ByVal Del_Q_ID As Integer)
        '-----------------------ตรวจสอบในตารางอื่นๆก่อนการลบ MASTER หัวข้อการประเมิน-----------------------------------------------------
        '---------------------------------------------------

        Dim SQL As String = " SELECT * FROM tb_ReAss_Detail"
        Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        Dim DT As New DataTable
        DA.Fill(DT)
        DT.DefaultView.RowFilter = " Q_ID=" & Del_Q_ID & vbLf

        If DT.DefaultView.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning',' มีประวัติการออกรายงานนี้อยู่ ไม่สามารถลบข้อมูลได้','');", True)
            Exit Sub
        Else
            '---------------------------------------------------
            SQL = " "
            SQL = " DELETE  FROM tb_Re_Q WHERE Q_GP_ID=" & Q_GP_ID & " AND Q_ID=" & Del_Q_ID & vbLf
            DA = New SqlDataAdapter(SQL, BL.ConnectionString)
            DT = New DataTable
            DA.Fill(DT)
            REORDER("Sub_Group")
            BindSubQuestion_List()
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('Success',' ลบเรียบร้อยแล้ว','');", True)
            Exit Sub
        End If
    End Sub

#End Region

    Private Sub REORDER(ByRef Group_Level As String)
        'Dim SQL As String = ""
        'SQL &= " SELECT Q_ID,Q_Order FROM tb_Re_Q" & vbLf
        'Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
        'Dim DT As New DataTable
        'DA.Fill(DT)
        'Dim DR As DataRow
        'Dim DT_TMP As DataTable = BL.GetSubQuestionsByCurrentSetting(Q_GP_ID, AVLBL.AssessmentType.ReAssessment)
        'For i As Integer = 0 To DT_TMP.Rows.Count - 1
        '    DR = DT_TMP.Rows(i)
        '    DR("Q_ID") = DT_TMP.Rows(i).Item("Q_ID")
        '    DR("Q_Order") = i + 1
        'Next
        'Dim cmd As New SqlCommandBuilder(DA)
        'DA.Update(DT_TMP)

        Dim SQL As String = ""

        Select Case Group_Level
            Case "Group"
                SQL &= " SELECT Q_GP_ID,Q_GP_Order FROM tb_Re_Q_Group ORDER BY Q_GP_Order" & vbLf
                Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
                Dim DT As New DataTable
                DA.Fill(DT)
                Dim DR As DataRow
                Dim DT_TMP As DataTable = DT.Copy
                For i As Integer = 0 To DT_TMP.Rows.Count - 1
                    DR = DT_TMP.Rows(i)
                    DR("Q_GP_ID") = DT_TMP.Rows(i).Item("Q_GP_ID")
                    DR("Q_GP_Order") = i + 1
                Next
                Dim cmd As New SqlCommandBuilder(DA)
                DA.Update(DT_TMP)
            Case "Sub_Group"
                
                SQL &= " SELECT Q_ID,Q_Order FROM tb_Re_Q" & vbLf
                Dim DA As New SqlDataAdapter(SQL, BL.ConnectionString)
                Dim DT As New DataTable
                DA.Fill(DT)
                Dim DR As DataRow
                Dim DT_TMP As DataTable = BL.GetSubQuestionsByCurrentSetting(Q_GP_ID, AVLBL.AssessmentType.ReAssessment)
                For i As Integer = 0 To DT_TMP.Rows.Count - 1
                    DR = DT_TMP.Rows(i)
                    DR("Q_ID") = DT_TMP.Rows(i).Item("Q_ID")
                    DR("Q_Order") = i + 1
                Next
                Dim cmd As New SqlCommandBuilder(DA)
                DA.Update(DT_TMP)
        End Select
    End Sub


    Protected Sub ddlQ_GP_Order_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlQ_GP_Order.SelectedIndexChanged
        'SaveQuestion()
    End Sub


   
    Protected Sub txtDialog_Max_Score_TextChanged(sender As Object, e As System.EventArgs) Handles txtDialog_Max_Score.TextChanged
        If CInt(txtDialog_Max_Score.Text) < 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','กรอกเป็นจำนวนเต็ม','');", True)
            txtDialog_Max_Score.Text = ""
            txtDialog_Max_Score.Focus()
            Exit Sub
        End If
    End Sub

    Protected Sub txtDialog_Pass_Score_TextChanged(sender As Object, e As System.EventArgs) Handles txtDialog_Pass_Score.TextChanged
        If CInt(txtDialog_Pass_Score.Text) < 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "ShowAlert('warning','กรอกเป็นจำนวนเต็ม','');", True)
            txtDialog_Pass_Score.Text = ""
            txtDialog_Pass_Score.Focus()
            Exit Sub
        End If
    End Sub
End Class


